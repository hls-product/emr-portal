import { insuranceModel } from "../../models/insurance";
import {
  GetRequestType,
  ListResponse,
  PostRequestType,
} from "../../models/shared/basePagedResponse";
import _axios from "../interceptor";

export default class InsuranceService {
  private readonly endpoint = "/insurance-plans";
  getList = ({ params }: GetRequestType) => {
    return _axios
      .get<ListResponse<insuranceModel>>(this.endpoint, { params: params })
      .then((res) => res.data);
  };
  getById = (id: string) => {
    return _axios
      .get<insuranceModel>(`${this.endpoint}/${id}`)
      .then((res) => res.data);
  };
  post = ({ body }: PostRequestType) => {
    return _axios
      .post<insuranceModel>(this.endpoint, body)
      .then((res) => res.data);
  };
  update = ({ body }: PostRequestType, id: string) => {
    return _axios
      .patch<insuranceModel>(`${this.endpoint}/${id}`, body)
      .then((res) => res.data);
  };
  delete = (id: string) => {
    return _axios
      .delete<boolean>(`${this.endpoint}/${id}`)
      .then((res) => res.data);
  };
}
