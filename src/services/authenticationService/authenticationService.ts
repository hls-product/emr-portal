import axios from "axios";
import qs from "qs";
import { Authentication } from "../../models/authentication";
interface loginType {
  username: string;
  password: string;
}

export default class AuthenticationService {
  login = ({ username, password }: loginType) => {
    return axios.post<Authentication>(
      "https://aka-keycloak.herokuapp.com/auth/realms/aka-mental/protocol/openid-connect/token",
      qs.stringify({
        client_id: "aka-mental-client",
        username: username,
        password: password,
        grant_type: "password",
      }),
      {
        headers: {
          "content-type": "application/x-www-form-urlencoded",
        },
      }
    );
  };
}
