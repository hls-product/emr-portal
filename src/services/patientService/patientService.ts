import { PatientGetRequest } from "../../models/apiRequest/patient";
import { patientModel } from "../../models/patients";
import _axios from "../interceptor";
import {
  GetRequestBase,
  ListResponse,
  PatchRequestBase,
  PostRequestBase,
} from "./../../models/shared/basePagedResponse";

export default class PatientService {
  private baseURL = "/patients";
  getList = async ({ params }: GetRequestBase<PatientGetRequest>) => {
    return _axios
      .get<ListResponse<patientModel>>(this.baseURL, { params: params })
      .then((res) => res.data);
  };
  getById = async (filter: string) => {
    return _axios
      .get<patientModel>(`${this.baseURL}/${filter}`)
      .then((res) => res.data);
  };
  post = async ({ body }: PostRequestBase<patientModel>) => {
    return _axios
      .post<patientModel>(this.baseURL, body)
      .then((res) => res.data);
  };
  delete = async (id: string) => {
    return _axios
      .delete<boolean>(`${this.baseURL}/${id}`)
      .then((res) => res.data);
  };
  patch = async ({ id, body }: PatchRequestBase<patientModel>) => {
    return _axios
      .patch<patientModel>(`${this.baseURL}/${id}`, body)
      .then((res) => res.data);
  };
}
