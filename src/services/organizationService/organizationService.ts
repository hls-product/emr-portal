import { Organization } from "./../../models/organizations/organization";
import {
  GetRequestType,
  ListResponse,
} from "./../../models/shared/basePagedResponse";
import _axios from "../interceptor";

export default class OrganizationService {
  getList = async ({ params }: GetRequestType) => {
    return _axios
      .get<ListResponse<Organization>>(`/organizations`, { params: params })
      .then((response) => response.data);
  };
  getById = async (id: string) => {
    return _axios
      .get<Organization>(`/organizations/${id}`)
      .then((response) => response.data);
  };
  delete = async (id: string) => {
    return _axios.delete(`/organizations/${id}`).then((response) => {
      if (response.data === true) return true;
      return false;
    });
  };
  patch = async (item: Organization) => {
    item.alias = undefined;
    return _axios
      .patch<Organization>(`/organizations/${item.id}`, item)
      .then((response) => response.data);
  };
  add = async (item: Organization) => {
    item.alias = undefined;
    return _axios
      .post<Organization>(`/organizations`, item)
      .then((response) => response.data);
  };
}
