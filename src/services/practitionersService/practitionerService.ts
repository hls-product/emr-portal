import { PractitionerGetRequest } from "../../models/apiRequest/practitioner";
import { practitionerModel } from "../../models/practitioners/practitioners";
import { GetRequestBase, ListResponse, PatchRequestBase, PostRequestBase, SingleResponse } from "../../models/shared/basePagedResponse";
import _axios from "../interceptor";
export default class PractitionersService {
    getList = async ({ params }: GetRequestBase<PractitionerGetRequest>) => {
        return _axios
          .get<ListResponse<practitionerModel>>(`/patients`, { params: params })
          .then((res) => res.data);
      };
      getById = async (filter: string) => {
        return _axios
          .get<practitionerModel>(`/patients/${filter}`)
          .then((res) => res.data);
      };
      post = async ({ body }: PostRequestBase<practitionerModel>) => {
        return _axios
          .post<practitionerModel>(`/patients`, body)
          .then((res) => res.data);
      };
      delete = async (id: string) => {
        return _axios
          .delete<SingleResponse<practitionerModel>>(`/patients/${id}`)
          .then((res) => res.data);
      };
      patch = async ({ id, body }: PatchRequestBase<practitionerModel>) => {
        return _axios
          .patch<practitionerModel>(`/patients/${id}`, body)
          .then((res) => res.data);
      };
}
