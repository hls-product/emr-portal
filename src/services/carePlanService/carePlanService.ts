import {
  PatchRequestBase,
  PostRequestBase,
  GetRequestType,
} from "./../../models/shared/basePagedResponse";
import _axios from "../interceptor";
import {
  ListResponse,
  SingleResponse,
} from "../../models/shared/basePagedResponse";
import { CarePlan } from "../../models/carePlan";

export default class CarePlanService {
  getList = async ({ params }: GetRequestType) => {
    return _axios
      .get<ListResponse<CarePlan>>(`/care-plans`, { params: params })
      .then((res) => res.data);
  };
  getById = async (filter: string) => {
    return _axios
      .get<CarePlan>(`/care-plans/${filter}`)
      .then((res) => res.data);
  };
  post = async ({ body }: PostRequestBase<CarePlan>) => {
    return _axios
      .post<SingleResponse<CarePlan>>(`/care-plans`, body)
      .then((res) => res.data);
  };
  delete = async (id: string) => {
    return _axios
      .delete<SingleResponse<CarePlan>>(`/care-plans/${id}`)
      .then((res) => res.data);
  };
  patch = async ({ id, body }: PatchRequestBase<CarePlan>) => {
    return _axios
      .patch<SingleResponse<CarePlan>>(`/care-plans/${id}`, body)
      .then((res) => res.data);
  };
}
