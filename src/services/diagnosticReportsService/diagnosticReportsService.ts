import { diagnosticReportsModel } from "../../models/diagnosticReports";
import {
  GetRequestType,
  ListResponse,
  PostRequestType,
} from "../../models/shared/basePagedResponse";
import _axios from "../interceptor";

export default class DiagnosticReportsService {
  private readonly baseURL = "/diagnostic-reports";
  getList = ({ params }: GetRequestType) => {
    return _axios
      .get<ListResponse<diagnosticReportsModel>>(this.baseURL, {
        params: params,
      })
      .then((res) => res.data);
  };
  getById = (filter: string) => {
    return _axios
      .get<diagnosticReportsModel>(`${this.baseURL}/${filter}`)
      .then((res) => res.data);
  };
  post = ({ body }: PostRequestType) => {
    return _axios
      .post<diagnosticReportsModel>(this.baseURL, body)
      .then((res) => res.data);
  };
  update = ({ body }: PostRequestType, id: string) => {
    return _axios
      .patch<diagnosticReportsModel>(`${this.baseURL}/${id}`, body)
      .then((res) => res.data);
  };
  delete = (id: string) => {
    return _axios
      .delete<boolean>(`${this.baseURL}/${id}`)
      .then((res) => res.data);
  };
}
