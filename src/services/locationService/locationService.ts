import { locationModel } from "../../models/location";
import {
  GetRequestType,
  ListResponse,
  PostRequestType,
  SingleResponse,
} from "../../models/shared/basePagedResponse";
import _axios from "../interceptor";

export default class LocationService {
  getList = async ({ params }: GetRequestType) => {
    return _axios
      .get<ListResponse<locationModel>>(`/locations`, { params: params })
      .then((response) => response.data);
  };
  getById = async (id: string) => {
    return _axios
      .get<locationModel>(`/locations/${id}`)
      .then((response) => response.data);
  };
  delete = async (id: string) => {
    return _axios.delete(`/locations/${id}`).then((response) => {
      if (response.data === true) return true;
      return false;
    });
  };
  patch = async (item: locationModel) => {
    return _axios
      .patch<locationModel>(`/locations/${item.id}`, item)
      .then((response) => response.data);
  };
  add = async (item: locationModel) => {
    item.alias = undefined;
    return _axios
      .post<locationModel>(`/locations`, item)
      .then((response) => response.data);
  };
}
