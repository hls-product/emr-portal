import _axios from "../interceptor";
import {
  QuestionBody,
  QuestionBodyResponse,
} from "../../models/question/QuestionBody";
import { ListResponse } from "../../models/shared/basePagedResponse";

export default class QuestionnaireService {
  private baseURL = "/questions";
  createQuestionnaire = async (questionBody: QuestionBody) => {
    return _axios
      .post<QuestionBody>(this.baseURL, questionBody)
      .then((res) => res.data);
  };
  getAllQuestionnaire = async (page: number, size: number) => {
    return _axios
      .get<ListResponse<QuestionBodyResponse>>(
        `${this.baseURL}?page=${page}&size=${size}`
      )
      .then((res) => res.data);
  };
  deleteQuestionnaireById = async (id: string) => {
    return _axios
      .delete<boolean>(`${this.baseURL}/${id}`)
      .then((res) => res.data);
  };
  getQuestionnaireById = async (id: string) => {
    return _axios
      .get<QuestionBodyResponse>(`${this.baseURL}/${id}`)
      .then((res) => res.data);
  };
  editQuestionnaire = async (questionBody: QuestionBody, id: string) => {
    return _axios
      .put<QuestionBody>(`${this.baseURL}/${id}`, questionBody)
      .then((res) => res.data);
  };
}
