import _axios from "../interceptor";
import {
  GetRequestType,
  ListResponse,
  PostRequestType,
  SingleResponse,
} from "../../models/shared/basePagedResponse";
import { appointmentModel } from "../../models/appointment";

export default class AppointmentService {
  getList = ({ params }: GetRequestType) => {
    return _axios
      .get<ListResponse<appointmentModel>>(`/appointments`, { params: params })
      .then((res) => res.data);
  };
  getById = (filter: string) => {
    return _axios
      .get<appointmentModel>(`/appointments/${filter}`)
      .then((res) => res.data);
  };
  post = ({ body }: PostRequestType) => {
    return _axios
      .post<appointmentModel>(`/appointments`, body)
      .then((res) => res.data);
  };
  update = ({ body }: PostRequestType, id: string) => {
    return _axios
      .patch<appointmentModel>(`/appointments/${id}`, body)
      .then((res) => res.data);
  };
  delete = async (id: string) => {
    return _axios
      .delete<boolean>(`/appointments/${id}`)
      .then((res) => res.data);
  };
}
