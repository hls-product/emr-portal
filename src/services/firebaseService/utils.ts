import {
  DocumentData,
  QueryDocumentSnapshot,
  Timestamp,
} from "firebase/firestore";

export const parseJwt = (token:string) =>  {
  try {
    return JSON.parse(atob(token.split('.')[1]));
  } catch (e) {
    return null;
  }
};

export const convertFirestoreTimestampToString = (timeStamp: Timestamp) =>
timeStamp.toDate().getTime()
// .toLocaleString();

export const transformMessage = (message: QueryDocumentSnapshot<DocumentData>) => ({
  id: message.id,
  ...message.data(), //spread data
  timestamp: message.data()['timestamp']
    ? convertFirestoreTimestampToString(message.data()['timestamp'] as Timestamp)
    : null
});
export const transformConversation = (convo: QueryDocumentSnapshot<DocumentData>) => ({
  id: convo.id,
  ...convo.data(),
  lastActive: convo.data()['lastActive']
    ? convertFirestoreTimestampToString(convo.data()['lastActive'] as Timestamp)
    : null,
});

const username = () => localStorage.getItem('access_token')? parseJwt(localStorage.getItem("access_token") as string).preferred_username : '';