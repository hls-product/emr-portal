import {parseJwt} from './utils'
import { collection, doc, getDoc, query, serverTimestamp, setDoc } from 'firebase/firestore';
import {db} from './config'

export default class UserActiveService {
     //get current user information from localstorage
  username = () =>
  localStorage.getItem("access_token")
    ? parseJwt(localStorage.getItem("access_token") as string)
        .preferred_username
    : "";
userId = () =>
  localStorage.getItem("access_token")
    ? parseJwt(localStorage.getItem("access_token") as string).sub
    : "";
name = () =>
  localStorage.getItem("access_token")
    ? parseJwt(localStorage.getItem("access_token") as string).name
    : "";
userRef = () => doc(db, "users", this.userId());

    setUserInDb = async () => {
        try {
            await setDoc(
              doc(db, 'users', this.userId()),
              {
                username: this.username(),
                display: this.name(),
                lastActive: serverTimestamp(),
                photoURL: '../assets/avatar.svg'
              },
              {merge: true} // ONLY UPDATE CHANGES
            )
          } catch (error) {
            console.error("EROR SETTING USER INFO IN DB FIRESTORE")
          }
    }
    getUserInfoSnapshot = async (id: string) => 
    (
      await getDoc(
        doc(db, 'users', id)
      )
    ).data();
}