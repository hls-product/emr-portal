import { parseJwt, transformConversation, transformMessage } from "./utils";
import {
  addDoc,
  arrayUnion,
  collection,
  doc,
  DocumentData,
  DocumentReference,
  Firestore,
  getDoc,
  getDocs,
  limit,
  orderBy,
  query,
  QueryDocumentSnapshot,
  refEqual,
  serverTimestamp,
  setDoc,
  Timestamp,
  updateDoc,
  where,
} from "firebase/firestore";
import {
  getDownloadURL,
  getStorage,
  ref,
  StorageReference,
  uploadBytes,
  uploadString,
} from "firebase/storage";
import { useCollection } from "react-firebase-hooks/firestore";
import { db, storage } from "./config";
import { type } from "os";

export type ContentType = "chat" | "attach" | "call" | "rating";

export default class ChatService {
  //get current user information from localstorage
  username = () =>
    localStorage.getItem("access_token")
      ? parseJwt(localStorage.getItem("access_token") as string)
          .preferred_username
      : "";
  userId = () =>
    localStorage.getItem("access_token")
      ? parseJwt(localStorage.getItem("access_token") as string).sub
      : "";
  name = () =>
    localStorage.getItem("access_token")
      ? parseJwt(localStorage.getItem("access_token") as string).name
      : "";
  userRef = () => doc(db, "users", this.userId());

  //get n conversations of current user
  generateQueryGetConversation = () => {
    return query(
      collection(db, "conversations"),
      // orderBy("lastActive", "desc"),
      where("usersRef", "array-contains-any", [this.userRef()])
      // limit(25)
    );
  };

  //get messages from a conversation
  generateQueryGetMessages = (convo_id: string) =>
    query(
      collection(db, `conversations/${convo_id}/messages`),
      orderBy("timestamp", "asc"),
      limit(30)
    );

  //send message
  sendNewMessages = async (
    content: string,
    convo_id: string,
    contentType: ContentType
  ) => {
    await setDoc(
      doc(db, "conversations", convo_id),
      {
        lastActive: serverTimestamp(),
        lastContent: contentType === "chat" ? content : "Attachment",
        lastContentType: contentType,
        lastUser: this.userRef(),
        usersSeen: [this.userId()],
      },
      { merge: true }
    );
    await addDoc(collection(db, `conversations/${convo_id}/messages`), {
      content: content,
      timestamp: serverTimestamp(),
      userSent: this.userRef(),
      contentType: contentType,
    });
    await setDoc(
      doc(db, "users", this.userId()),
      {
        username: this.username(),
        display: this.name(),
        lastActive: serverTimestamp(),
        photoURL: "../assets/avatar.svg",
      },
      { merge: true }
    );
  };

  //send attachments

  sendNewAttachment = (file: File, convoId: string) => {
    const storageRef = ref(storage, file.name);
    uploadBytes(storageRef, file).then((snapshot) =>
      this.sendNewMessages(snapshot.ref.toString(), convoId, "attach")
    );
  };

  getAttachment = async (content: string) => {
    return await getDownloadURL(ref(storage, content));
  };

  //set seen message

  setSeenUsers = async (convo_id: string) => {
    await updateDoc(doc(db, "conversations", convo_id), {
      usersSeen: arrayUnion(this.userId()),
    });
  };

  //set user lastActive
  setLastActive = async () => {
    await setDoc(
      doc(db, "users", this.userId()),
      {
        lastActive: serverTimestamp(),
      },
      { merge: true }
    );
  };

  //get own info
  getOwnInfo = async () => {
    const res = await getDoc(this.userRef());
    return res.data();
  };

  getConversationForSelectedUser = async (ids: string[]) => {
    const usersRef = ids.map((item) => doc(db, "users", item));
    const converRes = await getDocs(
      query(
        collection(db, "conversations"),
        where("usersRef", "array-contains", this.userRef())
      )
    );
    const conversations = await converRes.docs.filter((item) => {
      const arr1 = item.data().usersRef as DocumentReference[];
      const arr2 = [...usersRef, this.userRef()];
      if (arr1.length === arr2.length)
        return arr2.every((x) => arr1.some((y) => refEqual(x, y)));
      else return false;
    });
    return conversations;
  };

  createNewConversation = async (ids: string[], displays?: string[]) => {
    //set user in db
    try {
      const userRes = await getDocs(
        query(collection(db, "users"), where("username", "in", ids))
      );
      const setUserPromises = await ids.map((item, index) => {
        if (
          !userRes.docs.some((x) =>
            refEqual(x.ref, doc(db, "users", item))
          )
        ) {
          setDoc(
            doc(db, "users", item),
            {
              username: item,
              display: displays? displays[index] : item,
            },
            { merge: true } // ONLY UPDATE CHANGES
          );
        }
      });
      await Promise.all(setUserPromises);
    } catch (error) {
      console.error("EROR SETTING USER INFO IN DB FIRESTORE");
    }
    await addDoc(collection(db, "conversations"), {
      display: "",
      usersRef: [...ids.map((item) => doc(db, "users", item)), this.userRef()],
      lastActive: serverTimestamp()
    });
  };
}
