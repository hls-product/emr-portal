// Import the functions you need from the SDKs you need
import { getApps, getApp, initializeApp } from "firebase/app";
import {getFirestore} from 'firebase/firestore'
import {getAuth} from 'firebase/auth'
import { getStorage } from "firebase/storage";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries



// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyBt_BO2dYIFmlSw2aYghPOF6njP-G7vWQw",
    authDomain: "update-chat-app.firebaseapp.com",
    projectId: "update-chat-app",
    storageBucket: "update-chat-app.appspot.com",
    messagingSenderId: "168149912280",
    appId: "1:168149912280:web:f70da66a45be136ec365ad",
    measurementId: "G-RRKJSQNVFD"
};



// Initialize Firebase
const app = getApps().length ? getApp() : initializeApp(firebaseConfig);

const db = getFirestore(app)

const auth = getAuth(app)

const storage = getStorage(app)

export {db, auth, storage}