import _axios from "../interceptor";
import {
  GetRequestType,
  ListResponse,
  PostRequestType,
  SingleResponse,
} from "../../models/shared/basePagedResponse";
import { immunizationModel } from "../../models/immunization";

export default class ImmunizationService {
  getList = ({ params }: GetRequestType) => {
    return _axios
      .get<ListResponse<immunizationModel>>(`/immunizations`, { params: params })
      .then((res) => res.data);
  };
  getById = (filter: string) => {
    return _axios
      .get<immunizationModel>(`/immunizations/${filter}`)
      .then((res) => res.data);
  };
  post = ({ body }: PostRequestType) => {
    return _axios
      .post<immunizationModel>(`/immunizations`, body)
      .then((res) => res.data);
  };
  update = ({ body }: PostRequestType, id: string) => {
    return _axios
      .patch<immunizationModel>(`/immunizations/${id}`, body)
      .then((res) => res.data);
  };
  delete = (id: string) => {
    return _axios
      .delete<boolean>(`/immunizations/${id}`)
      .then((res) => res.data);
  };
}
