import _axios from "../interceptor";
import {
  GetRequestType,
  ListResponse,
  PostRequestType,
  SingleResponse,
} from "../../models/shared/basePagedResponse";
import { claimsModel } from "../../models/claims";

export default class ClaimService {
  private readonly baseURL = "/claims";
  getList = ({ params }: GetRequestType) => {
    return _axios
      .get<ListResponse<claimsModel>>(this.baseURL, { params: params })
      .then((res) => res.data);
  };
  getById = (filter: string) => {
    return _axios
      .get<claimsModel>(`${this.baseURL}/${filter}`)
      .then((res) => res.data);
  };
  post = ({ body }: PostRequestType) => {
    return _axios
      .post<claimsModel>(this.baseURL, body)
      .then((res) => res.data);
  };
  update = ({ body }: PostRequestType, id: string) => {
    return _axios
      .patch<claimsModel>(`${this.baseURL}/${id}`, body)
      .then((res) => res.data);
  };
  delete = (id: string) => {
    return _axios.delete<boolean>(`${this.baseURL}/${id}`).then((res) => res.data);
  };
}
