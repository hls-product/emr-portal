import _axios from "../interceptor";
import { ListResponse } from "../../models/shared/basePagedResponse";
import { Medicine } from "../../store/reducers/medicine/medicineSlice";

export default class MedicationService {
  private baseURL = "/medication";
  getAll = async (page: number, size: number) => {
    return _axios
      .post<ListResponse<Medicine>>(`${this.baseURL}?page=${page}&size=${size}`)
      .then((res) => res.data);
  };
}
