import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';

const createConfig = () => {
  const options: AxiosRequestConfig = {
    baseURL: 'https://fhir-service.herokuapp.com/api',
  };

  return options;
};

const config = createConfig();

const _axios: AxiosInstance = axios.create(config);

_axios.interceptors.request.use(
  (config: any) => {
    if (window.localStorage.getItem('access_token')) {
      config.headers.Authorization =
        `Bearer ${window.localStorage.getItem('access_token')}` || ``;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

_axios.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    if(axios.isCancel(error)){
        console.log('Request is canceled', error.message);
    }
    if(error?.response?.status === 401){
        localStorage.removeItem('access_token');
        // eventBus.dispatch('logout')
    }
    if(error?.response.status !== 401){
    }

    return Promise.reject(error);
  }
);

export default _axios;
