import {
    PatchRequestBase,
    PostRequestBase,
    GetRequestType,
  } from "./../../models/shared/basePagedResponse";
  import _axios from "../interceptor";
  import {
    ListResponse,
    SingleResponse,
  } from "../../models/shared/basePagedResponse";
  import { flagModel } from "../../models/flag";
  
  export default class FlagService {
    getList = async ({ params }: GetRequestType) => {
      return _axios
        .get<ListResponse<flagModel>>(`/flags`, { params: params })
        .then((res) => res.data);
    };
    getById = async (filter: string) => {
      return _axios
        .get<flagModel>(`/flags/${filter}`)
        .then((res) => res.data);
    };
    post = async ({ body }: PostRequestBase<flagModel>) => {
      return _axios
        .post<SingleResponse<flagModel>>(`/flags`, body)
        .then((res) => res.data);
    };
    delete = async (id: string) => {
      return _axios
        .delete<SingleResponse<flagModel>>(`/flags/${id}`)
        .then((res) => res.data);
    };
    patch = async ({ id, body }: PatchRequestBase<flagModel>) => {
      return _axios
        .patch<SingleResponse<flagModel>>(`/flags/${id}`, body)
        .then((res) => res.data);
    };
  }
  