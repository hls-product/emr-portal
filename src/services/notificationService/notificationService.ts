import {
  GetRequestType,
  ListResponse,
} from "../../models/shared/basePagedResponse";
import _axios from "../interceptor";

interface NotificationTemplates {
  id?: string;
  category?: string;
  channel?: string;
  value?: string;
  description?: string;
}
export default class NotificationTemplateService {

  private url = process.env.REACT_APP_NOTIFICATION_URL+"/notification-templates";
  getList = async ({ params }: GetRequestType) => {
    return _axios
      .get<ListResponse<NotificationTemplates>>(this.url, {
        params: params,
      })
      .then((response) => response.data);
  };
  getById = async (id: string) => {
    return _axios
      .get<NotificationTemplates>(`${this.url}/${id}`)
      .then((response) => response.data);
  };
  delete = async (id: string) => {
    return _axios.delete(`${this.url}/${id}`).then((response) => {
    });
  };
  patch = async (item: NotificationTemplates) => {
    return _axios
      .patch<NotificationTemplates>(`${this.url}/${item.id}`, item)
      .then((response) => response.data);
  };
  add = async (item: NotificationTemplates) => {
    return _axios
      .post<NotificationTemplates>(this.url, item)
      .then((response) => response.data);
  };
}
