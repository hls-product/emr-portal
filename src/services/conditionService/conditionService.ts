import { ConditionModel } from "../../models/conditions";
import {
  GetRequestType,
  ListResponse,
  PostRequestType,
} from "../../models/shared/basePagedResponse";
import _axios from "../interceptor";

export default class ConditionService {
  private readonly baseURL = `/conditions`;
  getList = ({ params }: GetRequestType) => {
    return _axios
      .get<ListResponse<ConditionModel>>(this.baseURL, { params: params })
      .then((res) => res.data);
  };
  getById = (filter: string) => {
    return _axios
      .get<ConditionModel>(`${this.baseURL}/${filter}`)
      .then((res) => res.data);
  };
  post = ({ body }: PostRequestType) => {
    return _axios
      .post<ConditionModel>(this.baseURL, body)
      .then((res) => res.data);
  };
  update = ({ body }: PostRequestType, id: string) => {
    return _axios
      .patch<ConditionModel>(`${this.baseURL}/${id}`, body)
      .then((res) => res.data);
  };
  delete = (id: string) => {
    return _axios
      .delete<ConditionModel>(`${this.baseURL}/${id}`)
      .then((res) => res.data);
  };
}
