import _axios from "../interceptor";
import {GetRequestType, ListResponse, PostRequestType, SingleResponse} from "../../models/shared/basePagedResponse"
import { EncounterModel } from "../../models/encounter";


export default class EncounterService {
    private readonly baseURL = `/encounters`;
    getList = ({params}: GetRequestType) => {
        return _axios
            .get<ListResponse<EncounterModel>>(this.baseURL,{params: params})
            .then(res =>res.data)
    };
    getById = (filter: string) => {
        return _axios
            .get<EncounterModel>(`${this.baseURL}/${filter}`)
            .then(res => res.data)
    };
    post = ({body}: PostRequestType) => {
        return _axios
            .post<EncounterModel>(this.baseURL, body)
            .then(res => res.data)
    };
    update = ({body}: PostRequestType, id: string) => {
        return _axios
            .patch<EncounterModel>(`${this.baseURL}/${id}`, body)
            .then(res => res.data)
    };
    delete = (id: string) => {
        return _axios   
            .delete<boolean>(`${this.baseURL}/${id}`)
            .then (res => res.data)
    }
}
