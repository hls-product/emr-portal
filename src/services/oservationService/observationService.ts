import {
  PatchRequestBase,
  PostRequestBase,
  GetRequestBase,
} from "./../../models/shared/basePagedResponse";
import {
  ListResponse,
  SingleResponse,
} from "../../models/shared/basePagedResponse";
import { observationModel } from "../../models/observation";
import { ObservationGetRequest } from "../../models/apiRequest/observation";
import _axios from "../interceptor";

export default class ObservationService {
  getList = async ({ params }: GetRequestBase<ObservationGetRequest>) => {
    return _axios
      .get<ListResponse<observationModel>>("/observations", { params: params })
      .then((res) => res.data);
  };

  getById = async (filter: string) => {
    return _axios
      .get<SingleResponse<observationModel>>(`/observations/${filter}`)
      .then((res) => res.data);
  };

  post = async ({ body }: PostRequestBase<observationModel>) => {
    return _axios
      .post<SingleResponse<observationModel>>(`/observations`, body)
      .then((res) => res.data);
  };
  delete = async (id: string) => {
    return _axios
      .delete<SingleResponse<observationModel>>(`/observations/${id}`)
      .then((res) => res.data);
  };
  patch = async ({ id, body }: PatchRequestBase<observationModel>) => {
    return _axios
      .patch<SingleResponse<observationModel>>(`/observations/${id}`, body)
      .then((res) => res.data);
  };
}
