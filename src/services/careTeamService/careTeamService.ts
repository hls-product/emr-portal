import {
  PatchRequestBase,
  PostRequestBase,
  GetRequestType,
} from "./../../models/shared/basePagedResponse";
import _axios from "../interceptor";
import {
  ListResponse,
  SingleResponse,
} from "../../models/shared/basePagedResponse";
import { CareTeam } from "../../models/careTeam";

export default class CareTeamService {
  getList = async ({ params }: GetRequestType) => {
    return _axios
      .get<ListResponse<CareTeam>>(`/care-teams`, { params: params })
      .then((res) => res.data);
  };
  getById = async (filter: string) => {
    return _axios
      .get<CareTeam>(`/care-teams/${filter}`)
      .then((res) => res.data);
  };
  post = async ({ body }: PostRequestBase<CareTeam>) => {
    return _axios
      .post<SingleResponse<CareTeam>>(`/care-teams`, body)
      .then((res) => res.data);
  };
  delete = async (id: string) => {
    return _axios
      .delete<SingleResponse<CareTeam>>(`/care-teams/${id}`)
      .then((res) => res.data);
  };
  patch = async ({ id, body }: PatchRequestBase<CareTeam>) => {
    return _axios
      .patch<SingleResponse<CareTeam>>(`/care-teams/${id}`, body)
      .then((res) => res.data);
  };
}
