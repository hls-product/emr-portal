import _axios from "../interceptor";
import {
  GetRequestType,
  ListResponse,
  PostRequestType,
  SingleResponse,
} from "../../models/shared/basePagedResponse";
import { relatedPatientModel } from "./../../models/relatedPatient";

export default class RelatedPatientService {
  private baseURL = "/relatedPersons";
  getList = async ({ params }: GetRequestType) => {
    return _axios
      .get<ListResponse<relatedPatientModel>>(this.baseURL, {
        params: params,
      })
      .then((res) => res.data);
  };
  getById = async (filter: string) => {
    return _axios
      .get<relatedPatientModel>(`${this.baseURL}/${filter}`)
      .then((res) => res.data);
  };
  post = async ({ body }: PostRequestType) => {
    return _axios
      .post<relatedPatientModel>(this.baseURL, body)
      .then((res) => res.data);
  };
  update = async ({ body }: PostRequestType, id: string) => {
    return _axios
      .patch<relatedPatientModel>(`${this.baseURL}/${id}`, body)
      .then((res) => res.data);
  };
  delete = async (id: string) => {
    return _axios
      .delete<relatedPatientModel>(`${this.baseURL}/${id}`)
      .then((res) => res.data);
  };
}
