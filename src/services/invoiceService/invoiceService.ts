import _axios from "../interceptor";
import {
  GetRequestType,
  ListResponse,
  PostRequestType,
  SingleResponse,
} from "../../models/shared/basePagedResponse";
import { invoiceModel } from "../../models/invoices";

export default class InvoiceService {
  getList = ({ params }: GetRequestType) => {
    return _axios
      .get<ListResponse<invoiceModel>>(`/invoices`, { params: params })
      .then((res) => res.data);
  };
  getById = (filter: string) => {
    return _axios
      .get<invoiceModel>(`/invoices/${filter}`)
      .then((res) => res.data);
  };
  post = ({ body }: PostRequestType) => {
    return _axios
      .post<SingleResponse<invoiceModel>>(`/invoices`, body)
      .then((res) => res.data);
  };
  update = ({ body }: PostRequestType, id: string) => {
    return _axios
      .patch<SingleResponse<invoiceModel>>(`/invoices/${id}`, body)
      .then((res) => res.data);
  };
  delete = (id: string) => {
    return _axios.delete<boolean>(`/invoices/${id}`).then((res) => res.data);
  };
}
