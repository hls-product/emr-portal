/* Libs */
import React from "react";
import { Routes, Route, Navigate, useNavigate } from "react-router-dom";
/*Components */
import Loading from "./components/Loading";
import ProtectedRoute from "./components/ProtectedRoute";
import { Login, ForgetPassword } from "./pages/Auth";
// import Verify from "./pages/Verify";
// import NotFound from "./pages/NotFound";
import { AdminPortal } from "./pages/Admin";
// import {Profile, ProfileDocs, StaffAttendance} from './pages/HumanResources'
import {
  MedicineStockProfile,
  MedicinesStock,
  EditMedicine,
} from "./pages/PharmacyManagement";
import { MedicalRecord } from "./pages/Patient";
import { useSelector } from "react-redux";
import { IRootState } from "./store/reducers";

import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

/* Styles */
import "./App.css";
import { QuestionDashboard } from "./pages/Question";
import { Profile } from "./pages/HumanResources";
import { Notification } from "./hooks/notifications";
const App: React.FC = () => {
  Notification();
  const isLoading = useSelector(
    (state: IRootState) => state.authReducer.loading
  );

  if (isLoading) return <Loading />;

  return (
    <Routes>
      <Route path="/" element={<Login />} />
      <Route path="/login" element={<Login />} />
      <Route path="/forget/*" element={<ForgetPassword />} />
      <Route
        path="/admin/*"
        element={
          <ProtectedRoute>
            <AdminPortal />
          </ProtectedRoute>
        }
      />
      {/* <Route path="/profile" element={<Profile />} /> */}
      {/* <Route path="/profile/docs" element={<ProfileDocs />} />
        <Route path="/profile/staff" element={<StaffAttendance />} />
        <Route path="/medicine" element={<MedicinesStock />} />
        <Route path="/medicine/:id" element={<MedicineStockProfile />} />
        <Route path="/medicine/:id/edit" element={<EditMedicine />} />
        <Route path="/medical/card" element={ <MedicalRecord />} /> */}

      {/* <Route path="/verify" element={<Verify />} />
      <Route path="/404" element={<NotFound />} />
      <Route path="*" element={<Navigate to="/404" />} /> */}
    </Routes>
  );
};

export default App;
