import React, { useEffect } from "react";
import Swal from "sweetalert2";
import { useSelector } from "react-redux";
import { IRootState } from "../../store/reducers";
import { useAppDispatch } from "../../store/Store";
import { removeNotification } from "../../store/reducers/notifications/notificationSlice";

export const Notification = () => {
  const dispatch = useAppDispatch();
  const { message } = useSelector(
    (state: IRootState) => state.notificationReducer
  );

  useEffect(() => {
    if (message.length !== 0) {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: message,
      }).then((result) => {
        if (result.isConfirmed) {
          dispatch(
            removeNotification({
              message: "",
            })
          );
        }
      });
    }
  });
};
