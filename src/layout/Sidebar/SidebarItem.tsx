import React, { useEffect } from "react";
import { ListItemButton, Typography } from "@mui/material";
import { Link, useLocation } from "react-router-dom";
import { routes, RouteType } from "../../PortalRoutes";
import { SubRouteSidebarItem } from "./SubRouteSidebarItem";

import open_sidebar from "../../assets/sidebar/open_sidebar.svg";
import open_sidebar_active from "../../assets/sidebar/open_sidebar_active.svg";
import close_sidebar_active from "../../assets/sidebar/close_sidebar_active.svg";

interface DashboardItem {
  title: string;
  icon: string;
  activeIcon: string;
  isActive: boolean;
  to?: string;
  openStatus: boolean;
  itemIndex: number;
  subRoute?: RouteType[];
  open: boolean;
  handleInteractSubRoute: (status: boolean) => void;
}

export const SidebarItem: React.FC<DashboardItem> = ({
  icon,
  title,
  isActive,
  to,
  activeIcon,
  openStatus,
  itemIndex,
  subRoute,
  handleInteractSubRoute,
  open,
}) => {
  const [isAtSubRoute, setIsAtSubRoute] = React.useState<boolean>(false);
  const location = useLocation();

  const SetAtSubRoute = () => {
    setIsAtSubRoute(true);
    handleInteractSubRoute(true);
  };

  return (
    <div
      style={{
        position: "relative",
      }}
    >
      <Link
        to={`/admin/` + to}
        style={{
          minWidth: `${openStatus ? "217px" : "56px"}`,
          height: "56px",
          background: `${isActive ? "#6C5DD3" : "#fafbff"}`,
          borderRadius: "12px",
          marginBottom: `${
            openStatus
              ? itemIndex === routes.length - 1
                ? "175px"
                : open
                ? subRoute
                  ? isActive
                    ? `${
                        ((subRoute as unknown as RouteType[]).length + 1) * 36
                      }px`
                    : "12px"
                  : "12px"
                : "12px"
              : "12px"
          }`,
          padding: `18px ${openStatus ? "12px" : "18px"} 18px 18px`,
          display: "flex",
          position: "relative",
          transition: "minWidth 0.5s linear",
          textDecoration: "none",
          alignItems: "center",
          justifyContent: "space-between",
        }}
        onClick={() => {
          if (!open && !isActive && subRoute) {
            handleInteractSubRoute(true);
          } else if (open && isActive && subRoute) {
            handleInteractSubRoute(!open);
          } else if (open && !isActive && subRoute) {
            handleInteractSubRoute(true);
          } else if (!open && isActive && subRoute) {
            handleInteractSubRoute(!open);
          } else {
            handleInteractSubRoute(false);
          }
        }}
      >
        <div
          style={{
            position: "absolute",
            background: "#6C5DD3",
            width: "56px",
            height: "56px",
            left: "-70px",
            top: 0,
            borderRadius: "0 12px 12px 0",
            display: `${isActive ? "block" : "none"}`,
          }}
        ></div>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <img src={isActive ? activeIcon : icon} width={20} height={20} />
          <Typography
            variant="body2"
            component="div"
            sx={{
              marginLeft: "15px",
              fontSize: "14px",
              fontWeight: "600",
              paddingTop: "2px !important",
              color: `${isActive ? "#FFFFFF" : "#8f95b2"}`,
            }}
            display={openStatus ? "inline" : "none"}
          >
            {title}
          </Typography>
        </div>
        {openStatus && subRoute ? (
          <img
            src={
              isActive
                ? open
                  ? open_sidebar_active
                  : close_sidebar_active
                : open_sidebar
            }
            width={isActive ? (open ? 12 : 7.5) : 7.5}
            height={isActive ? (open ? 7.5 : 12) : 12}
          />
        ) : (
          ""
        )}
      </Link>
      {openStatus && subRoute && open && isActive
        ? subRoute.map((route, index) => {
            const isActive =
              location.pathname === "/admin/" + route.path ? true : false;
            let bottom = 42 + index * 40;
            return route.hideInMenu ? null : (
              <SubRouteSidebarItem
                subRoute={route}
                key={route.name}
                bottom={bottom}
                open={open}
                isActive={isActive}
                to={to}
                SetAtSubRoute={SetAtSubRoute}
                openStatus={openStatus}
              />
            );
          })
        : ""}
    </div>
  );
};
