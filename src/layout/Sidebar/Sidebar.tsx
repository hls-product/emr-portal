import React, { useEffect } from "react";
import { Drawer, List, Typography } from "@mui/material";
import { useLocation } from "react-router-dom";
import { routes, RouteType } from "../../PortalRoutes";

import { SidebarItem } from "./SidebarItem";
import { useKeycloak } from "@react-keycloak/web";
import { logout } from "../../store/reducers/authentication/authSlice";
import { useAppDispatch } from "../../store/Store";

import logoIcon from "../../assets/sidebar/logo.svg";
import LogoutIcon from "@mui/icons-material/Logout";
import doctor from "../../assets/image/doctor.svg";

interface Props {
  openStatus: boolean;
}

const RenderUserCard = ({ openStatus }: Props) => {
  const dispatch = useAppDispatch();
  const { initialized, keycloak } = useKeycloak();

  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        padding: `23px`,
      }}
    >
      <img src={doctor} width={42} height={42} />
      <Typography
        variant="body2"
        component="div"
        sx={{ marginLeft: "10px" }}
        display={openStatus ? "block" : "none"}
      >
        Dr. Strange
      </Typography>
      <LogoutIcon
        sx={{
          marginLeft: "auto",
          display: `${openStatus ? "block" : "none"}`,
          width: "15px",
          color: "#9FA0A6",
          cursor: "pointer",
        }}
        onClick={() => {
          keycloak.logout();
          localStorage.removeItem("access_token");
          dispatch(logout);
        }}
      />
    </div>
  );
};

export const Sidebar: React.FC<Props> = ({ openStatus }) => {
  const location = useLocation();
  const [open, setOpen] = React.useState<boolean>(false);

  const handleInteractSubRoute = (status: boolean) => {
    setOpen(status);
  };

  return (
    <Drawer
      sx={{
        width: `${openStatus ? "257px" : "97px"}`,
        transition: "width 0.5s linear",
        flexShrink: 0,
        height: "100%",
        "& .MuiDrawer-paper": {
          width: `${openStatus ? "257px" : "97px"}`,
          transition: "width 0.5s linear",
          boxSizing: "border-box",
          // borderRight: "1px solid rgb(234, 234, 234) !important",
          overflow: "hidden",
        },
      }}
      variant="permanent"
      anchor="left"
    >
      <div
        style={{
          display: "flex",
          alignItems: "center",
          padding: `23px`,
          // borderBottom: "1px solid #EAEAEA",
        }}
      >
        <img src={logoIcon} width={50} height={50} />
        <Typography
          variant="h5"
          component="div"
          sx={{ color: "#6C5DD3", marginLeft: "10px" }}
          display={openStatus ? "block" : "none"}
        >
          Akamental
        </Typography>
      </div>
      <List
        sx={{
          padding: "20px 20px 0",
          overflowY: "scroll",
          flex: 1,
          msOverflowStyle: "none",
          scrollbarWidth: "none",
          "&::-webkit-scrollbar": {
            display: "none",
          },
        }}
      >
        {routes.map((item, index) => {
          const isActive =
            location.pathname.indexOf("/admin/" + item.path) !== -1;
          return item.hideInMenu ? null : (
            <SidebarItem
              icon={item.icon as string}
              title={item.title}
              isActive={isActive}
              key={item.title}
              to={item.path as unknown as string}
              activeIcon={item.activeIcon as string}
              openStatus={openStatus}
              itemIndex={index}
              subRoute={item.subRoutes as unknown as RouteType[]}
              open={open}
              handleInteractSubRoute={handleInteractSubRoute}
            />
          );
        })}
      </List>
      <RenderUserCard openStatus={openStatus} />
    </Drawer>
  );
};
