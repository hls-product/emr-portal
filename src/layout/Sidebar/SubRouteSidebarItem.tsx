import { Typography } from "@mui/material";
import React from "react";
import { Link, useLocation } from "react-router-dom";
import { RouteType } from "../../PortalRoutes";

interface Props {
  subRoute: RouteType;
  bottom: number;
  open: boolean;
  isActive: boolean;
  to?: string;
  SetAtSubRoute: () => void;
  openStatus: boolean;
}

export const SubRouteSidebarItem: React.FC<Props> = ({
  subRoute,
  bottom,
  open,
  isActive,
  to,
  SetAtSubRoute,
  openStatus,
}) => {
  return (
    <Link
      to={`/admin/` + subRoute.path}
      style={{
        position: "absolute",
        bottom: `${openStatus ? `-${bottom}px` : `0`}`,
        left: "54px",
        display: `${open ? "block" : "none"}`,
        textDecoration: "none",
      }}
      onClick={() => {
        SetAtSubRoute();
      }}
    >
      <Typography
        variant="body2"
        component="div"
        sx={{
          fontSize: "14px",
          fontWeight: "500",
          color: `${isActive ? "#4E4E4E" : "#9FA0A6"}`,
        }}
      >
        {subRoute.title}
      </Typography>
    </Link>
  );
};
