import React from 'react'

export const ForgetPasswordContent: React.FC = ({children}) => {
  return (
    <div>{children}</div>
  )
}
