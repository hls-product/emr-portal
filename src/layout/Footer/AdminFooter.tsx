import React from 'react';
import {Button} from '@mui/material';

interface Props {
    openStatus: boolean;
}

export const AdminFooter: React.FC<Props> = ({openStatus}) => {
  return (
    <div style={{
        marginLeft: `${openStatus ? '256px' : '96px'}`,
        transition: 'margin-left 0.5s linear',
        padding: "21px",
        borderTop: '1px solid #EAEAEA',
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center'   
    }}>
        <Button sx={{width: '173px', height: '40px', borderRadius: "99px"}} variant='contained'>Save General Setting</Button>
    </div>
  )
}