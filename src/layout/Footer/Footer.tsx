import React from 'react';
import { Button, Typography, Grid, Container, Box } from '@mui/material';
import { Logo } from '../../ui-components/Logo';

import mailIcon from '../../assets/icon/mail.svg';
import navigateIcon from '../../assets/icon/black_navigate.svg';
import telephoneIcon from '../../assets/icon/black_phone.svg';
import fbIcon from '../../assets/icon/Facebook.svg';
import instaIcon from '../../assets/icon/Instagram.svg';
import dribbleIcon from '../../assets/icon/Dribbble.svg';
import linkedinIcon from '../../assets/icon/LinkedIn.svg';
import twitterIcon from '../../assets/icon/Twitter.svg';

type informationType = {
  id: number;
  label: string;
  icon: string;
};

type linkType = {
  id: number;
  label: string;
  url: string;
};

type iconType = {
  id: number;
  icon: string;
  width: number;
  height: number;
};

type columnLinkType = {
  id: number;
  label: string;
  listLink: linkType[];
};

const listColumnLink: columnLinkType[] = [
  {
    id: 0,
    label: 'The Company',
    listLink: [
      { id: 0, label: 'About Us', url: '' },
      { id: 1, label: 'Supplement Store', url: '' },
      { id: 2, label: 'Careers', url: '' },
      { id: 3, label: 'News & Press', url: '' },
      { id: 4, label: 'For Employers', url: '' },
      { id: 5, label: 'Provider Referrals', url: '' },
    ],
  },
  {
    id: 1,
    label: 'Blog',
    listLink: [
      { id: 0, label: 'Health Concerns', url: '' },
      { id: 1, label: 'Recipes & Nutrition', url: '' },
      { id: 2, label: 'Optimizations', url: '' },
      { id: 3, label: 'News', url: '' },
      { id: 4, label: 'Guides', url: '' },
    ],
  },
  {
    id: 3,
    label: 'Getting Started',
    listLink: [
      { id: 0, label: 'FAQ', url: '' },
      { id: 1, label: 'Talk to Membership Advisor', url: '' },
      { id: 2, label: 'Refer', url: '' },
      { id: 3, label: 'Promotions & Others', url: '' },
    ],
  },
];

const listInformation: informationType[] = [
  {
    id: 0,
    label: '1 Wallich St Singapore',
    icon: navigateIcon,
  },
  {
    id: 1,
    label: '(406) 555-0120',
    icon: telephoneIcon,
  },
  {
    id: 2,
    label: 'nevaeh.simmons@example.com',
    icon: mailIcon,
  },
];

const listIcon: iconType[] = [
  {
    id: 0,
    icon: fbIcon,
    width: 14,
    height: 14,
  },
  {
    id: 1,
    icon: instaIcon,
    width: 12,
    height: 12,
  },
  {
    id: 2,
    icon: dribbleIcon,
    width: 12,
    height: 12,
  },
  {
    id: 3,
    icon: linkedinIcon,
    width: 12,
    height: 12,
  },
  {
    id: 4,
    icon: twitterIcon,
    width: 12,
    height: 12,
  },
];

const RenderInformation = () => {
  return (
    <div style={{
        display: "flex",
        flexDirection: "column",
        justifyContent: 'center',
        alignItems: 'flex-start'
    }}>
      <Typography
        variant="body2"
        component="div"
        sx={{
          fontSize: '14px',
          fontWeight: '500',
          color: '#4E4E4E',
          marginBottom: '44px',
        }}
      >
        Now you can build fast your awesome website with these ready-to-use
        blocks, elements and sections.
      </Typography>

      {listInformation.map((info) => (
        <div
        style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'flex-start',
            marginBottom: "12px",
        }}
        key={info.id}
        >
          <img src={info.icon} width={12} height={12} />
          <Typography
            variant="body2"
            component="div"
            sx={{
              marginLeft: '12px',
              fontSize: '14px',
              fontWeight: '500',
              color: ' #4E4E4E',
              marginTop: '-2px',
            }}
          >
            {info.label}
          </Typography>
        </div>
      ))}
    </div>
  );
};

const RenderLink = () => {
  return (
    <Grid container spacing={8}>
      {listColumnLink.map((item) => (
        <Grid item xs="auto" key={item.id}>
          <div
        style={{
            display: 'flex',
            alignItems: 'flex-start',
            justifyContent: 'center',
            flexDirection: 'column',
        }}
           >
            <Typography
              variant="h6"
              component="div"
              sx={{
                marginBottom: '10px',
                fontSize: '14px',
                fontWeight: '600',
                color: '#131626',
              }}
            >
              {item.label}
            </Typography>
            {item.listLink.map((link) => (
              <a
                href={`${link.url}`}
                style={{
                  marginBottom: '8px',
                  color: '#4E4E4E',
                  fontWeight: '500',
                  fontSize: '14px',
                }}
              >
                <Typography variant="body2" component="div" key={link.id}>
                  {link.label}
                </Typography>
              </a>
            ))}
          </div>
        </Grid>
      ))}
    </Grid>
  );
};

const RenderIcon = () => {
  return (
    <div 
    style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent:'flex-end',
        gap: "24px",
    }}
    >
      {listIcon.map((icon) => (
        <div
          key={icon.id}
          style={{
            width: '32px',
            height: '32px',
            borderRadius: '50%',
            background: '#F4F4F4',
            padding: '2px 11px',
          }}
        >
          <img src={icon.icon} width={icon.width} height={icon.height} />
        </div>
      ))}
    </div>
  );
};

export const Footer: React.FC = () => {
  return (
    <Box sx={{ background: '#FAFAFA', padding: '20px 0', marginTop: '32px' }}>
      <Container>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <div style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'flex-start'
            }} 
            >
              <Logo variant="primary" size={1} />
            </div>
          </Grid>

          <Grid item xs={12}>
            <Grid container spacing={2}>
              <Grid item xl={6} lg={5}>
                <RenderInformation />
              </Grid>

              <Grid item xl={6} lg={7}>
                <RenderLink />
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Typography
              variant="body2"
              component="div"
              sx={{
                width: '100%',
                borderTop: '1px solid #F4F4F4',
                fontSize: '14px',
                fontWeight: '500',
                color: '#9FA0A6',
                marginTop: '30px',
                marginBottom: '10px',
              }}
            >
              © 2022 Akamental Inc. All rights reserved.
            </Typography>
          </Grid>

          <Grid item xs={12}>
            <RenderIcon />
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};

