import React from "react";
/* Styles */

interface Props {
  openStatus: boolean;
  children: React.ReactNode;
}

export const Content: React.FC<Props> = ({ children, openStatus }) => {
  return (
    <div
      style={{
        marginLeft: `${openStatus ? "256px" : "96px"}`,
        transition: "margin-left 0.5s linear",
        background: "#fafbff",
        minHeight: "100vh",
        padding: "20px",
        display: "flex",
        flexDirection: "column",
        flex: 1,
      }}
    >
      {children}
    </div>
  );
};
