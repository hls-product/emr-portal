import React from 'react'
import {Typography} from '@mui/material'
import {Logo} from '../../ui-components/Logo'
import {BackgroundColorSignin} from '../../pages/Auth/style'

import forgetPass from '../../assets/image/forgetPass.svg';

export const ForgetPasswordHeader: React.FC = () => {
  return (
    <BackgroundColorSignin>
    <div style={{
      display: 'flex',
      alignItems: 'flex-start',
      justifyContent: 'center',
      margin: 'auto',
      width: 'fit-content',
      gap: '260px'
    }}>
      <div style={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        padding: "39px 0 47px"
      }}>
        <Logo variant='secondary' size={1.2} />
        <Typography variant='h5' component='div' sx={{marginTop: "93px", marginLeft: "-11px", fontSize: "32px", fontWeight: "600", color: "#FFFFFF"}}>Forget Password</Typography>
      </div>
      <img src={forgetPass} />
    </div>
    </BackgroundColorSignin>
  )
}
