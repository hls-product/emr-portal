import React from "react";
import { Typography, Button } from "@mui/material";

import { StyledInput } from "./style";

import arrowLeft from "../../assets/sidebar/arrowLeft.svg";
import arrowRight from "../../assets/sidebar/arrowRight.svg";
import secondarySearchIcon from "../../assets/adminHeader/secondarySearchIcon.svg";
import messengerIcon from "../../assets/adminHeader/messenger.svg";
import speakerIcon from "../../assets/adminHeader/speaker.svg";
import notificationIcon from "../../assets/adminHeader/noti.svg";
import { useAppDispatch } from "../../store/Store";
import { interactSidebar } from "../../store/reducers/sidebar/sidebarSlice";

interface Props {
  handleInteractSupport?: () => void;
  openStatus: boolean;
}

export const Header: React.FC<Props> = ({
  openStatus,
  handleInteractSupport,
}) => {
  const [user, setUser] = React.useState<string>("Admin Williens");
  const dispatch = useAppDispatch();
  let quantity = 7;
  return (
    <div
      style={{
        marginLeft: `${openStatus ? "276px" : "116px"}`,
        transition: "margin-left 0.5s linear",
        padding: "23px 23px 23px 0",
        position: "sticky",
        display: "flex",
        justifyContent: "space-between",
        top: 0,
        background: "white",
        zIndex: 10000,
      }}
    >
      <div
        style={{
          width: "24px",
          height: "24px",
          position: "absolute",
          borderRadius: "50%",
          background: "#6C5DD3",
          cursor: "pointer",
          zIndex: 1300,
          top: "36.64px",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          marginLeft: "8px",
        }}
        onClick={() => {
          dispatch(interactSidebar(!openStatus));
        }}
      >
        <img
          src={openStatus ? arrowLeft : arrowRight}
          width={8}
          height={13}
          style={{ marginBottom: "0" }}
        />
      </div>

      <div
        style={{
          display: "flex",
          flexDirection: "column",
          marginLeft: "38px",
        }}
      >
        <Typography
          variant="body2"
          component="div"
          sx={{
            fontSize: "16px",
            fontWeight: "600",
            color: "#4E4E4E",
          }}
        >
          Welcome back,
        </Typography>
        <Typography
          variant="h5"
          component="div"
          sx={{
            fontSize: "32px",
            fontWeight: "600",
            color: "#131626",
            marginTop: "10px",
          }}
        >
          {user}
        </Typography>
      </div>

      <div style={{ display: "flex", gap: "16px" }}>
        <div
          style={{
            display: "flex",
            width: "fit-content",
            border: "1px solid #EAEAEA",
            borderRadius: "12px",
          }}
        >
          <img
            src={secondarySearchIcon}
            alt="Search"
            width={20}
            height={20}
            style={{ margin: "14px 0 14px 14px" }}
          />
          <StyledInput
            sx={{ m: 1, width: "fit-content" }}
            placeholder="Search..."
          />
        </div>

        <div
          style={{
            display: "flex",
            gap: "16px",
          }}
        >
          <Button
            sx={{
              minWidth: "fit-content",
              minHeight: "fit-content",
              border: "1px solid #F4F4F4",
              padding: "14px",
              borderRadius: "50%",
            }}
          >
            <img src={messengerIcon} width={20} height={20} />
          </Button>
          <Button
            sx={{
              minWidth: "fit-content",
              minHeight: "fit-content",
              border: "1px solid #F4F4F4",
              padding: "14px 14.5px",
              borderRadius: "50%",
            }}
            onClick={handleInteractSupport}
          >
            <img src={speakerIcon} width={19} height={18} />
          </Button>
          <Button
            sx={{
              minWidth: "fit-content",
              minHeight: "fit-content",
              border: "1px solid #F4F4F4",
              padding: "14px 16px",
              borderRadius: "50%",
              position: "relative",
            }}
          >
            <img src={notificationIcon} width={16} height={20} />
            {quantity !== 0 && (
              <div
                style={{
                  width: "18px",
                  height: "18px",
                  borderRadius: "50%",
                  background: "#E03535",
                  position: "absolute",
                  top: 0,
                  right: "-5px",
                  color: "#FFFFFF",
                  fontSize: "12px",
                  fontWeight: "600",
                }}
              >
                {quantity}
              </div>
            )}
          </Button>
        </div>
      </div>
    </div>
  );
};
