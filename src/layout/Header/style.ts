import { InputBase } from '@mui/material';
import { styled as muiStyled } from '@mui/material';

export const StyledInput = muiStyled(InputBase)(({}) => ({
  fontSize: '16px',
  width: '180px',
  height: '26px',
  '&::placeholder': {
    color: '#9FA0A6',
  },
  '& .css-ngazp5-MuiInputBase-input': {
    padding: '9px 0px 4px',
  },
}));
