import {styled} from '@mui/material/styles';
import Button, {ButtonProps} from '@mui/material/Button';

export const RoundedButton = styled(Button)<ButtonProps>({
  borderRadius: '99px',
  padding: "12px 16px",
  textDecoration: "none"
});
