import React from "react";
import { Box, IconButton } from "@mui/material";
import { colorPalette } from "../../theme";
import { RoundedButton } from "../RoundedButton";
import { DeleteOutlined, DescriptionOutlined } from "@mui/icons-material";
import { ReactComponent as UploadIcon } from "../../assets/icon/xls-upload.svg";

function formatBytes(bytes: number, decimals = 2) {
  if (bytes === 0) return "0 Bytes";

  const k = 1024;
  const dm = decimals < 0 ? 0 : decimals;
  const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];

  const i = Math.floor(Math.log(bytes) / Math.log(k));

  return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
}

interface FileUploadProps {
  file: any;
  setFile: any;
  minHeight?: number | string;
}

export const FileUpload: React.FC<FileUploadProps> = ({
  file,
  setFile,
  minHeight,
}) => {
  function onDrop(event: React.DragEvent) {
    event.preventDefault();
    event.stopPropagation();
    if (event.dataTransfer.files.length) {
      setFile(event.dataTransfer.files[0]);
    }
  }

  function onDrag(event: React.DragEvent) {
    event.preventDefault();
    event.stopPropagation();
  }

  const UploadButton = () => {
    const fileInput = React.useRef<any>(null);
    return (
      <>
        <input
          type="file"
          hidden
          ref={fileInput}
          onChange={(e) => setFile(e.target.files ? e.target.files[0] : null)}
        />
        <RoundedButton
          color="primary"
          variant="outlined"
          sx={{ borderColor: colorPalette.line }}
          startIcon={<UploadIcon />}
          onClick={() => fileInput.current?.click()}
        >
          Drop a file here or click
        </RoundedButton>
      </>
    );
  };

  const UploadedFile = () => {
    return (
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          border: `1px solid ${colorPalette.stroke}`,
          borderRadius: "12px",
          height: 48,
          px: "11px",
          py: "16px",
        }}
      >
        <Box sx={{ display: "flex", alignItems: "center" }}>
          <DescriptionOutlined sx={{ fill: colorPalette.lightGrey }} />
          {file.name}
          {" • "}
          {formatBytes(file.size, 0)}
        </Box>
        <IconButton onClick={() => setFile(null)}>
          <DeleteOutlined color="error" />
        </IconButton>
      </Box>
    );
  };

  return file ? (
    <UploadedFile />
  ) : (
    <Box
      sx={{
        border: `1px dashed ${colorPalette.stroke}`,
        borderRadius: "12px",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        minHeight: minHeight ?? 200,
      }}
      onDragOver={onDrag}
      onDrop={onDrop}
    >
      <UploadButton />
    </Box>
  );
};
