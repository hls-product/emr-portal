import {
  InputLabel,
  Select,
  styled as muiStyled,
  TextField,
} from "@mui/material";

interface LabelProps {
  Inputlabel?: number;
  isMultiline?: boolean;
}

export const StyledInput = muiStyled(TextField)<LabelProps>(
  ({ Inputlabel, isMultiline }) => ({
    width: "100%",

    "& .MuiInputBase-root": {
      borderRadius: "12px",
    },
    "& .MuiInputLabel-asterisk": {
      color: "#E65050",
    },
    "& .MuiOutlinedInput-input": {
      padding: `${isMultiline ? "0" : "16px 14px"}`,
    },
    legend: {
      span: {
        paddingLeft: `${Inputlabel}px !important`,
      },
    },
  })
);

export const StyledSelect = muiStyled(Select)<LabelProps>(({ Inputlabel }) => ({
  width: "100%",
  legend: {
    span: {
      paddingLeft: `${Inputlabel}px !important`,
    },
  },
}));
