import {
  FormControl,
  InputLabel,
  MenuItem,
  SelectChangeEvent,
} from "@mui/material";
import React from "react";
import { StyledSelect } from "./style";
import { cssPadding } from "./CustomTextField";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";

interface Props {
  label: string;
  value: unknown;
  onChange: (e: unknown) => void;
  schema: {
    enum: string[];
  };
}

export const CustomSelect: React.FC<Props> = ({
  label,
  value,
  onChange,
  schema,
}) => {
  const handleChange = (event: SelectChangeEvent<unknown>) => {
    onChange(event.target.value);
  };
  return (
    <>
      <FormControl
        sx={{
          width: "100%",
        }}
      >
        <InputLabel
          id="demo-simple-select-helper-label"
          required
          sx={{
            fontWeight: "bold",
            fontSize: "18px",
            color: "#131626",
            textTransform: "capitalize",
          }}
        >
          {label}
        </InputLabel>
        <StyledSelect
          labelId="demo-simple-select-helper-label"
          id="demo-simple-select-helper"
          value={value}
          IconComponent={() => (
            <KeyboardArrowDownIcon sx={{ marginRight: "14px" }} />
          )}
          displayEmpty
          label={label}
          onChange={handleChange}
          Inputlabel={cssPadding(label)}
        >
          {schema.enum.map((item) => (
            <MenuItem value={item} key={item}>
              {item}
            </MenuItem>
          ))}
        </StyledSelect>
      </FormControl>
    </>
  );
};
