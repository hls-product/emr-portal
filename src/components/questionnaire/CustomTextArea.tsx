import React from "react";
import { StyledInput } from "./style";
import { cssPadding } from "./CustomTextField";

interface Props {
  label: string;
  value: unknown;
  onChange: (e: unknown) => void;
}

export const CustomTextArea: React.FC<Props> = ({ label, onChange, value }) => {
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    onChange(e.target.value);
  };

  return (
    <>
      <StyledInput
        multiline
        label={`${label}`}
        variant="outlined"
        placeholder={`Enter ${label} here...`}
        InputLabelProps={{ shrink: true }}
        value={value}
        onChange={handleChange}
        Inputlabel={cssPadding(label)}
        rows={10}
        isMultiline={true}
        inputProps={{
          readOnly: label === "enableWhenExpression" ? true : false,
        }}
      />
    </>
  );
};
