import { Typography } from "@mui/material";
import React from "react";
import { StyledInput } from "./style";

interface Props {
  label: string;
  value: unknown;
  onChange: (e: unknown) => void;
  required: boolean;
}

export const cssPadding = (label: string) => {
  if (label.length <= 2) {
    return 5;
  } else if (label.length >= 3 && label.length <= 7) {
    return 18;
  } else if (label.length >= 8 && label.length <= 12) {
    return 21;
  } else if (label.length >= 13 && label.length <= 15) {
    return 24;
  }
  return 30;
};

export const CustomTextField: React.FC<Props> = ({
  label,
  onChange,
  value,
  required,
}) => {
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    onChange(e.target.value);
  };

  return (
    <>
      <StyledInput
        label={`${label}`}
        variant="outlined"
        placeholder={`Enter ${label} here...`}
        // InputLabelProps={{ shrink: true }}
        required={required}
        value={value}
        onChange={handleChange}
        Inputlabel={cssPadding(label)}
      />
    </>
  );
};
