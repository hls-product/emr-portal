import React, { useEffect, useState } from "react";
import { Box, Button, Grid, Paper, Typography } from "@mui/material";
import { makeStyles, createStyles } from "@mui/styles";
import { Alert } from "@mui/lab";
import {
  Primitive,
  Question,
  Questionnaire,
  QuestionnaireEngine,
} from "@covopen/covquestions-js";
import { ResultComponent } from "../questionResult";
import "typeface-fira-sans";
import { QuestionFormComponent } from "../questionComponents/QuestionFormComponent";
import { QuestionnaireJsonEditor } from "../../components/questionnaireEditor/QuestionnaireJsonEditor";
import { JSONSchema7 } from "json-schema";
import questionnaireSchema from "../../schemas/questionnaire.json";
import DOMPurify from "dompurify";

type QuestionnaireExecutionProps = {
  currentQuestionnaire: Questionnaire;
  isJsonInvalid: boolean;
  downloadJson?: () => void;
  viewDetail?: boolean;
};

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      backgroundColor: "#F7FAFC",
      border: "1.5px solid #CBD5E0",
      borderRadius: 6,
      boxSizing: "border-box",
      boxShadow: "none",
      padding: 20,
    },
    padding: {
      padding: "10px 0",
    },
    execution: {
      overflow: "auto",
    },
    internalState: {
      backgroundColor: "#F7FAFC",
      border: "1.5px solid #CBD5E0",
      borderRadius: 6,
      boxSizing: "border-box",
      boxShadow: "none",
      fontFamily: "Fira Sans",
      fontSize: 14,
      fontWeight: 500,
      letterSpacing: "0.1rem",
      padding: 10,
      opacity: 0.6,
      overflow: "auto",
      flex: 1,
    },
    internalStateHeadline: {
      color: "#A0AEC0",
      fontFamily: "Fira Sans",
      fontWeight: 500,
      fontSize: 14,
      lineHeight: "17px",
      letterSpacing: "0.1em",
      opacity: 0.8,
      textTransform: "uppercase",
      margin: "auto",
      "margin-bottom": 0,
      "margin-left": 0,
    },
    questionHeadline: {
      opacity: 0.7,
      fontWeight: 500,
      fontSize: 18,
      lineHeight: "20px",
    },
    questionDetails: {
      color: "#686868",
      opacity: 1,
      fontSize: 16,
      lineHeight: "20px",
    },
    questionFormElement: {
      marginTop: 15,
      marginBottom: 15,
    },
    marginRight: {
      marginRight: "10px",
    },
  })
);

export const QuestionnaireExecution: React.FC<QuestionnaireExecutionProps> = ({
  currentQuestionnaire,
  isJsonInvalid,
  downloadJson,
  viewDetail,
}) => {
  const [questionnaireEngine, setQuestionnaireEngine] = useState(
    new QuestionnaireEngine(currentQuestionnaire)
  );
  const [currentQuestion, setCurrentQuestion] = useState<Question | undefined>(
    undefined
  );
  const [currentValue, setCurrentValue] = useState<
    Primitive | Array<Primitive> | undefined
  >(undefined);

  const classes = useStyles();

  function restartQuestionnaire() {
    setQuestionnaireEngine(new QuestionnaireEngine(currentQuestionnaire));
    setCurrentValue(undefined);
  }

  function handleNextClick() {
    questionnaireEngine.setAnswer(currentQuestion!.id, currentValue);
    setCurrentValue(undefined);
    setCurrentQuestion(questionnaireEngine.nextQuestion());
  }

  function handleBackClick() {
    const { question, answer } = questionnaireEngine.previousQuestion(
      currentQuestion!.id
    );
    setCurrentValue(answer);
    setCurrentQuestion(question);
  }

  useEffect(() => {
    setQuestionnaireEngine((prevEngine) => {
      const newEngine = new QuestionnaireEngine(currentQuestionnaire);
      newEngine.setAnswersPersistence(prevEngine.getAnswersPersistence());
      return newEngine;
    });
  }, [currentQuestionnaire]);

  useEffect(() => {
    setCurrentQuestion(questionnaireEngine.nextQuestion());
  }, [questionnaireEngine]);

  const progress = questionnaireEngine.getProgress();
  // TODO If the last question is not shown because of the enableWhenExpression, the progress never reaches 1 => fix in covquestions-js. Then it should be possible to use "=== 1")
  // https://github.com/CovOpen/CovQuestions/issues/138
  const results =
    progress > 0 && !currentQuestion
      ? questionnaireEngine.getCategoryResults()
      : undefined;

  return (
    <Grid
      container
      direction="column"
      justifyContent="space-between"
      className={`${classes.padding} overflow-pass-through`}
      spacing={2}
    >
      {!viewDetail && (
        <Grid
          item
          container
          direction="column"
          className="overflow-pass-through"
        >
          <Grid
            container
            direction="row"
            justifyContent="space-between"
            spacing={2}
          >
            <Grid
              item
              xs={12}
              style={{
                minHeight: "650px",
              }}
            >
              <Typography className={classes.internalStateHeadline}>
                JSON Preview
              </Typography>
              <QuestionnaireJsonEditor
                schema={questionnaireSchema as JSONSchema7}
              />
            </Grid>
          </Grid>
        </Grid>
      )}

      {questionnaireEngine ? (
        <Grid
          item
          container
          direction="column"
          className="overflow-pass-through"
        >
          <Grid
            container
            direction="row"
            justifyContent="space-between"
            spacing={2}
          >
            <Grid
              item
              xs={viewDetail ? 12 : 6}
              className={`${classes.execution}`}
            >
              {!viewDetail && (
                <Typography className={classes.internalStateHeadline}>
                  Questionnaire Preview
                </Typography>
              )}
              <>
                {isJsonInvalid ? (
                  <Alert severity="warning">
                    Cannot load questionnaire. JSON is invalid!
                  </Alert>
                ) : null}
                {results === undefined && currentQuestion ? (
                  <Paper className={classes.root}>
                    <Grid container direction="column" alignItems="stretch">
                      <Grid item container xs={12} spacing={1}>
                        <Grid item xs={12}>
                          <Typography className={classes.questionHeadline}>
                            {currentQuestion.text}
                          </Typography>
                        </Grid>
                        {currentQuestion.details ? (
                          <Grid item xs={12}>
                            <Typography
                              className={classes.questionDetails}
                              dangerouslySetInnerHTML={{
                                __html: DOMPurify.sanitize(
                                  currentQuestion.details
                                ),
                              }}
                            />
                          </Grid>
                        ) : undefined}
                        <Grid
                          item
                          xs={12}
                          className={classes.questionFormElement}
                        >
                          <QuestionFormComponent
                            currentQuestion={currentQuestion}
                            onChange={setCurrentValue}
                            value={currentValue}
                          />
                        </Grid>
                      </Grid>
                      <Grid container justifyContent="space-between">
                        <Grid item>
                          {progress > 0 ? (
                            <Button
                              onClick={handleBackClick}
                              variant="contained"
                              color="primary"
                            >
                              Back
                            </Button>
                          ) : null}
                        </Grid>
                        <Grid item>
                          <Button
                            onClick={handleNextClick}
                            variant="contained"
                            color="primary"
                            disabled={
                              !currentQuestion.optional &&
                              currentValue === undefined
                            }
                          >
                            Next
                          </Button>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Paper>
                ) : null}
                {results !== undefined ? (
                  <ResultComponent results={results} />
                ) : null}
              </>
              {viewDetail ? null : results === undefined && currentQuestion ? (
                <Button
                  onClick={downloadJson}
                  className={classes.marginRight}
                  variant="contained"
                  color="secondary"
                  sx={{ marginTop: "5px" }}
                >
                  Download
                </Button>
              ) : null}
            </Grid>

            {!viewDetail && (
              <Grid item xs={6}>
                <Typography className={classes.internalStateHeadline}>
                  Question Schema
                </Typography>
                <Paper className={classes.internalState}>
                  <Box style={{ whiteSpace: "pre-wrap", overflow: "auto" }}>
                    {JSON.stringify(
                      questionnaireEngine.getDataObjectForDeveloping(),
                      null,
                      2
                    )}
                  </Box>
                </Paper>
                <Button
                  onClick={restartQuestionnaire}
                  variant="contained"
                  color="secondary"
                  sx={{
                    marginTop: "5px",
                  }}
                >
                  Restart Questionnaire
                </Button>
              </Grid>
            )}
          </Grid>
        </Grid>
      ) : null}
    </Grid>
  );
};
