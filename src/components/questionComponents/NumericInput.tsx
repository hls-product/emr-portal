import React from "react";
import { Grid, Input, Slider } from "@mui/material";
import { QuestionFormComponentProps } from "./QuestionFormComponent";

export const NumericInput: React.FC<QuestionFormComponentProps> = ({
  currentQuestion,
  onChange,
  value,
}) => {
  const { min, max, step, defaultValue } = currentQuestion.numericOptions || {};
  const fallbackValue = defaultValue ?? min ?? max ?? 0;

  const handleSliderChange = (
    event: Event,
    value: number | number[],
    activeThumb: number
  ) => {
    onChange(typeof value === "number" ? value : fallbackValue);
  };

  return (
    <Grid container item spacing={2} alignItems="center" xs={12}>
      <Grid item xs>
        <Slider
          value={value ?? fallbackValue}
          min={min}
          max={max}
          step={step}
          key={currentQuestion.id}
          onChange={handleSliderChange}
          aria-labelledby="discrete-slider-always"
          valueLabelDisplay="auto"
        />
      </Grid>
    </Grid>
  );
};
