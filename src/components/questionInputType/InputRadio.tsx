import { Option } from "@covopen/covquestions-js/src/models/Questionnaire.generated";
import { FormControlLabel, Radio, RadioGroup } from "@mui/material";

export default function InputRadio(props: {
  options?: Option[];
  selectedValue?: string;
}) {
  const { selectedValue, options } = props;

  return (
    <>
      <RadioGroup
        aria-labelledby="demo-radio-buttons-group-label"
        defaultValue={selectedValue}
        name="radio-buttons-group"
      >
        {options &&
          options.map((option) => (
            <FormControlLabel
              value={option.value}
              control={<Radio />}
              label={option.text}
            />
          ))}
      </RadioGroup>
    </>
  );
}
