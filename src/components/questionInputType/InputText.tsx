import { TextField } from "@mui/material";

export default function InputText(props: { selectedValue?: string }) {
  const { selectedValue } = props;

  return (
    <>
      <TextField
        id="outlined-multiline-static"
        label="Answer"
        multiline
        rows={7}
        defaultValue={selectedValue}
        sx={{
          marginTop: "10px",
          minWidth: "800px",
        }}
      />
    </>
  );
}
