import { Option } from "@covopen/covquestions-js/src/models/Questionnaire.generated";
import { Checkbox, FormControlLabel } from "@mui/material";
import { AnswerContainter } from "./style";

export default function InputMultipleChoices(props: {
  options?: Option[];
  selectedValues?: string[];
}) {
  const { selectedValues = [], options } = props;

  return (
    <>
      <AnswerContainter>
        {options &&
          options.map((option) => (
            <FormControlLabel
              control={
                <Checkbox
                  defaultChecked={
                    selectedValues.includes(option.value) ? true : false
                  }
                />
              }
              label={option.text}
            />
          ))}
      </AnswerContainter>
    </>
  );
}
