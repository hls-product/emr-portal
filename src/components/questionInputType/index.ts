import InputBoolean from './InputBoolean';
import InputDate from './InputDate';
import InputMultipleChoices from './InputMultipleChoices';
import InputNumeric from './InputNumeric';
import InputRadio from './InputRadio';
import InputText from './InputText';

export {
  InputBoolean,
  InputDate,
  InputMultipleChoices,
  InputRadio,
  InputText,
  InputNumeric,
};
