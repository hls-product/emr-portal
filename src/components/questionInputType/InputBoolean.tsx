import { FormControlLabel, Radio, RadioGroup } from "@mui/material";

export default function InputBoolean(props: { selectedValue?: boolean }) {
  const { selectedValue } = props;
  const options = [
    {
      text: "Yes",
      value: true,
    },
    {
      text: "No",
      value: false,
    },
  ];

  return (
    <>
      <RadioGroup
        aria-labelledby="demo-radio-buttons-group-label"
        defaultValue={selectedValue}
        name="radio-buttons-group"
      >
        {options &&
          options.map((option) => (
            <FormControlLabel
              value={option.value}
              control={<Radio />}
              label={option.text}
            />
          ))}
      </RadioGroup>
    </>
  );
}
