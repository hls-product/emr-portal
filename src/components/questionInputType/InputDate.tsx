import { TextField } from "@mui/material";
import moment from "moment";

export default function InputDate(props: { selectedValue?: Date }) {
  const { selectedValue } = props;

  return (
    <>
      <TextField
        id="outlined-multiline-static"
        label="Answer"
        defaultValue={selectedValue}
        sx={{
          marginTop: "10px",
        }}
      />
    </>
  );
}
