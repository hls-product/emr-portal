import { Slider } from "@mui/material";

export default function InputNumeric(props: {
  min: number;
  max: number;
  step: number;
  selectedValue?: number;
}) {
  const { selectedValue, min, max, step } = props;

  return (
    <>
      <Slider
        defaultValue={selectedValue}
        step={step}
        marks
        min={min}
        max={max}
        valueLabelDisplay="auto"
      />
    </>
  );
}
