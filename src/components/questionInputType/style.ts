import { styled as muiStyled } from "@mui/material";

export const AnswerContainter = muiStyled("div")`
    width: 100%;
    width: 500px !important;
    display: flex !important;
    flex-wrap: wrap;
    width: 500px;
    justify-content: center;
    gap: 10px;

    .answer {
        height: 75px;
        border: 1px solid #EAEAEA;
        border-radius: 20px;
        margin-top: 8px;
        flex-direction: row;
        align-items: center;
        padding: 0 20px;
      }
`;
