import React from "react";
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  styled,
  Typography,
} from "@mui/material";
import { RoundedButton } from "../RoundedButton";
import { ReactComponent as DeleteIcon } from "../../assets/delete.svg";
import { colorPalette } from "../../theme";
import { Close } from "@mui/icons-material";

interface DeleteDialogProps {
  open: boolean;
  onClose?: React.EventHandler<any>;
  onConfirm?: React.EventHandler<any>;
  onCancel?: React.EventHandler<any>;
}

export const DeleteDialog: React.FC<DeleteDialogProps> = ({
  open,
  onClose,
  onConfirm,
  onCancel,
}) => (
  <Dialog
    open={open}
    onClose={onClose}
    PaperProps={{ sx: { height: 140, width: 500 } }}
  >
    <DialogTitle>
      <IconButton
        onClick={onCancel}
        sx={{
          position: "absolute",
          right: 8,
          top: 8,
          color: colorPalette.grey,
        }}
      >
        <Close />
      </IconButton>
    </DialogTitle>
    <DialogContent
      sx={{
        display: "flex",
        flexFlow: "column",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Typography variant="body1" sx={{ color: colorPalette.lightGrey }}>
        Do you really want to delete? Will not be able to restore.
      </Typography>
    </DialogContent>
    <DialogActions sx={{ justifyContent: "flex-end", marginRight: "1rem" }}>
      <Typography
        onClick={onCancel}
        sx={{
          color: "blue",
          marginRight: "0.2rem",
          cursor: "pointer",
          "&:hover": {
            color: " #050567",
          },
        }}
      >
        Cancel
      </Typography>
      <Typography
        onClick={onConfirm}
        sx={{
          color: "red",
          cursor: "pointer",
          "&:hover": {
            color: " #5b0606",
          },
        }}
      >
        Delete
      </Typography>
    </DialogActions>
  </Dialog>
);
