import { styled as muiStyled, Typography, Box } from "@mui/material";

interface ToolbarProps {
  background: string;
}

export const StyledToolbar = muiStyled(Box)<ToolbarProps>(({ background }) => ({
  display: "flex",
  alignItems: "center",
  justifyContent: "flex-start",
  padding: "12px",
  gap: "7px",
  background: `${background}`,
  borderRadius: "8px",
  width: "100%",
  boxSizing: "border-box",
}));
