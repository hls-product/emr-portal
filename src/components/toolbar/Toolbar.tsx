import React from "react";
import { Button } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { useAppDispatch } from "../../store/Store";

import { StyledToolbar } from "./style";

export interface Action {
  label: string;
  url: string;
}

interface Props {
  actionList: Action[];
  defaultValue: Action;
  background: string;
  id?: string;
}

export const Toolbar: React.FC<Props> = ({
  actionList,
  defaultValue,
  background,
  id
}) => {
  const [selected, setSelected] = React.useState<Action>(defaultValue);
  const navigate = useNavigate();
  return (
    <StyledToolbar background={background}>
      {actionList.map(({ label, url }) => (
        <Button
          key={label}
          variant={label === selected.label ? "contained" : "text"}
          onClick={() => {
            navigate(url+"/"+String(id));
            setSelected({
              label: label,
              url: url,
            });
          }}
          sx={{
            borderRadius: "10px",
            padding: "9px 16px",
          }}
        >
          {label}
        </Button>
      ))}
    </StyledToolbar>
  );
};
