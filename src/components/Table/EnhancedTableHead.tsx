import { visuallyHidden } from "@mui/utils";
import TableSortLabel from "@mui/material/TableSortLabel";
import UnfoldMoreRoundedIcon from "@mui/icons-material/UnfoldMoreRounded";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import Checkbox from "@mui/material/Checkbox";
import Box from "@mui/material/Box";
import { Data, EnhancedTableProps } from "./types";
import styles from "./Styles.module.css";

function EnhancedTableHead(props: EnhancedTableProps) {
  const {
    checkBox,
    onSelectAllClick,
    order,
    orderBy,
    numSelected,
    rowCount,
    onRequestSort,
  } = props;
  const createSortHandler =
    (property: keyof Data) => (event: React.MouseEvent<unknown>) => {
      onRequestSort(event, property);
    };
  return (
    <TableHead sx={{ background: "#FAFAFA" }}>
      <TableRow>
        {checkBox && (
          <TableCell padding="checkbox">
            <Checkbox
              color="primary"
              indeterminate={
                numSelected ? numSelected > 0 && numSelected < rowCount : false
              }
              checked={rowCount > 0 && numSelected === rowCount}
              onChange={onSelectAllClick}
            />
          </TableCell>
        )}
        {props.headcells?.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.id === "action" ? "center" : "left"}
            sortDirection={orderBy === headCell.id ? order : false}
            className={styles.tableCell_TableHead}
            sx={{whiteSpace: "nowrap"}}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : "asc"}
              onClick={createSortHandler(headCell.id as keyof Data)}
              IconComponent={UnfoldMoreRoundedIcon}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <Box component="span" sx={visuallyHidden}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </Box>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

export default EnhancedTableHead;
