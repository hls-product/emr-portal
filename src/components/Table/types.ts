export interface ShippingInfo {
  shippingName: string;
  shippingPhone: string;
}

export interface PatientInfo {
  patientName: string;
  patientPhone: string;
}

export interface Data {
  orderId?: string;
  patientInfo?: PatientInfo;
  patientAddress?: string;
  period?: string;
  type?: string;
  shippingInfo?: ShippingInfo;
  shippingAddress?: string;
  status?: string;
  orderDate?: string;
  total?: number;
  action?: any;
}

export type Order = "asc" | "desc";

export interface HeadCell {
  id: keyof Data | string;
  disablePadding? : boolean;
  numeric: boolean;
  label: string;
}

export interface EnhancedTableProps {
  headcells?: HeadCell[];
  numSelected?: number;
  onRequestSort: (event: React.MouseEvent<unknown>, property: any) => void;
  onSelectAllClick?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  order?: Order;
  orderBy?: string;
  rowCount: number;
  checkBox ?:boolean;
}

export interface EnhancedTableToolbarProps {
  numSelected: number;
  tableTitle: string;
}

export interface TablePaginationActionsProps {
  count: number;
  page: number;
  rowsPerPage: number;
  onPageChange: (
    event: React.MouseEvent<HTMLButtonElement>,
    newPage: number
  ) => void;
}