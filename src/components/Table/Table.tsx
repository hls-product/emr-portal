import {
  Box,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TablePagination,
  TableRow,
} from "@mui/material";
import React from "react";
import EnhancedTableHead from "./EnhancedTableHead";
import Loading from "../Loading";
import { EnhancedTableToolbarProps, HeadCell } from "./types";
import { TablePaginationActions } from "./TablePagination";
import EnhancedTableToolbar from "./EnhancedTableToolbar";

interface Props {
  title?: string;
  headcells: HeadCell[];
  body: {
    id: string;
    [key: string]: React.ReactElement[] | React.ReactElement | string | number | boolean | undefined;
  }[];
  paging: {
    page: number;
    rowsPerPage: number;
    rowsCount: number;
  };
  loading?: boolean;
  toolbar?: boolean;
  onPageChange: (event: any, newPage: number) => void;
  onRowsPerPageChange: (event: any) => void;
  selected?: string[];
  handleSelect?: (event: any, id: string) => void;
  handleSelectAll?: (event: any) => void;
}

export const CustomTable: React.FC<Props> = ({
  title,
  headcells,
  body,
  paging,
  loading,
  toolbar,
  onPageChange,
  onRowsPerPageChange,
  selected,
  handleSelectAll,
  handleSelect,
}) => {
  const [order, setOrder] = React.useState<"asc" | "desc">("asc");
  const [orderBy, setOrderBy] = React.useState<string>("id");

  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: string
  ) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };
  return (
    <React.Fragment>
      <Table
        sx={{
          background: "white",
          position: "relative",
          marginTop: "16px",
          "& .MuiTableRow-head": {
            background: "rgba(250, 250, 250, 1)",
          },
        }}
      >
        {/* head */}
        <EnhancedTableHead
          headcells={headcells}
          numSelected={selected?.length}
          onSelectAllClick={handleSelectAll}
          rowCount={paging.rowsPerPage}
          order={order}
          orderBy={orderBy}
          onRequestSort={handleRequestSort}
          checkBox={toolbar ? true : false}
        />
        {/* body */}
        {!loading && (
          <TableBody>
            {body.map((item, index) => (
              <TableRow key={item.id}>
                {toolbar && handleSelect && (
                  <TableCell
                    padding="checkbox"
                    children={
                      <Checkbox
                        color="primary"
                        checked={selected?.includes(item.id)}
                        onChange={(e) => handleSelect(e, item.id)}
                      />
                    }
                  />
                )}

                {headcells.map((key) => (
                  <TableCell key={key.id} children={item[key.id]} />
                ))}
              </TableRow>
            ))}
          </TableBody>
        )}
      </Table>

      {loading && (
        <Box
          sx={{
            width: "100%",
            justifyContent: "center",
          }}
          children={<Loading />}
        />
      )}
      {/* pagination */}
      <TablePagination
        count={paging.rowsCount || -1}
        rowsPerPageOptions={[5, 10, 25, 50]}
        page={paging.page}
        rowsPerPage={paging.rowsPerPage}
        onPageChange={onPageChange}
        onRowsPerPageChange={onRowsPerPageChange}
        sx={{ background: "rgba(250, 250, 250, 1)" }}
        ActionsComponent={TablePaginationActions}
      />
    </React.Fragment>
  );
};
