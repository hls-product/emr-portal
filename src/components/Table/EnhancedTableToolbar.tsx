import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import SearchIcon from "@mui/icons-material/Search";
import InputBase from "@mui/material/InputBase";
import styles from "./Styles.module.css";
import { alpha } from "@mui/material/styles";
import { EnhancedTableToolbarProps } from "./types";

const EnhancedTableToolbar = (props: EnhancedTableToolbarProps) => {
  const { numSelected } = props;

  return (
    <Toolbar
      sx={{
        pl: { sm: 2 },
        pr: { xs: 1, sm: 1 },
        ...(numSelected > 0 && {
          bgcolor: (theme) =>
            alpha(
              theme.palette.primary.main,
              theme.palette.action.activatedOpacity
            ),
        }),
      }}
    >
      {numSelected > 0 ? (
        <Typography
          sx={{ flex: "1 1 100%" }}
          color="inherit"
          variant="subtitle1"
          component="div"
          children = {numSelected + " selected"}
        />
      ) : (
        <Box
          component="form"
          noValidate
          autoComplete="off"
          className={styles.toolbar_TableToolbar}
        >
          <Typography
            variant="h6"
            id="tableTitle"
            component="div"
            className={styles.typo_TableToolbar}
          >
            {props.tableTitle}
          </Typography>
          <Paper component="form" className={styles.input_TableToolbar}>
            <SearchIcon
              sx={{
                height: "20px",
                width: "20px",
                margin: "14px 10px 14px 14px",
              }}
            />
            <InputBase
              placeholder="Search billing id"
              sx={{
                fontSize: "16px",
                width: "271px",
              }}
            />
          </Paper>
        </Box>
      )}
    </Toolbar>
  );
};

export default EnhancedTableToolbar;
