import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import React from 'react';

const TableHeader: React.FC<{ columns: string[] }> = ({ columns }) => {
  return (
    <TableHead sx={{ backgroundColor: "#FAFAFA" }}>
      <TableRow>
        {columns.map((c) => (
          <TableCell key={c} sx={{ fontSize: 14, fontWeight: 600 }}>
            {c}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
};

export default TableHeader;