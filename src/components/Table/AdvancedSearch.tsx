import React from "react";
import {
  BaseTextFieldProps,
  Box,
  Button,
  MenuItem,
  TextField,
  Typography,
} from "@mui/material";
import { DatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import dayjs, { Dayjs } from "dayjs";

export interface SearchFields extends BaseTextFieldProps {
  menuItems?: { value: string | boolean; display: string }[];
  dateTime?: "single" | "period" | string;
  inputFormat?: string;
  name: string;
}

export interface AdvancedSearchProps {
  props: Array<SearchFields>;
  description?: string;
  filter: { [key: string]: string };
  onFilterChange: React.Dispatch<
    React.SetStateAction<{ [key: string]: string }>
  >;
  onApply: ((arg?:any) => Promise<void>) | (() => void);
  onClose: React.Dispatch<React.SetStateAction<boolean>>;
}
export const AdvancedSearch: React.FC<AdvancedSearchProps> = ({
  props,
  filter,
  onApply,
  description,
  onFilterChange,
  onClose,
}) => {
  React.useEffect(() => {
    console.log(filter);
  }, [filter]);
  return (
    <Box
      sx={{
        padding: "1rem 1rem 2rem",
        marginTop: "1rem",
        background: "#fafbff",
      }}
    >
      <Box
        sx={{
          width: "100%",
          display: "flex",
          justifyContent: "space-between",
          alignItems: "flex-start",
        }}
      >
        {/* ---HEAD--- */}
        <Box
          children={
            <React.Fragment>
              <Typography
                fontSize="18px"
                fontWeight="400"
                children="Advanced Search"
              />
              <Typography
                sx={{
                  padding: "0.3rem 0",
                  color: "#949494",
                }}
                children={description}
              />
            </React.Fragment>
          }
        />
        <Box
          children={
            <React.Fragment>
              <Button onClick={() => onApply(filter)} children="Apply Filters" />
              <Button
                onClick={() => {
                  onFilterChange({});
                  onClose(false);
                  if(Object.keys(filter).some(item => filter[item])) onApply();
                }}
                children="Cancel"
              />
            </React.Fragment>
          }
        />
      </Box>
      {/* FIELDS */}
      <Box
        sx={{
          display: "flex",
          gap: "5%",
          flexWrap: "wrap",
          width: "100%",
          border: "1px solid #e4e5e9",
          padding: "0.5rem 1rem 2rem",
          borderRadius: "0.4rem",
          background: "white",
        }}
        children={props.map((item) =>
          item.dateTime === "single" ? (
            <LocalizationProvider
              dateAdapter={AdapterDayjs}
              children={
                <DatePicker
                  value={dayjs(filter[item.name as string]) || null}
                  label={item.label}
                  inputFormat={item.inputFormat as string}
                  onChange={(value) =>
                    onFilterChange({
                      ...filter,
                      [item.name as string]: value?.format(
                        item.inputFormat as string
                      ) as string,
                    })
                  }
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      sx={{ flexBasis: "30%", marginTop: "0.5rem" }}
                    />
                  )}
                  InputProps={{
                    style: {
                      borderRadius: "12px",
                      color: "black",
                    },
                  }}
                />
              }
            />
          ) : item.dateTime === "period" ? (
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              <Box
                sx={{
                  display: "flex",
                  flexBasis: "30%",
                  justifyContent: "space-between",
                }}
              >
                <DatePicker
                  value={dayjs(filter.from) || null}
                  label={"From"}
                  inputFormat={item.inputFormat as string}
                  onChange={(value) =>
                    onFilterChange({
                      ...filter,
                      from: value?.format(item.inputFormat as string) as string,
                    })
                  }
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      sx={{ flexBasis: "49%", marginTop: "0.5rem" }}
                    />
                  )}
                  InputProps={{
                    style: {
                      borderRadius: "12px",
                      color: "black",
                    },
                  }}
                />
                <DatePicker
                  value={dayjs(filter.to) || null}
                  label={"To"}
                  inputFormat={item.inputFormat as string}
                  onChange={(value) =>
                    onFilterChange({
                      ...filter,
                      to: value?.format(item.inputFormat as string) as string,
                    })
                  }
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      sx={{ flexBasis: "49%", marginTop: "0.5rem" }}
                    />
                  )}
                  InputProps={{
                    style: {
                      borderRadius: "12px",
                      color: "black",
                    },
                  }}
                />
              </Box>
            </LocalizationProvider>
          ) : (
            <TextField
              sx={{
                flexBasis: "30%",
                marginTop: "0.5rem",
              }}
              margin="dense"
              fullWidth
              variant="standard"
              value={filter[item.name as string] || ""}
              {...item}
              onChange={(e) =>
                onFilterChange({
                  ...filter,
                  [e.target.name]: e.target.value as string,
                })
              }
            >
              <MenuItem value="" children="--None--" />
              {item.select &&
                item.menuItems?.map((x, index) => (
                  <MenuItem
                    key={index}
                    value={x.value as string}
                    children={x.display}
                  />
                ))}
            </TextField>
          )
        )}
      />
    </Box>
  );
};
