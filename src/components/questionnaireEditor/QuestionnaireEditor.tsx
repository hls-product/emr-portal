import { Grid } from "@mui/material";
import { QuestionBodyResponse } from "../../models/question/QuestionBody";
import { QuestionnaireFormEditor } from "./QuestionnaireFormEditor";

interface Props {
  mode?: string;
  data?: QuestionBodyResponse;
}

export function QuestionnaireEditor({ mode, data }: Props) {
  const style = `
  .MuiTabs-root, .MuiTabs-scroller, .MuiTabs-flexContainer {
    margin: 0;
  }
  .rjsf .MuiBox-root {
    padding: 0;
  }
  .rjsf .MuiGrid-item {
    padding: 0px 8px 0px 8px;
  }
  .rjsf .form-group, .rjsf .panel-body {
    margin: 0;
  }
  `;

  return (
    <Grid container direction="column" className="overflow-pass-through">
      <style>{style}</style>
      <Grid item container xs={12} className="overflow-pass-through">
        <QuestionnaireFormEditor mode={mode} data={data} />
      </Grid>
    </Grid>
  );
}
