import {
  Button,
  Divider,
  Grid,
  IconButton,
  List,
  ListItem,
  ListItemText,
  Typography,
} from "@mui/material";
import { createStyles, makeStyles } from "@mui/styles";
import React, { useEffect, useState } from "react";
import ArrowUpwardIcon from "@mui/icons-material/ArrowUpward";
import ArrowDownwardIcon from "@mui/icons-material/ArrowDownward";
import DeleteIcon from "@mui/icons-material/Delete";
import FileCopy from "@mui/icons-material/FileCopy";
import AddIcon from "@mui/icons-material/Add";
import WarningIcon from "@mui/icons-material/Warning";
import { useAppDispatch } from "../../store/Store";
import { useSelector } from "react-redux";
import {
  removeItem,
  swapItemWithNextOne,
  addNewItem,
  setQuestionnaireInEditor,
} from "../../store/reducers/question/questionSlice";
import { ElementEditorSwitch } from "./formEditor/elementEditors/ElementEditorSwitch";
import {
  EditorQuestion,
  EditorQuestionnaire,
  EditorResultCategory,
  EditorVariable,
} from "../../models/question";
import { ALL_TEST_CASES_RUN } from "./formEditor/elementEditors/testCases/AllTestCaseView";
import { IRootState } from "../../store/reducers";
import { QuestionnaireExecution } from "../questionExecution";
import {
  isNumericQuestion,
  isQuestionWithOptions,
  Questionnaire,
  QuestionWithOptions,
} from "@covopen/covquestions-js";
import { removeStringRepresentationFromQuestionnaire } from "./converters";
import { createQuestionnaire } from "../../store/reducers/questionnaire/createQuestionnaireSlice";
import {
  QuestionBodyResponse,
  Questions,
  ResultCategory,
  Variable,
} from "../../models/question/QuestionBody";
import { NumericQuestion } from "@covopen/covquestions-js/src/models/Questionnaire.generated";
import { useNavigate } from "react-router-dom";
import { editQuestionnaire } from "../../store/reducers/questionnaire/editQuestionnaireSlice";

type QuestionnaireFormEditorProps = {
  mode?: string;
  data?: QuestionBodyResponse;
};

export enum SectionTypeSingle {
  META = "meta",
  RUN_TEST_CASES = "runTestCases",
}

export enum SectionTypeArray {
  QUESTIONS = "questions",
  RESULT_CATEGORIES = "resultCategories",
  VARIABLES = "variables",
  TEST_CASES = "testCases",
}

type SectionType = SectionTypeSingle | SectionTypeArray;

function isNonArraySection(section: SectionType): section is SectionTypeSingle {
  return (
    section === SectionTypeSingle.META ||
    section === SectionTypeSingle.RUN_TEST_CASES
  );
}

export type ActiveItem = {
  section: SectionType;
  index: number;
};

export function QuestionnaireFormEditor({
  mode,
  data,
}: QuestionnaireFormEditorProps) {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const [preview, setPreview] = useState<boolean>(false);
  const [executedQuestionnaire, setExecutedQuestionnaire] = useState<
    Questionnaire | undefined
  >(undefined);

  const definedData = data as unknown as QuestionBodyResponse;

  let filterQuestionnaire: EditorQuestion[] = [];
  let filterResult: EditorResultCategory[] | undefined = [];
  let filterVariable: EditorVariable[] | undefined = [];

  if (mode === "edit") {
    filterQuestionnaire = definedData.questions.map((question) => ({
      id: question.id,
      text: question.text,
      type: question.type,
      details: question.details,
      enableWhenExpression: question.enableWhenExpression
        ? JSON.parse(question.enableWhenExpression)
        : undefined,
      optional: question.optional,
      numericOptions: question.numberOptions
        ? question.numberOptions
        : undefined,
      options: question.options
        ? question.options.map((score) => ({
            text: score.text,
            value: score.value,
            scores: score.scores ? JSON.parse(score.scores) : undefined,
          }))
        : undefined,
    })) as unknown as EditorQuestion[];

    filterResult = definedData.resultCategories
      ? (definedData.resultCategories.map((resultCategory) => ({
          id: resultCategory.id,
          description: resultCategory.description,
          results: resultCategory.results.map((result) => ({
            id: result.id,
            text: result.text,
            expression: result.expression
              ? JSON.parse(result.expression)
              : undefined,
          })),
        })) as unknown as EditorResultCategory[])
      : undefined;

    filterVariable = definedData.variables
      ? (definedData.variables.map((variable) => ({
          id: variable.id,
          expression: variable.expression
            ? JSON.parse(variable.expression)
            : undefined,
        })) as unknown as EditorVariable[])
      : undefined;
  }

  let loadQuestionnaire: EditorQuestionnaire | undefined = undefined;

  if (mode === "edit") {
    loadQuestionnaire = {
      id: definedData.id,
      language: definedData.language,
      title: definedData.title,
      meta: definedData.meta,
      schemaVersion: definedData.schemaVersion,
      version: definedData.version,
      questions: filterQuestionnaire,
      resultCategories: filterResult,
      variables: filterVariable,
    } as unknown as EditorQuestionnaire;
  }

  const questionnaireJson = useSelector((state: IRootState) =>
    removeStringRepresentationFromQuestionnaire(
      state.questionReducer.questionnaire
    )
  );

  useEffect(() => {
    dispatch(
      setQuestionnaireInEditor(
        mode === "edit"
          ? loadQuestionnaire
          : {
              id: "",
              schemaVersion: "",
              version: 1,
              language: "none",
              title: "",
              meta: {
                author: "",
                creationDate: "",
                availableLanguages: [],
              },
              questions: [],
              resultCategories: [],
              variables: [],
            }
      )
    );
  }, []);

  const questionnaire = useSelector(
    (state: IRootState) => state.questionReducer.questionnaire
  );

  const currentQuestionnaire = useSelector(
    (state: IRootState) => state.questionReducer
  );

  const [currentTitle, setCurrentTitle] = useState<string | undefined>(
    undefined
  );

  const hasAnyError = useSelector((state: IRootState) => {
    const questionnaireErrors = state.questionReducer.errors;
    const currentErrorValues: boolean[] = [];
    currentErrorValues.push(questionnaireErrors.isJsonModeError);
    currentErrorValues.push(questionnaireErrors.formEditor.meta);

    const sections = [
      questionnaireErrors.formEditor.questions,
      questionnaireErrors.formEditor.resultCategories,
      questionnaireErrors.formEditor.variables,
      questionnaireErrors.formEditor.testCases,
    ];
    for (const section of sections) {
      for (const propertyName of Object.getOwnPropertyNames(section)) {
        currentErrorValues.push(section[parseInt(propertyName)]);
      }
    }

    return currentErrorValues.filter((it) => it).length > 0;
  });

  const downloadJson = () => {
    if (mode === "edit") {
      if (loadQuestionnaire === undefined) {
        return;
      }
      // after https://stackoverflow.com/questions/44656610/download-a-string-as-txt-file-in-react/44661948
      const linkElement = document.createElement("a");
      const jsonFile = new Blob([JSON.stringify(loadQuestionnaire, null, 2)], {
        type: "text/plain",
      });
      linkElement.href = URL.createObjectURL(jsonFile);
      linkElement.download = loadQuestionnaire.id + ".json";
      document.body.appendChild(linkElement);
      linkElement.click();
    }
    if (questionnaireJson === undefined) {
      return;
    }
    // after https://stackoverflow.com/questions/44656610/download-a-string-as-txt-file-in-react/44661948
    const linkElement = document.createElement("a");
    const jsonFile = new Blob([JSON.stringify(questionnaireJson, null, 2)], {
      type: "text/plain",
    });
    linkElement.href = URL.createObjectURL(jsonFile);
    linkElement.download = questionnaireJson.id + ".json";
    document.body.appendChild(linkElement);
    linkElement.click();
  };

  useEffect(() => {
    if (
      currentQuestionnaire === undefined ||
      currentQuestionnaire.questionnaire === undefined ||
      currentQuestionnaire.questionnaire.title === ""
    ) {
      setCurrentTitle(undefined);
    } else {
      setCurrentTitle(currentQuestionnaire.questionnaire.title);
    }

    if (!hasAnyError) {
      setExecutedQuestionnaire(currentQuestionnaire.questionnaire);
    }
  }, [currentQuestionnaire, hasAnyError]);

  const questionnaireInEditor = useSelector(
    (state: IRootState) => state.questionReducer
  );
  const formErrors = useSelector(
    (state: IRootState) => state.questionReducer.errors.formEditor
  );

  const duplicatedIds = useSelector((state: IRootState) => {
    const questionnaire = state.questionReducer.questionnaire;
    const ids: string[] = [];
    if (questionnaire.questions !== undefined) {
      ids.push(...questionnaire.questions.map((it) => it.id));
    }
    if (questionnaire.resultCategories !== undefined) {
      for (const resultCategory of questionnaire.resultCategories) {
        ids.push(resultCategory.id);
        if (resultCategory.results !== undefined) {
          ids.push(...resultCategory.results.map((it) => it.id));
        }
      }
    }
    if (questionnaire.variables !== undefined) {
      ids.push(...questionnaire.variables.map((it) => it.id));
    }
    return ids.filter(
      (item, index) => item !== undefined && ids.indexOf(item) !== index
    );
  });

  const useStyles = makeStyles(() =>
    createStyles({
      selectionList: {
        width: "100%",
      },
      selectionListDivider: {
        width: "100%",
      },
      selection: {
        padding: "10px 20px 0 10px",
        overflowY: "auto",
        overflowX: "hidden",
        display: "flex",
        alignItems: "flex-start",
        justifyContent: "flex-start",
        flexDirection: "column",
      },
      listItem: {
        paddingLeft: 0,
        paddingRight: 0,
        paddingTop: "2px",
        paddingBottom: "2px",
      },
      formContainer: {
        padding: "10px 10px 0 10px",
        "overflow-y": "auto",
        "overflow-x": "hidden",
        minHeight: "100vh",
      },
      alignRight: {
        display: "flex",
        justifyContent: "flex-end",
        width: "100%",
      },
      sectionHeader: {
        alignItems: "center",
        display: "inline-flex",
        width: "100%",
      },
      title: {
        fontWeight: "bold",
        textTransform: "capitalize",
      },
      listItemText: {
        fontSize: "0.9rem",
        lineHeight: "1.25rem",
        paddingLeft: 10,
      },
      addButton: {
        marginLeft: "auto",
      },
    })
  );

  const classes = useStyles();

  const [activeItem, setActiveItem] = useState<ActiveItem | undefined>({
    section: SectionTypeSingle.META,
    index: 0,
  });

  useEffect(() => {
    if (activeItem === undefined || isNonArraySection(activeItem.section)) {
      return;
    }

    const lengthOfActiveSection =
      questionnaireInEditor.questionnaire[activeItem.section]?.length;
    if (lengthOfActiveSection === undefined || lengthOfActiveSection === 0) {
      handleActiveItemChange(SectionTypeSingle.META, 0);
      return;
    }
    if (activeItem.index >= lengthOfActiveSection) {
      handleActiveItemChange(activeItem.section, lengthOfActiveSection - 1);
    }
  }, [activeItem]);

  if (activeItem === undefined) {
    return null;
  }

  const handleMoveUp = () => {
    if (!isNonArraySection(activeItem.section)) {
      dispatch(
        swapItemWithNextOne({
          section: activeItem.section,
          index: activeItem.index - 1,
        })
      );
      handleActiveItemChange(activeItem.section, activeItem.index - 1);
    }
  };

  const handleMoveDown = () => {
    if (!isNonArraySection(activeItem.section)) {
      dispatch(
        swapItemWithNextOne({
          section: activeItem.section,
          index: activeItem.index,
        })
      );
      handleActiveItemChange(activeItem.section, activeItem.index + 1);
    }
  };

  const handleRemove = () => {
    if (!isNonArraySection(activeItem.section)) {
      dispatch(
        removeItem({ section: activeItem.section, index: activeItem.index })
      );
    }
  };

  const handleAddItem = (
    section: SectionTypeArray,
    template: boolean = false
  ) => {
    dispatch(
      addNewItem({
        section: section,
        template: template
          ? questionnaireInEditor.questionnaire[section]?.[activeItem.index]
          : undefined,
      })
    );
    handleActiveItemChange(
      activeItem.section,
      questionnaireInEditor.questionnaire[section]?.length || 0
    );
  };

  const handleActiveItemChange = (section: SectionType, index: number) => {
    setActiveItem({ section, index });
  };

  const style = `
    .rjsf > .MuiFormControl-root {
      height: 100%;
    }
    .rjsf .MuiGrid-item {
      padding: 0px 8px 0px 8px;
    }
    .rjsf .MuiButton-textSecondary {
      color: rgba(0, 0, 0, 0.87);
    }
    .rjsf .MuiPaper-root {
      background: #F7FAFC;
    }
    `;

  const hasError = (hasError: boolean, id?: string) => {
    if (hasError) {
      return true;
    }
    if (id !== undefined) {
      return duplicatedIds.indexOf(id) > -1;
    }
    return false;
  };

  const hasResultDuplicatedId = (resultCategory: EditorResultCategory) => {
    if (resultCategory.results === undefined) {
      return false;
    }
    let isDuplicate = false;
    for (const result of resultCategory.results) {
      if (duplicatedIds.indexOf(result.id) > -1) {
        isDuplicate = true;
        break;
      }
    }
    return isDuplicate;
  };

  return (
    <>
      <Grid container direction="column" className="overflow-pass-through">
        <style>{style}</style>
        <Grid container className="overflow-pass-through flex-grow">
          <Grid
            container
            item
            direction="row"
            xs={3}
            className={`${classes.selection} overflow-pass-through`}
          >
            <div
              style={{
                width: "100%",
                paddingLeft: "5px",
                paddingTop: "5px",
              }}
            >
              <>
                <Typography className={classes.title}>General</Typography>
                <List className={classes.selectionList}>
                  <ListItem
                    className={classes.listItem}
                    button
                    selected={activeItem.section === SectionTypeSingle.META}
                    onClick={() => {
                      handleActiveItemChange(SectionTypeSingle.META, 0);
                      setPreview(false);
                    }}
                  >
                    <ListItemText
                      classes={{ primary: classes.listItemText }}
                      primary="Meta"
                    />
                    {formErrors.meta ? <WarningIcon color={"error"} /> : null}
                  </ListItem>
                </List>
              </>

              <>
                <div className={classes.sectionHeader}>
                  <Typography className={classes.title}>Questions</Typography>
                  <IconButton
                    className={classes.addButton}
                    aria-label="add-question"
                    onClick={() => {
                      handleAddItem(SectionTypeArray.QUESTIONS);
                    }}
                  >
                    <AddIcon />
                  </IconButton>
                </div>
                <List className={classes.selectionList}>
                  {questionnaireInEditor.questionnaire.questions.map(
                    (item, index) => (
                      <ListItem
                        button
                        className={classes.listItem}
                        selected={
                          activeItem.section === SectionTypeArray.QUESTIONS &&
                          activeItem.index === index
                        }
                        onClick={() => {
                          handleActiveItemChange(
                            SectionTypeArray.QUESTIONS,
                            index
                          );
                          setPreview(false);
                        }}
                        key={index}
                      >
                        <ListItemText
                          classes={{ primary: classes.listItemText }}
                          primary={item.text}
                        />
                        {hasError(formErrors.questions[index], item.id) ? (
                          <WarningIcon color={"error"} />
                        ) : null}
                      </ListItem>
                    )
                  )}
                </List>
              </>

              <>
                <div className={classes.sectionHeader}>
                  <Typography className={classes.title}>
                    Result Categories
                  </Typography>
                  <IconButton
                    className={classes.addButton}
                    aria-label="add-result-category"
                    onClick={() => {
                      handleAddItem(SectionTypeArray.RESULT_CATEGORIES);
                    }}
                  >
                    <AddIcon />
                  </IconButton>
                </div>
                <List className={classes.selectionList}>
                  {questionnaireInEditor.questionnaire.resultCategories.map(
                    (item, index) => (
                      <ListItem
                        button
                        className={classes.listItem}
                        selected={
                          activeItem.section ===
                            SectionTypeArray.RESULT_CATEGORIES &&
                          activeItem.index === index
                        }
                        onClick={() => {
                          handleActiveItemChange(
                            SectionTypeArray.RESULT_CATEGORIES,
                            index
                          );
                          setPreview(false);
                        }}
                        key={index}
                      >
                        <ListItemText
                          classes={{ primary: classes.listItemText }}
                          primary={item.id}
                        />
                        {hasError(
                          formErrors.resultCategories[index],
                          item.id
                        ) || hasResultDuplicatedId(item) ? (
                          <WarningIcon color={"error"} />
                        ) : null}
                      </ListItem>
                    )
                  )}
                </List>
              </>

              <>
                <div className={classes.sectionHeader}>
                  <Typography className={classes.title}>Variables</Typography>
                  <IconButton
                    className={classes.addButton}
                    aria-label="add-variable"
                    onClick={() => {
                      handleAddItem(SectionTypeArray.VARIABLES);
                    }}
                  >
                    <AddIcon />
                  </IconButton>
                </div>
                <List className={classes.selectionList}>
                  {questionnaireInEditor.questionnaire.variables.map(
                    (item, index) => (
                      <ListItem
                        button
                        className={classes.listItem}
                        selected={
                          activeItem.section === SectionTypeArray.VARIABLES &&
                          activeItem.index === index
                        }
                        onClick={() => {
                          handleActiveItemChange(
                            SectionTypeArray.VARIABLES,
                            index
                          );
                          setPreview(false);
                        }}
                        key={index}
                      >
                        <ListItemText
                          classes={{ primary: classes.listItemText }}
                          primary={item.id}
                        />
                        {hasError(formErrors.variables[index], item.id) ? (
                          <WarningIcon color={"error"} />
                        ) : null}
                      </ListItem>
                    )
                  )}
                </List>
              </>
              <div
                style={{
                  display: "flex",
                  gap: "10px",
                }}
              >
                <Button
                  variant="contained"
                  onClick={() => {
                    setPreview(!preview);
                  }}
                >
                  Preview
                </Button>
                <Button
                  variant="contained"
                  onClick={() => {
                    const filterQuestion = questionnaire.questions.map(
                      (question) => ({
                        id: question.id,
                        text: question.text,
                        type: question.type,
                        details: question.details ? question.details : "",
                        enableWhenExpression: question.enableWhenExpression
                          ? JSON.stringify(question.enableWhenExpression)
                          : undefined,
                        optional: question.optional ? question.optional : false,
                        numberOptions: isNumericQuestion(question)
                          ? (question as NumericQuestion).numericOptions
                          : undefined,
                        options: isQuestionWithOptions(question)
                          ? (question as QuestionWithOptions).options?.map(
                              (score) => ({
                                text: score.text,
                                value: score.value,
                                scores: score.scores
                                  ? JSON.stringify(score.scores)
                                  : undefined,
                              })
                            )
                          : undefined,
                      })
                    ) as unknown as Questions[];

                    const filterResult = questionnaire.resultCategories.map(
                      (resultCategory) => ({
                        description: resultCategory.description,
                        id: resultCategory.id,
                        results: resultCategory.results.map((result) => ({
                          id: result.id,
                          text: result.text,
                          expression: result.expression
                            ? JSON.stringify(result.expression)
                            : "",
                        })),
                      })
                    ) as unknown as ResultCategory[];

                    const filterVariable = questionnaire.variables.map(
                      (variable) => ({
                        id: variable.id,
                        expression: variable.expression
                          ? JSON.stringify(variable.expression)
                          : "",
                      })
                    ) as unknown as Variable[];

                    dispatch(
                      mode !== "edit"
                        ? createQuestionnaire({
                            questionBody: {
                              language: questionnaireJson.language,
                              meta: questionnaireJson.meta,
                              questions: filterQuestion,
                              schemaVersion: "1_2",
                              title: questionnaireJson.title,
                              version: questionnaireJson.version,
                              resultCategories: filterResult,
                              variables: filterVariable,
                            },
                          })
                        : editQuestionnaire({
                            id: String(loadQuestionnaire?.id),
                            questionBody: {
                              language: questionnaireJson.language,
                              meta: questionnaireJson.meta,
                              questions: filterQuestion,
                              schemaVersion: "1_2",
                              title: questionnaireJson.title,
                              version: questionnaireJson.version,
                              resultCategories: filterResult,
                              variables: filterVariable,
                            },
                          })
                    );
                    navigate("/admin/questions");
                  }}
                >
                  {mode === "edit" ? "Edit" : "Save"}
                </Button>
              </div>
            </div>
          </Grid>

          <Grid
            container
            direction="column"
            item
            xs={9}
            className={`${classes.formContainer} overflow-pass-through`}
          >
            {!isNonArraySection(activeItem.section) ? (
              <div className={classes.alignRight}>
                <IconButton
                  aria-label="move-up"
                  disabled={activeItem.index <= 0}
                  onClick={handleMoveUp}
                >
                  <ArrowUpwardIcon />
                </IconButton>
                <IconButton
                  aria-label="move-down"
                  disabled={
                    activeItem.index >=
                    (questionnaireInEditor.questionnaire[activeItem.section]
                      ?.length ?? 0) -
                      1
                  }
                  onClick={handleMoveDown}
                >
                  <ArrowDownwardIcon />
                </IconButton>
                <IconButton aria-label="remove" onClick={handleRemove}>
                  <DeleteIcon />
                </IconButton>
                <IconButton
                  aria-label="copy"
                  onClick={() => {
                    if (!isNonArraySection(activeItem.section))
                      handleAddItem(activeItem.section, true);
                  }}
                >
                  <FileCopy />
                </IconButton>
              </div>
            ) : null}

            {preview ? (
              <Grid
                item
                container
                xs={9}
                data-testid="QuestionnaireExecution"
                className={`flex-grow overflow-pass-through`}
              >
                {executedQuestionnaire !== undefined ? (
                  <QuestionnaireExecution
                    currentQuestionnaire={executedQuestionnaire}
                    isJsonInvalid={hasAnyError || duplicatedIds.length > 0}
                    downloadJson={downloadJson}
                  />
                ) : null}
              </Grid>
            ) : (
              <ElementEditorSwitch activeItem={activeItem} />
            )}
          </Grid>
        </Grid>
      </Grid>
    </>
  );
}
