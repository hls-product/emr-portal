import React from "react";
import { TestCaseResult } from "./TestCaseResult";
import { useSelector } from "react-redux";
import { Grid } from "@mui/material";
import { IRootState } from "../../../../../store/reducers";
import { removeStringRepresentationFromQuestionnaire } from "../../../converters";

export enum ALL_TEST_CASES_RUN {
  ALL = 0,
  MANUALLY = 1,
}
type AllTestCaseViewProps = {
  index: ALL_TEST_CASES_RUN;
};

export const AllTestCaseView: React.FC<AllTestCaseViewProps> = (props) => {
  const questionnaireJson = useSelector((state: IRootState) =>
    removeStringRepresentationFromQuestionnaire(
      state.questionReducer.questionnaire
    )
  );

  return (
    <Grid container>
      {(questionnaireJson.testCases ?? []).map((testCase) => (
        <Grid item sm={12}>
          <TestCaseResult testCase={testCase} runManually={props.index === 1} />
        </Grid>
      ))}
    </Grid>
  );
};
