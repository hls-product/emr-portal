import { ElementEditor } from "./ElementEditor";
import questionnaireMetaSchema from "../schemas/questionnaireMeta.json";
import React from "react";
import { useAppDispatch } from "../../../../store/Store";
import { useSelector } from "react-redux";
import { editMeta } from "../../../../store/reducers/question/questionSlice";
import { addRootPropertiesToMetaObject } from "../../converters";
import { IRootState } from "../../../../store/reducers";
import { CustomTextField, CustomSelect } from "../../../questionnaire";
import "./style.css";

const uiSchema = {
  "ui:order": ["title", "*"],
  creationDate: {
    "ui:widget": "date",
  },
  experiationDate: {
    "ui:widget": "date",
  },
  title: {
    "ui:widget": CustomTextField,
    label: false,
  },
  author: {
    "ui:widget": CustomTextField,
    label: false,
  },
  language: {
    "ui:widget": CustomSelect,
    label: false,
  },
  description: {
    "ui:widget": CustomTextField,
    label: false,
  },
  publisher: {
    "ui:widget": CustomTextField,
    label: false,
  },
};

export function MetaElementEditor() {
  const dispatch = useAppDispatch();

  const { questionnaire } = useSelector(
    (state: IRootState) => state.questionReducer
  );

  return (
    <ElementEditor
      id="editor-meta"
      className="form-wrapper"
      schema={questionnaireMetaSchema as any}
      formData={addRootPropertiesToMetaObject(questionnaire)}
      onChange={(formData, hasErrors) => {
        dispatch(editMeta({ changedMeta: formData, hasErrors }));
      }}
      uiSchema={uiSchema}
      addAdditionalValidationErrors={() => {}}
    />
  );
}
