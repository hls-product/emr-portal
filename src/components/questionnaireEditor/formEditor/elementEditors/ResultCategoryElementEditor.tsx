import { ElementEditor } from "./ElementEditor";
import React from "react";
import resultCategorySchema from "../schemas/resultCategory.json";
import {
  EditorResult,
  EditorResultCategory,
} from "../../../../models/question";
import { convertStringToLogicExpression } from "../../converters";
import { useAppDispatch } from "../../../../store/Store";
import { IRootState } from "../../../../store/reducers";
import { useSelector } from "react-redux";
import { editResultCategory } from "../../../../store/reducers/question/questionSlice";
import { uiSchemaLogic, uiSchemaLogicReadOnly } from "../schemas/uiSchemaLogic";
import {
  CustomScoreSelect,
  CustomTextField,
  CustomSelect,
} from "../../../questionnaire";
import "./style.css";

type ResultInStringRepresentation = Omit<EditorResult, "expression"> & {
  expression: string;
};
export type ResultCategoryInStringRepresentation = Omit<
  EditorResultCategory,
  "results"
> & {
  results: ResultInStringRepresentation[];
};

type ResultElementEditorProps = {
  index: number;
};

const uiSchema = {
  "ui:order": ["", "", "*"],
  results: {
    items: {
      "ui:order": ["id", "text", "expressionString", "*"],
      text: uiSchemaLogic(),
      expression: uiSchemaLogicReadOnly(),
      expressionString: uiSchemaLogic(),
    },
  },
  id: {
    "ui:widget": CustomTextField,
    label: false,
  },
  description: {
    "ui:widget": CustomTextField,
    label: false,
  },
};

function convertToStringRepresentation(
  formData: EditorResultCategory
): ResultCategoryInStringRepresentation {
  return {
    ...formData,
    results: formData?.results?.map((result) => {
      let expression = JSON.stringify(result.expression, null, 2);
      return { ...result, expression };
    }),
  };
}

export function ResultCategoryElementEditor(props: ResultElementEditorProps) {
  const dispatch = useAppDispatch();

  const resultCategory = useSelector(
    (state: IRootState) =>
      state.questionReducer.questionnaire.resultCategories[props.index]
  );
  const duplicatedIds = useSelector((state: IRootState) => {
    const questionnaire = state.questionReducer.questionnaire;
    const ids: string[] = [];
    if (questionnaire.questions !== undefined) {
      ids.push(...questionnaire.questions.map((it) => it.id));
    }
    if (questionnaire.resultCategories !== undefined) {
      for (const resultCategory of questionnaire.resultCategories) {
        ids.push(resultCategory.id);
        if (resultCategory.results !== undefined) {
          ids.push(...resultCategory.results.map((it) => it.id));
        }
      }
    }
    if (questionnaire.variables !== undefined) {
      ids.push(...questionnaire.variables.map((it) => it.id));
    }
    return ids.filter(
      (item, index) => item !== undefined && ids.indexOf(item) !== index
    );
  });

  const onChange = (
    formData: ResultCategoryInStringRepresentation,
    hasErrors: boolean
  ) => {
    dispatch(
      editResultCategory({
        index: props.index,
        changedResultCategory: formData,
        hasErrors: hasErrors,
      })
    );
  };

  const validate = (
    formData: ResultCategoryInStringRepresentation,
    errors: any
  ) => {
    if (duplicatedIds.indexOf(formData.id) > -1) {
      errors.id.addError("Value of ID is duplicated");
    }

    if (formData.results === undefined) {
      return;
    }
    for (let i = 0; i < formData.results.length; i++) {
      const result = formData.results[i];
      try {
        convertStringToLogicExpression(result.expressionString);
      } catch (error: any) {
        errors.results[i].expressionString.addError(error.message);
      }

      if (duplicatedIds.indexOf(result.id) > -1) {
        errors.results[i].id.addError("Value of ID is duplicated");
      }
    }
  };

  return (
    <ElementEditor
      className="form-wrapper"
      id={`editor-result-category-${props.index}`}
      schema={resultCategorySchema as any}
      formData={convertToStringRepresentation(resultCategory)}
      onChange={onChange}
      uiSchema={uiSchema}
      addAdditionalValidationErrors={validate}
    />
  );
}
