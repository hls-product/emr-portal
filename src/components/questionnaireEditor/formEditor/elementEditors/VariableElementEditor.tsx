import { EditorVariable } from "../../../../models/question";
import { ElementEditor } from "./ElementEditor";
import React from "react";
import variableSchema from "../schemas/variable.json";
import { convertStringToLogicExpression } from "../../converters";
import { IRootState } from "../../../../store/reducers";
import { useAppDispatch } from "../../../../store/Store";
import { useSelector } from "react-redux";
import { editVariable } from "../../../../store/reducers/question/questionSlice";
import { uiSchemaLogic, uiSchemaLogicReadOnly } from "../schemas/uiSchemaLogic";
import { CustomTextField } from "../../../questionnaire/CustomTextField";
import "./style.css";

export type VariableInStringRepresentation = Omit<
  EditorVariable,
  "expression"
> & { expression: string };

type VariableElementEditorProps = {
  index: number;
};

const uiSchema = {
  "ui:order": ["id", "expressionString", "*"],
  expression: uiSchemaLogicReadOnly(),
  expressionString: uiSchemaLogic(),
  id: {
    "ui:widget": CustomTextField,
    label: false,
  },
};

function convertToStringRepresentation(
  formData: EditorVariable
): VariableInStringRepresentation {
  return {
    ...formData,
    expression: JSON.stringify(formData?.expression, null, 2),
  };
}

export function VariableElementEditor(props: VariableElementEditorProps) {
  const dispatch = useAppDispatch();

  const variable = useSelector(
    (state: IRootState) =>
      state.questionReducer.questionnaire.variables[props.index]
  );
  const duplicatedIds = useSelector((state: IRootState) => {
    const questionnaire = state.questionReducer.questionnaire;
    const ids: string[] = [];
    if (questionnaire.questions !== undefined) {
      ids.push(...questionnaire.questions.map((it) => it.id));
    }
    if (questionnaire.resultCategories !== undefined) {
      for (const resultCategory of questionnaire.resultCategories) {
        ids.push(resultCategory.id);
        if (resultCategory.results !== undefined) {
          ids.push(...resultCategory.results.map((it) => it.id));
        }
      }
    }
    if (questionnaire.variables !== undefined) {
      ids.push(...questionnaire.variables.map((it) => it.id));
    }
    return ids.filter(
      (item, index) => item !== undefined && ids.indexOf(item) !== index
    );
  });
  const onChange = (
    formData: VariableInStringRepresentation,
    hasErrors: boolean
  ) => {
    dispatch(
      editVariable({
        index: props.index,
        changedVariable: formData,
        hasErrors: hasErrors,
      })
    );
  };

  const onValidate = (
    formData: VariableInStringRepresentation,
    errors: any
  ) => {
    try {
      convertStringToLogicExpression(formData?.expressionString);
    } catch (error: any) {
      errors.expressionString.addError(error.message);
    }

    if (duplicatedIds.indexOf(formData.id) > -1) {
      errors.id.addError("Value of ID is duplicated");
    }
  };

  return (
    <ElementEditor
      className="form-wrapper"
      id={`editor-variable-${props.index}`}
      schema={variableSchema as any}
      formData={convertToStringRepresentation(variable)}
      onChange={onChange}
      uiSchema={uiSchema}
      addAdditionalValidationErrors={onValidate}
    />
  );
}
