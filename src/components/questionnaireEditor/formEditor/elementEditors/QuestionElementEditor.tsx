import { EditorQuestion } from "../../../../models/question";
import { ElementEditor } from "./ElementEditor";
import questionSchema from "../schemas/question.json";
import React from "react";
import { convertStringToLogicExpression } from "../../converters";
import { editQuestion } from "../../../../store/reducers/question/questionSlice";
import { useAppDispatch } from "../../../../store/Store";
import { IRootState } from "../../../../store/reducers";
import { useSelector } from "react-redux";
import { uiSchemaLogic, uiSchemaLogicReadOnly } from "../schemas/uiSchemaLogic";
import {
  CustomScoreSelect,
  CustomTextField,
  CustomSelect,
} from "../../../questionnaire";
import "./style.css";

export type QuestionInStringRepresentation = Omit<
  EditorQuestion,
  "enableWhenExpression"
> & {
  enableWhenExpression: string;
};

type QuestionElementEditorProps = {
  index: number;
};

const uiSchema = {
  "ui:order": [
    "type",
    "text",
    "details",
    "id",
    "optional",
    "enableWhenExpressionString",
    "*",
  ],
  enableWhenExpression: uiSchemaLogicReadOnly(),
  enableWhenExpressionString: uiSchemaLogic(),
  options: {
    items: {
      "ui:order": ["text", "enableWhenExpressionString", "*"],
    },
    // "ui:widget": CustomScoreSelect,
    // label: false,
  },
  details: {
    "ui:widget": CustomTextField,
    label: false,
  },
  id: {
    "ui:widget": CustomTextField,
    label: false,
  },
  text: {
    "ui:widget": CustomTextField,
    label: false,
  },
  type: {
    "ui:widget": CustomSelect,
    label: false,
  },
  // scores: {
  //   "ui:widget": CustomScoreSelect,
  //   label: false,
  // },
};

function convertToStringRepresentation(
  formData: EditorQuestion
): QuestionInStringRepresentation {
  return {
    ...formData,
    enableWhenExpression: JSON.stringify(
      formData.enableWhenExpression,
      null,
      2
    ),
  };
}

export function ElementEditorQuestion(props: QuestionElementEditorProps) {
  const dispatch = useAppDispatch();

  const question = useSelector(
    (state: IRootState) =>
      state.questionReducer.questionnaire.questions[props.index]
  );
  const duplicatedIds = useSelector((state: IRootState) => {
    const questionnaire = state.questionReducer.questionnaire;
    const ids: string[] = [];
    if (questionnaire.questions !== undefined) {
      ids.push(...questionnaire.questions.map((it) => it.id));
    }
    if (questionnaire.resultCategories !== undefined) {
      for (const resultCategory of questionnaire.resultCategories) {
        ids.push(resultCategory.id);
        if (resultCategory.results !== undefined) {
          ids.push(...resultCategory.results.map((it) => it.id));
        }
      }
    }
    if (questionnaire.variables !== undefined) {
      ids.push(...questionnaire.variables.map((it) => it.id));
    }
    return ids.filter(
      (item, index) => item !== undefined && ids.indexOf(item) !== index
    );
  });

  const onChange = (
    formData: QuestionInStringRepresentation,
    hasErrors: boolean
  ) => {
    dispatch(
      editQuestion({
        index: props.index,
        changedQuestion: formData,
        hasErrors: hasErrors,
      })
    );
  };

  const validate = (formData: QuestionInStringRepresentation, errors: any) => {
    try {
      convertStringToLogicExpression(formData.enableWhenExpressionString);
    } catch (error: any) {
      errors.enableWhenExpressionString.addError(error.message);
    }
    if (duplicatedIds.indexOf(formData.id) > -1) {
      errors.id.addError("Value of ID is duplicated");
    }
  };

  if (question === undefined) {
    return null;
  }

  return (
    <ElementEditor
      className="form-wrapper"
      id={`editor-question-${props.index}`}
      schema={questionSchema as any}
      formData={convertToStringRepresentation(question)}
      onChange={onChange}
      uiSchema={uiSchema}
      addAdditionalValidationErrors={validate}
    />
  );
}
