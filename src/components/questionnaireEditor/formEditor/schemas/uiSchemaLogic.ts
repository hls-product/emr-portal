import { CustomTextArea } from "../../../questionnaire/CustomTextArea";

export const uiSchemaLogic = () => ({
  "ui:widget": CustomTextArea,
});

export const uiSchemaLogicReadOnly = () => ({
  ...uiSchemaLogic(),
  "ui:readonly": true,
});
