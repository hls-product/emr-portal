import * as React from "react";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";

interface Props {
  children: {
    element: React.ReactNode;
    title: string;
  }[];
  defaultValue: number;
}

interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}
function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box
          sx={{
            paddingTop: "16px",
          }}
        >
          {children}
        </Box>
      )}
    </div>
  );
}

function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
    sx: {
      color: "black",
      borderRadius: "8px",
      "&.Mui-selected": {
        background: "#6C5DD3",
        color: "#ffffff",
        border: "none",
        transition: "ease 300ms"
      },
    },
  };
}
export const Toolbar: React.FC<Props> = ({ children, defaultValue }) => {
  const [value, setValue] = React.useState(defaultValue);

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  return (
    <Box sx={{ width: "100%" }}>
      <Box >
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="basic tabs example"
          sx={{
            background: "#ffffff",
            padding: "12px",
            borderRadius: "16px",
            "& .MuiTabs-indicator": { background: "none" },
          }}
        >
          {children.map((item, index) => (
            <Tab key={item.title} label={item.title} {...a11yProps(index)} />
          ))}
        </Tabs>
      </Box>
      {children.map((item, index) => (
        <TabPanel
          key={index}
          value={value}
          index={index}
          children={item.element}
        />
      ))}
    </Box>
  );
};
