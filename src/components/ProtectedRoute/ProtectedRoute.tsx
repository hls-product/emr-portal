/* Libs */
import React from "react";
import { useKeycloak } from "@react-keycloak/web";
import { Navigate } from "react-router-dom";
import jwt_decode from "jwt-decode";
import { ROLES } from "../../constants/Role";

const ProtectedRoute: React.FC = ({ children }) => {
  const { keycloak } = useKeycloak();
  const isLoggedIn = keycloak.authenticated;
  const token = localStorage.getItem("access_token");

  return (
    <>
      {isLoggedIn ? (
        children
      ) : token ? (
        (
          jwt_decode(token as unknown as string) as unknown as {
            realm_access: {
              roles: string[];
            };
          }
        ).realm_access.roles.findIndex((role) => role === ROLES.DEFAULT) ? (
          children
        ) : (
          <Navigate to="/login" replace />
        )
      ) : (
        <Navigate to="/login" replace />
      )}
    </>
  );
};

export default ProtectedRoute;
