import Keycloak from "keycloak-js";

interface KeycloakType {
  url: string;
  realm: string;
  clientId: string;
}

export const keycloak = new Keycloak({
  url: "https://aka-keycloak.herokuapp.com/auth",
  realm: "aka-mental",
  clientId: "aka-mental-client",
} as KeycloakType);
