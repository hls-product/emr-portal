import { styled as muiStyled, Typography } from "@mui/material";

interface StyledTypographyProps {
  color: string;
  fontSize: number;
  fontWeight:
    | "100"
    | "200"
    | "300"
    | "400"
    | "500"
    | "600"
    | "700"
    | "800"
    | "900";
    padding?:string;
}

export const StyledTypography = muiStyled(Typography)<StyledTypographyProps>(
  ({ color, fontSize, fontWeight, padding }) => ({
    fontSize: `${fontSize}px`,
    fontWeight: fontWeight,
    color: color,
    padding: padding || 0
  })
);

export const StyledHeaderTypography = muiStyled(Typography)(() => ({
  fontSize: "24px",
  fontWeight: "700",
  color: "#131626",
  lineHeight: "1.5",
  marginBottom: "20px",
}));
