import { combineReducers } from "@reduxjs/toolkit";
import {
  getAllBillingReducer,
  getBillingByIdReducer,
} from "../../pages/Billing/billingSlice";
import {
  getAppointmentByIDReducer,
  getAppointmentReducer,
  postAppointmentReducer,
  updateAppointmentReducer,
} from "./appointment/appointment";
import {
  appointmentByIDReducer,
  appointmentReducer,
} from "./appointment/appointmentSlice";
import authReducer from "./authentication/authSlice";
import {
  createCareTeamReducer,
  deleteCareTeamReducer,
  getCareTeamByIdReducer,
  getCareTeamListReducer,
  updateCareTeamReducer,
} from "./careTeam/careTeamSlice";
import {
  getClaimByIDReducer,
  getClaimReducer,
  postClaimReducer,
  updateClaimReducer,
} from "./claim/claim";
import {
  getConditionByIDReducer,
  getConditionReducer,
  postConditionReducer,
  updateConditionReducer,
} from "./condition/conditionSlice";
import {
  getDiagnosticReportsByIDReducer,
  getDiagnosticReportsReducer,
  postDiagnosticReportsReducer,
  updateDiagnosticReportsReducer,
} from "./diagnosticReports/diagnosticReports";
import dialogReducer from "./dialog/dialogSlice";
import {
  getEncounterByIDReducer,
  getEncounterReducer,
  postEncounterReducer,
  updateEncounterReducer,
} from "./encounter/encounterSlice";
import { humanResourceReducer } from "./human/humanResourceSlice";
import {
  getImmunizationByIDReducer,
  getImmunizationReducer,
  postImmunizationReducer,
  updateImmunizationReducer,
} from "./immunization/immunizationSlice";
import {
  getInsuranceByIDReducer,
  getInsurancePlansReducer,
  postInsurancePlansReducer,
  updateInsuranceReducer,
} from "./insurance/insuranceSlice";
import {
  getLocationByIdReducer,
  getLocationReducer,
  patchLocationReducer,
  postLocationReducer,
} from "./locations/locationsSlice";
import { medicineReducer } from "./medicine/medicineSlice";
import notificationReducer from "./notifications/notificationSlice";
import { observationReducer } from "./observations/observationSlice";
import { organizationReducer } from "./organization/organizationSlice";
import {
  createPatientReducer,
  getPatientByIdReducer,
  updatePatientReducer,
} from "./patient/patientSlice";
import { patientRecordReducer } from "./patientRecord/patientRecordSlice";
import questionReducer from "./question/questionSlice";
import createQuestionnaireReducer from "./questionnaire/createQuestionnaireSlice";
import deleteQuestionnaireByIdReducer from "./questionnaire/deleteQuestionnaireByIdSlice";
import editQuestionnaireReducer from "./questionnaire/editQuestionnaireSlice";
import getQuestionnaireByIdReducer from "./questionnaire/getQuestionnaireByIdSlice";
import getQuestionnaireReducer from "./questionnaire/getQuestionnaireSlice";
import {
  getRelatedPatientByIDReducer,
  getRelatedPatientReducer,
  postRelatedPatientReducer,
  updateRelatedPatientReducer,
} from "./relatedPatient/relatedPatient";
import { settingReducer } from "./setup/settingSlice";

import changeViewReducer from "./changeView/changeViewSlice";
import sidebarReducer from "./sidebar/sidebarSlice";
export const rootReducer = combineReducers({
  authReducer,
  humanResourceReducer,
  createPatientReducer,
  settingReducer,
  getPatientByIdReducer,
  updatePatientReducer,
  appointmentReducer,
  getImmunizationReducer,
  postImmunizationReducer,
  updateImmunizationReducer,
  getImmunizationByIDReducer,
  getRelatedPatientReducer,
  postRelatedPatientReducer,
  getRelatedPatientByIDReducer,
  updateRelatedPatientReducer,
  appointmentByIDReducer,
  getAllBillingReducer,
  getBillingByIdReducer,
  medicineReducer,
  getInsurancePlansReducer,
  postInsurancePlansReducer,
  getInsuranceByIDReducer,
  updateInsuranceReducer,
  observationReducer,
  questionReducer,
  getDiagnosticReportsReducer,
  postDiagnosticReportsReducer,
  getDiagnosticReportsByIDReducer,
  updateDiagnosticReportsReducer,
  getEncounterReducer,
  postEncounterReducer,
  getEncounterByIDReducer,
  updateEncounterReducer,
  getConditionReducer,
  postConditionReducer,
  getConditionByIDReducer,
  updateConditionReducer,
  notificationReducer,
  getClaimReducer,
  postClaimReducer,
  updateClaimReducer,
  getClaimByIDReducer,
  patientRecordReducer,
  getLocationByIdReducer,
  getLocationReducer,
  postLocationReducer,
  patchLocationReducer,
  createQuestionnaireReducer,
  getAppointmentReducer,
  postAppointmentReducer,
  getAppointmentByIDReducer,
  updateAppointmentReducer,
  organizationReducer,
  getQuestionnaireReducer,
  getQuestionnaireByIdReducer,
  deleteQuestionnaireByIdReducer,
  dialogReducer,
  editQuestionnaireReducer,
  getCareTeamListReducer,
  getCareTeamByIdReducer,
  createCareTeamReducer,
  updateCareTeamReducer,
  deleteCareTeamReducer,
  sidebarReducer,
  changeViewReducer,
});

export type IRootState = ReturnType<typeof rootReducer>;
