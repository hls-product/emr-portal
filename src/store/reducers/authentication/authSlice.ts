import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import qs from "qs";
import { NavigateFunction, useNavigate } from "react-router-dom";
import { authenticationService } from "../../../services/agent";

interface RequestType {
  username: string;
  password: string;
  navigate: NavigateFunction;
}

interface AuthState {
  token: string;
  refresh: number;
  role: string[];
  loading: boolean;
  error: unknown;
}

interface ErrorType {
  response: {
    data: {
      error_description: string;
    };
  };
}

const initialState = {
  token: "",
  refresh: 0,
  role: [""],
  loading: false,
  error: "",
} as AuthState;

export const login = createAsyncThunk(
  "users/authentication",
  async ({ username, password, navigate }: RequestType, thunkAPI) => {
    try {
      const res = await authenticationService.login({
        username,
        password,
      });
      localStorage.setItem("access_token", res.data.access_token);
      navigate("/admin/dashboard");
      return res.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(
        (error as unknown as ErrorType).response.data.error_description
      );
    }
  }
);

const authSlice = createSlice({
  name: "authentication",
  initialState: initialState,
  reducers: {
    logout: (state, action) => {
      state.token = "";
      state.refresh = 0;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(login.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(login.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown;
    });
    builder.addCase(login.fulfilled, (state, action) => {
      state.loading = false;
      state.token = action.payload.access_token;
      state.refresh = action.payload.refresh_expires_in;
    });
  },
});

export const { logout } = authSlice.actions;

export default authSlice.reducer;
