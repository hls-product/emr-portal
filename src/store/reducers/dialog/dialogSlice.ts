import { createSlice } from "@reduxjs/toolkit";
import { DialogAction } from "../../../models/dialog/dialogAction";

export interface DataState<T> {
  name: string;
  data: T;
}

interface DialogState<T> {
  open: boolean;
  data: DataState<T>;
  full: boolean;
  maxWidth: "xs" | "sm" | "md" | "lg" | "xl" | false | true;
  dialogAction: DialogAction;
}

const initialState = {
  open: false,
  data: {
    name: "",
    data: {},
  },
  full: false,
  maxWidth: false,
  dialogAction: undefined,
} as DialogState<unknown>;

const dialogSlice = createSlice({
  name: "dialog",
  initialState: initialState,
  reducers: {
    openDialog: (state, action) => {
      state.open = action.payload.open;
      state.data.name = action.payload.data.name;
      state.data.data = action.payload.data.data;
      state.full = action.payload.full;
      state.maxWidth = action.payload.maxWidth;
      state.dialogAction = action.payload.dialogAction;
    },
    closeDialog: (state, action) => {
      state.open = action.payload.open;
      state.data.data = {};
      state.data.name = "";
      state.full = false;
      state.maxWidth = false;
    },
  },
});

export const { closeDialog, openDialog } = dialogSlice.actions;
export default dialogSlice.reducer;
