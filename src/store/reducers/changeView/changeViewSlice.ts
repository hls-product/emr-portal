import { createSlice } from "@reduxjs/toolkit";

interface ViewState<T> {
  view: "detail" | "general";
  value: T;
}

const initialState = {
  view: "general",
  value: null,
} as ViewState<unknown>;

const changeViewSlice = createSlice({
  name: "viewSlice",
  initialState: initialState,
  reducers: {
    changeView: (state, action) => {
      state.view = action.payload.view;
      state.value = action.payload.data;
    },
  },
});

export const { changeView } = changeViewSlice.actions;
export default changeViewSlice.reducer;
