import { Action, createSlice, Dispatch, PayloadAction } from "@reduxjs/toolkit";
import axios from "axios";
import { RootState } from "../../Store";
import dayjs, { Dayjs } from "dayjs";

export interface Coding {
  system?: string;
  version?: unknown;
  code?: string;
  display: string;
  userSelected?: unknown;
}

export interface CodeableConcept {
  coding: Coding[];
  text?: unknown;
}

export interface Subject {
  reference?: string;
  type?: string;
  identifier?: unknown;
  display: string;
}

export interface Period {
  start: string;
  end?: string;
}

export interface ValueQuantity {
  type?: string;
  value: number;
  comparator?: unknown;
  unit?: string;
  system?: string;
  code?: string;
}

export interface ObservationType {
  id: string;
  identifier?: unknown;
  basedOn?: unknown;
  partOf?: unknown;
  status: string;
  category: CodeableConcept[];
  code: CodeableConcept;
  subject: Subject;
  focus?: unknown;
  encounter?: unknown;
  effectivePeriod: Period;
  issued: string;
  performers?: unknown;
  value?: ValueQuantity;
  dataAbsentReason?: unknown;
  interpretation?: CodeableConcept[];
  notes?: unknown;
  bodySite?: unknown;
  method?: CodeableConcept;
  specimen?: unknown;
  device?: unknown;
  referenceRange?: unknown;
  hasMember?: unknown;
  derivedFrom?: unknown;
  components?: unknown;
}

export interface Paging {
  page: number;
  size: number;
  total: number;
}
export interface ResponseType {
  data: ObservationType[];
  paging: Paging;
}

export interface InitialState {
  loading: boolean;
  idUrl: string;
  observation: ObservationType[];
  observationEdit: ObservationType;
  observationDetail: ObservationType;
  paging: Paging;
}

const initialState: InitialState = {
  loading: false,
  idUrl: "",
  observation: [
    {
      id: "",
      status: "",
      category: [
        {
          coding: [{ display: "" }],
        },
      ],
      code: {
        coding: [{ display: "" }],
      },
      subject: { display: "" },
      effectivePeriod: { start: "" },
      issued: "",
      value: { value: 10 },
    },
  ],
  observationEdit: {
    id: "",
    status: "",
    category: [
      {
        coding: [{ display: "" }],
      },
    ],
    code: {
      coding: [{ display: "" }],
    },
    subject: { display: "" },
    effectivePeriod: { start: "" },
    issued: "",
    value: { value: 10 },
  },
  observationDetail: {
    id: "",
    status: "",
    category: [
      {
        coding: [{ display: "" }],
      },
    ],
    code: {
      coding: [{ display: "" }],
    },
    subject: { display: "" },
    effectivePeriod: { start: "" },
    issued: "",
    value: { value: 10 },
  },
  paging: {
    page: 1,
    size: 5,
    total: 10,
  },
};

const observationSlice = createSlice({
  name: "Observation",
  initialState,
  reducers: {
    loading(state) {
      state.loading = true;
    },
    loadingDone(state) {
      state.loading = false;
    },
    getIdUrlSuccess(state, action: PayloadAction<string>) {
      state.idUrl = action.payload;
    },
    getObservationSuccess(state, action: PayloadAction<ResponseType>) {
      state.observation = action.payload.data;
      state.paging = action.payload.paging;
    },
    getObservationEditSuccess(state, action: PayloadAction<ObservationType>) {
      state.observationEdit = action.payload;
    },
    getObservationDetailSuccess(state, action: PayloadAction<ObservationType>) {
      state.observationDetail = action.payload;
    },
  },
});

//actions
export const observationActions = observationSlice.actions;

export const getIdUrl = (id: string) => (dispatch: Dispatch<Action>) => {
  try {
    dispatch(observationActions.getIdUrlSuccess(id));
  } catch (error) {
    console.log(error);
  }
};

export const getObservation =
  (
    page: number,
    size: number,
    accessToken: string,
    name: string,
    status: string,
    classification: string,
    method: string,
    result: string,
    periodStart: Dayjs | null,
    periodEnd: Dayjs | null,
    issueDate: Dayjs | null
  ) =>
  async (dispatch: Dispatch<Action>) => {
    dispatch(observationActions.loading());
    try {
      const response = await axios.get(
        `https://fhir-service.herokuapp.com/api/observations?page=${page}&size=${size}&name=${name}&status=${status}&classification=${classification}&method=${method}&result=${result}&periodStart=${periodStart}&periodEnd=${periodEnd}&issue=${issueDate}`,
        {
          headers: { Authorization: "Bearer " + accessToken },
        }
      );
      dispatch(observationActions.getObservationSuccess(response.data));
    } catch (error) {
      console.log(error);
    } finally {
      dispatch(observationActions.loadingDone());
    }
  };

export const addObservation =
  (data: ObservationType, accessToken: string) =>
  async (dispatch: Dispatch<Action>) => {
    dispatch(observationActions.loading());
    try {
      await axios.post(
        "https://fhir-service.herokuapp.com/api/observations",
        data,
        {
          headers: {
            Authorization: "Bearer " + accessToken,
            ContentType: "application/json",
          },
        }
      );
    } catch (error) {
      console.log(error);
    } finally {
      dispatch(observationActions.loadingDone());
    }
  };

export const deleteObservation =
  (id: string, accessToken: string) => async (dispatch: Dispatch<Action>) => {
    dispatch(observationActions.loading());
    try {
      await axios.delete(
        `https://fhir-service.herokuapp.com/api/observations/${id}`,
        {
          headers: { Authorization: "Bearer " + accessToken },
        }
      );
    } catch (error) {
      console.log(error);
    } finally {
      dispatch(observationActions.loadingDone());
    }
  };

export const getObservationEdit =
  (item: ObservationType) => (dispatch: Dispatch<Action>) => {
    try {
      dispatch(observationActions.getObservationEditSuccess(item));
    } catch (error) {
      console.log(error);
    }
  };

export const getObservationDetail =
  (item: ObservationType) => (dispatch: Dispatch<Action>) => {
    try {
      dispatch(observationActions.getObservationDetailSuccess(item));
    } catch (error) {
      console.log(error);
    }
  };

export const updateObservation =
  (data: ObservationType, id: string, accessToken: string) =>
  async (dispatch: Dispatch<Action>) => {
    dispatch(observationActions.loading());
    try {
      await axios.patch(
        `https://fhir-service.herokuapp.com/api/observations/${id}`,
        data,
        {
          headers: {
            Authorization: `Bearer ${accessToken}`,
            ContentType: "application/json",
          },
        }
      );
    } catch (error) {
      console.log(error);
    } finally {
      dispatch(observationActions.loadingDone());
    }
  };

//selectors
export const selectLoading = (state: RootState) =>
  state.observationReducer.loading;

export const selectIdUrl = (state: RootState) => state.observationReducer.idUrl;

export const selectObservation = (state: RootState) =>
  state.observationReducer.observation;

export const selectObservationEdit = (state: RootState) =>
  state.observationReducer.observationEdit;

export const selectObservationDetail = (state: RootState) =>
  state.observationReducer.observationDetail;

export const selectPaging = (state: RootState) =>
  state.observationReducer.paging;

//reducer
export const observationReducer = observationSlice.reducer;
