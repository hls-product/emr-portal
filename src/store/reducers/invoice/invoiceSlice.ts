import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import _ from "lodash";
import {
  GetRequestType,
  PostRequestType,
  Paging,
  PatchRequestType,
} from "../../../models/shared/basePagedResponse";
import { invoiceModel } from "../../../models/invoices";
import { invoiceService } from "../../../services/agent";
//Define a type for the slice state
interface InvoiceState {
  data: invoiceModel[];
  paging: Paging;
  loading: boolean;
  error: unknown;
}
//Define the initial state using that type
const initialState: InvoiceState = {
  data: [],
  paging: {
    page: 0,
    size: 0,
    total: 0,
  },
  loading: false,
  error: "",
};

const initialStateByID = {
  data: {} as invoiceModel,
  loading: false,
  error: "",
};

export const getInvoiceData = createAsyncThunk(
  "patient/getInvoice",
  async ({ params }: GetRequestType) => {
    return await invoiceService.getList({ params });
  }
);

export const getInvoiceDataByID = createAsyncThunk(
  "patient/getInvoiceByID",
  async (filter: string) => {
    return await invoiceService.getById(filter);
  }
);

export const postInvoiceData = createAsyncThunk(
  "patient/postInvoice",
  async ({ body }: PostRequestType) => {
    return await invoiceService.post({ body });
  }
);

export const updateInvoiceData = createAsyncThunk(
  "patient/updateInvoice",
  async ({ id, body }: PatchRequestType) => {
    return await invoiceService.update({ body }, id);
  }
);

export const getInvoicePlansSlice = createSlice({
  name: "getInvoiceState",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getInvoiceData.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getInvoiceData.fulfilled, (state, action) => {
      state.loading = false;
      state.data = [...(action.payload?.data as any)];
      state.paging = _.cloneDeep(action.payload?.paging as Paging);
    });
    builder.addCase(getInvoiceData.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown;
    });
  },
});

export const getInvoicePlansByIDSlice = createSlice({
  name: "getInvoiceByIDState",
  initialState: initialStateByID,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getInvoiceDataByID.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getInvoiceDataByID.fulfilled, (state, action) => {
      state.loading = false;
      state.data = _.cloneDeep(action.payload as unknown as invoiceModel);
    });
    builder.addCase(getInvoiceDataByID.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown as string;
    });
  },
});

export const postInvoicePlansSlice = createSlice({
  name: "postInvoiceState",
  initialState: {
    data: {},
    loading: false,
    error: "",
  },
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(postInvoiceData.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(postInvoiceData.fulfilled, (state, action) => {
      state.loading = false;
      state.data = _.cloneDeep(action.payload as unknown as invoiceModel);
    });
    builder.addCase(postInvoiceData.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown as string;
    });
  },
});
export const updateInvoiceSlice = createSlice({
  name: "updateInvoice",
  initialState: {
    data: {},
    loading: false,
    error: "" as unknown,
  },
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(updateInvoiceData.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(updateInvoiceData.fulfilled, (state, action) => {
      state.loading = false;
    });
    builder.addCase(updateInvoiceData.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown;
    });
  },
});

export const getInvoicePlansReducer = getInvoicePlansSlice.reducer;
export const postInvoicePlansReducer = postInvoicePlansSlice.reducer;
export const getInvoiceByIDReducer = getInvoicePlansByIDSlice.reducer;
export const updateInvoiceReducer = updateInvoiceSlice.reducer;
