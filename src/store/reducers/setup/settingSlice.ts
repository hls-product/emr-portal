import { Alert } from "@mui/material";
import { Action, createSlice, Dispatch, PayloadAction } from "@reduxjs/toolkit";
import axios from "axios";
import { RootState } from "../../Store";

export interface DataSetup {
  id: string;
  category: string;
  dataType: string | number | boolean;
  key: string;
  value: string | number | boolean;
  jsonValue?: {
    text?: string;
  };
  override?: boolean;
  description?: string;
}

export interface InitialState {
  loading: boolean;
  dataSetup: DataSetup[];
}

const initialState: InitialState = {
  loading: false,
  dataSetup: [
    {
      id: "",
      category: "",
      dataType: "string",
      key: "",
      value: "",
      jsonValue: {
        text: "",
      },
      override: true,
      description: "",
    },
  ],
};

const settingSlice = createSlice({
  name: "setting",
  initialState,
  reducers: {
    loading(state) {
      state.loading = true;
    },
    loadingDone(state) {
      state.loading = false;
    },
    fetchDataSetupSuccess(state, action: PayloadAction<DataSetup[]>) {
      state.dataSetup = action.payload;
    },
  },
});

//actions
export const settingActions = settingSlice.actions;

export const getDataSetup =
  (accessToken: string) => async (dispatch: Dispatch<Action>) => {
    dispatch(settingActions.loading());
    try {
      const response = await axios.get(
        "https://aka-notification-service.herokuapp.com/api/system-settings?size=100",
        {
          headers: { Authorization: "Bearer " + accessToken },
        }
      );
      dispatch(settingActions.fetchDataSetupSuccess(response.data.data));
    } catch (error) {
      console.log("Failed to fetch settings data", error);
    } finally {
      dispatch(settingActions.loadingDone());
    }
  };

export const updateDataSetup =
  (accessToken: string, updateArray: DataSetup[]) =>
  async (dispatch: Dispatch<Action>) => {
    dispatch(settingActions.loading());
    try {
      updateArray.map(async (data: DataSetup) => {
        await axios.patch(
          `https://aka-notification-service.herokuapp.com/api/system-settings/${data.id}`,
          data,
          {
            headers: {
              Authorization: `Bearer ${accessToken}`,
              ContentType: "application/json",
            },
          }
        );
      });
    } catch (error) {
      console.log(error);
    } finally {
      dispatch(settingActions.loadingDone());
    }
  };

//selectors
export const selectLoading = (state: RootState) => state.settingReducer.loading;
export const selectDataSetup = (state: RootState) =>
  state.settingReducer.dataSetup;

//reducer
export const settingReducer = settingSlice.reducer;
