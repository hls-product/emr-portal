import { createSlice } from "@reduxjs/toolkit";
import { SectionTypeArray } from "../../../constants/SectionTypeArray";
import {
  addStringRepresentationToQuestionnaire,
  moveRootPropertiesToQuestionnaire,
  convertStringToLogicExpression,
} from "../../../components/questionnaireEditor/converters";
import { QuestionnaireWrapper } from "../../../models/question/QuestionnaireWrapper";
import { LogicExpression } from "@covopen/covquestions-js";
import { ResultCategoryInStringRepresentation } from "../../../models/question";

const initialState = {
  questionnaire: {
    id: "",
    schemaVersion: "",
    version: 1,
    language: "none",
    title: "",
    meta: {
      author: "",
      creationDate: "",
      availableLanguages: [],
    },
    questions: [],
    resultCategories: [],
    variables: [],
  },
  errors: {
    isJsonModeError: false,
    formEditor: {
      meta: false,
      questions: {},
      resultCategories: {},
      variables: {},
      testCases: {},
    },
  },
} as QuestionnaireWrapper;

const questionSlice = createSlice({
  name: "question",
  initialState: initialState,
  reducers: {
    setQuestionnaireInEditor: (state, action) => {
      state.questionnaire = addStringRepresentationToQuestionnaire(
        action.payload
      );
    },
    setHasErrorsInJsonMode: (state, action) => {
      state.errors.isJsonModeError = action.payload;
      state.errors.formEditor = {
        meta: false,
        questions: {},
        resultCategories: {},
        variables: {},
        testCases: {},
      };
    },
    addNewItem: (state, { payload: { section, template = {} } }) => {
      let item = {};
      switch (section) {
        case SectionTypeArray.QUESTIONS:
          item = {
            type: "boolean",
            ...template,
            id: "new_question_id",
            text: template.text ? "Copy of: " + template.text : "new question",
          };
          break;
        case SectionTypeArray.RESULT_CATEGORIES:
          item = {
            description: "",
            results: [],
            ...template,
            id: "new_result_category_id",
          };
          break;
        case SectionTypeArray.VARIABLES:
          item = {
            expression: "",
            ...template,
            id: "new_variable_id",
          };
          break;
        case SectionTypeArray.TEST_CASES:
          item = {
            answers: {},
            results: {},
            ...template,
            description: template.description
              ? "Copy of: " + template.description
              : "Description of the new test case",
          };
          break;
      }
      if (
        state.questionnaire[section as unknown as SectionTypeArray] ===
        undefined
      ) {
        state.questionnaire[section as unknown as SectionTypeArray] = [];
      }
      (state.questionnaire as any)[section as unknown as SectionTypeArray].push(
        item
      );
    },
    removeItem: (state, { payload: { index, section } }) => {
      const questionnaireElement =
        state.questionnaire[section as unknown as SectionTypeArray];
      if (questionnaireElement !== undefined) {
        questionnaireElement.splice(index, 1);
        if (
          state.errors.formEditor[section as unknown as SectionTypeArray][
            index
          ] !== undefined
        ) {
          delete state.errors.formEditor[
            section as unknown as SectionTypeArray
          ][index];
        }
      }
    },
    swapItemWithNextOne: (state, { payload: { index, section } }) => {
      const questionnaireElement =
        state.questionnaire[section as unknown as SectionTypeArray];
      if (questionnaireElement !== undefined) {
        const tmp = questionnaireElement[index];
        questionnaireElement[index] = questionnaireElement[index + 1];
        questionnaireElement[index + 1] = tmp;
        const tmpError =
          state.errors.formEditor[section as unknown as SectionTypeArray][
            index
          ] ?? false;
        state.errors.formEditor[section as unknown as SectionTypeArray][index] =
          state.errors.formEditor[section as unknown as SectionTypeArray][
            index + 1
          ] ?? false;
        state.errors.formEditor[section as unknown as SectionTypeArray][
          index + 1
        ] = tmpError;
      }
    },
    editMeta: (state, { payload: { changedMeta, hasErrors } }) => {
      state.questionnaire = moveRootPropertiesToQuestionnaire(
        state.questionnaire,
        changedMeta
      );
      state.errors.formEditor.meta = hasErrors;
    },
    editQuestion: (
      state,
      { payload: { index, changedQuestion, hasErrors } }
    ) => {
      let expression: LogicExpression | undefined = undefined;
      try {
        expression = convertStringToLogicExpression(
          changedQuestion.enableWhenExpressionString
        );
      } catch {}
      state.questionnaire.questions[index] = {
        ...changedQuestion,
        enableWhenExpression: expression,
      };
      state.errors.formEditor.questions[index] = hasErrors;
    },
    editResultCategory: (
      state,
      { payload: { index, changedResultCategory, hasErrors } }
    ) => {
      state.questionnaire.resultCategories[index] = {
        ...changedResultCategory,
        results: (
          changedResultCategory as unknown as ResultCategoryInStringRepresentation
        ).results.map((result) => {
          let expression: LogicExpression = "";
          try {
            expression =
              convertStringToLogicExpression(result.expressionString) || "";
          } catch {}
          return {
            ...result,
            expression,
          };
        }),
      };
      state.errors.formEditor.resultCategories[index] = hasErrors;
    },
    editVariable: (
      state,
      { payload: { index, changedVariable, hasErrors } }
    ) => {
      let expression: LogicExpression = "";
      try {
        expression =
          convertStringToLogicExpression(changedVariable.expressionString) ||
          "";
      } catch {}
      state.questionnaire.variables[index] = {
        ...changedVariable,
        expression,
      };
      state.errors.formEditor.resultCategories[index] = hasErrors;
    },
    editTestCase: (
      state,
      { payload: { index, changedTestCase, hasErrors } }
    ) => {
      if (!state.questionnaire.testCases) {
        state.questionnaire.testCases = [];
      }
      state.questionnaire.testCases[index] = changedTestCase;
      state.errors.formEditor.testCases[index] = hasErrors;
    },
  },
});

export const {
  addNewItem,
  editMeta,
  editQuestion,
  editResultCategory,
  editTestCase,
  editVariable,
  removeItem,
  setHasErrorsInJsonMode,
  setQuestionnaireInEditor,
  swapItemWithNextOne,
} = questionSlice.actions;

export default questionSlice.reducer;
