import { createSlice } from "@reduxjs/toolkit";
import { Notification } from "../../../models/notifications";

const initialState = {
  message: "",
  type: null,
} as Notification;

const notificationSlice = createSlice({
  name: "notifications",
  initialState: initialState,
  reducers: {
    addNotification: (state, action) => {
      state.message = (action.payload as unknown as Notification).message;
      state.type = (action.payload as unknown as Notification).type;
    },
    removeNotification: (state, action) => {
      state.message = (action.payload as unknown as Notification).message;
      state.type = null;
    },
  },
});

export const { addNotification, removeNotification } =
  notificationSlice.actions;
export default notificationSlice.reducer;
