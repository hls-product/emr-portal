import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import _ from "lodash";
import { claimsModel } from "../../../models/claims";
import {
  GetRequestType,
  PostRequestType,
  Paging,
  PatchRequestType,
} from "../../../models/shared/basePagedResponse";
import { claimService } from "../../../services/agent";
//Define a type for the slice state
interface ClaimState {
  data: claimsModel[];
  paging: Paging;
  loading: boolean;
  error: unknown;
}
//Define the initial state using that type
const initialState: ClaimState = {
  data: [],
  paging: {
    page: 0,
    size: 0,
    total: 0,
  },
  loading: false,
  error: "",
};

const initialStateByID = {
  data: {} as claimsModel,
  loading: false,
  error: "",
};

export const getClaimData = createAsyncThunk(
  "patient/getClaim",
  async ({ params }: GetRequestType) => {
    return await claimService.getList({ params });
  }
);

export const getClaimDataByID = createAsyncThunk(
  "patient/getClaimByID",
  async (filter: string) => {
    return await claimService.getById(filter);
  }
);

export const postClaimData = createAsyncThunk(
  "patient/postClaim",
  async ({ body }: PostRequestType) => {
    return await claimService.post({ body });
  }
);

export const updateClaimData = createAsyncThunk(
  "patient/updateClaim",
  async ({ id, body }: PatchRequestType) => {
    return await claimService.update({ body }, id);
  }
);

export const getClaimSlice = createSlice({
  name: "getClaimState",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getClaimData.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getClaimData.fulfilled, (state, action) => {
      state.loading = false;
      state.data = [...(action.payload?.data as any)];
      state.paging = _.cloneDeep(action.payload?.paging as Paging);
    });
    builder.addCase(getClaimData.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown;
    });
  },
});

export const getClaimByIDSlice = createSlice({
  name: "getClaimByIDState",
  initialState: initialStateByID,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getClaimDataByID.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getClaimDataByID.fulfilled, (state, action) => {
      state.loading = false;
      state.data = _.cloneDeep(action.payload as unknown as claimsModel);
    });
    builder.addCase(getClaimDataByID.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown as string;
    });
  },
});

export const postClaimSlice = createSlice({
  name: "postClaimState",
  initialState: {
    data: {},
    loading: false,
    error: "",
  },
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(postClaimData.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(postClaimData.fulfilled, (state, action) => {
      state.loading = false;
      state.data = _.cloneDeep(action.payload as unknown as claimsModel);
    });
    builder.addCase(postClaimData.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown as string;
    });
  },
});
export const updateClaimSlice = createSlice({
  name: "updateClaim",
  initialState: {
    data: {},
    loading: false,
    error: "" as unknown,
  },
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(updateClaimData.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(updateClaimData.fulfilled, (state, action) => {
      state.loading = false;
    });
    builder.addCase(updateClaimData.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown;
    });
  },
});

export const getClaimReducer = getClaimSlice.reducer;
export const postClaimReducer = postClaimSlice.reducer;
export const getClaimByIDReducer = getClaimByIDSlice.reducer;
export const updateClaimReducer = updateClaimSlice.reducer;
