import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import _ from "lodash";
import {
  GetRequestType,
  PostRequestType,
  Paging,
  PatchRequestType,
  DeleteRequestType,
} from "../../../models/shared/basePagedResponse";
import { insuranceModel } from "../../../models/insurance";
import { insuranceService } from "../../../services/agent";
//Define a type for the slice state
interface InsuranceState {
  data: insuranceModel[];
  paging: Paging;
  loading: boolean;
  error: unknown;
}
//Define the initial state using that type
const initialState: InsuranceState = {
  data: [],
  paging: {
    page: 0,
    size: 0,
    total: 0,
  },
  loading: false,
  error: "",
};

const initialStateByID = {
  data: {} as insuranceModel,
  loading: false,
  error: "",
};

export const getInsuranceData = createAsyncThunk(
  "patient/getInsurance",
  async ({ params }: GetRequestType) => {
    return await insuranceService.getList({ params });
  }
);

export const getInsuranceDataByID = createAsyncThunk(
  "patient/getInsuranceByID",
  async (filter: string) => {
    return await insuranceService.getById(filter);
  }
);

export const postInsuranceData = createAsyncThunk(
  "patient/postInsurance",
  async ({ body }: PostRequestType) => {
    return await insuranceService.post({ body });
  }
);

export const updateInsuranceData = createAsyncThunk(
  "patient/updateInsurance",
  async ({ id, body }: PatchRequestType) => {
    return await insuranceService.update({ body }, id);
  }
);

export const deleteInsuranceData = createAsyncThunk(
  "patient/deleteInsuranceByID",
  async ({ id }: DeleteRequestType) => {
    return await insuranceService.delete(id);
  }
);

export const getInsurancePlansSlice = createSlice({
  name: "getInsuranceState",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getInsuranceData.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getInsuranceData.fulfilled, (state, action) => {
      state.loading = false;
      state.data = [...(action.payload?.data as any)];
      state.paging = _.cloneDeep(action.payload?.paging as Paging);
    });
    builder.addCase(getInsuranceData.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown;
    });
  },
});

export const getInsurancePlansByIDSlice = createSlice({
  name: "getInsuranceByIDState",
  initialState: initialStateByID,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getInsuranceDataByID.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getInsuranceDataByID.fulfilled, (state, action) => {
      state.loading = false;
      state.data = _.cloneDeep(action.payload as unknown as insuranceModel);
    });
    builder.addCase(getInsuranceDataByID.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown as string;
    });
  },
});

export const postInsurancePlansSlice = createSlice({
  name: "postInsuranceState",
  initialState: {
    data: {},
    loading: false,
    error: "",
  },
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(postInsuranceData.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(postInsuranceData.fulfilled, (state, action) => {
      state.loading = false;
      state.data = _.cloneDeep(action.payload as insuranceModel);
    });
    builder.addCase(postInsuranceData.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown as string;
    });
  },
});
export const updateInsuranceSlice = createSlice({
  name: "updateInsurance",
  initialState: {
    data: {},
    loading: false,
    error: "" as unknown,
  },
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(updateInsuranceData.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(updateInsuranceData.fulfilled, (state, action) => {
      state.loading = false;
    });
    builder.addCase(updateInsuranceData.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown;
    });
  },
});

export const getInsurancePlansReducer = getInsurancePlansSlice.reducer;
export const postInsurancePlansReducer = postInsurancePlansSlice.reducer;
export const getInsuranceByIDReducer = getInsurancePlansByIDSlice.reducer;
export const updateInsuranceReducer = updateInsuranceSlice.reducer;
