import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import _ from "lodash";
import { appointmentModel } from "../../../models/appointment";
import {
  GetRequestType,
  PostRequestType,
  Paging,
  PatchRequestType,
  DeleteRequestType,
} from "../../../models/shared/basePagedResponse";
import { appointmentService } from "../../../services/agent";
//Define a type for the slice state
interface AppointmentState {
  data: appointmentModel[];
  paging: Paging;
  loading: boolean;
  error: unknown;
}
//Define the initial state using that type
const initialState: AppointmentState = {
  data: [],
  paging: {
    page: 0,
    size: 0,
    total: 0,
  },
  loading: false,
  error: "",
};

const initialStateByID = {
  data: {} as appointmentModel,
  loading: false,
  error: "",
};

export const getAppointmentData = createAsyncThunk(
  "patient/getAppointment",
  async ({ params }: GetRequestType) => {
    return await appointmentService.getList({ params });
  }
);

export const getAppointmentDataByID = createAsyncThunk(
  "patient/getAppointmentByID",
  async (filter: string) => {
    return await appointmentService.getById(filter);
  }
);

export const postAppointmentData = createAsyncThunk(
  "patient/postAppointment",
  async ({ body }: PostRequestType) => {
    return await appointmentService.post({ body });
  }
);

export const updateAppointmentData = createAsyncThunk(
  "patient/updateAppointment",
  async ({ id, body }: PatchRequestType) => {
    return await appointmentService.update({ body }, id);
  }
);

export const deleteAppointmentData = createAsyncThunk(
  "patient/deleteAppointmentByID",
  async ({ id }: DeleteRequestType) => {
    return await appointmentService.delete(id);
  }
);

export const getAppointmentSlice = createSlice({
  name: "getAppointmentState",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getAppointmentData.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getAppointmentData.fulfilled, (state, action) => {
      state.loading = false;
      state.data = [...(action.payload?.data as any)];
      state.paging = _.cloneDeep(action.payload?.paging as Paging);
    });
    builder.addCase(getAppointmentData.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown;
    });
  },
});

export const getAppointmentByIDSlice = createSlice({
  name: "getAppointmentByIDState",
  initialState: initialStateByID,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getAppointmentDataByID.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getAppointmentDataByID.fulfilled, (state, action) => {
      state.loading = false;
      state.data = _.cloneDeep(action.payload as unknown as appointmentModel);
    });
    builder.addCase(getAppointmentDataByID.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown as string;
    });
  },
});

export const postAppointmentSlice = createSlice({
  name: "postAppointmentState",
  initialState: {
    data: {},
    loading: false,
    error: "",
  },
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(postAppointmentData.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(postAppointmentData.fulfilled, (state, action) => {
      state.loading = false;
      state.data = _.cloneDeep(action.payload as unknown as appointmentModel);
    });
    builder.addCase(postAppointmentData.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown as string;
    });
  },
});
export const updateAppointmentSlice = createSlice({
  name: "updateAppointment",
  initialState: {
    data: {},
    loading: false,
    error: "" as unknown,
  },
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(updateAppointmentData.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(updateAppointmentData.fulfilled, (state, action) => {
      state.loading = false;
    });
    builder.addCase(updateAppointmentData.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown;
    });
  },
});

export const getAppointmentReducer = getAppointmentSlice.reducer;
export const postAppointmentReducer = postAppointmentSlice.reducer;
export const getAppointmentByIDReducer = getAppointmentByIDSlice.reducer;
export const updateAppointmentReducer = updateAppointmentSlice.reducer;
