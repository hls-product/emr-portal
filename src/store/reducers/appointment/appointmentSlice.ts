import {createSlice, createAsyncThunk} from '@reduxjs/toolkit'
import axios from 'axios';
import * as _ from 'lodash';

interface PostRequestType {
    endpoint: string;
    body: {
        id: string;
        category: string;
        channel: string;
        value: string;
        description: string;
    };
}

interface GetRequestType{
    endpoint: string;
    params?: object;
}

export interface Appointment{
    id: string;
    userId: string;
    status: "proposed" | "pending" | "booked" | "arrived" | "fulfilled" | "cancelled" | "noshow" | "entered-in-error" | "checked-in" | "waitlist";
    cancellationReason?: string;
    serviceCategory?: {
        coding: {
            system: string;
            code: string;
            display: string;
        }[]
    }[];
    serviceType?: {
        coding: {
            system: string;
            code: string;
            display: string;
        }[]
    }[];
    specialty?: {
        coding: {
            system: string;
            code: string;
            display: string;
        }[]
    }[];
    appointmentType?: {
        coding: {
            system: string;
            code: string;
            display: string;
        }[]
    };
    reasonCode?: {
        coding: {
            system: string;
            code: string;
        }[];
        text: string;
    }[];
    priority?: number;
    description?: string;
    minutesDuration?: number;
    slot?: [];
    start?: string;
    end?: string;
    created?: string;
    comment?: string;
    reasonReference?: [];
    basedOn?: [];
    supportingInformation?: [];
    patientInstruction?: unknown;
    participant:{
        type:{
            coding: {
                system: string;
                code: string;
            }[];
            text: string;
        }[];
        actor: {
            reference: string;
            type: string;
            display: string;
        };
        required: "required" | "optional" | "information-only";
        status: "accepted" | "declined" | "tentative" | "needs-action";
        period: string;
    }[];
    requestedPeriod?: {
        start: string;
        end: string;
    }[]
}

type Paging = { 
    page: number;
    size: number;
    total: number
}

interface ResponseType{
    data: Appointment[];
    paging: Paging;
}

const initialState = {
    data: [
        {
            id:'',
            userId: '',
            status: "proposed",
            description: '',
            minutesDuration: 0,
            start: '',
            participant:[
                {
                    type:[
                        {
                        coding: [{
                            system: '',
                            code: '',
                        }],
                        text: '',
                        }
                     ],
                    actor: {
                        reference: '',
                        type: '',
                        display: ''
                      },
                    required: "required",
                    status: "accepted",
                    period: ''
                }]
        }
    ] as Appointment[],
    paging:{
        page: 0,
        size: 0,
        total: 0,
    } as Paging,
    loading: false,
    error: '',
}

const initialStateByID = {
   data:{
            id:'',
            userId: '',
            status: "proposed",
            description: '',
            minutesDuration: 0,
            start: '',
            participant:[
                {
                    type:[
                        {
                        coding: [{
                            system: '',
                            code: '',
                        }],
                        text: '',
                        }
                     ],
                    actor: {
                        reference: '',
                        type: '',
                        display: ''
                      },
                    required: "required",
                    status: "accepted",
                    period: ''
                }]
        } as Appointment,
    loading: false,
    error: '',
}

export const getAppointment = createAsyncThunk(
    "patient/appointment",
    async({endpoint, params}:GetRequestType) => {
        try{
            const access_token = localStorage.getItem('access_token');
            const {data} = await axios.get<ResponseType>(
                `https://fhir-service.herokuapp.com/api/${endpoint}`,
                {
                    headers: {
                        Accept: 'application/json',
                        Authorization: `Bearer ${access_token}`,
                    },
                    params: {...params}
                });
            console.log(data)
            return data;
        }catch (err) {console.error(err)}
    }
)

export const getAppointmentByID = createAsyncThunk(
    "patient/appointmentByID",
    async({endpoint, params}:GetRequestType) => {
        try{
            const access_token = localStorage.getItem('access_token');
            const {data} = await axios.get<ResponseType>(
                `https://fhir-service.herokuapp.com/api/${endpoint}`,
                {
                    headers: {
                        Accept: 'application/json',
                        Authorization: `Bearer ${access_token}`,
                    },
                    params: {...params}
                });
            return data;
        }catch (err) {console.error(err)}
    }
)

export const appointmentSlice = createSlice({
    name: 'appointments',
    initialState,
    reducers:{},
    extraReducers:(builder) => {
        builder.addCase(getAppointment.pending,
            (state)=>{
                state.loading = true
            }
        )
        builder.addCase(getAppointment.fulfilled,
            (state, action)=>{
                state.loading = false;
                state.data = _.cloneDeep(action.payload?.data as unknown as Appointment[]);
                state.paging = _.cloneDeep(action.payload?.paging as unknown as Paging);
            }
        )
        builder.addCase(getAppointment.rejected,
            (state,action)=>{
                state.loading = false;
                state.error = action.payload as unknown as string
            }
        )
    }
})

export const appointmentByIDSlice = createSlice({
    name: 'appointments',
    initialState: initialStateByID,
    reducers:{},
    extraReducers:(builder) => {
        builder.addCase(getAppointmentByID.pending,
            (state)=>{
            state.loading = true
            }
        )
        builder.addCase(getAppointmentByID.fulfilled,
            (state, action)=>{
            state.loading = false;
            state.data = _.cloneDeep(action.payload as unknown as Appointment)  
            }
        )
        builder.addCase(getAppointmentByID.rejected,
            (state,action)=>{
            state.loading = false;
            state.error = action.payload as unknown as string
            }
        )
    }
})

export const appointmentReducer = appointmentSlice.reducer
export const appointmentByIDReducer = appointmentByIDSlice.reducer