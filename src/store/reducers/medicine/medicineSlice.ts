import { Action, createSlice, Dispatch, PayloadAction } from "@reduxjs/toolkit";
import axios from "axios";
import { RootState } from "../../Store";

export interface Identifier {
  use: string;
  type: unknown;
  system: string;
  value: string;
  assignerOrganizationId: unknown;
  period: unknown;
}

export interface Code {
  coding: {
    system?: string;
    version?: string;
    code?: string;
    display: string;
    userSelected?: boolean;
  }[];
  text?: string;
}

export interface Manufacturer {
  reference: string;
  type?: unknown;
  identifier?: unknown;
  display?: unknown;
}

export interface Form {
  coding: {
    system?: string;
    version?: string;
    code?: string;
    display: string;
    userSelected?: boolean;
  }[];
  text?: string;
}

export interface Amount {
  numerator: {
    value: number;
    comparator?: unknown;
    unit?: string;
    system?: string;
    code?: string;
  };
  denominator: {
    value: number;
    comparator?: unknown;
    unit?: string;
    system?: string;
    code?: string;
  };
}

export interface Batch {
  lotNumber: string;
  expirationDate: string;
}

export interface Medicine {
  id: string;
  identifier?: Identifier[];
  code: Code;
  status?: string;
  manufacturer: Manufacturer;
  form: Form;
  amount: Amount;
  ingredient?: unknown;
  batch?: Batch;
}

export interface Paging {
  page: number;
  size: number;
  total: number;
}

export interface ResponseType {
  data: Medicine[];
  paging: Paging;
}

export interface InitialState {
  loading: boolean;
  medicine: Medicine[];
  medicineById: Medicine;
  paging: Paging;
}

const initialState: InitialState = {
  loading: false,
  medicine: [
    {
      id: "",
      code: {
        coding: [{ display: "" }],
      },
      manufacturer: { reference: "" },
      form: {
        coding: [{ display: "" }],
      },
      amount: {
        numerator: { value: 1 },
        denominator: { value: 2 },
      },
    },
  ],
  medicineById: {
    id: "",
    code: {
      coding: [{ display: "" }],
    },
    manufacturer: { reference: "" },
    form: {
      coding: [{ display: "" }],
    },
    amount: {
      numerator: { value: 1 },
      denominator: { value: 2 },
    },
  },
  paging: {
    page: 1,
    size: 5,
    total: 10,
  },
};

const medicineSlice = createSlice({
  name: "medicine",
  initialState,
  reducers: {
    loading(state) {
      state.loading = true;
    },
    loadingDone(state) {
      state.loading = false;
    },
    getMedicineSuccess(state, action: PayloadAction<ResponseType>) {
      state.medicine = action.payload.data;
      state.paging = action.payload.paging;
    },
    getMedicineByIdSuccess(state, action: PayloadAction<Medicine>) {
      state.medicineById = action.payload;
    },
  },
});

//actions
export const medicineActions = medicineSlice.actions;

export const getMedicine =
  (page: number, size: number, accessToken: string) =>
  async (dispatch: Dispatch<Action>) => {
    dispatch(medicineActions.loading());
    try {
      const response = await axios.get(
        `https://fhir-service.herokuapp.com/api/medications?page=${page}&size=${size}`,
        {
          headers: { Authorization: "Bearer " + accessToken },
        }
      );
      dispatch(medicineActions.getMedicineSuccess(response.data));
    } catch (error) {
      console.log(error);
    } finally {
      dispatch(medicineActions.loadingDone());
    }
  };

export const createMedicine =
  (data: object, accessToken: string) => async (dispatch: Dispatch<Action>) => {
    dispatch(medicineActions.loading());
    try {
      await axios.post(
        "https://fhir-service.herokuapp.com/api/medications",
        data,
        {
          headers: {
            Authorization: "Bearer " + accessToken,
            ContentType: "application/json",
          },
        }
      );
    } catch (error) {
      console.log(error);
    } finally {
      dispatch(medicineActions.loadingDone());
    }
  };

export const getMedicineById =
  (id: string, accessToken: string) => async (dispatch: Dispatch<Action>) => {
    dispatch(medicineActions.loading());
    try {
      const response = await axios.get(
        `https://fhir-service.herokuapp.com/api/medications/${id}`,
        {
          headers: { Authorization: "Bearer " + accessToken },
        }
      );
      dispatch(medicineActions.getMedicineByIdSuccess(response.data));
    } catch (error) {
      console.log(error);
    } finally {
      dispatch(medicineActions.loadingDone());
    }
  };

export const updateMedicine =
  (id: string, updateData: object, accessToken: string) =>
  async (dispatch: Dispatch<Action>) => {
    dispatch(medicineActions.loading());
    try {
      await axios.patch(
        `https://fhir-service.herokuapp.com/api/medications/${id}`,
        updateData,
        {
          headers: {
            Authorization: "Bearer " + accessToken,
            ContentType: "application/json",
          },
        }
      );
    } catch (error) {
      console.log(error);
    } finally {
      dispatch(medicineActions.loadingDone());
    }
  };

//selectors
export const selectLoading = (state: RootState) =>
  state.medicineReducer.loading;

export const selectMedicine = (state: RootState) =>
  state.medicineReducer.medicine;

export const selectMedicineById = (state: RootState) =>
  state.medicineReducer.medicineById;

export const selectPaging = (state: RootState) => state.medicineReducer.paging;

//reducer
export const medicineReducer = medicineSlice.reducer;
