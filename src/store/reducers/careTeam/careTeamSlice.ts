import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import _ from "lodash";
import { careTeamService } from "../../../services/agent";
import {
  PostRequestType,
  Paging,
  PatchRequestType,
  DeleteRequestType,
  GetRequestType,
} from "../../../models/shared/basePagedResponse";
import { CareTeam } from "../../../models/careTeam";

interface CareTeamState {
  data: CareTeam[];
  paging: Paging;
  loading: boolean;
  error: unknown;
}

const initialCareTeamState: CareTeamState = {
  data: [],
  paging: {
    page: 0,
    size: 0,
    total: 0,
  },
  loading: false,
  error: "",
};

const initialCareTeamStateById = {
  data: {} as CareTeam,
  loading: false,
  error: "",
};

export const getCareTeamList = createAsyncThunk(
  "careTeam/getCareTeamList",
  async ({ params }: GetRequestType) => {
    return await careTeamService.getList({ params });
  }
);

export const getCareTeamById = createAsyncThunk(
  "careTeam/getCareTeamById",
  async (filter: string) => {
    return await careTeamService.getById(filter);
  }
);

export const createCareTeam = createAsyncThunk(
  "careTeam/createCareTeam",
  async ({ body }: PostRequestType) => {
    return await careTeamService.post({ body });
  }
);

export const updateCareTeam = createAsyncThunk(
  "careTeam/patchCareTeam",
  async ({ id, body }: PatchRequestType) => {
    return await careTeamService.patch({ id, body });
  }
);

export const deleteCareTeam = createAsyncThunk(
  "careTeam/deleteCareTeam",
  async ({ id }: DeleteRequestType) => {
    return await careTeamService.delete(id);
  }
);

export const getCareTeamListSlice = createSlice({
  name: "getCareTeamList",
  initialState: initialCareTeamState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getCareTeamList.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getCareTeamList.fulfilled, (state, action) => {
      state.loading = false;
      state.data = action.payload?.data as CareTeam[];
      state.paging = action.payload?.paging as Paging;
    });
    builder.addCase(getCareTeamList.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown as string;
    });
  },
});

export const getCareTeamByIdSlice = createSlice({
  name: "getCareTeamById",
  initialState: initialCareTeamStateById,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getCareTeamById.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getCareTeamById.fulfilled, (state, action) => {
      state.loading = false;
      state.data = action.payload as CareTeam;
    });
    builder.addCase(getCareTeamById.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown as string;
    });
  },
});

export const createCareTeamSlice = createSlice({
  name: "postCareTeam",
  initialState: {
    data: {} as CareTeam,
    loading: false,
    error: "" as unknown as string,
  },
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(createCareTeam.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(createCareTeam.fulfilled, (state, action) => {
      state.loading = false;
    });
    builder.addCase(createCareTeam.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown as string;
    });
  },
});

export const updateCareTeamSlice = createSlice({
  name: "patchCareTeam",
  initialState: {
    data: {} as CareTeam,
    loading: false,
    error: "" as unknown as string,
  },
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(updateCareTeam.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(updateCareTeam.fulfilled, (state, action) => {
      state.loading = false;
    });
    builder.addCase(updateCareTeam.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown as string;
    });
  },
});

export const deleteCareTeamSlice = createSlice({
  name: "deleteCareTeam",
  initialState: {
    data: "",
    loading: false,
    error: "" as unknown as string,
  },
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(deleteCareTeam.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(deleteCareTeam.fulfilled, (state, action) => {
      state.loading = false;
    });
    builder.addCase(deleteCareTeam.rejected, (state, action) => {
      state.error = action.payload as unknown as string;
    });
  },
});

export const getCareTeamListReducer = getCareTeamListSlice.reducer;
export const getCareTeamByIdReducer = getCareTeamByIdSlice.reducer;
export const createCareTeamReducer = createCareTeamSlice.reducer;
export const updateCareTeamReducer = updateCareTeamSlice.reducer;
export const deleteCareTeamReducer = deleteCareTeamSlice.reducer;
