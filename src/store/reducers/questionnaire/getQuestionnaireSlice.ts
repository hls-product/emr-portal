import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { QuestionBodyResponse } from "../../../models/question/QuestionBody";
import { Paging } from "../../../models/shared/basePagedResponse";
import { questionnaireService } from "../../../services/agent";

interface State {
  loading: boolean;
  error: unknown;
  data: QuestionBodyResponse[];
  paging: Paging;
}

interface ErrorType {
  response: {
    data: {
      error_description: string;
    };
  };
}

interface RequestType {
  page: number;
  size: number;
}

const initialState = {
  data: [],
  loading: false,
  error: null,
  paging: {
    page: 0,
    size: 0,
    total: 0,
  },
} as State;

export const getAllQuestionnaire = createAsyncThunk(
  "admin/questionnaire",
  async ({ page, size }: RequestType, thunkAPI) => {
    try {
      return await questionnaireService.getAllQuestionnaire(page, size);
    } catch (error) {
      return thunkAPI.rejectWithValue(
        (error as unknown as ErrorType).response.data.error_description
      );
    }
  }
);

const getAllQuestionnaireSlice = createSlice({
  name: "getAllQuestionnaire",
  initialState: initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getAllQuestionnaire.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(getAllQuestionnaire.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
    });
    builder.addCase(getAllQuestionnaire.fulfilled, (state, action) => {
      state.loading = false;
      state.paging = action.payload.paging;
      state.data = action.payload.data;
    });
  },
});

export default getAllQuestionnaireSlice.reducer;
