import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { QuestionBodyResponse } from "../../../models/question/QuestionBody";
import { questionnaireService } from "../../../services/agent";

interface State {
  loading: boolean;
  error: unknown;
  data: QuestionBodyResponse;
}

interface RequestType {
  id: string;
}

interface ErrorType {
  response: {
    data: {
      error_description: string;
    };
  };
}

const initialState = {
  data: {},
  loading: false,
  error: null,
} as State;

interface ErrorType {
  response: {
    data: {
      error_description: string;
    };
  };
}

export const getQuestionnaireById = createAsyncThunk(
  "admin/questionnaire/id",
  async ({ id }: RequestType, thunkAPI) => {
    try {
      return await questionnaireService.getQuestionnaireById(id);
    } catch (error) {
      return thunkAPI.rejectWithValue(
        (error as unknown as ErrorType).response.data.error_description
      );
    }
  }
);

const getQuestionnaireByIdSlice = createSlice({
  name: "createQuestionnaire",
  initialState: initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getQuestionnaireById.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(getQuestionnaireById.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
    });
    builder.addCase(getQuestionnaireById.fulfilled, (state, action) => {
      state.loading = false;
      state.data = action.payload;
    });
  },
});

export default getQuestionnaireByIdSlice.reducer;
