import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { QuestionBody } from "../../../models/question/QuestionBody";
import { questionnaireService } from "../../../services/agent";

interface State {
  loading: boolean;
  error: unknown;
  data: boolean;
}

interface RequestType {
  id: string;
}

interface ErrorType {
  response: {
    data: {
      error_description: string;
    };
  };
}

const initialState = {
  data: false,
  loading: false,
  error: null,
} as State;

interface ErrorType {
  response: {
    data: {
      error_description: string;
    };
  };
}

export const deleteQuestionnaireById = createAsyncThunk(
  "admin/questionnaire/delete",
  async ({ id }: RequestType, thunkAPI) => {
    try {
      return await questionnaireService.deleteQuestionnaireById(id);
    } catch (error) {
      return thunkAPI.rejectWithValue(
        (error as unknown as ErrorType).response.data.error_description
      );
    }
  }
);

const deleteQuestionnaireByIdSlice = createSlice({
  name: "deleteQuestionnaireById",
  initialState: initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(deleteQuestionnaireById.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(deleteQuestionnaireById.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
    });
    builder.addCase(deleteQuestionnaireById.fulfilled, (state, action) => {
      state.loading = false;
      state.data = action.payload;
    });
  },
});

export default deleteQuestionnaireByIdSlice.reducer;
