import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { QuestionBody } from "../../../models/question/QuestionBody";
import { questionnaireService } from "../../../services/agent";

interface State {
  loading: boolean;
  error: unknown;
  data: QuestionBody;
}

interface RequestType {
  questionBody: QuestionBody;
  id: string;
}

interface ErrorType {
  response: {
    data: {
      error_description: string;
    };
  };
}

const initialState = {
  data: {},
  loading: false,
  error: null,
} as State;

interface ErrorType {
  response: {
    data: {
      error_description: string;
    };
  };
}

export const editQuestionnaire = createAsyncThunk(
  "admin/questionnaire/edit",
  async ({ questionBody, id }: RequestType, thunkAPI) => {
    try {
      return await questionnaireService.editQuestionnaire(questionBody, id);
    } catch (error) {
      return thunkAPI.rejectWithValue(
        (error as unknown as ErrorType).response.data.error_description
      );
    }
  }
);

const editQuestionnaireSlice = createSlice({
  name: "createQuestionnaire",
  initialState: initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(editQuestionnaire.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(editQuestionnaire.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
    });
    builder.addCase(editQuestionnaire.fulfilled, (state, action) => {
      state.loading = false;
      state.data = action.payload;
    });
  },
});

export default editQuestionnaireSlice.reducer;
