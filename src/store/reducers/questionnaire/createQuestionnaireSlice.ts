import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { QuestionBody } from "../../../models/question/QuestionBody";
import { questionnaireService } from "../../../services/agent";

interface State {
  loading: boolean;
  error: unknown;
  data: QuestionBody;
}

interface RequestType {
  questionBody: QuestionBody;
}

interface ErrorType {
  response: {
    data: {
      error_description: string;
    };
  };
}

const initialState = {
  data: {},
  loading: false,
  error: null,
} as State;

interface ErrorType {
  response: {
    data: {
      error_description: string;
    };
  };
}

export const createQuestionnaire = createAsyncThunk(
  "admin/questionnaire/create",
  async ({ questionBody }: RequestType, thunkAPI) => {
    try {
      return await questionnaireService.createQuestionnaire(questionBody);
    } catch (error) {
      return thunkAPI.rejectWithValue(
        (error as unknown as ErrorType).response.data.error_description
      );
    }
  }
);

const createQuestionnaireSlice = createSlice({
  name: "createQuestionnaire",
  initialState: initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(createQuestionnaire.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(createQuestionnaire.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
    });
    builder.addCase(createQuestionnaire.fulfilled, (state, action) => {
      state.loading = false;
      state.data = action.payload;
    });
  },
});

export default createQuestionnaireSlice.reducer;
