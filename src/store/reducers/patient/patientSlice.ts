import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import _ from "lodash";
import { patientService } from "../../../services/agent";
import {
  GetRequestType,
  PostRequestType,
  Paging,
  PatchRequestType,
  DeleteRequestType,
} from "../../../models/shared/basePagedResponse";
import { patientModel } from "../../../models/patients";

//Define a type for the slice state
interface PatientState {
  data: patientModel[];
  paging: Paging;
  loading: boolean;
  error: unknown;
}
//Define the initial state using that type
const initialPatientState: PatientState = {
  data: [],
  paging: {
    page: 0,
    size: 0,
    total: 0,
  },
  loading: false,
  error: "",
};

const initialPatientStateByID = {
  data: {} as patientModel,
  loading: false,
  error: "",
};

// export const getPatientList = createAsyncThunk(
//   "patient/getList",
//   async ({ params }: GetRequestType) => {
//     return await patientService.getList({ params });
//   }
// );

export const getPatientById = createAsyncThunk(
  "patient/getPatientById",
  async (filter: string) => {
    return await patientService.getById(filter);
  }
);
export const createPatient = createAsyncThunk(
  "patient/createPatient",
  async ({ body }: PostRequestType) => {
    return await patientService.post({ body });
  }
);
export const updatePatient = createAsyncThunk(
  "patient/patchPatient",
  async ({ id, body }: PatchRequestType) => {
    return await patientService.patch({ id, body });
  }
);

export const deletePatient = createAsyncThunk(
  "patient/deletePatient",
  async ({ id }: DeleteRequestType) => {
    return await patientService.delete(id);
  }
);

// export const getPatientListSlice = createSlice({
//   name: "getPatientList",
//   initialState: initialPatientState,
//   reducers: {},
//   extraReducers: (builder) => {
//     builder.addCase(getPatientList.pending, (state) => {
//       state.loading = true;
//     });
//     builder.addCase(getPatientList.fulfilled, (state, action) => {
//       state.loading = false;
//       state.data = _.cloneDeep(
//         action.payload?.data as unknown as PatientState["data"]
//       );
//       state.paging = _.cloneDeep(action.payload?.paging as unknown as Paging);
//     });
//     builder.addCase(getPatientList.rejected, (state, action) => {
//       // state.loading = false;
//       state.error = action.payload as unknown;
//     });
//   },
// });

export const getPatientByIdSlice = createSlice({
  name: "getPatientById",
  initialState: initialPatientStateByID,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getPatientById.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getPatientById.fulfilled, (state, action) => {
      state.loading = false;
      state.data = _.cloneDeep(action.payload as unknown as patientModel);
    });
    builder.addCase(getPatientById.rejected, (state, action) => {
      // state.loading = false;
      state.error = action.payload as unknown as string;
    });
  },
});

export const createPatientSlice = createSlice({
  name: "postPatient",
  initialState: {
    data: {},
    loading: false,
    error: "" as unknown,
  },
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(createPatient.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(createPatient.fulfilled, (state, action) => {
      state.loading = false;
    });
    builder.addCase(createPatient.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown;
    });
  },
});

export const updatePatientSlice = createSlice({
  name: "patchedPatient",
  initialState: {
    data: {},
    loading: false,
    error: "" as unknown,
  },
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(updatePatient.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(updatePatient.fulfilled, (state, action) => {
      state.loading = false;
    });
    builder.addCase(updatePatient.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown;
    });
  },
});

// export const getPatientListReducer = getPatientListSlice.reducer;
export const getPatientByIdReducer = getPatientByIdSlice.reducer;
export const createPatientReducer = createPatientSlice.reducer;
export const updatePatientReducer = updatePatientSlice.reducer;
