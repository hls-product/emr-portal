import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import _ from "lodash";
import {
  GetRequestType,
  PostRequestType,
  Paging,
  PatchRequestType,
  DeleteRequestType,
} from "../../../models/shared/basePagedResponse";
import { relatedPatientService } from "../../../services/agent";
import { relatedPatientModel } from "./../../../models/relatedPatient";
//Define a type for the slice state
interface RelatedPatientState {
  data: relatedPatientModel[];
  paging: Paging;
  loading: boolean;
  error: unknown;
}
//Define the initial state using that type
const initialState: RelatedPatientState = {
  data: [],
  paging: {
    page: 0,
    size: 0,
    total: 0,
  },
  loading: false,
  error: "",
};

const initialStateByID = {
  data: {} as relatedPatientModel,
  loading: false,
  error: "",
};

export const getRelatedPatientData = createAsyncThunk(
  "patient/getRelatedPatient",
  async ({ params }: GetRequestType) => {
    return await relatedPatientService.getList({ params });
  }
);

export const getRelatedPatientDataByID = createAsyncThunk(
  "patient/getRelatedPatientByID",
  async (filter: string) => {
    return await relatedPatientService.getById(filter);
  }
);

export const postRelatedPatientData = createAsyncThunk(
  "patient/postRelatedPatient",
  async ({ body }: PostRequestType) => {
    return await relatedPatientService.post({ body });
  }
);

export const updateRelatedPatientData = createAsyncThunk(
  "patient/updateRelatedPatient",
  async ({ id, body }: PatchRequestType) => {
    return await relatedPatientService.update({ body }, id);
  }
);

export const deleteRelatedPatientData = createAsyncThunk(
  "patient/deleteRelatedPatientByID",
  async ({ id }: DeleteRequestType) => {
    return await relatedPatientService.delete(id);
  }
);

export const getRelatedPatientSlice = createSlice({
  name: "getRelatedPatientState",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getRelatedPatientData.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getRelatedPatientData.fulfilled, (state, action) => {
      state.loading = false;
      state.data = [...(action.payload?.data as any)];
      state.paging = _.cloneDeep(action.payload?.paging as Paging);
    });
    builder.addCase(getRelatedPatientData.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown;
    });
  },
});

export const getRelatedPatientByIDSlice = createSlice({
  name: "getRelatedPatientByIDState",
  initialState: initialStateByID,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getRelatedPatientDataByID.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getRelatedPatientDataByID.fulfilled, (state, action) => {
      state.loading = false;
      state.data = _.cloneDeep(action.payload as unknown as relatedPatientModel);
    });
    builder.addCase(getRelatedPatientDataByID.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown as string;
    });
  },
});

export const postRelatedPatientSlice = createSlice({
  name: "postRelatedPatientState",
  initialState: {
    data: {},
    loading: false,
    error: "",
  },
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(postRelatedPatientData.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(postRelatedPatientData.fulfilled, (state, action) => {
      state.loading = false;
      state.data = _.cloneDeep(action.payload as relatedPatientModel);
    });
    builder.addCase(postRelatedPatientData.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown as string;
    });
  },
});
export const updateRelatedPatientSlice = createSlice({
  name: "updateRelatedPatient",
  initialState: {
    data: {},
    loading: false,
    error: "" as unknown,
  },
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(updateRelatedPatientData.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(updateRelatedPatientData.fulfilled, (state, action) => {
      state.loading = false;
    });
    builder.addCase(updateRelatedPatientData.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown;
    });
  },
});

export const getRelatedPatientReducer = getRelatedPatientSlice.reducer;
export const postRelatedPatientReducer = postRelatedPatientSlice.reducer;
export const getRelatedPatientByIDReducer = getRelatedPatientByIDSlice.reducer;
export const updateRelatedPatientReducer = updateRelatedPatientSlice.reducer;
