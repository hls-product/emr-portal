import {createSlice, createAsyncThunk} from '@reduxjs/toolkit'
import _ from 'lodash';
import { ConditionModel } from '../../../models/conditions';
import {GetRequestType, PostRequestType, Paging, PatchRequestType} from "../../../models/shared/basePagedResponse";
import { conditionService } from '../../../services/agent';
//Define a type for the slice state
interface ConditionState {
    data: ConditionModel[];
    paging: Paging;
    loading: boolean;
    error: unknown;
}
//Define the initial state using that type
const initialState: ConditionState= {
    data: [],
    paging: {
        page: 0,
        size: 0,
        total: 0,
    },
    loading: false,
    error: '',
}

const initialStateByID = {
    data: {} as ConditionModel,
    loading: false,
    error: ''
}

export const getConditionData = createAsyncThunk(
    "patient/getCondition",
    async({params}:GetRequestType) => {
        return (await conditionService.getList({params}))    
    }
)

export const getConditionDataByID = createAsyncThunk(
    "patient/getConditionByID",
    async(filter: string) => {
        return (await conditionService.getById(filter))
    }
)

export const postConditionData = createAsyncThunk(
    'patient/postCondition',
    async ( { body}: PostRequestType) => {
      return (await conditionService.post({body}))
    }
)

export const updateConditionData = createAsyncThunk(
    'patient/updateCondition',
    async ({id, body}: PatchRequestType) => {
        return (await conditionService.update({body}, id))
    }
)

export const getConditionSlice = createSlice({
    name: 'getConditionState',
    initialState,
    reducers:{},
    extraReducers:(builder) => {
        builder.addCase(getConditionData.pending,
            (state)=>{
            state.loading = true
            }
        )
        builder.addCase(getConditionData.fulfilled,
            (state, action)=>{
            state.loading = false;
            state.data = [...action.payload?.data as any]
            state.paging = _.cloneDeep(action.payload?.paging as Paging) 

            }
        )
        builder.addCase(getConditionData.rejected,
            (state,action)=>{
            state.loading = false;
            state.error = action.payload as unknown
            }
        )
    }
})

export const getConditionByIDSlice = createSlice({
    name: 'getConditionByIDState',
    initialState: initialStateByID,
    reducers:{},
    extraReducers:(builder) => {
        builder.addCase(getConditionDataByID.pending,
            (state)=>{
            state.loading = true
            }
        )
        builder.addCase(getConditionDataByID.fulfilled,
            (state, action)=>{
            state.loading = false;
            state.data= _.cloneDeep(action.payload as unknown as ConditionModel);
            }
        )
        builder.addCase(getConditionDataByID.rejected,
            (state,action)=>{
            state.loading = false;
            state.error = action.payload as unknown as string
            }
        )
    }
})

export const postConditionSlice = createSlice({
    name: 'postConditionState',
    initialState: {
        data: {},
        loading: false,
        error: ''
    },
    reducers:{},
    extraReducers:(builder) => {
        builder.addCase(postConditionData.pending,
            (state)=>{
            state.loading = true
            }
        )
        builder.addCase(postConditionData.fulfilled,
            (state, action)=>{
            state.loading = false;
            state.data = _.cloneDeep(action.payload as unknown as ConditionModel) 

            }
        )
        builder.addCase(postConditionData.rejected,
            (state,action)=>{
            state.loading = false;
            state.error = action.payload as unknown as string;
            }
        )
    }
})
export const updateConditionSlice = createSlice({
    name: 'updateCondition',
    initialState: {
        data: {},
        loading: false,
        error: '' as unknown,
    },
    reducers:{

    },
    extraReducers: (builder) => {
        builder.addCase(updateConditionData.pending,
            (state) => {
                state.loading = true;
            }
        )
        builder.addCase(updateConditionData.fulfilled,
            (state, action) => {
                state.loading = false;
            }
        )
        builder.addCase(updateConditionData.rejected,
            (state, action) => {
                state.loading = false;
                state.error = action.payload as unknown;
            }
        )
    }
})

export const getConditionReducer = getConditionSlice.reducer
export const postConditionReducer = postConditionSlice.reducer
export const getConditionByIDReducer = getConditionByIDSlice.reducer
export const updateConditionReducer = updateConditionSlice.reducer