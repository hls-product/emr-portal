import { createSlice } from "@reduxjs/toolkit";
import { DialogAction } from "../../../models/dialog/dialogAction";

interface SidebarState {
  openStatus: boolean;
}

const initialState = {
  openStatus: false,
} as SidebarState;

const sidebarSlice = createSlice({
  name: "dialog",
  initialState: initialState,
  reducers: {
    interactSidebar: (state, action) => {
      state.openStatus = action.payload;
    },
  },
});

export const { interactSidebar } = sidebarSlice.actions;
export default sidebarSlice.reducer;
