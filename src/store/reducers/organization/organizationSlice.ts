import { Organization } from "./../../../models/organizations/organization";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import _ from "lodash";
import { organizationService } from "../../../services/agent";
import {
  GetRequestType,
  Paging,
  DeleteRequestType,
} from "../../../models/shared/basePagedResponse";

//Define a type for the slice state
interface OrganizationState {
  data: Organization[];
  paging: Paging;
  loading: boolean;
  error: unknown;
}
//Define the initial state using that type
const initialOrganizationState: OrganizationState = {
  data: [],
  paging: {
    page: 0,
    size: 0,
    total: 0,
  },
  loading: false,
  error: "",
};

const initialOrganizationStateByID = {
  data: {} as Organization,
  loading: false,
  error: "",
};

export const getOrganizationData = createAsyncThunk(
  "organization/getOrganization",
  async ({ params }: GetRequestType) => {
    return await organizationService.getList({ params });
  }
);
export const getOrganizationDataByID = createAsyncThunk(
  "organization/getOrganizationByID",
  async (filter: string) => {
    return await organizationService.getById(filter);
  }
);
export const postOrganizationData = createAsyncThunk(
  "organization/createOrganization",
  async (body: Organization) => {
    return await organizationService.add(body);
  }
);
export const updateOrganizationData = createAsyncThunk(
  "organization/patchOrganization",
  async (body: Organization) => {
    return await organizationService.patch(body);
  }
);

export const deleteOrganizationData = createAsyncThunk(
  "organization/deleteOrganizationByID",
  async ({ id }: DeleteRequestType) => {
    return await organizationService.delete(id);
  }
);

export const organizationSlice = createSlice({
  name: "organization",
  initialState: initialOrganizationState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getOrganizationData.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getOrganizationData.fulfilled, (state, action) => {
      state.loading = false;
      state.data = _.cloneDeep(
        action.payload?.data as unknown as OrganizationState["data"]
      );
      state.paging = _.cloneDeep(action.payload?.paging as unknown as Paging);
    });
    builder.addCase(getOrganizationData.rejected, (state, action) => {
      // state.loading = false;
      state.error = action.payload as unknown;
    });
  },
});
export const postOrganizationSlice = createSlice({
  name: "postOrganization",
  initialState: {
    data: {},
    loading: false,
    error: "" as unknown,
  },
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(postOrganizationData.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(postOrganizationData.fulfilled, (state, action) => {
      state.loading = false;
    });
    builder.addCase(postOrganizationData.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown;
    });
  },
});
export const organizationByIDSlice = createSlice({
  name: "organization",
  initialState: initialOrganizationStateByID,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getOrganizationDataByID.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getOrganizationDataByID.fulfilled, (state, action) => {
      state.loading = false;
      state.data = _.cloneDeep(action.payload as unknown as Organization);
    });
    builder.addCase(getOrganizationDataByID.rejected, (state, action) => {
      // state.loading = false;
      state.error = action.payload as unknown as string;
    });
  },
});
export const updateOrganizationDataSlice = createSlice({
  name: "patchedOrganization",
  initialState: {
    data: {},
    loading: false,
    error: "" as unknown,
  },
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(updateOrganizationData.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(updateOrganizationData.fulfilled, (state, action) => {
      state.loading = false;
    });
    builder.addCase(updateOrganizationData.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown;
    });
  },
});

export const postOrganizationReducer = postOrganizationSlice.reducer;
export const organizationReducer = organizationSlice.reducer;
export const organizationByIDReducer = organizationByIDSlice.reducer;
export const updateOrganizationReducer = updateOrganizationDataSlice.reducer;
