import {createSlice, createAsyncThunk} from '@reduxjs/toolkit'
import _ from 'lodash';
import { EncounterModel } from '../../../models/encounter';
import {GetRequestType, PostRequestType, Paging, PatchRequestType} from "../../../models/shared/basePagedResponse";
import { encounterService } from '../../../services/agent';
//Define a type for the slice state
interface EncounterState {
    data: EncounterModel[];
    paging: Paging;
    loading: boolean;
    error: unknown;
}
//Define the initial state using that type
const initialState: EncounterState= {
    data: [],
    paging: {
        page: 0,
        size: 0,
        total: 0,
    },
    loading: false,
    error: '',
}

const initialStateByID = {
    data: {} as EncounterModel,
    loading: false,
    error: ''
}

export const getEncounterData = createAsyncThunk(
    "patient/getEncounter",
    async({params}:GetRequestType) => {
        return (await encounterService.getList({params}))    
    }
)

export const getEncounterDataByID = createAsyncThunk(
    "patient/getEncounterByID",
    async(filter: string) => {
        return (await encounterService.getById(filter))
    }
)

export const postEncounterData = createAsyncThunk(
    'patient/postEncounter',
    async ( { body}: PostRequestType) => {
      return (await encounterService.post({body}))
    }
)

export const updateEncounterData = createAsyncThunk(
    'patient/updateEncounter',
    async ({id, body}: PatchRequestType) => {
        return (await encounterService.update({body}, id))
    }
)

export const getEncounterSlice = createSlice({
    name: 'getEncounterState',
    initialState,
    reducers:{},
    extraReducers:(builder) => {
        builder.addCase(getEncounterData.pending,
            (state)=>{
            state.loading = true
            }
        )
        builder.addCase(getEncounterData.fulfilled,
            (state, action)=>{
            state.loading = false;
            state.data = [...action.payload?.data as any]
            state.paging = _.cloneDeep(action.payload?.paging as Paging) 

            }
        )
        builder.addCase(getEncounterData.rejected,
            (state,action)=>{
            state.loading = false;
            state.error = action.payload as unknown
            }
        )
    }
})

export const getEncounterByIDSlice = createSlice({
    name: 'getEncounterByIDState',
    initialState: initialStateByID,
    reducers:{},
    extraReducers:(builder) => {
        builder.addCase(getEncounterDataByID.pending,
            (state)=>{
            state.loading = true
            }
        )
        builder.addCase(getEncounterDataByID.fulfilled,
            (state, action)=>{
            state.loading = false;
            state.data= _.cloneDeep(action.payload as unknown as EncounterModel);
            }
        )
        builder.addCase(getEncounterDataByID.rejected,
            (state,action)=>{
            state.loading = false;
            state.error = action.payload as unknown as string
            }
        )
    }
})

export const postEncounterSlice = createSlice({
    name: 'postEncounterState',
    initialState: {
        data: {},
        loading: false,
        error: ''
    },
    reducers:{},
    extraReducers:(builder) => {
        builder.addCase(postEncounterData.pending,
            (state)=>{
            state.loading = true
            }
        )
        builder.addCase(postEncounterData.fulfilled,
            (state, action)=>{
            state.loading = false;
            state.data = _.cloneDeep(action.payload as unknown as EncounterModel) 

            }
        )
        builder.addCase(postEncounterData.rejected,
            (state,action)=>{
            state.loading = false;
            state.error = action.payload as unknown as string;
            }
        )
    }
})
export const updateEncounterSlice = createSlice({
    name: 'updateEncounter',
    initialState: {
        data: {},
        loading: false,
        error: '' as unknown,
    },
    reducers:{

    },
    extraReducers: (builder) => {
        builder.addCase(updateEncounterData.pending,
            (state) => {
                state.loading = true;
            }
        )
        builder.addCase(updateEncounterData.fulfilled,
            (state, action) => {
                state.loading = false;
            }
        )
        builder.addCase(updateEncounterData.rejected,
            (state, action) => {
                state.loading = false;
                state.error = action.payload as unknown;
            }
        )
    }
})

export const getEncounterReducer = getEncounterSlice.reducer
export const postEncounterReducer = postEncounterSlice.reducer
export const getEncounterByIDReducer = getEncounterByIDSlice.reducer
export const updateEncounterReducer = updateEncounterSlice.reducer