import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import _ from "lodash";
import { locationService } from "../../../services/agent";
import {
  GetRequestType,
  PostRequestType,
  Paging,
  PatchRequestType,
  DeleteRequestType,
} from "../../../models/shared/basePagedResponse";
import { locationModel } from "../../../models/location";

//Define a type for the slice state
interface LocationState {
  data: locationModel[];
  paging: Paging;
  loading: boolean;
  error: unknown;
}
//Define the initial state using that type
const initialLocationState: LocationState = {
  data: [],
  paging: {
    page: 0,
    size: 0,
    total: 0,
  },
  loading: false,
  error: "",
};

const initialLocationStateByID = {
  data: {} as locationModel,
  loading: false,
  error: "",
};

export const getLocationsData = createAsyncThunk(
  "locations/getList",
  async ({ params }: GetRequestType) => {
    return await locationService.getList({ params });
  }
);
export const getLocationDataByID = createAsyncThunk(
  "locations/getByID",
  async (filter: string) => {
    return await locationService.getById(filter);
  }
);
export const postLocationData = createAsyncThunk(
  "locations/add",
  async (item: locationModel) => {
    return await locationService.add(item);
  }
);
export const updateLocationData = createAsyncThunk(
  "locations/patch",
  async (item: locationModel) => {
    return await locationService.patch(item);
  }
);
export const deleteLocationData = createAsyncThunk(
  "locations/delete",
  async ({ id }: DeleteRequestType) => {
    return await locationService.delete(id);
  }
);
export const getLocationSlice = createSlice({
  name: "location",
  initialState: initialLocationState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getLocationsData.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getLocationsData.fulfilled, (state, action) => {
      state.loading = false;
      state.data = _.cloneDeep(
        action.payload?.data as unknown as LocationState["data"]
      );
      state.paging = _.cloneDeep(action.payload?.paging as unknown as Paging);
    });
    builder.addCase(getLocationsData.rejected, (state, action) => {
      // state.loading = false;
      state.error = action.payload as unknown;
    });
  },
});
export const postLocationSlice = createSlice({
  name: "postLocation",
  initialState: {
    data: {},
    loading: false,
    error: "" as unknown,
  },
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(postLocationData.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(postLocationData.fulfilled, (state, action) => {
      state.loading = false;
    });
    builder.addCase(postLocationData.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown;
    });
  },
});
export const getLocationByIdSlice = createSlice({
  name: "location",
  initialState: initialLocationStateByID,
  reducers: {
    clear(state) {
      console.log("clear");
      state.data = {};
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getLocationDataByID.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getLocationDataByID.fulfilled, (state, action) => {
      state.loading = false;
      state.data = _.cloneDeep(action.payload as unknown as locationModel);
    });
    builder.addCase(getLocationDataByID.rejected, (state, action) => {
      // state.loading = false;
      state.error = action.payload as unknown as string;
    });
  },
});
export const patchLocationSlice = createSlice({
  name: "patchedLocation",
  initialState: {
    data: {},
    loading: false,
    error: "" as unknown,
  },
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(updateLocationData.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(updateLocationData.fulfilled, (state, action) => {
      state.loading = false;
    });
    builder.addCase(updateLocationData.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown;
    });
  },
});

export const postLocationReducer = postLocationSlice.reducer;
export const getLocationReducer = getLocationSlice.reducer;
export const getLocationByIdReducer = getLocationByIdSlice.reducer;
export const patchLocationReducer = patchLocationSlice.reducer;
