import { Action, createSlice, Dispatch, PayloadAction } from "@reduxjs/toolkit";
import axios from "axios";
import { claimsModel } from "../../../models/claims";
import { immunizationModel } from "../../../models/immunization";
import { insuranceModel } from "../../../models/insurance";
import { invoiceModel } from "../../../models/invoices";
import { patientModel } from "../../../models/patients";
import { relatedPatientModel } from "../../../models/relatedPatient";
import { Paging } from "../../../models/shared/basePagedResponse";
import { CheckList } from "../../../pages/PatientDetails/PatientRecords/PatientRecords";
import { RootState } from "../../Store";
import { Appointment } from "../appointment/appointmentSlice";
import { ObservationType } from "../observations/observationSlice";

export interface AppointmentType {
  data: Appointment[];
  paging: Paging;
}

export interface ImmunizationType {
  data: immunizationModel[];
  paging: Paging;
}

export interface InsuranceType {
  data: insuranceModel[];
  paging: Paging;
}

export interface InvoiceType {
  data: invoiceModel[];
  paging: Paging;
}

export interface observationModelType {
  data: ObservationType[];
  paging: Paging;
}

export interface RelatedType {
  data: relatedPatientModel[];
  paging: Paging;
}

export interface ClaimType {
  data: claimsModel[];
  paging: Paging;
}

export interface InitialState {
  loading: boolean;
  idUrl: string;
  checkList: CheckList;
  patientById: patientModel;
  appointment: Appointment[];
  immunization: immunizationModel[];
  insurance: insuranceModel[];
  invoice: invoiceModel[];
  observation: ObservationType[];
  related: relatedPatientModel[];
  claim: claimsModel[];
}

const initialState: InitialState = {
  loading: false,
  idUrl: "",
  checkList: {},
  patientById: {},
  appointment: [],
  immunization: [],
  insurance: [],
  invoice: [],
  observation: [],
  related: [],
  claim: [],
};

const patientRecordSlice = createSlice({
  name: "Patient Record",
  initialState,
  reducers: {
    loading(state) {
      state.loading = true;
    },
    loadingDone(state) {
      state.loading = false;
    },
    getIdUrlSuccess(state, action: PayloadAction<string>) {
      state.idUrl = action.payload;
    },
    getCheckListSuccess(state, action: PayloadAction<CheckList>) {
      state.checkList = action.payload;
    },
    getPatientByIdSuccess(state, action: PayloadAction<patientModel>) {
      state.patientById = action.payload;
    },
    getAppointmentSuccess(state, action: PayloadAction<AppointmentType>) {
      state.appointment = action.payload.data;
    },
    getImmunizationSuccess(state, action: PayloadAction<ImmunizationType>) {
      state.immunization = action.payload.data;
    },
    getInsuranceSuccess(state, action: PayloadAction<InsuranceType>) {
      state.insurance = action.payload.data;
    },
    getInvoiceSuccess(state, action: PayloadAction<InvoiceType>) {
      state.invoice = action.payload.data;
    },
    getObservationSuccess(state, action: PayloadAction<observationModelType>) {
      state.observation = action.payload.data;
    },
    getRelatedSuccess(state, action: PayloadAction<RelatedType>) {
      state.related = action.payload.data;
    },
    getClaimSuccess(state, action: PayloadAction<ClaimType>) {
      state.claim = action.payload.data;
    },
  },
});

//actions
export const patientRecordActions = patientRecordSlice.actions;

export const getIdUrl = (id: string) => (dispatch: Dispatch<Action>) => {
  try {
    dispatch(patientRecordActions.getIdUrlSuccess(id));
  } catch (error) {
    console.log(error);
  }
};

export const getCheckList =
  (checkList: CheckList) => (dispatch: Dispatch<Action>) => {
    try {
      dispatch(patientRecordActions.getCheckListSuccess(checkList));
    } catch (error) {
      console.log(error);
    }
  };

export const getPatientById =
  (id: string, accessToken: string) => async (dispatch: Dispatch<Action>) => {
    dispatch(patientRecordActions.loading());
    try {
      const response = await axios.get(
        `https://fhir-service.herokuapp.com/api/patients/${id}`,
        {
          headers: { Authorization: "Bearer " + accessToken },
        }
      );
      dispatch(patientRecordActions.getPatientByIdSuccess(response.data));
    } catch (error) {
      console.log(error);
    } finally {
      dispatch(patientRecordActions.loadingDone());
    }
  };

export const getAppointment =
  (accessToken: string) => async (dispatch: Dispatch<Action>) => {
    dispatch(patientRecordActions.loading());
    try {
      const response = await axios.get(
        "https://fhir-service.herokuapp.com/api/appointments?page=1&size=10",
        {
          headers: { Authorization: "Bearer " + accessToken },
        }
      );
      dispatch(patientRecordActions.getAppointmentSuccess(response.data));
    } catch (error) {
      console.log(error);
    } finally {
      dispatch(patientRecordActions.loadingDone());
    }
  };

export const getImmunization =
  (accessToken: string) => async (dispatch: Dispatch<Action>) => {
    dispatch(patientRecordActions.loading());
    try {
      const response = await axios.get(
        "https://fhir-service.herokuapp.com/api/immunizations?page=1&size=10",
        {
          headers: { Authorization: "Bearer " + accessToken },
        }
      );
      dispatch(patientRecordActions.getImmunizationSuccess(response.data));
    } catch (error) {
      console.log(error);
    } finally {
      dispatch(patientRecordActions.loadingDone());
    }
  };

export const getInsurance =
  (accessToken: string) => async (dispatch: Dispatch<Action>) => {
    dispatch(patientRecordActions.loading());
    try {
      const response = await axios.get(
        "https://fhir-service.herokuapp.com/api/insurance-plans?page=1&size=10",
        {
          headers: { Authorization: "Bearer " + accessToken },
        }
      );
      dispatch(patientRecordActions.getInsuranceSuccess(response.data));
    } catch (error) {
      console.log(error);
    } finally {
      dispatch(patientRecordActions.loadingDone());
    }
  };

export const getInvoice =
  (accessToken: string) => async (dispatch: Dispatch<Action>) => {
    dispatch(patientRecordActions.loading());
    try {
      const response = await axios.get(
        "https://fhir-service.herokuapp.com/api/invoices?page=1&size=10",
        {
          headers: { Authorization: "Bearer " + accessToken },
        }
      );
      dispatch(patientRecordActions.getInvoiceSuccess(response.data));
    } catch (error) {
      console.log(error);
    } finally {
      dispatch(patientRecordActions.loadingDone());
    }
  };

export const getObservation =
  (accessToken: string) => async (dispatch: Dispatch<Action>) => {
    dispatch(patientRecordActions.loading());
    try {
      const response = await axios.get(
        "https://fhir-service.herokuapp.com/api/observations?page=1&size=10",
        {
          headers: { Authorization: "Bearer " + accessToken },
        }
      );
      dispatch(patientRecordActions.getObservationSuccess(response.data));
    } catch (error) {
      console.log(error);
    } finally {
      dispatch(patientRecordActions.loadingDone());
    }
  };

export const getRelated =
  (accessToken: string) => async (dispatch: Dispatch<Action>) => {
    dispatch(patientRecordActions.loading());
    try {
      const response = await axios.get(
        "https://fhir-service.herokuapp.com/api/relatedPersons?page=1&size=10",
        {
          headers: { Authorization: "Bearer " + accessToken },
        }
      );
      dispatch(patientRecordActions.getRelatedSuccess(response.data));
    } catch (error) {
      console.log(error);
    } finally {
      dispatch(patientRecordActions.loadingDone());
    }
  };

export const getClaim =
  (accessToken: string) => async (dispatch: Dispatch<Action>) => {
    dispatch(patientRecordActions.loading());
    try {
      const response = await axios.get(
        "https://fhir-service.herokuapp.com/api/claims?page=1&size=10",
        {
          headers: { Authorization: "Bearer " + accessToken },
        }
      );
      dispatch(patientRecordActions.getClaimSuccess(response.data));
    } catch (error) {
      console.log(error);
    } finally {
      dispatch(patientRecordActions.loadingDone());
    }
  };

//selectors
export const selectLoading = (state: RootState) =>
  state.patientRecordReducer.loading;

export const selectIdUrl = (state: RootState) =>
  state.patientRecordReducer.idUrl;

export const selectCheckList = (state: RootState) =>
  state.patientRecordReducer.checkList;

export const selectPatientById = (state: RootState) =>
  state.patientRecordReducer.patientById;

export const selectAppointment = (state: RootState) =>
  state.patientRecordReducer.appointment;

export const selectImmunization = (state: RootState) =>
  state.patientRecordReducer.immunization;

export const selectInsurance = (state: RootState) =>
  state.patientRecordReducer.insurance;

export const selectInvoice = (state: RootState) =>
  state.patientRecordReducer.invoice;

export const selectObservation = (state: RootState) =>
  state.patientRecordReducer.observation;

export const selectRelated = (state: RootState) =>
  state.patientRecordReducer.related;

export const selectClaim = (state: RootState) =>
  state.patientRecordReducer.claim;

//reducer
export const patientRecordReducer = patientRecordSlice.reducer;
