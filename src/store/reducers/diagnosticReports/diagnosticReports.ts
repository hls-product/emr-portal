import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import _ from "lodash";
import { diagnosticReportsModel } from "../../../models/diagnosticReports";
import {
  GetRequestType,
  PostRequestType,
  Paging,
  PatchRequestType,
} from "../../../models/shared/basePagedResponse";
import { diagnosticReportsService } from "../../../services/agent";
//Define a type for the slice state
interface DiagnosticReportsState {
  data: diagnosticReportsModel[];
  paging: Paging;
  loading: boolean;
  error: unknown;
}
//Define the initial state using that type
const initialState: DiagnosticReportsState = {
  data: [],
  paging: {
    page: 0,
    size: 0,
    total: 0,
  },
  loading: false,
  error: "",
};

const initialStateByID = {
  data: {} as diagnosticReportsModel,
  loading: false,
  error: "",
};

export const getDiagnosticReportsData = createAsyncThunk(
  "patient/getDiagnosticReports",
  async ({ params }: GetRequestType) => {
    return await diagnosticReportsService.getList({ params });
  }
);

export const getDiagnosticReportsDataByID = createAsyncThunk(
  "patient/getDiagnosticReportsByID",
  async (filter: string) => {
    return await diagnosticReportsService.getById(filter);
  }
);

export const postDiagnosticReportsData = createAsyncThunk(
  "patient/postDiagnosticReports",
  async ({ body }: PostRequestType) => {
    return await diagnosticReportsService.post({ body });
  }
);

export const updateDiagnosticReportsData = createAsyncThunk(
  "patient/updateDiagnosticReports",
  async ({ id, body }: PatchRequestType) => {
    return await diagnosticReportsService.update({ body }, id);
  }
);

export const getDiagnosticReportsSlice = createSlice({
  name: "getDiagnosticReportsState",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getDiagnosticReportsData.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getDiagnosticReportsData.fulfilled, (state, action) => {
      state.loading = false;
      state.data = [...(action.payload?.data as any)];
      state.paging = _.cloneDeep(action.payload?.paging as Paging);
    });
    builder.addCase(getDiagnosticReportsData.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown;
    });
  },
});

export const getDiagnosticReportsByIDSlice = createSlice({
  name: "getDiagnosticReportsByIDState",
  initialState: initialStateByID,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getDiagnosticReportsDataByID.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getDiagnosticReportsDataByID.fulfilled, (state, action) => {
      state.loading = false;
      state.data = _.cloneDeep(action.payload as unknown as diagnosticReportsModel);
    });
    builder.addCase(getDiagnosticReportsDataByID.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown as string;
    });
  },
});

export const postDiagnosticReportsSlice = createSlice({
  name: "postDiagnosticReportsState",
  initialState: {
    data: {},
    loading: false,
    error: "",
  },
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(postDiagnosticReportsData.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(postDiagnosticReportsData.fulfilled, (state, action) => {
      state.loading = false;
      state.data = _.cloneDeep(action.payload as diagnosticReportsModel);
    });
    builder.addCase(postDiagnosticReportsData.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown as string;
    });
  },
});
export const updateDiagnosticReportsSlice = createSlice({
  name: "updateDiagnosticReports",
  initialState: {
    data: {},
    loading: false,
    error: "" as unknown,
  },
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(updateDiagnosticReportsData.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(updateDiagnosticReportsData.fulfilled, (state, action) => {
      state.loading = false;
    });
    builder.addCase(updateDiagnosticReportsData.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown;
    });
  },
});

export const getDiagnosticReportsReducer = getDiagnosticReportsSlice.reducer;
export const postDiagnosticReportsReducer = postDiagnosticReportsSlice.reducer;
export const getDiagnosticReportsByIDReducer =
  getDiagnosticReportsByIDSlice.reducer;
export const updateDiagnosticReportsReducer =
  updateDiagnosticReportsSlice.reducer;
