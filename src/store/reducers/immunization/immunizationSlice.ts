import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import * as _ from "lodash";
import { immunizationModel } from "./../../../models/immunization";
import {
  GetRequestType,
  PostRequestType,
  Paging,
  PatchRequestType,
  DeleteRequestType,
} from "../../../models/shared/basePagedResponse";
import { immunizationService } from "../../../services/agent";

// interface PostRequestType {
//     endpoint: string;
//     body: {
//         id: string;
//         category: string;
//         channel: string;
//         value: string;
//         description: string;
//     };
// }

// interface GetRequestType{
//     endpoint: string;
//     params?: object;
// }

// interface Immunization{
//     id: string;
//     status: "completed" | "pending" ;
//     statusReason?: string;
//     vaccineCode: {
//         coding: {
//             system: string;
//             code: string;
//         }[];
//         text: string;
//     };
//     occurrenceDateTime:string;
//     lotNumber: string;
//     expirationDate: string;
//     site: {
//         coding: {
//             system: string;
//             code: string;
//             display: string;
//         }[];
//     };
//     route: {
//         coding: {
//             system: string;
//             code: string;
//             display: string;
//         }[];
//     };
//     doseQuantity:{
//         value: number;
//         code: string;
//     };
//     note: {
//         text: string;
//     }[];

// }

// type Paging = {
//     page: number;
//     size: number;
//     total: number
// }

// interface ResponseType{
//     data: Immunization[];
//     paging: Paging;
// }

// const initialState = {

//     data: [
//         {
//             id:'',
//             status: "pending",
//             vaccineCode:{
//                 coding:[
//                     {
//                         system:'',
//                         code:'',
//                     }
//                 ],
//                 text:'',
//             },
//             lotNumber: '',
//             expirationDate: '',
//             doseQuantity:{
//                 value: 0,
//                 code: '',
//             },
//             note: [
//                 {
//                     text:''
//                 }
//             ]
//         }
//     ] as Immunization[],
//     paging:{
//         page: 0,
//         size: 0,
//         total: 0,
//     } as Paging,
//     loading: false,
//     error: '',

// }

// const initialStateByID = {
//     data:{
//             id:'',
//             status: "pending",
//             vaccineCode:{
//                 coding:[
//                     {
//                         system:'',
//                         code:'',
//                     }
//                 ],
//                 text:'',
//             },

//             occurrenceDateTime: '',
//             site:{
//                 coding:[
//                     {
//                         system:'',
//                         code:'',
//                         display:'',
//                     }
//                 ],
//                 text:'',
//             },
//             route:{
//                 coding:[
//                     {
//                         system:'',
//                         code:'',
//                         display:'',
//                     }
//                 ],
//                 text:'',
//             },
//             lotNumber: '',
//             expirationDate: '',
//             doseQuantity:{
//                 value: 0,
//                 code: '',
//             },
//             note: [
//                 {
//                     text:''
//                 }
//             ]
//         } as Immunization,
//      loading: false,
//      error: '',
//  }
// export const getImmunization = createAsyncThunk(
//     "patient/immunizations",
//     async({endpoint, params}:GetRequestType) => {
//         try{
//             const access_token = localStorage.getItem('access_token');
//             const {data} = await axios.get<ResponseType>(
//                 `https://fhir-service.herokuapp.com/api/${endpoint}`,
//                 {
//                     headers: {
//                         Accept: 'application/json',
//                         Authorization: `Bearer ${access_token}`,
//                     },
//                     params: {...params}
//                 });
//             console.log(data)
//             return data;
//         }catch (err) {console.error(err)}
//     }
// )
// export const getImmunizationByID = createAsyncThunk(
//     "patient/immunizationByID",
//     async({endpoint, params}:GetRequestType) => {
//         try{
//             const access_token = localStorage.getItem('access_token');
//             const {data} = await axios.get<ResponseType>(
//                 `https://fhir-service.herokuapp.com/api/${endpoint}`,
//                 {
//                     headers: {
//                         Accept: 'application/json',
//                         Authorization: `Bearer ${access_token}`,
//                     },
//                     params: {...params}
//                 });
//             return data;
//         }catch (err) {console.error(err)}
//     }
// )
// export const immunizationSlice = createSlice({
//     name: 'immunizations',
//     initialState,
//     reducers:{},
//     extraReducers:(builder) => {
//         builder.addCase(getImmunization.pending,
//             (state)=>{
//                 state.loading = true
//             }
//         )
//         builder.addCase(getImmunization.fulfilled,
//             (state, action)=>{
//             // state.loading = false;
//             // if (state.paging.page === 0)
//             //     state.data = _.cloneDeep(action.payload?.data as unknown as Immunization[]);
//             // else if (state.paging.page < (action.payload?.paging.page as number))
//             //     state.data.push(..._.cloneDeep(action.payload?.data as unknown as Immunization[]));
//             // else if (state.paging.size != (action.payload?.paging.size as number))
//             //     state.data = _.cloneDeep(action.payload?.data as unknown as Immunization[]);
//             // state.paging = _.cloneDeep(action.payload?.paging as unknown as Paging)
//                 state.loading = false;
//                 state.data = _.cloneDeep(action.payload?.data as unknown as Immunization[]);
//                 state.paging = _.cloneDeep(action.payload?.paging as unknown as Paging);
//         }
//         )
//         builder.addCase(getImmunization.rejected,
//             (state,action)=>{
//             state.loading = false;
//             state.error = action.payload as unknown as string
//             }
//         )
//     }
// })

// export const immunizationByIDSlice = createSlice({
//     name: 'immunizations',
//     initialState: initialStateByID,
//     reducers:{},
//     extraReducers:(builder) => {
//         builder.addCase(getImmunizationByID.pending,
//             (state)=>{
//             state.loading = true
//             }
//         )
//         builder.addCase(getImmunizationByID.fulfilled,
//             (state, action)=>{
//             state.loading = false;
//             state.data = _.cloneDeep(action.payload as unknown as Immunization)
//             }
//         )
//         builder.addCase(getImmunizationByID.rejected,
//             (state,action)=>{
//             state.loading = false;
//             state.error = action.payload as unknown as string
//             }
//         )
//     }
// })

// export const immunizationReducer = immunizationSlice.reducer
// export const immunizationByIDReducer = immunizationByIDSlice.reducer

interface ImmunizationState {
  data: immunizationModel[];
  paging: Paging;
  loading: boolean;
  error: unknown;
}

const initialState: ImmunizationState = {
  data: [],
  paging: {
    page: 0,
    size: 0,
    total: 0,
  },
  loading: false,
  error: "",
};

const initialStateByID = {
  data: {} as immunizationModel,
  loading: false,
  error: "",
};

export const getImmunizationData = createAsyncThunk(
  "patient/getImmunization",
  async ({ params }: GetRequestType) => {
    return await immunizationService.getList({ params });
  }
);

export const getImmunizationDataByID = createAsyncThunk(
  "patient/getImmunizationByID",
  async (filter: string) => {
    return await immunizationService.getById(filter);
  }
);

export const postImmunizationData = createAsyncThunk(
  "patient/postImmunization",
  async ({ body }: PostRequestType) => {
    return await immunizationService.post({ body });
  }
);

export const updateImmunizationData = createAsyncThunk(
  "patient/updateImmunization",
  async ({ id, body }: PatchRequestType) => {
    return await immunizationService.update({ body }, id);
  }
);

export const deleteImmunizationData = createAsyncThunk(
  "patient/deleteImmunizationByID",
  async ({ id }: DeleteRequestType) => {
    return await immunizationService.delete(id);
  }
);

export const getImmunizationSlice = createSlice({
  name: "getImmunizationState",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getImmunizationData.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getImmunizationData.fulfilled, (state, action) => {
      state.loading = false;
      state.data = [...(action.payload?.data as any)];
      state.paging = _.cloneDeep(action.payload?.paging as Paging);
    });
    builder.addCase(getImmunizationData.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown;
    });
  },
});

export const getImmunizationByIDSlice = createSlice({
  name: "getImmunizationByIDState",
  initialState: initialStateByID,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getImmunizationDataByID.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getImmunizationDataByID.fulfilled, (state, action) => {
      state.loading = false;
      state.data = _.cloneDeep(action.payload as unknown as immunizationModel);
    });
    builder.addCase(getImmunizationDataByID.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown as string;
    });
  },
});

export const postImmunizationSlice = createSlice({
  name: "postImmunizationState",
  initialState: {
    data: {},
    loading: false,
    error: "",
  },
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(postImmunizationData.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(postImmunizationData.fulfilled, (state, action) => {
      state.loading = false;
      state.data = _.cloneDeep(action.payload as immunizationModel);
    });
    builder.addCase(postImmunizationData.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown as string;
    });
  },
});
export const updateImmunizationSlice = createSlice({
  name: "updateImmunization",
  initialState: {
    data: {},
    loading: false,
    error: "" as unknown,
  },
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(updateImmunizationData.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(updateImmunizationData.fulfilled, (state, action) => {
      state.loading = false;
    });
    builder.addCase(updateImmunizationData.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown;
    });
  },
});

export const getImmunizationReducer = getImmunizationSlice.reducer;
export const postImmunizationReducer = postImmunizationSlice.reducer;
export const getImmunizationByIDReducer = getImmunizationByIDSlice.reducer;
export const updateImmunizationReducer = updateImmunizationSlice.reducer;
