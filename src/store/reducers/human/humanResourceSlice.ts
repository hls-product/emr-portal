import { Action, createSlice, Dispatch, PayloadAction } from "@reduxjs/toolkit";
import axios from "axios";
import { RootState } from "../../Store";

export interface ResponseType {
  data: Array<HumanState>;
  paging: Paging;
}

export interface Paging {
  total: number;
  page: number;
  size: number;
}

export interface HumanName {
  use: string;
  family: string;
  given: Array<string>;
  prefix: Array<string>;
}
export interface Telecom {
  system: string;
  value: string;
}

export interface Code {
  coding: [{ code: string }];
  text: string;
}

export interface Qualification {
  issuer: {
    reference: string;
  };
  code: Code;
}

export interface Address {
  use: string;
  line: Array<string>;
  city: string;
  text: string;
}

export interface HumanState {
  id: string;
  gender: string;
  name: HumanName[];
  telecom: Telecom[];
  qualification: Qualification[];
  address: Address[];
}

export interface InitialState {
  loading: boolean;
  staffList: HumanState[];
  paging: Paging;
  profileStaff: DataStaff;
  staffEdit: HumanState;
}
export interface DataStaff {
  name: {
    use: string;
    family: string;
    given: string[];
  }[];

  gender: string;
  birthDate: string;
  telecom: {
    system: string;
    value: string;
  }[];
  address: {
    use: string;
    text: string;
  }[];
  qualification: {
    code: {
      text: string;
    };
    issuer: {
      reference: string;
    };
  }[];
}

const initialState: InitialState = {
  loading: false,
  staffList: [
    {
      id: "",
      name: [
        {
          use: "official",
          family: "",
          given: [""],
          prefix: [""],
        },
      ],
      gender: "",
      telecom: [
        {
          system: "phone",
          value: "",
        },
      ],
      qualification: [
        {
          code: {
            coding: [{ code: "BS" }],
            text: "",
          },
          issuer: {
            reference: "",
          },
        },
      ],
      address: [
        {
          use: "work",
          line: [""],
          city: "",
          text: "",
        },
      ],
    },
  ] as HumanState[],
  paging: {
    page: 1,
    size: 5,
    total: 10,
  },
  profileStaff: {
    name: [
      {
        use: "",
        family: "",
        given: [""],
      },
    ],
    gender: "Male",
    birthDate: "1999-07-19",
    telecom: [
      {
        system: "",
        value: "",
      },
    ],
    address: [
      {
        use: "",
        text: "",
      },
    ],
    qualification: [
      {
        code: {
          text: "",
        },
        issuer: {
          reference: "",
        },
      },
    ],
  },
  staffEdit: {
    id: "",
    name: [
      {
        use: "official",
        family: "",
        given: [""],
        prefix: [""],
      },
    ],
    telecom: [
      {
        system: "phone",
        value: "",
      },
    ],
    qualification: [
      {
        code: {
          coding: [{ code: "BS" }],
          text: "",
        },
      },
    ],
    address: [
      {
        use: "work",
        line: [""],
        city: "",
        text: "",
      },
    ],
  } as HumanState,
};

const humanResourceSlice = createSlice({
  name: "human-resource",
  initialState,
  reducers: {
    loading(state) {
      state.loading = true;
    },
    fetchStaffListSuccess(state, action: PayloadAction<ResponseType>) {
      state.staffList = action.payload.data;
      state.paging = action.payload.paging;
    },
    loadingDone(state) {
      state.loading = false;
    },
    viewProfileStaffSuccess(state, action: PayloadAction<DataStaff>) {
      state.profileStaff = action.payload;
    },
    getStaffEditSuccess(state, action: PayloadAction<HumanState>) {
      state.staffEdit = action.payload;
    },
  },
});

//actions
export const humanResourceActions = humanResourceSlice.actions;

export const getStaffList =
  (page: number, size: number, accessToken: string) =>
  async (dispatch: Dispatch<Action>) => {
    dispatch(humanResourceActions.loading());
    try {
      const response = await axios.get(
        `https://fhir-service.herokuapp.com/api/practitioners?page=${page}&size=${size}`,
        {
          headers: { Authorization: "Bearer " + accessToken },
        }
      );
      dispatch(humanResourceActions.fetchStaffListSuccess(response.data));
    } catch (error) {
    } finally {
      dispatch(humanResourceActions.loadingDone());
    }
  };

export const createStaff =
  (dataStaff: DataStaff) => async (dispatch: Dispatch<Action>) => {
    dispatch(humanResourceActions.loading());
    try {
      const res = await axios.post(
        "https://fhir-service.herokuapp.com/api/practitioners",
        { ...dataStaff }
      );
      dispatch(humanResourceActions.viewProfileStaffSuccess(res.data));
    } catch (error) {
      console.log(error);
    } finally {
      dispatch(humanResourceActions.loadingDone());
    }
  };

export const getStaffEdit =
  (staff: HumanState) => (dispatch: Dispatch<Action>) => {
    try {
      dispatch(humanResourceActions.getStaffEditSuccess(staff));
    } catch (error) {
      console.log(error);
    }
  };

export const updateStaff =
  (id: string, dataStaff: DataStaff) => async (dispatch: Dispatch<Action>) => {
    dispatch(humanResourceActions.loading());
    try {
      const res = await axios.patch(
        `https://fhir-service.herokuapp.com/api/practitioners/${id}`,
        { ...dataStaff },
        {
          headers: {
            ContentType: "application/json",
          },
        }
      );
      dispatch(humanResourceActions.viewProfileStaffSuccess(res.data));
    } catch (error) {
      console.log(error);
    } finally {
      dispatch(humanResourceActions.loadingDone());
    }
  };

//selectors
export const selectLoading = (state: RootState) =>
  state.humanResourceReducer.loading;

export const selectStaffList = (state: RootState) =>
  state.humanResourceReducer.staffList;

export const selectPaging = (state: RootState) =>
  state.humanResourceReducer.paging;

export const selectProfileStaff = (state: RootState) =>
  state.humanResourceReducer.profileStaff;

export const selectStaffEdit = (state: RootState) =>
  state.humanResourceReducer.staffEdit;

//reducer
export const humanResourceReducer = humanResourceSlice.reducer;
