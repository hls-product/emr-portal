import { ISOLanguage, Questionnaire } from "@covopen/covquestions-js";
import { QuestionnaireBaseData } from "../models/question";

const rootUrl = "http://localhost:8001/api";
const urlSuffix = ".json";

export async function getAllQuestionnaires(): Promise<QuestionnaireBaseData[]> {
  let url = rootUrl + "/questionnaires";
  // if (process.env.NODE_ENV !== "production") {
  url += ".json";

  // } else {
  // url += urlSuffix;
  // }
  let response = await fetch(url);
  if (response.ok) {
    return await response.json();
  }
  return [];
}

export async function getQuestionnaireByIdVersionAndLanguage(
  id: string,
  version: number,
  language: ISOLanguage
): Promise<Questionnaire | undefined> {
  const response = await fetch(
    `${rootUrl}/questionnaires/${id}/${version}/${language}${urlSuffix}`
  );
  if (response.ok) {
    return await response.json();
  }
  return undefined;
}

export async function getInstructions(): Promise<string | undefined> {
  const response = await fetch(`./INSTRUCTIONS.md`);
  if (response.ok) {
    return await response.text();
  }
  return undefined;
}
