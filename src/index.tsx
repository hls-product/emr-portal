/* Libs */
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { CssBaseline } from "@mui/material";
import { BrowserRouter as Router } from "react-router-dom";
import { ThemeProvider } from "@mui/material";
import {ReactKeycloakProvider} from '@react-keycloak/web'
import {keycloak} from '../src/components/Keycloak'
/* Components */
// import {KcApp, defaultKcProps, getKcContext} from 'keycloakify'
// import {css} from 'tss-react/@emotion/css'
import App from "./app";
import store from './store/Store'
/* Styles */
import { theme } from "./theme";

/* ? */
import reportWebVitals from "./reportWebVitals";


// const {kcContext} = getKcContext(
//   {
//   "mockPageId": "login.ftl"
//   }
// );

// const myClass = css({
//   color: 'red'
// })

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <ReactKeycloakProvider authClient={keycloak}>
          <Router>
            <CssBaseline>
              <App />
            </CssBaseline>
          </Router>
        </ReactKeycloakProvider>
      </ThemeProvider>
    </Provider> 
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
