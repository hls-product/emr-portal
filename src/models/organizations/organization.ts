import { Contact } from './contact';
import { Identifier } from "../shared/identifier";
import { Telecom } from "./telecom";
import { Type } from "../shared/type";
import { Address } from '../dataTypes/address';

export interface Organization {
	id?: string;
	identifier: Identifier[];
	active: boolean;
	type: Type[];
	name: string;
	alias?: string[];
	telecom: Telecom[];
	address: Address[];
	partOf?: any;
	contact?: Contact[];
	endpoint?: any;
}