export interface Telecom {
	system: string;
	value: string;
	use: string;
	rank?: any;
	period?: any;
}
