import { Asserter, BodySite, Category, ClinicalStatus, CodeableConcept, Encounter, Evidence, Identifier, Recorder, Severity, Subject, VerificationStatus } from "../shared/dataType";

export interface ConditionModel {
	id?: string;
	identifier?: Identifier[];
	clinicalStatus?: ClinicalStatus;
	verificationStatus?: VerificationStatus;
	category?: Category[];
	severity?: Severity;
	code?: CodeableConcept;
	bodySite?: BodySite[];
	subject?: Subject;
	encounter?: Encounter;
	onsetAge?: any;
	onsetDateTime?: Date;
	onsetPeriod?: any;
	onsetRange?: any;
	onsetString?: string;
	abatementAge?: any;
	abatementDateTime?: Date;
	abatementPeriod?: any;
	abatementRange?: any;
	abatementString?: string;
	recordedDate?: string;
	recorder?: Recorder;
	asserter?: Asserter;
	stage?: any;
	evidence?: Evidence[];
	note?: string;
}
