import { ROLES } from "./../../constants/Role";

export interface RealmAccess {
  roles: ROLES[];
}
