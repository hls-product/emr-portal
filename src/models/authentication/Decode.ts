import { RealmAccess } from "./RealmAccess";

export interface Decode {
  name: string;
  email: string;
  given_name: string;
  family_name: string;
  preferred_username: string;
  sub: string;
  realm_access: RealmAccess;
}
