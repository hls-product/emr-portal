import {Address, CodeableConcept, ContactPoint, Education, HumanName, Identifier, Money, Note, Performer, Period, Quantity, Reference} from '../shared/dataType'

export interface IImmunization{
    id?: string;
    identifier?: Identifier[];
    status?: "completed" | "pending" | "uncompleted" | "unknown";
    statusReason? :string;
    vaccineCode? :CodeableConcept;
    patient?: Reference;
    encounter?: Reference;
    occurrenceDateTime?: string;
    occurrenceString?: string;
    recorded?: string;
    primarySource? :boolean;
    reportOrigin? : string;
    location?: Reference;
    manufacturer?: Reference;
    lotNumber?: string;
    expirationDate?: string;
    site?: CodeableConcept;
    route?: CodeableConcept;
    doseQuantity?: Quantity;
    performer?: Performer[];
    note?: Note[];
    reasonCode?: CodeableConcept[];
    reasonReference?: string;
    isSubpotent?: boolean;
    subpotentReason?: string;
    education?: Education[];
    programEligibility?: CodeableConcept[];
    fundingSource?: CodeableConcept;
    reaction?: string;
    protocolApplied?: string;
}