import { STATE } from "../../constants/State";

export interface Notification {
  message: string;
  type: STATE | null;
}
