import {
    CodeableConcept,
    Identifier,
    Period,
    Reference,
  } from "../shared/dataType";
  
  export interface Flag {
    id?: string;
    identifier?: Identifier[];
    status?: string;
    category?: CodeableConcept[];
    code?: CodeableConcept;
    subject?: Reference;
    period?: Period;
    encounter?: Reference;
    author?: Reference;
  }
  