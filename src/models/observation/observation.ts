export {};
import {
  Address,
  Attachment,
  CodeableConcept,
  ContactPoint,
  HumanName,
  Identifier,
  Period,
  Reference,
} from "../shared/dataType";

export interface Coding {
  system?: string;
  version?: unknown;
  code?: string;
  display: string;
  userSelected?: unknown;
}

export interface Subject {
  reference?: string;
  type?: string;
  identifier?: unknown;
  display: string;
}

export interface ValueQuantity {
  type?: string;
  value: number;
  comparator?: unknown;
  unit?: string;
  system?: string;
  code?: string;
}

export interface Observation {
  id?: string;
  identifier?: unknown;
  basedOn?: unknown;
  partOf?: unknown;
  status?: string;
  category?: CodeableConcept[];
  code?: CodeableConcept;
  subject?: Subject;
  focus?: unknown;
  encounter?: unknown;
  effectivePeriod?: Period;
  issued?: string;
  performers?: unknown;
  value?: ValueQuantity;
  dataAbsentReason?: unknown;
  interpretation?: CodeableConcept[];
  notes?: unknown;
  bodySite?: unknown;
  method?: CodeableConcept;
  specimen?: unknown;
  device?: unknown;
  referenceRange?: unknown;
  hasMember?: unknown;
  derivedFrom?: unknown;
  components?: unknown;
}
