import {Address, CodeableConcept, Coding, ContactPoint, HumanName, Identifier, ImmuIden, Money, Payee, Period, Quantity, Reference} from '../shared/dataType'

export interface EncounterModel{
    id?: string;
    identifier?: Identifier[];
    status?: string;
    type?: CodeableConcept[];
    priority?: CodeableConcept;
    subject?: Reference;
    participant?: Address[];
    length?: Quantity;
    reasonCode: CodeableConcept[];
    hospitalization?: {
        preAdmissionIdentifier?: ImmuIden;
        admitSource?: CodeableConcept;
        dischargeDisposition?: CodeableConcept;
    };
    serviceProvider?: Reference;
}