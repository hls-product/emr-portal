import {
  Address,
  Attachment,
  CodeableConcept,
  ContactPoint,
  HumanName,
  Identifier,
  Period,
  Reference,
} from "../shared/dataType";

export interface Patient {
  id?: string;
  identifier?: Identifier[];
  active?: boolean;
  name?: HumanName[];
  telecom?: ContactPoint[];
  gender?: "male" | "female" | "other" | "unknown";
  birthDate?: string;
  deceased?: {
    deceasedBoolean: boolean;
    deceasedDateTime: string;
  };
  address?: Address[];
  maritalStatus?: CodeableConcept;
  multipleBirth?: {
    multipleBirthBoolean?: boolean;
    multipleBirthInteger?: number;
  };
  photo?: Attachment[];
  contact?: {
    relationship?: CodeableConcept[];
    name?: HumanName;
    telecom?: ContactPoint[];
    address?: Address;
    gender?: "male" | "female" | "other" | "unknown";
    organization?: Reference;
    period?: Period;
  }[];
  communication?: {
    language: CodeableConcept;
    preferred?: boolean;
  }[];
  generalPractitioner?: Reference[];
  managingOrganization?: Reference;
  link?: {
    other: Reference;
    type: "replaced-by" | "replaces" | "refer" | "seealso";
  }[];
}
