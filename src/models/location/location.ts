import { Address, CodeableConcept, Coding, ContactPoint, Identifier, Reference } from "../shared/dataType";

export interface ILocation {
    id?:string;
    identifier?: Identifier[];
    status?: 'active' | 'suspended' | 'inactive';
    operationalStatus?: Coding;
    name?: string;
    alias?: string[];
    description?: string;
    mode?: 'instance' | 'kind';
    type?: CodeableConcept[];
    telecom?: ContactPoint[];
    address?: Address;
    physicalType?: CodeableConcept;
    position?:{
        longitude: number;
        latitude: number;
        altitude?: number;
    };
    managingOrganization?: Reference;
    partOf?: Reference;
    hoursOfOperation?: {
        daysOfWeek?: Array<"mon" | "tue" | "wed" | "thu" | "fri" | "sat" | "sun">;
        allDay?: boolean;
        openingTime?: string;
        closingTime?: string;
    }[];
    availabilityExceptions?: string;
    endpoint?:Reference[];
}

