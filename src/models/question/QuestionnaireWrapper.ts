import { EditorQuestionnaire } from "./EditorQuestionnaire";

export interface QuestionnaireWrapper {
  questionnaire: EditorQuestionnaire;
  errors: {
    isJsonModeError: boolean;
    formEditor: {
      meta: boolean;
      questions: { [key: number]: boolean };
      resultCategories: { [key: number]: boolean };
      variables: { [key: number]: boolean };
      testCases: { [key: number]: boolean };
    };
  };
}
