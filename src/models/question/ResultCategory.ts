import { EditorResult, EditorResultCategory } from "./EditorQuestionnaire";

type ResultInStringRepresentation = Omit<EditorResult, "expression"> & {
  expression: string;
};
export type ResultCategoryInStringRepresentation = Omit<
  EditorResultCategory,
  "results"
> & {
  results: ResultInStringRepresentation[];
};
