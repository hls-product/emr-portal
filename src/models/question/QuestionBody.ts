export interface Meta {
  availableLanguages: string[];
  author: string;
  creationDate: string;
  description?: string;
  regions?: string[];
}

export interface Option {
  scores: string;
  text: string;
  value: string;
}

export interface NumericOption {
  defaultValue: number;
  max: number;
  min: number;
  step: number;
}

export interface Result {
  expression: string;
  id: string;
  text: string;
}

export interface Questions {
  options?: Option[];
  type: "select" | "multiselect" | "number" | "boolean" | "date" | "text";
  id: string;
  text: string;
  optional?: boolean;
  details?: string;
  enableWhenExpression?: string;
  numberOptions?: NumericOption;
}

export interface ResultCategory {
  results: Result[];
  description: string;
  id: string;
}

export interface Variable {
  expression: string;
  id: string;
}

export interface QuestionBody {
  schemaVersion: "1_2";
  version: number;
  language: string;
  title: string;
  meta: Meta;
  questions: Questions[];
  resultCategories: ResultCategory[];
  variables?: Variable[];
}

export type QuestionBodyResponse = QuestionBody & {
  id: string;
};
