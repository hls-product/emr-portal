import {
  Annotation,
  CodeableConcept,
  ContactPoint,
  Identifier,
  Period,
  Reference,
} from "../shared/dataType";

export interface Participant {
  role?: CodeableConcept[];
  member?: Reference;
  onBehalfOf?: Reference;
  period?: Period;
}

export interface CareTeam {
  id?: string;
  identifier?: Identifier[];
  status?: string;
  category?: CodeableConcept[];
  name?: string;
  subject?: Reference;
  encounter?: Reference;
  period?: Period;
  participant?: Participant[];
  reasonCode?: CodeableConcept[];
  reasonReference?: Reference[];
  managingOrganization?: Reference[];
  telecom?: ContactPoint[];
  note?: Annotation[];
}
