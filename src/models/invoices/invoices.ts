import { Annotation, CodeableConcept, Identifier, Money, Reference } from "../shared/dataType";

export interface IInvoice { 
    id: string;
    identifier?: Identifier[];
    status: "draft" | "issued" | "balanced" | "cancelled" | "entered-in-error";
    cancelledReason?: string;
    type? : CodeableConcept;
    subject?: Reference;
    recipient?: Reference;
    date?: string;
    participant?: {
        role?: CodeableConcept;
        actor: Reference;
    }[];
    issuer?: Reference;
    account?: Reference;
    lineItem?: {
        sequence?: number;
        chargeItem: {
            chargeItemReference: Reference;
            chargeItemCodeableConcept: CodeableConcept;
        };
        priceComponent?: {
            type: "base" | "surcharge" | "deduction" | "discount" | "tax" | "informational";
            code?: CodeableConcept;
            factor?: number;
            amount?: Money;
        }[]
    }[];
    totalPriceComponent?: {
        type: "base" | "surcharge" | "deduction" | "discount" | "tax" | "informational";
        code?: CodeableConcept;
        factor?: number;
        amount?: Money;
    }[];
    totalNet?: Money;
    totalGross?: Money; 
    paymentTerms?: string;
    note?: Annotation[]; 
}