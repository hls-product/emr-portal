import {Address, CodeableConcept, ContactPoint, HumanName, Identifier, Money, Period, Quantity, Reference} from '../shared/dataType'

export interface RRelatedPatient{
    id?: string;
    identifier?: Identifier[];
    active?: boolean;
    patientId?: string;
    relationship?: CodeableConcept[];
    name?: HumanName[];
    telecom?: ContactPoint[];
    gender?: string;
    birthDate?: string;
    address?: Address[];
    period?: Period;
    communication?: string;
}