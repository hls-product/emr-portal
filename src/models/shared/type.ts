import { Coding } from "./coding";

export interface Type {
	coding?: Coding[];
	text?: string;
}