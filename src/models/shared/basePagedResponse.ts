export interface ListResponse<T> {
  data: T[];
  paging: Paging;
}

export interface SingleResponse<T> {
  data: T;
}

export interface Paging {
  page: number;
  size: number;
  total: number;
}

export interface GetRequestBase<T> {
  params?: T;
}
export interface PostRequestBase<T> {
  body: T;
}
export interface PatchRequestBase<T> {
  body: T;
  id: string;
}

//will be obsolete in the future
export interface GetRequestType {
  params?: object;
}

export interface PostRequestType {
  body: object;
}

export interface PatchRequestType {
  id: string;
  body: object;
}

export interface DeleteRequestType {
  id: string;
}
