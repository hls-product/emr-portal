import { Code } from "./code";

export type Attachment = {
  contentType?: string;
  language?: string;
  data?: string;
  url?: string;
  size?: number;
  hash?: string;
  title?: string;
  creation?: string;
};

export type Coding = {
  system?: string;
  version?: string;
  code?: string;
  display?: string;
  userSelected?: boolean;
};

export type CodeableConcept = {
  coding?: Coding[];
  text?: string;
};

export type Quantity = {
  value?: number;
  comparator?: "<" | "<=" | ">=" | ">";
  unit?: string;
  system?: string;
  code?: string;
};

export type Money = {
  value?: number;
  currency?: string;
};

export type Range = {
  low?: Quantity;
  high?: Quantity;
};

export type Ratio = {
  numerator?: Quantity;
  denominator?: Quantity;
};

export type RatioRange = {
  lowNumerator?: Quantity;
  highNumerator?: Quantity;
  denominator?: Quantity;
};

export type Period = {
  start?: string;
  end?: string;
};

export type SampledDara = {
  origin: Quantity;
  period: number;
  factor?: number;
  lowerLimit?: number;
  upperLimit?: number;
  dimensions: number;
  data?: string;
};

export type Identifier = {
  use?: "usual" | "official" | "temp" | "secondary" | "old";
  type?: CodeableConcept;
  system?: string;
  value?: string;
  period?: Period;
  assigner?: Reference | string;
};

export type Reference = {
  reference?: string;
  type?: string;
  identifier?: Identifier;
  display?: string;
};

export type HumanName = {
  use?:
    | "usual"
    | "official"
    | "temp"
    | "nickname"
    | "anonymous"
    | "old"
    | "maiden";
  text?: string;
  family?: string;
  given?: string[];
  prefix?: string[];
  suffix?: string[];
  period?: Period;
};

export type Address = {
  use?: "home" | "work" | "temp" | "old" | "billing";
  type?: "postal" | "physical" | "both";
  text?: string;
  line?: string[];
  city?: string;
  district?: string;
  state?: string;
  postalCode?: string;
  country?: string;
  period?: Period;
};

export type ContactPoint = {
  system?: "phone" | "fax" | "email" | "pager" | "url" | "sms" | "other";
  value?: string;
  use?: "home" | "work" | "temp" | "old" | "mobile";
  rank?: number;
  period?: Period;
};

export type Duration = Quantity;

export type Timing = {
  event?: string[];
  repeat?: {
    bounds?: {
      boundsDuration: Duration;
      boundsRange: Range;
      boundsPeriod: Period;
    };
    count: number;
    countMax: number;
    duration: number;
    durationMax: number;
    durationUnit: "s" | "min" | "h" | "d" | "wk" | "mo" | "a";
    frequency?: number;
    frequencyMax?: number;
    period?: number;
    periodMax?: number;
    periodUnit?: "s" | "min" | "h" | "d" | "wk" | "mo" | "a";
    dayOfWeek?: Array<"mon" | "tue" | "wed" | "thu" | "fri" | "sat" | "sun">;
    timeOfDay?: string[];
    when?: string[];
    offset?: number;
  };
  code?: CodeableConcept;
};

export type Signature = {
  type: Coding[];
  when: string;
  who: Reference;
  onBehalfOf?: Reference;
  targetFormat?: string;
  sigFormat?: string;
  data?: string;
};

export type Annotation = {
  author?: {
    authorReference: Reference;
    authorString: string;
  };
  time?: string;
  text: string;
};

export type ImmuIden = {
  use?: string;
  type?: string;
  system?: string;
  value?: string;
  assignerOrganizationId?: string;
  period?: string;
};

export type Performer = {
  function?: CodeableConcept;
  actor?: Reference;
};

export type Note = {
  authorId?: string;
  authorOrg?: string;
  time?: string;
  text?: string;
};

export type Education = {
  documentType?: string;
  reference?: string;
  publicationDate?: string;
  presentationDate?: string;
};

export type Payee = {
  language?: string;
  preferred?: string;
};

export interface Type {
  coding?: Coding[];
  text?: string;
}

export interface Asserter {
  reference: string;
  type?: any;
  identifier?: any;
  display?: any;
}

export interface BodySite {
  coding: Coding[];
  text?: string;
}

export interface Category {
  coding: Coding[];
  text?: string;
}

export interface ClinicalStatus {
  coding: Coding[];
  text?: string;
}

export interface Encounter {
  reference: string;
  type?: any;
  identifier?: any;
  display?: string;
}

export interface Evidence {
  code: Code[];
  detail: EvidenceDetail[];
}

export interface EvidenceDetail {
  reference: string;
  type?: any;
  identifier?: any;
  display: string;
}

export interface Recorder {
  reference: string;
  type?: any;
  identifier?: any;
  display?: string;
}

export interface Severity {
  coding: Coding[];
  text?: string;
}

export interface Subject {
  reference: string;
  type?: any;
  identifier?: any;
  display: string;
}

export interface VerificationStatus {
  coding: Coding[];
  text?: string;
}
