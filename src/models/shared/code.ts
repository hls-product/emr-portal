import { Coding } from "./coding";

export interface Code {
	coding: Coding[];
	text?: string;
}