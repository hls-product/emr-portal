import { Type } from './type';
export interface Identifier {
	use?: any;
	type?: Type;
	system?: any;
	value: string;
	assignerOrganizationId?: any;
	period?: any;
}