
export interface Coding {
	system: string;
	version?: any;
	code: string;
	display?: any;
	userSelected?: any;
}
