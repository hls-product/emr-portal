import {Address, CodeableConcept, Coding, ContactPoint, HumanName, Identifier, Money, Payee, Period, Quantity, Reference} from '../shared/dataType'

export interface CClaims{
    id?: string;
    identifier?: Identifier[];
    status?: "draft" | "active" | "retired" | "unknown";
    type?: CodeableConcept;
    ruleset?: string;
    originalRuleset?: string;
    created?: string;
    target?: string;
    provider?: Reference;
    organization?: string;
    use?: string;
    priority?: Coding;
    fundsReserve?: string;
    enterer?: string;
    facility?: string;
    prescription?: Reference;
    originalPrescription: Reference;
    payee?: Payee;
    referral?: string;
    diagnosis?: Payee[];
    condition?: string;
    patient?: Reference;
    coverage?: string;
    exception?: string;
    school?: string;
    accident?: string;
    accidentType?: string;
    interventionException?: string;
    item?: Payee[];
    additionalMaterials?: string;
    missingTeeth?: string;
}