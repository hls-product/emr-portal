import {Address, CodeableConcept, Coding, ContactPoint, HumanName, Identifier, Money, Payee, Period, Quantity, Reference} from '../shared/dataType'

export interface DDiagnosticReports{
    id?: string;
    identifier?: Identifier[];
    basedOn?: Reference[];
    status?: string;
    category?: CodeableConcept[];
    code?: CodeableConcept;
    subject?: Reference;
    encounter?: string;
    effective?: string;
    issued?: string;
    performer?: Reference[];
    resultsInterpreter?: string;
    specimen?: string;
    result?: Reference[];
    imagingStudy?: string;
    media?: string;
    conclusion?: string;
    conclusionCode?: string;
}