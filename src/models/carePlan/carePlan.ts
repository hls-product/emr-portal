import {
  Annotation,
  CodeableConcept,
  Identifier,
  Period,
  Reference,
} from "../shared/dataType";

export interface CarePlan {
  id?: string;
  identifier?: Identifier[];
  instantiatesCanonical?: unknown;
  instantiatesUri?: unknown;
  basedOn?: Reference[];
  replaces?: Reference[];
  partOf?: Reference[];
  status?: string;
  intent?: string;
  category?: CodeableConcept[];
  title?: string;
  description?: string;
  subject?: Reference;
  encounter?: Reference;
  period?: Period;
  created?: string;
  author?: Reference;
  contributor?: Reference[];
  careTeam?: Reference[];
  addresses?: Reference[];
  supportingInfor?: Reference[];
  goal?: Reference[];
  activity?: {
    outcomeCodeableConcept?: CodeableConcept[];
    outcomeReference?: Reference[];
    progress?: Annotation[];
    reference?: Reference;
    detail?: unknown;
  }[];
  note?: Annotation[];
}
