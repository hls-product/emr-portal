import { Address, Attachment, CodeableConcept, ContactPoint, HumanName, Identifier, Period, Reference } from "../shared/dataType";

export interface practitionerModel {
  id?: string;
  identifier?: Identifier[];
  active?: boolean;
  name?: HumanName[];
  telecom?: ContactPoint[];
  address?: Address[];
  gender?: "male" | "female" | "other" | "unknown";
  birthDate? : string;
  photo?: Attachment[];
  qualification?: {
    identifier?: Identifier[];
    code: CodeableConcept;
    period?: Period;
    issuer?: Reference;
  }[];
  communication?: CodeableConcept[]
}
