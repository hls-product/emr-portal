import { DocumentReference } from "firebase/firestore";

export interface Message {
    id: string;
    content: string;
    contentType:"chat" | "attach" | "call" | "rating";
    timestamp: number;
    userSent: DocumentReference;
  }