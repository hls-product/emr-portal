import {Address, CodeableConcept, ContactPoint, HumanName, Identifier, Money, Period, Quantity, Reference} from '../shared/dataType'

export interface AAppointment{
    id?: string;
    userId?: string;
    status: "proposed" | "pending" | "booked" | "arrived" | "fulfilled" | "cancelled" | "noshow" | "entered-in-error" | "checked-in" | "waitlist";
    cancellationReason?: string;
    serviceCategory?: CodeableConcept[];
    serviceType?: CodeableConcept[];
    specialty?: CodeableConcept[];
    appointmentType?: CodeableConcept[];
    reasonCode?: CodeableConcept[];
    priority?: number;
    description?: string;
    minutesDuration?: number;
    slot?: [];
    start?: string;
    end?: string;
    created?: string;
    comment?: string;
    patientInstruction?: unknown;
    participant?:{
        type?:{
            coding?: {
                system?: string;
                code?: string;
            }[];
            text?: string;
        }[];
        actor?: {
            reference?: string;
            type?: string;
            display?: string;
        };
        // required?: string;
        // status?: string;
        required?: "required" | "optional" | "information-only";
        status?: "accepted" | "declined" | "tentative" | "needs-action";
        period?: string;
    }[];
    requestedPeriod?: {
        start: string;
        end: string;
    }[];
}