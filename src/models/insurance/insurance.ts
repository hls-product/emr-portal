import {Address, CodeableConcept, ContactPoint, HumanName, Identifier, Money, Period, Quantity, Reference} from '../shared/dataType'

export interface IInsurance{
    id?: string;
    identifier?: Identifier[];
    status?: "draft" | "active" | "retired" | "unknown";
    type?: CodeableConcept[];
    name?: string;
    alias?: string[];
    period?: Period;
    ownedBy?: Reference;
    administeredBy?: Reference;
    coverageArea?: Reference[];
    contact?: {
        purpose?: CodeableConcept;
        name?:HumanName;
        telecom?: ContactPoint[];
        address?: Address;
    }[];
    endpoint?: Reference[];
    network?: Reference[];
    coverage?: {
        type: CodeableConcept;
        network?: Reference[];
        benefit: {
            type: CodeableConcept;
            requirement:string;
            limit: {
                value: Quantity;
                code: CodeableConcept
            }
        }[]
    }[];
    plan?:{
        identifier?: Identifier[];
        type?: CodeableConcept;
        coverageArea?: Reference[];
        network?: Reference[];
        generalCost?: {
            type?: CodeableConcept;
            groupSize?: number;
            cost?: Money;
            comment?: string;
        }[];
        specificCost?: {
            category: CodeableConcept;
            benefit?: {
                type: CodeableConcept;
                cost?: {
                    type: CodeableConcept;
                    applicability?: CodeableConcept;
                    qualifiers?: CodeableConcept[];
                    value?: Quantity;
                }[]
            }[]
        }[]
    }[];
}