import { PagingRequest } from "../shared";

export type PatientGetRequest = PagingRequest & {
  name?: string;
  gender?: string;
  id?: string;
  disease?: string;
  status?: string;
  doctor?: string;
  phone?: string;
};

export type PatientPostRequest = {
  name: string;
  gender: string;
  phone: string;
  email: string;
  address: string;
  protectorName: string;
  protectorPhone: string;
  protectorEmail: string;
  protectorRelationship: string;
  insuranceNumber: string;
  insuranceExpiredDate: string;
  bloodGroup: string;
  allergies: string[];
  anamnesis: string[];
};
