import { PagingRequest } from "../shared";

export type InvoiceGetRequest = PagingRequest & {
    name?: string;
    gender?: string;
    id?: string;
    disease?: string;
    status?: string;
    doctor?: string;
    phone?: string;
  };