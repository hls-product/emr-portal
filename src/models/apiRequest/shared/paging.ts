export interface PagingRequest {
  page: number;
  size: number;
}
