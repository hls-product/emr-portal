import { PagingRequest } from "../shared";

export type PractitionerGetRequest = PagingRequest & {
  active?: boolean;
  address?: string;
  communication?: string;
  email?: string;
  name?: string;
  gender?: string;
  id?: string;
  phone?: string;
  
};

export type PractitionerPostRequest = {
//   name: string;
//   gender: string;
//   phone: string;
//   email: string;
//   address: string;
//   protectorName: string;
//   protectorPhone: string;
//   protectorEmail: string;
//   protectorRelationship: string;
//   insuranceNumber: string;
//   insuranceExpiredDate: string;
//   bloodGroup: string;
//   allergies: string[];
//   anamnesis: string[];
};
