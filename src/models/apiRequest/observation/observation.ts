import { PagingRequest } from "../shared";
import { ObservationType } from "../../../store/reducers/observations/observationSlice";
import dayjs, { Dayjs } from "dayjs";

export type ObservationGetRequest = PagingRequest & {
  name?: string;
  status?: string;
  classification?: string;
  periodStart?: Dayjs;
  periodEnd?: Dayjs;
  issue?: Dayjs;
  method?: string;
  result?: string;
};

export type ObservationPostRequest = ObservationType;
