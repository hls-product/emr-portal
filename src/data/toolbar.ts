import { Action } from "../components/toolbar";

export const question: Action[] = [
  {
    label: "Question",
    url: "/admin/questions",
  },
];

export const editHumnaResource: Action[] = [
  {
    label: "Basic Information",
    url: "/admin/human/edit",
  },
  {
    label: "Add More Details",
    url: "/admin/human/edit/edit-details",
  },
];
