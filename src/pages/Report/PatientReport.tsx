// Import React/React Router/Redux/Axios
import React from "react";

// Import MUI components
import AddIcon from "@mui/icons-material/Add";
import { Box, TextField, Typography } from "@mui/material";
import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";

// Import types
import { PatientGetRequest } from "../../models/apiRequest/patient";
import { patientModel } from "../../models/patients";

// Import reducers/api services
import { patientService } from "../../services/agent";

// Import common components
import * as XLSX from "xlsx";
import { RoundedButton } from "../../components/RoundedButton";
import { AdvancedSearch } from "../../components/Table/AdvancedSearch";
import { CustomTable } from "../../components/Table/Table";
import { HeadCell } from "../../components/Table/types";

//advance search
const advancedSearchProps = [
  {
    name: "patientName",
    label: "Patient Name",
  },
  {
    name: "id",
    label: "ID",
  },
  {
    name: "street",
    label: "Street",
  },
  {
    name: "city",
    label: "City",
  },
  {
    name: "state",
    label: "State",
  },
  {
    name: "zip",
    label: "Zip",
  },
  {
    name: "mobilePhone",
    label: "Mobile Phone",
  },
];
const headCells: HeadCell[] = [
  {
    id: "number",
    numeric: false,
    label: "No.",
  },
  {
    id: "date",
    numeric: false,
    label: "Date",
  },
  {
    id: "name",
    numeric: false,
    label: "Patient",
  },
  {
    id: "id",
    numeric: false,
    label: "ID",
  },
  {
    id: "street",
    numeric: false,
    label: "Street",
  },
  {
    id: "city",
    numeric: false,
    label: "City",
  },
  {
    id: "state",
    numeric: false,
    label: "State",
  },
  {
    id: "zip",
    numeric: false,
    label: "Zip",
  },
  {
    id: "mobilePhone",
    numeric: false,
    label: "Mobile Phone",
  },
];
export const PatientReport: React.FC = () => {
  const [isLoading, setIsLoading] = React.useState(false);
  const [data, setData] = React.useState<patientModel[]>([]);
  const [page, setPage] = React.useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = React.useState<number>(10);
  const [rowsCount, setRowsCount] = React.useState<number>(0);
  const [openAdvancedSearch, setOpenAdvancedSearch] = React.useState(false);
  // Search states
  const [searchValue, setSearchValue] = React.useState<string>("");
  const [debounceSeachValue, setDebounceSearchValue] =
    React.useState<string>("");
  const [filter, setFilter] = React.useState<{ [key: string]: string }>({});
  // Api calling functions
  //date picker
  const [startDate, setStartDate] = React.useState<string>(
    "2014-08-18T21:11:54"
  );
  const [endDate, setEndDate] = React.useState<string>("2015-08-18T21:11:54");

  const handleChangeStart = (value: any) => {
    setStartDate(value.toISOString().replace(/.\d+Z$/g, "Z") as string);
  };

  const handleChangeEnd = (value: any) => {
    setEndDate(value.toISOString().replace(/.\d+Z$/g, "Z") as string);
  };

  const getPatientList = async (filter?: PatientGetRequest) => {
    const params: PatientGetRequest = {
      ...filter,
      page: page + 1,
      size: rowsPerPage,
      name: debounceSeachValue,
    };
    setIsLoading(true);
    try {
      let { data, paging } = await patientService.getList({ params });
      setData(data);
      setRowsCount(paging.total);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      console.log("getPatientList error: ", error);
    }
  };
  // Event Handler
  const handleChangePage = (event: any, newPage: number) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event: any) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleClickAdvancedSearchOpen = () => {
    setOpenAdvancedSearch(true);
  };

  //Debounce for name search
  React.useEffect(() => {
    let timeout = setTimeout(() => {
      setDebounceSearchValue(searchValue);
    }, 500);

    return () => {
      clearTimeout(timeout);
    };
  }, [searchValue]);

  // Back to previous page if current page is empty after deleting an item
  React.useEffect(() => {
    let totalPages = Math.ceil(rowsCount / rowsPerPage);
    let currentPage = page + 1;
    if (currentPage > totalPages && totalPages > 0) {
      setPage(totalPages - 1);
    }
  }, [rowsCount]);

  React.useEffect(() => {
    getPatientList();
  }, [debounceSeachValue, rowsPerPage, page]);

  //map data
  const mapDataToRender = () => {
    const render = data.map((item, index) => ({
      number: index + 1,
      id: item.id as string,
      date: [item.address?.[0].period?.start, item.address?.[0].period?.end]
        .map((x) => x?.split("-")[0] || "N/A")
        .join(" - "),
      name:
        item.name?.[0].text ||
        item.name?.[0].given?.join(" ") + " " + item.name?.[0].family ||
        "-",
      street: item.address?.[0].text || "-",
      city: item.address?.[0].city || "-",
      state: item.address?.[0].state || "-",
      zip: item.address?.[0].postalCode || "-",
      mobilePhone:
        item.telecom
          ?.filter((x) => x.use === "mobile")
          .map((x) => x.value)
          .join(", ") || "-",
    }));
    return render;
  };

  //call api to export data

  //export data
  const exportToCSV = () => {
    const ws = XLSX.utils.json_to_sheet(mapDataToRender());
    const wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "PatientReport");
    XLSX.writeFile(wb, "PatientReport.xlsx");
  };

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        width: "100%",
        padding: "20px",
        backgroundColor: "white",
        borderRadius: "16px",
      }}
    >
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          gap: "8px",
        }}
      >
        <Typography variant="h2" children={"PATIENT REPORT LIST"} />
        <Box sx={{ display: "flex", gap: "8px" }}>
          <LocalizationProvider dateAdapter={AdapterDayjs}>
            <DesktopDatePicker
              label="Starting Date"
              inputFormat="MM/DD/YYYY"
              value={startDate}
              onChange={handleChangeStart}
              renderInput={(params) => (
                <TextField
                  {...params}
                  error={startDate ? startDate >= endDate : true}
                />
              )}
              InputProps={{
                sx: {
                  borderRadius: 3,
                  height: 48,
                  width: 150,
                },
              }}
            />
            <DesktopDatePicker
              label="Ending Date"
              inputFormat="MM/DD/YYYY"
              value={endDate}
              onChange={handleChangeEnd}
              renderInput={(params) => (
                <TextField
                  {...params}
                  error={endDate ? endDate <= startDate : true}
                />
              )}
              InputProps={{
                sx: {
                  borderRadius: 3,
                  height: 48,
                  width: 150,
                },
              }}
            />
          </LocalizationProvider>
          <RoundedButton
            variant="contained"
            onClick={handleClickAdvancedSearchOpen}
            children="Advanced Search"
          />
          <RoundedButton
            variant="contained"
            onClick={exportToCSV}
            endIcon={<AddIcon />}
            children={"Export Data"}
          />
        </Box>
      </Box>
      {openAdvancedSearch && (
        <AdvancedSearch
          props={advancedSearchProps}
          filter={filter}
          description="Find patients with..."
          onFilterChange={setFilter}
          onApply={getPatientList}
          onClose={setOpenAdvancedSearch}
        />
      )}
      <CustomTable
        title={"PATIENT REPORT LIST"}
        headcells={headCells}
        body={mapDataToRender()}
        paging={{
          page: page,
          rowsPerPage: rowsPerPage,
          rowsCount: rowsCount,
        }}
        loading={isLoading}
        toolbar={false}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Box>
  );
};
