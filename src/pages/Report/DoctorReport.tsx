// Import React/React Router/Redux/Axios
import React from "react";

// Import MUI components
import AddIcon from "@mui/icons-material/Add";
import { Box, TextField, Typography } from "@mui/material";
import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";

// Import types
import { practitionerModel } from "../../models/practitioners";

// Import reducers/api services
import { practitionersService } from "../../services/agent";

// Import common components
import * as XLSX from "xlsx";
import { RoundedButton } from "../../components/RoundedButton";
import { AdvancedSearch } from "../../components/Table/AdvancedSearch";
import { CustomTable } from "../../components/Table/Table";
import { HeadCell } from "../../components/Table/types";
import { PractitionerGetRequest } from "../../models/apiRequest/practitioner";
//advance search
const advancedSearchProps = [
  {
    name: "patientName",
    label: "Patient Name",
  },
  {
    name: "id",
    label: "ID",
  },
  {
    name: "street",
    label: "Street",
  },
  {
    name: "city",
    label: "City",
  },
  {
    name: "state",
    label: "State",
  },
  {
    name: "zip",
    label: "Zip",
  },
  {
    name: "mobilePhone",
    label: "Mobile Phone",
  },
];
const headCells: HeadCell[] = [
  {
    id: "number",
    numeric: false,
    label: "No.",
  },
  {
    id: "id",
    numeric: false,
    label: "ID",
  },
  {
    id: "date",
    numeric: false,
    label: "Date",
  },
  {
    id: "name",
    numeric: false,
    label: "Doctor",
  },
  {
    id: "street",
    numeric: false,
    label: "Street",
  },
  {
    id: "city",
    numeric: false,
    label: "City",
  },
  {
    id: "state",
    numeric: false,
    label: "State",
  },
  {
    id: "zip",
    numeric: false,
    label: "Zip",
  },
  {
    id: "mobilePhone",
    numeric: false,
    label: "Mobile Phone",
  },
];
export const DoctorReport: React.FC = () => {
  //state
  const [rowsCount, setRowsCount] = React.useState<number>(0);
  const [data, setData] = React.useState<practitionerModel[]>([]);
  const [isLoading, setIsLoading] = React.useState(false);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  //date picker
  const [startDate, setStartDate] = React.useState<string>(
    "2014-08-18T21:11:54"
  );
  const [endDate, setEndDate] = React.useState<string>("2015-08-18T21:11:54");

  const handleChangeStart = (value: any) => {
    setStartDate(value.toISOString().replace(/.\d+Z$/g, "Z") as string);
  };

  const handleChangeEnd = (value: any) => {
    setEndDate(value.toISOString().replace(/.\d+Z$/g, "Z") as string);
  };

  //handler event
  const handleChangePage = (event: any, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: any) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  // Search states
  const [nameSearchValue, setNameSearchValue] = React.useState<string>("");
  const [debounceSeachValue, setDebounceSearchValue] =
    React.useState<string>("");
  const [filter, setFilter] = React.useState<{ [key: string]: string }>({});

  const [advancedSearchPopUpOpen, setAdvancedSearchPopUpOpen] =
    React.useState(false);

  const handleClickAdvancedSearchOpen = () => {
    setAdvancedSearchPopUpOpen(true);
  };
  const handleClickAdvancedSearchCancel = () => {
    setAdvancedSearchPopUpOpen(false);
  };

  //api call
  const getDoctorListFunc = async (filter?: PractitionerGetRequest) => {
    const params: PractitionerGetRequest = {
      ...filter,
      page: page + 1,
      size: rowsPerPage,
    };
    setIsLoading(true);
    try {
      let { data, paging } = await practitionersService.getList({ params });
      setData(data);
      setRowsCount(paging.total);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      console.log("getPatientList error: ", error);
    }
  };

  React.useEffect(() => {
    getDoctorListFunc();
  }, [rowsPerPage, page]);

  //map data
  const mapDataToRender = () => {
    const render = data.map((item, index) => ({
      number: index + 1,
      id: item.id as string,
      date: [item.address?.[0].period?.start, item.address?.[0].period?.end]
        .map((x) => x?.split("-")[0] || "N/A")
        .join(" - "),
      name:
        item.name?.[0].text ||
        item.name?.[0].given?.join(" ") + " " + item.name?.[0].family ||
        "-",
      street: item.address?.[0].text || "-",
      city: item.address?.[0].city || "-",
      state: item.address?.[0].state || "-",
      zip: item.address?.[0].postalCode || "-",
      mobilePhone:
        item.telecom
          ?.filter((x) => x.use === "mobile")
          .map((x) => x.value)
          .join(", ") || "-",
    }));
    return render;
  };

  //export data
  const exportToCSV = () => {
    const ws = XLSX.utils.json_to_sheet(mapDataToRender());
    const wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "DoctorReport");
    XLSX.writeFile(wb, "DoctorReport.xlsx");
  };

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        width: "100%",
        padding: "20px",
        backgroundColor: "white",
        borderRadius: "16px",
      }}
    >
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          gap: "8px",
        }}
      >
        <Typography variant="h2" children={"DOCTOR REPORT LIST"} />
        <Box sx={{ display: "flex", gap: "8px" }}>
          <LocalizationProvider dateAdapter={AdapterDayjs}>
            <DesktopDatePicker
              label="Starting Date"
              inputFormat="MM/DD/YYYY"
              value={startDate}
              onChange={handleChangeStart}
              renderInput={(params) => (
                <TextField
                  {...params}
                  error={startDate ? startDate >= endDate : true}
                />
              )}
              InputProps={{
                sx: {
                  borderRadius: 3,
                  height: 48,
                  width: 150,
                },
              }}
            />
          </LocalizationProvider>

          <LocalizationProvider dateAdapter={AdapterDayjs}>
            <DesktopDatePicker
              label="Ending Date"
              inputFormat="MM/DD/YYYY"
              value={endDate}
              onChange={handleChangeEnd}
              renderInput={(params) => (
                <TextField
                  {...params}
                  error={endDate ? endDate <= startDate : true}
                />
              )}
              InputProps={{
                sx: {
                  borderRadius: 3,
                  height: 48,
                  width: 150,
                },
              }}
            />
          </LocalizationProvider>
          <RoundedButton
            variant="contained"
            onClick={handleClickAdvancedSearchOpen}
            children="Advanced Search"
          />
          <RoundedButton
            variant="contained"
            onClick={exportToCSV}
            endIcon={<AddIcon />}
            children={"Export Data"}
          />
        </Box>
      </Box>

      {advancedSearchPopUpOpen && (
        <AdvancedSearch
          props={advancedSearchProps}
          filter={filter}
          description="Find doctors with..."
          onFilterChange={setFilter}
          onApply={getDoctorListFunc}
          onClose={setAdvancedSearchPopUpOpen}
        />
      )}

      <CustomTable
        title={"DOCTOR REPORT LIST"}
        headcells={headCells}
        body={mapDataToRender()}
        paging={{
          page: page,
          rowsPerPage: rowsPerPage,
          rowsCount: rowsCount,
        }}
        loading={isLoading}
        toolbar={false}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Box>
  );
};
