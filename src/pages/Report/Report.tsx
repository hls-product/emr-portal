import { Box, Typography } from "@mui/material";
import React from "react";
import { Toolbar } from "../../components/tab/Tab";
import { PatientReport } from "./PatientReport";
import { DoctorReport } from "./DoctorReport";

const tabList = [
  { element: <PatientReport />, title: "Patient Report" },
  { element: <DoctorReport />, title: "Doctor Report" },
];

export const Report: React.FC = () => {
  return (
    <React.Fragment>
      <Typography
        fontSize={"24px"}
        fontWeight={700}
        color={"#131626"}
        style={{ paddingBottom: "20px" }}
        children="Report"
      />
      <Toolbar children={tabList} defaultValue={0} />
    </React.Fragment>
  );
};
