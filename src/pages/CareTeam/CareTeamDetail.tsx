import {
  Box,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { IRootState } from "../../store/reducers";
import { Participant } from "../../models/careTeam";
import { ReactComponent as BackArrow } from "../../assets/image/BackArrow.svg";
import Loading from "../../components/Loading";
import { RoundedButton } from "../../components/RoundedButton";

interface DetailProps {
  label: string;
  value: string;
}

interface TitleProps {
  title: string;
}

export const CareTeamDetail = () => {
  const navigate = useNavigate();

  const data = useSelector(
    (state: IRootState) => state.getCareTeamByIdReducer.data
  );

  const loading = useSelector(
    (state: IRootState) => state.getCareTeamByIdReducer.loading
  );

  const phone = data?.telecom?.find((x) => x.system === "phone");
  const email = data?.telecom?.find((x) => x.system === "email");

  const participants = data?.participant as Participant[];

  const RenderDetail = (detailprops: DetailProps) => {
    const { label, value } = detailprops;
    return (
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          marginBottom: "16px",
        }}
      >
        <Typography
          variant="h3"
          sx={{
            fontSize: "16px",
            fontWeight: 600,
          }}
        >
          {label}
        </Typography>

        <Typography
          sx={{
            fontSize: "16px",
            fontWeight: 500,
          }}
        >
          {value}
        </Typography>
      </Box>
    );
  };

  const RenderTitle = (titleProps: TitleProps) => {
    const { title } = titleProps;
    return (
      <Typography
        variant="h3"
        sx={{
          fontSize: "20px",
          marginBottom: "20px",
        }}
      >
        {title}
      </Typography>
    );
  };

  const handleClickEdit = () => {
    navigate(`/admin/care-teams/edit?${data.id}`);
  };

  return (
    <Box>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          marginBottom: "16px",
        }}
      >
        <Box sx={{ display: "flex", gap: 1, alignItems: "center" }}>
          <IconButton
            onClick={() => history.back()}
            style={{
              width: "40px",
              height: "40px",
              border: "1px solid #F4F4F4",
              background: "white",
            }}
          >
            <BackArrow />
          </IconButton>
          <Typography variant="h2" sx={{ fontWeight: 600, fontSize: "24px" }}>
            Care Team Detail
          </Typography>
        </Box>

        <RoundedButton
          variant="contained"
          style={{ padding: "10px 20px" }}
          onClick={handleClickEdit}
        >
          Edit
        </RoundedButton>
      </Box>

      {loading ? (
        <Loading />
      ) : (
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
          }}
        >
          <Box
            sx={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              marginBottom: "16px",
            }}
          >
            <Box
              sx={{
                width: "49.25%",
                display: "flex",
                background: "white",
                borderRadius: "16px",
                padding: "16px 16px 0 16px",
                flexDirection: "column",
              }}
            >
              <RenderTitle title="Basic Information" />
              <RenderDetail
                label="Patient Name"
                value={data?.subject?.display || "-"}
              />
              <RenderDetail label="ID" value={data?.id || "-"} />
              <RenderDetail label="Team Name" value={data?.name || "-"} />
              <RenderDetail
                label="External IDs"
                value={data?.identifier?.[0]?.value || "-"}
              />
              <RenderDetail label="Status" value={data?.status || "-"} />
              <RenderDetail
                label="Category"
                value={data?.category?.[0]?.coding?.[0]?.display || "-"}
              />
              <RenderDetail
                label="Start-time Period"
                value={data?.period?.start?.slice(0, 10) || "-"}
              />
              <RenderDetail
                label="End-time Period"
                value={data?.period?.end?.slice(0, 10) || "-"}
              />
            </Box>

            <Box
              sx={{
                width: "49.25%",
                display: "flex",
                background: "white",
                borderRadius: "16px",
                padding: "16px 16px 0 16px",
                flexDirection: "column",
              }}
            >
              <RenderTitle title="Advanced Information" />
              <RenderDetail
                label="Clinical Findings"
                value={data?.reasonCode?.[0]?.text || "-"}
              />
              <RenderDetail
                label="Organization Responsible"
                value={data?.managingOrganization?.[0]?.display || "-"}
              />
              <RenderDetail label="Phone Number" value={phone?.value || "-"} />
              <RenderDetail label="Email" value={email?.value || "-"} />
            </Box>
          </Box>

          <Box
            sx={{ background: "white", padding: "16px", borderRadius: "16px" }}
          >
            <RenderTitle title="Participants" />
            <TableContainer>
              <Table sx={{ minWidth: 650, borderBottom: "1px solid #e0e0e0" }}>
                <TableHead
                  sx={{
                    backgroundColor: "#FAFAFA",
                  }}
                >
                  <TableCell sx={{ fontWeight: 600, color: "#131626" }}>
                    No.
                  </TableCell>
                  <TableCell sx={{ fontWeight: 600, color: "#131626" }}>
                    Member Name
                  </TableCell>
                  <TableCell sx={{ fontWeight: 600, color: "#131626" }}>
                    Role
                  </TableCell>
                  <TableCell sx={{ fontWeight: 600, color: "#131626" }}>
                    Organization
                  </TableCell>
                  <TableCell sx={{ fontWeight: 600, color: "#131626" }}>
                    Period
                  </TableCell>
                </TableHead>

                <TableBody>
                  {participants?.map((item, index) => (
                    <TableRow
                      key={index}
                      sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                    >
                      <TableCell>{index + 1}</TableCell>
                      <TableCell>{item?.member?.display || "-"}</TableCell>
                      <TableCell>{item?.role?.[0]?.text || "-"}</TableCell>
                      <TableCell>{item?.onBehalfOf?.display || "-"}</TableCell>
                      <TableCell>
                        {item?.period?.start?.slice(0, 4) || "N/A"} -{" "}
                        {item?.period?.end?.slice(0, 4) ||
                          new Date().getFullYear()}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Box>
        </Box>
      )}
    </Box>
  );
};
