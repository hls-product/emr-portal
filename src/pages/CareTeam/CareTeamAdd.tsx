import {
  Box,
  CircularProgress,
  FormControl,
  IconButton,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
  TextField,
  Typography
} from "@mui/material";
import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { ReactComponent as BackArrow } from "../../assets/image/BackArrow.svg";
import { RoundedButton } from "../../components/RoundedButton";
import { Participant } from "../../models/careTeam";
import { careTeamService } from "../../services/agent";

const Status = [
  { value: "Proposed" },
  { value: "Active" },
  { value: "Suspended" },
  { value: "Inactive" },
  { value: "Entered in Error" },
];

const Category = [
  { value: "Event-focused care team" },
  { value: "Encounter-focused care team" },
  { value: "Episode of care-focused care team" },
  { value: "Condition-focused care team" },
  { value: "Longitudinal care-coordination focused care team" },
  { value: "Home & Community Based Services (HCBS)-focused care team" },
  { value: "Clinical research-focused care team" },
  { value: "Public health-focused care team" },
];

const Role = [
  { value: "Person" },
  { value: "Healthcare professional" },
  { value: "Healthcare related organisation" },
];

export const CareTeamAdd: React.FC = () => {
  const navigate = useNavigate();

  //-----------------------------------
  const [loading, setLoading] = useState<boolean>(false);
  const [patientName, setPatientName] = useState<string>("");
  const [teamName, setTeamName] = useState<string>("");
  const [externalId, setExternalId] = useState<string>("");
  const [status, setStatus] = useState<string>("Active");
  const [category, setCategory] = useState<string>(
    "Encounter-focused care team"
  );
  const [startPeriod, setStartPeriod] = useState<string>("2014-08-18T21:11:54");
  const [endPeriod, setEndPeriod] = useState<string>("2015-08-18T21:11:54");
  const [reason, setReason] = useState<string>("");
  const [organization, setOrganization] = useState<string>("");
  const [phone, setPhone] = useState<string>("");
  const [email, setEmail] = useState<string>("");
  const [participant, setParticipant] = useState<Participant[]>(
    [] as Participant[]
  );

  //--------------------------------------------

  const handleChangePatientName = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setPatientName(event.target.value as string);
  };

  const handleChangeTeamName = (event: React.ChangeEvent<HTMLInputElement>) => {
    setTeamName(event.target.value as string);
  };

  const handleChangeExternalId = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setExternalId(event.target.value as string);
  };

  const handleChangeStatus = (event: SelectChangeEvent) => {
    setStatus(event.target.value as string);
  };

  const handleChangeCategory = (event: SelectChangeEvent) => {
    setCategory(event.target.value as string);
  };

  const handleChangeStart = (value: any) => {
    setStartPeriod(value.toISOString().replace(/.\d+Z$/g, "Z") as string);
  };

  const handleChangeEnd = (value: any) => {
    setEndPeriod(value.toISOString().replace(/.\d+Z$/g, "Z") as string);
  };

  const handleChangeReason = (event: React.ChangeEvent<HTMLInputElement>) => {
    setReason(event.target.value as string);
  };

  const handleChangeOrganization = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setOrganization(event.target.value as string);
  };

  const handleChangePhone = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPhone(event.target.value as string);
  };

  const handleChangeEmail = (event: React.ChangeEvent<HTMLInputElement>) => {
    setEmail(event.target.value as string);
  };

  //----------------------------------------------------------------

  const [renderParticipant, setRenderParticipant] = useState<JSX.Element[]>([]);

  //--------------------------------------------

  const [participantName, setParticipantName] = useState<string>("");
  const [roleParticipant, setRoleParticipant] = useState<string>(
    "Healthcare related organisation"
  );
  const [organizationParticipant, setOrganizationParticipant] =
    useState<string>("");

  const [startParticipant, setStartParticipant] = useState<string>(
    "2014-08-18T21:11:54"
  );
  const [endParticipant, setEndParticipant] = useState<string>(
    "2015-08-18T21:11:54"
  );

  //---------------------------------------------

  const AddParticipant: JSX.Element = (
    <Box
      sx={{
        display: "flex",
        justifyContent: "space-between",
        marginBottom: "24px",
      }}
    >
      <TextField
        label="Name"
        placeholder="Enter"
        InputProps={{
          style: {
            height: "48px",
            borderRadius: "12px",
            color: "black",
          },
        }}
        InputLabelProps={{
          style: {
            color: "black",
          },
        }}
        sx={{ width: "19%" }}
        onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
          setParticipantName(event.target.value as string)
        }
      />

      <FormControl sx={{ width: "19%" }}>
        <InputLabel sx={{ color: "black" }}>Role</InputLabel>
        <Select
          label="Role"
          sx={{
            height: "48px",
            borderRadius: "12px",
            color: "black",
          }}
          onChange={(event: SelectChangeEvent) =>
            setRoleParticipant(event.target.value as string)
          }
        >
          {Role.map((item, index) => (
            <MenuItem key={index} value={item.value}>
              {item.value}
            </MenuItem>
          ))}
        </Select>
      </FormControl>

      <TextField
        label="Organization"
        placeholder="Enter"
        InputProps={{
          style: {
            height: "48px",
            borderRadius: "12px",
            color: "black",
          },
        }}
        InputLabelProps={{
          style: {
            color: "black",
          },
        }}
        sx={{ width: "19%" }}
        onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
          setOrganizationParticipant(event.target.value as string)
        }
      />

      <LocalizationProvider dateAdapter={AdapterDayjs}>
        <DesktopDatePicker
          label="Starting time"
          inputFormat="MM/DD/YYYY"
          value={startParticipant}
          onChange={(value: any) =>
            setStartParticipant(
              value.toISOString().replace(/.\d+Z$/g, "Z") as string
            )
          }
          renderInput={(params) => (
            <TextField
              {...params}
              error={
                startParticipant ? startParticipant >= endParticipant : true
              }
            />
          )}
          InputProps={{
            style: {
              height: "48px",
              borderRadius: "12px",
              color: "black",
            },
          }}
        />
      </LocalizationProvider>

      <LocalizationProvider dateAdapter={AdapterDayjs}>
        <DesktopDatePicker
          label="Ending time"
          inputFormat="MM/DD/YYYY"
          value={endParticipant}
          onChange={(value: any) =>
            setEndParticipant(
              value.toISOString().replace(/.\d+Z$/g, "Z") as string
            )
          }
          renderInput={(params) => (
            <TextField
              {...params}
              error={endParticipant ? endParticipant <= startParticipant : true}
            />
          )}
          InputProps={{
            style: {
              height: "48px",
              borderRadius: "12px",
              color: "black",
            },
          }}
        />
      </LocalizationProvider>
    </Box>
  );

  const dataParticipant: Participant = {
    role: [{ text: roleParticipant }],
    member: { display: participantName },
    onBehalfOf: { display: organizationParticipant },
    period: { start: startParticipant, end: endParticipant },
  };

  const handleRenderAdd = () => {
    setRenderParticipant([...renderParticipant, AddParticipant]);
    setParticipant([...participant, dataParticipant]);
  };

  //----------------------------------------------------

  const handleAdd = () => {
    setLoading(true);
    setParticipant([...participant, dataParticipant]);
    careTeamService
      .post({
        body: {
          identifier: [{ value: externalId }],
          status,
          category: [{ coding: [{ display: category }] }],
          name: teamName,
          subject: { display: patientName },
          participant: [...participant, dataParticipant].slice(1),
          managingOrganization: [{ display: organization }],
          telecom: [
            { system: "phone", value: phone },
            { system: "email", value: email },
          ],
        },
      })
      .then(() => navigate("/admin/care-teams"));
  };

  return (
    <Box>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          marginBottom: "16px",
        }}
      >
        <Box sx={{ display: "flex", gap: 1, alignItems: "center" }}>
          <IconButton
            onClick={() => history.back()}
            style={{
              width: "40px",
              height: "40px",
              border: "1px solid #F4F4F4",
              background: "white",
            }}
          >
            <BackArrow />
          </IconButton>
          <Typography variant="h2" sx={{ fontWeight: 600, fontSize: "24px" }}>
            Add Care Team
          </Typography>
        </Box>

        <RoundedButton
          variant="contained"
          style={{ padding: "10px 20px" }}
          onClick={handleAdd}
        >
          {loading && (
            <CircularProgress
              size={16}
              sx={{ marginRight: "8px", color: "white" }}
            />
          )}
          Save
        </RoundedButton>
      </Box>

      <Box
        sx={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          marginBottom: "16px",
        }}
      >
        <Box
          sx={{
            width: "49.25%",
          }}
        >
          <Box
            sx={{
              background: "white",
              borderRadius: "16px",
              padding: "16px",
              marginBottom: "16px",
            }}
          >
            <Typography
              variant="h3"
              sx={{
                fontSize: "16px",
                fontWeight: 700,
                marginBottom: "20px",
                textTransform: "uppercase",
              }}
            >
              Basic Information
            </Typography>

            <TextField
              required
              label="Patient Name"
              placeholder="Enter"
              InputProps={{
                style: {
                  height: "48px",
                  borderRadius: "12px",
                  color: "black",
                },
              }}
              InputLabelProps={{
                style: {
                  color: "black",
                },
              }}
              sx={{ width: "100%", marginBottom: "16px" }}
              onChange={handleChangePatientName}
            />

            <TextField
              required
              label="Team Name"
              placeholder="Enter"
              InputProps={{
                style: {
                  height: "48px",
                  borderRadius: "12px",
                  color: "black",
                },
              }}
              InputLabelProps={{
                style: {
                  color: "black",
                },
              }}
              sx={{ width: "100%", marginBottom: "16px" }}
              onChange={handleChangeTeamName}
            />

            <TextField
              label="External IDs"
              placeholder="Enter"
              InputProps={{
                style: {
                  height: "48px",
                  borderRadius: "12px",
                  color: "black",
                },
              }}
              InputLabelProps={{
                style: {
                  color: "black",
                },
              }}
              sx={{ width: "100%", marginBottom: "16px" }}
              onChange={handleChangeExternalId}
            />
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                width: "100%",
              }}
            >
              <FormControl sx={{ width: "49%" }}>
                <InputLabel required sx={{ color: "black" }}>
                  Status
                </InputLabel>
                <Select
                  label="Status"
                  sx={{
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  }}
                  onChange={handleChangeStatus}
                >
                  {Status.map((item, index) => (
                    <MenuItem key={index} value={item.value}>
                      {item.value}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>

              <FormControl sx={{ width: "49%" }}>
                <InputLabel required sx={{ color: "black" }}>
                  Category
                </InputLabel>
                <Select
                  label="Category"
                  sx={{
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  }}
                  onChange={handleChangeCategory}
                >
                  {Category.map((item, index) => (
                    <MenuItem key={index} value={item.value}>
                      {item.value}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Box>
          </Box>

          <Box
            sx={{
              background: "white",
              borderRadius: "16px",
              padding: "16px",
            }}
          >
            <Typography
              variant="h3"
              sx={{
                fontSize: "16px",
                fontWeight: 700,
                marginBottom: "20px",
                textTransform: "uppercase",
              }}
            >
              CareTeam Schedule
            </Typography>

            <Box
              sx={{
                gap: 1,
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <LocalizationProvider dateAdapter={AdapterDayjs}>
                <DesktopDatePicker
                  label="Starting time"
                  inputFormat="MM/DD/YYYY"
                  value={startPeriod}
                  onChange={handleChangeStart}
                  renderInput={(params) => (
                    <TextField
                      sx={{ width: "49%" }}
                      {...params}
                      error={startPeriod ? startPeriod >= endPeriod : true}
                    />
                  )}
                  InputProps={{
                    style: {
                      height: "48px",
                      borderRadius: "12px",
                      color: "black",
                    },
                  }}
                />
              </LocalizationProvider>

              <LocalizationProvider dateAdapter={AdapterDayjs}>
                <DesktopDatePicker
                  label="Ending time"
                  inputFormat="MM/DD/YYYY"
                  value={endPeriod}
                  onChange={handleChangeEnd}
                  renderInput={(params) => (
                    <TextField
                      sx={{ width: "49%" }}
                      {...params}
                      error={endPeriod ? endPeriod <= startPeriod : true}
                    />
                  )}
                  InputProps={{
                    style: {
                      height: "48px",
                      borderRadius: "12px",
                      color: "black",
                    },
                  }}
                />
              </LocalizationProvider>
            </Box>
          </Box>
        </Box>

        <Box
          sx={{
            width: "49.25%",
            background: "white",
            borderRadius: "16px",
            padding: "16px",
          }}
        >
          <Typography
            variant="h3"
            sx={{
              fontSize: "16px",
              fontWeight: 700,
              marginBottom: "20px",
              textTransform: "uppercase",
            }}
          >
            Advanced Information
          </Typography>

          <TextField
            label="Clinical Findings"
            placeholder="Enter"
            InputProps={{
              style: {
                height: "48px",
                borderRadius: "12px",
                color: "black",
              },
            }}
            InputLabelProps={{
              style: {
                color: "black",
              },
            }}
            sx={{ width: "100%", marginBottom: "16px" }}
            onChange={handleChangeReason}
          />

          <TextField
            label="Organization Responsible"
            placeholder="Enter"
            InputProps={{
              style: {
                height: "48px",
                borderRadius: "12px",
                color: "black",
              },
            }}
            InputLabelProps={{
              style: {
                color: "black",
              },
            }}
            sx={{ width: "100%", marginBottom: "16px" }}
            onChange={handleChangeOrganization}
          />

          <TextField
            label="Phone Number"
            placeholder="Enter"
            InputProps={{
              style: {
                height: "48px",
                borderRadius: "12px",
                color: "black",
              },
            }}
            InputLabelProps={{
              style: {
                color: "black",
              },
            }}
            sx={{ width: "100%", marginBottom: "16px" }}
            onChange={handleChangePhone}
          />

          <TextField
            label="Email"
            placeholder="Enter"
            InputProps={{
              style: {
                height: "48px",
                borderRadius: "12px",
                color: "black",
              },
            }}
            InputLabelProps={{
              style: {
                color: "black",
              },
            }}
            sx={{ width: "100%", marginBottom: "16px" }}
            onChange={handleChangeEmail}
          />
        </Box>
      </Box>

      <Box
        sx={{
          background: "white",
          borderRadius: "16px",
          padding: "16px",
        }}
      >
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            marginBottom: "20px",
          }}
        >
          <Typography
            variant="h3"
            sx={{
              fontSize: "16px",
              fontWeight: 700,
              textTransform: "uppercase",
            }}
          >
            Participants
          </Typography>

          <IconButton
            sx={{ border: "1px solid #eaeaea", width: "46px" }}
            onClick={handleRenderAdd}
          >
            +
          </IconButton>
        </Box>
        <Box>
          {renderParticipant?.map((x, index) => (
            <div key={index}>{x}</div>
          ))}
        </Box>
      </Box>
    </Box>
  );
};
