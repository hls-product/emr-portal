import { Box, Checkbox, FormControl, Typography } from "@mui/material";

import { LocalizationProvider, TimePicker } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import dayjs, { Dayjs } from "dayjs";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Loading from "../../../components/Loading";
import { StyledHeaderTypography } from "../../../components/styledTypography";
import { locationModel } from "../../../models/location";
import { Address } from "../../../models/shared/dataType";
import { locationService } from "../../../services/agent";
import { colorPalette } from "../../../theme";
import {
  StyledArrowBack,
  StyledInputTextField,
  StyledMenuItem,
  StyledTopContainer,
  StyledTopLeftContainer,
} from "../AddLocation/style";
import { Countries, PhysicalType, RoleType } from "../ServiceDeliveryModel";

const initialLocation: locationModel = {
  identifier: [],
  status: "active",
  operationalStatus: {},
  name: "",
  alias: [],
  description: "",
  mode: "instance",
  type: undefined,
  telecom: [],
  address: {} as Address,
  physicalType: undefined,
  managingOrganization: {},
  partOf: {},
  availabilityExceptions: "",
  endpoint: [],
};

const dates = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"];

export const DetailLocation: React.FC = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const [loading, setLoading] = useState<boolean>(true);

  const [location, setLocation] = useState(initialLocation);
  const [roleType, setRoleType] = useState<string[]>(
    location.type &&
      location.type.length > 0 &&
      location.type[0].coding &&
      location.type[0].coding?.length > 0
      ? (location?.type[0].coding.map((p) => p.code) as string[])
      : []
  );
  const [physicalType, setPhysicalType] = useState<string>(
    location.physicalType?.coding && location.physicalType?.coding?.length > 0
      ? location?.physicalType?.coding[0].code ?? ""
      : ""
  );
  const [openningDays, setOpenningDays] = useState<string[]>(
    location.hoursOfOperation && location.hoursOfOperation[0]
      ? (location.hoursOfOperation[0].daysOfWeek as string[])
      : []
  );

  const [openningTime, setOpenningTime] = React.useState<Dayjs | null>(
    dayjs("2014-08-18T21:11:54")
  );

  const handleOpenningTimeChange = (newValue: Dayjs | null) => {
    setOpenningTime(newValue);
  };

  const [closingTime, setClosingTime] = React.useState<Dayjs | null>(
    dayjs("2014-08-18T21:11:54")
  );

  const handleClosingTimeChange = (newValue: Dayjs | null) => {
    setClosingTime(newValue);
  };

  useEffect(() => {
    if (id) {
      locationService
        .getById(id)
        .then((res) => {
          const newInitLocation: locationModel = {
            ...location,
            name: res.name as string,
            status: res.status,
            description: res.description as string,
            mode: res.mode,
            hoursOfOperation:
              res.hoursOfOperation as locationModel["hoursOfOperation"],
            address: res.address as Address,
            id: res.id,
          };
          setLocation(newInitLocation);
          setLoading(false);
        })
        .catch(() => navigate("/admin/locations"));
    } else {
      navigate("/admin/locations");
    }
  }, [id]);

  return loading ? (
    <Loading />
  ) : (
    <Box
      sx={{
        minHeight: "calc(100vh - 100px)",
      }}
    >
      {/* ------------NAV-BAR ------------*/}
      <StyledTopContainer>
        <StyledTopLeftContainer>
          {" "}
          <StyledArrowBack onClick={() => navigate(`/admin/locations`)} />{" "}
          <StyledHeaderTypography
            style={{
              marginBottom: "0",
            }}
          >
            Detail Location
          </StyledHeaderTypography>
        </StyledTopLeftContainer>
        {/* <StyledSaveButton onClick={()=>{navigate(`/admin/locations/edit/${id}`)}}>Edit</StyledSaveButton> */}
      </StyledTopContainer>
      {/*------------ FORM ------------*/}
      <Box
        sx={{
          display: "flex",
          flexDirection: "row",
          gap: "16px",
        }}
      >
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            backgroundColor: colorPalette.white,
            borderRadius: "16px",
            p: "16px",
            gap: "16px",
            flexBasis: "50%",
          }}
        >
          {/*-----------GENERAL----------------*/}
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              mb: "16px",
            }}
          >
            <span style={{ fontSize: "16px", fontWeight: 600 }}>
              GENERAL INFORMATION:
            </span>
          </Box>

          {/* ------------NAME FIELD ------------*/}
          <StyledInputTextField
            disabled={true}
            label="Name"
            id="name"
            name="name"
            sx={{ width: "100%", height: "3rem" }}
            value={location.name}
          />
          {/* -----------DESCRIPTION---------------- */}
          <StyledInputTextField
            disabled={true}
            label="Description"
            id="description"
            name="description"
            value={location.description}
            sx={{ width: "100%", height: "3rem" }}
          />

          {/* ------------STATUS AND MODE AND PHYS TYPE------------*/}
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              gap: "8px",
            }}
          >
            {/* ---STATUS--- */}
            <FormControl sx={{ flexBasis: "33%" }}>
              <StyledInputTextField
                disabled={true}
                name="status"
                id="status"
                label="Status"
                value={location.status}
                select
                sx={{ width: "100%", height: "3rem" }}
              >
                <StyledMenuItem value="active">Active</StyledMenuItem>
                <StyledMenuItem value="suspended">Suspended</StyledMenuItem>
                <StyledMenuItem value="inactive">Inactive</StyledMenuItem>
              </StyledInputTextField>
            </FormControl>

            {/* ---MODE--- */}
            <FormControl sx={{ flexBasis: "33%" }}>
              <StyledInputTextField
                disabled={true}
                name="mode"
                id="mode"
                label="Mode"
                value={location.mode}
                select
                sx={{ width: "100%", height: "3rem" }}
              >
                <StyledMenuItem value={"instance"}>Instance</StyledMenuItem>
                <StyledMenuItem value={"kind"}>Kind</StyledMenuItem>
              </StyledInputTextField>
            </FormControl>

            {/* ---PHYSICAL TYPE--- */}
            <FormControl sx={{ flexBasis: "33%" }}>
              <StyledInputTextField
                disabled={true}
                name="physicalType"
                id={`physical-type`}
                label="Physical Type"
                value={physicalType}
                select
                sx={{ width: "100%", height: "3rem" }}
              >
                {PhysicalType.map((item) => (
                  <StyledMenuItem value={item.code}>
                    {item.display}
                  </StyledMenuItem>
                ))}
              </StyledInputTextField>
            </FormControl>
          </Box>

          {/* ------------ROLE TYPE ------------*/}
          <FormControl>
            <StyledInputTextField
              disabled={true}
              name="role-type"
              id={`role-type`}
              label="Role Type"
              value={roleType}
              select
              sx={{ width: "100%", height: "3rem" }}
            >
              {RoleType.map((item) => (
                <StyledMenuItem value={item.code}>
                  {item.display}
                </StyledMenuItem>
              ))}
            </StyledInputTextField>
          </FormControl>

          {/* ----HOURS OPERATION---- */}
          <Box sx={{ display: "flex" }}>
            <FormControl sx={{ flex: 1 }}>
              <StyledInputTextField
                disabled={true}
                name="hoursOfOperation[0].daysOfWeek"
                id={`hours-operation`}
                label="Openning days"
                value={openningDays}
                select
                sx={{ width: "100%", height: "3rem" }}
              >
                {dates.map((item) => (
                  <StyledMenuItem value={item}>{item}</StyledMenuItem>
                ))}
              </StyledInputTextField>
            </FormControl>
            <Box sx={{ display: "flex", alignItems: "center" }}>
              <Checkbox disabled={true} />
              <Typography>All day?</Typography>
            </Box>
          </Box>
          <LocalizationProvider dateAdapter={AdapterDayjs}>
            <Box sx={{ display: "flex", gap: "16px" }}>
              <TimePicker
                disabled={true}
                label="Openning Time"
                value={openningTime}
                onChange={handleOpenningTimeChange}
                renderInput={(params) => (
                  <StyledInputTextField sx={{ flex: 1 }} {...params} />
                )}
              />
              <TimePicker
                disabled={true}
                label="Closing Time"
                value={closingTime}
                onChange={handleClosingTimeChange}
                renderInput={(params) => (
                  <StyledInputTextField sx={{ flex: 1 }} {...params} />
                )}
              />
            </Box>
          </LocalizationProvider>
        </Box>

        {/* -------------ADDRESS-------------- */}
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            backgroundColor: colorPalette.white,
            borderRadius: "16px",
            p: "16px",
            gap: "16px",
            flexBasis: "50%",
          }}
        >
          <Typography sx={{ fontSize: "16px", fontWeight: 600 }}>
            ADDRESS:
          </Typography>
          {/* LINE 1 */}

          <StyledInputTextField
            disabled={true}
            label="Address line 1"
            id="address-line-1"
            name="line[0]"
            value={location.address?.line ? location.address?.line[0] : null}
            sx={{ width: "100%", height: "3rem" }}
          />
          {/* LINE 2 */}
          <StyledInputTextField
            disabled={true}
            label="Address line 2"
            id="address-line-2"
            name="line[1]"
            value={location.address?.line ? location.address?.line[1] : null}
            sx={{ width: "100%", height: "3rem" }}
          />
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              gap: "8px",
            }}
          >
            {/* COUNTRY */}
            <FormControl sx={{ flexBasis: "50%" }}>
              <StyledInputTextField
                disabled={true}
                name="country"
                id={`address-country`}
                label="Country"
                value={location.address?.country}
                select
                sx={{ width: "100%", height: "3rem" }}
              >
                {Countries.map((item) => (
                  <StyledMenuItem value={item.name}>{item.name}</StyledMenuItem>
                ))}
              </StyledInputTextField>
            </FormControl>
            <StyledInputTextField
              disabled={true}
              label="Postal Code"
              id="address-postal"
              name="postalCode"
              value={location.address?.postalCode}
              sx={{ flexBasis: "50%", height: "3rem" }}
            />
            {/* City */}
          </Box>
          <StyledInputTextField
            disabled={true}
            label="City"
            id="address-city"
            name="city"
            value={location.address?.city}
            sx={{ width: "100%", height: "3rem" }}
          />
          <StyledInputTextField
            disabled={true}
            label="District"
            id="address-district"
            name="district"
            value={location.address?.district}
            sx={{ width: "100%", height: "3rem" }}
          />
          <StyledInputTextField
            disabled={true}
            label="State"
            id="address-State"
            name="state"
            value={location.address?.state}
            sx={{ width: "100%", height: "3rem" }}
          />
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              gap: "8px",
            }}
          >
            {/* USE */}
            <FormControl sx={{ flexBasis: "50%" }}>
              <StyledInputTextField
                disabled={true}
                name="use"
                id={`address-use`}
                label="Use"
                value={location.address?.use}
                select
                sx={{ width: "100%", height: "3rem" }}
              >
                <StyledMenuItem value="home">Home</StyledMenuItem>
                <StyledMenuItem value="work">Work</StyledMenuItem>
                <StyledMenuItem value="temp">Temporary</StyledMenuItem>
                <StyledMenuItem value="old">Old</StyledMenuItem>
                <StyledMenuItem value="billing">Billing</StyledMenuItem>
              </StyledInputTextField>
            </FormControl>
            {/* TYPE */}
            <FormControl sx={{ flexBasis: "50%" }}>
              <StyledInputTextField
                disabled={true}
                name="type"
                id={`address-type`}
                label="Type"
                value={location.address?.type}
                select
                sx={{ width: "100%", height: "3rem" }}
              >
                <StyledMenuItem value="postal">Postal</StyledMenuItem>
                <StyledMenuItem value="physical">Physical</StyledMenuItem>
                <StyledMenuItem value="both">Both</StyledMenuItem>
              </StyledInputTextField>
            </FormControl>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};
