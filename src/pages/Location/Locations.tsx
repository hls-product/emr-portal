import React from "react";
import { DeleteDialog } from "../../components/DeleteDialog";
import Box from "@mui/material/Box";
import { Link, useNavigate } from "react-router-dom";
import {
  IconButton,
  InputAdornment,
  TextField,
  Typography,
} from "@mui/material";
import { RoundedButton } from "../../components/RoundedButton";
import { Add, Create, Search, Visibility } from "@mui/icons-material";
import DeleteIcon from "@mui/icons-material/Delete";
import { HeadCell } from "../../components/Table/types";
import { locationService } from "../../services/agent";
import { locationModel } from "../../models/location";
import { CustomTable } from "../../components/Table/Table";
import { AdvancedSearch } from "../../components/Table/AdvancedSearch";

const headCells: HeadCell[] = [
  {
    id: "number",
    numeric: true,
    label: "No.",
  },
  {
    id: "name",
    numeric: true,
    label: "Location Name",
  },
  {
    id: "address",
    numeric: true,
    label: "Address",
  },
  {
    id: "status",
    numeric: true,
    label: "Status",
  },
  {
    id: "hoursOfOperation",
    numeric: true,
    label: "Operation Hour",
  },
  {
    id: "type",
    numeric: true,
    label: "Type",
  },
  {
    id: "physicalType",
    numeric: true,
    label: "Physical Type",
  },
  {
    id: "action",
    numeric: true,
    label: "Action",
  },
];
const advancedSearchProps = [
  {
    name: "address",
    label: "Address",
  },
  {
    name: "status",
    label: "Status",
    select: true,
    menuItems: [
      {
        value: "active",
        display: "Active",
      },
      {
        value: "inactive",
        display: "Inactive",
      },
      {
        value: "suspended",
        display: "Suspended",
      },
    ],
  },
];
export const Locations: React.FC = () => {
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = React.useState(false);
  const [data, setData] = React.useState<locationModel[]>([]);
  const [page, setPage] = React.useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = React.useState<number>(10);
  const [rowsCount, setRowsCount] = React.useState<number>(0);
  const [deletePopUpOpen, setDeletePopUpOpen] = React.useState(false);
  const [openAdvancedSearch, setOpenAdvancedSearch] = React.useState(false);
  const [currentSelectedId, setCurrentSelectedId] = React.useState("");

  // Search states
  const [searchValue, setSearchValue] = React.useState<string>("");
  const [debounceSeachValue, setDebounceSearchValue] = React.useState<string>("");
  const [filter, setFilter] = React.useState<{ [key: string]: string }>({});

  // Api calling functions
  const getLocationList = async (filter: { [key: string]: string }) => {
    const params = {
      ...filter,
      page: page + 1,
      size: rowsPerPage,
      name: debounceSeachValue,
    };
    setIsLoading(true);
    try {
      let { data, paging } = await locationService.getList({ params });
      setData(data);
      setRowsCount(paging.total);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      console.log("getLocationList error: ", error);
    }
  };

  const deleteLocation = async (id: string) => {
    setIsLoading(true);
    try {
      let response = await locationService.delete(id);
      setIsLoading(false);
      return response;
    } catch (error) {
      setIsLoading(false);
      console.log("deleteLoaction error: ", error);
    }
  };

  // Event Handler
  const handleChangePage = (event: any, newPage: number) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event: any) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleDelete = (id: string) => {
    setDeletePopUpOpen(true);
    setCurrentSelectedId(id);
  };
  const handleCancelDelete = () => {
    setDeletePopUpOpen(false);
  };
  const handleConfirmDelete = () => {
    setDeletePopUpOpen(false);
    //currentPatientIdSelected
    deleteLocation(currentSelectedId).then((res) => {
      setCurrentSelectedId("");
      getLocationList(filter);
    });
  };
  const handleClickAdvancedSearchOpen = () => {
    setOpenAdvancedSearch(true);
  };

  // useEffect

  //Debounce for name search
  React.useEffect(() => {
    let timeout = setTimeout(() => {
      setDebounceSearchValue(searchValue);
    }, 500);

    return () => {
      clearTimeout(timeout);
    };
  }, [searchValue]);

  // Back to previous page if current page is empty after deleting an item
  React.useEffect(() => {
    let totalPages = Math.ceil(rowsCount / rowsPerPage);
    let currentPage = page + 1;
    if (currentPage > totalPages && totalPages > 0) {
      setPage(totalPages - 1);
    }
  }, [rowsCount]);
  React.useEffect(() => {
    getLocationList(filter);
  }, [debounceSeachValue, rowsPerPage, page]);

  const mapDataToRender = () => {
    const render = data.map((item, index) => ({
      id: item.id as string,
      number: index +1,
      name: item.name as string,
      address: item.address?.text as string,
      status: item.status
        ? item.status.charAt(0).toUpperCase() + item.status.slice(1)
        : "N/A",
      hoursOfOperation: item.hoursOfOperation
        ? item.hoursOfOperation[0]?.openingTime +
          " - " +
          item.hoursOfOperation[0]?.closingTime
        : "N/A" + " - " + "N/A",
      type: item.type
        ?.map((item) =>
          item.text ? item.text : item.coding?.map((x) => x.display).join(", ")
        )
        .join(", "),
      physicalType: item.physicalType?.text
        ? item.physicalType?.text
        : item.physicalType?.coding?.map((x) => x.display).join(", "),
      action: (
        <Box style={{ textAlign: "center" }}>
          <Link
            to={`edit/${item.id}`}
            children={<IconButton children={<Create />} />}
          />
          <Link
            to={`detail/${item.id}`}
            children={<IconButton children={<Visibility />} />}
          />
          <IconButton
            children={<DeleteIcon />}
            onClick={() => {
              handleDelete(`${item.id}`);
            }}
          />
        </Box>
      ),
    }));
    return render;
  };

  return (
    <>
      {/* Dialog for delete item popup */}
      <DeleteDialog
        open={deletePopUpOpen}
        onClose={handleCancelDelete}
        onConfirm={handleConfirmDelete}
        onCancel={handleCancelDelete}
      />
      <Box>
        <Typography
          fontSize={"24px"}
          fontWeight={700}
          color={"#131626"}
          style={{ paddingBottom: "20px" }}
          children="Locations"
        />
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            width: "100%",
            padding: "20px",
            backgroundColor: "white",
            borderRadius: "16px",
          }}
        >
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: "8px",
            }}
          >
            <Typography variant="h2" children={"LOCATION LIST"} />
            <Box sx={{ display: "flex", gap: "8px" }}>
              <TextField
                placeholder="Search location name..."
                InputProps={{
                  startAdornment: (
                    <InputAdornment
                      position="start"
                      children={<Search sx={{ height: 20 }} />}
                    />
                  ),
                }}
                sx={{
                  "& .MuiInputBase-root": {
                    height: 48,
                    width: 331,
                    borderRadius: 3,
                  },
                }}
                value={searchValue}
                onChange={(e) => {
                  setSearchValue(e.target.value);
                }}
              />
              <RoundedButton
                variant="contained"
                onClick={handleClickAdvancedSearchOpen}
                children="Advanced Search"
              />
              <RoundedButton
                variant="contained"
                endIcon={<Add />}
                onClick={() => {
                  navigate("add");
                }}
                children="Add Location"
              />
            </Box>
          </Box>
          {openAdvancedSearch && (
            <AdvancedSearch
              props={advancedSearchProps}
              filter={filter}
              description="Find locations with..."
              onFilterChange={setFilter}
              onApply={getLocationList}
              onClose={setOpenAdvancedSearch}
            />
          )}
          <CustomTable
            title={"LOCATION LIST"}
            headcells={headCells}
            body={mapDataToRender()}
            paging={{
              page: page,
              rowsPerPage: rowsPerPage,
              rowsCount: rowsCount,
            }}
            loading={isLoading}
            toolbar={false}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Box>
      </Box>
    </>
  );
};
