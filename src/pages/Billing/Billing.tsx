import styles from "./Billing.module.css";
import * as React from "react";
import Box from "@mui/material/Box";
import Table from "@mui/material/Table";
import Drawer from "@mui/material/Drawer";
import { Detail } from "./Detail";
import { invoiceModel } from "../../models/invoices";
import { InvoiceGetRequest } from "../../models/apiRequest/invoice";
import { invoiceService } from "../../services/agent";
import {
  IconButton,
  InputAdornment,
  TextField,
  Typography,
} from "@mui/material";
import { CustomTable } from "../../components/Table/Table";
import { Add, Create, Search, Visibility } from "@mui/icons-material";
import { Link, useNavigate } from "react-router-dom";
import { HeadCell } from "../../components/Table/types";
import { RoundedButton } from "../../components/RoundedButton";
import { DeleteDialog } from "../../components/DeleteDialog";
import DeleteIcon from "@mui/icons-material/Delete";
import { AdvancedSearch } from "../../components/Table/AdvancedSearch";

const headCells: HeadCell[] = [
  {
    id: "number",
    numeric: false,
    label: "No.",
  },
  {
    id: "id",
    numeric: false,
    label: "Invoice Id",
  },
  {
    id: "recipient",
    numeric: false,
    label: "Recipient",
  },
  {
    id: "status",
    numeric: false,
    label: "Status",
  },
  {
    id: "date",
    numeric: false,
    label: "Order Date",
  },
  {
    id: "totalNet",
    numeric: false,
    label: "Total net",
  },
  {
    id: "totalGross",
    numeric: false,
    label: "Total gross",
  },
  {
    id: "action",
    numeric: false,
    label: "Action",
  },
];
const advancedSearchProps = [
  {
    name: "recipient",
    label: "Recipient",
  },
  {
    name: "status",
    label: "Status",
    select: true,
    menuItems: [
      { value: "draft", display: "Draft" },
      { value: "issued", display: "Issued" },
      { value: "balanced", display: "Balanced" },
      { value: "cancelled", display: "Cancelled" },
      { value: "entered-in-error", display: "Enter in Error" },
    ],
  },
  {
    name: "date",
    label: "Date",
    dateTime: "single",
    inputFormat: "DD-MM-YYYY"
  }
];

export const Billing = () => {
  const [selected, setSelected] = React.useState<string[]>([]);
  const [isLoading, setIsLoading] = React.useState(false);
  const [data, setData] = React.useState<invoiceModel[]>([]);
  const [page, setPage] = React.useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = React.useState<number>(10);
  const [rowsCount, setRowsCount] = React.useState<number>(0);
  const [deletePopUpOpen, setDeletePopUpOpen] = React.useState(false);
  const [openAdvancedSearch, setOpenAdvancedSearch] = React.useState(false);
  const [currentSelectedId, setCurrentSelectedId] = React.useState("");

  // Search states
  const [nameSearchValue, setNameSearchValue] = React.useState<string>("");
  const [debounceSeachValue, setDebounceSearchValue] =
    React.useState<string>("");
  const [filter, setFilter] = React.useState<{ [key: string]: string }>({});
  const navigate = useNavigate();
  // Api calling functions
  const getInvoiceList = async (filter?: InvoiceGetRequest) => {
    const params: InvoiceGetRequest = {
      ...filter,
      page: page + 1,
      size: rowsPerPage,
      id: debounceSeachValue,
    };
    setIsLoading(true);
    try {
      let { data, paging } = await invoiceService.getList({ params });
      setData(data);
      setRowsCount(paging.total);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      console.log("getInvoiceList error: ", error);
    }
  };

  // Event Handler
  const handleChangePage = (event: any, newPage: number) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event: any) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleSelectAll = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (selected.length === rowsPerPage) {
      setSelected([]);
    } else {
      setSelected(data.map((item) => item.id));
    }
  };

  const handleSelect = (event: React.MouseEvent<unknown>, id: string) => {
    const selectedIndex = selected.indexOf(id);
    let newSelected: string[] = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
  };

  // Details
  const [open, setOpen] = React.useState(false);
  const [isId, setIsId] = React.useState<string>("");

  const handleClickAdvancedSearchOpen = () => {
    setOpenAdvancedSearch(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleClickOpen = (id: string) => {
    setOpen(true);
    setIsId(id);
  };
  //--------------
  const handleDelete = (id: string) => {
    setDeletePopUpOpen(true);
    setCurrentSelectedId(id);
  };
  const handleCancelDelete = () => {
    setDeletePopUpOpen(false);
  };
  const deletePatient = async (id: string) => {
    setIsLoading(true);
    try {
      let response = await invoiceService.delete(id);
      setIsLoading(false);
      return response;
    } catch (error) {
      setIsLoading(false);
      console.log("deletePatient error: ", error);
    }
  };
  const handleConfirmDelete = () => {
    setDeletePopUpOpen(false);
    //currentPatientIdSelected
    deletePatient(currentSelectedId).then((res) => {
      setCurrentSelectedId("");
      getInvoiceList();
    });
  };
  //-------------
  React.useEffect(() => {
    getInvoiceList();
  }, [debounceSeachValue, rowsPerPage, page]);

  React.useEffect(() => {
    let timeout = setTimeout(() => {
      setDebounceSearchValue(nameSearchValue);
    }, 500);

    return () => {
      clearTimeout(timeout);
    };
  }, [nameSearchValue]);

  const mapDataToRender = () => {
    const render = data.map((item, index) => ({
      id: item.id as string,
      number: index +1,
      recipient: item.recipient?.reference || item.recipient?.display,
      status: <Typography textTransform="capitalize" children={item.status} />,
      date: new Date(item.date as string).toLocaleString().split(",")[0],
      totalNet: item.totalNet?.currency + " " + item.totalNet?.value,
      totalGross: item.totalGross?.currency + " " + item.totalGross?.value,
      action: (
        <Box style={{ textAlign: "center" }}>
          <Link
            to={`details/edit?id=${item.id}`}
            children={<IconButton children={<Create />} />}
          />
          <IconButton
            onClick={() => handleClickOpen(item.id)}
            children={<Visibility />}
          />
          <Link
            to={`#`}
            onClick={() => {
              handleDelete(item.id as string);
            }}
            children={<IconButton children={<DeleteIcon />} />}
          />
        </Box>
      ),
    }));
    return render;
  };
  return (
    <>
      <Drawer
        anchor="top"
        open={open}
        children={<Detail id={isId} handleClose={handleClose} rows={data} />}
        sx={{ zIndex: 10000 }}
      />
      <DeleteDialog
        open={deletePopUpOpen}
        onClose={handleCancelDelete}
        onConfirm={handleConfirmDelete}
        onCancel={handleCancelDelete}
      />
      <Box sx={{ width: "100%" }}>
        <Typography
          fontSize={"24px"}
          fontWeight={700}
          color={"#131626"}
          style={{ paddingBottom: "20px" }}
          children="Billing"
        />
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            width: "100%",
            padding: "20px",
            backgroundColor: "white",
            borderRadius: "16px",
          }}
        >
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: "8px",
            }}
          >
            <Typography variant="h2" children={"BILLING LIST"} />
            <Box sx={{ display: "flex", gap: "8px" }}>
              <TextField
                placeholder="Search billing id..."
                InputProps={{
                  startAdornment: (
                    <InputAdornment
                      position="start"
                      children={<Search sx={{ height: 20 }} />}
                    />
                  ),
                }}
                sx={{
                  "& .MuiInputBase-root": {
                    height: 48,
                    width: 331,
                    borderRadius: 3,
                  },
                }}
                value={nameSearchValue}
                onChange={(e) => {
                  setNameSearchValue(e.target.value);
                }}
              />
              <RoundedButton
                variant="contained"
                onClick={handleClickAdvancedSearchOpen}
                children="Advanced Search"
              />
              <RoundedButton
                variant="contained"
                endIcon={<Add />}
                onClick={() => {
                  navigate("add");
                }}
                children="Add Invoice"
              />
            </Box>
          </Box>
          {openAdvancedSearch && (
            <AdvancedSearch
              props={advancedSearchProps}
              filter={filter}
              description="Find invoices with..."
              onFilterChange={setFilter}
              onApply={getInvoiceList}
              onClose={setOpenAdvancedSearch}
            />
          )}
          <CustomTable
            title={"BILLING LIST"}
            headcells={headCells}
            body={mapDataToRender()}
            paging={{
              page: page,
              rowsPerPage: rowsPerPage,
              rowsCount: rowsCount,
            }}
            loading={isLoading}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
            selected={selected}
            handleSelect={handleSelect}
            handleSelectAll={handleSelectAll}
          />
        </Box>
      </Box>
    </>
  );
};
