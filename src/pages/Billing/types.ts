import { createData } from "./table.utils";

export type CreateDataType = typeof createData;
export interface ShippingInfo {
  shippingName: string;
  shippingPhone: string;
}

export interface Data {
  orderId: string;
  shippingInfo: ShippingInfo;
  shippingAddress: string;
  status: string;
  orderDate: string;
  total: number;
  action: any;
}

export type Order = "asc" | "desc";

export interface HeadCell {
  id: keyof Data;
  numeric: boolean;
  label: string;
}

export interface EnhancedTableProps {
  headcells: HeadCell[];
  numSelected: number;
  onRequestSort: (
    event: React.MouseEvent<unknown>,
    property: keyof Data
  ) => void;
  onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
  order: Order;
  orderBy: string;
  rowCount: number;
}

export interface EnhancedTableToolbarProps {
  numSelected: number;
}
