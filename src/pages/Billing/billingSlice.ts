import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import axios from "axios";
import _ from "lodash";

interface GetRequestType {
  endpoint?: string;
  params?: object;
  id?: string;
}

interface PostRequestType {
  endpoint: string;
  body?: object;
}

interface ResponseType {
  data: Array<Response>;
  paging: {
    page: number;
    size: number;
  };
}

export const getAllBilling = createAsyncThunk(
  "billing/getAllBilling",
  async () => {
    try {
      //console.log("get all billing dee");
      const { data } = await axios.get<ResponseType>(
        `https://fhir-service.herokuapp.com/api/invoices`
      );
      //console.log("data", data.data);
      return data.data;
    } catch (err) {
      console.log(err);
    }
  }
);

export const getBillingById = createAsyncThunk(
  "billing/getBillingById",
  async ({ id }: GetRequestType) => {
    try {
      const { data } = await axios.get<ResponseType>(
        `https://fhir-service.herokuapp.com/api/invoices/${id}`
      );
      return data.data;
    } catch (err) {
      console.log(err);
    }
  }
);

export const getAllBillingSlice = createSlice({
  name: "getAllBilling",
  initialState: {
    data: [],
    loading: false,
    error: "" as unknown,
  },
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getAllBilling.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(
      getAllBilling.fulfilled,
      (state, action: PayloadAction<any>) => {
        state.loading = false;
        state.data = action.payload;
      }
    );
    builder.addCase(
      getAllBilling.rejected,
      (state, action: PayloadAction<any>) => {
        state.loading = false;
        state.error = action.payload as unknown;
      }
    );
  },
});

export const getBillingByIdSlice = createSlice({
  name: "getBillingById",
  initialState: {
    data: [],
    loading: false,
    error: "" as unknown,
  },
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getBillingById.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(
      getBillingById.fulfilled,
      (state, action: PayloadAction<any>) => {
        state.loading = false;
        state.data = action.payload;
      }
    );
    builder.addCase(getBillingById.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload as unknown;
    });
  },
});

export const getAllBillingReducer = getAllBillingSlice.reducer;
export const getBillingByIdReducer = getBillingByIdSlice.reducer;
