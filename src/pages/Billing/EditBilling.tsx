import { Box, IconButton, MenuItem, Typography } from "@mui/material";
import React from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import { RoundedButton } from "../../components/RoundedButton";
import { ReactComponent as BackArrow } from "../../assets/image/BackArrow.svg";
import { Participant } from "../../models/careTeam";
import { Section, StyledTextField as TextField, Title } from "./component";
import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { invoiceModel } from "../../models/invoices";
import { invoiceService } from "../../services/agent";
import Loading from "../../components/Loading";

const Status = [
  { value: "draft", display: "Draft" },
  { value: "issued", display: "Issued" },
  { value: "balanced", display: "Balanced" },
  { value: "cancelled", display: "Cancelled" },
  { value: "entered-in-error", display: "Enter in Error" },
];
const currency = [
  { value: "USD", display: "$ (USD)" },
  { value: "EUR", display: "€ (EUR)" },
  { value: "GBP", display: "£ (GBP)" },
];

export const EditBilling: React.FC = () => {
  const navigate = useNavigate();
  const [searchParams, setSearchParams] = useSearchParams();
  const [loading, setLoading] = React.useState<boolean>(true);
  const [id, setId] = React.useState<string>(
    (searchParams.get("id") as unknown as string) || ""
  );
  const [subject, setSubject] = React.useState<string>("");
  const [status, setStatus] = React.useState<string>("");
  const [recipient, setRecipient] = React.useState<string>("");
  const [cancelledReason, setCancelledReason] = React.useState<string>("");
  const [date, setDate] = React.useState<Date>(new Date());
  const [type, setType] = React.useState<string>("");
  const [issuer, setIssuer] = React.useState<string>("");
  const [account, setAccount] = React.useState<string>("");
  const [totalNet, setTotalNet] = React.useState<{
    value: number;
    currency: string;
  }>({ value: 0, currency: "" });
  const [totalGross, setTotalGross] = React.useState<{
    value: number;
    currency: string;
  }>({ value: 0, currency: "" });
  const [participant, setParticipant] = React.useState<
    { role: string; actor: string }[]
  >([{ role: "", actor: "" }]);
  const handleChangeSubject = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSubject(event.target.value as string);
  };
  const handleChangeStatus = (event: React.ChangeEvent<HTMLInputElement>) => {
    setStatus(event.target.value as string);
  };
  const handleChangeRecipient = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRecipient(event.target.value as string);
  };
  const handleChangeCancelledReason = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setCancelledReason(event.target.value as string);
  };
  const handleChangeDate = (value: Date | null) => {
    setDate(value ? value : new Date());
  };
  const handleChangeType = (event: React.ChangeEvent<HTMLInputElement>) => {
    setType(event.target.value as string);
  };
  const handleChangeIssuer = (event: React.ChangeEvent<HTMLInputElement>) => {
    setIssuer(event.target.value as string);
  };
  const handleChangeAccount = (event: React.ChangeEvent<HTMLInputElement>) => {
    setAccount(event.target.value as string);
  };
  const handleTotalGross = (event: React.ChangeEvent<HTMLInputElement>) => {
    setTotalGross({
      value:
        event.target.name === "value"
          ? (event.target.value as unknown as number)
          : totalGross.value,
      currency:
        event.target.name === "currency"
          ? event.target.value
          : totalGross.currency,
    });
  };
  const handleTotalNet = (event: React.ChangeEvent<HTMLInputElement>) => {
    setTotalNet({
      value:
        event.target.name === "value"
          ? (event.target.value as unknown as number)
          : totalNet.value,
      currency:
        event.target.name === "currency"
          ? event.target.value
          : totalNet.currency,
    });
  };
  const handleParticipant = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    index: number
  ) => {
    const newParticipant = {
      role:
        event.target.name === "role"
          ? event.target.value
          : participant[index].role,
      actor:
        event.target.name === "actor"
          ? event.target.value
          : participant[index].actor,
    };
    participant.splice(index, 1, newParticipant);
    setParticipant([...participant]);
  };
  const handleRenderAdd = () => {
    setParticipant([
      ...participant,
      {
        role: "",
        actor: "",
      },
    ]);
  };
  //----------------------------------------------------------------

  const handleAdd = async () => {
    try {
      await invoiceService.post({
        body: {
          status: status,
          cancelledReason: cancelledReason,
          type: {
            text: type,
          },
          subject: {
            reference: subject,
          },
          recipient: {
            reference: recipient,
          },
          date: date.toISOString().split(".")[0] + "Z",
          participant: participant.map((item) => ({
            role: {
              text: item.role,
            },
            actor: {
              reference: item.actor,
            },
          })),
          issuer: {
            reference: issuer,
          },
          account: {
            reference: account,
          },
          totalNet: totalNet,
          totalGross: totalGross,
        },
      });
    } catch (error) {
      console.log(error);
    } finally {
      navigate("/admin/billing");
    }
  };
  //------------------------------------------------------
  React.useEffect(() => {
    try {
      invoiceService
        .getById(searchParams.get("id") as unknown as string)
        .then((res) => {
          setSubject(
            res.subject?.reference || (res.subject?.display as string)
          );
          setStatus(res.status);
          setRecipient(
            res.recipient?.reference || (res.recipient?.display as string)
          );
          setCancelledReason(res.cancelledReason as string);
          setDate(new Date(res.date as string));
          setType(res.type?.text as string);
          setIssuer(res.issuer?.reference || (res.issuer?.display as string));
          setAccount(
            res.account?.reference || (res.account?.display as string)
          );
          setTotalNet(res.totalNet as typeof totalNet);
          setTotalGross(res.totalGross as typeof totalGross);
          setLoading(false)
        });
    } catch (error) {
      console.log(error);
      navigate("/admin/billing");
    }
  }, []);

  return (
    <Box sx={{ display: "flex", flexDirection: "column", gap: "16px" }}>
      {loading ? (
        <Loading />
      ) : (
        <React.Fragment>
          {/* ------------ HEADER --------------*/}
          <Box sx={{ display: "flex", justifyContent: "space-between" }}>
            <Box sx={{ display: "flex", gap: "12px" }}>
              <RoundedButton
                onClick={() => history.back()}
                style={{
                  width: "40px",
                  height: "40px",
                  border: "1px solid #F4F4F4",
                  background: "white",
                }}
                children={<BackArrow />}
              />
              <Typography
                fontWeight={600}
                fontSize="24px"
                children="Add Invoice"
              />
            </Box>
            <RoundedButton
              variant="contained"
              style={{ padding: "10px 20px" }}
              onClick={handleAdd}
              children="Save"
            />
          </Box>

          {/* --------------- BODY --------------- */}
          <Box sx={{ display: "flex", gap: "16px" }}>
            {/* ------------ LEFT --------------*/}
            <Section>
              <Title children="Invoice Information" />
              <TextField disabled label={id} />
              <TextField
                label="Status"
                value={status}
                select
                onChange={handleChangeStatus}
                children={Status.map((item) => (
                  <MenuItem
                    key={item.value}
                    value={item.value}
                    children={item.display}
                  />
                ))}
              />
              <TextField
                label="Subject"
                value={subject}
                onChange={handleChangeSubject}
              />
              <TextField
                label="Recipient"
                value={recipient}
                onChange={handleChangeRecipient}
              />
              <TextField
                label="Cancelled Reason"
                value={cancelledReason}
                onChange={handleChangeCancelledReason}
              />
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "space-between",
                  gap: "8px",
                }}
              >
                <LocalizationProvider
                  dateAdapter={AdapterDateFns}
                  children={
                    <DesktopDatePicker
                      label="Date"
                      inputFormat="dd/MM/yyyy"
                      value={date}
                      onChange={(value) => handleChangeDate(value)}
                      renderInput={(params) => <TextField {...params} />}
                    />
                  }
                />
                <TextField
                  label="Type"
                  value={type}
                  onChange={handleChangeType}
                />
              </Box>
              <TextField
                label="Issuer"
                value={issuer}
                onChange={handleChangeIssuer}
              />
              <TextField
                label="Account"
                value={account}
                onChange={handleChangeAccount}
              />
              <Box sx={{ display: "flex", width: "100%", gap: "8px" }}>
                <React.Fragment>
                  <TextField
                    type="number"
                    name="value"
                    label="Total Net"
                    sx={{ flexBasis: "30%" }}
                    onChange={handleTotalNet}
                  />
                  <TextField
                    select
                    name="currency"
                    label="Currency"
                    sx={{ flexBasis: "20%" }}
                    onChange={handleTotalNet}
                    children={currency.map((item) => (
                      <MenuItem
                        key={item.value}
                        value={item.value}
                        children={item.display}
                      />
                    ))}
                  />
                </React.Fragment>
                <React.Fragment>
                  <TextField
                    type="number"
                    name="value"
                    label="Total Gross"
                    sx={{ flexBasis: "30%" }}
                    onChange={handleTotalGross}
                  />
                  <TextField
                    select
                    name="currency"
                    label="Currency"
                    sx={{ flexBasis: "20%" }}
                    onChange={handleTotalGross}
                    children={currency.map((item) => (
                      <MenuItem
                        key={item.value}
                        value={item.value}
                        children={item.display}
                      />
                    ))}
                  />
                </React.Fragment>
              </Box>
            </Section>
            {/* ------------ RIGHT --------------*/}
            <Section>
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                <Title children="Participants" />
                <IconButton
                  sx={{ border: "1px solid #eaeaea", width: "46px" }}
                  onClick={handleRenderAdd}
                  children="+"
                />
              </Box>
              {participant?.map((item, index) => (
                <Box key={index}>
                  <Typography
                    fontSize="16px"
                    padding={1}
                    children={`Participant ${index + 1}`}
                  />
                  <Box sx={{ display: "flex", gap: "8px" }}>
                    <TextField
                      label="Role"
                      name="role"
                      value={item.role}
                      onChange={(e) => handleParticipant(e, index)}
                    />
                    <TextField
                      label="Actor ID"
                      name="actor"
                      value={item.actor}
                      onChange={(e) => handleParticipant(e, index)}
                    />
                  </Box>
                </Box>
              ))}
            </Section>
          </Box>
        </React.Fragment>
      )}
    </Box>
  );
};
