import * as React from "react";
import { styled } from "@mui/system";
import { TextField, Typography } from "@mui/material";

export const StyledTextField = styled(TextField)({
    width: "100%",
    "& .MuiOutlinedInput-root": {
        height: "100%",
        "& fieldset": {
          border: "1px solid #EAEAEA",
          borderRadius: "0.8rem",
          padding: 0,
          margin: 0
        },
       " &:hover fieldset": {
          border: "1px solid #dbd7d7",       
        },
        "&.Mui-focused fieldset": {
          border: "1px solid #EAEAEA"
        }
      }
});
export const Section = styled("div")({
    padding: "1rem",
    flexBasis: "49%",
    backgroundColor: "#FFFFFF",
    borderRadius: "1rem",
    display: "flex",
    flexDirection: "column",
    gap:"16px"
    // justifyContent: "space-between",
  });

export const Title = styled(Typography)({
    color:"#081735",
    fontSize:"20px",
    fontWeight:600,
    padding:"8px 0",
})