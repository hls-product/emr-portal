import * as React from "react";
import Box from "@mui/material/Box";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableFooter from "@mui/material/TableFooter";
import TableRow from "@mui/material/TableRow";
import Typography from "@mui/material/Typography";
import Paper from "@mui/material/Paper";
import ClearRoundedIcon from "@mui/icons-material/ClearRounded";
import logoIcon from "../../assets/sidebar/logo.svg";
import Grid from "@mui/material/Grid";
import PersonOutlineIcon from "@mui/icons-material/PersonOutline";
import RoomOutlinedIcon from "@mui/icons-material/RoomOutlined";
import PhoneInTalkOutlinedIcon from "@mui/icons-material/PhoneInTalkOutlined";
import Button from "@mui/material/Button";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Switch from "@mui/material/Switch";
import styles from "./Detail.module.css";
import { createData } from "./table.utils";
import VisibilityOutlinedIcon from "@mui/icons-material/VisibilityOutlined";

interface Props {
  id: string;
  handleClose: () => void;
  rows?: any;
}

//------------- data table detail-----------------------------
interface Item {
  nameItem: string;
  noteItem: string;
}
interface ItemDetail {
  id: number;
  item: Item;
  quantity: number;
  unitCost: number;
  amount: number;
}
function createItemDetail(
  id: number,
  item: Item,
  quantity: number,
  unitCost: number,
  amount: number
): ItemDetail {
  return { id, item, quantity, unitCost, amount };
}

const rowsItemDetail = [
  createItemDetail(
    1,
    { nameItem: "Paracetamol", noteItem: "1 tablet everyday" },
    20,
    5,
    100
  ),
  createItemDetail(
    2,
    { nameItem: "Liquiprin", noteItem: "1 tablet everyday" },
    5,
    1,
    5
  ),
  createItemDetail(
    3,
    { nameItem: "Medicine", noteItem: "2 tablets everyday" },
    5,
    1,
    5
  ),
];

export const rows = [
  createData(
    "123456789",
    { shippingName: "Taylor Swift", shippingPhone: "(123) 456-7890" },
    "795 Folsom Ave, Suite 600San Francisco, CA 94107",
    "Paid",
    "26 Jun 2022 08:45",
    121,
    <VisibilityOutlinedIcon />
  ),
  createData(
    "234567891",
    { shippingName: "Katy Perry", shippingPhone: "(123) 456-7890" },
    "795 Folsom Ave, Suite 600San Francisco, CA 94107",
    "Paid",
    "26 Jun 2022 08:45",
    121,
    <VisibilityOutlinedIcon />
  ),
  createData(
    "345678912",
    { shippingName: "Adele", shippingPhone: "(123) 456-7890" },
    "795 Folsom Ave, Suite 600San Francisco, CA 94107",
    "Paid",
    "26 Jun 2022 08:45",
    121,
    <VisibilityOutlinedIcon />
  ),
  createData(
    "456789123",
    { shippingName: "Rihanna", shippingPhone: "(123) 456-7890" },
    "795 Folsom Ave, Suite 600San Francisco, CA 94107",
    "Paid",
    "26 Jun 2022 08:45",
    121,
    <VisibilityOutlinedIcon />
  ),
  createData(
    "567891234",
    { shippingName: "Lady Gaga", shippingPhone: "(123) 456-7890" },
    "795 Folsom Ave, Suite 600San Francisco, CA 94107",
    "Paid",
    "26 Jun 2022 08:45",
    121,
    <VisibilityOutlinedIcon />
  ),
  createData(
    "678912345",
    { shippingName: "Justin Bieber", shippingPhone: "(123) 456-7890" },
    "795 Folsom Ave, Suite 600San Francisco, CA 94107",
    "Paid",
    "26 Jun 2022 08:45",
    121,
    <VisibilityOutlinedIcon />
  ),
  createData(
    "789123456",
    { shippingName: "Celine Dion", shippingPhone: "(123) 456-7890" },
    "795 Folsom Ave, Suite 600San Francisco, CA 94107",
    "Paid",
    "26 Jun 2022 08:45",
    121,
    <VisibilityOutlinedIcon />
  ),
  createData(
    "891234567",
    { shippingName: "One Direction", shippingPhone: "(123) 456-7890" },
    "795 Folsom Ave, Suite 600San Francisco, CA 94107",
    "Paid",
    "26 Jun 2022 08:45",
    121,
    <VisibilityOutlinedIcon />
  ),
  createData(
    "912345678",
    { shippingName: "Fifth Harmony", shippingPhone: "(123) 456-7890" },
    "795 Folsom Ave, Suite 600San Francisco, CA 94107",
    "Paid",
    "26 Jun 2022 08:45",
    121,
    <VisibilityOutlinedIcon />
  ),
];

export const Detail: React.FC<Props> = ({ id, handleClose }) => {
  // const newDetail = rows.filter((row: any) => row.orderId === id)[0];
  const newDetail = rows[0];

  const [currency, setCurrency] = React.useState("usd");
  const [isChecked, setIsChecked] = React.useState(true);

  const handleChangeCurrency = (
    event: React.ChangeEvent<HTMLSelectElement>
  ) => {
    console.log(event.target.value);
    setCurrency(event.target.value);
  };

  return (
    <Box sx={{ backgroundColor: "#FAFAFA" }} role="presentation">
      <Paper className={styles.header_Detail}>
        <Box className={styles.close_Detail} onClick={handleClose}>
          <ClearRoundedIcon
            fontSize="large"
            className={styles.iconClose_Detail}
          />
          <Typography className={styles.textClose_Detail}>Close</Typography>
        </Box>

        <Box className={styles.logo_Detail}>
          <img src={logoIcon} width={50} height={50} />
          <Typography
            variant="h5"
            component="div"
            className={styles.textLogo_Detail}
          >
            Akamental
          </Typography>
        </Box>

        <Typography className={styles.textHeader_Detail}>
          Prescription
        </Typography>
      </Paper>

      <Paper className={styles.content_Detail}>
        <Box>
          <Grid
            container
            spacing="1.3rem"
            direction="row"
            justifyContent="space-between"
            alignItems="center"
          >
            <Grid item md={4}>
              <Box className={styles.box_InfoBilling_Detail}>
                <Typography className={styles.title_InfoBilling_Detail}>
                  Billing Address
                </Typography>
                <Box className={styles.content_InfoBilling_Detail}>
                  <PersonOutlineIcon
                    fontSize="medium"
                    style={{ color: "#6C5DD3" }}
                  />
                  <Typography className={styles.text_InfoBilling_Detail}>
                    {newDetail.shippingInfo.shippingName}
                  </Typography>
                </Box>
                <hr
                  style={{
                    height: 0,
                    border: "none",
                    borderTop: "1px solid #f4f4f4",
                    marginTop: "12px",
                  }}
                />
                <Box className={styles.content_InfoBilling_Detail}>
                  <RoomOutlinedIcon
                    fontSize="medium"
                    style={{ color: "#6C5DD3" }}
                  />
                  <Typography className={styles.text_InfoBilling_Detail}>
                    {newDetail.shippingAddress}
                  </Typography>
                </Box>
                <hr
                  style={{
                    height: 0,
                    border: "none",
                    borderTop: "1px solid #f4f4f4",
                    marginTop: "32px",
                  }}
                />
                <Box className={styles.content_InfoBilling_Detail}>
                  <PhoneInTalkOutlinedIcon
                    fontSize="medium"
                    style={{ color: "#6C5DD3" }}
                  />
                  <Typography className={styles.text_InfoBilling_Detail}>
                    {newDetail.shippingInfo.shippingPhone}
                  </Typography>
                </Box>
              </Box>
            </Grid>

            <Grid item md={4}>
              <Box className={styles.box_InfoBilling_Detail}>
                <Typography className={styles.title_InfoBilling_Detail}>
                  Shipping Address
                </Typography>
                <Box className={styles.content_InfoBilling_Detail}>
                  <PersonOutlineIcon
                    fontSize="medium"
                    style={{ color: "#6C5DD3" }}
                  />
                  <Typography className={styles.text_InfoBilling_Detail}>
                    {newDetail.shippingInfo.shippingName}
                  </Typography>
                </Box>
                <hr
                  style={{
                    height: 0,
                    border: "none",
                    borderTop: "1px solid #f4f4f4",
                    marginTop: "12px",
                  }}
                />
                <Box className={styles.content_InfoBilling_Detail}>
                  <RoomOutlinedIcon
                    fontSize="medium"
                    style={{ color: "#6C5DD3" }}
                  />
                  <Typography className={styles.text_InfoBilling_Detail}>
                    {newDetail.shippingAddress}
                  </Typography>
                </Box>
                <hr
                  style={{
                    height: 0,
                    border: "none",
                    borderTop: "1px solid #f4f4f4",
                    marginTop: "32px",
                  }}
                />
                <Box className={styles.content_InfoBilling_Detail}>
                  <PhoneInTalkOutlinedIcon
                    fontSize="medium"
                    style={{ color: "#6C5DD3" }}
                  />
                  <Typography className={styles.text_InfoBilling_Detail}>
                    {newDetail.shippingInfo.shippingPhone}
                  </Typography>
                </Box>
              </Box>
            </Grid>

            <Grid item md={4}>
              <Box className={styles.box_InfoBilling_Detail}>
                <Typography className={styles.title_InfoBilling_Detail}>
                  Order Info
                </Typography>
                <Box className={styles.content_OrderBilling_Detail}>
                  <Typography className={styles.textLeft_OrderBilling_Detail}>
                    Order ID
                  </Typography>
                  <Typography className={styles.textRight_OrderBilling_Detail}>
                    {newDetail.orderId}
                  </Typography>
                </Box>
                <Box className={styles.middle_OrderBilling_Detail}>
                  <Typography
                    className={styles.textLeftMiddle_OrderBilling_Detail}
                  >
                    Status
                  </Typography>
                  <Box className={styles.textRightMiddle_OrderBilling_Detail}>
                    {newDetail.status}
                  </Box>
                </Box>
                <Box className={styles.content_OrderBilling_Detail}>
                  <Typography className={styles.textLeft_OrderBilling_Detail}>
                    Order Date
                  </Typography>
                  <Typography className={styles.textRight_OrderBilling_Detail}>
                    {newDetail.orderDate}
                  </Typography>
                </Box>
              </Box>
            </Grid>
          </Grid>
        </Box>

        <Box style={{ marginTop: "20px" }}>
          <Grid
            container
            spacing="1.3rem"
            direction="row"
            justifyContent="space-between"
            alignItems="center"
          >
            <Grid item md={8}>
              <Box className={styles.table_Detail}>
                <Typography className={styles.titleTable_Detail}>
                  Order Items
                </Typography>
                <TableContainer component={Paper}>
                  <Table
                    sx={{ minWidth: "40.5rem", background: "#FAFAFA" }}
                    aria-label="order items"
                  >
                    <TableHead>
                      <TableRow>
                        <TableCell
                          className={styles.tableHead_Detail}
                          align="center"
                        >
                          #
                        </TableCell>
                        <TableCell
                          className={styles.tableHead_Detail}
                          align="left"
                        >
                          Item
                        </TableCell>
                        <TableCell
                          className={styles.tableHead_Detail}
                          align="left"
                        >
                          Quantity (unit)
                        </TableCell>
                        <TableCell
                          className={styles.tableHead_Detail}
                          align="left"
                        >
                          Unit Cost
                        </TableCell>
                        <TableCell
                          className={styles.tableHead_Detail}
                          align="left"
                        >
                          Amount
                        </TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody sx={{ background: "white" }}>
                      {rowsItemDetail.map((rowItem) => (
                        <TableRow
                          key={rowItem.id}
                          sx={{
                            "&:last-child td, &:last-child th": { border: 0 },
                          }}
                        >
                          <TableCell
                            className={styles.tableBody_Detail}
                            align="center"
                          >
                            {rowItem.id}
                          </TableCell>
                          <TableCell align="left">
                            <Typography className={styles.tableBody_Detail}>
                              {rowItem.item.nameItem}
                            </Typography>
                            <Typography className={styles.noteItem_Detail}>
                              {rowItem.item.noteItem}
                            </Typography>
                          </TableCell>
                          <TableCell
                            className={styles.tableBody_Detail}
                            align="left"
                          >
                            {rowItem.quantity}
                          </TableCell>
                          <TableCell
                            className={styles.tableBody_Detail}
                            align="left"
                          >
                            $ {rowItem.unitCost}
                          </TableCell>
                          <TableCell
                            className={styles.tableBody_Detail}
                            align="left"
                          >
                            $ {rowItem.amount}
                          </TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                    <TableFooter>
                      <Button className={styles.btnAddItem}>Add Item</Button>
                    </TableFooter>
                  </Table>
                </TableContainer>
              </Box>
            </Grid>

            <Grid item md={4}>
              <Box className={styles.orderInfo}>
                <Typography className={styles.title_OrderInfo_Detail}>
                  Order Info
                </Typography>
                <Box className={styles.line_OrderInfo_Detail}>
                  <Typography className={styles.textLeft_OrderInfo_Detail}>
                    Sub total
                  </Typography>
                  <Typography className={styles.textRight_OrderInfo_Detail}>
                    $ 110
                  </Typography>
                </Box>
                <Box className={styles.line_OrderInfo_Detail}>
                  <Typography className={styles.textLeft_OrderInfo_Detail}>
                    VAT (10%)
                  </Typography>
                  <Typography className={styles.textRight_OrderInfo_Detail}>
                    $ 11
                  </Typography>
                </Box>
                <Box className={styles.line_OrderInfo_Detail}>
                  <Typography className={styles.textLeft_OrderInfo_Detail}>
                    Total
                  </Typography>
                  <Typography className={styles.total_OrderInfo_Detail}>
                    $ 121
                  </Typography>
                </Box>
                <Button className={styles.sendBtn}>Send Prescription</Button>
                <Button className={styles.btn_OrderInfo}>Preview</Button>
                <Button className={styles.btn_OrderInfo}>Download</Button>
              </Box>
            </Grid>
          </Grid>
        </Box>

        <Box style={{ marginTop: "20px" }}>
          <Grid
            container
            spacing="1.3rem"
            direction="row"
            justifyContent="space-between"
            alignItems="center"
          >
            <Grid item md={5}>
              <Box className={styles.box_Currency_Detail}>
                <Typography className={styles.title_Currency}>
                  Currency
                </Typography>

                <select
                  value={currency}
                  onChange={(e) => handleChangeCurrency(e)}
                  className={styles.selectCurrency}
                >
                  <option value="usd">USD (United States dollar)</option>
                  <option value="eur">EUR (Euro)</option>
                  <option value="jpy">JPY (Japanese yen)</option>
                  <option value="vnd">VND (Vietnamese dong)</option>
                </select>

                <FormGroup>
                  <FormControlLabel
                    control={<Switch />}
                    label={
                      <div className={styles.textSwitch}>Payment method</div>
                    }
                    className={styles.fSwitch_Currency}
                  />
                  <FormControlLabel
                    control={<Switch />}
                    label={<div className={styles.textSwitch}>Late fees</div>}
                    className={styles.switch_Currency}
                  />
                  <FormControlLabel
                    control={
                      <Switch
                        checked={isChecked}
                        defaultChecked
                        onChange={(event: any) => {
                          setIsChecked(event.target.checked);
                          console.log(isChecked);
                        }}
                      />
                    }
                    label={<div className={styles.textSwitch}>Notes</div>}
                    className={styles.switch_Currency}
                  />
                </FormGroup>
              </Box>
            </Grid>

            <Grid item md={7}>
              <Box className={styles.box_Notes_Detail}>
                <Typography className={styles.title_Notes}>Notes</Typography>
                <textarea
                  disabled={!isChecked}
                  rows={6}
                  className={styles.textNotes}
                  placeholder="lorem ipsum dolor sit amet, consectetur adipisciofficiaet"
                />
              </Box>
            </Grid>
          </Grid>
        </Box>
      </Paper>
    </Box>
  );
};
