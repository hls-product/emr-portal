// Import React/React Router/Redux/Axios
import axios from "axios";
import React from "react";

// Import MUI components
import AddIcon from "@mui/icons-material/Add";
import { Box, IconButton, InputAdornment, Link, TextField, Typography } from "@mui/material";
import { Add, Create, Search, Visibility } from "@mui/icons-material";
import DeleteIcon from "@mui/icons-material/Delete";

// Import types
import { flagModel } from "../../models/flag";

// Import reducers/api services
import { flagService } from "../../services/agent";

// Import common components
import { RoundedButton } from "../../components/RoundedButton";
import { AdvancedSearch } from "../../components/Table/AdvancedSearch";
import { CustomTable } from "../../components/Table/Table";

// Import style
import { HeadCell } from "../../components/Table/types";

const headCells: HeadCell[] = [
    {
      id: "number",
      numeric: true,
      label: "No.",
    },
    {
      id: "id",
      numeric: false,
      label: "ID",
    },
    {
      id: "patient",
      numeric: false,
      label: "Patient",
    },
    {
      id: "author",
      numeric: false,
      label: "Author",
    },
    {
      id: "status",
      numeric: false,
      label: "Status",
    },
    {
      id: "category",
      numeric: false,
      label: "Category",
    },
    {
      id: "flag",
      numeric: false,
      label: "Flag",
    },
    {
      id: "action",
      numeric: false,
      label: "Action",
    },
  ];

export const Flag: React.FC = () => {
    //state
    const [searchTerm, setSearchTerm] = React.useState<string>("");
    const [rowsCount, setRowsCount] = React.useState<number>(0);

    const [data, setData] = React.useState<flagModel[]>([]);
    const [isLoading, setIsLoading] = React.useState(false);
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);


    //handler event
    const handleChangePage = (event: any, newPage: number) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event: any) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };
    //search states

    const [debounceSeachValue, setDebounceSearchValue] = React.useState<string>("");

    const [searchValue, setSearchValue] = React.useState<string>("");

    const [filter, setFilter] = React.useState<{ [key: string]: string }>({});

    //advance search
    const advancedSearchProps = [
        {
        name: "id",
        label: "ID",
        },
        {
        name: "patient",
        label: "Patient",
        },
        {
        name: "author",
        label: "Author",
        },
        {
        name: "status",
        label: "Status",
        },
        {
        name: "category",
        label: "Category",
        },
        {
        name: "flag",
        label: "Flag",
        },
    ];

  const [advancedSearchPopUpOpen, setAdvancedSearchPopUpOpen] =
    React.useState(false);

  const handleClickAdvancedSearchOpen = () => {
    setAdvancedSearchPopUpOpen(true);
  };
  const handleClickAdvancedSearchCancel = () => {
    setAdvancedSearchPopUpOpen(false);
  };

  const handleFilter = () => {
    setIsLoading(true);
    flagService
      .getList({
        params: {
          ...filter,
          title: debounceSeachValue,
          page: page + 1,
          size: rowsPerPage,
        },
      })
      .then((res) => {
        setData(res.data);
        setRowsCount(res.paging.total);
        setIsLoading(false);
      })
      .catch((err) => console.error(err));
  };

  //api call
  const getFlagList = async (filter?: { [key: string]: string }) => {
    const params = {
      ...filter,
      page: page + 1,
      size: rowsPerPage,
      name: debounceSeachValue,
    };
    setIsLoading(true);
    try {
      let { data, paging } = await flagService.getList({ params });
      setData(data);
      setRowsCount(paging.total);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
    }
  };

  React.useEffect(() => {
    getFlagList();
  }, [debounceSeachValue, rowsPerPage, page]);
  console.log(data)

  //debounce for name search

//   React.useEffect(() => {
//     let timeout = setTimeout(() => {
//       setIsLoading(true);
//       setDebounceSearchValue(searchValue);
//     }, 500);
//     return () => {
//       clearTimeout(timeout);
//     };
//   }, [searchValue]);

  //map data
  const mapDataToRender = () => {
    const render = data.map((item, index) => ({
        number: index + 1,
        id: item.id as string,
        patient: item.subject?.display || "N/A",
        author: item.author?.display || "N/A",
        status: item.status ? item.status : "N/A",
        category:  item.category
            ?.filter((x) => x.coding?.[0])
            .map((x) => x.coding?.[0].display)
            .join(", ") || "-",
        flag: item.code
            ? item.code.text as string
                : "N/A",
        action: (
            <Box style={{ textAlign: "center", whiteSpace: "nowrap" }}>
              <Link
                //to={`details/edit?id=${item.id}`}
                children={<IconButton children={<Create />} />}
              />
              <Link
                //to={`details?id=${item.id}`}
                children={<IconButton children={<Visibility />} />}
              />
    
              <Link
                //to={`#`}
                // onClick={() => {
                //   handleDelete(item.id as string);
                // }}
                children={<IconButton children={<DeleteIcon />} />}
              />
            </Box>
          ),
        }));
        
    return render;
  };


    return(
        <>
        {/* Dialog for delete item popup */}
        {/* <DeleteDialog
          open={deletePopUpOpen}
          onClose={handleCancelDelete}
          onConfirm={handleConfirmDelete}
          onCancel={handleCancelDelete}
        /> */}
        <Box>
          <Typography
            fontSize={"24px"}
            fontWeight={700}
            color={"#131626"}
            style={{ paddingBottom: "20px" }}
            children="Patients"
          />
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              width: "100%",
              padding: "20px",
              backgroundColor: "white",
              borderRadius: "16px",
            }}
          >
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                gap: "8px",
              }}
            >
              <Typography variant="h2" children={"FLAG"} />
              <Box sx={{ display: "flex", gap: "8px" }}>
                <TextField
                  placeholder="Search patient name..."
                  InputProps={{
                    startAdornment: (
                      <InputAdornment
                        position="start"
                        children={<Search sx={{ height: 20 }} />}
                      />
                    ),
                  }}
                  sx={{
                    "& .MuiInputBase-root": {
                      height: 48,
                      width: 331,
                      borderRadius: 3,
                    },
                  }}
                  value={searchValue}
                  onChange={(e) => {
                    setSearchValue(e.target.value);
                  }}
                />
                <RoundedButton
                  variant="contained"
                  onClick={handleClickAdvancedSearchOpen}
                  children="Advanced Search"
                />
                <RoundedButton
                  variant="contained"
                  endIcon={<Add />}
                //   onClick={() => {
                //     navigate("add");
                //   }}
                  children="Add Flag"
                />
              </Box>
            </Box>
            {advancedSearchPopUpOpen && (
              <AdvancedSearch
                props={advancedSearchProps}
                filter={filter}
                description="Find patients with..."
                onFilterChange={setFilter}
                onApply={getFlagList}
                onClose={setAdvancedSearchPopUpOpen}
              />
            )}
            <CustomTable
              title={"FLAG LIST"}
              headcells={headCells}
              body={mapDataToRender()}
              paging={{
                page: page,
                rowsPerPage: rowsPerPage,
                rowsCount: rowsCount,
              }}
              loading={isLoading}
              toolbar={false}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
          </Box>
        </Box>
      </>
    );
}