import { Box } from "@mui/material";
import React from "react";

export const PerformanceDashboard: React.FC = () => {
  return (
    <Box
      position="relative"
      flex={1}
      sx={{ "& > iframe::-webkit-scrollbar": { display: "none" } }}
    >
      <iframe
        src={process.env.REACT_APP_PERFORMANCE_DASHBOARD_URL}
        frameBorder="0"
        style={{
          overflow: "hidden",
          width: "100%",
          height: "1400px",
          position: "absolute",
          top: 0,
          bottom: 0,
          right: 0,
          left: 0,
          msOverflowStyle: "none",
          scrollbarWidth: "none",
          margin: 0,
          padding: 0,
        }}
        allowFullScreen
      ></iframe>
    </Box>
  );
};
