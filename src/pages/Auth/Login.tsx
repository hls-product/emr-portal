/* Libs */
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useKeycloak } from "@react-keycloak/web";

import signIn from "../../assets/image/signIn.svg";
import fbIcon from "../../assets/icon/color_fb.svg";
import ggIcon from "../../assets/icon/color_gg.svg";
import twitterIcon from "../../assets/icon/color_twitter.svg";

import {
  FormControl,
  InputLabel,
  InputAdornment,
  IconButton,
  Typography,
  Button,
} from "@mui/material";
/* Components */
import { Logo } from "../../ui-components/Logo";
/* Styles */
import {
  AuthContainer,
  BackgroundColorSignin,
  StyledLink,
  FlexBox,
  StyledButton,
  StyledTextField,
  StyledOutlineInput,
} from "./style";

import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import { Footer } from "../../layout/Footer";
import Loading from "../../components/Loading";
import { login } from "../../store/reducers/authentication/authSlice";
import { useAppDispatch } from "../../store/Store";
import { useSelector } from "react-redux";
import { IRootState } from "../../store/reducers";
import { STATE } from "../../constants/State";
import { addNotification } from "../../store/reducers/notifications/notificationSlice";

interface State {
  password: string;
  userID: string;
  showPassword: boolean;
}

interface ThirdPartyType {
  id: number;
  icon: string;
  url: string;
}

const listThirdParty: ThirdPartyType[] = [
  {
    id: 0,
    icon: fbIcon,
    url: "https://www.facebook.com/",
  },
  {
    id: 1,
    icon: ggIcon,
    url: "https://www.google.com/",
  },
  {
    id: 2,
    icon: twitterIcon,
    url: "https://twitter.com/",
  },
];

const RenderThirdPartyLogin = () => {
  const navigate = useNavigate();
  return (
    <>
      <Typography
        sx={{ color: "#9FA0A6", fontSize: "12px", fontWeight: "500" }}
        variant="body2"
        component="div"
      >
        Or sign in with
      </Typography>
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyItems: "center",
          gap: "12px",
          width: "fit-content",
        }}
      >
        {listThirdParty.map((item) => (
          <Button
            key={item.id}
            onClick={() => {
              navigate(item.url);
            }}
            sx={{
              border: "1px solid #F4F4F4",
              minWidth: "fit-content",
              borderRadius: "50%",
              padding: "15px",
            }}
          >
            <img src={item.icon} width={24} height={24} />
          </Button>
        ))}
      </div>
    </>
  );
};

export const Login: React.FC = () => {
  const navigate = useNavigate();
  const { keycloak, initialized } = useKeycloak();
  const dispatch = useAppDispatch();
  const token = useSelector((state: IRootState) => state.authReducer.token);
  const isLoading = useSelector(
    (state: IRootState) => state.authReducer.loading
  );

  const handleSubmit = () => {
    navigate("/policy");
  };

  const [values, setValues] = React.useState<State>({
    password: "",
    showPassword: false,
    userID: "",
  });

  const handleClickShowPassword = () => {
    setValues({
      ...values,
      showPassword: !values.showPassword,
    });
  };

  const handleMouseDownPassword = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    event.preventDefault();
  };

  const handleChange =
    (prop: keyof State) => (event: React.ChangeEvent<HTMLInputElement>) => {
      setValues({ ...values, [prop]: event.target.value });
    };

  if (isLoading) return <Loading />;

  useEffect(() => {
    if (keycloak.authenticated) {
      navigate("/admin/dashboard");
    }
  });

  const signin = () => {
    dispatch(
      login({
        username: values.userID,
        password: values.password,
        navigate: navigate,
      })
    )
      .unwrap()
      .then()
      .catch((error) => {
        dispatch(
          addNotification({
            message: error,
            type: STATE.ERROR,
          })
        );
      });
  };

  return (
    <>
      <FlexBox
        column
        justify="flex-start"
        align="center"
        sx={{ minHeight: "750px" }}
      >
        <BackgroundColorSignin>
          <div
            style={{
              display: "flex",
              alignItems: "flex-start",
              justifyContent: "center",
              margin: "auto",
              width: "fit-content",
              gap: "400px",
            }}
          >
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "flex-start",
                justifyContent: "flex-start",
                padding: "39px 0 47px",
              }}
            >
              <Logo variant="secondary" size={1.2} />
              <Typography
                variant="h5"
                component="div"
                sx={{
                  marginTop: "93px",
                  marginLeft: "-11px",
                  fontSize: "32px",
                  fontWeight: "600",
                  color: "#FFFFFF",
                }}
              >
                Welcome Back
              </Typography>
            </div>
            <img src={signIn} />
          </div>
        </BackgroundColorSignin>

        <AuthContainer width={382} gap={20} marginTop={45}>
          <FormControl sx={{ width: "100%" }} variant="outlined">
            <StyledTextField
              id="outlined-multiline-flexible"
              label="Your ID"
              placeholder="Enter your ID here!"
              value={values.userID}
              onChange={handleChange("userID")}
            />
          </FormControl>

          <FormControl sx={{ width: "100%" }} variant="outlined">
            <InputLabel htmlFor="outlined-adornment-password">
              Password
            </InputLabel>
            <StyledOutlineInput
              id="outlined-adornment-password"
              type={values.showPassword ? "text" : "password"}
              value={values.password}
              onChange={handleChange("password")}
              placeholder="Enter your password here!"
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    onMouseDown={handleMouseDownPassword}
                    edge="end"
                  >
                    {values.showPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              }
              label="Password"
            />
          </FormControl>

          <StyledLink to="/forget/send">
            <Typography
              sx={{ color: "#999999", fontSize: "14px", fontWeight: "500" }}
              variant="body2"
              component="div"
              align="right"
            >
              Forget Password ?
            </Typography>
          </StyledLink>

          <StyledButton
            width="322px"
            height="56px"
            borderRadius="99px"
            variant="contained"
            sx={{ fontSize: "18px" }}
            onClick={signin}
          >
            Sign in
          </StyledButton>

          <StyledButton
            width="322px"
            height="56px"
            borderRadius="99px"
            variant="contained"
            onClick={() => {
              if (!keycloak.authenticated) {
                keycloak.login();
              }
            }}
            sx={{ fontSize: "18px" }}
          >
            Sign in with EMR Security
          </StyledButton>

          {/* <Typography
            sx={{
              color: "#9FA0A6",
              fontSize: "14px",
              fontWeight: "500",
              cursor: "pointer",
            }}
            variant="body2"
            component="div"
            onClick={() => {
            }}
          > */}
            {/* Don't have an account?{" "} */}
            {/* <StyledLink className="size-16px weight-400" to="/auth/register">
              Create account
            </StyledLink> */}
          {/* </Typography> */}
          <RenderThirdPartyLogin />
        </AuthContainer>
      </FlexBox>
      <Footer />
    </>
  );
};
