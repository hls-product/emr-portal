import React, { useEffect } from 'react'
import { Typography, FormControl, InputLabel, InputAdornment, IconButton, Grid } from '@mui/material'
import { useNavigate } from 'react-router-dom';
import {AuthContainer, StyledButton, StyledOutlineInput} from '../style'
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';

interface State {
  password: string;
  showPassword: boolean;
}

interface Condition {
  id: number;
  label: string;
}

interface Props {
  isChar: boolean;
  isUpper: boolean;
  isNum: boolean;
  isLength: boolean;
}

const listCondition: Condition[] = [
  {
    id: 0,
    label: '6+ Characters',
  },
  {
    id: 1,
    label: '1+ UPPERCASES',
  },
  {
    id: 2,
    label: '1+ Symbols',
  },
  {
    id: 3,
    label: '1+ Numbers',
  },
]

const RenderPasswordLevel = () => {
  return <div style={{
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    gap: '10px',
  }}>
    {Array(4).fill(<div style={{
      width:'93px',
      height: '4px',
      borderRadius: '10px',
      background: '#EAEAEA',
    }}></div>)}
  </div>
}

const RenderPasswordCondition = (Props: Props) => {
  const {isChar, isLength, isNum, isUpper} = Props;
  return <Grid container spacing={3}>
    {listCondition.map((condition, index)=>(
      <Grid item xs={5} key={condition.id}>
        <div style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'flex-start',
        }}>
          <div style={{
            width: '7px',
            height: '7px',
            borderRadius: '50%',
            background: `${
              '#EAEAEA'
            }`
          }}></div>
          <Typography variant='body2' component='div' sx={{
            marginLeft: '7px',
            fontSize: "13px",
            fontWeight: "500",
            color: "#4E4E4E"
          }}>{condition.label}</Typography>
        </div>
      </Grid>
    ))}
  </Grid>
}

export const SetPassword: React.FC = () => {
  const navigate = useNavigate();
  const [values, setValues] = React.useState<State>({
    password: '',
    showPassword: false,
  });

  const [char, setChar] = React.useState<boolean>(false);
  const [upper, setUpper] = React.useState<boolean>(false);
  const [number, setNumber] = React.useState<boolean>(false);
  const [length, setLength] = React.useState<boolean>(false);

  const isUpper = (str: string) => {
    return !/[a-z]/.test(str) && /[A-Z]/.test(str);
  }

  const containsNumber = (str: string) => {
    return /\d/.test(str);
  }

  const containsSpecialChars = (str: string) => {
    const specialChars = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
    return specialChars.test(str);
  }

  const handleClickShowPassword = () => {
    setValues({
      ...values,
      showPassword: !values.showPassword,
    });
  };

  const handleMouseDownPassword = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    event.preventDefault();
  };
  

  const handleLoadLengthCondition = (value: string) => {
    if(value.length > 5) {
      setLength(true);
    }
    else{
      setLength(false);
    }
  }

  const handleLoadUpperCaseCondition = (value: string) => {
    if(isUpper(value)){
      setUpper(true);
    }else{
      setUpper(false);
    }
  }

  const handleLoadSymbolCondition = (value: string) => {
    if(containsSpecialChars(value)){
      setChar(true);
    }else{
      setChar(false);
    }
  }

  const handleLoadNumberCondition = (value: string) => {
    if(containsNumber(value)){
      setNumber(true);
    }else{
      setNumber(false);
    }
  }

  const handleChange =
    (prop: keyof State) => (event: React.ChangeEvent<HTMLInputElement>) => {
      setValues({ ...values, [prop]: event.target.value });
      handleLoadLengthCondition(values.password);
      handleLoadNumberCondition(values.password);
      handleLoadSymbolCondition(values.password);
      handleLoadUpperCaseCondition(values.password);
    };
  
  return (
    <AuthContainer width={442} gap={20} marginTop={45}>
      <Typography variant='h6' component='div' sx={{fontSize: "24px", fontWeight: "600", color: "#131626", marginBottom: "40px"}}>Set New Password</Typography>
      <FormControl sx={{ width: '100%' }} variant="outlined">
        <InputLabel htmlFor="outlined-adornment-password">
          Password
        </InputLabel>
        <StyledOutlineInput
          id="outlined-adornment-password"
          type={values.showPassword ? 'text' : 'password'}
          value={values.password}
          onChange={handleChange('password')}
          placeholder="Enter your password here!"
          endAdornment={
            <InputAdornment position="end">
              <IconButton
                aria-label="toggle password visibility"
                onClick={handleClickShowPassword}
                onMouseDown={handleMouseDownPassword}
                edge="end"
              >
                {values.showPassword ? <VisibilityOff /> : <Visibility />}
              </IconButton>
            </InputAdornment>
          }
          label="Password"
        />
      </FormControl>
      <RenderPasswordLevel />
      <RenderPasswordCondition isChar={char} isUpper={upper} isNum={number} isLength={length}  />
      <StyledButton
        width="362px"
        height="56px"
        borderRadius="99px"
        variant="contained"
        onClick={()=>{
          navigate('/forget/success');
        }}
        sx={{ fontSize: '18px', marginTop: "40px" }}
        >
        Set New Password
      </StyledButton>
    </AuthContainer>
  )
}