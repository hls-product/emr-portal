/* Libs */
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import {
  FormControl,
  Typography,
} from '@mui/material';
import ReactCodeInput from 'react-code-input';
/* Components */

/* Styles */
import {
  AuthContainer,
  StyledButton,
  StyledLink
} from '../style';
import {StyledCodeInput} from './style'


interface State {
  email: string;
}

export const VerifyCode: React.FC = () => {
  const navigate = useNavigate();
  const [isPinCodeValid, setIsPinCodeValid] = useState<boolean>(true);
  const [pinCode, setPinCode] = useState<string>("");
  const [btnIsPressed, setBtnIsPressed] = useState<boolean>(false);
  const handlePinCodeChange = (pinCode: string) => {
    setPinCode(pinCode);
    setBtnIsPressed(false);
  }
  
  const checkPinCode = () => {
    if(pinCode.length === 4){
      setBtnIsPressed(true);
      navigate('/forget/change-password');
    }
  }

  return (
    <AuthContainer width={442} gap={20} marginTop={45}>
      <Typography variant='h6' component='div' sx={{fontSize: "24px", fontWeight: "600", color: "#131626"}}>Verify Code</Typography>
      <Typography variant='body2' component='div' sx={{fontSize: "16px", fontWeight: "500", color: "#9FA0A6"}}>We’ll text you on +8490506****</Typography>

      <StyledCodeInput
        name="resetPassword"
        inputMode="numeric"
        fields={4}
        type="text"
        onChange={handlePinCodeChange}
        value={pinCode}
      />

      <StyledLink sx={{fontSize: "14px", fontWeight: "400", marginBottom: '12px'}} to="/forget/verify">
        Send me a new code
      </StyledLink>

      <StyledButton
        width="362px"
        height="56px"
        borderRadius="99px"
        variant="contained"
        onClick={checkPinCode}
        sx={{ fontSize: '18px' }}
        >
        Verify Code
      </StyledButton>
    </AuthContainer>
  );
};
