import React from 'react'
import { Typography } from '@mui/material';
import {AuthContainer, StyledButton} from '../style'
import { useNavigate } from 'react-router-dom';
import createAccountSuccess from '../../../assets/image/createAccountSuccess.svg';

export const SetPasswordSuccess: React.FC = () => {
  const navigate = useNavigate();
  return (
    <AuthContainer width={402} gap={20} marginTop={45}>
      <img src={createAccountSuccess} />
      <Typography variant='h6' component='div' sx={{fontSize: "24px", fontWeight: "600", color: "#131626"}}>Set new password successfully !</Typography>
      <Typography variant='body2' component='div' sx={{fontSize: "16px", fontWeight: "500", color: "#9FA0A6"}}>You can login your account with new password</Typography>
      <StyledButton
        width="362px"
        height="56px"
        borderRadius="99px"
        variant="contained"
        onClick={()=>{
          navigate('/login');
        }}
        sx={{ fontSize: '18px' }}
        >
        Login
      </StyledButton>
    </AuthContainer>
  )
}
