import { OutlinedInput, styled as muiStyled, TextField } from '@mui/material';
import ReactCodeInput from 'react-code-input';

export const StyledCodeInput = muiStyled(ReactCodeInput)(({})=>({
    marginTop: "20px",
    marginBottom: "12px",
    'input': {
        width: '56px',
        height: '56px',
        border: '1px solid #EAEAEA',
        borderRadius: '12px',
        marginRight: '24px',
        fontSize: '24px',
        fontWeight: '600',
        color: '#131626',
        padding: "12px 20px"
    },
}))