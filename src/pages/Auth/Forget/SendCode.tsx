import React from 'react'
import {FormControl} from '@mui/material'
import { useNavigate } from 'react-router-dom'

import {AuthContainer, StyledButton, StyledTextField} from '../style'

interface State {
  email: string;
}

export const SendCode: React.FC = () => {
  const navigate = useNavigate();
  const [values, setValues] = React.useState<State>({
    email: '',
  });

  const handleMouseDownPassword = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    event.preventDefault();
  };

  const handleChange =
    (prop: keyof State) => (event: React.ChangeEvent<HTMLInputElement>) => {
      setValues({ ...values, [prop]: event.target.value });
    };
  return (
    <AuthContainer width={442} gap={40} marginTop={70}>
      <FormControl
        sx={{  width: '100%' }}
        variant="outlined"
      >
        <StyledTextField
          id="outlined-multiline-flexible"
          label="Your email or phone number"
          value={values.email}
          onChange={handleChange('email')}
          placeholder='Enter your email or phone number here !'
        />
      </FormControl>
        <StyledButton
          width="362px"
          height="56px"
          borderRadius="99px"
          variant="contained"
          onClick={() => navigate('/forget/verify')}
          sx={{ fontSize: '18px' }}
        >
          Get Code
        </StyledButton>
    </AuthContainer> 
  )
}
