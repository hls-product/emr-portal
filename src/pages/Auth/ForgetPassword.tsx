/* Libs */
import React, { useState } from 'react';
import { useNavigate, Routes } from 'react-router-dom';
/* Components */
import {ForgetPasswordHeader} from '../../layout/ForgetPasswordHeader'
import {Footer} from '../../layout/Footer';
import { ForgetPasswordContent } from '../../layout/ForgetPasswordContent';
import { getRoutes, ForgetRoutes } from '../../PortalRoutes';
/* Styles */
import {
  FlexBox,
} from './style';

export const ForgetPassword: React.FC = () => {
  return (
    <>
      <FlexBox column justify="flex-start" align="center" sx={{minHeight: "750px"}}>
        <ForgetPasswordHeader/>
        <ForgetPasswordContent>
          <Routes>
          {getRoutes(ForgetRoutes)}
          </Routes>
        </ForgetPasswordContent>
      </FlexBox>
      <Footer />
    </>
  );
};
