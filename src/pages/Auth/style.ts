import { OutlinedInput, styled as muiStyled, TextField } from '@mui/material';
import { Link } from 'react-router-dom';
import { colorPalette } from '../../theme/ColorPalette';
import { ButtonProps, Button } from '@mui/material';

interface StyledButtonProps extends ButtonProps {
    width: string;
    height: string;
    borderRadius: string;
    component?: React.ElementType;
    defaultActiveStyle?: boolean;
    to?: string;
  }

  interface FlexBoxProps {
    maxHeight?: boolean;
    column?: boolean;
    justify: 'flex-start' | 'flex-end' | 'center' |'space-between' | 'space-around' | 'space-evenly' | 'initial' | 'inherit';
    align: 'flex-start' | 'flex-end' | 'center' |'space-between' | 'space-around' | 'space-evenly' | 'initial' | 'inherit';
  }

  interface StyledButtonProps extends ButtonProps {
    width: string;
    height: string;
    borderRadius: string;
    component?: React.ElementType;
    defaultActiveStyle?: boolean;
    to?: string;
  }

export const AuthContainer = muiStyled('div')<{width: number, gap: number, marginTop: number}>(({ theme, width, gap, marginTop }) => ({
    backgroundColor: "#ffffff",
    // border: `5px solid ${theme.palette.primary.main}`,
    width: `${width}px`,
    minHeight: 'fit-content',
    padding: '20px',
    display: 'flex',
    flexDirection: 'column',
    gap: `${gap}px`,
    alignItems: 'center',
    marginTop: `${marginTop}px`,
  }))

  export const BackgroundColorSignin = muiStyled('div')(({ theme }) => ({
    backgroundColor: colorPalette.purple.shade_500,
    width: '100%',
    height: '237px',
  }))

  export const BackgroundImage = muiStyled('img')<{ src: string, zIndex: number, top: string, right: string, left: string, bottom: string }>(({ theme, zIndex, top, right, left, bottom }) => ({
    borderRadius: '8px',
    position: 'absolute',
    // border: '5px solid red',
    zIndex: zIndex,
    top: top,
    right: right,
    bottom: bottom,
    left: left,
    height: '250px',
  }))

  export const MainRegisterLogo = muiStyled('img')<{ src: string, top: string, right: string, left: string, bottom: string}>(({ theme, top, right, left, bottom}) => ({
    borderRadius: '16%',
    position: 'absolute',
    // border: '5px solid red',
    top: top,
    right: right,
    bottom: bottom,
    left: left,
    width: '10%',
    height: '10%',
  }))

  export const StyledLink = muiStyled(Link)(({ theme }) => ({
    color: theme.palette.primary.main,
    textDecoration: 'none',
    width: "100%"
  }))

  export const FlexBox = muiStyled('div')<FlexBoxProps>`
  display: flex;
  width: 100%;
  height: ${props => props.maxHeight ? '100%' : 'auto'};
  flex-direction: ${props => props.column ? 'column' : 'row'};
  justify-content: ${props => props.justify};
  align-items: ${props => props.align};
`

export const StyledButton = muiStyled(Button)<StyledButtonProps>(({ theme, width, height, borderRadius }) => ({
    width: width,
    height: height,
    borderRadius: borderRadius
  }))

export const StyledTextField = muiStyled(TextField)(({})=>({
  '& .MuiOutlinedInput-notchedOutline':{
    borderRadius: "12px",
    border: "1px solid #EAEAEA"
  }
}))

export const StyledOutlineInput = muiStyled(OutlinedInput)(({})=>({
  '& .MuiOutlinedInput-notchedOutline':{
    borderRadius: "12px",
    border: "1px solid #EAEAEA"
  }
}))
