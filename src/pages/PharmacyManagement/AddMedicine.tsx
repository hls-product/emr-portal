import React from "react";
import {
  Box,
  FormControl,
  IconButton,
  InputBase,
  InputLabel,
  MenuItem,
  Select,
  styled,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import { RoundedButton } from "../../components/RoundedButton";
import { ReactComponent as DownloadIcon } from "../../assets/icon/xls-download.svg";
import { colorPalette } from "../../theme";
import { Check, KeyboardBackspace } from "@mui/icons-material";
import { FileUpload } from "../../components/FileUpload";
import { useNavigate } from "react-router-dom";
import { createMedicine } from "../../store/reducers/medicine/medicineSlice";
import { useAppDispatch } from "../../store/Store";

const WhiteBox = styled(Box)({
  backgroundColor: colorPalette.white,
  borderRadius: "16px",
  flexGrow: 1,
});

const checklist = [
  ["Name", true],
  ["Company", true],
  ["Composition", true],
  ["Group", true],
  ["Unit", false],
  ["Min Level", false],
  ["Re-Order Level", true],
  ["VAT", false],
  ["Unit/Packing", false],
  ["Note", false],
];

export const AddMedicine: React.FC = () => {
  const [file, setFile] = React.useState<File>();
  const navigate = useNavigate();
  const dispatch = useAppDispatch();

  const accessToken = localStorage.getItem("access_token") as string;

  const [name, setName] = React.useState<string>("");
  const [company, setCompany] = React.useState<string>("");
  const [category, setCategory] = React.useState<string>("Select");
  const [unit, setUnit] = React.useState<number>();
  const [packing, setPacking] = React.useState<number>();

  const data = {
    code: {
      coding: [{ display: name }],
    },
    manufacturer: { reference: company },
    form: {
      coding: [{ display: category }],
    },
    amount: {
      numerator: { value: unit },
      denominator: { value: packing },
    },
  };

  const handleCreate = () => {
    dispatch(createMedicine(data, accessToken));
    navigate("/admin/medicine");
  };

  return (
    <Box sx={{ backgroundColor: "#FAFAFA" }}>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          mb: "16px",
        }}
      >
        <Box sx={{ display: "flex", alignItems: "center", gap: "16px" }}>
          <IconButton
            sx={{
              backgroundColor: colorPalette.white,
              borderColor: colorPalette.line,
              color: colorPalette.primary,
            }}
            onClick={() => history.back()}
          >
            <KeyboardBackspace />
          </IconButton>
          <Typography variant="h2" component="span">
            Import medicines
          </Typography>
        </Box>

        <RoundedButton
          variant="outlined"
          color="primary"
          sx={{
            height: 40,
            backgroundColor: colorPalette.white,
            borderColor: colorPalette.line,
          }}
          startIcon={<DownloadIcon />}
          href={`${process.env.PUBLIC_URL}/sample-medicine.xls`}
        >
          Download Sample Data
        </RoundedButton>
      </Box>
      <Box sx={{ display: "flex", flexFlow: "row", gap: "20px" }}>
        <WhiteBox>
          <TableContainer sx={{ p: "16px" }}>
            <Table>
              <TableHead sx={{ backgroundColor: "#FAFAFA" }}>
                <TableRow>
                  <TableCell sx={{ fontWeight: 600 }}>Column</TableCell>
                  <TableCell
                    align="center"
                    sx={{ width: 107, fontWeight: 600 }}
                  >
                    Required
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow>
                  <TableCell>
                    <InputBase
                      placeholder="Name"
                      inputProps={{
                        style: {
                          color: "#131626",
                        },
                      }}
                      value={name}
                      onChange={(e) => setName(e.target.value)}
                    />
                  </TableCell>
                  <TableCell align="center">
                    <Check
                      sx={{
                        width: 24,
                        height: 24,
                        color: colorPalette.green.shade_500,
                      }}
                    />
                  </TableCell>
                </TableRow>

                <TableRow>
                  <TableCell>
                    <InputBase
                      placeholder="Company"
                      inputProps={{
                        style: {
                          color: "#131626",
                        },
                      }}
                      value={company}
                      onChange={(e) => setCompany(e.target.value)}
                    />
                  </TableCell>
                  <TableCell align="center">
                    <Check
                      sx={{
                        width: 24,
                        height: 24,
                        color: colorPalette.green.shade_500,
                      }}
                    />
                  </TableCell>
                </TableRow>

                <TableRow>
                  <TableCell>
                    <InputBase
                      placeholder="Composition"
                      inputProps={{
                        style: {
                          color: "#131626",
                        },
                      }}
                    />
                  </TableCell>
                  <TableCell align="center">
                    <Check
                      sx={{
                        width: 24,
                        height: 24,
                        color: colorPalette.green.shade_500,
                      }}
                    />
                  </TableCell>
                </TableRow>

                <TableRow>
                  <TableCell>
                    <InputBase
                      placeholder="Group"
                      inputProps={{
                        style: {
                          color: "#131626",
                        },
                      }}
                    />
                  </TableCell>
                  <TableCell align="center">
                    <Check
                      sx={{
                        width: 24,
                        height: 24,
                        color: colorPalette.green.shade_500,
                      }}
                    />
                  </TableCell>
                </TableRow>

                <TableRow>
                  <TableCell>
                    <InputBase
                      placeholder="Unit"
                      inputProps={{
                        style: {
                          color: "#131626",
                        },
                      }}
                      value={unit}
                      onChange={(e) => setUnit(Number(e.target.value))}
                    />
                  </TableCell>
                  <TableCell />
                </TableRow>

                <TableRow>
                  <TableCell>
                    <InputBase
                      placeholder="Min Level"
                      inputProps={{
                        style: {
                          color: "#131626",
                        },
                      }}
                    />
                  </TableCell>
                  <TableCell />
                </TableRow>

                <TableRow>
                  <TableCell>
                    <InputBase
                      placeholder="Re-Order Level"
                      inputProps={{
                        style: {
                          color: "#131626",
                        },
                      }}
                    />
                  </TableCell>
                  <TableCell align="center">
                    <Check
                      sx={{
                        width: 24,
                        height: 24,
                        color: colorPalette.green.shade_500,
                      }}
                    />
                  </TableCell>
                </TableRow>

                <TableRow>
                  <TableCell>
                    <InputBase
                      placeholder="VAT"
                      inputProps={{
                        style: {
                          color: "#131626",
                        },
                      }}
                    />
                  </TableCell>
                  <TableCell />
                </TableRow>

                <TableRow>
                  <TableCell>
                    <InputBase
                      placeholder="Unit/Packing"
                      inputProps={{
                        style: {
                          color: "#131626",
                        },
                      }}
                      value={packing}
                      onChange={(e) => setPacking(Number(e.target.value))}
                    />
                  </TableCell>
                  <TableCell />
                </TableRow>

                <TableRow>
                  <TableCell>
                    <InputBase
                      placeholder="Note"
                      inputProps={{
                        style: {
                          color: "#131626",
                        },
                      }}
                    />
                  </TableCell>
                  <TableCell />
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
        </WhiteBox>
        <WhiteBox sx={{ p: "16px", height: "fit-content" }}>
          <FormControl fullWidth sx={{ gap: "16px" }}>
            <InputLabel id="medicine-category" required shrink>
              Medicine Category
            </InputLabel>
            <Select
              labelId="medicine-category"
              label="Medicine Category*"
              value={category}
              onChange={(e) => setCategory(e.target.value)}
            >
              <MenuItem value="Select" disabled>
                Select
              </MenuItem>
              <MenuItem value="Capsule">Capsule</MenuItem>
              <MenuItem value="Injection">Injection</MenuItem>
              <MenuItem value="Tablet">Tablet</MenuItem>
            </Select>
            <FileUpload file={file} setFile={setFile} />
            <RoundedButton
              color="primary"
              variant="contained"
              sx={{ height: 48, width: 158, mx: "auto" }}
              disabled={
                !(name && company && file && category && category !== "Select")
              }
              onClick={handleCreate}
            >
              Import Medicines
            </RoundedButton>
          </FormControl>
        </WhiteBox>
      </Box>
    </Box>
  );
};
