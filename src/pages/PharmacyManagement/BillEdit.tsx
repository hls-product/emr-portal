import React from "react";
import {
  Box,
  IconButton,
  InputLabel,
  styled,
  MenuItem,
  Select as MuiSelect,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TextField as MuiTextField,
  Typography,
  SxProps,
  FormControl,
  Divider,
  InputAdornment,
  TextFieldProps,
} from "@mui/material";
import { useParams } from "react-router-dom";
import {
  DeleteOutlined,
  EventOutlined,
  ExpandMoreOutlined,
  KeyboardBackspace,
  Percent,
} from "@mui/icons-material";
import { RoundedButton } from "../../components/RoundedButton";
import { colorPalette } from "../../theme";
import styles from "./BillEdit.module.scss";

const medicine = [
  {
    category: "Capsule",
    name: "WORMSTOP",
    batch: 3432,
    unit: 20,
    expiryDate: "May 2023",
    quantity: 2,
    price: "100.00",
    tax: "10%",
    amount: 200,
  },
  {
    category: "Injection",
    name: "BICASOL",
    batch: 456,
    unit: 20,
    expiryDate: "May 2023",
    quantity: 2,
    price: "100.00",
    tax: "10%",
    amount: 200,
  },
];
const categories = medicine.map((m) => m.category);
const names = medicine.map((m) => m.name);
const batches = medicine.map((m) => m.batch);

const DisabledTableCell = styled(TableCell)({
  cursor: "not-allowed",
  opacity: 0.5,
});

const WhiteButton = styled(RoundedButton)({
  height: 40,
  backgroundColor: colorPalette.white,
  borderColor: colorPalette.line,
});

const Highlight = styled("span")({
  color: colorPalette.primary,
  marginRight: "24px",
});

const TextField: React.FC<
  | {
      label: string;
      defaultValue: string;
      sx?: SxProps;
    }
  | TextFieldProps
> = ({ label, defaultValue, sx, ...props }) => (
  <MuiTextField
    label={label}
    defaultValue={defaultValue}
    sx={{
      input: {
        color: colorPalette.dark,
        fontSize: "16px",
        fontWeight: 500,
      },
      label: {
        color: colorPalette.dark,
        fontWeight: 500,
      },
      ...sx,
    }}
    {...props}
  />
);

const Select: React.FC<{
  defaultValue: string | number;
  options: (string | number)[];
}> = ({ defaultValue, options }) => {
  return (
    <MuiSelect
      defaultValue={defaultValue}
      inputProps={{
        sx: {
          color: colorPalette.dark,
          height: "40px !important",
          lineHeight: "40px",
          py: 0,
          pl: "16px",
        },
      }}
      IconComponent={ExpandMoreOutlined}
      sx={{ borderRadius: "99px", width: 1 }}
    >
      {options.map((o) => (
        <MenuItem key={o} value={o}>
          {o}
        </MenuItem>
      ))}
    </MuiSelect>
  );
};

const TableHeader: React.FC<{ columns: string[] }> = ({ columns }) => {
  return (
    <TableHead sx={{ backgroundColor: "#FAFAFA" }}>
      <TableRow>
        {columns.map((c) => (
          <TableCell key={c} sx={{ fontSize: 14, fontWeight: 600 }}>
            {c}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
};

export const BillEdit = () => {
  const { id } = useParams();
  const [billItems, setBillItems] = React.useState(medicine);

  function removeBillItem(index: number) {
    let newBillItems = billItems.slice();
    newBillItems.splice(index, 1);
    setBillItems(newBillItems);
  }

  return (
    <Box
      sx={{
        backgroundColor: "#FAFAFA",
        minHeight: "calc(100vh - 100px)",
      }}
    >
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            gap: "16px",
          }}
        >
          <IconButton
            color="primary"
            sx={{ backgroundColor: colorPalette.white }}
            onClick={() => history.back()}
          >
            <KeyboardBackspace />
          </IconButton>
          <Typography component="span" variant="h2">
            {id}
          </Typography>
        </Box>

        <Box sx={{ display: "flex", gap: "12px" }}>
          <WhiteButton variant="outlined" sx={{ width: 93 }}>
            Cancel
          </WhiteButton>
          <WhiteButton variant="outlined" sx={{ width: 125 }}>
            Save & Print
          </WhiteButton>
          <RoundedButton variant="contained" sx={{ height: 40, width: 72 }}>
            Save
          </RoundedButton>
        </Box>
      </Box>
      <Box
        sx={{
          backgroundColor: colorPalette.white,
          borderRadius: "16px",
          p: "16px",
          my: "16px",
        }}
      >
        <Box
          sx={{ display: "flex", justifyContent: "space-between", mb: "16px" }}
        >
          <span style={{ fontSize: "16px", fontWeight: 600 }}>
            BILL NO: <Highlight>{id}</Highlight>
            CASE ID: <Highlight>1641</Highlight>
            COLLECTED BY: <Highlight>SUPPER ADMIN</Highlight>
          </span>
          <span
            style={{
              color: colorPalette.lightGrey,
              fontSize: "14px",
              fontWeight: 500,
            }}
          >
            <EventOutlined
              sx={{ height: 18, width: 18, verticalAlign: "text-top" }}
            />
            Date:&nbsp;
            <span style={{ color: colorPalette.grey }}>
              04.07.2022 01:10 PM
            </span>
          </span>
        </Box>
        <Table>
          <TableHeader
            columns={[
              "Medicine Category",
              "Medicine Name",
              "Batch No",
              "Expiry Date",
              "Quantity",
              "Sale Price ($)",
              "Tax ($)",
              "Amount ($)",
              "Action",
            ]}
          />
          <TableBody>
            {billItems.map(
              (
                {
                  category,
                  name,
                  batch,
                  unit,
                  expiryDate,
                  quantity,
                  price,
                  tax,
                  amount,
                },
                index
              ) => (
                <TableRow key={index}>
                  <TableCell>
                    <Select defaultValue={category} options={categories} />
                  </TableCell>
                  <TableCell>
                    <Select defaultValue={name} options={names} />
                  </TableCell>
                  <TableCell>
                    <Select defaultValue={batch} options={batches} />
                  </TableCell>
                  <DisabledTableCell>{expiryDate}</DisabledTableCell>
                  <TableCell>
                    <input
                      className={styles.number}
                      type="number"
                      defaultValue={quantity}
                    />
                  </TableCell>
                  <DisabledTableCell>{price}</DisabledTableCell>
                  <DisabledTableCell>{tax}</DisabledTableCell>
                  <DisabledTableCell>{amount}</DisabledTableCell>
                  <TableCell>
                    <IconButton onClick={() => removeBillItem(index)}>
                      <DeleteOutlined />
                    </IconButton>
                  </TableCell>
                </TableRow>
              )
            )}
          </TableBody>
        </Table>
      </Box>

      <Box
        sx={{ display: "flex", gap: "20px", justifyContent: "space-between" }}
      >
        <Box
          sx={{
            display: "flex",
            flexFlow: "column",
            gap: "16px",
            flexGrow: 1,
            backgroundColor: colorPalette.white,
            borderRadius: "16px",
            p: "16px",
          }}
        >
          <TextField
            label="Name"
            defaultValue="Olivier Thomas (1)"
            InputProps={{ sx: { borderRadius: "12px" } }}
          />
          <FormControl>
            <InputLabel
              id="hospital-doctor"
              shrink
              sx={{ color: colorPalette.dark }}
            >
              Hospital Doctor
            </InputLabel>
            <MuiSelect
              labelId="hospital-doctor"
              label="Hospital Doctor"
              defaultValue="ABC Hospital"
              sx={{ borderRadius: "12px" }}
            >
              <MenuItem value="ABC Hospital">ABC Hospital</MenuItem>
            </MuiSelect>
          </FormControl>
          <MuiTextField
            label="Note"
            defaultValue="Enter"
            sx={{ label: { color: colorPalette.dark, fontWeight: 500 } }}
            InputProps={{ sx: { minHeight: 120, borderRadius: "12px" } }}
            multiline
          />
        </Box>
        <Box
          sx={{
            display: "flex",
            flexFlow: "column",
            gap: "16px",
            flexGrow: 1,
            backgroundColor: colorPalette.white,
            borderRadius: "16px",
            p: "16px",
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "space-between" }}>
            <Typography variant="h6" sx={{ color: colorPalette.dark }}>
              Total ($)
            </Typography>
            <Typography variant="body2" sx={{ color: colorPalette.grey }}>
              300.00
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <Typography variant="h6" sx={{ color: colorPalette.dark }}>
              Discount
            </Typography>
            <MuiTextField
              placeholder="0.00"
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <Percent />
                  </InputAdornment>
                ),
                sx: {
                  width: 106,
                  borderRadius: "12px",
                },
              }}
            />
          </Box>
          <Box sx={{ display: "flex", justifyContent: "space-between" }}>
            <Typography variant="h6" sx={{ color: colorPalette.dark }}>
              Tax ($)
            </Typography>
            <Typography variant="body2" sx={{ color: colorPalette.grey }}>
              25.00
            </Typography>
          </Box>
          <Box sx={{ display: "flex", justifyContent: "space-between" }}>
            <Typography variant="h6" sx={{ color: colorPalette.dark }}>
              Net Amount ($)
            </Typography>
            <Typography variant="body2" sx={{ color: colorPalette.grey }}>
              295.00
            </Typography>
          </Box>
          <Divider />
          <Typography variant="h3">REFUND AMOUNT</Typography>
          <FormControl>
            <InputLabel
              id="payment-mode"
              shrink
              sx={{ color: colorPalette.dark }}
            >
              Payment Mode
            </InputLabel>
            <MuiSelect
              labelId="payment-mode"
              label="Payment mode"
              defaultValue="Cash"
              sx={{ borderRadius: "12px" }}
            >
              <MenuItem value="Cash">Cash</MenuItem>
            </MuiSelect>
          </FormControl>
          <MuiTextField
            label="Payment Amount ($)"
            defaultValue="0.00"
            InputProps={{
              sx: {
                width: 1,
                borderRadius: "12px",
              },
            }}
          />
        </Box>
      </Box>
    </Box>
  );
};
