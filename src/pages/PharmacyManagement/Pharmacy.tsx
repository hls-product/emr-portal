import { Add, Delete, EditOutlined, Search } from "@mui/icons-material";
import {
  Box,
  Checkbox,
  Chip,
  IconButton,
  InputAdornment,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  TextField,
  Typography
} from "@mui/material";
import { format } from "date-fns";
import React from "react";
import { useNavigate } from "react-router-dom";
import { DeleteDialog } from "../../components/DeleteDialog";
import { RoundedButton } from "../../components/RoundedButton";
import { colorPalette } from "../../theme";

const bills = [
  {
    bill: "B123456",
    caseID: "1678",
    date: new Date("04 May 2022 01:10 PM"),
    patient: "Olivier Thomas",
    doctor: "Amit Singh",
    discount: "30.00 (10.00%)",
    amount: 295,
    paidAmount: 295,
    shippingMethod: "Shipping",
    paymentMethod: "Credit Card",
  },
  {
    bill: "B123456",
    caseID: "1678",
    date: new Date("04 May 2022 01:10 PM"),
    patient: "Olivier Thomas",
    doctor: "Amit Singh",
    discount: "30.00 (10.00%)",
    amount: 295,
    paidAmount: 295,
    shippingMethod: "Shipping",
    paymentMethod: "Bank Transfer",
  },
  {
    bill: "B123456",
    caseID: "1678",
    date: new Date("04 May 2022 01:10 PM"),
    patient: "Olivier Thomas",
    doctor: "Amit Singh",
    discount: "30.00 (10.00%)",
    amount: 295,
    paidAmount: 295,
    shippingMethod: "Pickup",
    paymentMethod: "Bank Transfer",
  },
  {
    bill: "B123456",
    caseID: "1678",
    date: new Date("04 May 2022 01:10 PM"),
    patient: "Olivier Thomas",
    doctor: "Amit Singh",
    discount: "30.00 (10.00%)",
    amount: 295,
    paidAmount: 295,
    shippingMethod: "None",
    paymentMethod: null,
  },
];

interface THeadProps {
  numSelected: number;
  onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
  rowCount: number;
}

const THead: React.FC<THeadProps> = (props) => (
  <TableHead>
    <TableRow>
      <TableCell>
        <Checkbox
          indeterminate={
            props.numSelected > 0 && props.numSelected < props.rowCount
          }
          checked={props.numSelected === props.rowCount}
          onChange={props.onSelectAllClick}
        />
      </TableCell>
      <TableCell sx={{ minWidth: 140, fontWeight: 600 }}>Bill No</TableCell>
      <TableCell sx={{ minWidth: 130, fontWeight: 600 }}>Case ID</TableCell>
      <TableCell sx={{ minWidth: 130, fontWeight: 600 }}>Date</TableCell>
      <TableCell sx={{ minWidth: 130, fontWeight: 600 }}>
        Patient Name
      </TableCell>
      <TableCell sx={{ minWidth: 130, fontWeight: 600 }}>Doctor name</TableCell>
      <TableCell sx={{ minWidth: 130, fontWeight: 600 }}>
        Discount ($)
      </TableCell>
      <TableCell sx={{ minWidth: 130, fontWeight: 600 }}>Amount ($)</TableCell>
      <TableCell sx={{ minWidth: 130, fontWeight: 600 }}>
        Paid Amount ($)
      </TableCell>
      <TableCell sx={{ minWidth: 140, fontWeight: 600 }}>
        Shipping Method
      </TableCell>
      <TableCell sx={{ minWidth: 140, fontWeight: 600 }}>
        Payment Method
      </TableCell>
      <TableCell sx={{ minWidth: 120, fontWeight: 600 }}>Action</TableCell>
    </TableRow>
  </TableHead>
);

function shippingMethodSx(method: string) {
  switch (method) {
    case "Shipping":
      return {
        color: colorPalette.orange.shade_500,
        borderColor: colorPalette.orange.shade_100,
      };
    case "Pickup":
      return {
        color: colorPalette.blue.shade_500,
        borderColor: colorPalette.blue.shade_100,
      };
    default:
      return { color: colorPalette.dark, borderColor: colorPalette.stroke };
  }
}

function paymentMethodSx(method: string) {
  switch (method) {
    case "Credit Card":
      return {
        color: colorPalette.blue.shade_500,
        borderColor: colorPalette.blue.shade_100,
      };
    case "Bank Transfer":
      return {
        color: colorPalette.primary,
        borderColor: colorPalette.purple.shade_50,
      };
    default:
      return {};
  }
}

const tableContainerStyle = {
  backgroundColor: "#FAFAFA",
  "&::-webkit-scrollbar": {
    width: "6px",
    height: "6px",
    borderRadius: "18px",
  },
  "&::-webkit-scrollbar-track": {
    borderRadius: "18px",
  },
  "&::-webkit-scrollbar-thumb": {
    backgroundColor: "#4E4E4E",
    borderRadius: "18px",
  },
};

export const Pharmacy: React.FC = () => {
  const [selected, setSelected] = React.useState<readonly number[]>([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [searchTerm, setSearchTerm] = React.useState<string>("");
  const shrink = searchTerm.length > 0;
  const navigate = useNavigate();

  function handleSelectAllClick(e: React.ChangeEvent<HTMLInputElement>) {
    setSelected(e.target.checked ? bills.map((_, index) => index) : []);
  }

  function handleClick(e: React.ChangeEvent<HTMLInputElement>, idx: number) {
    setSelected(
      selected.includes(idx)
        ? selected.filter((item) => item !== idx)
        : selected.concat(idx)
    );
  }

  return (
    <Box
    sx={{
      width: "100%",
      padding: "16px",
      background: "#FFFFFF",
      borderRadius: "16px",
    }}>
      <Box sx={{ height: 48, mb: 2, position: "relative" }}>
        <Typography
          variant="h3"
          sx={{
            float: "left",
            lineHeight: "48px",
            textTransform: "uppercase",
          }}
        >
          PHARMACY BILL LIST
        </Typography>
        <TextField
          label="Search"
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <Search sx={{ height: 20 }} />
              </InputAdornment>
            ),
          }}
          onChange={(e) => setSearchTerm(e.target.value)}
          InputLabelProps={{
            shrink,
            sx: {
              transform: shrink ? "" : "translate(45px, 12px) scale(1)",
            },
          }}
          variant="outlined"
          sx={{
            position: "absolute",
            right: 150,
            "& .MuiInputBase-root": {
              height: 48,
              width: 250,
              borderRadius: 3,
            },
          }}
        />
        <RoundedButton
          variant="contained"
          startIcon={<Add />}
          sx={{ position: "absolute", right: 0, height: 48, py: 0 }}
        >
          Generate Bill
        </RoundedButton>
      </Box>
      <TableContainer sx={tableContainerStyle}>
        <Table>
          <THead
            numSelected={selected.length}
            onSelectAllClick={handleSelectAllClick}
            rowCount={bills.length}
          />
          <TableBody>
            {bills
              .map(
                (
                  {
                    bill,
                    caseID,
                    date,
                    patient,
                    doctor,
                    discount,
                    amount,
                    paidAmount,
                    shippingMethod,
                    paymentMethod,
                  },
                  index
                ) => {
                  const [deleteDialogOpen, setDeleteDialogOpen] =
                    React.useState<boolean>(false);

                  function onDeleteDialogClose(
                    e: React.MouseEvent<HTMLButtonElement>
                  ) {
                    setDeleteDialogOpen(false);
                  }

                  function openDeleteDialog(
                    e: React.MouseEvent<HTMLButtonElement>
                  ) {
                    setDeleteDialogOpen(true);
                  }

                  return (
                    <TableRow
                      key={index}
                      sx={{ backgroundColor: colorPalette.white }}
                    >
                      <TableCell>
                        <Checkbox
                          checked={selected.includes(index)}
                          onChange={(e) => handleClick(e, index)}
                        />
                      </TableCell>
                      <TableCell>{bill}</TableCell>
                      <TableCell>{caseID}</TableCell>
                      <TableCell>
                        {format(date, "dd MMM yyyy")}
                        <Typography
                          variant="subtitle1"
                          sx={{ fontWeight: 500, color: "#4E4E4E" }}
                        >
                          {format(date, "hh:mm aa")}
                        </Typography>
                      </TableCell>
                      <TableCell>{patient}</TableCell>
                      <TableCell>{doctor}</TableCell>
                      <TableCell>{discount}</TableCell>
                      <TableCell>
                        {amount.toLocaleString(undefined, {
                          minimumFractionDigits: 2,
                        })}
                      </TableCell>
                      <TableCell>
                        {paidAmount.toLocaleString(undefined, {
                          minimumFractionDigits: 2,
                        })}
                      </TableCell>
                      <TableCell>
                        <Chip
                          label={shippingMethod}
                          variant="outlined"
                          sx={shippingMethodSx(shippingMethod)}
                        />
                      </TableCell>
                      <TableCell>
                        {paymentMethod && (
                          <Chip
                            label={paymentMethod}
                            variant="outlined"
                            sx={paymentMethodSx(paymentMethod)}
                          />
                        )}
                      </TableCell>
                      <TableCell>
                        <IconButton
                          onClick={() =>
                            navigate(`/admin/pharmacy/bill/${bill}`)
                          }
                        >
                          <EditOutlined />
                        </IconButton>
                        <IconButton onClick={openDeleteDialog}>
                          <Delete />
                        </IconButton>
                        <DeleteDialog
                          open={deleteDialogOpen}
                          onConfirm={onDeleteDialogClose}
                          onCancel={onDeleteDialogClose}
                          onClose={onDeleteDialogClose}
                        />
                      </TableCell>
                    </TableRow>
                  );
                }
              )
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        count={bills.length}
        component="div"
        rowsPerPageOptions={[5, 10, 25, 50]}
        page={page}
        rowsPerPage={rowsPerPage}
        onPageChange={(ev, newValue) => setPage(newValue)}
        onRowsPerPageChange={(e) => {
          setRowsPerPage(parseInt(e.target.value, 10));
          setPage(0);
        }}
        showFirstButton
        showLastButton
        sx={{ backgroundColor: "#FAFAFA" }}
      />
    </Box>
  );
};
