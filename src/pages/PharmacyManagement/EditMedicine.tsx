import React from "react";
import {
  Toolbar,
  Box,
  Typography,
  Button,
  Grid,
  FormControl,
  Select,
  MenuItem,
  SelectChangeEvent,
  Dialog,
  Slider,
  InputLabel,
} from "@mui/material";
import ImageUploader, { ImageListType } from "react-images-uploading";
import { StyledInput } from "./style";

import back_icon from "../../assets/icon/arrow_back.svg";
import drug from "../../assets/image/drug.svg";
import upload_icon from "../../assets/icon/upload_icon.svg";
import size_up from "../../assets/icon/size_up_icon.svg";
import size_down from "../../assets/icon/size_down_icon.svg";
import { useSelector } from "react-redux";
import {
  Medicine,
  selectMedicineById,
  updateMedicine,
} from "../../store/reducers/medicine/medicineSlice";
import { useNavigate } from "react-router-dom";
import { useAppDispatch } from "../../store/Store";
interface DialogProps {
  open: boolean;
  onClose: () => void;
  onUpload: (image: string) => void;
}
interface PropsHeader {
  name: string;
  id: string;
  updateData: object;
}

const RenderPageHeader = (props: PropsHeader) => {
  const { name, id, updateData } = props;
  const accessToken = localStorage.getItem("access_token") as string;
  const navigate = useNavigate();
  const dispatch = useAppDispatch();

  const handleUpdate = () => {
    dispatch(updateMedicine(id, updateData, accessToken));
    navigate("/admin/medicine");
  };
  return (
    <Toolbar sx={{ padding: "0 !important" }}>
      <Box
        sx={{
          flexGrow: 1,
          alignItems: "center",
          display: "flex",
          justifyContent: "space-between",
          marginBottom: "16px",
        }}
      >
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "flex-start",
            gap: "16px",
          }}
        >
          <div
            style={{
              background: "#FFFFFF",
              width: "40px",
              height: "40px",
              borderRadius: "50%",
              border: "1px solid #F4F4F4",
              padding: "10px 12px",
              cursor: "pointer",
            }}
            onClick={() => history.back()}
          >
            <img src={back_icon} width={15} height={12} />
          </div>
          <Typography
            variant="h5"
            sx={{ fontSize: "24px", fontWeight: "600", color: "#131626" }}
          >
            {name}
          </Typography>
        </div>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "flex-end",
            gap: "20px",
          }}
        >
          <Button
            sx={{
              background: "#FFFFFF",
              width: "85px",
              height: "40px",
              border: "1px solid #F4F4F4",
              borderRadius: "99px",
            }}
            onClick={() => navigate("/admin/medicine")}
          >
            Cancel
          </Button>
          <Button
            variant="contained"
            sx={{
              width: "85px",
              height: "40px",
              borderRadius: "99px",
              boxShadow: "0px 5px 10px rgba(74, 92, 208, 0.2)",
            }}
            onClick={handleUpdate}
          >
            Save
          </Button>
        </div>
      </Box>
    </Toolbar>
  );
};

const RenderUploadImageDialog = (Props: DialogProps) => {
  const { onClose, onUpload, open } = Props;
  const [images, setImages] = React.useState<ImageListType[]>([]);

  const onChange = (imageList: ImageListType) => {
    setImages(imageList as never[]);
  };

  return (
    <Dialog onClose={onClose} open={open}>
      <Box
        sx={{
          width: "454px",
          height: "fit-content",
          display: "flex",
          padding: "24px 20px",
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "column",
        }}
      >
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            width: "100%",
            marginBottom: "25px",
            overflow: "hidden",
          }}
        >
          <Typography variant="h6" component="div" sx={{ marginLeft: "14px" }}>
            Upload medicine image
          </Typography>
          <Button
            onClick={() => {
              onClose();
              setImages([]);
            }}
          >
            Cancel
          </Button>
        </div>
        <div
          style={{
            width: "100%",
          }}
        >
          <ImageUploader value={images} onChange={onChange}>
            {({ imageList, onImageUpload, dragProps }) => (
              <div>
                {imageList.length === 0 && (
                  <Button
                    sx={{
                      width: "400px",
                      height: "54px",
                      border: "1px solid #F4F4F4",
                      borderRadius: "99px",
                      marginBottom: "40px",
                    }}
                    onClick={onImageUpload}
                    {...dragProps}
                  >
                    <img
                      src={upload_icon}
                      width={17}
                      height={17}
                      style={{ marginRight: "14px" }}
                    />
                    Upload medicine image
                  </Button>
                )}

                {imageList.map((image, index) => (
                  <div
                    key={index}
                    style={{
                      marginBottom: "40px",
                    }}
                  >
                    <img src={image.dataURL} width="100%" height="100%" />
                  </div>
                ))}
              </div>
            )}
          </ImageUploader>
        </div>
        {images.length !== 0 && (
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
              width: "100%",
              gap: "20px",
            }}
          >
            <img src={size_down} width={20} height={20} />
            <Slider
              defaultValue={0}
              aria-label="Default"
              valueLabelDisplay="auto"
            />
            <img src={size_up} width={20} height={20} />
          </div>
        )}
        <Button
          variant="contained"
          sx={{
            width: "100%",
            borderRadius: "99px",
            boxShadow: "0px 5px 10px rgba(74, 92, 208, 0.2)",
            marginTop: "50px",
          }}
        >
          Upload
        </Button>
      </Box>
    </Dialog>
  );
};

export const EditMedicine: React.FC = () => {
  const [openDialog, setOpenDialog] = React.useState(false);
  const [img, setImg] = React.useState(drug);

  const data = useSelector(selectMedicineById);
  const [profile, setProfile] = React.useState(data);

  const [name, setName] = React.useState<string>("");
  const [category, setCategory] = React.useState<string>("Select");
  const [unit, setUnit] = React.useState<number>();
  const [packing, setPacking] = React.useState<number>();

  React.useEffect(() => {
    setProfile({ ...data } as Medicine);
    setName(profile.code?.coding?.[0].display);
    setCategory(profile.form?.coding?.[0]?.display);
    setUnit(profile.amount?.numerator?.value);
    setPacking(profile.amount?.denominator?.value);
  }, [data]);

  const updateData = {
    code: {
      coding: [{ display: name }],
    },
    form: {
      coding: [{ display: category }],
    },
    amount: {
      numerator: { value: unit },
      denominator: { value: packing },
    },
  };

  const closeDialog = () => {
    setOpenDialog(false);
  };

  const uploadImage = (image: string) => {
    setImg(image);
    setOpenDialog(false);
  };

  return (
    <Box>
      <RenderPageHeader
        name={profile.code?.coding?.[0].display}
        id={profile.id}
        updateData={updateData}
      />
      <Grid container spacing={2}>
        <Grid item xs={6}>
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "flex-start",
              width: "100%",
              background: "#FFFFFF",
              borderRadius: "16px",
              gap: "26px",
              flexDirection: "column",
              padding: "16px",
              minHeight: "600px",
            }}
          >
            <Button
              fullWidth
              sx={{
                height: "54px",
                border: "1px solid #F4F4F4",
                borderRadius: "99px",
              }}
              onClick={() => {
                setOpenDialog(true);
              }}
            >
              <img
                src={upload_icon}
                width={17}
                height={17}
                style={{ marginRight: "14px" }}
              />
              Upload medicine image
            </Button>
            <StyledInput
              placeholder="Enter Medicine Name here..."
              label="Medicine Name"
              variant="outlined"
              required
              InputLabelProps={{ shrink: true }}
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
            <StyledInput
              placeholder="Enter Medicine Group here..."
              label="Medicine Group"
              variant="outlined"
              required
              InputLabelProps={{ shrink: true }}
              defaultValue="AMD s"
            />
            <FormControl sx={{ width: "100%" }}>
              <InputLabel id="medicine-category" required shrink>
                Medicine Category
              </InputLabel>
              <Select
                required
                value={category}
                onChange={(event: SelectChangeEvent) =>
                  setCategory(event.target.value as unknown as string)
                }
                labelId="medicine-category"
                label="Medicine Category"
                sx={{
                  "& .css-1d3z3hw-MuiOutlinedInput-notchedOutline": {
                    border: "1px solid #EAEAEA",
                    borderRadius: "12px",
                  },
                  "& .MuiSelect-select": {
                    padding: "11px 16px",
                  },
                }}
              >
                <MenuItem value="Select" disabled>
                  Select
                </MenuItem>
                <MenuItem value="Capsule">Capsule</MenuItem>
                <MenuItem value="Injection">Injection</MenuItem>
                <MenuItem value="Tablet">Tablet</MenuItem>
              </Select>
            </FormControl>
          </div>
        </Grid>

        <Grid item xs={6}>
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "flex-start",
              width: "100%",
              background: "#FFFFFF",
              borderRadius: "16px",
              gap: "26px",
              flexDirection: "column",
              padding: "16px",
              minHeight: "600px",
            }}
          >
            <StyledInput
              placeholder="Enter Medicine Composition here..."
              label="Medicine Composition"
              variant="outlined"
              required
              InputLabelProps={{ shrink: true }}
            />
            <StyledInput
              placeholder="Enter Unit here..."
              label="Unit"
              variant="outlined"
              required
              InputLabelProps={{ shrink: true }}
              value={unit}
              onChange={(e) => setUnit(Number(e.target.value))}
            />
            <StyledInput
              placeholder="Enter Min Level here..."
              label="Min Level"
              variant="outlined"
              InputLabelProps={{ shrink: true }}
              defaultValue={25}
            />
            <StyledInput
              placeholder="Enter Re-Order Level here..."
              label="Re-Order Level"
              variant="outlined"
              InputLabelProps={{ shrink: true }}
              defaultValue={200}
            />
            <StyledInput
              placeholder="Enter Tax here..."
              label="Tax"
              variant="outlined"
              InputLabelProps={{ shrink: true }}
              defaultValue={10}
            />
            <StyledInput
              placeholder="Enter Unit/Packing here..."
              label="Unit/Packing"
              variant="outlined"
              required
              InputLabelProps={{ shrink: true }}
              value={packing}
              onChange={(e) => setPacking(Number(e.target.value))}
            />
            <StyledInput
              placeholder="Enter VAT A/C here..."
              label="VAT A/C"
              variant="outlined"
              InputLabelProps={{ shrink: true }}
              defaultValue={10}
            />
            <StyledInput
              placeholder="Enter..."
              label="Note"
              variant="outlined"
              InputLabelProps={{ shrink: true }}
              multiline
            />
          </div>
        </Grid>
      </Grid>
      <RenderUploadImageDialog
        onClose={closeDialog}
        onUpload={uploadImage}
        open={openDialog}
      />
    </Box>
  );
};
