import {
  Box,
  IconButton,
  styled,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import { useNavigate, useParams } from "react-router-dom";
import {
  DeleteOutlined,
  EditOutlined,
  EventOutlined,
  KeyboardBackspace,
  LocalPrintshopOutlined,
} from "@mui/icons-material";
import { RoundedButton } from "../../components/RoundedButton";
import { colorPalette } from "../../theme";

const medicine = [
  {
    category: "Capsule",
    name: "WORMSTOP",
    batch: 3421,
    unit: 20,
    expiryDate: "May 2023",
    quantity: 2,
    tax: "20 (10%)",
    amount: 200,
  },
  {
    category: "Injection",
    name: "BICASOL",
    batch: 3421,
    unit: 20,
    expiryDate: "May 2023",
    quantity: 2,
    tax: "20 (10%)",
    amount: 200,
  },
];

const Highlight = styled("span")({
  color: colorPalette.primary,
  marginRight: "24px",
});
const TableHeader: React.FC<{ columns: string[] }> = ({ columns }) => {
  return (
    <TableHead sx={{ backgroundColor: "#FAFAFA" }}>
      <TableRow>
        {columns.map((c) => (
          <TableCell key={c} sx={{ fontWeight: 600 }}>
            {c}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
};

const DefinitionList: React.FC<{ definitions: object }> = ({ definitions }) => {
  return (
    <Box
      sx={{
        display: "flex",
        flexFlow: "column",
        gap: "16px",
        flexGrow: 1,
        backgroundColor: colorPalette.white,
        borderRadius: "16px",
        p: "16px",
      }}
    >
      {Object.entries(definitions).map(([key, value]) => (
        <Box sx={{ display: "flex", justifyContent: "space-between" }}>
          <Typography variant="h6" sx={{ color: colorPalette.dark }}>
            {key}
          </Typography>
          <Typography variant="body2" sx={{ color: colorPalette.grey }}>
            {value}
          </Typography>
        </Box>
      ))}
    </Box>
  );
};

export const BillDetails = () => {
  const { id } = useParams();
  const navigate = useNavigate();

  return (
    <Box sx={{ backgroundColor: "#FAFAFA", minHeight: "calc(100vh - 100px)" }}>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            gap: "16px",
          }}
        >
          <IconButton
            color="primary"
            sx={{ backgroundColor: colorPalette.white }}
            onClick={() => history.back()}
          >
            <KeyboardBackspace />
          </IconButton>
          <Typography component="span" variant="h2">
            {id}
          </Typography>
        </Box>

        <Box>
          <IconButton onClick={() => window.print()}>
            <LocalPrintshopOutlined />
          </IconButton>
          <IconButton>
            <DeleteOutlined />
          </IconButton>
          <RoundedButton
            variant="outlined"
            startIcon={<EditOutlined />}
            sx={{ backgroundColor: colorPalette.white, height: 40 }}
            onClick={() => navigate("edit")}
          >
            Edit
          </RoundedButton>
        </Box>
      </Box>
      <Box
        sx={{
          backgroundColor: colorPalette.white,
          borderRadius: "16px",
          p: "16px",
          my: "16px",
        }}
      >
        <Box
          sx={{ display: "flex", justifyContent: "space-between", mb: "16px" }}
        >
          <span style={{ fontSize: "16px", fontWeight: 600 }}>
            BILL NO: <Highlight>{id}</Highlight>
            CASE ID: <Highlight>1641</Highlight>
            COLLECTED BY: <Highlight>SUPPER ADMIN</Highlight>
          </span>
          <span
            style={{
              color: colorPalette.lightGrey,
              fontSize: "14px",
              fontWeight: 500,
            }}
          >
            <EventOutlined
              sx={{ height: 18, width: 18, verticalAlign: "text-top" }}
            />
            Date:&nbsp;
            <span style={{ color: colorPalette.grey }}>
              04.07.2022 01:10 PM
            </span>
          </span>
        </Box>
        <Table>
          <TableHeader
            columns={[
              "Medicine Category",
              "Medicine Name",
              "Batch No",
              "Unit",
              "Expiry Date",
              "Quantity",
              "Tax ($)",
              "Amount ($)",
            ]}
          />
          <TableBody>
            {medicine.map(
              (
                {
                  category,
                  name,
                  batch,
                  unit,
                  expiryDate,
                  quantity,
                  tax,
                  amount,
                },
                index
              ) => (
                <TableRow key={index}>
                  <TableCell>{category}</TableCell>
                  <TableCell>{name}</TableCell>
                  <TableCell>{batch}</TableCell>
                  <TableCell>{unit}</TableCell>
                  <TableCell>{expiryDate}</TableCell>
                  <TableCell>{quantity}</TableCell>
                  <TableCell>{tax}</TableCell>
                  <TableCell>{amount}</TableCell>
                </TableRow>
              )
            )}
          </TableBody>
        </Table>
      </Box>

      <Box
        sx={{ display: "flex", gap: "20px", justifyContent: "space-between" }}
      >
        <DefinitionList
          definitions={{
            Name: "Olivier Thomas (1)",
            Phone: "7896541230",
            Doctor: "Amit Singh (9009)",
            Prescription: "OPDP114",
          }}
        />
        <DefinitionList
          definitions={{
            "Total ($)": "300.00",
            Discount: "30.00",
            "Tax ($)": "25.00",
            "Net Amount ($)": "295.00",
            "Paid Amount ($)": "250.00",
            "Refund Amount": "0.00",
            "Due Amount ($)": "45.00",
          }}
        />
      </Box>
    </Box>
  );
};
