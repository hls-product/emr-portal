
import { styled as muiStyled, TextField } from '@mui/material';

export const StyledInput = muiStyled(TextField)(({}) => ({
    width: '100%',
    marginTop: '2px',
    '& .MuiInputBase-root': {
      borderRadius: '12px',
    },
    '& .MuiInputLabel-asterisk': {
      color: '#E65050',
    },
    '& .MuiInputLabel-root': {
      fontWeight: '500',
      fontSize: '16px',
      color: '#131626',
    },
    '& .MuiOutlinedInput-notchedOutline': {
      border: '1px solid #EAEAEA',
    },
    '& .MuiOutlinedInput-input': {
      padding: '9.5px 14px',
    },
}));