import React from "react";
import {
  Grid,
  Button,
  Typography,
  Table,
  TableContainer,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Toolbar,
  Box,
  IconButton,
} from "@mui/material";

import { StyledInput } from "../../layout/Header/style";

import back_icon from "../../assets/icon/arrow_back.svg";
import bin from "../../assets/icon/bin_icon.svg";
import upload_icon from "../../assets/icon/upload_icon.svg";
import edit_icon from "../../assets/icon/edit_icon_theme_color.svg";
import secondarySearchIcon from "../../assets/adminHeader/secondarySearchIcon.svg";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import {
  Medicine,
  selectMedicineById,
} from "../../store/reducers/medicine/medicineSlice";

interface tableData {
  inwardDate: string;
  no: number;
  expireDate: string;
  packingQty: number;
  purchaseRate: number;
  amount: number;
  qty: number;
  mrp: number;
  salePrice: number;
}

const createRows = (
  inwardDate: string,
  no: number,
  expireDate: string,
  packingQty: number,
  purchaseRate: number,
  amount: number,
  qty: number,
  mrp: number,
  salePrice: number
): tableData => {
  return {
    inwardDate,
    no,
    expireDate,
    packingQty,
    purchaseRate,
    amount,
    qty,
    mrp,
    salePrice,
  };
};

const rows = [
  createRows("02.07.2022", 2104, "Dec 2022", 500, 100, 10000, 1000, 100, 100),
  createRows("02.07.2022", 2104, "Dec 2022", 500, 100, 10000, 1000, 100, 100),
  createRows("02.07.2022", 2104, "Dec 2022", 500, 100, 10000, 1000, 100, 100),
];

const tableHead: string[] = [
  "Inward Date",
  "Batch No",
  "Expiry Date",
  "Packing Qty ($)",
  "Purchase Rate ($)",
  "Amount ($)",
  "Quantity",
  "MRP ($)",
  "Sale Price ($)",
  "Action",
];

interface PropsHeader {
  name: string;
}

interface PropsField {
  label: string;
  value: string | number;
}

const RenderPageHeader = (props: PropsHeader) => {
  const { name } = props;
  const navigate = useNavigate();
  const handleEditMedicine = () => {
    navigate("edit");
  };
  return (
    <Toolbar sx={{ padding: "0 !important" }}>
      <Box
        sx={{
          flexGrow: 1,
          alignItems: "center",
          display: "flex",
          justifyContent: "space-between",
          marginBottom: "16px",
        }}
      >
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "flex-start",
            gap: "16px",
          }}
        >
          <div
            style={{
              background: "#FFFFFF",
              width: "40px",
              height: "40px",
              borderRadius: "50%",
              border: "1px solid #F4F4F4",
              padding: "10px 12px",
              cursor: "pointer",
            }}
            onClick={() => history.back()}
          >
            <img src={back_icon} width={15} height={12} />
          </div>
          <Typography
            variant="h5"
            component="div"
            sx={{ fontWeight: 600, fontSize: "24px", color: "#131626" }}
          >
            {name}
          </Typography>
        </div>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "flex-end",
            gap: "20px",
          }}
        >
          <IconButton>
            <img src={bin} width={20} height={20} />
          </IconButton>
          <Button
            sx={{
              background: "#FFFFFF",
              width: "97px",
              height: "40px",
              border: "1px solid #F4F4F4",
              borderRadius: "99px",
            }}
            onClick={handleEditMedicine}
          >
            <img
              src={edit_icon}
              width={16}
              height={16}
              style={{
                marginRight: "10px",
              }}
            />
            Edit
          </Button>
        </div>
      </Box>
    </Toolbar>
  );
};

const RenderField = (props: PropsField) => {
  const { label, value } = props;
  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "space-between",
        width: "100%",
      }}
      key={label}
    >
      <Typography
        sx={{
          fontSize: "14px",
          fontWeight: "600",
          color: "#131626",
        }}
      >
        {label}
      </Typography>
      <Typography
        sx={{
          fontSize: "14px",
          fontWeight: "500",
          color: "#4E4E4E",
        }}
      >
        {value}
      </Typography>
    </div>
  );
};

const RenderTableHead = () => {
  return (
    <Toolbar
      sx={{
        background: "#FFFFFF",
        padding: "0 !important",
      }}
    >
      <Box
        sx={{
          flexGrow: 1,
          alignItems: "center",
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <Typography
          variant="h6"
          component="div"
          sx={{
            fontSize: "16px",
            fontWeight: "600",
            color: "#131626",
            textTransform: "uppercase",
          }}
        >
          Stock list
        </Typography>
        <div
          style={{
            display: "flex",
            width: "22%",
            border: "1px solid #EAEAEA",
            borderRadius: "12px",
            marginBottom: "16px",
          }}
        >
          <img
            src={secondarySearchIcon}
            alt="Search"
            width={20}
            height={20}
            style={{ margin: "14px 0 14px 14px" }}
          />
          <StyledInput
            sx={{ m: 1, width: "fit-content" }}
            placeholder="Search..."
          />
        </div>
      </Box>
    </Toolbar>
  );
};

export const MedicineStockProfile: React.FC = () => {
  const data = useSelector(selectMedicineById);
  const [profile, setProfile] = React.useState(data);

  React.useEffect(() => {
    setProfile({ ...data } as Medicine);
  }, [data]);

  return (
    <Box>
      <RenderPageHeader name={profile.code?.coding?.[0].display} />
      <Grid container spacing={2}>
        <Grid item xs={6}>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "flex-start",
              background: "#FFFFFF",
              borderRadius: "16px",
              gap: "16px",
              padding: "16px",
              minHeight: "275px",
            }}
          >
            <Button
              fullWidth
              sx={{
                height: "54px",
                border: "1px solid #F4F4F4",
                borderRadius: "99px",
              }}
            >
              <img
                src={upload_icon}
                width={17}
                height={17}
                style={{ marginRight: "14px" }}
              />
              Upload medicine image
            </Button>
            <RenderField
              label="Medicine Name"
              value={profile.code?.coding?.[0].display || "N/A"}
            />
            <RenderField
              label="Medicine Company"
              value={profile.manufacturer?.reference || "N/A"}
            />
            <RenderField label="Medicine Group" value="AMD s" />
            <RenderField
              label="Medicine Category"
              value={profile.form?.coding?.[0]?.display || "N/A"}
            />
          </div>
        </Grid>

        <Grid item xs={6}>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "flex-start",
              background: "#FFFFFF",
              borderRadius: "16px",
              gap: "16px",
              padding: "16px",
              minHeight: "275px",
            }}
          >
            <RenderField
              label="Medicine Composition"
              value={profile.code?.coding?.[0].display || "N/A"}
            />
            <RenderField
              label="Unit"
              value={profile.amount?.numerator?.value}
            />
            <RenderField label="Min Level" value={25} />
            <RenderField label="Render-Order Level" value={200} />
            <RenderField label="VAT (%)" value={10} />
            <RenderField
              label="Unit/Packing"
              value={profile.amount?.denominator?.value}
            />
            <RenderField label="VAT A/C" value={10} />
          </div>
        </Grid>

        <Box
          sx={{
            width: "100%",
            padding: "16px",
            background: "#FFFFFF",
            borderRadius: "16px",
            margin: "16px",
          }}
        >
          <RenderTableHead />
          <TableContainer>
            <Table>
              <TableHead
                sx={{
                  background: "#FAFAFA",
                }}
              >
                <TableRow>
                  {tableHead.map((element, index) => (
                    <TableCell
                      key={index}
                      align={`${element === "Action" ? "center" : "left"}`}
                      sx={{
                        borderBottom: "1px solid #EAEAEA",
                        color: "#131626",
                        fontSize: "14px",
                        fontWeight: "600",
                      }}
                    >
                      {element}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow>
                  <TableCell
                    align="left"
                    sx={{
                      borderBottom: "1px solid #EAEAEA",
                    }}
                  >
                    2014-11-11
                  </TableCell>
                  <TableCell
                    align="left"
                    sx={{
                      borderBottom: "1px solid #EAEAEA",
                    }}
                  >
                    {profile.batch?.lotNumber || "N/A"}
                  </TableCell>
                  <TableCell
                    align="left"
                    sx={{
                      borderBottom: "1px solid #EAEAEA",
                    }}
                  >
                    {profile.batch?.expirationDate.slice(0, 10) || "N/A"}
                  </TableCell>
                  <TableCell
                    align="left"
                    sx={{
                      borderBottom: "1px solid #EAEAEA",
                    }}
                  >
                    500
                  </TableCell>
                  <TableCell
                    align="left"
                    sx={{
                      borderBottom: "1px solid #EAEAEA",
                    }}
                  >
                    100
                  </TableCell>
                  <TableCell
                    align="left"
                    sx={{
                      borderBottom: "1px solid #EAEAEA",
                    }}
                  >
                    10000
                  </TableCell>
                  <TableCell
                    align="left"
                    sx={{
                      borderBottom: "1px solid #EAEAEA",
                    }}
                  >
                    1000
                  </TableCell>
                  <TableCell
                    align="left"
                    sx={{
                      borderBottom: "1px solid #EAEAEA",
                    }}
                  >
                    100
                  </TableCell>
                  <TableCell
                    align="left"
                    sx={{
                      borderBottom: "1px solid #EAEAEA",
                    }}
                  >
                    100
                  </TableCell>
                  <TableCell
                    align="center"
                    sx={{
                      borderBottom: "1px solid #EAEAEA",
                    }}
                  >
                    <IconButton>
                      <img src={bin} width={20} height={20} />
                    </IconButton>
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
        </Box>
      </Grid>
    </Box>
  );
};
