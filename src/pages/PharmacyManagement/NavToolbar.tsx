import React from "react";
import { Box, Button, ButtonProps, styled, Toolbar } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { colorPalette } from "../../theme";

const NavButton = styled(Button)<ButtonProps>({
  borderRadius: "10px",
  height: 40,
});

const tabs = [
  {
    name: "pharmacy",
    label: "Pharmacy Bill",
    link: "/admin/pharmacy",
  },
  {
    name: "medicine",
    label: "Medicines Stock",
    link: "/admin/medicine",
  },
];

export const NavToolbar: React.FC<{ activeTab: string }> = ({ activeTab }) => {
  const navigate = useNavigate();
  return (
    <Toolbar disableGutters>
      <Box
        sx={{
          display: "flex",
          gap: "16px",
          backgroundColor: colorPalette.white,
          width: 1,
          borderRadius: "16px",
          p: "16px",
        }}
      >
        {tabs.map(({ name, label, link }) => (
          <NavButton
            key={name}
            variant={name === activeTab ? "contained" : "text"}
            onClick={() => navigate(link)}
          >
            {label}
          </NavButton>
        ))}
      </Box>
    </Toolbar>
  );
};
