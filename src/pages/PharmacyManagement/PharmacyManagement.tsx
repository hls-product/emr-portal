import { Box, Typography } from "@mui/material";
import React from "react";
import { Tab } from "../../components/tab";
import { MedicinesStock } from "./MedicinesStock";
import { Pharmacy } from "./Pharmacy";

const tabList =[
    {
        element: <Pharmacy/>,
        title: "Pharmacy Bill"
    },
    {
        element: <MedicinesStock/>,
        title: "Medicine Stock"
    }
]

export const PharmacyManagement: React.FC = () => {
    return(
        <Box>
        <Typography
          component="div"
          sx={{
            fontSize: "24px",
            fontWeight: "600",
          }}
          children="Pharmacy Management"
        />
        <Tab children={tabList} defaultValue={0} />
      </Box>
    )
}