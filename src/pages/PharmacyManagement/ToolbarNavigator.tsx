import {navigationLink} from '../HumanResources/ToolbarNavigator'

export const listNavigationLink: navigationLink[] = [
    {
        id: 0,
        label: 'Pharmacy Bill',
        url: '',
    },
    {
        id: 1,
        label: 'Medicines Stock',
        url: '/medicine',
    },
]


