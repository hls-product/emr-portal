import React from "react";
import {
  Box,
  Typography,
  Button,
  Toolbar,
  IconButton,
  useTheme,
  TableContainer,
  Table,
  TableRow,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  Checkbox,
  TableFooter,
} from "@mui/material";
import FirstPageIcon from "@mui/icons-material/FirstPage";
import KeyboardArrowLeft from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRight from "@mui/icons-material/KeyboardArrowRight";
import LastPageIcon from "@mui/icons-material/LastPage";
import { useNavigate } from "react-router-dom";
import { StyledInput } from "../../layout/Header/style";

import secondarySearchIcon from "../../assets/adminHeader/secondarySearchIcon.svg";
import downloadIcon from "../../assets/icon/download_icon_theme_color.svg";
import addIcon from "../../assets/icon/add_icon.svg";
import { NavToolbar } from "./NavToolbar";
import { Delete, EditOutlined } from "@mui/icons-material";
import { useAppDispatch } from "../../store/Store";
import {
  getMedicine,
  getMedicineById,
  Medicine,
  selectLoading,
  selectMedicine,
  selectPaging,
} from "../../store/reducers/medicine/medicineSlice";
import { useSelector } from "react-redux";
import Loading from "../../components/Loading";

interface TablePaginationActionsProps {
  count: number;
  page: number;
  rowsPerPage: number;
  onPageChange: (
    event: React.MouseEvent<HTMLButtonElement>,
    newPage: number
  ) => void;
}

const headCells: string[] = [
  "Medicine Name",
  "Medicine Company",
  "Medicine Composition",
  "Medicine Category",
  "Medicine Group",
  "Unit",
  "Available Qty",
  "Action",
];

function TablePaginationActions(props: TablePaginationActionsProps) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ flexShrink: 0, ml: 2.5 }}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowRight />
        ) : (
          <KeyboardArrowLeft />
        )}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowLeft />
        ) : (
          <KeyboardArrowRight />
        )}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
}

const RenderTableHead = () => {
  const navigate = useNavigate();
  return (
    <Toolbar
      sx={{
        background: "#FFFFFF",
        padding: "0 !important",
      }}
    >
      <Box
        sx={{
          flexGrow: 1,
          alignItems: "center",
          display: "flex",
          justifyContent: "space-between",
          marginBottom: "16px",
        }}
      >
        <Typography
          variant="h3"
          sx={{ float: "left", lineHeight: "48px", textTransform: "uppercase" }}
        >
          Medicines Stock list
        </Typography>
        <div
          style={{
            display: "flex",
            width: "fit-content",
            gap: "16px",
          }}
        >
          <div
            style={{
              display: "flex",
              width: "fit-content",
              border: "1px solid #EAEAEA",
              borderRadius: "12px",
            }}
          >
            <img
              src={secondarySearchIcon}
              alt="Search"
              width={20}
              height={20}
              style={{ margin: "14px 0 14px 14px" }}
            />
            <StyledInput
              sx={{ m: 1, width: "fit-content" }}
              placeholder="Search..."
            />
          </div>
          <Button
            sx={{
              borderRadius: "99px",
              border: "1px solid #F4F4F4",
              width: "182px",
              height: "48px",
            }}
          >
            <img
              src={downloadIcon}
              width={15}
              height={15}
              style={{ marginRight: "10px" }}
            />
            Import Medicines
          </Button>
          <Button
            variant="contained"
            sx={{
              borderRadius: "99px",
              width: "182px",
              height: "48px",
            }}
            onClick={() => navigate("./add-medicine")}
          >
            <img
              src={addIcon}
              width={15}
              height={15}
              style={{ marginRight: "10px" }}
            />
            Add Medicine
          </Button>
        </div>
      </Box>
    </Toolbar>
  );
};

export const MedicinesStock: React.FC = () => {
  const data = useSelector(selectMedicine);
  const loading = useSelector(selectLoading);
  const paging = useSelector(selectPaging);

  const [rowsPerPage, setRowsPerPage] = React.useState<number>(5);
  const [page, setPage] = React.useState<number>(0);
  const [selected, setSelected] = React.useState<readonly string[]>([]);
  const [rowsCount, setRowsCount] = React.useState(paging.total);
  const [medicine, setMedicine] = React.useState<Medicine[]>(data);

  const accessToken = localStorage.getItem("access_token") as string;

  const dispatch = useAppDispatch();

  React.useEffect(() => {
    dispatch(getMedicine(page + 1, rowsPerPage, accessToken));
  }, [page, rowsPerPage, accessToken]);

  React.useEffect(() => {
    setMedicine([...data] as Medicine[]);
    setRowsCount(paging.total);
  }, [data]);

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked) {
      const newSelected = medicine.map((n) => n.id);
      setSelected(newSelected);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event: React.MouseEvent<unknown>, name: string) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected: readonly string[] = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }

    setSelected(newSelected);
  };

  const isSelected = (name: string) => selected.indexOf(name) !== -1;

  const navigate = useNavigate();

  const handleViewProfile = (id: string) => {
    dispatch(getMedicineById(id, accessToken));
    navigate("profile");
  };

  return (
    <Box
      sx={{
        width: "100%",
        padding: "16px",
        background: "#FFFFFF",
        borderRadius: "16px",
      }}
    >
      <RenderTableHead />
      {loading && <Loading />}

      {!loading && (
        <TableContainer>
          <Table>
            <TableHead
              sx={{
                background: "#FAFAFA",
              }}
            >
              <TableRow>
                <TableCell align="left">
                  <Checkbox
                    color="primary"
                    indeterminate={
                      selected.length > 0 && selected.length < medicine.length
                    }
                    checked={
                      medicine.length > 0 && selected.length === medicine.length
                    }
                    onChange={handleSelectAllClick}
                    inputProps={{
                      "aria-label": "select all desserts",
                    }}
                  />
                </TableCell>
                {headCells.map((cell) => (
                  <TableCell
                    key={cell}
                    align={`${
                      cell === "#"
                        ? "center"
                        : cell === "Action"
                        ? "center"
                        : "left"
                    }`}
                    sx={{
                      borderBottom: "1px solid #EAEAEA",
                      color: "#131626",
                      fontSize: "14px",
                      fontWeight: "600",
                    }}
                  >
                    {cell}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {medicine.map((row) => {
                const isItemSelected = isSelected(
                  row.code?.coding?.[0].display
                );
                const labelId = `enhanced-table-checkbox-${row.id}`;
                return (
                  <TableRow
                    onClick={(event) =>
                      handleClick(event, row.code?.coding?.[0].display)
                    }
                    key={row.id}
                    selected={isItemSelected}
                    role="checkbox"
                    aria-checked={isItemSelected}
                  >
                    <TableCell
                      align="left"
                      sx={{
                        borderBottom: "1px solid #EAEAEA",
                      }}
                    >
                      <Checkbox
                        color="primary"
                        checked={isItemSelected}
                        inputProps={{
                          "aria-labelledby": labelId,
                        }}
                      />
                    </TableCell>
                    <TableCell
                      align="left"
                      sx={{
                        borderBottom: "1px solid #EAEAEA",
                      }}
                      id={labelId}
                    >
                      {row.code?.coding?.[0].display || "N/A"}
                    </TableCell>
                    <TableCell
                      sx={{
                        borderBottom: "1px solid #EAEAEA",
                      }}
                      align="left"
                    >
                      {row.manufacturer?.reference || "N/A"}
                    </TableCell>
                    <TableCell
                      align="left"
                      sx={{
                        borderBottom: "1px solid #EAEAEA",
                      }}
                    >
                      {row.code?.coding?.[0].display || "N/A"}
                    </TableCell>
                    <TableCell
                      align="left"
                      sx={{
                        borderBottom: "1px solid #EAEAEA",
                      }}
                    >
                      {row.form?.coding?.[0].display || "N/A"}
                    </TableCell>
                    <TableCell
                      align="left"
                      sx={{
                        borderBottom: "1px solid #EAEAEA",
                      }}
                    >
                      30.00 (10.00%)
                    </TableCell>
                    <TableCell
                      align="left"
                      sx={{
                        borderBottom: "1px solid #EAEAEA",
                      }}
                    >
                      Varicella
                    </TableCell>
                    <TableCell
                      align="left"
                      sx={{
                        borderBottom: "1px solid #EAEAEA",
                      }}
                    >
                      <div
                        style={{
                          display: "flex",
                          alignItems: "flex-start",
                          justifyContent: "center",
                          flexDirection: "column",
                        }}
                      >
                        {row.amount?.denominator.value || 0}
                        <Typography
                          variant="body2"
                          component="div"
                          sx={{
                            fontSize: "12px",
                            fontWeight: "500",
                            color: `${
                              row.amount?.denominator.value === 0 ||
                              row.amount === null
                                ? "#E03535"
                                : row.amount?.denominator.value < 50
                                ? "#FF7300"
                                : "#1685DA"
                            }`,
                          }}
                        >
                          {row.amount?.denominator.value === 0 ||
                          row.amount === null
                            ? "Out of stock"
                            : row.amount?.denominator.value < 50
                            ? "Low stock"
                            : "Reorder"}
                        </Typography>
                      </div>
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        borderBottom: "1px solid #EAEAEA",
                      }}
                    >
                      <div
                        style={{
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                          gap: "5px",
                        }}
                      >
                        <IconButton onClick={() => handleViewProfile(row.id)}>
                          <EditOutlined />
                        </IconButton>
                        <IconButton>
                          <Delete />
                        </IconButton>
                      </div>
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
            <TableFooter>
              <TableRow>
                <TablePagination
                  sx={{ background: "#FAFAFA" }}
                  rowsPerPageOptions={[5, 10, 25]}
                  count={rowsCount}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  onPageChange={handleChangePage}
                  onRowsPerPageChange={handleChangeRowsPerPage}
                  ActionsComponent={TablePaginationActions}
                />
              </TableRow>
            </TableFooter>
          </Table>
        </TableContainer>
      )}
    </Box>
  );
};
