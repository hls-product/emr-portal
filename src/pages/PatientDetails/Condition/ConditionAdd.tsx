import AddIcon from "@mui/icons-material/Add";
import { Box, Button, styled, TextField, Typography } from "@mui/material";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import React from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import Loading from "../../../components/Loading";
import { ConditionModel } from "../../../models/conditions";
import { conditionService } from "../../../services/agent";
import { StyledArrowBack } from "../style";

export const ConditionAdd: React.FC = () => {
  const navigate = useNavigate();
  const [searchParams, setSearchParams] = useSearchParams();
  const id = searchParams.get("id");
  const [loading, setLoading] = React.useState<boolean>(id ? true : false);

  const [startPeriod, setStartPeriod] = React.useState<string>(
    new Date().toISOString().replace(/.\d+Z$/g, "Z")
  );
  const [recordedDate, setRecordedDate] = React.useState<string>(
    new Date().toISOString().replace(/.\d+Z$/g, "Z")
  );
  const [endPeriod, setEndPeriod] = React.useState<string>(
    new Date().toISOString().replace(/.\d+Z$/g, "Z")
  );
  const [formValue, setFormValue] = React.useState<{ [key: string]: string }>({
    startPeriod: startPeriod,
    endPeriod: endPeriod,
    recordedDate: recordedDate,
  });

  //declare function
  const handleStartChange = (newValue: Date | null) => {
    setStartPeriod(newValue?.toISOString().replace(/.\d+Z$/g, "Z") as string);
    setFormValue({
      ...formValue,
      startPeriod: startPeriod as unknown as string,
    });
  };
  const handleEndChange = (newValue: Date | null) => {
    setEndPeriod(newValue?.toISOString().replace(/.\d+Z$/g, "Z") as string);
    setFormValue({
      ...formValue,
      endPeriod: endPeriod as unknown as string,
    });
  };
  const handleRecordDateChange = (newValue: Date | null) => {
    setRecordedDate(newValue?.toISOString().replace(/.\d+Z$/g, "Z") as string);
    setFormValue({
      ...formValue,
      recordedDate: recordedDate as unknown as string,
    });
  };
  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setFormValue({
      ...formValue,
      [event.currentTarget.name]: event.currentTarget.value,
    });
    console.log(formValue);
  };
  const handleSelectChange = (event: SelectChangeEvent) => {
    setFormValue({ ...formValue, [event.target.name]: event.target.value });
  };
  const handleSubmit = () => {
    //mapping value to submit
    const submitValue = {
      identifier: [
        {
          use: "usual",
          type: {
            coding: [
              {
                system: "http://terminology.hl7.org/CodeSystem/v2-0203",
                version: "1.2",
                code: "MR",
                display: "THX",
                userSelected: true,
              },
            ],
            text: "THX",
          },
          system: "urn:oid:1.2.36.146.595.217.0.1",
          value: "12345",
          assignerOrganizationId: "272375da-d2df-4494-9c32-f48ac1bff315",
          period: {
            start: startPeriod,
            end: endPeriod,
          },
        },
      ],

      clinicalStatus: {
        coding: [
          {
            system: "http://terminology.hl7.org/CodeSystem/condition-clinical",
            version: "1.2",
            code: "N",
            display: "THX",
            userSelected: false,
          },
          {
            system: "http://terminology.hl7.org/CodeSystem/condition-clinical",
            version: "1.2",
            code: formValue?.clinicalStatus,
            display: "JBOD",
            userSelected: true,
          },
        ],
        text: formValue?.clinicalStatus,
      },
      verificationStatus: {
        coding: [
          {
            system:
              "http://terminology.hl7.org/CodeSystem/condition-ver-status",
            code: formValue?.verificationStatus,
          },
        ],
      },
      category: [
        {
          coding: [
            {
              system: "http://snomed.info/sct",
              version: "1.2",
              code: "55607006",
              display: formValue?.category,
              userSelected: true,
            },
            {
              system:
                "http://terminology.hl7.org/CodeSystem/condition-category",
              version: "1.2",
              code: "problem-list-item",
              display: "Problem-list",
              userSelected: false,
            },
          ],
        },
      ],
      severity: {
        coding: [
          {
            system: "http://snomed.info/sct",
            version: "1.2",
            code: "255604002",
            display: formValue?.serverity,
            userSelected: false,
          },
        ],
      },
      code: {
        coding: [
          {
            system: "http://snomed.info/sct",
            version: "1.2",
            code: "386661006",
            display: formValue?.code,
            userSelected: false,
          },
        ],
        text: null,
      },
      bodySite: [
        {
          coding: [
            {
              system: "http://snomed.info/sct",
              version: "1.2",
              code: "38266002",
              display: formValue?.bodySite,
              userSelected: false,
            },
          ],
        },
      ],
      subject: {
        reference: "Patient/f201",
        display: formValue?.subject,
      },
      recordedDate: recordedDate,
      evidence: [
        {
          code: [
            {
              coding: [
                {
                  system: "http://snomed.info/sct",
                  version: "1.2",
                  code: "258710007",
                  display: formValue?.evidence,
                  userSelected: false,
                },
              ],
              text: null,
            },
          ],
          detail: [
            {
              reference: "Observation/f202",
              type: null,
              identifier: null,
              display: "Temperature",
            },
          ],
        },
      ],
    } as unknown as ConditionModel;

    if (formValue?.subject) {
      setLoading(true);
      if (id)
        conditionService
          .update({ body: submitValue }, id)
          .then(() =>
            navigate(`/admin/patient/details?id=${id}`, { state: 10 })
          )
          .catch(() => setLoading(false));
      else
        conditionService
          .post({ body: submitValue })
          .then(() =>
            navigate(`/admin/patient/details?id=${id}`, { state: 10 })
          )
          .catch(() => setLoading(false));
    }
  };

  //styling
  const style = {
    "& .MuiOutlinedInput-root": {
      height: "100%",
      "& fieldset": {
        border: "1px solid #EAEAEA",
        borderRadius: "0.8rem",
        padding: 0,
        margin: 0,
      },
      "&:hover fieldset": {
        border: "1px solid #dbd7d7",
      },
      "&.Mui-focused fieldset": {
        border: "1px solid #EAEAEA",
      },
    },
  };
  const StyledButton = styled(Button)(({}) => ({
    margin: "0",
    color: "#FFFFFF",
    fontWeight: "700",
    fontSize: "1rem",
    lineHeight: "1.2rem",
    backgroundColor: "#6C5DD3",
    borderRadius: "6rem",
    padding: "10px 16px",
    whiteSpace: "nowrap",
    "&:hover": {
      backgroundColor: "#887ae6",
    },
  }));
  //Field header Mapping
  const conditionDetailsFields = [
    {
      name: "subject",
      value: formValue?.subject,
      label: "Patient Name",
      required: true,
    },
    {
      name: "category",
      value: formValue?.category,
      label: "Category",
      required: false,
    },
    {
      name: "code",
      value: formValue?.code,
      label: "Disease",
      required: false,
    },
  ];
  const coverageFields = [
    {
      name: "bodySite",
      value: formValue?.bodySite,
      label: "Body Site",
      required: false,
    },
    {
      name: "evidence",
      value: formValue?.evidence,
      label: "Evidence",
      required: false,
    },
  ];
  const ClinicalStatus = [
    { value: "resolve", display: "Resolve" },
    { value: "pending", display: "Pending" },
    { value: "reject", display: "Reject" },
  ];

  const VerificationStatus = [
    { value: "confirmed", display: "Confirmed" },
    { value: "unconfirmed", display: "Unconfirmed" },
  ];

  React.useEffect(() => {
    if (id)
      conditionService
        .getById(id)
        .then((res) => {
          setFormValue({
            subject: res.subject ? (res.subject.display as string) : "",
            status: "resolve",
          });
          setLoading(false);
        })
        .catch(() => history.back());
  }, [id]);

  return (
    <Box>
      {/* Header */}
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <Typography
          sx={{
            fontWeight: 700,
            fontSize: 24,
            display: "flex",
            alignItems: "center",
            gap: "16px",
          }}
        >
          <StyledArrowBack onClick={() => history.back()} />
          {id ? "UPDATE CONDITION" : "ADD CONDITION"}
        </Typography>
        <StyledButton onClick={handleSubmit}>
          {searchParams.get("id") ? (
            <>Save changes</>
          ) : (
            <>
              {" "}
              Add <AddIcon style={{ marginLeft: "10px" }} />
            </>
          )}
        </StyledButton>
      </Box>
      {/* Body */}
      {loading && <Loading />}
      {!loading && (
        <Box
          style={{
            display: "flex",
            justifyContent: "space-between",
            background: "white",
            borderRadius: "16px",
            margin: "16px 0",
            padding: "16px",
          }}
        >
          {/* Left Col */}
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              gap: "16px",
              width: "49%",
            }}
          >
            <Typography sx={{ fontWeight: 600, fontSize: 20 }}>
              General Information
            </Typography>
            {conditionDetailsFields.map((item) => (
              <>
                <TextField
                  name={item.name}
                  value={item.value}
                  onChange={handleInputChange}
                  label={item.label}
                  required={item.required}
                  error={item.required ? !item.value : false}
                  sx={style}
                />
              </>
            ))}

            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                "& .MuiFormControl-root": {
                  flexBasis: "100%",
                },
                ...style,
              }}
            >
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DesktopDatePicker
                  label="Valid From"
                  inputFormat="MM/dd/yyyy"
                  value={new Date(startPeriod)}
                  onChange={handleStartChange}
                  renderInput={(params) => (
                    <TextField
                      sx={{ marginRight: "8px" }}
                      {...params}
                      error={startPeriod ? startPeriod >= endPeriod : true}
                    />
                  )}
                />
                <DesktopDatePicker
                  label="Good Thru"
                  inputFormat="MM/dd/yyyy"
                  value={new Date(endPeriod)}
                  onChange={handleEndChange}
                  renderInput={(params) => (
                    <TextField
                      sx={{ marginRight: "8px" }}
                      {...params}
                      error={endPeriod ? endPeriod <= startPeriod : true}
                    />
                  )}
                />
              </LocalizationProvider>
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DesktopDatePicker
                  label="Recorded Date"
                  inputFormat="MM/dd/yyyy"
                  value={new Date()}
                  onChange={handleRecordDateChange}
                  renderInput={(params) => <TextField {...params} />}
                />
              </LocalizationProvider>
            </Box>
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                "& .MuiFormControl-root": {
                  flexBasis: "100%",
                },
                ...style,
              }}
            ></Box>
          </Box>
          {/* Right Col*/}
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              gap: "16px",
              width: "49%",
            }}
          >
            <Typography sx={{ fontWeight: 600, fontSize: 20 }}>
              More Information
            </Typography>
            {coverageFields.map((item) => (
              <TextField
                name={item.name}
                value={item.value}
                onChange={handleInputChange}
                label={item.label}
                required={item.required}
                error={item.required ? !item.value : false}
                sx={style}
              />
            ))}
            <FormControl>
              <InputLabel id="clinicalStatus-select-label">
                Clinical Status
              </InputLabel>
              <Select
                sx={{
                  borderRadius: "10px",
                }}
                labelId="clinicalStatus-select-label"
                id="clinicalStatus-select"
                value={formValue?.clinicalStatus}
                label="Clinical Status"
                name="clinicalStatus"
                onChange={handleSelectChange}
                required
                error={!formValue?.clinicalStatus}
              >
                {ClinicalStatus.map(({ value, display }) => (
                  <MenuItem value={value}>{display}</MenuItem>
                ))}
              </Select>
            </FormControl>
            <FormControl>
              <InputLabel id="verificationStatus-select-label">
                Verification Status
              </InputLabel>
              <Select
                sx={{
                  borderRadius: "10px",
                }}
                labelId="verificationStatus-select-label"
                id="verificationStatus-select"
                value={formValue?.verificationStatus}
                label="Verification Status"
                name="verificationStatus"
                onChange={handleSelectChange}
                required
                error={!formValue?.verificationStatus}
              >
                {VerificationStatus.map(({ value, display }) => (
                  <MenuItem value={value}>{display}</MenuItem>
                ))}
              </Select>
            </FormControl>
          </Box>
        </Box>
      )}
    </Box>
  );
};
