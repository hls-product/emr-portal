import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import { IconButton } from "@mui/material";
import { styled } from "@mui/system";
import React from "react";
import { Link, useSearchParams } from "react-router-dom";
import Loading from "../../../components/Loading";
import { conditionService } from "../../../services/agent";

interface TableProps {
  patient: string;
  date: string;
  time: string;
  severity: string;
  disease: string;
  category: string;
  clinicalStatus: any;
  verificationStatus: any;
  bodySite: string;
  id: string;
  evidence: string;
  abatementString: string;
}

const Table = styled("table")({
  background: "white",
  flex: 1,
  padding: "8px 16px",
  borderRadius: "16px",
  "& tr": {
    lineHeight: "22px",
  },
  "& th": {
    padding: "8px 0",
    textAlign: "left",
    lineHeight: "22px",
  },
  "& td": {
    padding: "8px 0",
    textAlign: "right",
  },
});

const DetailTable: React.FC<TableProps> = ({ ...props }) => (
  <div
    style={{
      display: "flex",
      justifyContent: "space-between",
      gap: "20px",
    }}
  >
    <Table>
      <tr>
        <th>Patient Name</th>
        <td>{props.patient}</td>
      </tr>
      <tr>
        <th>Date</th>
        <td>{props.date}</td>
      </tr>
      <tr>
        <th>Time</th>
        <td>{props.time}</td>
      </tr>
      <tr>
        <th>Severity</th>
        <td>{props.severity}</td>
      </tr>
      <tr>
        <th>Disease</th>
        <td>{props.disease}</td>
      </tr>
      <tr>
        <th>Category</th>
        <td>{props.category}</td>
      </tr>
    </Table>
    <Table>
      <tr>
        <th>Clinical Status</th>
        <td>
          <div
            style={{
              padding: "3px 12px",
              border: "1px solid rgba(255, 115, 0, 1)",
              color: "rgba(255, 115, 0, 1)",
              borderRadius: "21px",
              width: "fit-content",
              float: "right",
            }}
          >
            {props.clinicalStatus}
          </div>
        </td>
      </tr>
      <tr>
        <th>Verification Status</th>
        <td>
          <div
            style={{
              padding: "3px 12px",
              border: "1px solid blue",
              color: "blue",
              borderRadius: "21px",
              width: "fit-content",
              float: "right",
            }}
          >
            {props.verificationStatus}
          </div>
        </td>
      </tr>
      <tr>
        <th>Body Site</th>
        <td>{props.bodySite}</td>
      </tr>
      <tr>
        <th>Abatement</th>
        <td>{props.abatementString}</td>
      </tr>
      <tr>
        <th>Evidence</th>
        <td>{props.evidence}</td>
      </tr>
      <tr>
        <th>ID</th>
        <td>{props.id}</td>
      </tr>
    </Table>
  </div>
);

export const ConditionDetail: React.FC = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const id = searchParams.get("id");
  const [loading, setLoading] = React.useState<boolean>(true);
  const [data, setData] = React.useState<TableProps>({
    patient: "",
    date: "",
    time: "",
    severity: "",
    disease: "",
    category: "",
    clinicalStatus: "",
    verificationStatus: "",
    bodySite: "",
    id: "",
    evidence: "",
    abatementString: "",
  });

  React.useEffect(() => {
    if (!id) throw history.back();
    conditionService
      .getById(id)
      .then((res) => {
        setData(() => {
          const newData = {
            patient: res.subject ? (res.subject.display as string) : "",
            date: res.identifier
              ? res.identifier[0].period
                ? (res.identifier[0].period.start?.slice(0, 10) as string)
                : ""
              : "",
            time: res.identifier
              ? res.identifier[0].period
                ? (res.identifier[0].period.start?.slice(11, 19) as string)
                : ""
              : "",
            verificationStatus: res.verificationStatus?.coding
              ? (res.verificationStatus.coding[0]?.code
                  ? (res.verificationStatus.coding[0]?.code[0].toUpperCase() as string)
                  : "") +
                (res.verificationStatus.coding[0]?.code
                  ? (res.verificationStatus.coding[0]?.code.substring(
                      1
                    ) as string)
                  : "")
              : "N/A",
            clinicalStatus: res.clinicalStatus?.coding
              ? (res.clinicalStatus.coding[1]?.code
                  ? (res.clinicalStatus.coding[1]?.code[0].toUpperCase() as string)
                  : "") +
                (res.clinicalStatus.coding[1]?.code
                  ? (res.clinicalStatus.coding[1]?.code.substring(1) as string)
                  : "")
              : "N/A",
            severity: res.severity
              ? res.severity.coding
                ? (res.severity.coding[0]?.display as string)
                : ""
              : "",

            disease: res.code
              ? res.code.coding
                ? (res.code.coding[0]?.display as string)
                : ""
              : "",
            category: res.category
              ? res.category[0]?.coding
                ? (res.category[0].coding[0]?.display as string)
                : ""
              : "",
            bodySite: res.bodySite
              ? res.bodySite[0]
                ? res.bodySite[0].coding
                  ? (res.bodySite[0].coding[0]?.display as string)
                  : ""
                : ""
              : "",
            evidence: res.evidence
              ? res.evidence[0]?.code
                ? res.evidence[0]?.code[0]?.coding
                  ? (res.evidence[0]?.code[0]?.coding[0]?.display as string)
                  : ""
                : ""
              : "",
            abatementString: res.recordedDate
              ? (res.recordedDate.slice(0, 10) as string)
              : "",
            id: res.id as string,
          } as TableProps;
          return newData;
        });
        setLoading(false);
      })
      .catch(() => history.back());
  }, [id]);

  return (
    <div>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <div
          style={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <IconButton
            style={{
              color: "rgba(108, 93, 211, 1)",
              background: "white",
              borderRadius: "50%",
              width: "40px",
              height: "40px",
              marginRight: "16px",
            }}
            onClick={() => history.back()}
          >
            <ArrowBackIcon />
          </IconButton>
          <h1 style={{ fontSize: "24px" }}>Condition Details</h1>
        </div>
        <Link
          to={`update?id=${data.id}`}
          style={{
            padding: "10px 30px",
            borderRadius: "30px",
            color: "rgba(108, 93, 211, 1)",
            background: "white",
            textDecoration: "none",
          }}
        >
          <EditOutlinedIcon style={{ marginRight: "12px" }} />
          Edit
        </Link>
      </div>
      {loading && <Loading></Loading>}
      {!loading && <DetailTable {...data} />}
    </div>
  );
};
