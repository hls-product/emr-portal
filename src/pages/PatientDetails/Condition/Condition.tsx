import { Add, Create, Delete, Search, Visibility } from "@mui/icons-material";
import {
  Box,
  IconButton,
  InputAdornment,
  TextField,
  Typography,
} from "@mui/material";
import React from "react";
import { Link, useNavigate } from "react-router-dom";
import { DeleteDialog } from "../../../components/DeleteDialog";
import { RoundedButton } from "../../../components/RoundedButton";
import { AdvancedSearch } from "../../../components/Table/AdvancedSearch";
import { CustomTable } from "../../../components/Table/Table";

import { HeadCell } from "../../../components/Table/types";
import { ConditionModel } from "../../../models/conditions";
import { conditionService } from "../../../services/agent";
import { colorPalette } from "../../../theme";
const headCells: HeadCell[] = [
  {
    id: "number",
    numeric: false,
    label: "No.",
  },
  {
    id: "patient",
    numeric: false,
    label: "Patient Name",
  },
  {
    id: "date",
    numeric: false,
    label: "Date",
  },
  {
    id: "time",
    numeric: false,
    label: "Time",
  },
  {
    id: "severity",
    numeric: false,
    label: "Severity",
  },
  {
    id: "disease",
    numeric: false,
    label: "Disease",
  },
  {
    id: "bodySite",
    numeric: false,
    label: "Body Site",
  },
  {
    id: "verificationStatus",
    numeric: false,
    label: "Verification Status",
  },
  {
    id: "action",
    numeric: false,
    label: "Action",
  },
];
const advancedSearchProps = [
  {
    name: "severity",
    label: "Severity",
    select: true,
    menuItems: [
      {
        value: "24484000",
        display: "Severe",
      },
      {
        value: "6736007",
        display: "Moderate",
      },
      {
        value: "255604002",
        display: "Mild",
      },
    ],
  },
  {
    name: "verificationStatus",
    label: "Verification Status",
    select: true,
    menuItems: [
      {
        value: "unconfirmed",
        display: "Unconfirmed",
      },
      {
        value: "provisional",
        display: "Provisional",
      },
      {
        value: "differential",
        display: "Differential",
      },
      {
        value: "confirmed",
        display: "Confirmed",
      },
      {
        value: "refuted",
        display: "Refuted",
      },
      {
        value: "entered-in-error",
        display: "Entered-in-Error",
      },
    ],
  },
];
export const Condition: React.FC = () => {
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = React.useState(false);
  const [data, setData] = React.useState<ConditionModel[]>([]);
  const [page, setPage] = React.useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = React.useState<number>(10);
  const [rowsCount, setRowsCount] = React.useState<number>(0);
  const [deletePopUpOpen, setDeletePopUpOpen] = React.useState(false);
  const [openAdvancedSearch, setOpenAdvancedSearch] = React.useState(false);
  const [currentSelectedId, setCurrentSelectedId] = React.useState("");

  // Search states
  const [searchValue, setSearchValue] = React.useState<string>("");
  const [debounceSeachValue, setDebounceSearchValue] =
    React.useState<string>("");
  const [filter, setFilter] = React.useState<{ [key: string]: string }>({});

  const getConditionList = async (filter?: { [keys: string]: string }) => {
    const params = {
      ...filter,
      page: page + 1,
      size: rowsPerPage,
      name: debounceSeachValue,
    };
    setIsLoading(true);
    try {
      let { data, paging } = await conditionService.getList({ params });
      setData(data);
      setRowsCount(paging.total);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      console.log("getConditionList error: ", error);
    }
  };
  const deleteCondition = async (id: string) => {
    setIsLoading(true);
    try {
      let response = await conditionService.delete(id);
      setIsLoading(false);
      return response;
    } catch (error) {
      setIsLoading(false);
      console.log("deleteCondition error: ", error);
    }
  };

  // Event Handler
  const handleChangePage = (event: any, newPage: number) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event: any) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleDelete = (id: string) => {
    setDeletePopUpOpen(true);
    setCurrentSelectedId(id);
  };
  const handleCancelDelete = () => {
    setDeletePopUpOpen(false);
  };
  const handleConfirmDelete = () => {
    setDeletePopUpOpen(false);
    deleteCondition(currentSelectedId).then((res) => {
      setCurrentSelectedId("");
      getConditionList();
    });
  };
  const handleClickAdvancedSearchOpen = () => {
    setOpenAdvancedSearch(true);
  };
  //Debounce for name search
  React.useEffect(() => {
    let timeout = setTimeout(() => {
      setDebounceSearchValue(searchValue);
    }, 500);
    return () => {
      clearTimeout(timeout);
    };
  }, [searchValue]);

  // Back to previous page if current page is empty after deleting an item
  React.useEffect(() => {
    let totalPages = Math.ceil(rowsCount / rowsPerPage);
    let currentPage = page + 1;
    if (currentPage > totalPages && totalPages > 0) {
      setPage(totalPages - 1);
    }
  }, [rowsCount]);
  React.useEffect(() => {
    getConditionList();
  }, [debounceSeachValue, rowsPerPage, page]);

  const StatusCircle = (status: string) => {
    const theme = {
      confirmed: colorPalette.purple.shade_500,
      unconfirmed: colorPalette.orange.shade_500,
    };
    return (
      <div
        style={{
          height: 6,
          width: 6,
          borderRadius: "50%",
          background: theme[status as keyof typeof theme] as string,
        }}
      />
    );
  };

  const mapDataToRender = () => {
    const render = data.map((item, index) => ({
      id: item.id as string,
      number: index + 1,
      patient: item.subject?.display,
      date: new Date(
        item.identifier?.[0]?.period?.start as string
      ).toLocaleDateString(),
      time: new Date(
        item.identifier?.[0]?.period?.start as string
      ).toLocaleTimeString(),
      severity: item.severity?.coding?.[0]?.display,
      disease: item.code?.coding?.[0]?.display,
      bodySite: item.bodySite?.[0]?.coding?.[0]?.display,
      verificationStatus: (
        <Box display="flex" alignItems="center" gap="8px">
          {StatusCircle(item.verificationStatus?.coding?.[0]?.code as string)}
          <Typography
            textTransform="capitalize"
            children={item.verificationStatus?.coding?.[0]?.code}
          />
        </Box>
      ),
      action: (
        <Box style={{ textAlign: "center", whiteSpace: "nowrap" }}>
          <Link
            to={`condition/update?id=${item.id}`}
            children={<IconButton children={<Create />} />}
          />
          <Link
            to={`condition?id=${item.id}`}
            children={<IconButton children={<Visibility />} />}
          />

          <Link
            to={`#`}
            onClick={() => {
              handleDelete(item.id as string);
            }}
            children={<IconButton children={<Delete />} />}
          />
        </Box>
      ),
    }));
    return render;
  };

  return (
    <>
      {/* Dialog for delete item popup */}
      <DeleteDialog
        open={deletePopUpOpen}
        onClose={handleCancelDelete}
        onConfirm={handleConfirmDelete}
        onCancel={handleCancelDelete}
      />
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          width: "100%",
          padding: "20px",
          backgroundColor: "white",
          borderRadius: "16px",
        }}
      >
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            gap: "8px",
          }}
        >
          <Typography variant="h2" children={"CONDITION"} />
          <Box sx={{ display: "flex", gap: "8px" }}>
            <TextField
              placeholder="Search condition..."
              InputProps={{
                startAdornment: (
                  <InputAdornment
                    position="start"
                    children={<Search sx={{ height: 20 }} />}
                  />
                ),
              }}
              sx={{
                "& .MuiInputBase-root": {
                  height: 48,
                  width: 331,
                  borderRadius: 3,
                },
              }}
              value={searchValue}
              onChange={(e) => {
                setSearchValue(e.target.value);
              }}
            />
            <RoundedButton
              variant="contained"
              onClick={handleClickAdvancedSearchOpen}
              children="Advanced Search"
            />
            <RoundedButton
              variant="contained"
              endIcon={<Add />}
              onClick={() => {
                navigate("condition/add");
              }}
              children="Add New Condtition"
            />
          </Box>
        </Box>
        {openAdvancedSearch && (
          <AdvancedSearch
            props={advancedSearchProps}
            filter={filter}
            description="Find condition with..."
            onFilterChange={setFilter}
            onApply={getConditionList}
            onClose={setOpenAdvancedSearch}
          />
        )}
        <CustomTable
          title={"CONDITION"}
          headcells={headCells}
          body={mapDataToRender()}
          paging={{
            page: page,
            rowsPerPage: rowsPerPage,
            rowsCount: rowsCount,
          }}
          loading={isLoading}
          toolbar={false}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Box>
    </>
  );
};
