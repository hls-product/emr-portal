import { styled as muiStyled } from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import TextField from "@mui/material/TextField";
import PersonOutlineIcon from "@mui/icons-material/PersonOutline";
import TransgenderIcon from "@mui/icons-material/Transgender";
import PhoneIphoneIcon from "@mui/icons-material/PhoneIphone";
import MailOutlineIcon from "@mui/icons-material/MailOutline";
import BloodtypeIcon from "@mui/icons-material/Bloodtype";
import WarningAmberIcon from "@mui/icons-material/WarningAmber";
import DescriptionIcon from "@mui/icons-material/Description";
import EditIcon from "@mui/icons-material/Edit";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import CircleIcon from "@mui/icons-material/Circle";

// 0. Type
type InputProps = {
  widthProp?: string;
  heightProp?: string;
  backgroundProp?: string;
  paddingLeftProp?: string;
  marginLeftProp?: string;
  tabStatusProp?: "current" | "disabled";
};

// 1. Containers

// Component container
export const StyledComponentContainer = muiStyled("div")(({}) => ({
  width: "100%",
  height: "100%",
  minHeight: "10rem",
  margin: "0",
}));

// Container for top section (back arrow + page title)
export const StyledTopContainer = muiStyled("div")(({}) => ({
  padding: "0",
  margin: "0",
  width: "100%",
  height: "32px",
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
}));

// Container for tab section (information/appoinment/medical record)
export const StyledTabContainer = muiStyled("div")(({}) => ({
  padding: "0",
  margin: "0",
  width: "100%",
  height: "4.5rem",
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
  backgroundColor: "#FFFFFF",
  borderRadius: "1rem",
}));

// Container for Back Arrow and Page Title
export const StyledTopLeftContainer = muiStyled("div")(({}) => ({
  padding: "0rem",
  margin: "0",
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
  width: "13.5%",
}));

// Container for Tab buttons
export const StyledTabButtonContainer = muiStyled("div")(({}) => ({
  padding: "0",
  margin: "0",
  display: "flex",
  justifyContent: "flex-start",
  alignItems: "center",
  width: "100%",
}));

// Body container
export const StyledBodyMainContainer = muiStyled("div")(({}) => ({
  margin: "0",
  padding: "0",
  width: "100%",
  height: "100%",
  display: "flex",
  justifyContent: "space-between",
}));

// Profile row container
export const StyledProfileRowContainer = muiStyled("div")(
  ({ paddingLeftProp }: InputProps) => ({
    margin: "0",
    padding: "0",
    paddingLeft: paddingLeftProp,
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
    lineHeight: "100%",
  })
);

// Profile row left side container
export const StyledLeftSideProfileRowContainer = muiStyled("div")(
  ({ paddingLeftProp }: InputProps) => ({
    margin: "0",
    padding: "0",
    width: "40%",
    height: "100%",
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
    color: "#131626",
    fontWeight: "500",
    fontSize: "1rem",
  })
);

// Profile row right side container
export const StyledRightSideProfileRowContainer = muiStyled("div")(
  ({ paddingLeftProp }: InputProps) => ({
    margin: "0",
    padding: "0",
    width: "30%",
    height: "100%",
    display: "flex",
    justifyContent: "flex-end",
    color: "#131626",
    fontWeight: "500",
    fontSize: "1rem",
  })
);

// Body child container
export const StyledLeftBodyChildContainer = muiStyled("div")(({}) => ({
  margin: "0",
  paddingTop: "0",
  paddingBottom: "1rem",
  paddingLeft: "1rem",
  paddingRight: "1rem",
  width: "49%",
  height: "37rem",
  backgroundColor: "#FFFFFF",
  borderRadius: "1rem",
  display: "flex",
  flexDirection: "column",
  justifyContent: "space-around",
}));

export const StyledRightBodyChildContainer = muiStyled("div")(({}) => ({
  margin: "0",
  paddingTop: "0",
  paddingBottom: "1rem",
  paddingLeft: "1rem",
  paddingRight: "1rem",
  width: "49%",
  height: "37rem",
  backgroundColor: "#FFFFFF",
  borderRadius: "1rem",
  display: "flex",
  flexDirection: "column",
  justifyContent: "flex-start",
}));

// Container for single activity
export const StyledActivityContainer = muiStyled("div")(({}) => ({
  margin: "0",
  padding: "0",
  width: "100%",
  height: "40rem",
  display: "flex",
  flexDirection: "column",
  justifyContent: "space-around",
}));

// Profile row container
export const StyledProfileChip = muiStyled("div")(({}) => ({
  margin: "0",
  padding: "0",
  marginRight: "0.1rem",
  border: "1px solid #EAEAEA",
  width: "3.4rem",
  height: "1.5rem",
  borderRadius: "1.3rem",
  color: "#131626",
  fontSize: "0.75rem",
  fontWeight: "500",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
}));

// Single activity container
export const StyledSingleActivityContainer = muiStyled("div")(({}) => ({
  margin: "0",
  padding: "0",
  width: "100%",
  height: "3.8rem",
  lineHeight: "100%",
  display: "flex",
  flexDirection: "column",
  justifyContent: "space-between",
}));

// 2. Child Elements of above containers

// Back Arrow
export const StyledArrowBack = muiStyled(ArrowBackIcon)(({}) => ({
  color: "#6C5DD3",
  width: "2.5rem",
  height: "2.5rem",
  borderRadius: "6rem",
  padding: "0.5rem",
  cursor: "pointer",
}));

// Page title

export const StyledPageTitle = muiStyled("div")(({}) => ({
  color: "#131626",
  fontWeight: "600",
  fontSize: "24px",
  lineHeight: "32px",
  padding: "0",
  margin: "0",
}));

// Tab button (Information/Appoinment/Medical record)
export const StyledTabButton = muiStyled("div")(
  ({ tabStatusProp }: InputProps) => {
    if (tabStatusProp == "current") {
      return {
        margin: "0",
        paddingTop: "0.5rem",
        paddingBottom: "0.5rem",
        paddingLeft: "0.6rem",
        paddingRight: "0.6rem",
        color: "#FFFFFF",
        fontWeight: "600",
        fontSize: "0.8rem",
        lineHeight: "1.37rem",
        backgroundColor: "#6C5DD3",
        borderRadius: "0.62rem",
        width: "7rem",
        height: "2.5rem",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexWrap: "wrap",
        cursor: "pointer",
        "&:hover": {
          backgroundColor: "#887ae6",
        },
      };
    } else {
      return {
        margin: "0",
        paddingTop: "0.5rem",
        paddingBottom: "0.5rem",
        paddingLeft: "0.6rem",
        paddingRight: "0.6rem",
        color: "#4E4E4E",
        fontWeight: "600",
        fontSize: "0.8rem",
        lineHeight: "1.37rem",
        backgroundColor: "#FFFFFF",
        borderRadius: "0.62rem",
        width: "7rem",
        height: "2.5rem",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexWrap: "wrap",
        cursor: "pointer",
        "&:hover": {
          color: "red",
        },
      };
    }
  }
);

// Body Section Title
export const StyledBodySectionTitle = muiStyled("div")(({}) => ({
  color: "#081735",
  fontWeight: "600",
  fontSize: "1rem",
  lineHeight: "1.5rem",
  padding: "0",
  margin: "0",
  height: "2.5rem",
  width: "100%",
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
}));

// Sub Body Section Title
export const StyledSubBodySectionTitle = muiStyled("div")(({}) => ({
  color: "#4E4E4E",
  fontWeight: "600",
  fontSize: "0.85rem",
  lineHeight: "1.37rem",
  padding: "0",
  margin: "0",
  height: "1.37rem",
  width: "100%",
  display: "flex",
  justifyContent: "flex-start",
  alignItems: "center",
}));

// Input Style
export const StyledInputTextField = muiStyled(TextField)`
    & .MuiOutlinedInput-root {
      height: 100%;
      & fieldset {
        border: 1px solid #EAEAEA;
        border-radius: 0.8rem;
        padding: 0;
        margin: 0
      }

      &:hover fieldset {
        border: 1px solid #dbd7d7;
      }

      &.Mui-focused fieldset {
        border: 1px solid #EAEAEA;
      }
    }
  `;

// Styled break line
export const StyledBreakLine = muiStyled("div")(
  ({ marginLeftProp }: InputProps) => {
    return {
      marginLeft: marginLeftProp,
      border: "1px solid #F4F4F4;",
    };
  }
);

// Invisible
export const StyledInvisibleBreakLine = muiStyled("div")(
  ({ marginLeftProp }: InputProps) => {
    return {
      marginLeft: marginLeftProp,
      border: "1px solid white",
    };
  }
);

// Styled profile icons
const commonIconStyleObject = {
  padding: "0",
  margin: "0",
  color: "#6C5DD3",
  width: "1.2rem",
  height: "1.2rem",
  marginRight: "1rem",
};

export const StyledProfileNameIcon = muiStyled(PersonOutlineIcon)(
  ({}) => commonIconStyleObject
);

export const StyledProfileGenderIcon = muiStyled(TransgenderIcon)(
  ({}) => commonIconStyleObject
);

export const StyledProfilePhoneIcon = muiStyled(PhoneIphoneIcon)(
  ({}) => commonIconStyleObject
);

export const StyledProfileMailIcon = muiStyled(MailOutlineIcon)(
  ({}) => commonIconStyleObject
);

export const StyledProfileBloodTypeIcon = muiStyled(BloodtypeIcon)(
  ({}) => commonIconStyleObject
);

export const StyledProfileWarningIcon = muiStyled(WarningAmberIcon)(
  ({}) => commonIconStyleObject
);

export const StyledProfileDescriptionIcon = muiStyled(DescriptionIcon)(
  ({}) => commonIconStyleObject
);

export const StyledProfileEditIcon = muiStyled(EditIcon)(({}) => ({
  color: "#4E4E4E",
  width: "1rem",
  height: "1rem",
  cursor: "pointer",
  "&:hover": {
    color: "red",
    width: "1.1rem",
    height: "1.1rem",
  },
}));

// Activity icons

export const StyledActivityMoreIcon = muiStyled(MoreVertIcon)(({}) => ({
  color: "#4E4E4E",
  width: "1rem",
  height: "1rem",
  cursor: "pointer",
  borderRadius: "50%",
  "&:hover": {
    width: "1.1rem",
    height: "1.1rem",
    backgroundColor: "#D3D3D3",
  },
}));

export const StyledArrowDownIcon = muiStyled(KeyboardArrowDownIcon)(
  ({}) => ({})
);

// View more activity

export const StyledViewMoreActivity = muiStyled("div")(({}) => ({
  margin: "0",
  padding: "0",
  marginTop: "1rem",
  width: "20%",
  height: "1.5rem",
  color: "#6C5DD3",
  fontSize: "0.9rem",
  lineHeight: "100%",
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
  cursor: "pointer",
  "&:hover": {
    color: "blue",
  },
}));

// Single activity styled child elements

// Container for title row in single activity
export const StyledSingleActivityTitleRow = muiStyled("div")(({}) => ({
  margin: "0",
  padding: "0",
  height: "1.2rem",
  width: "100%",
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
}));

// Left side of title row in single activity component
export const StyledActivityTitleLeft = muiStyled("div")(({}) => ({
  margin: "0",
  padding: "0",
  height: "1.2rem",
  lineHeight: "100%",
  color: "#131626",
  width: "4.5rem",
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
  fontWeight: "600",
  fontSize: "1rem",
}));

// Right side of title row in single activity component
export const StyledActivityTitleRight = muiStyled("div")(({}) => ({
  margin: "0",
  padding: "0",
  height: "1.2rem",
  lineHeight: "1.2rem",
  color: "##9FA0A6",
  width: "9rem",
  display: "flex",
  justifyContent: "flex-end",
  alignItems: "center",
  fontWeight: "500",
  fontSize: "0.75rem",
}));

// Round bullet point single activity component
export const StyledActivityBulletPoint = muiStyled("div")(({}) => ({
  margin: "0",
  padding: "0",
  height: "1.2rem",
  width: "1.2rem",
  border: "1px solid #9FA0A6",
  borderRadius: "50%",
}));

// Container for single activity description row
export const StyledSingleActivityDescriptionRow = muiStyled("div")(({}) => ({
  margin: "0",
  padding: "0",
  height: "2.4rem",
  width: "100%",
  display: "flex",
  justifyContent: "flex-start",
  alignItems: "center",
  paddingLeft: "0.5rem",
  paddingTop: "0.5rem",
}));

// Styled single activity bullet point connection line
export const StyledSingleActivityConnectionLine = muiStyled("div")(({}) => ({
  margin: "0",
  padding: "0",
  height: "100%",
  border: "1px solid #EAEAEA",
  width: "1px",
}));

// Styled single activity description
export const StyledSingleActivityDescription = muiStyled("div")(({}) => ({
  margin: "0",
  padding: "0",
  paddingLeft: "1.3rem",
  height: "1.1rem",
  lineHeight: "1.1rem",
  color: "#4E4E4E",
  width: "26rem",
  display: "flex",
  justifyContent: "flex-start",
  alignItems: "center",
  fontWeight: "500",
  fontSize: "0.75rem",
}));

// Bullet point of description line
export const StyledDescriptionLineBullet = muiStyled(CircleIcon)(({}) => ({
  margin: "0",
  marginRight: "0.5rem",
  padding: "0",
  fontSize: "25%",
}));
