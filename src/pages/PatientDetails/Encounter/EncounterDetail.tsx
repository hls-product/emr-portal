import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import { IconButton } from "@mui/material";
import { styled } from "@mui/system";
import React from "react";
import { Link, useSearchParams } from "react-router-dom";
import Loading from "../../../components/Loading";
import { encounterService } from "../../../services/agent";

interface TableProps {
  patient: string;
  date: string;
  time: string;
  duration: string;
  type: string;
  reasonCode: string;
  status: any;
  priority: string;
  id: string;
  serviceProvider: string;
  admitSource: string;
  dischargeDisposition: string;
}

const Table = styled("table")({
  background: "white",
  flex: 1,
  padding: "8px 16px",
  borderRadius: "16px",
  "& tr": {
    lineHeight: "22px",
  },
  "& th": {
    padding: "8px 0",
    textAlign: "left",
    lineHeight: "22px",
  },
  "& td": {
    padding: "8px 0",
    textAlign: "right",
  },
});

const DetailTable: React.FC<TableProps> = ({ ...props }) => (
  <div
    style={{
      display: "flex",
      justifyContent: "space-between",
      gap: "20px",
    }}
  >
    <Table>
      <tr>
        <th>Patient Name</th>
        <td>{props.patient}</td>
      </tr>
      <tr>
        <th>Date</th>
        <td>{props.date}</td>
      </tr>
      <tr>
        <th>Time</th>
        <td>{props.time}</td>
      </tr>
      <tr>
        <th>Duration</th>
        <td>{props.duration}</td>
      </tr>
      <tr>
        <th>Type</th>
        <td>{props.type}</td>
      </tr>
      <tr>
        <th>Reason</th>
        <td>{props.reasonCode}</td>
      </tr>
    </Table>
    <Table>
      <tr>
        <th>Status</th>
        <td>
          <div
            style={{
              padding: "3px 12px",
              border: "1px solid rgba(255, 115, 0, 1)",
              color: "rgba(255, 115, 0, 1)",
              borderRadius: "21px",
              width: "fit-content",
              float: "right",
            }}
          >
            {props.status}
          </div>
        </td>
      </tr>
      <tr>
        <th>Priority</th>
        <td>{props.priority}</td>
      </tr>
      <tr>
        <th>Hospitalization Admit Source</th>
        <td>{props.admitSource}</td>
      </tr>
      <tr>
        <th>Discharge Disposition</th>
        <td>{props.dischargeDisposition}</td>
      </tr>
      <tr>
        <th>Service Provider</th>
        <td>{props.serviceProvider}</td>
      </tr>
      <tr>
        <th>ID</th>
        <td>{props.id}</td>
      </tr>
    </Table>
  </div>
);

export const EncounterDetail: React.FC = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const id = searchParams.get("id");
  const [loading, setLoading] = React.useState<boolean>(true);
  const [data, setData] = React.useState<TableProps>({
    patient: "",
    date: "",
    time: "",
    duration: "",
    type: "",
    reasonCode: "",
    status: "",
    priority: "",
    id: "",
    serviceProvider: "",
    admitSource: "",
    dischargeDisposition: "",
  });

  React.useEffect(() => {
    if (!id) throw history.back();
    encounterService
      .getById(id)
      .then((res) => {
        setData(() => {
          const newData = {
            patient: res.subject ? (res.subject.display as string) : "",
            date: res.identifier
              ? res.identifier[0].period
                ? (res.identifier[0].period.start?.slice(0, 10) as string)
                : ""
              : "",
            time: res.identifier
              ? res.identifier[0].period
                ? (res.identifier[0].period.start?.slice(11, 19) as string)
                : ""
              : "",
            status: res.status as string,
            duration:
              (res.length ? (res.length.value as number) : "") +
              " " +
              (res.length ? (res.length.code as string) : ""),
            type: res.type
              ? res.type[0].coding
                ? (res.type[0].coding[0].display as string)
                : ""
              : "",
            reasonCode: res.reasonCode
              ? res.reasonCode[0].coding
                ? (res.reasonCode[0].coding[0].display as string)
                : ""
              : "",
            priority: res.priority
              ? res.priority.coding
                ? (res.priority.coding[0].display as string)
                : ""
              : "",
            serviceProvider: res.serviceProvider?.display as string,
            dischargeDisposition: res.hospitalization
              ? res.hospitalization.dischargeDisposition
                ? res.hospitalization.dischargeDisposition.coding
                  ? (res.hospitalization.dischargeDisposition.coding[0]
                      .display as string)
                  : ""
                : ""
              : "",
            admitSource: res.hospitalization
              ? res.hospitalization.admitSource
                ? res.hospitalization.admitSource.coding
                  ? (res.hospitalization.admitSource.coding[0]
                      .display as string)
                  : ""
                : ""
              : "",
            id: res.id as string,
          } as TableProps;
          return newData;
        });
        setLoading(false);
      })
      .catch(() => history.back());
  }, [id]);

  return (
    <div>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <div
          style={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <IconButton
            style={{
              color: "rgba(108, 93, 211, 1)",
              background: "white",
              borderRadius: "50%",
              width: "40px",
              height: "40px",
              marginRight: "16px",
            }}
            onClick={() => history.back()}
          >
            <ArrowBackIcon />
          </IconButton>
          <h1 style={{ fontSize: "24px" }}>Encounter Details</h1>
        </div>
        <Link
          to={`update?id=${data.id}`}
          style={{
            padding: "10px 30px",
            borderRadius: "30px",
            color: "rgba(108, 93, 211, 1)",
            background: "white",
            textDecoration: "none",
          }}
        >
          <EditOutlinedIcon style={{ marginRight: "12px" }} />
          Edit
        </Link>
      </div>
      {loading && <Loading></Loading>}
      {!loading && <DetailTable {...data} />}
    </div>
  );
};
