import AddIcon from "@mui/icons-material/Add";
import { Box, Button, styled, TextField, Typography } from "@mui/material";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import React from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import Loading from "../../../components/Loading";
import { EncounterModel } from "../../../models/encounter";
import { encounterService } from "../../../services/agent";
import { StyledArrowBack } from "../style";

export const EncounterAdd: React.FC = () => {
  const navigate = useNavigate();
  const [searchParams, setSearchParams] = useSearchParams();
  const id = searchParams.get("id");
  const [loading, setLoading] = React.useState<boolean>(id ? true : false);

  const [startPeriod, setStartPeriod] = React.useState<string>(
    new Date().toISOString().replace(/.\d+Z$/g, "Z")
  );
  const [endPeriod, setEndPeriod] = React.useState<string>(
    new Date().toISOString().replace(/.\d+Z$/g, "Z")
  );
  const [formValue, setFormValue] = React.useState<{ [key: string]: string }>({
    startPeriod: startPeriod,
    endPeriod: endPeriod,
  });

  //declare function
  const handleStartChange = (newValue: Date | null) => {
    setStartPeriod(newValue?.toISOString().replace(/.\d+Z$/g, "Z") as string);
    setFormValue({
      ...formValue,
      startPeriod: startPeriod as unknown as string,
    });
  };
  const handleEndChange = (newValue: Date | null) => {
    setEndPeriod(newValue?.toISOString().replace(/.\d+Z$/g, "Z") as string);
    setFormValue({
      ...formValue,
      endPeriod: endPeriod as unknown as string,
    });
  };
  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setFormValue({
      ...formValue,
      [event.currentTarget.name]: event.currentTarget.value,
    });
    console.log(formValue);
  };
  const handleSelectChange = (event: SelectChangeEvent) => {
    setFormValue({ ...formValue, [event.target.name]: event.target.value });
  };
  const handleSubmit = () => {
    //mapping value to submit
    const submitValue = {
      status: formValue?.status,
      identifier: [
        {
          use: "usual",
          type: {
            coding: [
              {
                system: "http://terminology.hl7.org/CodeSystem/v2-0203",
                version: "1.2",
                code: "MR",
                display: "IB",
                userSelected: true,
              },
            ],
            text: "THX",
          },
          system: "urn:oid:1.2.36.146.595.217.0.1",
          value: "12345",
          period: {
            start: startPeriod,
            end: endPeriod,
          },
        },
      ],
      type: [
        {
          coding: [
            {
              system: "http://snomed.info/sct",
              version: "1.2",
              code: "270427003",
              display: formValue?.typeDisplay,
              userSelected: true,
            },
          ],
        },
      ],
      priority: {
        coding: [
          {
            system: "http://snomed.info/sct",
            version: "1.2",
            code: "310361003",
            display: formValue?.priority,
            userSelected: true,
          },
        ],
      },
      subject: {
        reference: "Patient/f001",
        display: formValue?.subject,
      },
      length: {
        value: parseInt(formValue?.lengthValue),
        system: "http://unitsofmeasure.org",
        code: formValue?.unit,
      },
      reasonCode: [
        {
          coding: [
            {
              system: "http://snomed.info/sct",
              version: "1.2",
              code: "34068001",
              display: formValue?.reasonDisplay,
              userSelected: true,
            },
          ],
        },
      ],
      hospitalization: {
        preAdmissionIdentifier: {
          use: "official",
          system: "http://www.amc.nl/zorgportal/identifiers/pre-admissions",
          value: "93042",
        },
        admitSource: {
          coding: [
            {
              system: "http://snomed.info/sct",
              version: "1.2",
              code: "305956004",
              display: formValue?.admitSource,
              userSelected: true,
            },
          ],
        },
        dischargeDisposition: {
          coding: [
            {
              system: "http://snomed.info/sct",
              version: "1.2",
              code: "306689006",
              display: formValue?.dischargeDisposition,
              userSelected: true,
            },
          ],
        },
      },
      serviceProvider: {
        reference: "Organization/f001",
        display: formValue?.serviceProvider,
      },
    } as EncounterModel;

    if (formValue?.subject) {
      setLoading(true);
      if (id)
        encounterService
          .update({ body: submitValue }, id)
          .then(() => navigate(`/admin/patient/details?id=${id}`, { state: 9 }))
          .catch(() => setLoading(false));
      else
        encounterService
          .post({ body: submitValue })
          .then(() => navigate(`/admin/patient/details?id=${id}`, { state: 9 }))
          .catch(() => setLoading(false));
    }
  };

  //styling
  const style = {
    "& .MuiOutlinedInput-root": {
      height: "100%",
      "& fieldset": {
        border: "1px solid #EAEAEA",
        borderRadius: "0.8rem",
        padding: 0,
        margin: 0,
      },
      "&:hover fieldset": {
        border: "1px solid #dbd7d7",
      },
      "&.Mui-focused fieldset": {
        border: "1px solid #EAEAEA",
      },
    },
  };
  const StyledButton = styled(Button)(({}) => ({
    margin: "0",
    color: "#FFFFFF",
    fontWeight: "700",
    fontSize: "1rem",
    lineHeight: "1.2rem",
    backgroundColor: "#6C5DD3",
    borderRadius: "6rem",
    padding: "10px 16px",
    whiteSpace: "nowrap",
    "&:hover": {
      backgroundColor: "#887ae6",
    },
  }));
  //Field header Mapping
  const encounterDetailsFields = [
    {
      name: "subject",
      value: formValue?.subject,
      label: "Patient Name",
      required: true,
    },
    {
      name: "typeDisplay",
      value: formValue?.typeDisplay,
      label: "Type",
      required: false,
    },
    {
      name: "lengthValue",
      value: formValue?.lengthValue,
      label: "Length Value",
      required: false,
    },
  ];
  const coverageFields = [
    {
      name: "priority",
      value: formValue?.priority,
      label: "Priority",
      required: false,
    },
    {
      name: "reasonDisplay",
      value: formValue?.reasonDisplay,
      label: "Reason",
      required: false,
    },
    {
      name: "admitSource",
      value: formValue?.admitSource,
      label: "Admit Source",
      required: false,
    },
    {
      name: "dischargeDisposition",
      value: formValue?.dischargeDisposition,
      label: "Discharge Disposition",
      required: false,
    },
    {
      name: "serviceProvider",
      value: formValue?.serviceProvider,
      label: "Service Provider",
      required: false,
    },
  ];
  const Status = [
    { value: "finished", display: "Finished" },
    { value: "ongoing", display: "Ongoing" },
  ];
  const LengthUnit = [
    { value: "min", display: "minutes" },
    { value: "h", display: "hours" },
    { value: "d", display: "days" },
  ];

  React.useEffect(() => {
    if (id)
      encounterService
        .getById(id)
        .then((res) => {
          setFormValue({
            subject: res.subject ? (res.subject.display as string) : "",
            status: "finished",
          });
          setLoading(false);
        })
        .catch(() => history.back());
  }, [id]);

  return (
    <Box>
      {/* Header */}
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <Typography
          sx={{
            fontWeight: 700,
            fontSize: 24,
            display: "flex",
            alignItems: "center",
            gap: "16px",
          }}
        >
          <StyledArrowBack onClick={() => history.back()} />
          {id ? "UPDATE ENCOUNTER" : "ADD ENCOUNTER"}
        </Typography>
        <StyledButton onClick={handleSubmit}>
          {searchParams.get("id") ? (
            <>Save changes</>
          ) : (
            <>
              {" "}
              Add <AddIcon style={{ marginLeft: "10px" }} />
            </>
          )}
        </StyledButton>
      </Box>
      {/* Body */}
      {loading && <Loading />}
      {!loading && (
        <Box
          style={{
            display: "flex",
            justifyContent: "space-between",
            background: "white",
            borderRadius: "16px",
            margin: "16px 0",
            padding: "16px",
          }}
        >
          {/* Left Col */}
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              gap: "16px",
              width: "49%",
            }}
          >
            <Typography sx={{ fontWeight: 600, fontSize: 20 }}>
              General Information
            </Typography>
            {encounterDetailsFields.map((item) => (
              <>
                <TextField
                  name={item.name}
                  value={item.value}
                  onChange={handleInputChange}
                  label={item.label}
                  required={item.required}
                  error={item.required ? !item.value : false}
                  sx={style}
                />
              </>
            ))}

            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                "& .MuiFormControl-root": {
                  flexBasis: "100%",
                },
                ...style,
              }}
            >
              <FormControl
                sx={{
                  marginRight: "10px",
                }}
              >
                <InputLabel id="lengthUnit-select-label">
                  Length Unit
                </InputLabel>
                <Select
                  labelId="lengthUnit-select-label"
                  id="lengthUnit-select"
                  value={formValue?.unit}
                  label="Length Unit"
                  name="unit"
                  onChange={handleSelectChange}
                  required
                  error={!formValue?.unit}
                >
                  {LengthUnit.map(({ value, display }) => (
                    <MenuItem value={value}>{display}</MenuItem>
                  ))}
                </Select>
              </FormControl>
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DesktopDatePicker
                  label="Valid From"
                  inputFormat="MM/dd/yyyy"
                  value={new Date(startPeriod)}
                  onChange={handleStartChange}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      required
                      error={startPeriod ? startPeriod >= endPeriod : true}
                    />
                  )}
                />
              </LocalizationProvider>
            </Box>
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                "& .MuiFormControl-root": {
                  flexBasis: "100%",
                },
                ...style,
              }}
            >
              <FormControl>
                <InputLabel id="status-select-label">Status</InputLabel>
                <Select
                  labelId="status-select-label"
                  id="status-select"
                  value={formValue?.status}
                  label="Status"
                  name="status"
                  onChange={handleSelectChange}
                  required
                  error={!formValue?.status}
                >
                  {Status.map(({ value, display }) => (
                    <MenuItem value={value}>{display}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Box>
          </Box>
          {/* Right Col*/}
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              gap: "16px",
              width: "49%",
            }}
          >
            <Typography sx={{ fontWeight: 600, fontSize: 20 }}>
              More Information
            </Typography>
            {coverageFields.map((item) => (
              <TextField
                name={item.name}
                value={item.value}
                onChange={handleInputChange}
                label={item.label}
                required={item.required}
                error={item.required ? !item.value : false}
                sx={style}
              />
            ))}
          </Box>
        </Box>
      )}
    </Box>
  );
};
