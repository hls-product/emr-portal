import { Typography } from "@mui/material";
import React, { useState } from "react";
import { Link, useNavigate, useSearchParams } from "react-router-dom";
import Loading from "../../../components/Loading";
import { patientService } from "../../../services/agent";
import {
  StyledActivityBulletPoint,
  StyledActivityContainer,
  StyledActivityMoreIcon,
  StyledActivityTitleLeft,
  StyledActivityTitleRight,
  StyledArrowDownIcon,
  StyledBodyMainContainer,
  StyledBodySectionTitle,
  StyledBreakLine,
  StyledDescriptionLineBullet,
  StyledInvisibleBreakLine,
  StyledLeftBodyChildContainer,
  StyledLeftSideProfileRowContainer,
  StyledProfileBloodTypeIcon,
  StyledProfileChip,
  StyledProfileDescriptionIcon,
  StyledProfileEditIcon,
  StyledProfileGenderIcon,
  StyledProfileMailIcon,
  StyledProfileNameIcon,
  StyledProfilePhoneIcon,
  StyledProfileRowContainer,
  StyledProfileWarningIcon,
  StyledRightBodyChildContainer,
  StyledRightSideProfileRowContainer,
  StyledSingleActivityConnectionLine,
  StyledSingleActivityContainer,
  StyledSingleActivityDescription,
  StyledSingleActivityDescriptionRow,
  StyledSingleActivityTitleRow,
  StyledSubBodySectionTitle,
  StyledViewMoreActivity,
} from "../style";

export const Details: React.FC = () => {
  const [loading, setLoading] = React.useState<boolean>(true);
  const [searchParams, setSearchParams] = useSearchParams();
  const id = searchParams.get("id");
  const [patientName, setPatientName] = useState("");
  const [patientGender, setPatientGender] = useState("");
  const [patientPhoneNumber, setPatientPhoneNumber] = useState("");
  const [patientEmail, setPatientEmail] = useState("");
  const [patientBloodgroup, setPatientBloodgroup] = useState("AB");
  const [patientAllergies, setPatientAllergies] = useState([
    "Bird",
    "Cat",
    "Dog",
    "Rat",
  ]);
  const [patientAnamnesis, setPatientAnamnesis] = useState([
    "Bird",
    "Cat",
    "Dog",
    "Rat",
  ]);

  React.useEffect(() => {
    if (!id) throw history.back();
    patientService
      .getById(id)
      .then((res) => {
        setPatientName(() => {
          const name = res.name?.find((item) => item.use === "official");
          return name?.text || name?.given?.join(" ") + " " + name?.family;
        });
        setPatientGender(res.gender as string);
        setPatientPhoneNumber(
          res.telecom
            ?.filter((item) => item.system === "phone" && item.value)
            ?.map((item) => item.value)
            ?.join("-") as string
        );
        setPatientEmail(
          res.telecom
            ?.filter((item) => item.system === "phone" && item.value)
            ?.map((item) => item.value)
            ?.join("-") as string
        );
        setLoading(false);
      })
      .catch(() => history.back());
  }, [id]);

  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        <StyledBodyMainContainer>
          {/* Left Section: Profile */}
          <StyledLeftBodyChildContainer>
            <StyledBodySectionTitle>
              PROFILE
              <Link to={`/admin/patient/details/edit?id=${id}`}>
                <StyledProfileEditIcon />
              </Link>
            </StyledBodySectionTitle>

            <StyledBreakLine />

            <StyledSubBodySectionTitle>ABOUT</StyledSubBodySectionTitle>
            <StyledProfileRowContainer>
              <StyledLeftSideProfileRowContainer>
                <StyledProfileNameIcon />
                Patient Name
              </StyledLeftSideProfileRowContainer>
              <StyledRightSideProfileRowContainer>
                {patientName}
              </StyledRightSideProfileRowContainer>
            </StyledProfileRowContainer>
            <StyledBreakLine marginLeftProp="7%" />
            <StyledProfileRowContainer>
              <StyledLeftSideProfileRowContainer>
                <StyledProfileGenderIcon />
                Gender
              </StyledLeftSideProfileRowContainer>
              <StyledRightSideProfileRowContainer>
                <Typography
                  textTransform="capitalize"
                  children={patientGender}
                />
              </StyledRightSideProfileRowContainer>
            </StyledProfileRowContainer>
            <StyledBreakLine marginLeftProp="7%" />
            <StyledProfileRowContainer>
              <StyledLeftSideProfileRowContainer>
                <StyledProfilePhoneIcon />
                Phone Number
              </StyledLeftSideProfileRowContainer>
              <StyledRightSideProfileRowContainer>
                {patientPhoneNumber}
              </StyledRightSideProfileRowContainer>
            </StyledProfileRowContainer>
            <StyledBreakLine marginLeftProp="7%" />
            <StyledProfileRowContainer>
              <StyledLeftSideProfileRowContainer>
                <StyledProfileMailIcon />
                Email
              </StyledLeftSideProfileRowContainer>
              <StyledRightSideProfileRowContainer>
                {patientEmail}
              </StyledRightSideProfileRowContainer>
            </StyledProfileRowContainer>
            <StyledInvisibleBreakLine />
            <StyledSubBodySectionTitle>
              HEALTH INFORMATION
            </StyledSubBodySectionTitle>
            <StyledProfileRowContainer>
              <StyledLeftSideProfileRowContainer>
                <StyledProfileBloodTypeIcon />
                Blood Group
              </StyledLeftSideProfileRowContainer>
              <StyledRightSideProfileRowContainer>
                {patientBloodgroup}
              </StyledRightSideProfileRowContainer>
            </StyledProfileRowContainer>
            <StyledBreakLine marginLeftProp="7%" />
            <StyledProfileRowContainer>
              <StyledLeftSideProfileRowContainer>
                <StyledProfileWarningIcon />
                Allergies
              </StyledLeftSideProfileRowContainer>
            </StyledProfileRowContainer>
            <StyledProfileRowContainer paddingLeftProp="7%">
              <StyledLeftSideProfileRowContainer>
                {patientAllergies.map((item) => {
                  return <StyledProfileChip>{item}</StyledProfileChip>;
                })}
              </StyledLeftSideProfileRowContainer>
            </StyledProfileRowContainer>
            <StyledBreakLine marginLeftProp="7%" />
            <StyledProfileRowContainer>
              <StyledLeftSideProfileRowContainer>
                <StyledProfileDescriptionIcon />
                Anamnesis
              </StyledLeftSideProfileRowContainer>
            </StyledProfileRowContainer>
            <StyledProfileRowContainer paddingLeftProp="7%">
              <StyledLeftSideProfileRowContainer>
                {patientAnamnesis.map((item) => {
                  return <StyledProfileChip>{item}</StyledProfileChip>;
                })}
              </StyledLeftSideProfileRowContainer>
            </StyledProfileRowContainer>
          </StyledLeftBodyChildContainer>
          {/* End of Left Section: Profile */}
          {/* Right Section: Activity */}
          <StyledRightBodyChildContainer>
            <StyledActivityContainer>
              <StyledBodySectionTitle>
                ACTIVITY
                <StyledActivityMoreIcon />
              </StyledBodySectionTitle>
              <StyledBreakLine />
              <StyledSingleActivityContainer>
                <StyledSingleActivityTitleRow>
                  <StyledActivityTitleLeft>
                    <StyledActivityBulletPoint />
                    TITLE
                  </StyledActivityTitleLeft>
                  <StyledActivityTitleRight>
                    20th July, 2022 10:30:11
                  </StyledActivityTitleRight>
                </StyledSingleActivityTitleRow>

                <StyledSingleActivityDescriptionRow>
                  <StyledSingleActivityConnectionLine />
                  <StyledSingleActivityDescription>
                    <StyledDescriptionLineBullet /> This is a short description
                    of this single activity ...
                  </StyledSingleActivityDescription>
                </StyledSingleActivityDescriptionRow>
              </StyledSingleActivityContainer>
              <StyledSingleActivityContainer>
                <StyledSingleActivityTitleRow>
                  <StyledActivityTitleLeft>
                    <StyledActivityBulletPoint />
                    TITLE
                  </StyledActivityTitleLeft>
                  <StyledActivityTitleRight>
                    20th July, 2022 10:30:11
                  </StyledActivityTitleRight>
                </StyledSingleActivityTitleRow>
                <StyledSingleActivityDescriptionRow>
                  <StyledSingleActivityConnectionLine />
                  <StyledSingleActivityDescription>
                    <StyledDescriptionLineBullet /> This is a short description
                    of this single activity ...
                  </StyledSingleActivityDescription>
                </StyledSingleActivityDescriptionRow>
              </StyledSingleActivityContainer>
              <StyledSingleActivityContainer>
                <StyledSingleActivityTitleRow>
                  <StyledActivityTitleLeft>
                    <StyledActivityBulletPoint />
                    TITLE
                  </StyledActivityTitleLeft>
                  <StyledActivityTitleRight>
                    20th July, 2022 10:30:11
                  </StyledActivityTitleRight>
                </StyledSingleActivityTitleRow>
                <StyledSingleActivityDescriptionRow>
                  <StyledSingleActivityConnectionLine />
                  <StyledSingleActivityDescription>
                    <StyledDescriptionLineBullet /> This is a short description
                    of this single activity ...
                  </StyledSingleActivityDescription>
                </StyledSingleActivityDescriptionRow>
              </StyledSingleActivityContainer>
              <StyledSingleActivityContainer>
                <StyledSingleActivityTitleRow>
                  <StyledActivityTitleLeft>
                    <StyledActivityBulletPoint />
                    TITLE
                  </StyledActivityTitleLeft>
                  <StyledActivityTitleRight>
                    20th July, 2022 10:30:11
                  </StyledActivityTitleRight>
                </StyledSingleActivityTitleRow>
                <StyledSingleActivityDescriptionRow>
                  <StyledSingleActivityConnectionLine />
                  <StyledSingleActivityDescription>
                    <StyledDescriptionLineBullet /> This is a short description
                    of this single activity ...
                  </StyledSingleActivityDescription>
                </StyledSingleActivityDescriptionRow>
              </StyledSingleActivityContainer>
              <StyledSingleActivityContainer>
                <StyledSingleActivityTitleRow>
                  <StyledActivityTitleLeft>
                    <StyledActivityBulletPoint />
                    TITLE
                  </StyledActivityTitleLeft>
                  <StyledActivityTitleRight>
                    20th July, 2022 10:30:11
                  </StyledActivityTitleRight>
                </StyledSingleActivityTitleRow>
                <StyledSingleActivityDescriptionRow>
                  <StyledSingleActivityConnectionLine />
                  <StyledSingleActivityDescription>
                    <StyledDescriptionLineBullet /> This is a short description
                    of this single activity ...
                  </StyledSingleActivityDescription>
                </StyledSingleActivityDescriptionRow>
              </StyledSingleActivityContainer>
              <StyledSingleActivityContainer>
                <StyledSingleActivityTitleRow>
                  <StyledActivityTitleLeft>
                    <StyledActivityBulletPoint />
                    TITLE
                  </StyledActivityTitleLeft>
                  <StyledActivityTitleRight>
                    20th July, 2022 10:30:11
                  </StyledActivityTitleRight>
                </StyledSingleActivityTitleRow>
                <StyledSingleActivityDescriptionRow>
                  <StyledSingleActivityConnectionLine />
                  <StyledSingleActivityDescription>
                    <StyledDescriptionLineBullet /> This is a short description
                    of this single activity ...
                  </StyledSingleActivityDescription>
                </StyledSingleActivityDescriptionRow>
              </StyledSingleActivityContainer>
              <StyledViewMoreActivity>
                View more
                <StyledArrowDownIcon />
              </StyledViewMoreActivity>
            </StyledActivityContainer>
          </StyledRightBodyChildContainer>
          {/* End of right section: Activity */}
        </StyledBodyMainContainer>
      )}
    </>
  );
};
