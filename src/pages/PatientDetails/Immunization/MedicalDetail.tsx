import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import { IconButton } from "@mui/material";
import { styled } from "@mui/system";
import React from "react";
import { Link, useSearchParams } from "react-router-dom";
import Loading from "../../../components/Loading";
import { immunizationService } from "../../../services/agent";

interface TableProps {
  vaccine: string;
  date: string;
  time: string;
  lotNumber: string;
  site: string;
  route: string;
  status: any;
  doseQuantity: number;
  vaccineCode: string;
  id: string;
  note: string | null;
}

const Table = styled("table")({
  background: "white",
  flex: 1,
  padding: "8px 16px",
  borderRadius: "16px",
  "& tr": {
    lineHeight: "22px",
  },
  "& th": {
    padding: "8px 0",
    textAlign: "left",
    lineHeight: "22px",
  },
  "& td": {
    padding: "8px 0",
    textAlign: "right",
  },
});

const DetailTable: React.FC<TableProps> = ({ ...props }) => (
  <div
    style={{
      display: "flex",
      justifyContent: "space-between",
      gap: "20px",
    }}
  >
    <Table>
      <tr>
        <th>Vaccine</th>
        <td>{props.vaccine}</td>
      </tr>
      <tr>
        <th>Vaccine Code</th>
        <td>{props.vaccineCode}</td>
      </tr>
      <tr>
        <th>Occurrence Date</th>
        <td>{props.date}</td>
      </tr>
      <tr>
        <th>Expiration Date</th>
        <td>{props.time}</td>
      </tr>
      <tr>
        <th>Lot Number</th>
        <td>{props.lotNumber}</td>
      </tr>
      <tr>
        <th>Site</th>
        <td>{props.site}</td>
      </tr>
    </Table>
    <Table>
      <tr>
        <th>Route</th>
        <td>{props.route}</td>
      </tr>
      <tr>
        <th>Status</th>
        <td>
          <div
            style={{
              padding: "3px 12px",
              border: "1px solid rgba(255, 115, 0, 1)",
              color: "rgba(255, 115, 0, 1)",
              borderRadius: "21px",
              width: "fit-content",
              float: "right",
            }}
          >
            {props.status}
          </div>
        </td>
      </tr>
      <tr>
        <th>Dose Quantity</th>
        <td>{props.doseQuantity} mg</td>
      </tr>
      <tr>
        <th>Record ID</th>
        <td>{props.id}</td>
      </tr>
      <tr>
        <th>Note</th>
        <td>{props.note}</td>
      </tr>
    </Table>
  </div>
);

export const MedicalDetail: React.FC = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const id = searchParams.get("id");
  const [loading, setLoading] = React.useState<boolean>(true);
  const [data, setData] = React.useState<TableProps>({
    vaccine: "",
    date: "",
    time: "",
    lotNumber: "",
    site: "",
    route: "",
    status: "",
    doseQuantity: 0,
    vaccineCode: "",
    id: "",
    note: "",
  });

  React.useEffect(() => {
    if (!id) throw history.back();
    immunizationService.getById(id).then((res) => {
      setData(() => {
        const newData = {
          vaccine: res ? (res.vaccineCode?.text as string) : "",
          date: res ? (res.occurrenceDateTime?.slice(0, 10) as string) : "",
          time: res ? (res.expirationDate as string) : "",
          lotNumber: res ? (res.lotNumber as string) : "",
          site: res.site
            ? res.site.coding
              ? (res.site.coding[0].display as string)
              : ""
            : "",
          route: res.route
            ? res.route.coding
              ? (res.route.coding[0].display as string)
              : ""
            : "",
          status: res.status
            ? ((res.status[0].toUpperCase() +
                res.status.substring(1)) as string)
            : "",
          doseQuantity: res ? (res.doseQuantity?.value as number) : "",
          vaccineCode: res.vaccineCode
            ? res.vaccineCode.coding
              ? (res.vaccineCode?.coding[0].code as string)
              : ""
            : "",
          id: res ? (res.id as string) : "",
          note: res.note ? (res.note[0].text as string) : "",
        } as TableProps;
        return newData;
      });
      setLoading(false);
    });
  }, [id]);

  return (
    <div>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <div
          style={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <Link to={`/admin/patient/details?id=${data.id}`} state={3}>
            <IconButton
              style={{
                color: "rgba(108, 93, 211, 1)",
                background: "white",
                borderRadius: "50%",
                width: "40px",
                height: "40px",
                marginRight: "16px",
              }}
            >
              <ArrowBackIcon />
            </IconButton>
          </Link>
          <h1 style={{ fontSize: "24px" }}>Immunization Details</h1>
        </div>
        <Link
          to={`update?id=${data.id}`}
          style={{
            padding: "10px 30px",
            borderRadius: "30px",
            color: "rgba(108, 93, 211, 1)",
            background: "white",
            textDecoration: "none",
          }}
        >
          <EditOutlinedIcon style={{ marginRight: "12px" }} />
          Edit
        </Link>
      </div>
      {loading && <Loading></Loading>}
      {!loading && <DetailTable {...data} />}
    </div>
  );
};
