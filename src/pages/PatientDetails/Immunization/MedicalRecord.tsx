import { Add, Create, Search, Visibility } from "@mui/icons-material";
import {
  Box,
  IconButton,
  InputAdornment,
  TextField,
  Typography,
} from "@mui/material";
import React from "react";
import { Link, useNavigate } from "react-router-dom";
import { RoundedButton } from "../../../components/RoundedButton";

import Delete from "@mui/icons-material/Delete";
import { DeleteDialog } from "../../../components/DeleteDialog";
import { AdvancedSearch } from "../../../components/Table/AdvancedSearch";
import { CustomTable } from "../../../components/Table/Table";
import { HeadCell } from "../../../components/Table/types";
import { immunizationModel } from "../../../models/immunization";
import { immunizationService } from "../../../services/agent";
import { colorPalette } from "../../../theme";

const headCells: HeadCell[] = [
  {
    id: "number",
    numeric: false,
    label: "No.",
  },
  {
    id: "vaccine",
    numeric: false,
    label: "Vaccine",
  },
  {
    id: "expiredDate",
    numeric: false,
    label: "Expired Date",
  },
  {
    id: "lotNumber",
    numeric: false,
    label: "Lot Number",
  },
  {
    id: "doseQuantity",
    numeric: false,
    label: "Issuer",
  },
  {
    id: "notes",
    numeric: false,
    label: "Notes",
  },
  {
    id: "status",
    numeric: false,
    label: "Status",
  },
  {
    id: "action",
    numeric: false,
    label: "Action",
  },
];
const advancedSearchProps = [
  {
    name: "status",
    label: "Status",
    select: true,
    menuItems: [
      {
        value: "completed",
        display: "Completed",
      },
      {
        value: "entered-in-error",
        display: "Enter-in-Error",
      },
      {
        value: "not-done",
        display: "Not Done",
      },
    ],
  },
  {
    name: "period",
    dateTime: "period",
    inputFormat: "YYYY",
  },
];

export const Immunization: React.FC = () => {
  const navigate = useNavigate();
  // useState
  const [isLoading, setIsLoading] = React.useState(false);
  const [data, setData] = React.useState<immunizationModel[]>([]);
  const [page, setPage] = React.useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = React.useState<number>(10);
  const [rowsCount, setRowsCount] = React.useState<number>(0);
  const [deletePopUpOpen, setDeletePopUpOpen] = React.useState(false);
  const [openAdvancedSearch, setOpenAdvancedSearch] = React.useState(false);
  const [currentSelectedId, setCurrentSelectedId] = React.useState("");

  // Search states
  const [searchValue, setSearchValue] = React.useState<string>("");
  const [debounceSeachValue, setDebounceSearchValue] =
    React.useState<string>("");
  const [filter, setFilter] = React.useState<{ [key: string]: string }>({});

  const getImmunizationList = async (filter?: { [keys: string]: string }) => {
    const params = {
      ...filter,
      page: page + 1,
      size: rowsPerPage,
      name: debounceSeachValue,
    };
    setIsLoading(true);
    try {
      let { data, paging } = await immunizationService.getList({ params });
      setData(data);
      setRowsCount(paging.total);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      console.log("getImmunizationList error: ", error);
    }
  };
  const deleteImmunization = async (id: string) => {
    setIsLoading(true);
    try {
      let response = await immunizationService.delete(id);
      setIsLoading(false);
      return response;
    } catch (error) {
      setIsLoading(false);
      console.log("deleteImmunization error: ", error);
    }
  };

  // Event Handler
  const handleChangePage = (event: any, newPage: number) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event: any) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleDelete = (id: string) => {
    setDeletePopUpOpen(true);
    setCurrentSelectedId(id);
  };
  const handleCancelDelete = () => {
    setDeletePopUpOpen(false);
  };
  const handleConfirmDelete = () => {
    setDeletePopUpOpen(false);
    deleteImmunization(currentSelectedId).then((res) => {
      setCurrentSelectedId("");
      getImmunizationList();
    });
  };
  const handleClickAdvancedSearchOpen = () => {
    setOpenAdvancedSearch(true);
  };
  //Debounce for name search
  React.useEffect(() => {
    let timeout = setTimeout(() => {
      setDebounceSearchValue(searchValue);
    }, 500);
    return () => {
      clearTimeout(timeout);
    };
  }, [searchValue]);

  // Back to previous page if current page is empty after deleting an item
  React.useEffect(() => {
    let totalPages = Math.ceil(rowsCount / rowsPerPage);
    let currentPage = page + 1;
    if (currentPage > totalPages && totalPages > 0) {
      setPage(totalPages - 1);
    }
  }, [rowsCount]);
  React.useEffect(() => {
    getImmunizationList();
  }, [debounceSeachValue, rowsPerPage, page]);

  const StatusCircle = (status: string) => {
    const theme = {
      completed: colorPalette.purple.shade_500,
      "not-done": colorPalette.orange.shade_500,
      "entered-in-error": colorPalette.red.shade_500,
    };
    return (
      <div
        style={{
          height: 6,
          width: 6,
          borderRadius: "50%",
          background: theme[status as keyof typeof theme] as string,
        }}
      />
    );
  };

  const mapDataToRender = () => {
    const render = data.map((item, index) => ({
      id: item.id as string,
      number: index + 1,
      vaccine: item.vaccineCode?.text,
      expiredDate: item.expirationDate,
      lotNumber: item.lotNumber,
      doseQuantity: ((item.doseQuantity?.value ? item.doseQuantity?.value : 0) +
        " " +
        item.doseQuantity?.code) as string,
      notes: item.note?.[0].text,
      status: (
        <Box display="flex" alignItems="center" gap="8px">
          {StatusCircle(item.status as string)}
          <Typography textTransform="capitalize" children={item.status} />
        </Box>
      ),
      action: (
        <Box style={{ textAlign: "center", whiteSpace: "nowrap" }}>
          <Link
            to={`immunization/update?id=${item.id}`}
            children={<IconButton children={<Create />} />}
          />
          <Link
            to={`immunization?id=${item.id}`}
            children={<IconButton children={<Visibility />} />}
          />

          <Link
            to={`#`}
            onClick={() => {
              handleDelete(item.id as string);
            }}
            children={<IconButton children={<Delete />} />}
          />
        </Box>
      ),
    }));
    return render;
  };

  return (
    <>
      {/* Dialog for delete item popup */}
      <DeleteDialog
        open={deletePopUpOpen}
        onClose={handleCancelDelete}
        onConfirm={handleConfirmDelete}
        onCancel={handleCancelDelete}
      />
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          width: "100%",
          padding: "20px",
          backgroundColor: "white",
          borderRadius: "16px",
        }}
      >
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            gap: "8px",
          }}
        >
          <Typography variant="h2" children={"IMMUNIZATION"} />
          <Box sx={{ display: "flex", gap: "8px" }}>
            <TextField
              placeholder="Search vaccine name..."
              InputProps={{
                startAdornment: (
                  <InputAdornment
                    position="start"
                    children={<Search sx={{ height: 20 }} />}
                  />
                ),
              }}
              sx={{
                "& .MuiInputBase-root": {
                  height: 48,
                  width: 331,
                  borderRadius: 3,
                },
              }}
              value={searchValue}
              onChange={(e) => {
                setSearchValue(e.target.value);
              }}
            />
            <RoundedButton
              variant="contained"
              onClick={handleClickAdvancedSearchOpen}
              children="Advanced Search"
            />
            <RoundedButton
              variant="contained"
              endIcon={<Add />}
              onClick={() => {
                navigate("immunization/add");
              }}
              children="Add New"
            />
          </Box>
        </Box>
        {openAdvancedSearch && (
          <AdvancedSearch
            props={advancedSearchProps}
            filter={filter}
            description="Find immunization plans with..."
            onFilterChange={setFilter}
            onApply={getImmunizationList}
            onClose={setOpenAdvancedSearch}
          />
        )}
        <CustomTable
          title={"INSURANCE LIST"}
          headcells={headCells}
          body={mapDataToRender()}
          paging={{
            page: page,
            rowsPerPage: rowsPerPage,
            rowsCount: rowsCount,
          }}
          loading={isLoading}
          toolbar={false}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Box>
    </>
  );
};
