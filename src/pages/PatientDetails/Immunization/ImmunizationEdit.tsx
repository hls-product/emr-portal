import AddIcon from "@mui/icons-material/Add";
import { Box, Button, styled, TextField, Typography } from "@mui/material";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { DesktopDatePicker } from "@mui/x-date-pickers/DesktopDatePicker";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import React from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import Loading from "../../../components/Loading";
import { immunizationModel } from "../../../models/immunization";
import { immunizationService } from "../../../services/agent";
import { StyledArrowBack } from "../style";

export const ImmunizationEdit: React.FC = () => {
  const navigate = useNavigate();
  const [searchParams, setSearchParams] = useSearchParams();
  const id = searchParams.get("id");
  const [loading, setLoading] = React.useState<boolean>(id ? true : false);

  const [startPeriod, setStartPeriod] = React.useState<string>(
    new Date().toISOString().replace(/.\d+Z$/g, "Z")
  );
  const [endPeriod, setEndPeriod] = React.useState<string>(
    new Date().toISOString().replace(/.\d+Z$/g, "Z")
  );
  const [formValue, setFormValue] = React.useState<{ [key: string]: string }>({
    startPeriod: startPeriod,
    endPeriod: endPeriod,
  });

  //declare function
  const handleStartChange = (newValue: Date | null) => {
    setStartPeriod(newValue?.toISOString().replace(/.\d+Z$/g, "Z") as string);
    setFormValue({
      ...formValue,
      startPeriod: startPeriod as unknown as string,
    });
  };
  const handleEndChange = (newValue: Date | null) => {
    setEndPeriod(newValue?.toISOString().replace(/.\d+Z$/g, "Z") as string);
    setFormValue({
      ...formValue,
      endPeriod: endPeriod as unknown as string,
    });
  };
  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setFormValue({
      ...formValue,
      [event.currentTarget.name]: event.currentTarget.value,
    });
    console.log(formValue);
  };
  const handleSelectChange = (event: SelectChangeEvent) => {
    setFormValue({ ...formValue, [event.target.name]: event.target.value });
  };
  const handleSubmit = () => {
    //mapping value to submit
    const submitValue = {
      status: formValue?.status,
      vaccineCode: {
        coding: [
          {
            system: "urn:oid:1.2.36.1.2001.1005.17",
            code: formValue?.vaccineCode,
          },
        ],
        text: formValue?.vaccine,
      },
      patient: {
        reference: "Patient/example",
      },
      encounter: {
        reference: "Encounter/example",
      },
      occurrenceDateTime: startPeriod,
      primarySource: true,
      location: {
        reference: "Location/1",
      },
      manufacturer: {
        reference: "Organization/h7",
      },
      lotNumber: formValue?.lotNumber,
      expirationDate: endPeriod,
      site: {
        coding: [
          {
            system: "http://terminology.hl7.org/CodeSystem/v3-ActSite",
            code: "LA",
            display: formValue?.site,
          },
        ],
      },

      route: {
        coding: [
          {
            system:
              "http://terminology.hl7.org/CodeSystem/v3-RouteOfAdministration",
            code: "IM",
            display: formValue?.route,
          },
        ],
      },

      doseQuantity: {
        value: parseInt(formValue?.doseQuantity),
        sytem: "http://unitsofmeasure.org",
        code: "mg",
      },

      note: [
        {
          text: formValue?.note,
        },
      ],
    } as immunizationModel;
    setLoading(true);
    if (
      formValue?.vaccineCode &&
      formValue?.vaccine &&
      formValue?.status &&
      startPeriod &&
      endPeriod
    ) {
      if (id) {
        immunizationService
          .update({ body: submitValue }, id)
          .then(() => navigate(`/admin/patient/details?id=${id}`, { state: 3 }))
          .catch(() => setLoading(false));
      } else {
        immunizationService
          .post({ body: submitValue })
          .then(() => navigate(`/admin/patient/details?id=${id}`, { state: 3 }))
          .catch(() => setLoading(false));
      }
    }
  };

  //styling
  const style = {
    "& .MuiOutlinedInput-root": {
      height: "100%",
      "& fieldset": {
        border: "1px solid #EAEAEA",
        borderRadius: "0.8rem",
        padding: 0,
        margin: 0,
      },
      "&:hover fieldset": {
        border: "1px solid #dbd7d7",
      },
      "&.Mui-focused fieldset": {
        border: "1px solid #EAEAEA",
      },
    },
  };
  const StyledButton = styled(Button)(({}) => ({
    margin: "0",
    color: "#FFFFFF",
    fontWeight: "700",
    fontSize: "1rem",
    lineHeight: "1.2rem",
    backgroundColor: "#6C5DD3",
    borderRadius: "6rem",
    padding: "10px 16px",
    whiteSpace: "nowrap",
    "&:hover": {
      backgroundColor: "#887ae6",
    },
  }));
  //Field header Mapping
  const immunizationDetailsFields = [
    {
      name: "vaccineCode",
      value: formValue?.vaccineCode,
      label: "Vaccine Code",
      required: true,
    },
    {
      name: "vaccine",
      value: formValue?.vaccine,
      label: "Vaccine ",
      required: true,
    },
    {
      name: "lotNumber",
      value: formValue?.lotNumber,
      label: "Lot Number",
      required: true,
    },
  ];
  const coverageFields = [
    {
      name: "site",
      value: formValue?.site,
      label: "Site",
      required: false,
    },
    {
      name: "route",
      value: formValue?.route,
      label: "Route",
      required: false,
    },
    {
      name: "doseQuantity",
      value: formValue?.doseQuantity,
      label: "Dose Quantity",
      required: false,
    },

    {
      name: "note",
      value: formValue?.note,
      label: "Note",
      required: false,
    },
  ];
  const immunizationStatus = [
    { value: "uncompleted", display: "Uncompleted" },
    { value: "completed", display: "Completed" },
    { value: "pending", display: "Pending" },
  ];

  React.useEffect(() => {
    if (id) {
      immunizationService
        .getById(id)
        .then((res) => {
          setFormValue({
            status: "completed",
            vaccineCode: res.vaccineCode
              ? res.vaccineCode.coding
                ? (res.vaccineCode.coding[0].code as string)
                : ""
              : "",
            vaccine: res.vaccineCode ? (res.vaccineCode.text as string) : "",
            lotNumber: res.lotNumber ? (res.lotNumber as string) : "",
          });
          setStartPeriod(
            res.occurrenceDateTime
              ? res.occurrenceDateTime
              : new Date().toISOString().replace(/.\d+Z$/g, "Z")
          );
          setEndPeriod(
            res.expirationDate
              ? res.expirationDate
              : new Date().toISOString().replace(/.\d+Z$/g, "Z")
          );
          setLoading(false);
        })
        .catch(() => history.back());
    }
  }, [id]);

  return (
    <Box>
      {/* Header */}
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <Typography
          sx={{
            fontWeight: 700,
            fontSize: 24,
            display: "flex",
            alignItems: "center",
            gap: "16px",
          }}
        >
          <StyledArrowBack onClick={() => history.back()} />
          ADD IMMUNIZATIONS
        </Typography>
        <StyledButton onClick={handleSubmit}>
          {searchParams.get("id") ? (
            <>Save changes</>
          ) : (
            <>
              {" "}
              Add <AddIcon style={{ marginLeft: "10px" }} />
            </>
          )}
        </StyledButton>
      </Box>
      {/* Body */}
      {loading ? (
        <Loading />
      ) : (
        <Box
          style={{
            display: "flex",
            justifyContent: "space-between",
            background: "white",
            borderRadius: "16px",
            margin: "16px 0",
            padding: "16px",
          }}
        >
          {/* Left Col */}
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              gap: "16px",
              width: "49%",
            }}
          >
            <Typography sx={{ fontWeight: 600, fontSize: 20 }}>
              General Information
            </Typography>
            {immunizationDetailsFields.map((item) => (
              <>
                <TextField
                  name={item.name}
                  value={item.value}
                  onChange={handleInputChange}
                  label={item.label}
                  required={item.required}
                  error={item.required ? !item.value : false}
                  sx={style}
                />
              </>
            ))}
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                "& .MuiFormControl-root": {
                  flexBasis: "33%",
                },
                ...style,
              }}
            >
              <FormControl>
                <InputLabel id="status-select-label">Status</InputLabel>
                <Select
                  labelId="status-select-label"
                  id="status-select"
                  value={formValue?.status}
                  label="Status"
                  name="status"
                  onChange={handleSelectChange}
                  required
                  error={!formValue?.status}
                >
                  {immunizationStatus.map(({ value, display }) => (
                    <MenuItem value={value}>{display}</MenuItem>
                  ))}
                </Select>
              </FormControl>
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DesktopDatePicker
                  label="Valid From"
                  inputFormat="MM/dd/yyyy"
                  value={new Date(startPeriod)}
                  onChange={handleStartChange}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      required
                      error={startPeriod ? startPeriod >= endPeriod : true}
                    />
                  )}
                />
                <DesktopDatePicker
                  label="Good Thru"
                  inputFormat="MM/dd/yyyy"
                  value={new Date(endPeriod)}
                  onChange={handleEndChange}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      required
                      error={endPeriod ? endPeriod <= startPeriod : true}
                    />
                  )}
                />
              </LocalizationProvider>
            </Box>
          </Box>
          {/* Right Col*/}
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              gap: "16px",
              width: "49%",
            }}
          >
            <Typography sx={{ fontWeight: 600, fontSize: 20 }}>
              More Information
            </Typography>
            {coverageFields.map((item) => (
              <TextField
                name={item.name}
                value={item.value}
                onChange={handleInputChange}
                label={item.label}
                required={item.required}
                error={item.required ? !item.value : false}
                sx={style}
              />
            ))}
          </Box>
        </Box>
      )}
    </Box>
  );
};
