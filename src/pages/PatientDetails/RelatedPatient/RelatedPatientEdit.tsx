import React from "react";
import * as _ from "lodash";
import { Box, TextField, styled, Typography, Button } from "@mui/material";
import { useSelector } from "react-redux";
import { IRootState } from "../../../store/reducers";
import { useAppDispatch } from "../../../store/Store";
import { useNavigate, useSearchParams } from "react-router-dom";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import AddIcon from "@mui/icons-material/Add";
import Loading from "../../../components/Loading";
import { relatedPatientModel } from "../../../models/relatedPatient";
import { getRelatedPatientDataByID, postRelatedPatientData, updateRelatedPatientData } from "../../../store/reducers/relatedPatient/relatedPatient";
import { StyledArrowBack } from "../style";
import { relatedPatientService } from "../../../services/agent";

export const RelatedPatientEdit: React.FC = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const [searchParams, setSearchParams] = useSearchParams();
  const id = searchParams.get("id");
  const [loading, setLoading] = React.useState<boolean>(id? true : false);

  const [startPeriod, setStartPeriod] = React.useState<string>(
    new Date().toISOString().replace(/.\d+Z$/g, "Z")
  );
  const [endPeriod, setEndPeriod] = React.useState<string>(
    new Date().toISOString().replace(/.\d+Z$/g, "Z")
  );
  const [formValue, setFormValue] = React.useState<{ [key: string]: string }>({
    startPeriod: startPeriod,
    endPeriod: endPeriod,
  });

  //declare function
  const handleStartChange = (newValue: Date | null) => {
    setStartPeriod(newValue?.toISOString().replace(/.\d+Z$/g, "Z") as string);
    setFormValue({
      ...formValue,
      startPeriod: startPeriod as unknown as string,
    });
  };
  const handleEndChange = (newValue: Date | null) => {
    setEndPeriod(newValue?.toISOString().replace(/.\d+Z$/g, "Z") as string);
    setFormValue({
      ...formValue,
      endPeriod: endPeriod as unknown as string,
    });
  };
  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setFormValue({
      ...formValue,
      [event.currentTarget.name]: event.currentTarget.value,
    });
    console.log(formValue);
  };
  const handleSelectChange = (event: SelectChangeEvent) => {
    setFormValue({ ...formValue, [event.target.name]: event.target.value });
  };
  const handleSubmit = () => {
    //mapping value to submit
    const submitValue = {
      patientId: "aab9f746-1ede-487e-a3d1-9aa15a3b1aea",
      active: (formValue?.status === "true"),
      patient: {
        reference: "Patient/example",
      },
      gender: formValue?.gender,
      relationship: [
        {
          coding: [
            {
              system: "http://terminology.hl7.org/CodeSystem/v2-0131",
              code: "N",
            },
            {
              system: "http://terminology.hl7.org/CodeSystem/v3-RoleCode",
              code: formValue?.relationship,
            },
          ],
        },
      ],
      name: [
        {
          use: "official",
          text: formValue?.givenName + formValue?.familyName,
          family: formValue?.familyName,
          given: [formValue?.givenName],
        },
      ],
      telecom: [
        {
          system: "phone",
          value: formValue.phoneNumber,
        },
      ],
      address: [
        {
          line: [formValue?.lineAddress],
          city: formValue?.city,
          postalCode: formValue?.postalCode,
          country: formValue?.country,
        },
      ]
    } as relatedPatientModel;

    if (
      formValue?.familyName &&
      formValue?.givenName &&
      formValue?.phoneNumber
    ) {
      setLoading(true);
      if (id)
      relatedPatientService
        .update({ body: submitValue }, id)
        .then(() => navigate(`/admin/patient/details?id=${id}`, { state: 6 }))
        .catch(() => setLoading(false));
    else
      relatedPatientService
        .post({ body: submitValue })
        .then(() => navigate(`/admin/patient/details?id=${id}`, { state: 6 }))
        .catch(() => setLoading(false));
    }
  };

  //styling
  const style = {
    "& .MuiOutlinedInput-root": {
      height: "100%",
      "& fieldset": {
        border: "1px solid #EAEAEA",
        borderRadius: "0.8rem",
        padding: 0,
        margin: 0,
      },
      "&:hover fieldset": {
        border: "1px solid #dbd7d7",
      },
      "&.Mui-focused fieldset": {
        border: "1px solid #EAEAEA",
      },
    },
  };
  const StyledButton = styled(Button)(({}) => ({
    margin: "0",
    color: "#FFFFFF",
    fontWeight: "700",
    fontSize: "1rem",
    lineHeight: "1.2rem",
    backgroundColor: "#6C5DD3",
    borderRadius: "6rem",
    padding: "10px 16px",
    whiteSpace: "nowrap",
    "&:hover": {
      backgroundColor: "#887ae6",
    },
  }));
  //Field header Mapping
  const relatedPatientDetailsFields = [
    {
      name: "familyName",
      value: formValue?.familyName,
      label: "Family Name",
      required: true,
    },
    {
      name: "givenName",
      value: formValue?.givenName,
      label: "Given Name",
      required: true,
    },
    {
      name: "phoneNumber",
      value: formValue?.phoneNumber,
      label: "Phone Number",
      required: true,
    },
  ];
  const coverageFields = [
    {
      name: "relationship",
      value: formValue?.relationship,
      label: "Relationship",
      required: false,
    },
    {
      name: "lineAddress",
      value: formValue?.lineAddress,
      label: "Address Line",
      required: false,
    },
    {
      name: "city",
      value: formValue?.city,
      label: "City",
      required: false,
    },
    {
      name: "postalCode",
      value: formValue?.postalCode,
      label: "Postal Code",
      required: false,
    },
    {
      name: "country",
      value: formValue?.country,
      label: "Country",
      required: false,
    },
  ];
  const gender = [
    { value: "Male", display: "Male" },
    { value: "Female", display: "Female" },
  ];
  const Status = [
    { value: "true", display: "Active" },
    { value: "false", display: "Not Active" },
  ];

  React.useEffect(() => {
    if (id)
    relatedPatientService.getById(id).then((res) => {
    setFormValue({
      patientId: "aab9f746-1ede-487e-a3d1-9aa15a3b1aea",
      gender: "Male",
    })
    setLoading(false)
  })}, [id]);

  return (
    <Box>
      {/* Header */}
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <Typography sx={{ fontWeight: 700, fontSize: 24, display: "flex", alignItems: "center", gap:"16px" }}>
          <StyledArrowBack onClick={() =>  history.back()}/>
          {id? "EDIT RELATED PERSON": "ADD RELATED PERSON"}
        </Typography>
        <StyledButton onClick={handleSubmit}>
          {searchParams.get("id") ? (
            <>Save changes</>
          ) : (
            <>
              {" "}
              Add <AddIcon style={{ marginLeft: "10px" }} />
            </>
          )}
        </StyledButton>
      </Box>
      {/* Body */}
      {loading && <Loading />}
      {!loading && (
        <Box
          style={{
            display: "flex",
            justifyContent: "space-between",
            background: "white",
            borderRadius: "16px",
            margin: "16px 0",
            padding: "16px",
          }}
        >
          {/* Left Col */}
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              gap: "16px",
              width: "49%",
            }}
          >
            <Typography sx={{ fontWeight: 600, fontSize: 20 }}>
              General Information
            </Typography>
            {relatedPatientDetailsFields.map((item) => (
              <>
                <TextField
                  name={item.name}
                  value={item.value}
                  onChange={handleInputChange}
                  label={item.label}
                  required={item.required}
                  error={item.required ? !item.value : false}
                  sx={style}
                />
              </>
            ))}

            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                "& .MuiFormControl-root": {
                  flexBasis: "100%",
                },
                ...style,
              }}
            >
              <FormControl>
                <InputLabel id="status-select-label">Status</InputLabel>
                <Select
                  labelId="status-select-label"
                  id="status-select"
                  value={formValue?.status}
                  label="Status"
                  name="status"
                  onChange={handleSelectChange}
                  required
                  error={!formValue?.status}
                >
                  {Status.map(({ value, display }) => (
                    <MenuItem value={value}>{display}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Box>
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                "& .MuiFormControl-root": {
                  flexBasis: "100%",
                },
                ...style,
              }}
            >
              <FormControl fullWidth sx={style}>
                <InputLabel id="type-select-label">Gender</InputLabel>
                <Select
                  labelId="gender-select-label"
                  id="gender-select"
                  value={formValue?.gender}
                  label="Gender"
                  name="gender"
                  onChange={handleSelectChange}
                  required
                  error={!formValue?.gender}
                >
                  {gender.map(({ value, display }) => (
                    <MenuItem value={value}>{display}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Box>
          </Box>
          {/* Right Col*/}
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              gap: "16px",
              width: "49%",
            }}
          >
            <Typography sx={{ fontWeight: 600, fontSize: 20 }}>
              More Information
            </Typography>
            {coverageFields.map((item) => (
              <TextField
                name={item.name}
                value={item.value}
                onChange={handleInputChange}
                label={item.label}
                required={item.required}
                error={item.required ? !item.value : false}
                sx={style}
              />
            ))}
          </Box>
        </Box>
      )}
    </Box>
  );
};
