import { Add, Create, Delete, Search, Visibility } from "@mui/icons-material";
import {
  Box,
  IconButton,
  InputAdornment,
  TextField,
  Typography,
} from "@mui/material";
import React from "react";
import { Link, useNavigate } from "react-router-dom";
import { DeleteDialog } from "../../../components/DeleteDialog";
import { RoundedButton } from "../../../components/RoundedButton";
import { AdvancedSearch } from "../../../components/Table/AdvancedSearch";
import { CustomTable } from "../../../components/Table/Table";
import { HeadCell } from "../../../components/Table/types";
import { relatedPatientModel } from "../../../models/relatedPatient";
import { relatedPatientService } from "../../../services/agent";
import { colorPalette } from "../../../theme/ColorPalette";

const headCells: HeadCell[] = [
  {
    id: "number",
    numeric: false,
    label: "No.",
  },
  {
    id: "name",
    numeric: false,
    label: "Name",
  },
  {
    id: "relationship",
    numeric: false,
    label: "Relationship",
  },
  {
    id: "gender",
    numeric: false,
    label: "Gender",
  },
  {
    id: "phone",
    numeric: false,
    label: "Phone Number",
  },
  {
    id: "address",
    numeric: false,
    label: "Address",
  },
  {
    id: "status",
    numeric: false,
    label: "Status",
  },
  {
    id: "action",
    numeric: false,
    label: "Action",
  },
];
const advancedSearchProps = [
  {
    name: "relationship",
    label: "Relationship",
    select: true,
    menuItems: [
      {
        value: "wife",
        display: "Wife",
      },
      {
        value: "son",
        display: "Son",
      },
      {
        value: "husband",
        display: "Husband",
      },
      {
        value: "daughter",
        display: "Daughter",
      },
      {
        value: "mother",
        display: "Mother",
      },
      {
        value: "father",
        display: "Father",
      },
    ],
  },
  {
    name: "gender",
    label: "Gender",
    select: true,
    menuItems: [
      {
        value: "male",
        display: "Male",
      },
      {
        value: "female",
        display: "Female",
      },
    ],
  },
  {
    name: "active",
    label: "Status",
    select: true,
    menuItems: [
      {
        value: "true",
        display: "Active",
      },
      {
        value: "false",
        display: "Inactive",
      },
    ],
  },
  {
    name: "period",
    dateTime: "period",
    inputFormat: "YYYY",
  },
  {
    name: "birthDate",
    dateTime: "single",
    label: "D.O.B",
    inputFormat: "MM-DD-YYYY",
  },
];
export const RelatedPatient: React.FC = () => {
  const navigate = useNavigate();
  // useState
  const [isLoading, setIsLoading] = React.useState(false);
  const [data, setData] = React.useState<relatedPatientModel[]>([]);
  const [page, setPage] = React.useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = React.useState<number>(10);
  const [rowsCount, setRowsCount] = React.useState<number>(0);
  const [deletePopUpOpen, setDeletePopUpOpen] = React.useState(false);
  const [openAdvancedSearch, setOpenAdvancedSearch] = React.useState(false);
  const [currentSelectedId, setCurrentSelectedId] = React.useState("");

  // Search states
  const [searchValue, setSearchValue] = React.useState<string>("");
  const [debounceSeachValue, setDebounceSearchValue] =
    React.useState<string>("");
  const [filter, setFilter] = React.useState<{ [key: string]: string }>({});

  const getRelatedPatientList = async (filter?: { [keys: string]: string }) => {
    const params = {
      ...filter,
      page: page + 1,
      size: rowsPerPage,
      name: debounceSeachValue,
    };
    setIsLoading(true);
    try {
      let { data, paging } = await relatedPatientService.getList({ params });
      setData(data);
      setRowsCount(paging.total);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      console.log("getRelatedPatientList error: ", error);
    }
  };
  const deleteRelatedPatient = async (id: string) => {
    setIsLoading(true);
    try {
      let response = await relatedPatientService.delete(id);
      setIsLoading(false);
      return response;
    } catch (error) {
      setIsLoading(false);
      console.log("deletePatient error: ", error);
    }
  };

  // Event Handler
  const handleChangePage = (event: any, newPage: number) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event: any) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleDelete = (id: string) => {
    setDeletePopUpOpen(true);
    setCurrentSelectedId(id);
  };
  const handleCancelDelete = () => {
    setDeletePopUpOpen(false);
  };
  const handleConfirmDelete = () => {
    setDeletePopUpOpen(false);
    deleteRelatedPatient(currentSelectedId).then((res) => {
      setCurrentSelectedId("");
      getRelatedPatientList();
    });
  };
  const handleClickAdvancedSearchOpen = () => {
    setOpenAdvancedSearch(true);
  };
  //Debounce for name search
  React.useEffect(() => {
    let timeout = setTimeout(() => {
      setDebounceSearchValue(searchValue);
    }, 500);
    return () => {
      clearTimeout(timeout);
    };
  }, [searchValue]);

  // Back to previous page if current page is empty after deleting an item
  React.useEffect(() => {
    let totalPages = Math.ceil(rowsCount / rowsPerPage);
    let currentPage = page + 1;
    if (currentPage > totalPages && totalPages > 0) {
      setPage(totalPages - 1);
    }
  }, [rowsCount]);
  React.useEffect(() => {
    getRelatedPatientList();
  }, [debounceSeachValue, rowsPerPage, page]);

  const StatusCircle = (status: string) => {
    const theme = {
      true: colorPalette.purple.shade_500,
      false: colorPalette.red.shade_500,
    };
    return (
      <div
        style={{
          height: 6,
          width: 6,
          borderRadius: "50%",
          background: theme[status as keyof typeof theme] as string,
        }}
      />
    );
  };

  const mapDataToRender = () => {
    const render = data.map((item, index) => ({
      id: item.id as string,
      number: index + 1,
      name:
        item.name?.find((x) => x.use === "official")?.given?.join(" ") +
          " " +
          item.name?.find((x) => x.use === "official")?.family ||
        item.name?.find((x) => x.use === "official")?.text,
      relationship: (
        <Typography
          sx={{
            textTransform: "lowercase !important",
          }}
          children={item.relationship?.[0].coding?.[1].code}
        />
      ),
      gender: (
        <Typography
          sx={{ textTransform: "capitalize !important" }}
          children={item.gender}
        />
      ),
      phone: item.telecom?.find((x) => x.system == "phone")?.value,
      address: item.address
        ? item.address[0].line
          ? (item.address[0].line[0] as string)
          : ""
        : "",
      status: (
        <Box display="flex" alignItems="center" gap="8px">
          {StatusCircle(String(item.active))}
          <Typography
            textTransform="capitalize"
            children={item.active ? "Active" : "Inactive"}
          />
        </Box>
      ),
      action: (
        <Box style={{ textAlign: "center", whiteSpace: "nowrap" }}>
          <Link
            to={`relatedPatient/edit?id=${item.id}`}
            children={<IconButton children={<Create />} />}
          />
          <Link
            to={`relatedPatient?id=${item.id}`}
            children={<IconButton children={<Visibility />} />}
          />

          <Link
            to={`#`}
            onClick={() => {
              handleDelete(item.id as string);
            }}
            children={<IconButton children={<Delete />} />}
          />
        </Box>
      ),
    }));
    return render;
  };
  return (
    <>
      {/* Dialog for delete item popup */}
      <DeleteDialog
        open={deletePopUpOpen}
        onClose={handleCancelDelete}
        onConfirm={handleConfirmDelete}
        onCancel={handleCancelDelete}
      />
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          width: "100%",
          padding: "20px",
          backgroundColor: "white",
          borderRadius: "16px",
        }}
      >
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            gap: "8px",
          }}
        >
          <Typography variant="h2" children={"RELATED PERSON"} />
          <Box sx={{ display: "flex", gap: "8px" }}>
            <TextField
              placeholder="Search plan name..."
              InputProps={{
                startAdornment: (
                  <InputAdornment
                    position="start"
                    children={<Search sx={{ height: 20 }} />}
                  />
                ),
              }}
              sx={{
                "& .MuiInputBase-root": {
                  height: 48,
                  width: 331,
                  borderRadius: 3,
                },
              }}
              value={searchValue}
              onChange={(e) => {
                setSearchValue(e.target.value);
              }}
            />
            <RoundedButton
              variant="contained"
              onClick={handleClickAdvancedSearchOpen}
              children="Advanced Search"
            />
            <RoundedButton
              variant="contained"
              endIcon={<Add />}
              onClick={() => {
                navigate("relatedPatient/add");
              }}
              children="Add New"
            />
          </Box>
        </Box>
        {openAdvancedSearch && (
          <AdvancedSearch
            props={advancedSearchProps}
            filter={filter}
            description="Find related patient name..."
            onFilterChange={setFilter}
            onApply={getRelatedPatientList}
            onClose={setOpenAdvancedSearch}
          />
        )}
        <CustomTable
          title={"INSURANCE"}
          headcells={headCells}
          body={mapDataToRender()}
          paging={{
            page: page,
            rowsPerPage: rowsPerPage,
            rowsCount: rowsCount,
          }}
          loading={isLoading}
          toolbar={false}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Box>
    </>
  );
};
