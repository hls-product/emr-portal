import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import { IconButton } from "@mui/material";
import { styled } from "@mui/system";
import React from "react";
import { Link, useSearchParams } from "react-router-dom";
import Loading from "../../../components/Loading";
import { relatedPatientService } from "../../../services/agent";

interface TableProps {
  name: string;
  relationship: string;
  gender: string;
  phoneNumber: string;
  address: string;
  patientID: string;
  status: any;
  dob: string;
  id: string;
  note: string | null;
}

const Table = styled("table")({
  background: "white",
  flex: 1,
  padding: "8px 16px",
  borderRadius: "16px",
  "& tr": {
    lineHeight: "22px",
  },
  "& th": {
    padding: "8px 0",
    textAlign: "left",
    lineHeight: "22px",
  },
  "& td": {
    padding: "8px 0",
    textAlign: "right",
  },
});

const DetailTable: React.FC<TableProps> = ({ ...props }) => (
  <div
    style={{
      display: "flex",
      justifyContent: "space-between",
      gap: "20px",
    }}
  >
    <Table>
      <tr>
        <th>Name</th>
        <td>{props.name}</td>
      </tr>
      <tr>
        <th>Relationship</th>
        <td>{props.relationship}</td>
      </tr>
      <tr>
        <th>Gender</th>
        <td>{props.gender}</td>
      </tr>
      <tr>
        <th>Phone Number</th>
        <td>{props.phoneNumber}</td>
      </tr>
      <tr>
        <th>Address</th>
        <td>{props.address}</td>
      </tr>
    </Table>
    <Table>
      <tr>
        <th>Date of Birth</th>
        <td>{props.dob}</td>
      </tr>
      <tr>
        <th>Status</th>
        <td>
          <div
            style={{
              padding: "3px 12px",
              border: "1px solid rgba(255, 115, 0, 1)",
              color: "rgba(255, 115, 0, 1)",
              borderRadius: "21px",
              width: "fit-content",
              float: "right",
            }}
          >
            {props.status}
          </div>
        </td>
      </tr>
      <tr>
        <th>ID</th>
        <td>{props.id}</td>
      </tr>
      <tr>
        <th>Patient ID</th>
        <td>{props.patientID}</td>
      </tr>
      <tr>
        <th>Note</th>
        <td>{props.note}</td>
      </tr>
    </Table>
  </div>
);

export const RelatedDetail: React.FC = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const id = searchParams.get("id");
  const [loading, setLoading] = React.useState<boolean>(true);
  const [data, setData] = React.useState<TableProps>({
    name: "",
    relationship: "",
    gender: "",
    phoneNumber: "",
    address: "",
    patientID: "",
    status: "",
    dob: "",
    id: "",
    note: "",
  });

  React.useEffect(() => {
    if (!id) throw history.back();
    relatedPatientService.getById(id).then((res) => {
      setData(() => {
        const newData = {
          name:
            (res.name ? (res.name[0].family as string) : "") +
            " " +
            (res.name
              ? res.name[0].given
                ? (res.name[0].given[0] as string)
                : ""
              : ""),
          relationship: res.relationship
            ? res.relationship[0]
              ? res.relationship[0].coding
                ? res.relationship[0].coding[1]
                  ? (res.relationship[0].coding[1].code as string)
                  : ""
                : ""
              : ""
            : "",
          gender: res ? (res.gender as string) : "",
          phoneNumber: res.telecom ? (res.telecom[0].value as string) : "",
          address: res.address
            ? res.address[0].line
              ? (res.address[0].line[0] as string)
              : ""
            : "",
          patientID: res ? (res.patientId as string) : "",
          status: res ? "Active" : "Not Active",
          dob: "12 - 02 - 1993",
          id: res ? (res.id as string) : "",
          note: "Notes on adminstration",
        } as TableProps;
        return newData;
      });
      setLoading(false);
    }).catch(() => history.back());
  }, [id]);

  return (
    <div>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <div
          style={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <Link to={`/admin/patient/details?id=${data.id}`} state={6}>
            <IconButton
              style={{
                color: "rgba(108, 93, 211, 1)",
                background: "white",
                borderRadius: "50%",
                width: "40px",
                height: "40px",
                marginRight: "16px",
              }}
            >
              <ArrowBackIcon />
            </IconButton>
          </Link>
          <h1 style={{ fontSize: "24px" }}>Related Person Details</h1>
        </div>
        <Link
          to={`update?id=${id}`}
          style={{
            padding: "10px 30px",
            borderRadius: "30px",
            color: "rgba(108, 93, 211, 1)",
            background: "white",
            textDecoration: "none",
          }}
        >
          <EditOutlinedIcon style={{ marginRight: "12px" }} />
          Edit
        </Link>
      </div>
      {loading && <Loading></Loading>}
      {!loading && <DetailTable {...data} />}
    </div>
  );
};
