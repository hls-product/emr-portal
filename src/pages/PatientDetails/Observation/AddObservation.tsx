import {
  Box,
  FormControl,
  IconButton,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
  TextField,
  Typography,
} from "@mui/material";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { RoundedButton } from "../../../components/RoundedButton";
import { ReactComponent as BackArrow } from "../../../assets/image/BackArrow.svg";
import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";

import { selectIdUrl } from "../../../store/reducers/observations/observationSlice";
import { observationModel } from "../../../models/observation";
import { useSelector } from "react-redux";
import { observationService } from "../../../services/agent";
import Loading from "../../../components/Loading";

const Status = [
  { value: "Registered" },
  { value: "Preliminary" },
  { value: "Final" },
  { value: "Amended" },
  { value: "Corrected" },
  { value: "Cancelled" },
  { value: "Entered in Error" },
  { value: "Unknown" },
];

const Classification = [
  { value: "Social History" },
  { value: "Vital Signs" },
  { value: "Imaging" },
  { value: "Laboratory" },
  { value: "Procedure" },
  { value: "Survey" },
  { value: "Exam" },
  { value: "Therapy" },
  { value: "Activity" },
];

const Type = [
  { value: "Body Weight" },
  { value: "Body Temperature" },
  { value: "Emotion" },
  { value: "Glucose [Moles/volume] in Blood" },
];

const Unit = [{ value: "kg" }, { value: "C" }, { value: "mmol/l" }];

export const AddObservation: React.FC = () => {
  const navigate = useNavigate();
  const idUrl = useSelector(selectIdUrl);
  const [name, setName] = useState<string>("");
  const [status, setStatus] = useState<string>("Final");
  const [classification, setClassification] = useState<string>("Vital Signs");
  const [typeObservation, setTypeObservation] = useState<string>("Body Weight");
  const [startPeriod, setStartPeriod] = useState<string>("2014-08-18T21:11:54");
  const [endPeriod, setEndPeriod] = useState<string>("2015-08-18T21:11:54");
  const [issued, setIssued] = useState<string>("2014-08-18T21:11:54");
  const [result, setResult] = useState<number>();
  const [unit, setUnit] = useState<string>("");
  const [method, setMethod] = useState<string>("");
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const handleChangeName = (event: React.ChangeEvent<HTMLInputElement>) => {
    setName(event.target.value as string);
  };

  const handleChangeStatus = (event: SelectChangeEvent) => {
    setStatus(event.target.value as string);
  };

  const handleChangeClassification = (event: SelectChangeEvent) => {
    setClassification(event.target.value as string);
  };

  const handleChangeType = (event: SelectChangeEvent) => {
    setTypeObservation(event.target.value as string);
  };

  const handleChangeStart = (value: any) => {
    setStartPeriod(value.toISOString().replace(/.\d+Z$/g, "Z") as string);
  };

  const handleChangeEnd = (value: any) => {
    setEndPeriod(value.toISOString().replace(/.\d+Z$/g, "Z") as string);
  };

  const handleChangeIssued = (value: any) => {
    setIssued(value.toISOString().replace(/.\d+Z$/g, "Z") as string);
  };

  const handleChangeResult = (event: React.ChangeEvent<HTMLInputElement>) => {
    setResult(Number(event.target.value));
  };

  const handleChangeUnit = (event: SelectChangeEvent) => {
    setUnit(event.target.value as string);
  };

  const handleChangeMethod = (event: React.ChangeEvent<HTMLInputElement>) => {
    setMethod(event.target.value as string);
  };

  const addNewObservation = async () => {
    const data = {
      status,
      category: [
        {
          coding: [{ display: classification }],
        },
      ],
      code: {
        coding: [{ display: typeObservation }],
      },
      subject: { display: name },
      effectivePeriod: {
        start: startPeriod,
        end: endPeriod,
      },
      issued,
      value: {
        type: "quantity",
        value: result,
        unit,
      },
      method: {
        coding: [{ display: method }],
      },
    };
    setIsLoading(true);
    try {
      await observationService.post({ body: data as observationModel });
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      console.log("Error creating new observation: ", error);
    }
  };

  const handleAdd = () => {
    addNewObservation();
    navigate(`/admin/patient/details?id=${idUrl}`, { state: 5 });
  };

  return (
    <Box>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          marginBottom: "16px",
        }}
      >
        <Box sx={{ display: "flex", gap: 1, alignItems: "center" }}>
          <IconButton
            onClick={() =>
              navigate(`/admin/patient/details?id=${idUrl}`, { state: 5 })
            }
            style={{
              width: "40px",
              height: "40px",
              border: "1px solid #F4F4F4",
              background: "white",
            }}
          >
            <BackArrow />
          </IconButton>
          <Typography variant="h2" sx={{ fontWeight: 600, fontSize: "24px" }}>
            Add Observation
          </Typography>
        </Box>

        <RoundedButton
          variant="contained"
          style={{ padding: "10px 20px" }}
          onClick={handleAdd}
        >
          Add
        </RoundedButton>
      </Box>

      <Box
        sx={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <Box sx={{ width: "49.25%" }}>
          <Box
            sx={{
              background: "white",
              borderRadius: "16px",
              padding: "16px",
              marginBottom: "16px",
            }}
          >
            <Typography
              variant="h3"
              sx={{
                fontSize: "16px",
                fontWeight: 700,
                marginBottom: "20px",
                textTransform: "uppercase",
              }}
            >
              Basic Information
            </Typography>

            <TextField
              required
              label="Name"
              placeholder="Enter"
              InputProps={{
                style: {
                  height: "48px",
                  borderRadius: "12px",
                  color: "black",
                },
              }}
              InputLabelProps={{
                style: {
                  color: "black",
                },
              }}
              sx={{ width: "100%", marginBottom: "16px" }}
              onChange={handleChangeName}
            />

            <FormControl sx={{ width: "100%", marginBottom: "16px" }}>
              <InputLabel required sx={{ color: "black" }}>
                Status
              </InputLabel>
              <Select
                label="Status"
                sx={{
                  height: "48px",
                  borderRadius: "12px",
                  color: "black",
                }}
                onChange={handleChangeStatus}
              >
                {Status.map((item, index) => (
                  <MenuItem key={index} value={item.value}>
                    {item.value}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                width: "100%",
              }}
            >
              <FormControl sx={{ width: "49%" }}>
                <InputLabel required sx={{ color: "black" }}>
                  Classification
                </InputLabel>
                <Select
                  label="Classification"
                  sx={{
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  }}
                  onChange={handleChangeClassification}
                >
                  {Classification.map((item, index) => (
                    <MenuItem key={index} value={item.value}>
                      {item.value}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>

              <FormControl sx={{ width: "49%" }}>
                <InputLabel required sx={{ color: "black" }}>
                  Observation Type
                </InputLabel>
                <Select
                  label="Observation Type"
                  sx={{
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  }}
                  onChange={handleChangeType}
                >
                  {Type.map((item, index) => (
                    <MenuItem key={index} value={item.value}>
                      {item.value}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Box>
          </Box>

          <Box
            sx={{
              background: "white",
              borderRadius: "16px",
              padding: "16px",
              marginBottom: "16px",
            }}
          >
            <Typography
              variant="h3"
              sx={{
                fontSize: "16px",
                fontWeight: 700,
                marginBottom: "20px",
                textTransform: "uppercase",
              }}
            >
              Observation Schedule
            </Typography>

            <Box
              sx={{
                gap: 1,
                display: "flex",
                justifyContent: "space-between",
                "& .MuiOutlinedInput-root": {
                  "& fieldset": {
                    borderRadius: "12px",
                    padding: 0,
                    margin: 0,
                  },
                },
              }}
            >
              <LocalizationProvider dateAdapter={AdapterDayjs}>
                <DesktopDatePicker
                  label="Starting time"
                  inputFormat="MM/DD/YYYY"
                  value={startPeriod}
                  onChange={handleChangeStart}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      error={startPeriod ? startPeriod >= endPeriod : true}
                    />
                  )}
                />
              </LocalizationProvider>

              <LocalizationProvider dateAdapter={AdapterDayjs}>
                <DesktopDatePicker
                  label="Ending time"
                  inputFormat="MM/DD/YYYY"
                  value={endPeriod}
                  onChange={handleChangeEnd}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      error={endPeriod ? endPeriod <= startPeriod : true}
                    />
                  )}
                />
              </LocalizationProvider>

              <LocalizationProvider dateAdapter={AdapterDayjs}>
                <DesktopDatePicker
                  label="Issued"
                  inputFormat="MM/DD/YYYY"
                  value={issued}
                  onChange={handleChangeIssued}
                  renderInput={(params) => <TextField {...params} />}
                />
              </LocalizationProvider>
            </Box>
          </Box>

          <Box
            sx={{
              background: "white",
              borderRadius: "16px",
              padding: "16px",
            }}
          >
            <Typography
              variant="h3"
              sx={{
                fontSize: "16px",
                fontWeight: 700,
                marginBottom: "20px",
                textTransform: "uppercase",
              }}
            >
              Result
            </Typography>

            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                gap: 1,
                justifyContent: "space-between",
              }}
            >
              <TextField
                required
                label="Result"
                placeholder="Enter"
                InputProps={{
                  style: {
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  },
                }}
                InputLabelProps={{
                  style: {
                    color: "black",
                  },
                }}
                sx={{ width: "66%" }}
                onChange={handleChangeResult}
              />

              <FormControl sx={{ width: "33%" }}>
                <InputLabel required sx={{ color: "black" }}>
                  Unit
                </InputLabel>
                <Select
                  label="Unit"
                  sx={{
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  }}
                  onChange={handleChangeUnit}
                >
                  {Unit.map((item, index) => (
                    <MenuItem key={index} value={item.value}>
                      {item.value}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Box>
          </Box>
        </Box>

        <Box
          sx={{
            width: "49.25%",
            background: "white",
            borderRadius: "16px",
            padding: "16px",
          }}
        >
          <Typography
            variant="h3"
            sx={{
              fontSize: "16px",
              fontWeight: 700,
              marginBottom: "20px",
              textTransform: "uppercase",
            }}
          >
            Method
          </Typography>

          <TextField
            required
            label="Method"
            placeholder="Enter"
            InputProps={{
              style: {
                height: "176px",
                borderRadius: "12px",
                color: "black",
              },
            }}
            InputLabelProps={{
              style: {
                color: "black",
              },
            }}
            sx={{ width: "100%" }}
            onChange={handleChangeMethod}
          />
        </Box>
      </Box>
    </Box>
  );
};
