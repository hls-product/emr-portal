import {
  Box,
  FormControl,
  IconButton,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
  TextField,
  Typography,
} from "@mui/material";
import { useNavigate, useSearchParams } from "react-router-dom";
import React, { useEffect, useState } from "react";
import { ReactComponent as BackArrow } from "../../../assets/image/BackArrow.svg";
import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { selectIdUrl } from "../../../store/reducers/observations/observationSlice";
import { useSelector } from "react-redux";
import { observationModel } from "../../../models/observation";
import { observationService } from "../../../services/agent";
import Loading from "../../../components/Loading";

const Status = [
  { value: "Registered" },
  { value: "Preliminary" },
  { value: "Final" },
  { value: "Amended" },
  { value: "Corrected" },
  { value: "Cancelled" },
  { value: "Entered in Error" },
  { value: "Unknown" },
];

const Classification = [
  { value: "Social History" },
  { value: "Vital Signs" },
  { value: "Imaging" },
  { value: "Laboratory" },
  { value: "Procedure" },
  { value: "Survey" },
  { value: "Exam" },
  { value: "Therapy" },
  { value: "Activity" },
];

const Type = [
  { value: "Body Weight" },
  { value: "Body Temperature" },
  { value: "Emotion" },
  { value: "Glucose [Moles/volume] in Blood" },
];

const Unit = [{ value: "kg" }, { value: "C" }, { value: "mmol/l" }];

export const DetailObservation: React.FC = () => {
  const navigate = useNavigate();
  const idUrl = useSelector(selectIdUrl);
  const [name, setName] = useState<string>("");
  const [status, setStatus] = useState<string>("");
  const [classification, setClassification] = useState<string>("");
  const [typeObservation, setTypeObservation] = useState<string>("");
  const [startPeriod, setStartPeriod] = useState<string>("");
  const [endPeriod, setEndPeriod] = useState<string>("");
  const [issued, setIssued] = useState<string>("");
  const [result, setResult] = useState<number>();
  const [unit, setUnit] = useState<string>("");
  const [method, setMethod] = useState<string>("");
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [data, setData] = useState<observationModel>({});
  const [searchParams, setSearchParams] = useSearchParams();

  const getObservationById = async () => {
    setIsLoading(true);
    try {
      let res = await observationService.getById(
        searchParams.get("id") as string
      );
      console.log("res: ", res);
      setData(res as observationModel);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    getObservationById();
  }, [searchParams]);

  useEffect(() => {
    let statusTransfer = data?.status || "";
    setName(data?.subject?.display || "");
    setStatus(statusTransfer.charAt(0).toUpperCase() + statusTransfer.slice(1));
    setClassification(data?.category?.[0]?.coding?.[0]?.display as string);
    setTypeObservation(data?.code?.coding?.[0]?.display as string);
    setStartPeriod(data?.effectivePeriod?.start || "");
    setEndPeriod(data?.effectivePeriod?.end || "");
    setIssued(data?.issued as string);
    setResult(data?.value?.value);
    setUnit(data?.value?.unit || "");
    setMethod(data?.method?.coding?.[0]?.display || "");
  }, [data]);

  const handleChangeName = (event: React.ChangeEvent<HTMLInputElement>) => {
    setName(event.target.value as string);
  };

  const handleChangeStatus = (event: SelectChangeEvent) => {
    setStatus(event.target.value as string);
  };

  const handleChangeClassification = (event: SelectChangeEvent) => {
    setClassification(event.target.value as string);
  };

  const handleChangeType = (event: SelectChangeEvent) => {
    setTypeObservation(event.target.value as string);
  };

  const handleChangeStart = (value: any) => {
    setStartPeriod(value.toISOString().replace(/.\d+Z$/g, "Z") as string);
  };

  const handleChangeEnd = (value: any) => {
    setEndPeriod(value.toISOString().replace(/.\d+Z$/g, "Z") as string);
  };

  const handleChangeIssued = (value: any) => {
    setIssued(value.toISOString().replace(/.\d+Z$/g, "Z") as string);
  };

  const handleChangeResult = (event: React.ChangeEvent<HTMLInputElement>) => {
    setResult(Number(event.target.value));
  };

  const handleChangeUnit = (event: SelectChangeEvent) => {
    setUnit(event.target.value as string);
  };

  const handleChangeMethod = (event: React.ChangeEvent<HTMLInputElement>) => {
    setMethod(event.target.value as string);
  };

  return (
    <Box>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          marginBottom: "16px",
        }}
      >
        <Box sx={{ display: "flex", gap: 1, alignItems: "center" }}>
          <IconButton
            onClick={() =>
              navigate(`/admin/patient/details?id=${idUrl}`, { state: 5 })
            }
            style={{
              width: "40px",
              height: "40px",
              border: "1px solid #F4F4F4",
              background: "white",
            }}
          >
            <BackArrow />
          </IconButton>
          <Typography variant="h2" sx={{ fontWeight: 600, fontSize: "24px" }}>
            Observation Details
          </Typography>
        </Box>
      </Box>

      {isLoading && <Loading />}
      {!isLoading && (
        <Box
          sx={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <Box sx={{ width: "49.25%" }}>
            <Box
              sx={{
                background: "white",
                borderRadius: "16px",
                padding: "16px",
                marginBottom: "16px",
              }}
            >
              <Typography
                variant="h3"
                sx={{
                  fontSize: "16px",
                  fontWeight: 700,
                  marginBottom: "20px",
                  textTransform: "uppercase",
                }}
              >
                Basic Information
              </Typography>

              <TextField
                required
                disabled
                label="Name"
                placeholder="Enter"
                InputProps={{
                  style: {
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  },
                }}
                InputLabelProps={{
                  style: {
                    color: "black",
                  },
                }}
                sx={{ width: "100%", marginBottom: "16px" }}
                onChange={handleChangeName}
                value={name}
              />

              <FormControl sx={{ width: "100%", marginBottom: "16px" }}>
                <InputLabel required sx={{ color: "black" }}>
                  Status
                </InputLabel>
                <Select
                  disabled
                  label="Status"
                  sx={{
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  }}
                  onChange={handleChangeStatus}
                  value={status}
                >
                  {Status.map((item, index) => (
                    <MenuItem key={index} value={item.value}>
                      {item.value}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>

              <Box
                sx={{
                  display: "flex",
                  justifyContent: "space-between",
                  width: "100%",
                }}
              >
                <FormControl sx={{ width: "49%" }}>
                  <InputLabel required sx={{ color: "black" }}>
                    Classification
                  </InputLabel>
                  <Select
                    disabled
                    label="Classification"
                    sx={{
                      height: "48px",
                      borderRadius: "12px",
                      color: "black",
                    }}
                    onChange={handleChangeClassification}
                    value={classification}
                  >
                    {Classification.map((item, index) => (
                      <MenuItem key={index} value={item.value}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>

                <FormControl sx={{ width: "49%" }}>
                  <InputLabel required sx={{ color: "black" }}>
                    Observation Type
                  </InputLabel>
                  <Select
                    disabled
                    label="Observation Type"
                    sx={{
                      height: "48px",
                      borderRadius: "12px",
                      color: "black",
                    }}
                    onChange={handleChangeType}
                    value={typeObservation}
                  >
                    {Type.map((item, index) => (
                      <MenuItem key={index} value={item.value}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Box>
            </Box>

            <Box
              sx={{
                background: "white",
                borderRadius: "16px",
                padding: "16px",
                marginBottom: "16px",
              }}
            >
              <Typography
                variant="h3"
                sx={{
                  fontSize: "16px",
                  fontWeight: 700,
                  marginBottom: "20px",
                  textTransform: "uppercase",
                }}
              >
                Observation Schedule
              </Typography>

              <Box
                sx={{
                  gap: 1,
                  display: "flex",
                  justifyContent: "space-between",
                  "& .MuiOutlinedInput-root": {
                    "& fieldset": {
                      borderRadius: "12px",
                      padding: 0,
                      margin: 0,
                    },
                  },
                }}
              >
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DesktopDatePicker
                    disabled
                    label="Starting time"
                    inputFormat="MM/DD/YYYY"
                    value={startPeriod}
                    onChange={handleChangeStart}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        error={startPeriod ? startPeriod >= endPeriod : true}
                      />
                    )}
                  />
                </LocalizationProvider>

                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DesktopDatePicker
                    disabled
                    label="End time"
                    inputFormat="MM/DD/YYYY"
                    value={endPeriod}
                    onChange={handleChangeEnd}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        error={endPeriod ? endPeriod <= startPeriod : true}
                      />
                    )}
                  />
                </LocalizationProvider>

                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DesktopDatePicker
                    disabled
                    label="Issued"
                    inputFormat="MM/DD/YYYY"
                    value={issued}
                    onChange={handleChangeIssued}
                    renderInput={(params) => <TextField {...params} />}
                  />
                </LocalizationProvider>
              </Box>
            </Box>

            <Box
              sx={{
                background: "white",
                borderRadius: "16px",
                padding: "16px",
              }}
            >
              <Typography
                variant="h3"
                sx={{
                  fontSize: "16px",
                  fontWeight: 700,
                  marginBottom: "20px",
                  textTransform: "uppercase",
                }}
              >
                Result
              </Typography>

              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  gap: 1,
                  justifyContent: "space-between",
                }}
              >
                <TextField
                  required
                  disabled
                  label="Result"
                  placeholder="Enter"
                  InputProps={{
                    style: {
                      height: "48px",
                      borderRadius: "12px",
                      color: "black",
                    },
                  }}
                  InputLabelProps={{
                    style: {
                      color: "black",
                    },
                  }}
                  sx={{ width: "66%" }}
                  onChange={handleChangeResult}
                  value={result}
                />

                <FormControl sx={{ width: "33%" }}>
                  <InputLabel required sx={{ color: "black" }}>
                    Unit
                  </InputLabel>
                  <Select
                    disabled
                    label="Unit"
                    sx={{
                      height: "48px",
                      borderRadius: "12px",
                      color: "black",
                    }}
                    onChange={handleChangeUnit}
                    value={unit}
                  >
                    {Unit.map((item, index) => (
                      <MenuItem key={index} value={item.value}>
                        {item.value}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Box>
            </Box>
          </Box>

          <Box
            sx={{
              width: "49.25%",
              background: "white",
              borderRadius: "16px",
              padding: "16px",
            }}
          >
            <Typography
              variant="h3"
              sx={{
                fontSize: "16px",
                fontWeight: 700,
                marginBottom: "20px",
                textTransform: "uppercase",
              }}
            >
              Method
            </Typography>

            <TextField
              disabled
              required
              label="Method"
              placeholder="Enter"
              InputProps={{
                style: {
                  height: "176px",
                  borderRadius: "12px",
                  color: "black",
                },
              }}
              InputLabelProps={{
                style: {
                  color: "black",
                },
              }}
              sx={{ width: "100%" }}
              onChange={handleChangeMethod}
              value={method}
            />
          </Box>
        </Box>
      )}
    </Box>
  );
};
