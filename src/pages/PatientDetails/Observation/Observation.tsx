// Import React, Router Dom, Redux & Lodash
import React from "react";
import { Link, useNavigate } from "react-router-dom";

// Import types
import { observationModel } from "../../../models/observation";

// Import styling

// Import MUI components
import { Add, Create, Delete, Search, Visibility } from "@mui/icons-material";
import {
  Box,
  IconButton,
  InputAdornment,
  TextField,
  Typography,
} from "@mui/material";
import { RoundedButton } from "../../../components/RoundedButton";

// Import Common components
import { DeleteDialog } from "../../../components/DeleteDialog";

// Import Redux reducer

// Import api service
import { AdvancedSearch } from "../../../components/Table/AdvancedSearch";
import { CustomTable } from "../../../components/Table/Table";
import { HeadCell } from "../../../components/Table/types";
import { observationService } from "../../../services/agent";

// Main component

const headCells: HeadCell[] = [
  {
    id: "number",
    numeric: false,
    label: "No.",
  },
  {
    id: "patientName",
    numeric: false,
    label: "Patient Name",
  },
  {
    id: "status",
    numeric: false,
    label: "Status",
  },
  {
    id: "classification",
    numeric: false,
    label: "Classification",
  },
  {
    id: "period",
    numeric: false,
    label: "Period",
  },
  {
    id: "issued",
    numeric: false,
    label: "Issued",
  },
  {
    id: "result",
    numeric: false,
    label: "Result",
  },
  {
    id: "method",
    numeric: false,
    label: "Method",
  },
  {
    id: "action",
    numeric: false,
    label: "Action",
  },
];
const advancedSearchProps = [
  {
    name: "status",
    label: "Status",
    select: true,
    menuItems: [
      {
        value: "registered",
        display: "Registered",
      },
      {
        value: "preliminary",
        display: "Preliminary",
      },
      {
        value: "final",
        display: "Final",
      },
      {
        value: "amended",
        display: "Amended",
      },
    ],
  },
  {
    name: "classification",
    label: "Classification",
  },
  {
    name: "method",
    label: "Method",
  },
  {
    name: "result",
    label: "Result",
  },
  {
    name: "period",
    dateTime: "period",
    inputFormat: "YYYY",
  },
  {
    name: "issue",
    dateTime: "single",
    inputFormat: "MM-dd-YYYY",
  },
];

export const Observation: React.FC = () => {
  const navigate = useNavigate();
  // useState
  const [isLoading, setIsLoading] = React.useState(false);
  const [data, setData] = React.useState<observationModel[]>([]);
  const [page, setPage] = React.useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = React.useState<number>(10);
  const [rowsCount, setRowsCount] = React.useState<number>(0);
  const [deletePopUpOpen, setDeletePopUpOpen] = React.useState(false);
  const [openAdvancedSearch, setOpenAdvancedSearch] = React.useState(false);
  const [currentSelectedId, setCurrentSelectedId] = React.useState("");

  // Search states
  const [searchValue, setSearchValue] = React.useState<string>("");
  const [debounceSeachValue, setDebounceSearchValue] =
    React.useState<string>("");
  const [filter, setFilter] = React.useState<{ [key: string]: string }>({});

  const getObservationList = async (filter?: { [keys: string]: string }) => {
    const params = {
      ...filter,
      page: page + 1,
      size: rowsPerPage,
      name: debounceSeachValue,
    };
    setIsLoading(true);
    try {
      let { data, paging } = await observationService.getList({ params });
      setData(data);
      setRowsCount(paging.total);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      console.log("getObservationList error: ", error);
    }
  };
  const deleteObservation = async (id: string) => {
    setIsLoading(true);
    try {
      let response = await observationService.delete(id);
      setIsLoading(false);
      return response;
    } catch (error) {
      setIsLoading(false);
      console.log("deleteObservation error: ", error);
    }
  };

  // Event Handler
  const handleChangePage = (event: any, newPage: number) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event: any) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleDelete = (id: string) => {
    setDeletePopUpOpen(true);
    setCurrentSelectedId(id);
  };
  const handleCancelDelete = () => {
    setDeletePopUpOpen(false);
  };
  const handleConfirmDelete = () => {
    setDeletePopUpOpen(false);
    deleteObservation(currentSelectedId).then((res) => {
      setCurrentSelectedId("");
      getObservationList();
    });
  };
  const handleClickAdvancedSearchOpen = () => {
    setOpenAdvancedSearch(true);
  };
  //Debounce for name search
  React.useEffect(() => {
    let timeout = setTimeout(() => {
      setDebounceSearchValue(searchValue);
    }, 500);
    return () => {
      clearTimeout(timeout);
    };
  }, [searchValue]);

  // Back to previous page if current page is empty after deleting an item
  React.useEffect(() => {
    let totalPages = Math.ceil(rowsCount / rowsPerPage);
    let currentPage = page + 1;
    if (currentPage > totalPages && totalPages > 0) {
      setPage(totalPages - 1);
    }
  }, [rowsCount]);
  React.useEffect(() => {
    getObservationList();
  }, [debounceSeachValue, rowsPerPage, page]);

  const mapDataToRender = () => {
    const render = data.map((item, index) => ({
      id: item.id as string,
      number: index + 1,
      patientName: item?.subject?.display,
      status: (
        <Typography
          textTransform="capitalize"
          fontSize="14px"
          children={item?.status}
        />
      ),
      classification: item?.code?.coding?.[0]?.display,
      period:
        item?.effectivePeriod?.start?.slice(0, 4) ||
        "N/A" + " - " + item?.effectivePeriod?.end?.slice(0, 4) ||
        new Date().getFullYear(),
      issued: item?.issued?.slice(0, 10),
      result: item?.value?.value || "-" + " " + item?.value?.unit || "-",
      method: item?.method?.coding?.[0]?.display,
      action: (
        <Box style={{ textAlign: "center", whiteSpace: "nowrap" }}>
          <Link
            to={`observation/edit?id=${item.id}`}
            children={<IconButton children={<Create />} />}
          />
          <Link
            to={`observation/details?id=${item.id}`}
            children={<IconButton children={<Visibility />} />}
          />

          <Link
            to={`#`}
            onClick={() => {
              handleDelete(item.id as string);
            }}
            children={<IconButton children={<Delete />} />}
          />
        </Box>
      ),
    }));
    return render;
  };

  return (
    <>
      {/* Dialog for delete item popup */}
      <DeleteDialog
        open={deletePopUpOpen}
        onClose={handleCancelDelete}
        onConfirm={handleConfirmDelete}
        onCancel={handleCancelDelete}
      />
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          width: "100%",
          padding: "20px",
          backgroundColor: "white",
          borderRadius: "16px",
        }}
      >
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            gap: "8px",
          }}
        >
          <Typography variant="h2" children={"OBSERVATION"} />
          <Box sx={{ display: "flex", gap: "8px" }}>
            <TextField
              placeholder="Search patient name..."
              InputProps={{
                startAdornment: (
                  <InputAdornment
                    position="start"
                    children={<Search sx={{ height: 20 }} />}
                  />
                ),
              }}
              sx={{
                "& .MuiInputBase-root": {
                  height: 48,
                  width: 331,
                  borderRadius: 3,
                },
              }}
              value={searchValue}
              onChange={(e) => {
                setSearchValue(e.target.value);
              }}
            />
            <RoundedButton
              variant="contained"
              onClick={handleClickAdvancedSearchOpen}
              children="Advanced Search"
            />
            <RoundedButton
              variant="contained"
              endIcon={<Add />}
              onClick={() => {
                navigate("observation/add");
              }}
              children="Add New Plan"
            />
          </Box>
        </Box>
        {openAdvancedSearch && (
          <AdvancedSearch
            props={advancedSearchProps}
            filter={filter}
            description="Find observation plans with..."
            onFilterChange={setFilter}
            onApply={getObservationList}
            onClose={setOpenAdvancedSearch}
          />
        )}
        <CustomTable
          title={"OBSERVATION"}
          headcells={headCells}
          body={mapDataToRender()}
          paging={{
            page: page,
            rowsPerPage: rowsPerPage,
            rowsCount: rowsCount,
          }}
          loading={isLoading}
          toolbar={false}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Box>
    </>
  );
};
