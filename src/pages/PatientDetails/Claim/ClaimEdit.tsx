import AddIcon from "@mui/icons-material/Add";
import { Box, Button, styled, TextField, Typography } from "@mui/material";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import React from "react";
import { useSelector } from "react-redux";
import { useNavigate, useSearchParams } from "react-router-dom";
import Loading from "../../../components/Loading";
import { IRootState } from "../../../store/reducers";
import { useAppDispatch } from "../../../store/Store";

import { claimsModel } from "../../../models/claims";
import {
  getClaimDataByID,
  postClaimData,
  updateClaimData
} from "../../../store/reducers/claim/claim";
import { StyledArrowBack } from "../style";
import { claimService } from "../../../services/agent";

export const ClaimEdit: React.FC = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const navigate = useNavigate();
  const id = searchParams.get("id");
  const [loading, setLoading] = React.useState<boolean>(id? true: false);

  const [startPeriod, setStartPeriod] = React.useState<string>(
    new Date().toISOString().replace(/.\d+Z$/g, "Z")
  );
  const [endPeriod, setEndPeriod] = React.useState<string>(
    new Date().toISOString().replace(/.\d+Z$/g, "Z")
  );
  const [formValue, setFormValue] = React.useState<{ [key: string]: string }>({
    startPeriod: startPeriod,
    endPeriod: endPeriod,
  });

  //declare function
  const handleStartChange = (newValue: Date | null) => {
    setStartPeriod(newValue?.toISOString().replace(/.\d+Z$/g, "Z") as string);
    setFormValue({
      ...formValue,
      startPeriod: startPeriod as unknown as string,
    });
  };
  const handleEndChange = (newValue: Date | null) => {
    setEndPeriod(newValue?.toISOString().replace(/.\d+Z$/g, "Z") as string);
    setFormValue({
      ...formValue,
      endPeriod: endPeriod as unknown as string,
    });
  };
  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setFormValue({
      ...formValue,
      [event.currentTarget.name]: event.currentTarget.value,
    });
    console.log(formValue);
  };
  const handleSelectChange = (event: SelectChangeEvent) => {
    setFormValue({ ...formValue, [event.target.name]: event.target.value });
  };
  const handleSubmit = () => {
    //mapping value to submit
    const submitValue = {
      status: formValue?.status,
      patient: {
        reference: "Patient/1",
      },
      prescription: {
        reference: "http://pharmacy.org/MedicationRequest/AB1234G",
      },
      originalPrescription: {
        reference: "http://pharmacy.org/MedicationRequest/AB1202B",
      },
      identifier: [
        {
          system: "http://happypharma.com/claim",
          value: formValue?.identifier,
        },
      ],
      type: {
        coding: [
          {
            system: "http://terminology.hl7.org/CodeSystem/claim-type",
            code: formValue?.type,
          },
        ],
      },
      use: formValue?.use,
    } as claimsModel;

    if (formValue?.identifier) {
      setLoading(true);
      if (id)
        claimService
          .update({ body: submitValue }, id)
          .then(() => navigate(`/admin/patient/details?id=${id}`, { state: 4 }))
          .catch(() => setLoading(false));
      else
        claimService
          .post({ body: submitValue })
          .then(() => navigate(`/admin/patient/details?id=${id}`, { state: 4 }))
          .catch(() => setLoading(false));
    }
  };

  //styling
  const style = {
    "& .MuiOutlinedInput-root": {
      height: "100%",
      "& fieldset": {
        border: "1px solid #EAEAEA",
        borderRadius: "0.8rem",
        padding: 0,
        margin: 0,
      },
      "&:hover fieldset": {
        border: "1px solid #dbd7d7",
      },
      "&.Mui-focused fieldset": {
        border: "1px solid #EAEAEA",
      },
    },
  };
  const StyledButton = styled(Button)(({}) => ({
    margin: "0",
    color: "#FFFFFF",
    fontWeight: "700",
    fontSize: "1rem",
    lineHeight: "1.2rem",
    backgroundColor: "#6C5DD3",
    borderRadius: "6rem",
    padding: "10px 16px",
    whiteSpace: "nowrap",
    "&:hover": {
      backgroundColor: "#887ae6",
    },
  }));
  //Field header Mapping
  const claimDetailsFields = [
    {
      name: "identifier",
      value: formValue?.identifier,
      label: "Identifier",
      required: true,
    },
    {
      name: "type",
      value: formValue?.type,
      label: "Type",
      required: false,
    },
  ];
  const coverageFields = [
    {
      name: "use",
      value: formValue?.use,
      label: "Use",
      required: false,
    },
    {
      name: "totalValue",
      value: formValue?.totalValue,
      label: "Total Value",
      required: false,
    },
  ];
  const Status = [
    { value: "Active", display: "Active" },
    { value: "Not Active", display: "Not Active" },
  ];

  const Currency = [
    { value: "USD", display: "USD" },
    { value: "EURO", display: "EURO" },
    { value: "POUND", display: "POUND" },
  ];

  React.useEffect(() => {
    if(id) claimService.getById(id).then(res => {
      setFormValue({
        status: "Active",
      });
      setLoading(false);
    }).catch(() => history.back())
  }, [id]);

  return (
    <Box>
      {/* Header */}
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <Typography sx={{ fontWeight: 700, fontSize: 24, display: "flex", alignItems: "center", gap:"16px" }}>
          <StyledArrowBack onClick={() =>  history.back()}/>
          ADD CLAIM
        </Typography>
        <StyledButton onClick={handleSubmit}>
          {searchParams.get("id") ? (
            <>Save changes</>
          ) : (
            <>
              {" "}
              Add <AddIcon style={{ marginLeft: "10px" }} />
            </>
          )}
        </StyledButton>
      </Box>
      {/* Body */}
      {loading && <Loading />}
      {!loading && (
        <Box
          style={{
            display: "flex",
            justifyContent: "space-between",
            background: "white",
            borderRadius: "16px",
            margin: "16px 0",
            padding: "16px",
          }}
        >
          {/* Left Col */}
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              gap: "16px",
              width: "49%",
            }}
          >
            <Typography sx={{ fontWeight: 600, fontSize: 20 }}>
              General Information
            </Typography>
            {claimDetailsFields.map((item) => (
              <>
                <TextField
                  name={item.name}
                  value={item.value}
                  onChange={handleInputChange}
                  label={item.label}
                  required={item.required}
                  error={item.required ? !item.value : false}
                  sx={style}
                />
              </>
            ))}

            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                "& .MuiFormControl-root": {
                  flexBasis: "100%",
                },
                ...style,
              }}
            >
              <FormControl>
                <InputLabel id="status-select-label">Status</InputLabel>
                <Select
                  labelId="status-select-label"
                  id="status-select"
                  value={formValue?.status}
                  label="Status"
                  name="status"
                  onChange={handleSelectChange}
                  required
                  error={!formValue?.status}
                >
                  {Status.map(({ value, display }) => (
                    <MenuItem value={value}>{display}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Box>
          </Box>
          {/* Right Col*/}
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              gap: "16px",
              width: "49%",
            }}
          >
            <Typography sx={{ fontWeight: 600, fontSize: 20 }}>
              More Information
            </Typography>
            {coverageFields.map((item) => (
              <TextField
                name={item.name}
                value={item.value}
                onChange={handleInputChange}
                label={item.label}
                required={item.required}
                error={item.required ? !item.value : false}
                sx={style}
              />
            ))}
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                "& .MuiFormControl-root": {
                  flexBasis: "100%",
                },
                ...style,
              }}
            >
              <FormControl>
                <InputLabel id="currency-select-label">Currency</InputLabel>
                <Select
                  labelId="currency-select-label"
                  id="currency-select"
                  value={formValue?.currency}
                  label="Currency"
                  name="currency"
                  onChange={handleSelectChange}
                  required
                  error={!formValue?.currency}
                >
                  {Currency.map(({ value, display }) => (
                    <MenuItem value={value}>{display}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Box>
          </Box>
        </Box>
      )}
    </Box>
  );
};
