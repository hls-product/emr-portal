import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import { IconButton } from "@mui/material";
import { styled } from "@mui/system";
import React from "react";
import { Link, useSearchParams } from "react-router-dom";
import Loading from "../../../components/Loading";
import { claimService } from "../../../services/agent";

interface TableProps {
  identifier: string;
  type: string;
  use: string;
  note: string;
  product: string;
  currency: string;
  status: any;
  servicedDate: string;
  id: string;
  totalValue: string;
}

const Table = styled("table")({
  background: "white",
  flex: 1,
  padding: "8px 16px",
  borderRadius: "16px",
  "& tr": {
    lineHeight: "22px",
  },
  "& th": {
    padding: "8px 0",
    textAlign: "left",
    lineHeight: "22px",
  },
  "& td": {
    padding: "8px 0",
    textAlign: "right",
  },
});

const DetailTable: React.FC<TableProps> = ({ ...props }) => (
  <div
    style={{
      display: "flex",
      justifyContent: "space-between",
      gap: "20px",
    }}
  >
    <Table>
      <tr>
        <th>ID</th>
        <td>{props.id}</td>
      </tr>
      <tr>
        <th>Identifier</th>
        <td>{props.identifier}</td>
      </tr>
      <tr>
        <th>Type</th>
        <td>{props.type}</td>
      </tr>
      <tr>
        <th>Use</th>
        <td>{props.use}</td>
      </tr>
      <tr>
        <th>Note</th>
        <td>{props.note}</td>
      </tr>
    </Table>
    <Table>
      <tr>
        <th>Product</th>
        <td>{props.product}</td>
      </tr>
      <tr>
        <th>Status</th>
        <td>
          <div
            style={{
              padding: "3px 12px",
              border: "1px solid rgba(255, 115, 0, 1)",
              color: "rgba(255, 115, 0, 1)",
              borderRadius: "21px",
              width: "fit-content",
              float: "right",
            }}
          >
            {props.status}
          </div>
        </td>
      </tr>
      <tr>
        <th>Serviced Date</th>
        <td>{props.servicedDate}</td>
      </tr>
      <tr>
        <th>Total value</th>
        <td>{props.totalValue}</td>
      </tr>
      <tr>
        <th>Currency</th>
        <td>
          <div
            style={{
              padding: "3px 12px",
              border: "1px solid rgba(0,0,255,0.3)",
              color: "rgba(0,0,255,0.3)",
              borderRadius: "21px",
              width: "fit-content",
              float: "right",
            }}
          >
            {props.currency}
          </div>
        </td>
      </tr>
    </Table>
  </div>
);

export const ClaimDetail: React.FC = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const id = searchParams.get("id");
  const [loading, setLoading] = React.useState<boolean>(true);

  const [data, setData] = React.useState<TableProps>({
    identifier: "",
    type: "",
    use: "",
    note: "",
    product: "",
    currency: "",
    status: "",
    servicedDate: "",
    id: "",
    totalValue: "",
  });

  React.useEffect(() => {
    if (!id) throw history.back();
    claimService
      .getById(id)
      .then((res) => {
        setData(() => {
          const newData = {
            identifier: res.identifier
              ? (res.identifier[0].value as string)
              : "N/A",
            type: res.type
              ? res.type.coding
                ? res.type.coding[0].code
                  ? ((res.type.coding[0].code[0].toUpperCase() +
                      res.type.coding[0].code.substring(1)) as string)
                  : "N/A"
                : "N/A"
              : "N/A",
            use: res.use
              ? ((res.use[0].toUpperCase() + res.use.substring(1)) as string)
              : "N/A",
            status: res.status as string,
            note: "Nothing to note",
            product: "Alprazolam 0.25mg (Xanax)",
            currency: "USD",
            servicedDate: "2020-02-07",
            totalValue: "90",
            id: res.id as string,
          } as TableProps;
          return newData;
        });
        setLoading(false);
      })
      .catch(() => history.back());
  }, [id]);

  return (
    <div>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <div
          style={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <Link to={`/admin/patient/details?id=${data.id}`} state={8}>
            <IconButton
              style={{
                color: "rgba(108, 93, 211, 1)",
                background: "white",
                borderRadius: "50%",
                width: "40px",
                height: "40px",
                marginRight: "16px",
              }}
            >
              <ArrowBackIcon />
            </IconButton>
          </Link>
          <h1 style={{ fontSize: "24px" }}>Claim Details</h1>
        </div>
        <Link
          to={`update?id=${data.id}`}
          style={{
            padding: "10px 30px",
            borderRadius: "30px",
            color: "rgba(108, 93, 211, 1)",
            background: "white",
            textDecoration: "none",
          }}
        >
          <EditOutlinedIcon style={{ marginRight: "12px" }} />
          Edit
        </Link>
      </div>
      {loading && <Loading></Loading>}
      {!loading && <DetailTable {...data} />}
    </div>
  );
};
