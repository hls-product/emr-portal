import { Add, Create, Delete, Search, Visibility } from "@mui/icons-material";
import {
  Box,
  IconButton,
  InputAdornment,
  TextField,
  Typography,
} from "@mui/material";
import React from "react";
import { Link, useNavigate } from "react-router-dom";
import { DeleteDialog } from "../../../components/DeleteDialog";
import { RoundedButton } from "../../../components/RoundedButton";
import { AdvancedSearch } from "../../../components/Table/AdvancedSearch";
import { CustomTable } from "../../../components/Table/Table";
import { HeadCell } from "../../../components/Table/types";
import { claimsModel } from "../../../models/claims";
import { claimService } from "../../../services/agent";

const headCells: HeadCell[] = [
  {
    id: "number",
    numeric: false,
    label: "No.",
  },
  {
    id: "identifier",
    numeric: false,
    label: "Identifier",
  },
  {
    id: "type",
    numeric: false,
    label: "Type",
  },
  {
    id: "use",
    numeric: false,
    label: "Use",
  },
  {
    id: "created",
    numeric: false,
    label: "Created",
  },
  {
    id: "payee",
    numeric: false,
    label: "Payee",
  },
  {
    id: "status",
    numeric: false,
    label: "Status",
  },
  {
    id: "action",
    numeric: false,
    label: "Action",
  },
];
const advancedSearchProps = [
  {
    name: "status",
    label: "Status",
    select: true,
    menuItems: [
      {
        value: "active",
        display: "Active",
      },
      {
        value: "cancelled",
        display: "Cancelled",
      },
      {
        value: "draft",
        display: "Draft",
      },
      {
        value: "entered-in-error",
        display: "Entered-in-Error",
      },
    ],
  },
  {
    name: "type",
    label: "Type",
    select: true,
    menuItems: [
      {
        value: "institutional",
        display: "Institutional",
      },
      {
        value: "oral",
        display: "Oral",
      },
      {
        value: "pharmacy",
        display: "Pharmacy",
      },
      {
        value: "professional",
        display: "Professional",
      },
      {
        value: "vision",
        display: "Vision",
      },
    ],
  },
  {
    name: "use",
    label: "Use",
    select: true,
    menuItems: [
      {
        value: "claim",
        display: "Claim",
      },
      {
        value: "preauthorization",
        display: "Preauthorization",
      },
      {
        value: "predetermination",
        display: "Predetermination",
      },
    ],
  },
];

export const Claim: React.FC = () => {
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = React.useState(false);
  const [data, setData] = React.useState<claimsModel[]>([]);
  const [page, setPage] = React.useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = React.useState<number>(10);
  const [rowsCount, setRowsCount] = React.useState<number>(0);
  const [deletePopUpOpen, setDeletePopUpOpen] = React.useState(false);
  const [openAdvancedSearch, setOpenAdvancedSearch] = React.useState(false);
  const [currentSelectedId, setCurrentSelectedId] = React.useState("");

  // Search states
  const [searchValue, setSearchValue] = React.useState<string>("");
  const [debounceSeachValue, setDebounceSearchValue] =
    React.useState<string>("");
  const [filter, setFilter] = React.useState<{ [key: string]: string }>({});

  const getClaimList = async (filter?: {
    [keys: string]: string;
  }) => {
    const params = {
      ...filter,
      page: page + 1,
      size: rowsPerPage,
      name: debounceSeachValue,
    };
    setIsLoading(true);
    try {
      let { data, paging } = await claimService.getList({ params });
      setData(data);
      setRowsCount(paging.total);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      console.log("getClaimList error: ", error);
    }
  };
  const deleteClaim = async (id: string) => {
    setIsLoading(true);
    try {
      let response = await claimService.delete(id);
      setIsLoading(false);
      return response;
    } catch (error) {
      setIsLoading(false);
      console.log("delete claim error: ", error);
    }
  };

  // Event Handler
  const handleChangePage = (event: any, newPage: number) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event: any) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleDelete = (id: string) => {
    setDeletePopUpOpen(true);
    setCurrentSelectedId(id);
  };
  const handleCancelDelete = () => {
    setDeletePopUpOpen(false);
  };
  const handleConfirmDelete = () => {
    setDeletePopUpOpen(false);
    deleteClaim(currentSelectedId).then((res) => {
      setCurrentSelectedId("");
      getClaimList();
    });
  };
  const handleClickAdvancedSearchOpen = () => {
    setOpenAdvancedSearch(true);
  };
  //Debounce for name search
  React.useEffect(() => {
    let timeout = setTimeout(() => {
      setDebounceSearchValue(searchValue);
    }, 500);
    return () => {
      clearTimeout(timeout);
    };
  }, [searchValue]);

  // Back to previous page if current page is empty after deleting an item
  React.useEffect(() => {
    let totalPages = Math.ceil(rowsCount / rowsPerPage);
    let currentPage = page + 1;
    if (currentPage > totalPages && totalPages > 0) {
      setPage(totalPages - 1);
    }
  }, [rowsCount]);
  React.useEffect(() => {
    getClaimList();
  }, [debounceSeachValue, rowsPerPage, page]);

  const mapDataToRender = () => {
    const render = data.map((item, index) => ({
      id: item.id as string,
      number: index + 1,
      identifier: item.identifier?.[0].value,
      type: <Typography textTransform="capitalize" children={item.type?.coding?.[0]?.code} />,
      use: <Typography textTransform="capitalize" children={item.use} />,
      created: item.created,
      payee: item.payee?.language,
      status: <Typography textTransform="capitalize" children={item.status} />,
      action: (
        <Box style={{ textAlign: "center", whiteSpace: "nowrap" }}>
          <Link
            to={`claim/update?id=${item.id}`}
            children={<IconButton children={<Create />} />}
          />
          <Link
            to={`claim?id=${item.id}`}
            children={<IconButton children={<Visibility />} />}
          />

          <Link
            to={`#`}
            onClick={() => {
              handleDelete(item.id as string);
            }}
            children={<IconButton children={<Delete />} />}
          />
        </Box>
      ),
    }));
    return render;
  };

  return (
    <>
      {/* Dialog for delete item popup */}
      <DeleteDialog
        open={deletePopUpOpen}
        onClose={handleCancelDelete}
        onConfirm={handleConfirmDelete}
        onCancel={handleCancelDelete}
      />
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          width: "100%",
          padding: "20px",
          backgroundColor: "white",
          borderRadius: "16px",
        }}
      >
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            gap: "8px",
          }}
        >
          <Typography variant="h2" children={"CLAIM"} />
          <Box sx={{ display: "flex", gap: "8px" }}>
            <TextField
              placeholder="Search claim..."
              InputProps={{
                startAdornment: (
                  <InputAdornment
                    position="start"
                    children={<Search sx={{ height: 20 }} />}
                  />
                ),
              }}
              sx={{
                "& .MuiInputBase-root": {
                  height: 48,
                  width: 331,
                  borderRadius: 3,
                },
              }}
              value={searchValue}
              onChange={(e) => {
                setSearchValue(e.target.value);
              }}
            />
            <RoundedButton
              variant="contained"
              onClick={handleClickAdvancedSearchOpen}
              children="Advanced Search"
            />
            <RoundedButton
              variant="contained"
              endIcon={<Add />}
              onClick={() => {
                navigate("claim/add");
              }}
              children="Add New Claim"
            />
          </Box>
        </Box>
        {openAdvancedSearch && (
          <AdvancedSearch
            props={advancedSearchProps}
            filter={filter}
            description="Find claim plans with..."
            onFilterChange={setFilter}
            onApply={getClaimList}
            onClose={setOpenAdvancedSearch}
          />
        )}
        <CustomTable
          title={"CLAIM"}
          headcells={headCells}
          body={mapDataToRender()}
          paging={{
            page: page,
            rowsPerPage: rowsPerPage,
            rowsCount: rowsCount,
          }}
          loading={isLoading}
          toolbar={false}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Box>
    </>
  );
};
