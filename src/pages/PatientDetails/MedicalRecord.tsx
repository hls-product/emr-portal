import React from "react";
import {
  Grid,
  Typography,
  Table,
  TableContainer,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Box,
  Button,
} from "@mui/material";
import { Line } from "react-chartjs-2";

import { Header } from "../../layout/Header";
import { Content } from "../../layout/Content";
import { AdminFooter } from "../../layout/Footer";
import { Sidebar } from "../../layout/Sidebar";
import { StyledInput } from "../../layout/Header/style";

import secondarySearchIcon from "../../assets/adminHeader/secondarySearchIcon.svg";
import human_body from "../../assets/image/human_body.svg";
import heart_icon from "../../assets/icon/heart_icon.svg";
import oxygen_icon from "../../assets/icon/oxygen_icon.svg";
import thermometer_icon from "../../assets/icon/thermometer_icon.svg";

interface TreatmentType {
  id: number;
  OPDNo: number;
  caseId: number;
  appointmentDate: string;
  consultant: string;
  symptoms: string;
}

interface MedicationType {
  id: number;
  OPDNo: number;
  date: string;
  doctor: string;
  shipMethod: "Order" | "Pickup" | "None";
  status: "Ordered" | "Shipping" | "Cancelled" | "Pending" | "Delivered";
}

interface TestType {
  id: number;
  test: string;
  date: string;
}

interface BodyStatsType {
  id: number;
  label: string;
  value: number;
  measure?: string;
  icon?: string;
}

interface chartType {
  labels: string[];
  datasets: [
    {
      label: string;
      data: number[];
      fill: boolean;
      backgroundColor: string;
      borderColor: string;
      tension: number;
    }
  ];
}

const BloodPressure: chartType = {
  labels: [
    "01/08/2022",
    "02/08/2022",
    "03/08/2022",
    "04/08/2022",
    "05/08/2022",
    "06/08/2022",
    "07/08/2022",
    "08/08/2022",
    "09/08/2022",
    "10/08/2022",
    "11/08/2022",
    "12/08/2022",
  ],
  datasets: [
    {
      label: "BloodPressure",
      data: [72, 74, 80, 75, 70, 85, 90, 88, 86, 77, 81, 89],
      fill: false,
      backgroundColor: "#ECF1FF",
      borderColor: "#ECF1FF",
      tension: 0.5,
    },
  ],
};

const optionsRemoveLegend: any = {
  plugins: {
    legend: {
      display: false,
    },
  },
  scales: {
    x: {
      display: false,
      grid: {
        display: false,
        drawBorder: false,
      },
    },
    y: {
      display: false,
      grid: {
        display: false,
        drawBorder: false,
      },
    },
  },
  elements: {
    point: {
      radius: 0,
    },
  },
};

const PhysicalStats: BodyStatsType[] = [
  {
    id: 0,
    label: "Height",
    value: 5.3,
    measure: "in",
  },
  {
    id: 1,
    label: "Weight",
    value: 110,
    measure: "lbs",
  },
  {
    id: 2,
    label: "Average",
    value: 20.4,
    measure: "BMI",
  },
];

const InnerBodyStats: BodyStatsType[] = [
  {
    id: 0,
    label: "Heart beat",
    value: 110,
    icon: heart_icon,
  },
  {
    id: 1,
    label: "Oxygen saturation",
    value: 97,
    icon: oxygen_icon,
  },
  {
    id: 2,
    label: "Boday temperature",
    value: 97.5,
    measure: "F",
    icon: thermometer_icon,
  },
];

const TreatmentHistoryHead: string[] = [
  "OPD No",
  "Case ID",
  "Appointment Date",
  "Consultant",
  "Symptoms",
];

const TreatmentHistory: TreatmentType[] = [
  {
    id: 0,
    OPDNo: 13,
    caseId: 16,
    appointmentDate: "03 May 2021 10:00AM",
    consultant: "Amit Singh (9009)",
    symptoms:
      "Atopic dermatitis (Eczema). Atopic dermatitis usually develops in early childhood and is more common in people who have a family history of the condition.",
  },
  {
    id: 1,
    OPDNo: 13,
    caseId: 16,
    appointmentDate: "03 May 2021 10:00AM",
    consultant: "Amit Singh (9009)",
    symptoms:
      "Atopic dermatitis (Eczema). Atopic dermatitis usually develops in early childhood and is more common in people who have a family history of the condition.",
  },
  {
    id: 2,
    OPDNo: 13,
    caseId: 16,
    appointmentDate: "03 May 2021 10:00AM",
    consultant: "Amit Singh (9009)",
    symptoms:
      "Atopic dermatitis (Eczema). Atopic dermatitis usually develops in early childhood and is more common in people who have a family history of the condition.",
  },
];

const MedicationHead: string[] = [
  "ID",
  "Date & Time",
  "Doctor/Consultant",
  "Shipping Method",
  "Status",
];

const Medications: MedicationType[] = [
  {
    id: 0,
    OPDNo: 13,
    date: "03 May 2021 10:00 AM",
    doctor: "Dr. Strange",
    shipMethod: "Order",
    status: "Ordered",
  },
  {
    id: 1,
    OPDNo: 13,
    date: "03 May 2021 10:00 AM",
    doctor: "Dr. Sleep",
    shipMethod: "Pickup",
    status: "Shipping",
  },
  {
    id: 2,
    OPDNo: 13,
    date: "03 May 2021 10:00 AM",
    doctor: "Dr. Who",
    shipMethod: "None",
    status: "Cancelled",
  },
  {
    id: 3,
    OPDNo: 13,
    date: "03 May 2021 10:00 AM",
    doctor: "Dr. Doom",
    shipMethod: "Order",
    status: "Pending",
  },
  {
    id: 4,
    OPDNo: 13,
    date: "03 May 2021 10:00 AM",
    doctor: "Dr. Star Lord",
    shipMethod: "Pickup",
    status: "Delivered",
  },
  {
    id: 5,
    OPDNo: 13,
    date: "03 May 2021 10:00 AM",
    doctor: "Dr. Steven",
    shipMethod: "None",
    status: "Ordered",
  },
  {
    id: 6,
    OPDNo: 13,
    date: "03 May 2021 10:00 AM",
    doctor: "Dr. D",
    shipMethod: "Order",
    status: "Cancelled",
  },
];

const TestListHead: string[] = ["No", "Test", "Date & Time", "Result"];

const TestList: TestType[] = [
  {
    id: 0,
    test: "Test 1",
    date: "03 May 2021 10:00 AM",
  },
  {
    id: 1,
    test: "Test 2",
    date: "03 May 2021 10:00 AM",
  },
  {
    id: 2,
    test: "Test 3",
    date: "03 May 2021 10:00 AM",
  },
];

const RenderMedicalCard = () => {
  return (
    <Box
      sx={{
        background: "#FFFFFF",
        borderRadius: "16px",
        padding: "16px",
        display: "flex",
        alignItems: "flex-start",
        justifyContent: "flex-start",
        flexDirection: "column",
        gap: "16px",
      }}
    >
      <Typography
        variant="h6"
        component="div"
        sx={{
          fontSize: "16px",
          fontWeight: "600",
          color: "#081735",
          paddingBottom: "16px",
          borderBottom: "1px solid #F4F4F4",
          width: "100%",
          textTransform: "uppercase",
        }}
      >
        Medical card
      </Typography>
      <Grid container spacing={2}>
        <Grid item xs="auto">
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
              width: "148px",
              height: "200px",
            }}
          >
            <img
              src={human_body}
              width={67.55}
              height={175}
              style={{ margin: "12.5px 0" }}
            />
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "space-between",
                height: "100%",
                gap: "16px",
              }}
            >
              {PhysicalStats.map((stat) => (
                <div
                  style={{
                    display: "flex",
                    alignItems: "flex-start",
                    justifyContent: "flex-start",
                    flexDirection: "column",
                  }}
                  key={stat.id}
                >
                  <Typography
                    variant="body2"
                    component="div"
                    sx={{
                      fontSize: "14px",
                      fontWeight: "500",
                      color: "#999999",
                    }}
                  >
                    {stat.measure}
                  </Typography>
                  <Typography
                    variant="body2"
                    component="div"
                    sx={{
                      fontSize: "16px",
                      fontWeight: "600",
                      color: "#FF8050",
                    }}
                  >
                    {stat.value}
                  </Typography>
                  <Typography
                    variant="body2"
                    component="div"
                    sx={{
                      fontSize: "12px",
                      fontWeight: "600",
                      color: "#333333",
                    }}
                  >
                    {stat.label}
                  </Typography>
                </div>
              ))}
            </div>
          </div>
        </Grid>

        <Grid item xs="auto">
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-between",
              height: "230px",
              padding: "16px 20px",
              width: "140px",
              background: "#F0EFFB",
              borderRadius: "16px",
            }}
          >
            {InnerBodyStats.map((stat) => (
              <div
                style={{
                  display: "flex",
                  alignItems: "flex-start",
                  justifyContent: "flex-start",
                  gap: "10px",
                  width: "100%",
                }}
                key={stat.id}
              >
                <div style={{ width: "20px" }}>
                  <img src={stat.icon} width={20} height={20} />
                </div>
                <div>
                  <Typography
                    variant="body2"
                    component="div"
                    sx={{
                      fontSize: "16px",
                      fontWeight: "600",
                      color: "#6C5DD3",
                    }}
                  >
                    {stat.value}
                  </Typography>
                  <Typography
                    variant="body2"
                    component="div"
                    sx={{
                      fontSize: "12px",
                      fontWeight: "600",
                      color: "#6C5DD3",
                    }}
                  >
                    {stat.label}
                  </Typography>
                </div>
              </div>
            ))}
          </div>
        </Grid>

        <Grid item xs="auto">
          <div
            style={{
              display: "flex",
              alignItems: "flex-start",
              justifyContent: "flex-start",
              flexDirection: "column",
              background: "#6C5DD3",
              borderRadius: "16px",
              padding: "16px",
              gap: "5px",
            }}
          >
            <div
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
                width: "100%",
              }}
            >
              <Typography
                variant="body2"
                component="div"
                sx={{
                  fontSize: "16px",
                  fontWeight: "600",
                  color: "#FFFFFF",
                }}
              >
                Blood pressure
              </Typography>
              <Typography
                variant="body2"
                component="div"
                sx={{
                  fontSize: "14px",
                  fontWeight: "500",
                  color: "#FFFFFF",
                }}
              >
                Today
              </Typography>
            </div>
            <Typography
              variant="body2"
              component="div"
              sx={{
                padding: "2px 6px",
                background: "#FFFFFF",
                fontSize: "12px",
                fontWeight: "600",
                color: "#DE0000",
                borderRadius: "20px",
              }}
            >
              Recommended 120/80
            </Typography>
            <Line
              data={BloodPressure}
              options={optionsRemoveLegend}
              width={1200}
            />
          </div>
        </Grid>
      </Grid>
    </Box>
  );
};

const RenderTreatmentHistory = () => {
  return (
    <Box
      sx={{
        background: "#FFFFFF",
        borderRadius: "16px",
        padding: "16px",
        display: "flex",
        alignItems: "flex-start",
        justifyContent: "flex-start",
        flexDirection: "column",
        gap: "16px",
      }}
    >
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          width: "100%",
        }}
      >
        <Typography
          variant="h6"
          component="div"
          sx={{
            textTransform: "uppercase",
            fontSize: "16px",
            fontWeight: "600",
            color: "#081735",
          }}
        >
          Treatment history
        </Typography>
        <div
          style={{
            display: "flex",
            width: "fit-content",
            border: "1px solid #EAEAEA",
            borderRadius: "12px",
          }}
        >
          <img
            src={secondarySearchIcon}
            alt="Search"
            width={20}
            height={20}
            style={{ margin: "14px 0 14px 14px" }}
          />
          <StyledInput
            sx={{ m: 1, width: "fit-content" }}
            placeholder="Search..."
          />
        </div>
      </div>
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow
              sx={{
                background: "#FAFAFA",
              }}
            >
              {TreatmentHistoryHead.map((item) => (
                <TableCell
                  key={item}
                  sx={{
                    fontSize: "14px",
                    fontWeight: "600",
                    color: "#131626",
                  }}
                >
                  {item}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {TreatmentHistory.map((item) => (
              <TableRow key={item.id}>
                <TableCell
                  sx={{
                    borderBottom: "1px solid #EAEAEA",
                  }}
                >
                  OPDN{item.OPDNo}
                </TableCell>
                <TableCell
                  sx={{
                    borderBottom: "1px solid #EAEAEA",
                  }}
                >
                  {item.caseId}
                </TableCell>
                <TableCell
                  sx={{
                    borderBottom: "1px solid #EAEAEA",
                  }}
                >
                  {item.appointmentDate}
                </TableCell>
                <TableCell
                  sx={{
                    borderBottom: "1px solid #EAEAEA",
                  }}
                >
                  {item.consultant}
                </TableCell>
                <TableCell
                  sx={{
                    borderBottom: "1px solid #EAEAEA",
                  }}
                >
                  {item.symptoms}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  );
};

const RenderMedication = () => {
  return (
    <Box
      sx={{
        background: "#FFFFFF",
        borderRadius: "16px",
        padding: "16px",
        display: "flex",
        alignItems: "flex-start",
        justifyContent: "flex-start",
        flexDirection: "column",
        gap: "16px",
      }}
    >
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          width: "100%",
        }}
      >
        <Typography
          variant="h6"
          component="div"
          sx={{
            textTransform: "uppercase",
            fontSize: "16px",
            fontWeight: "600",
            color: "#081735",
          }}
        >
          Medication
        </Typography>
        <div
          style={{
            display: "flex",
            width: "fit-content",
            border: "1px solid #EAEAEA",
            borderRadius: "12px",
          }}
        >
          <img
            src={secondarySearchIcon}
            alt="Search"
            width={20}
            height={20}
            style={{ margin: "14px 0 14px 14px" }}
          />
          <StyledInput
            sx={{ m: 1, width: "fit-content" }}
            placeholder="Search..."
          />
        </div>
      </div>
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow
              sx={{
                background: "#FAFAFA",
              }}
            >
              {MedicationHead.map((item) => (
                <TableCell
                  key={item}
                  sx={{
                    fontSize: "14px",
                    fontWeight: "600",
                    color: "#131626",
                  }}
                >
                  {item}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {Medications.map((item) => (
              <TableRow key={item.id}>
                <TableCell
                  sx={{
                    borderBottom: "1px solid #EAEAEA",
                  }}
                >
                  OPDN{item.OPDNo}
                </TableCell>
                <TableCell
                  sx={{
                    borderBottom: "1px solid #EAEAEA",
                  }}
                >
                  {item.date}
                </TableCell>
                <TableCell
                  sx={{
                    borderBottom: "1px solid #EAEAEA",
                  }}
                >
                  {item.doctor}
                </TableCell>
                <TableCell
                  sx={{
                    borderBottom: "1px solid #EAEAEA",
                  }}
                >
                  <Typography
                    variant="body2"
                    component="div"
                    sx={{
                      padding: "3px 12px",
                      borderRadius: "21px",
                      background: "#FFFFFF",
                      border: `1px solid ${
                        item.shipMethod === "Order"
                          ? "#BAE5C8"
                          : item.shipMethod === "Pickup"
                          ? "#B7D9F4"
                          : "#EAEAEA"
                      }`,
                      color: `${
                        item.shipMethod === "Order"
                          ? "#22AA4F"
                          : item.shipMethod === "Pickup"
                          ? "#1685DA"
                          : "#131626"
                      }`,
                      width: "fit-content",
                    }}
                  >
                    {item.shipMethod}
                  </Typography>
                </TableCell>
                <TableCell
                  sx={{
                    borderBottom: "1px solid #EAEAEA",
                  }}
                >
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "flex-start",
                      gap: "10px",
                    }}
                  >
                    <div
                      style={{
                        width: "12px",
                        height: "12px",
                        borderRadius: "50%",
                        background: `${
                          item.status === "Ordered"
                            ? "#6C5DD3"
                            : item.status === "Shipping"
                            ? "#1685DA"
                            : item.status === "Cancelled"
                            ? "#E03535"
                            : item.status === "Pending"
                            ? "#FF7300"
                            : "#22AA4F"
                        }`,
                      }}
                    ></div>
                    {item.status}
                  </div>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  );
};

const RenderTestList = () => {
  return (
    <Box
      sx={{
        background: "#FFFFFF",
        borderRadius: "16px",
        padding: "16px",
        display: "flex",
        alignItems: "flex-start",
        justifyContent: "flex-start",
        flexDirection: "column",
        gap: "16px",
      }}
    >
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          width: "100%",
        }}
      >
        <Typography
          variant="h6"
          component="div"
          sx={{
            textTransform: "uppercase",
            fontSize: "16px",
            fontWeight: "600",
            color: "#081735",
          }}
        >
          TEST LIST
        </Typography>
        <div
          style={{
            display: "flex",
            width: "fit-content",
            border: "1px solid #EAEAEA",
            borderRadius: "12px",
          }}
        >
          <img
            src={secondarySearchIcon}
            alt="Search"
            width={20}
            height={20}
            style={{ margin: "14px 0 14px 14px" }}
          />
          <StyledInput
            sx={{ m: 1, width: "fit-content" }}
            placeholder="Search..."
          />
        </div>
      </div>
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow
              sx={{
                background: "#FAFAFA",
              }}
            >
              {TestListHead.map((item) => (
                <TableCell
                  key={item}
                  sx={{
                    fontSize: "14px",
                    fontWeight: "600",
                    color: "#131626",
                  }}
                >
                  {item}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {TestList.map((item, index) => (
              <TableRow key={item.id}>
                <TableCell
                  sx={{
                    borderBottom: "1px solid #EAEAEA",
                  }}
                >
                  {index + 1}
                </TableCell>
                <TableCell
                  sx={{
                    borderBottom: "1px solid #EAEAEA",
                  }}
                >
                  {item.test}
                </TableCell>
                <TableCell
                  sx={{
                    borderBottom: "1px solid #EAEAEA",
                  }}
                >
                  {item.date}
                </TableCell>
                <TableCell
                  sx={{
                    borderBottom: "1px solid #EAEAEA",
                  }}
                >
                  <Button
                    sx={{
                      padding: "10px 24px",
                      borderRadius: "99px",
                      border: "1px solid #F4F4F4",
                    }}
                  >
                    View result
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  );
};

export const MedicalRecord: React.FC = () => {
  const [open, setOpen] = React.useState<boolean>(true);

  const handleInteractSidebar = () => {
    setOpen(!open);
  };

  return (
    <Box>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <RenderMedicalCard />
        </Grid>
        <Grid item xs={12}>
          <RenderTreatmentHistory />
        </Grid>
        <Grid item xs={12}>
          <RenderMedication />
        </Grid>
        <Grid item xs={12}>
          <RenderTestList />
        </Grid>
      </Grid>
    </Box>
  );
};
