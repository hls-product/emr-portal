export { Details } from "./Information/Details";
export { Appointment } from "./Appointment/Appointment";
export { PatientDetails } from "./PatientDetails";
export { Immunization } from "./Immunization/MedicalRecord";
export { Claim } from "./Claim/Claim";
export { DiagnosticReports } from "./DiagnosticReports/DiagnosticReports";
// export {CarePlan} from './CarePlan/CarePlan'
// export {CarePlan} from './CareTeam/CareTeam'

export { ImmunizationEdit } from "./Immunization/ImmunizationEdit";
export { InsuranceAdd } from "./Insurance/InsuranceAdd";
export { InsuranceDetails } from "./Insurance/InsuranceDetails";
export { RelatedPatient } from "./RelatedPatient/RelatedPatient";
export { PatientAppointment } from "../PatientDetails/Appointment/PatientAppointment";
export { AddAppointment } from "../PatientDetails/Appointment/AddAppointment";
export { PatientAppointmentEdit } from "../PatientDetails/Appointment/PatientAppointmentEdit";
export { MedicalDetail } from "./Immunization/MedicalDetail";
export { RelatedPatientEdit } from "./RelatedPatient/RelatedPatientEdit";
export { DiagnosticReportsEdit } from "./DiagnosticReports/DiagnosticReportsEdit";
export { DiagnosticReportsDetail } from "./DiagnosticReports/DiagnosticReportsDetail";
export { ClaimEdit } from "./Claim/ClaimEdit";
export { ClaimDetail } from "./Claim/ClaimDetail";
export { Encounter } from './Encounter/Encounter';
export { EncounterDetail } from "./Encounter/EncounterDetail";
export { EncounterAdd } from "./Encounter/EncounterAdd";
export { ConditionDetail } from "./Condition/ConditionDetail";
export { ConditionAdd } from "./Condition/ConditionAdd";