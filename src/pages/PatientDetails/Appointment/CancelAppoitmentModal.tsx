import * as React from 'react';
import Backdrop from '@mui/material/Backdrop';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Fade from '@mui/material/Fade';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import TextField from '@mui/material/TextField';
import {Button} from "@mui/material";
import {styled} from '@mui/system'
const style = {
    display: "flex",
    flexDirection: "column",
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 500,
    background: "rgba(255, 255, 255, 1)",
    borderRadius: "16px",
    padding:"24px 20px"
};
interface Props{
    open: boolean;
    toggleModal: () => void;
}

export const CancelAppointmentModal:React.FC<Props> = ({open, toggleModal}) => {
    const [value, setValue] = React.useState('');
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setValue(event.target.value);
    };
    const CSSButton=styled(Button)({
        background:"rgba(108, 93, 211, 1)",
        color: "white",
        padding: "15px 0",
        borderRadius: "100px",
        marginTop: "24px",
        '&:hover': {
            background:"rgba(108, 93, 211, 1)",
            color: "white",
            cursor: "pointer"
        }
    })

    return (
        <div>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                open={open}
                onClose={toggleModal}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 300,
                }}
            >
                <Fade in={open}>
                    <Box sx={style}>
                        <span
                            style={{
                                fontWeight: 600,
                                color:"rgba(19, 22, 38, 1)" ,
                                fontSize: "18px",
                                marginBottom: "24px"
                            }}>Cancel Appointment</span>
                        <FormControl>
                            <FormLabel
                                id="cancel-appointment-label"
                                sx={{
                                    fontWeight: 600,
                                    color:"rgba(19, 22, 38, 1)" ,
                                    fontSize: "16px"
                                }}>
                                Cancel Reason
                            </FormLabel>
                            <RadioGroup
                                aria-labelledby="cancel-appointment-label"
                                defaultValue="female"
                                name="radio-buttons-group"
                            >
                                <FormControlLabel value="request-by-patient" control={<Radio />} label="Requested by Patient" />
                                <FormControlLabel value="request-by-doctor" control={<Radio />} label="Requested by Doctor" />
                                <FormControlLabel value="error-system" control={<Radio />} label="Error System" />
                                <FormControlLabel value="other" control={<Radio />} label="Other" />
                            </RadioGroup>
                            <TextField
                                id="outlined-multiline-flexible"
                                label="Other reason..."
                                multiline
                                maxRows={4}
                                value={value}
                                onChange={handleChange}
                                fullWidth
                                sx={{marginTop: "16px"}}
                                />
                        </FormControl>
                        <CSSButton
                            onClick={toggleModal}
                        >Confirm</CSSButton>
                    </Box>
                </Fade>
            </Modal>
        </div>
    );
}