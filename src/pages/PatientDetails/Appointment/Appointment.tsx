import { Add, Create, Delete, Search, Visibility } from "@mui/icons-material";
import {
  Box,
  IconButton,
  InputAdornment,
  TextField,
  Typography
} from "@mui/material";
import React from "react";
import { Link, useNavigate } from "react-router-dom";
import { RoundedButton } from "../../../components/RoundedButton";

import { DeleteDialog } from "../../../components/DeleteDialog";
import { AdvancedSearch } from "../../../components/Table/AdvancedSearch";
import { CustomTable } from "../../../components/Table/Table";
import { HeadCell } from "../../../components/Table/types";
import { appointmentModel } from "../../../models/appointment";
import { appointmentService } from "../../../services/agent";
import { colorPalette } from "../../../theme";

const headCells: HeadCell[] = [
  {
    id: "number",
    numeric: false,
    label: "No.",
  },
  {
    id: "doctor",
    numeric: false,
    label: "Doctor",
  },
  {
    id: "start",
    numeric: false,
    label: "Start at",
  },
  {
    id: "duration",
    numeric: false,
    label: "Duration",
  },
  {
    id: "description",
    numeric: false,
    label: "Description",
  },
  {
    id: "status",
    numeric: false,
    label: "Status",
  },
  {
    id: "action",
    numeric: false,
    label: "Action",
  },
];
const advancedSearchProps = [
  {
    name: "status",
    label: "Status",
    select: true,
    menuItems: [
      {
        value: "proposed",
        display: "Proposed",
      },
      {
        value: "pending",
        display: "Pending",
      },
      {
        value: "booked",
        display: "Booked",
      },
      {
        value: "arrived",
        display: "Arrived",
      },
      {
        value: "fulfilled",
        display: "Fulfilled",
      },
      {
        value: "cancelled",
        display: "Cancelled",
      },
      {
        value: "noshow",
        display: "Noshow",
      },
      {
        value: "entered-in-error",
        display: "Entered-in-Error",
      },
      {
        value: "checked-in",
        display: "Checked-in",
      },
      {
        value: "waitlist",
        display: "Waitlist",
      },
    ],
  },
  {
    name: "start",
    dateTime: "single",
    inputFormat: "MM-DD-YYYY",
    label: "Start at",
  },
];

export const Appointment: React.FC = () => {
  const navigate = useNavigate();
  // useState
  const [isLoading, setIsLoading] = React.useState(false);
  const [data, setData] = React.useState<appointmentModel[]>([]);
  const [page, setPage] = React.useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = React.useState<number>(10);
  const [rowsCount, setRowsCount] = React.useState<number>(0);
  const [deletePopUpOpen, setDeletePopUpOpen] = React.useState(false);
  const [openAdvancedSearch, setOpenAdvancedSearch] = React.useState(false);
  const [currentSelectedId, setCurrentSelectedId] = React.useState("");

  // Search states
  const [searchValue, setSearchValue] = React.useState<string>("");
  const [debounceSeachValue, setDebounceSearchValue] =
    React.useState<string>("");
  const [filter, setFilter] = React.useState<{ [key: string]: string }>({});

  const getAppointmentList = async (filter?: { [keys: string]: string }) => {
    const params = {
      ...filter,
      page: page + 1,
      size: rowsPerPage,
      name: debounceSeachValue,
    };
    setIsLoading(true);
    try {
      let { data, paging } = await appointmentService.getList({ params });
      setData(data);
      setRowsCount(paging.total);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      console.log("getAppointmentList error: ", error);
    }
  };
  const deleteAppointment = async (id: string) => {
    setIsLoading(true);
    try {
      let response = await appointmentService.delete(id);
      setIsLoading(false);
      return response;
    } catch (error) {
      setIsLoading(false);
      console.log("deleteAppointment error: ", error);
    }
  };

  // Event Handler
  const handleChangePage = (event: any, newPage: number) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event: any) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleDelete = (id: string) => {
    setDeletePopUpOpen(true);
    setCurrentSelectedId(id);
  };
  const handleCancelDelete = () => {
    setDeletePopUpOpen(false);
  };
  const handleConfirmDelete = () => {
    setDeletePopUpOpen(false);
    deleteAppointment(currentSelectedId).then((res) => {
      setCurrentSelectedId("");
      getAppointmentList();
    });
  };
  const handleClickAdvancedSearchOpen = () => {
    setOpenAdvancedSearch(true);
  };
  //Debounce for name search
  React.useEffect(() => {
    let timeout = setTimeout(() => {
      setDebounceSearchValue(searchValue);
    }, 500);
    return () => {
      clearTimeout(timeout);
    };
  }, [searchValue]);

  // Back to previous page if current page is empty after deleting an item
  React.useEffect(() => {
    let totalPages = Math.ceil(rowsCount / rowsPerPage);
    let currentPage = page + 1;
    if (currentPage > totalPages && totalPages > 0) {
      setPage(totalPages - 1);
    }
  }, [rowsCount]);
  React.useEffect(() => {
    getAppointmentList();
  }, [debounceSeachValue, rowsPerPage, page]);

  const StatusCircle = (status: string) => {
    const theme = {
      booked: colorPalette.purple.shade_500,
      pending: colorPalette.orange.shade_500,
      fulfilled: colorPalette.pink.shade_500,
      cancelled: colorPalette.red.shade_500,
    };
    return (
      <div
        style={{
          height: 6,
          width: 6,
          borderRadius: "50%",
          background: theme[status as keyof typeof theme] as string,
        }}
      />
    );
  };

  const mapDataToRender = () => {
    const render = data.map((item, index) => ({
      id: item.id as string,
      number: index + 1,
      doctor: item.participant
        ?.filter((x) => x.actor?.type === "Practitioner" && x.actor?.display)
        .map((x) => x.actor?.display)
        .join(" ,"),
      start: new Date(item.start as string).toLocaleDateString(),
      duration: item.minutesDuration + " min",
      description: item.description,
      status: (
        <Box display="flex" alignItems="center" gap="8px">
          {StatusCircle(item.status as string)}
          <Typography textTransform="capitalize" children={item.status} />
        </Box>
      ),
      action: (
        <Box style={{ textAlign: "center", whiteSpace: "nowrap" }}>
          <Link
            to={`appointment/edit?id=${item.id}`}
            children={<IconButton children={<Create />} />}
          />
          <Link
            to={`appointment?id=${item.id}`}
            children={<IconButton children={<Visibility />} />}
          />

          <Link
            to={`#`}
            onClick={() => {
              handleDelete(item.id as string);
            }}
            children={<IconButton children={<Delete />} />}
          />
        </Box>
      ),
    }));
    return render;
  };

  return (
    <>
      {/* Dialog for delete item popup */}
      <DeleteDialog
        open={deletePopUpOpen}
        onClose={handleCancelDelete}
        onConfirm={handleConfirmDelete}
        onCancel={handleCancelDelete}
      />
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          width: "100%",
          padding: "20px",
          backgroundColor: "white",
          borderRadius: "16px",
        }}
      >
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            gap: "8px",
          }}
        >
          <Typography variant="h2" children={"APPOINTMENTS"} />
          <Box sx={{ display: "flex", gap: "8px" }}>
            <TextField
              placeholder="Search appointment with doctor..."
              InputProps={{
                startAdornment: (
                  <InputAdornment
                    position="start"
                    children={<Search sx={{ height: 20 }} />}
                  />
                ),
              }}
              sx={{
                "& .MuiInputBase-root": {
                  height: 48,
                  width: 331,
                  borderRadius: 3,
                },
              }}
              value={searchValue}
              onChange={(e) => {
                setSearchValue(e.target.value);
              }}
            />
            <RoundedButton
              variant="contained"
              onClick={handleClickAdvancedSearchOpen}
              children="Advanced Search"
            />
            <RoundedButton
              variant="contained"
              endIcon={<Add />}
              onClick={() => {
                navigate("appointment/add");
              }}
              children="Add New Appointment"
            />
          </Box>
        </Box>
        {openAdvancedSearch && (
          <AdvancedSearch
            props={advancedSearchProps}
            filter={filter}
            description="Find appointment with doctor..."
            onFilterChange={setFilter}
            onApply={getAppointmentList}
            onClose={setOpenAdvancedSearch}
          />
        )}
        <CustomTable
          title={"APPOINTMENT LIST"}
          headcells={headCells}
          body={mapDataToRender()}
          paging={{
            page: page,
            rowsPerPage: rowsPerPage,
            rowsCount: rowsCount,
          }}
          loading={isLoading}
          toolbar={false}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Box>
    </>
  );
};
