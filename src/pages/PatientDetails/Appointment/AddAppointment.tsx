import AddIcon from "@mui/icons-material/Add";
import { Box, Button, styled, TextField, Typography } from "@mui/material";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { DesktopDatePicker } from "@mui/x-date-pickers/DesktopDatePicker";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import React from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import Loading from "../../../components/Loading";
import { appointmentService } from "../../../services/agent";
import { StyledArrowBack } from "../style";
import { appointmentModel } from "./../../../models/appointment";

export const AddAppointment: React.FC = () => {
  const navigate = useNavigate();

  const [searchParams, setSearchParams] = useSearchParams();
  const id = searchParams.get("id");
  const [loading, setLoading] = React.useState<boolean>(id? true : false);
  const [startPeriod, setStartPeriod] = React.useState<string>(
    new Date().toISOString().replace(/.\d+Z$/g, "Z")
  );
  const [endPeriod, setEndPeriod] = React.useState<string>(
    new Date().toISOString().replace(/.\d+Z$/g, "Z")
  );
  const [formValue, setFormValue] = React.useState<{ [key: string]: string }>({
    startPeriod: startPeriod,
    endPeriod: endPeriod,
  });

  //declare function
  const handleStartChange = (newValue: Date | null) => {
    setStartPeriod(newValue?.toISOString().replace(/.\d+Z$/g, "Z") as string);
    setFormValue({
      ...formValue,
      startPeriod: startPeriod as unknown as string,
    });
  };
  const handleEndChange = (newValue: Date | null) => {
    setEndPeriod(newValue?.toISOString().replace(/.\d+Z$/g, "Z") as string);
    setFormValue({
      ...formValue,
      endPeriod: endPeriod as unknown as string,
    });
  };
  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setFormValue({
      ...formValue,
      [event.currentTarget.name]: event.currentTarget.value,
    });
    console.log(formValue);
  };
  const handleSelectChange = (event: SelectChangeEvent) => {
    setFormValue({ ...formValue, [event.target.name]: event.target.value });
  };
  const handleSubmit = () => {
    setLoading(true);
    //mapping value to submit
    const submitValue = {
      status: formValue?.status,
      serviceType: [
        {
          coding: [
            {
              code: "52",
              display: formValue?.serviceType,
            },
          ],
        },
      ],
      priority: parseInt(formValue?.priority),
      description: formValue?.description,
      start: startPeriod,
      end: endPeriod,
      minutesDuration: parseInt(formValue?.minutesDuration),
      created: endPeriod,
      comment: formValue?.comment,
      basedOn: [
        {
          reference: "ServiceRequest/myringotomy",
        },
      ],
      participant: [
        {
          type: [
            {
              coding: [
                {
                  system: "http://terminology.hl7.org/CodeSystem/v3-Patient",
                  code: "PAT",
                },
              ],
            },
          ],
          actor: {
            reference: "801025db-98e3-49db-8cc0-cd5fd253b772",
            type: "Patient",
            display: "Peter James Chalmers",
          },
          required: "required",
          status: "accepted",
        },
        {
          type: [
            {
              coding: [
                {
                  system:
                    "http://terminology.hl7.org/CodeSystem/v3-ParticipationType",
                  code: "ATND",
                },
              ],
            },
          ],
          actor: {
            reference: "68bd4adf-30c4-464e-9789-ab94f92766b2",
            type: "Practitioner",
            display: formValue?.doctorName,
          },
          required: "required",
          status: "tentative",
        },
        {
          actor: {
            reference: "Location/1",
            display: "South Wing, second floor",
          },
          required: "required",
          status: "declined",
        },
      ],
    } as appointmentModel;

    if (formValue?.status) {
      if (id)
        appointmentService
          .update({ body: submitValue }, id)
          .then(() => navigate(`/admin/patient/details?id=${id}`, { state: 1 }))
          .catch(() => setLoading(false));
      else
        appointmentService
          .post({ body: submitValue })
          .then(() => navigate(`/admin/patient/details?id=${id}`, { state: 1 }))
          .catch(() => setLoading(false));
    }
  };

  //styling
  const style = {
    "& .MuiOutlinedInput-root": {
      height: "100%",
      "& fieldset": {
        border: "1px solid #EAEAEA",
        borderRadius: "0.8rem",
        padding: 0,
        margin: 0,
      },
      "&:hover fieldset": {
        border: "1px solid #dbd7d7",
      },
      "&.Mui-focused fieldset": {
        border: "1px solid #EAEAEA",
      },
    },
  };
  const StyledButton = styled(Button)(({}) => ({
    margin: "0",
    color: "#FFFFFF",
    fontWeight: "700",
    fontSize: "1rem",
    lineHeight: "1.2rem",
    backgroundColor: "#6C5DD3",
    borderRadius: "6rem",
    padding: "10px 16px",
    whiteSpace: "nowrap",
    "&:hover": {
      backgroundColor: "#887ae6",
    },
  }));
  //Field header Mapping
  const appointmentDetailsFields = [
    {
      name: "doctorName",
      value: formValue?.doctorName,
      label: "Doctor Name",
      required: true,
    },
    {
      name: "description",
      value: formValue?.description,
      label: "Description ",
      required: false,
    },
    {
      name: "serviceType",
      value: formValue?.serviceType,
      label: "Service Type",
      required: false,
    },
  ];
  const coverageFields = [
    {
      name: "priority",
      value: formValue?.priority,
      label: "Priority",
      required: false,
    },
    {
      name: "comment",
      value: formValue?.comment,
      label: "Comment",
      required: false,
    },
  ];
  const appointmentStatus = [
    { value: "uncompleted", display: "Uncompleted" },
    { value: "booked", display: "Booked" },
    { value: "pending", display: "Pending" },
  ];
  const minutesDuration = [
    { value: 15, display: "15" },
    { value: 30, display: "30" },
    { value: 45, display: "45" },
    { value: 60, display: "60" },
  ];

  React.useEffect(() => {
    if (id)
      appointmentService.getById(id).then((res) => {
        setFormValue({
          status: "booked",
          doctorName: res.participant
            ? res.participant[1].actor
              ? (res.participant[1].actor.display as string)
              : ""
            : "",
        });
        setStartPeriod(
          res.start
            ? (res.start.slice(0, 10) as string)
            : new Date().toISOString().replace(/.\d+Z$/g, "Z")
        );
        setEndPeriod(
          res.start
            ? (res.start.slice(11, 19) as string)
            : new Date().toISOString().replace(/.\d+Z$/g, "Z")
        );
      });
      setLoading(false)
  }, [id]);

  return (
    <Box>
      {/* Header */}
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <Typography
          sx={{
            fontWeight: 700,
            fontSize: 24,
            display: "flex",
            alignItems: "center",
            gap: "16px",
          }}
        >
          <StyledArrowBack
            onClick={() =>
              navigate(`/admin/patient/details?id=${id}`, { state: 1 })
            }
          />
          {id? "EDIT APPOINTMENT" : "ADD APPOINTMENT"}
        </Typography>
        <StyledButton onClick={handleSubmit}>
          {searchParams.get("id") ? (
            <>Save changes</>
          ) : (
            <>
              {" "}
              Add <AddIcon style={{ marginLeft: "10px" }} />
            </>
          )}
        </StyledButton>
      </Box>
      {/* Body */}
      {loading ? (
        <Loading />
      ) : (
        <Box
          style={{
            display: "flex",
            justifyContent: "space-between",
            background: "white",
            borderRadius: "16px",
            margin: "16px 0",
            padding: "16px",
          }}
        >
          {/* Left Col */}
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              gap: "16px",
              width: "49%",
            }}
          >
            <Typography sx={{ fontWeight: 600, fontSize: 20 }}>
              General Information
            </Typography>
            {appointmentDetailsFields.map((item) => (
              <>
                <TextField
                  name={item.name}
                  value={item.value}
                  onChange={handleInputChange}
                  label={item.label}
                  required={item.required}
                  error={item.required ? !item.value : false}
                  sx={style}
                />
              </>
            ))}
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                "& .MuiFormControl-root": {
                  flexBasis: "33%",
                },
                ...style,
              }}
            >
              <FormControl>
                <InputLabel id="status-select-label">Status</InputLabel>
                <Select
                  labelId="status-select-label"
                  id="status-select"
                  value={formValue?.status}
                  label="Status"
                  name="status"
                  onChange={handleSelectChange}
                  required
                  error={!formValue?.status}
                >
                  {appointmentStatus.map(({ value, display }) => (
                    <MenuItem value={value}>{display}</MenuItem>
                  ))}
                </Select>
              </FormControl>
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DesktopDatePicker
                  label="Valid From"
                  inputFormat="MM/dd/yyyy"
                  value={new Date(startPeriod)}
                  onChange={handleStartChange}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      required
                      error={startPeriod ? startPeriod >= endPeriod : true}
                    />
                  )}
                />
                <DesktopDatePicker
                  label="Good Thru"
                  inputFormat="MM/dd/yyyy"
                  value={new Date(endPeriod)}
                  onChange={handleEndChange}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      required
                      error={endPeriod ? endPeriod <= startPeriod : true}
                    />
                  )}
                />
              </LocalizationProvider>
            </Box>
          </Box>
          {/* Right Col*/}
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              gap: "16px",
              width: "49%",
            }}
          >
            <Typography sx={{ fontWeight: 600, fontSize: 20 }}>
              More Information
            </Typography>
            {coverageFields.map((item) => (
              <TextField
                name={item.name}
                value={item.value}
                onChange={handleInputChange}
                label={item.label}
                required={item.required}
                error={item.required ? !item.value : false}
                sx={style}
              />
            ))}
            <FormControl>
              <InputLabel id="duration-select-label">
                Minutes Duration
              </InputLabel>
              <Select
                labelId="duration-select-label"
                id="duration-select"
                value={formValue?.minutesDuration}
                label="Minutes Duration"
                name="minutesDuration"
                onChange={handleSelectChange}
                required
                error={!formValue?.minutesDuration}
              >
                {minutesDuration.map(({ value, display }) => (
                  <MenuItem value={value}>{display}</MenuItem>
                ))}
              </Select>
            </FormControl>
          </Box>
        </Box>
      )}
    </Box>
  );
};
