import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import { IconButton, styled } from "@mui/material";
import React from "react";
import { Link, useSearchParams } from "react-router-dom";
import Loading from "../../../components/Loading";
import { appointmentService } from "../../../services/agent";
import { colorPalette } from "../../../theme/ColorPalette";
interface TableProps {
  doctor: string;
  date: string;
  time: string;
  duration: number;
  topic: string;
  method: string;
  status: any;
  amount: number;
  payment: string;
  id: string;
  note: string | null;
}

const Table = styled("table")({
  background: "white",
  flex: 1,
  padding: "8px 16px",
  borderRadius: "16px",
  "& tr": {
    lineHeight: "22px",
  },
  "& th": {
    padding: "8px 0",
    textAlign: "left",
    lineHeight: "22px",
  },
  "& td": {
    padding: "8px 0",
    textAlign: "right",
  },
});

const OutlinedPill = (status: string) => {
  const theme = {
    Pending: colorPalette.orange.shade_500,
    Booked: colorPalette.green.shade_500,
    Cancelled: colorPalette.pink.shade_500,
    Fulfilled: colorPalette.purple.shade_500,
  };
  return (
    <div
      style={{
        padding: "3px 12px",
        border: "1px solid",
        borderColor: theme[status as keyof typeof theme] as string,
        color: theme[status as keyof typeof theme] as string,
        borderRadius: "21px",
        width: "fit-content",
        float: "right",
      }}
    >
      {status}
    </div>
  );
};

const DetailTable: React.FC<TableProps> = ({ ...props }) => (
  <div
    style={{
      display: "flex",
      justifyContent: "space-between",
      gap: "20px",
    }}
  >
    <Table>
      <tr>
        <th>Doctor</th>
        <td>{props.doctor}</td>
      </tr>
      <tr>
        <th>Date</th>
        <td>{props.date}</td>
      </tr>
      <tr>
        <th>Time</th>
        <td>{props.time}</td>
      </tr>
      <tr>
        <th>Duration</th>
        <td>{props.duration}</td>
      </tr>
      <tr>
        <th>Consultant Topic</th>
        <td>{props.topic}</td>
      </tr>
      <tr>
        <th>Consultant Method</th>
        <td>{props.method}</td>
      </tr>
    </Table>
    <Table>
      <tr>
        <th>Status</th>
        <td>{props.status ? OutlinedPill(props.status) : ""}</td>
      </tr>
      <tr>
        <th>Amount</th>
        <td>{props.amount}</td>
      </tr>
      <tr>
        <th>Payment Mode</th>
        <td>{props.payment}</td>
      </tr>
      <tr>
        <th>Transaction ID</th>
        <td>{props.id}</td>
      </tr>
      <tr>
        <th>Note</th>
        <td>{props.note ? props.note : "-"}</td>
      </tr>
    </Table>
  </div>
);

export const PatientAppointment: React.FC = () => {
  const [loading, setLoading] = React.useState<boolean>(true);
  const [searchParams, setSearchParams] = useSearchParams();
  const id = searchParams.get("id");
  const [data, setData] = React.useState<TableProps>({
    doctor: "",
    date: "",
    time: "",
    duration: 0,
    topic: "",
    method: "",
    status: "",
    amount: 0,
    payment: "",
    id: "",
    note: "",
  });

  React.useEffect(() => {
    if (!id) throw history.back();
    appointmentService.getById(id).then((res) => {
      setData(() => {
        const newData = {
          doctor: res
            ? (res.participant?.reduce((pre: any, cur: any) => {
                if (cur.actor.type === "Practitioner") return cur.actor.display;
                else return pre as string;
              }, "") as unknown as string)
            : "",
          date: res ? (res.start?.slice(0, 10) as string) : "",
          time: res ? (res.start?.slice(11, 19) as string) : "",
          duration: res ? (res.minutesDuration as number) : "",
          topic: res ? (res.description as string) : "",
          method: res
            ? res.serviceType
              ? res.serviceType?.[0].coding?.[0].display
              : ""
            : "",
          status: res
            ? ((res.status[0].toUpperCase() +
                res.status.substring(1)) as string)
            : "",
          amount: 144.0,
          payment: "Visa",
          id: "TRANID2553",
          note: res ? res.comment : "",
        } as TableProps;
        return newData;
      });
    });
    setLoading(false);
  }, [id]);

  return (
    <div>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <div
          style={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <Link to={`/admin/patient/details?id=${id}`} state={1}>
            <IconButton
              style={{
                color: "rgba(108, 93, 211, 1)",
                background: "white",
                borderRadius: "50%",
                width: "40px",
                height: "40px",
                marginRight: "16px",
              }}
            >
              <ArrowBackIcon />
            </IconButton>
          </Link>
          <h1 style={{ fontSize: "24px" }}>Appointment Details</h1>
        </div>
        <Link
          to={`edit?id=${id}`}
          style={{
            display: "flex",
            gap: "14px",
            padding: "10px 30px",
            borderRadius: "30px",
            color: "rgba(108, 93, 211, 1)",
            background: "white",
            textDecoration: "none",
          }}
        >
          <EditOutlinedIcon />
          <span style={{ fontWeight: 700 }}>Edit</span>
        </Link>
      </div>
      {loading && <Loading></Loading>}
      {!loading && <DetailTable {...data} />}
    </div>
  );
};
