import React from "react";
import * as _ from "lodash";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { CalendarPicker } from "@mui/x-date-pickers/CalendarPicker";
import { styled } from "@mui/system";
import { Button, IconButton } from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { CancelAppointmentModal } from "./CancelAppoitmentModal";
import { colorPalette } from "../../../theme/ColorPalette";
import { useNavigate } from "react-router-dom";
interface TableProps {
  doctor: string;
  date: string;
  time: string;
  duration: string;
  topic: string;
  method: string;
  status: any;
  amount: number;
  payment: string;
  id: string;
  note: string | null;
}

const data = {
  doctor: "Dr.Steven",
  date: "20 May 2022",
  time: "08:20 AM",
  duration: "60 mins",
  topic: "Topic A",
  method: "Chat consulting",
  status: "Pending",
  amount: 144.0,
  payment: "Visa",
  id: "TRANID2553",
  note: "",
};

const Picker: React.FC = () => {
  const disabledDate = [new Date().toDateString()];
  const [doctor, setDoctor] = React.useState<string>("");
  const handleDoctorChange = (e: SelectChangeEvent) => {
    setDoctor(e.target.value as string);
  };
  const [date, setDate] = React.useState<Date | null>(new Date());
  const [time, setTime] = React.useState<string>("");
  const handleTimeChange = (e: SelectChangeEvent) => {
    setTime(e.target.value as string);
  };

  const CSSCalendarPicker = styled(CalendarPicker)({
    width: "100%",
    marginTop: "5px",
    "& .css-1dozdou": {
      display: "none",
    },
    "& .css-mvmu1r, & .css-1n2mv2k": {
      justifyContent: "space-between",
      webkitJustifyContent: "space-between",
    },
    "& .css-1cnkspq": {
      minHeight: "300px",
    },
    "& .css-mvmu1r > div > *, & .css-1n2mv2k span": {
      // background: "transparent",
      padding: "24px 40px",
      borderRadius: "40px",
      fontWeight: 600,
      fontSize: "14px",
      lineHeight: "18px",
      color: "#19181A",
    },
    "& .css-mvmu1r button:hover": {
      color: "#6C5DD3",
    },
    "& .css-t3c9ra-MuiButtonBase-root-MuiPickersDay-root.Mui-selected": {
      color: "white",
    },
    "& .css-t3c9ra-MuiButtonBase-root-MuiPickersDay-root.Mui-selected:hover": {
      color: "white",
    },
    "& .css-1n2mv2k": {
      marginBottom: "8px",
    }
  });
  const CSSSelect = styled(Select)({
    borderRadius: "12px",
    borderColor: "rgba(234, 234, 234, 1)",
    color: "rgba(19, 22, 38, 1)",
  });

  const picker = React.useRef(null);
  return (
    <div
      style={{
        flex: 1,
        background: "white",
        padding: "16px",
        borderRadius: "16px",
      }}
    >
      <div style={{ paddingBottom: "10px" }}>
        <FormControl fullWidth>
          <InputLabel id="select-doctor-label">Doctor</InputLabel>
          <CSSSelect
            labelId="select-doctor-label"
            id="doctor-select"
            value={doctor}
            label="Doctor"
            onChange={(e) => handleDoctorChange(e as SelectChangeEvent<string>)}
          >
            <MenuItem value="Dr.Steven">Dr. Steven</MenuItem>
          </CSSSelect>
        </FormControl>
      </div>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <h2>Select date</h2>
        <ul style={{ listStyle: "none", display: "flex", gap: "10px" }}>
          <li style={{ display: "flex", alignItems: "center", gap: "5px" }}>
            <div
              style={{
                width: 9,
                height: 9,
                borderRadius: "50%",
                background: "rgba(159, 160, 166, 1)",
              }}
            />
            No slot
          </li>
          <li style={{ display: "flex", alignItems: "center", gap: "5px" }}>
            <div
              style={{
                width: 9,
                height: 9,
                borderRadius: "50%",
                background: "rgba(19, 22, 38, 1)",
              }}
            />
            Available
          </li>
        </ul>
      </div>
      <div>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <CSSCalendarPicker
            shouldDisableDate={(day) => 
              disabledDate.includes(new Date(day as Date).toDateString())
            }
            minDate={new Date("01-01-2000")}
            ref={picker}
            date={date}
            onChange={(newDate) => setDate(newDate as Date | null)}
            disableHighlightToday={true}
          />
        </LocalizationProvider>
      </div>
      <div>
        <FormControl fullWidth sx={{ margin: "10px 0" }}>
          <InputLabel id="select-time-label">Select time</InputLabel>
          <CSSSelect
            labelId="select-time-label"
            id="time-select"
            value={time}
            label="Select time"
            onChange={(e) => handleTimeChange(e as SelectChangeEvent<string>)}
          >
            <MenuItem value="10:00am">10:00 AM</MenuItem>
          </CSSSelect>
        </FormControl>
      </div>
    </div>
  );
};

const DetailTable: React.FC<TableProps> = ({ ...props }) => {
  const Table = styled("table")({
    background: "white",
    flex: 1,
    padding: "8px 16px",
    borderRadius: "16px",
    "& tr": {
      lineHeight: "22px",
    },
    "& th": {
      padding: "8px 0",
      textAlign: "left",
      lineHeight: "22px",
    },
    "& td": {
      padding: "8px 0",
      textAlign: "right",
    },
  });

  return (
    <Table>
      <tr>
        <th>Consulting Topic</th>
      </tr>
      <tr>
        <div style={{ display: "flex", gap: "5px" }}>
          {["Topic A", "Topic B", "Topic C"].map((item) => (
            <div
              style={{
                padding: "0 12px",
                border: "1px solid rgba(234, 234, 234, 1)",
                color: "rgba(19, 22, 38, 1)",
                borderRadius: "21px",
                width: "fit-content",
                fontSize: "12px",
              }}
            >
              {item}
            </div>
          ))}
        </div>
      </tr>
      <tr>
        <th>Duration</th>
        <td>{props.duration}</td>
      </tr>
      <tr>
        <th>Consultant Method</th>
        <td>{props.method}</td>
      </tr>
      <tr>
        <th>Status</th>
        <td>
          <div
            style={{
              padding: "0 12px",
              border: "1px solid rgba(255, 115, 0, 1)",
              color: "rgba(255, 115, 0, 1)",
              borderRadius: "21px",
              width: "fit-content",
              float: "right",
              fontSize: "12px",
            }}
          >
            {props.status}
          </div>
        </td>
      </tr>
      <tr>
        <th>Amount</th>
        <td>{props.amount}</td>
      </tr>
      <tr>
        <th>Payment Mode</th>
        <td>{props.time}</td>
      </tr>
      <tr>
        <th>Transaction ID</th>
        <td>{props.id}</td>
      </tr>
      <tr>
        <th>Note</th>
        <td>{props.note ? props.note : "-"}</td>
      </tr>
    </Table>
  );
};

export const PatientAppointmentEdit: React.FC = () => {
  const [open, setOpen] = React.useState(false);
  const toggleModal = () => setOpen(!open);
  const navigate = useNavigate();
  const OutlinedPill = (status: string) => {
    const theme = {
      Pending: colorPalette.orange.shade_500,
      Booked: colorPalette.green.shade_500,
      Cancelled: colorPalette.pink.shade_500,
      Fulfilled: colorPalette.purple.shade_500,
    };
    return (
      <div
        style={{
          padding: "3px 12px",
          border: "1px solid",
          borderColor: theme[status as keyof typeof theme] as string,
          color: theme[status as keyof typeof theme] as string,
          borderRadius: "21px",
          width: "fit-content",
          float: "right",
        }}
      >
        {status}
      </div>
    );
  };

  return (
    <div>
      <CancelAppointmentModal open={open} toggleModal={toggleModal} />
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          marginBottom: "20px",
        }}
      >
        <div
          style={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <IconButton
            style={{
              color: "rgba(108, 93, 211, 1)",
              background: "white",
              borderRadius: "50%",
              width: "40px",
              height: "40px",
              marginRight: "16px",
            }}
            onClick={() => navigate(`/admin/patient/details`)}
          >
            <ArrowBackIcon />
          </IconButton>
          <span style={{ fontSize: "24px", fontWeight: 700 }}>
            Edit Appointment
          </span>
        </div>
        <ul style={{ listStyle: "none", display: "flex", gap: "9px" }}>
          <li>
            <Button
              style={{
                padding: "10px 30px",
                borderRadius: "30px",
                color: "rgba(108, 93, 211, 1)",
                background: "white",
                fontWeight: 700,
              }}
            >
              Cancel
            </Button>
          </li>
          <li>
            <Button
              onClick={toggleModal}
              style={{
                padding: "10px 30px",
                borderRadius: "30px",
                color: "rgba(255, 255, 255, 1)",
                background: "rgba(224, 53, 53, 1)",
                whiteSpace: "nowrap",
                fontWeight: 700,
              }}
            >
              Cancel Appointment
            </Button>
          </li>
          <li>
            <Button
              style={{
                padding: "10px 30px",
                borderRadius: "30px",
                color: "white",
                background: "rgba(108, 93, 211, 1)",
                fontWeight: 700,
              }}
            >
              Save
            </Button>
          </li>
        </ul>
      </div>
      <div
        style={{
          display: "flex",
          gap: "16px",
        }}
      >
        <Picker />
        <DetailTable {...data} />
      </div>
    </div>
  );
};
