import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import { Box, IconButton, styled, Typography } from "@mui/material";
import React from "react";
import { Link, useSearchParams } from "react-router-dom";
import Loading from "../../../components/Loading";
import { insuranceModel } from "../../../models/insurance";
import { insuranceService } from "../../../services/agent";

export const InsuranceDetails: React.FC = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const [loading, setLoading] = React.useState<boolean>(true);
  const [data, setData] = React.useState<insuranceModel>({});
  const id = searchParams.get("id");

  const Table = styled("table")({
    background: "white",
    flex: 1,
    padding: "8px 16px",
    borderRadius: "16px",
    "& tr": {
      lineHeight: "22px",
    },
    "& th": {
      padding: "8px 0",
      textAlign: "left",
      lineHeight: "22px",
    },
    "& td": {
      padding: "8px 0",
      textAlign: "right",
    },
  });

  const insuranceDetailsFields = [
    {
      value: data?.name,
      label: "Insurance Plans",
    },
    {
      value: data?.ownedBy || "N/A",
      label: "Issuer",
    },
    {
      value: data?.administeredBy || "N/A",
      label: "Administrator",
    },
    {
      value: data?.coverageArea || "N/A",
      label: "Coverage Area",
    },
    {
      value: data?.type
        ? data?.type[0].text
          ? data?.type[0].text
          : data?.type[0].coding
          ? data?.type[0].coding[0]?.display
          : "N/A"
        : "N/A",
      label: "Type",
    },
    {
      value: <Typography textTransform="capitalize" children={data.status} />,
      label: "Status",
    },
  ];
  const relatedContactFields = [
    {
      value: data?.contact
        ? data?.contact[0].name?.text
          ? data?.contact[0].name?.text
          : data?.contact[0].name?.given
          ? data?.contact[0].name.given.reduce((pre: string, cur: string) => {
              return pre + " " + cur;
            }) +
            " " +
            data?.contact[0].name.family
          : "N/A"
        : "N/A",
      label: "Name",
    },
    {
      value: data.contact
        ? data.contact[0].telecom
          ? data?.contact[0]?.telecom.reduce((pre: string, cur: any) => {
              if (cur.system === "phone") return cur.value;
              else return pre;
            }, "")
          : ""
        : "",
      label: "Phone",
    },
    {
      value: data.contact
        ? data.contact[0].telecom
          ? data?.contact[0]?.telecom.reduce((pre: string, cur: any) => {
              if (cur.system === "email") return cur.value;
              else return pre;
            }, "")
          : ""
        : "",
      label: "Email",
    },
    {
      value: data?.contact ? data?.contact[0].address?.text : "N/A",
      label: "Address",
    },
  ];
  const coverageFields = [
    {
      value: data?.coverage
        ? data?.coverage[0].type
          ? "Coverage"
          : "N/A"
        : "N/A",
      label: "Type",
    },
    {
      value: data?.coverage
        ? data?.coverage[0].network
          ? "Coverage Network"
          : "N/A"
        : "N/A",
      label: "Provider",
    },
    {
      value: data?.coverage
        ? data?.coverage[0].benefit
          ? "Coverage Benefit"
          : "N/A"
        : "N/A",
      label: "Benefit",
    },
  ];
  const planFields = [
    {
      value: data?.plan ? (data?.plan[0].type ? "Plan Type" : "N/A") : "N/A",
      label: "Type",
    },
    {
      value: data?.plan
        ? data?.plan[0].generalCost
          ? "General Cost"
          : "N/A"
        : "N/A",
      label: "General Cost",
    },
    {
      value: data?.plan
        ? data?.plan[0].specificCost
          ? "Specific Cost"
          : "N/A"
        : "N/A",
      label: "Specific Cost",
    },
  ];

  React.useEffect(() => {
    if (!id) throw history.back();
    insuranceService
      .getById(id)
      .then((res) => {
        setData(res);
        setLoading(false);
      })
      .catch(() => history.back());
  }, [id]);

  return (
    <Box>
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <IconButton
            onClick={() => history.back()}
            style={{
              color: "rgba(108, 93, 211, 1)",
              background: "white",
              borderRadius: "50%",
              width: "40px",
              height: "40px",
              marginRight: "16px",
            }}
            children={<ArrowBackIcon />}
          />
          <h1 style={{ fontSize: "24px" }}>Insurance Plan Details</h1>
        </Box>
        <Link
          to={`./edit?id=${data.id}`}
          style={{
            display: "flex",
            gap: "14px",
            padding: "10px 30px",
            borderRadius: "30px",
            color: "rgba(108, 93, 211, 1)",
            background: "white",
            textDecoration: "none",
          }}
        >
          <EditOutlinedIcon />
          <span style={{ fontWeight: 700 }}>Edit</span>
        </Link>
      </Box>
      {loading ? (
        <Loading />
      ) : (
        <>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              gap: "20px",
            }}
          >
            <Table>
              <Typography
                sx={{ fontSize: "18px", fontWeight: "600", padding: "8px 0" }}
              >
                General Information
              </Typography>
              {insuranceDetailsFields.map((item) => (
                <tr>
                  <th>{item.label}</th>
                  <td>{item.value}</td>
                </tr>
              ))}
            </Table>
            <Table>
              <Typography
                sx={{ fontSize: "18px", fontWeight: "600", padding: "8px 0" }}
              >
                Related Contact
              </Typography>
              {relatedContactFields.map((item) => (
                <tr>
                  <th>{item.label}</th>
                  <td>{item.value}</td>
                </tr>
              ))}
            </Table>
          </Box>

          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              marginTop: "16px",
            }}
          >
            <Table>
              <Typography
                sx={{ fontSize: "18px", fontWeight: "700", padding: "8px 0" }}
              >
                Coverages
              </Typography>
              {coverageFields.map((item) => (
                <tr>
                  <th>{item.label}</th>
                  <td>{item.value}</td>
                </tr>
              ))}
            </Table>
          </Box>

          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              marginTop: "16px",
            }}
          >
            <Table>
              <Typography
                sx={{ fontSize: "18px", fontWeight: "600", padding: "8px 0" }}
              >
                Plans
              </Typography>
              {planFields.map((item) => (
                <tr>
                  <th>{item.label}</th>
                  <td>{item.value}</td>
                </tr>
              ))}
            </Table>
          </Box>
        </>
      )}
    </Box>
  );
};
