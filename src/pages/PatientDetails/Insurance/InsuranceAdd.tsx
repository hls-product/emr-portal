import AddIcon from "@mui/icons-material/Add";
import { Box, Button, styled, TextField, Typography } from "@mui/material";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { DesktopDatePicker } from "@mui/x-date-pickers/DesktopDatePicker";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import React from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import Loading from "../../../components/Loading";
import { insuranceModel } from "../../../models/insurance";
import { insuranceService } from "../../../services/agent";
import { StyledArrowBack } from "../style";

export const InsuranceAdd: React.FC = () => {
  const navigate = useNavigate();
  const [loading, setLoading] = React.useState<boolean>(false);
  const [searchParams, setSearchParams] = useSearchParams();
  const [startPeriod, setStartPeriod] = React.useState<string>(
    new Date().toISOString().replace(/.\d+Z$/g, "Z")
  );
  const [endPeriod, setEndPeriod] = React.useState<string>(
    new Date().toISOString().replace(/.\d+Z$/g, "Z")
  );
  const [formValue, setFormValue] = React.useState<{ [key: string]: string }>({
    startPeriod: startPeriod,
    endPeriod: endPeriod,
  });
  const id = searchParams.get("id");

  //declare function
  const handleStartChange = (newValue: Date | null) => {
    setStartPeriod(newValue?.toISOString().replace(/.\d+Z$/g, "Z") as string);
    setFormValue({
      ...formValue,
      startPeriod: startPeriod as unknown as string,
    });
  };
  const handleEndChange = (newValue: Date | null) => {
    setEndPeriod(newValue?.toISOString().replace(/.\d+Z$/g, "Z") as string);
    setFormValue({
      ...formValue,
      endPeriod: endPeriod as unknown as string,
    });
  };
  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setFormValue({
      ...formValue,
      [event.currentTarget.name]: event.currentTarget.value,
    });
    console.log(formValue);
  };
  const handleSelectChange = (event: SelectChangeEvent) => {
    setFormValue({ ...formValue, [event.target.name]: event.target.value });
  };
  const handleSubmit: () => void = () => {
    //mapping value to submit
    const submitValue = {
      status: formValue?.status,
      name: formValue?.name,
      type: [
        {
          coding: [
            {
              system:
                "http://terminology.hl7.org/CodeSystem/insurance-plan-type",
              code: formValue?.type,
              display: insuranceTypes.reduce(
                (pre, cur) =>
                  cur.value === formValue?.type ? cur.display : pre,
                ""
              ),
            },
          ],
        },
      ],
      period: {
        start: startPeriod,
        end: endPeriod,
      },
      contact: [
        {
          purpose: {
            coding: [
              {
                system:
                  "https://hl7.org/fhir/codesystem-contactentity-type.html",
                code: formValue?.contactPurpose,
                display: contactPurpose.reduce(
                  (pre, cur) =>
                    cur.value === formValue?.contactPurpose ? cur.display : pre,
                  ""
                ),
              },
            ],
          },
          name: {
            use: "official",
            text: formValue?.contactName,
          },
          telecom: [
            {
              system: "phone",
              value: formValue?.contactTelecom,
            },
          ],
          address: {
            use: "home",
            text: formValue?.contactAddress,
          },
        },
      ],
    } as insuranceModel;

    if (
      formValue?.name &&
      formValue?.type &&
      formValue?.status &&
      startPeriod &&
      endPeriod
    ) {
      setLoading(true);
      if (id)
        insuranceService
          .update({ body: submitValue }, id)
          .then(() => navigate(`/admin/patient/details?id=${id}`, { state: 4 }))
          .catch(() => setLoading(false));
      else
        insuranceService
          .post({ body: submitValue })
          .then(() => navigate(`/admin/patient/details?id=${id}`, { state: 4 }))
          .catch(() => setLoading(false));
    }
  };

  //styling
  const style = {
    "& .MuiOutlinedInput-root": {
      height: "100%",
      "& fieldset": {
        border: "1px solid #EAEAEA",
        borderRadius: "0.8rem",
        padding: 0,
        margin: 0,
      },
      "&:hover fieldset": {
        border: "1px solid #dbd7d7",
      },
      "&.Mui-focused fieldset": {
        border: "1px solid #EAEAEA",
      },
    },
  };
  const StyledButton = styled(Button)(({}) => ({
    margin: "0",
    color: "#FFFFFF",
    fontWeight: "700",
    fontSize: "1rem",
    lineHeight: "1.2rem",
    backgroundColor: "#6C5DD3",
    borderRadius: "6rem",
    padding: "10px 16px",
    whiteSpace: "nowrap",
    "&:hover": {
      backgroundColor: "#887ae6",
    },
  }));
  //Field header Mapping
  const insuranceDetailsFields = [
    {
      name: "name",
      value: formValue?.name,
      label: "Insurance Plans",
      required: true,
    },
    {
      name: "ownedBy",
      value: formValue?.ownedBy,
      label: "Issuer",
      required: false,
    },
    {
      name: "administeredBy",
      value: formValue?.administeredBy,
      label: "Administrator",
      required: false,
    },
    {
      name: "coverageArea",
      value: formValue?.coverageArea,
      label: "Coverage Area",
      required: false,
    },
  ];
  const relatedContactFields = [
    {
      name: "contactName",
      value: formValue?.contactName,
      label: "Name",
      required: false,
    },
    {
      name: "contactTelecom",
      value: formValue?.contactTelecom,
      label: "Phone",
      required: false,
    },
    {
      name: "contactAddress",
      value: formValue?.contactAddress,
      label: "Address",
      required: false,
    },
  ];
  const coverageFields = [
    {
      name: "coverageType",
      value: formValue?.coverageType,
      label: "Type",
      required: false,
    },
    {
      name: "coverageNetwork",
      value: formValue?.coverageNetwork,
      label: "Provider",
      required: false,
    },
    {
      name: "coverageBenefit",
      value: formValue?.coverageBenefit,
      label: "Benefit",
      required: false,
    },
  ];
  const planFields = [
    {
      name: "planType",
      value: formValue?.planType,
      label: "Type",
      required: false,
    },
    {
      name: "planGeneralCost",
      value: formValue?.planGeneralCost,
      label: "General Cost",
      required: false,
    },
    {
      name: "planSpecificCost",
      value: formValue?.planSpecificCost,
      label: "Specific Cost",
      required: false,
    },
  ];
  const contactPurpose = [
    { value: "BILL", display: "Billing" },
    { value: "ADMIN", display: "Administrative" },
    { value: "HR", display: "Human Resource" },
    { value: "PAYOR", display: "Payor" },
    { value: "PATINF", display: "Patient" },
    { value: "PRESS", display: "Press" },
  ];
  const insuranceTypes = [
    { value: "medical", display: "Medical" },
    { value: "dental", display: "Dental" },
    { value: "mental", display: "Mental" },
    { value: "subst-ab", display: "Substance Abuse" },
    { value: "vision", display: "Vision" },
    { value: "Drug", display: "Drug" },
    { value: "short-term", display: "Short Term" },
    { value: "long-term", display: "Long Term" },
    { value: "hospice", display: "Hospice" },
    { value: "home", display: "Home Health" },
  ];
  const insuranceStatus = [
    { value: "draft", display: "Draft" },
    { value: "active", display: "Active" },
    { value: "retired", display: "Retired" },
    { value: "unknown", display: "Unknown" },
  ];

  React.useEffect(() => {
    if (id)
      insuranceService
        .getById(id)
        .then((res) => {
          setFormValue({
            name: res.name ? res.name : "",
            type: res.type
              ? res.type[0].coding
                ? (res.type[0].coding[0].code as string)
                : "medical"
              : "medical",
            status: "active",
            contactPurpose: res.contact
              ? res.contact[0].purpose?.coding
                ? (res.contact[0].purpose?.coding[0].code as string)
                : "PATINF"
              : "PATINF",
            contactName: res.contact
              ? (res.contact[0].name?.text as string)
              : "",
            contactTelecom: res.contact
              ? res.contact[0].telecom
                ? (res.contact[0].telecom[0].value as string)
                : ""
              : "",
            contactAddress: res.contact
              ? (res.contact[0].address?.text as string)
              : "",
          });
          setStartPeriod(
            res.period?.start
              ? res.period?.start
              : new Date().toISOString().replace(/.\d+Z$/g, "Z")
          );
          setEndPeriod(
            res.period?.end
              ? res.period?.end
              : new Date().toISOString().replace(/.\d+Z$/g, "Z")
          );
          setLoading(false);
        })
        .catch(() => history.back());
  }, [id]);

  return (
    <Box>
      {/* Header */}
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <Typography
          sx={{
            fontWeight: 700,
            fontSize: 24,
            display: "flex",
            alignItems: "center",
            gap: "16px",
          }}
        >
          <StyledArrowBack onClick={() => history.back()} />
          {id ? "EDIT INSURANCE PLAN" : "ADD INSURANCE PLAN"}
        </Typography>
        <StyledButton onClick={handleSubmit}>
          {searchParams.get("id") ? (
            <>Save changes</>
          ) : (
            <>
              {" "}
              Add <AddIcon style={{ marginLeft: "10px" }} />
            </>
          )}
        </StyledButton>
      </Box>
      {/* Body */}
      {loading ? (
        <Loading />
      ) : (
        <Box
          style={{
            display: "flex",
            justifyContent: "space-between",
            background: "white",
            borderRadius: "16px",
            margin: "16px 0",
            padding: "16px",
          }}
        >
          {/* Left Col */}
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              gap: "16px",
              width: "49%",
            }}
          >
            <Typography sx={{ fontWeight: 600, fontSize: 20 }}>
              General Information
            </Typography>
            {insuranceDetailsFields.map((item) => (
              <>
                <TextField
                  name={item.name}
                  value={item.value}
                  onChange={handleInputChange}
                  label={item.label}
                  required={item.required}
                  error={item.required ? !item.value : false}
                  sx={style}
                />
              </>
            ))}
            <FormControl fullWidth sx={style}>
              <InputLabel id="type-select-label">
                Select Insurance Type
              </InputLabel>
              <Select
                labelId="type-select-label"
                id="type-select"
                value={formValue?.type}
                label="Select Insurance Type"
                name="type"
                onChange={handleSelectChange}
                required
                error={!formValue?.type}
              >
                {insuranceTypes.map(({ value, display }) => (
                  <MenuItem value={value}>{display}</MenuItem>
                ))}
              </Select>
            </FormControl>
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                "& .MuiFormControl-root": {
                  flexBasis: "33%",
                },
                ...style,
              }}
            >
              <FormControl>
                <InputLabel id="status-select-label">Status</InputLabel>
                <Select
                  labelId="status-select-label"
                  id="status-select"
                  value={formValue?.status}
                  label="Status"
                  name="status"
                  onChange={handleSelectChange}
                  required
                  error={!formValue?.status}
                >
                  {insuranceStatus.map(({ value, display }) => (
                    <MenuItem value={value}>{display}</MenuItem>
                  ))}
                </Select>
              </FormControl>
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DesktopDatePicker
                  label="Valid From"
                  inputFormat="MM/dd/yyyy"
                  value={new Date(startPeriod)}
                  onChange={handleStartChange}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      required
                      error={startPeriod ? startPeriod >= endPeriod : true}
                    />
                  )}
                />
                <DesktopDatePicker
                  label="Good Thru"
                  inputFormat="MM/dd/yyyy"
                  value={new Date(endPeriod)}
                  onChange={handleEndChange}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      required
                      error={endPeriod ? endPeriod <= startPeriod : true}
                    />
                  )}
                />
              </LocalizationProvider>
            </Box>
            <Typography sx={{ fontWeight: 600, fontSize: 20 }}>
              Related Contact
            </Typography>
            {relatedContactFields.map((item) => (
              <>
                <TextField
                  name={item.name}
                  value={item.value}
                  onChange={handleInputChange}
                  label={item.label}
                  required={item.required}
                  error={item.required ? !item.value : false}
                  sx={style}
                />
              </>
            ))}
            <FormControl fullWidth sx={style}>
              <InputLabel id="purpose-select-label">
                Select Insurance Type
              </InputLabel>
              <Select
                labelId="purpose-select-label"
                id="purpose-select"
                value={formValue?.contactPurpose}
                label="Type of Contact"
                name="contactPurpose"
                onChange={handleSelectChange}
              >
                {contactPurpose.map(({ value, display }) => (
                  <MenuItem value={value}>{display}</MenuItem>
                ))}
              </Select>
            </FormControl>
          </Box>
          {/* Right Col*/}
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              gap: "16px",
              width: "49%",
            }}
          >
            <Typography sx={{ fontWeight: 600, fontSize: 20 }}>
              Coverage
            </Typography>
            {coverageFields.map((item) => (
              <TextField
                name={item.name}
                value={item.value}
                onChange={handleInputChange}
                label={item.label}
                required={item.required}
                error={item.required ? !item.value : false}
                sx={style}
              />
            ))}
            <Typography sx={{ fontWeight: 600, fontSize: 20 }}>
              Plans
            </Typography>
            {planFields.map((item) => (
              <TextField
                name={item.name}
                value={item.value}
                onChange={handleInputChange}
                label={item.label}
                required={item.required}
                error={item.required ? !item.value : false}
                sx={style}
              />
            ))}
          </Box>
        </Box>
      )}
    </Box>
  );
};
