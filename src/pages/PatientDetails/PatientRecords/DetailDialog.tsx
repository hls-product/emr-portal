import React, { useState } from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import Typography from "@mui/material/Typography";
import { Box } from "@mui/material";
import VisibilityIcon from "@mui/icons-material/Visibility";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";
import { Download, GenerateReport } from "./PatientRecords";
import DoneAllIcon from "@mui/icons-material/DoneAll";
import SettingsBackupRestoreIcon from "@mui/icons-material/SettingsBackupRestore";
import DownloadIcon from "@mui/icons-material/Download";

interface Props {
  open: boolean;
  handleClose: () => void;
}

export interface DialogTitleProps {
  id: string;
  onClose: () => void;
}

const allSections = [
  "Patient Demographics",
  "Immunizations",
  "Vital Signs",
  "Problems & Diagnoses",
  "Insurance Information",
  "Health Care Providers",
  "Encounter Information",
  "Allergies/Alerting Data",
  "Appropriate Results",
  "Medication",
  "Procedures",
  "Results",
  "Necessary Medical Equipment",
  "Social History",
  "Statistics",
  "Family History",
  "Care Plan ",
];

const coreDataset = [
  "Document identifying information*",
  "Patient identifying information*",
  "Patient insurance/financial information*",
  "Advance Directives",
  "Patient`s health status*",
  "Care documentation*",
  "Care plan recommendations*",
  "List of health care practitioners",
];

const templateCDA = [
  "Continuity of Care Document",
  "Consultation Notes - 2008",
  "Discharge Summary - 2009",
  "Imaging Integration and DICOM Diagnostic Imaging Reports - 2009",
  "History and Physical - 2008",
  "Operative Note - 2009",
  "Progress Note - 2010",
  "Procedure Note - 2010",
  "Unstructured Documents - 2010",
];

const CustomDialogTitle = (props: DialogTitleProps) => {
  const { onClose, ...other } = props;

  return (
    <DialogTitle
      sx={{
        height: "70px",
        padding: "16px",
        display: "flex",
        alignItems: "center",
        justifyContent: "flex-end",
      }}
      {...other}
    >
      {onClose ? (
        <IconButton
          onClick={onClose}
          sx={{
            fontSize: "1.5rem",
            fontWeigth: 700,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
};

export const DetailDialog = (props: Props) => {
  const { open, handleClose } = props;

  const [openCCR, setOpenCCR] = useState(false);
  const [openCCD, setOpenCCD] = useState(false);

  const handleViewCCR = () => {
    setOpenCCR(!openCCR);
  };

  const handleViewCCD = () => {
    setOpenCCD(!openCCD);
  };

  return (
    <Dialog onClose={handleClose} open={open} maxWidth="lg">
      <CustomDialogTitle onClose={handleClose} id="customized-dialog-title" />

      <DialogContent dividers sx={{ padding: "16px 15%" }}>
        <Typography
          sx={{
            fontSize: "32px",
            fontWeight: 500,
            color: "#cdcdcd",
            "&:hover": { color: "black" },
            marginBottom: "16px",
            textAlign: "center",
          }}
        >
          Patient Reports Help
        </Typography>

        <Typography
          sx={{
            marginBottom: "16px",
            fontSize: "16px",
            color: "black",
          }}
        >
          Reports consisting of various portions of the patien's medical record
          can be created here.
        </Typography>

        <Typography
          sx={{
            marginBottom: "16px",
            fontSize: "16px",
            color: "black",
          }}
        >
          There are three main types of reports that can be created, two pertain
          to continuity of ongoing care - Continuity of Care Record (CCR) and
          Continuity of Care Document (CCD) and the third - Patient Report that
          creates a document containing various sections of the patient's
          medical record including demographics, medical issues, procedures and
          encounters. It also has the ability to include all or any of the
          scanned documents in the patient's chart.
        </Typography>

        <Typography sx={{ fontSize: "16px", color: "black" }}>
          This help file is divided into three sections:
        </Typography>

        <Box sx={{ fontSize: "16px", marginBottom: "16px" }}>
          <a
            href="#ccr"
            style={{
              textDecoration: "none",
            }}
          >
            <Typography
              sx={{
                color: "#cdcdcd",
                paddingLeft: "24px",
                marginTop: "10px",
                ":hover": {
                  cursor: "pointer",
                  color: "black",
                },
              }}
            >
              1. Continuity of Care Record (CCR)
            </Typography>
          </a>

          <a
            href="#ccd"
            style={{
              textDecoration: "none",
            }}
          >
            <Typography
              sx={{
                color: "#cdcdcd",
                paddingLeft: "24px",
                marginTop: "5px",
                ":hover": {
                  cursor: "pointer",
                  color: "black",
                },
              }}
            >
              2. Continuity of Care Document (CCD)
            </Typography>
          </a>

          <a
            href="#report"
            style={{
              textDecoration: "none",
            }}
          >
            <Typography
              sx={{
                color: "#cdcdcd",
                paddingLeft: "24px",
                marginTop: "5px",
                ":hover": {
                  cursor: "pointer",
                  color: "black",
                },
              }}
            >
              3. Patient Report
            </Typography>
          </a>
        </Box>

        <Typography
          sx={{ fontSize: "16px", color: "black", marginBottom: "16px" }}
        >
          To help explore the various components of the Report page especially
          if you want to use it as an instruction manual it is suggested that
          you reduce the size of the browser to cover half the viewport, resize
          the help pop-up by clicking and dragging the bottom right corner of
          the pop-up. Open another instance of the browser and resize it to
          cover the other half of the viewport, login to open EMR.
        </Typography>

        <Box id="ccr" sx={{ marginBottom: "16px" }}>
          <Typography
            sx={{
              color: "#cdcdcd",
              fontWeight: 500,
              fontSize: "24px",
              marginBottom: "16px",
              "&:hover": {
                color: "black",
              },
            }}
          >
            Continuity of Care Record (CCR)
          </Typography>

          <Typography
            sx={{ marginBottom: "16px", fontSize: "16px", color: "black" }}
          >
            The Continuity of Care Record (CCR) is the clinical record of the
            patient's current and historical health care status.
          </Typography>

          <Typography
            sx={{ marginBottom: "16px", fontSize: "16px", color: "black" }}
          >
            Basic patient information is included, such as patient and provider
            information, insurance, patient health status, recent care provided,
            care plan information, and reason for referral or transfer.
          </Typography>

          <Typography
            sx={{ marginBottom: "16px", fontSize: "16px", color: "black" }}
          >
            It is intended to include only the information that is critical to
            effectively continue care.
          </Typography>

          <Typography
            sx={{
              marginBottom: "16px",
              fontSize: "16px",
              color: "black",
              display: "flex",
              alignItems: "center",
            }}
          >
            For those interested in details of what constitutes a Continuity of
            Care Record (CCR) click on the eye icon
            <VisibilityIcon
              sx={{
                marginLeft: "10px",
                display: !openCCR ? "block" : "none",
                cursor: "pointer",
                color: "GrayText",
              }}
              onClick={handleViewCCR}
            />
            <VisibilityOffIcon
              sx={{
                marginLeft: "10px",
                display: openCCR ? "block" : "none",
                cursor: "pointer",
                color: "GrayText",
              }}
              onClick={handleViewCCR}
            />
          </Typography>

          {openCCR && (
            <Box
              sx={{
                borderRadius: "5px",
                border: "6px solid #cdcdcd",
                background: "#e9ecef",
                padding: "16px",
                color: "#cdcdcd",
                fontStyle: "italic",
                marginBottom: "16px",
              }}
            >
              <Typography sx={{ marginBottom: "16px", fontSize: "16px" }}>
                The Continuity of Care Record (CCR) has been developed jointly
                by ASTM International - an organization that is involved in the
                development and delivery of voluntary consensus, the
                Massachusetts Medical Society (MMS), the Health Information
                Management and Systems Society (HIMSS), the American Academy of
                Family Physicians (AAFP), and the American Academy of
                Pediatrics.
              </Typography>

              <Typography sx={{ marginBottom: "16px", fontSize: "16px" }}>
                The basis for CCR is a Patient Care Referral Form developed by
                the Massachusetts Department of Public Health.
              </Typography>

              <Typography sx={{ marginBottom: "16px", fontSize: "16px" }}>
                In total, there are seventeen sections that can be included
                within a Continuity of Care Record (CCR).
              </Typography>

              <Box sx={{ marginBottom: "16px", fontSize: "16px" }}>
                All sections are not required, rather, they are available based
                on the information a practitioner believes is critical data for
                the patient at any given moment in time:
                <ul>
                  {allSections.map((section, index) => (
                    <li key={index} style={{ marginTop: "5px" }}>
                      {section}
                    </li>
                  ))}
                </ul>
              </Box>

              <Box sx={{ marginBottom: "16px", fontSize: "16px" }}>
                The core dataset consists of:
                <ul>
                  {coreDataset.map((section, index) => (
                    <li key={index} style={{ marginTop: "5px" }}>
                      {section}
                    </li>
                  ))}
                </ul>
              </Box>

              <Typography sx={{ fontSize: "16px" }}>
                The items that have asterisks after them are mandated.
              </Typography>
            </Box>
          )}

          <Typography
            sx={{ marginBottom: "16px", fontSize: "16px", color: "black" }}
          >
            In open EMR the Continuity of Care Record (CCR) reports to be
            generated can be limited to a specific date range by checking the
            Use Date Range check-box and entering the desired date range.
          </Typography>

          <Box sx={{ fontSize: "16px", color: "black" }}>
            There are two options available when you create a Continuity of Care
            Record (CCR):
            <ul>
              <li>
                Generate Report - that opens the CCR in a separate tab on the
                browser
                <GenerateReport />
              </li>

              <li style={{ marginTop: "10px" }}>
                Download - the created CCR is downloaded as a pdf file to the
                downloads from the browser
                <Download />
              </li>
            </ul>
          </Box>
        </Box>

        <Box id="ccd" sx={{ marginBottom: "16px" }}>
          <Typography
            sx={{
              color: "#cdcdcd",
              fontWeight: 500,
              fontSize: "24px",
              marginBottom: "16px",
              "&:hover": {
                color: "black",
              },
            }}
          >
            Continuity of Care Document (CCD)
          </Typography>

          <Typography
            sx={{ marginBottom: "16px", fontSize: "16px", color: "black" }}
          >
            Continuity of Care Document (CCD) is an electronic document
            containing patient specific information that aids in continuity of
            care.
          </Typography>

          <Typography
            sx={{ marginBottom: "16px", fontSize: "16px", color: "black" }}
          >
            It contains structured data that is included in the Continuity of
            Care Record (CCR) set to the the Clinical Document Architecture
            standard.
          </Typography>

          <Typography
            sx={{
              marginBottom: "16px",
              fontSize: "16px",
              color: "black",
              display: "flex",
              alignItems: "center",
            }}
          >
            Click to learn more about the Clinical Document Architecture (CDA)
            and Consolidated CDA (C-CDA)
            <VisibilityIcon
              sx={{
                marginLeft: "10px",
                display: !openCCD ? "block" : "none",
                cursor: "pointer",
                color: "GrayText",
              }}
              onClick={handleViewCCD}
            />
            <VisibilityOffIcon
              sx={{
                marginLeft: "10px",
                display: openCCD ? "block" : "none",
                cursor: "pointer",
                color: "GrayText",
              }}
              onClick={handleViewCCD}
            />
          </Typography>

          {openCCD && (
            <Box
              sx={{
                borderRadius: "5px",
                border: "6px solid #cdcdcd",
                background: "#e9ecef",
                padding: "16px",
                color: "#cdcdcd",
                fontStyle: "italic",
                marginBottom: "16px",
              }}
            >
              <Typography sx={{ marginBottom: "16px", fontSize: "16px" }}>
                CDA or Clinical Document Architecture is a document standard
                developed by the HL7 organization.
              </Typography>

              <Typography sx={{ marginBottom: "16px", fontSize: "16px" }}>
                CDA defines building blocks which can be used to contain
                healthcare data elements that can be captured, stored, accessed,
                displayed and transmitted electronically for use and reuse in
                many formats.
              </Typography>

              <Typography sx={{ marginBottom: "16px", fontSize: "16px" }}>
                Clinical documents are produced by arranging or limiting CDA
                elements in defined ways using templates and Implementation
                Guides (IG).
              </Typography>

              <Typography sx={{ marginBottom: "16px", fontSize: "16px" }}>
                The CDA is a plain text document that is coded using the
                Extensible Markup Language (XML) and contains a Header and Body.
              </Typography>

              <Typography sx={{ marginBottom: "16px", fontSize: "16px" }}>
                The Header defines the context for the clinical document as a
                whole.
              </Typography>

              <Typography sx={{ marginBottom: "16px", fontSize: "16px" }}>
                The Body contains the clinical data that may be structured and
                organized as one or more sections or may be an unstructured blob
                of data.
              </Typography>

              <Typography sx={{ marginBottom: "16px", fontSize: "16px" }}>
                Each Section has one Narrative Block and zero to many coded
                Entries.
              </Typography>

              <Typography sx={{ marginBottom: "16px", fontSize: "16px" }}>
                The Narrative Block contains information that is rendered as
                readable text.
              </Typography>

              <Typography sx={{ marginBottom: "16px", fontSize: "16px" }}>
                The Entries contain data used for further computer processing.
              </Typography>

              <Typography sx={{ marginBottom: "16px", fontSize: "16px" }}>
                Examples of Sections are - Medications, Allergies, Vital Signs.
              </Typography>

              <Typography sx={{ marginBottom: "16px", fontSize: "16px" }}>
                The initial efforts led to the creation of duplicative and
                conflicting Implementation Guides (IGs) published by different
                standards organizations.
              </Typography>

              <Typography sx={{ marginBottom: "16px", fontSize: "16px" }}>
                The end result was a confusing collection of documents
                containing ambiguous and/or conflicting information.
              </Typography>

              <Typography sx={{ marginBottom: "16px", fontSize: "16px" }}>
                To help simplify implementations, commonly used templates were
                harmonized from existing CDA implementation guides and
                consolidated into a single implementation guide - the C-CDA
                Implementation Guide (IG) (07/2012).
              </Typography>

              <Box sx={{ marginBottom: "16px", fontSize: "16px" }}>
                There are nine C-CDA templates that are currently defined:
                <ul>
                  {templateCDA.map((section, index) => (
                    <li key={index} style={{ marginTop: "5px" }}>
                      {section}
                    </li>
                  ))}
                </ul>
              </Box>

              <Typography sx={{ marginBottom: "16px", fontSize: "16px" }}>
                The Continuity of Care Document (CCD) and Continuity of Care
                Record (CCR) were both selected as acceptable formats to extract
                information for clinical care summaries as a part of Meaningful
                Use Stage 1.
              </Typography>

              <Typography sx={{ fontSize: "16px" }}>
                In the second stage of Meaningful Use, the CCD, but not the CCR,
                was included as part of the standard for clinical document
                exchange.
              </Typography>
            </Box>
          )}

          <Typography
            sx={{ marginBottom: "16px", fontSize: "16px", color: "black" }}
          >
            Continuity of Care Document (CCD) is meant to be created at the
            conclusion of an encounter prior to transfer of care to enable the
            next provider to easily access such information.
          </Typography>

          <Box sx={{ marginBottom: "16px", fontSize: "16px" }}>
            It serves as a necessary bridge to a different environment, often
            with new clinicians who know nothing about the patient, enabling
            next provider to easily
            <ul>
              <li style={{ marginTop: "5px" }}>
                Access core data set of patient information at the beginning of
                an encounter
              </li>

              <li style={{ marginTop: "5px" }}>
                Update information when the patient goes to another provider, to
                support safety, quality, and continuity of patient care
              </li>
            </ul>
          </Box>

          <Box sx={{ fontSize: "16px", color: "black" }}>
            There are two options available when you create a Continuity of Care
            Document (CCD):
            <ul>
              <li>
                Generate Report - that opens the CCD in a separate tab on the
                browser
                <GenerateReport />
              </li>

              <li style={{ marginTop: "10px" }}>
                Download - the created CCD is downloaded as a pdf file to the
                downloads from the browser
                <Download />
              </li>
            </ul>
          </Box>
        </Box>

        <Box id="report">
          <Typography
            sx={{
              color: "#cdcdcd",
              fontWeight: 500,
              fontSize: "24px",
              marginBottom: "16px",
              "&:hover": {
                color: "black",
              },
            }}
          >
            Patient Report
          </Typography>

          <Typography
            sx={{ marginBottom: "16px", fontSize: "16px", color: "black" }}
          >
            Creates a report that contains various sections of the patient's
            medical record.
          </Typography>

          <Typography
            sx={{ marginBottom: "16px", fontSize: "16px", color: "black" }}
          >
            The created report can contain as much or as little information
            based on the need.
          </Typography>

          <Typography
            sx={{ marginBottom: "16px", fontSize: "16px", color: "black" }}
          >
            By default only Demographics and Billing information is selected.
          </Typography>

          <Box sx={{ marginBottom: "16px", fontSize: "16px", color: "black" }}>
            There are two buttons that enable you to Check or Clear all
            available check-boxes.
            <Box sx={{ display: "flex", gap: 1 }}>
              <Button
                variant="outlined"
                sx={{ display: "flex", borderRadius: "8px" }}
              >
                <DoneAllIcon
                  sx={{ width: "18px", heigth: "18px", marginRight: "5px" }}
                />
                <Typography sx={{ fontSize: "14px" }}>Check All</Typography>
              </Button>

              <Button
                variant="outlined"
                sx={{ display: "flex", borderRadius: "8px" }}
              >
                <SettingsBackupRestoreIcon
                  sx={{ width: "18px", heigth: "18px", marginRight: "5px" }}
                />
                <Typography sx={{ fontSize: "14px" }}>Clear All</Typography>
              </Button>
            </Box>
          </Box>

          <Typography
            sx={{ marginBottom: "16px", fontSize: "16px", color: "black" }}
          >
            Alternatively you could clear all selections and select only the
            items that you want to be a part of the report.
          </Typography>

          <Typography
            sx={{ marginBottom: "16px", fontSize: "16px", color: "black" }}
          >
            To include the scanned documents that are a part of the patient`s
            record select the desired records by check the relevant check-boxes.
          </Typography>

          <Box sx={{ fontSize: "16px", color: "black", marginBottom: "16px" }}>
            There are two options for the records that will be created:
            <ul>
              <li>
                Generate Report that creates a report and displays it in a
                separate tab on the browser
                <GenerateReport />
              </li>

              <li style={{ marginTop: "10px" }}>
                Download report as a pdf file into the browser's download folder
                <Button
                  variant="contained"
                  sx={{ display: "flex", borderRadius: "8px" }}
                >
                  <DownloadIcon
                    sx={{ width: "18px", heigth: "18px", marginRight: "5px" }}
                  />
                  <Typography sx={{ fontSize: "14px" }}>
                    Download PDF
                  </Typography>
                </Button>
              </li>
            </ul>
          </Box>

          <Typography sx={{ fontSize: "16px", color: "black" }}>
            When the generated report is displayed in a separate tab there is an
            option that lets you view a Printable Version that can be printed.
          </Typography>
        </Box>
      </DialogContent>
      <DialogActions sx={{ height: "70px", padding: "0 16px" }}>
        <Button autoFocus onClick={handleClose} sx={{ display: "flex" }}>
          <CloseIcon />
          Close
        </Button>
      </DialogActions>
    </Dialog>
  );
};
