import {
  Box,
  Button,
  Divider,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useNavigate, useSearchParams } from "react-router-dom";
import { selectCheckList } from "../../../store/reducers/patientRecord/patientRecordSlice";
import { ReactComponent as BackArrow } from "../../../assets/image/BackArrow.svg";
import DownloadIcon from "@mui/icons-material/Download";
import { Appointment } from "../../../store/reducers/appointment/appointmentSlice";
import { immunizationModel } from "../../../models/immunization";
import { patientModel } from "../../../models/patients";
import { insuranceModel } from "../../../models/insurance";
import { relatedPatientModel } from "../../../models/relatedPatient";
import { claimsModel } from "../../../models/claims";
import Loading from "../../../components/Loading";
import {
  appointmentService,
  claimService,
  conditionService,
  encounterService,
  immunizationService,
  insuranceService,
  observationService,
  patientService,
  relatedPatientService,
} from "../../../services/agent";
import {
  GetRequestBase,
  GetRequestType,
} from "../../../models/shared/basePagedResponse";
import { observationModel } from "../../../models/observation";
import { ObservationGetRequest } from "../../../models/apiRequest/observation";
import { EncounterModel } from "../../../models/encounter";
import { ConditionModel } from "../../../models/conditions";

function createInfo(
  field: string,
  firstLabel: string,
  firstValue: string,
  secondLabel: string,
  secondValue: string | boolean
) {
  return { field, firstLabel, firstValue, secondLabel, secondValue };
}

export const RecordPDF: React.FC = () => {
  const checkList = useSelector(selectCheckList);
  const [loading, setLoading] = React.useState<boolean>(true);
  const [searchParams, setSearchParams] = useSearchParams();
  const id = searchParams.get("id");

  let {
    informationCheck,
    appointmentCheck,
    immunizationCheck,
    insuranceCheck,
    observationCheck,
    relatedCheck,
    claimCheck,
    encounterCheck,
    conditionCheck,
  } = checkList;

  const navigate = useNavigate();

  const [patientById, setPatientById] = useState<patientModel>({});
  const [appointment, setAppointment] = useState<Appointment[]>([]);
  const [immunization, setImmunization] = useState<immunizationModel[]>([]);
  const [insurance, setInsurance] = useState<insuranceModel[]>([]);
  const [observation, setObservation] = useState<observationModel[]>([]);
  const [related, setRelated] = useState<relatedPatientModel[]>([]);
  const [claim, setClaim] = useState<claimsModel[]>([]);
  const [encounter, setEncounter] = useState<EncounterModel[]>([]);
  const [condition, setCondition] = useState<ConditionModel[]>([]);

  useEffect(() => {
    if (!id) throw history.back();
    //------------
    patientService
      .getById(id)
      .then((res) => {
        setPatientById(res);
        setLoading(false);
      })
      .catch(() => history.back());
    //------------
    appointmentService
      .getList({ page: 1, size: 10 } as GetRequestType)
      .then((res) => {
        setAppointment(res.data as Appointment[]);
        setLoading(false);
      })
      .catch(() => history.back());
    //------------
    immunizationService
      .getList({ page: 1, size: 10 } as GetRequestType)
      .then((res) => {
        setImmunization(res.data as immunizationModel[]);
        setLoading(false);
      })
      .catch(() => history.back());
    //------------
    insuranceService
      .getList({ page: 1, size: 10 } as GetRequestType)
      .then((res) => {
        setInsurance(res.data as insuranceModel[]);
        setLoading(false);
      })
      .catch(() => history.back());
    //------------
    observationService
      .getList({ page: 1, size: 10 } as GetRequestBase<ObservationGetRequest>)
      .then((res) => {
        setObservation(res.data as observationModel[]);
        setLoading(false);
      })
      .catch(() => history.back());
    //------------
    relatedPatientService
      .getList({ page: 1, size: 10 } as GetRequestType)
      .then((res) => {
        setRelated(res.data as relatedPatientModel[]);
        setLoading(false);
      })
      .catch(() => history.back());
    //------------
    claimService
      .getList({ page: 1, size: 10 } as GetRequestType)
      .then((res) => {
        setClaim(res.data as claimsModel[]);
        setLoading(false);
      })
      .catch(() => history.back());
    //------------
    encounterService
      .getList({ page: 1, size: 10 } as GetRequestType)
      .then((res) => {
        setEncounter(res.data as EncounterModel[]);
        setLoading(false);
      })
      .catch(() => history.back());
    //------------
    conditionService
      .getList({ page: 1, size: 10 } as GetRequestType)
      .then((res) => {
        setCondition(res.data as ConditionModel[]);
        setLoading(false);
      })
      .catch(() => history.back());
  }, [id]);

  //-------------Information------------

  const fullNamePatient = patientById?.name?.filter(
    (x) => x?.use === "official"
  );

  const phoneData = patientById?.telecom?.find(
    (item) => item?.system === "phone"
  );

  const emailData = patientById?.telecom?.find(
    (item) => item?.system === "email"
  );

  const inforData = [
    createInfo(
      "Who",
      "Name:",
      fullNamePatient?.[0]?.family && fullNamePatient?.[0]?.given
        ? `${fullNamePatient?.[0]?.given?.join(" ")} ${
            fullNamePatient?.[0]?.family
          }`
        : `${fullNamePatient?.[0]?.text}` || "",
      "",
      ""
    ),
    createInfo(
      "",
      "External ID:",
      patientById?.identifier?.[0]?.value || "",
      "DOB:",
      patientById?.birthDate || ""
    ),
    createInfo(
      "",
      "Sex:",
      `${patientById?.gender
        ?.charAt(0)
        .toUpperCase()}${patientById?.gender?.slice(1)}` || "",
      "Marital Status:",
      patientById?.maritalStatus?.text || ""
    ),
    createInfo(
      "Contact",
      "Address:",
      patientById?.address?.[0]?.text || "",
      "",
      ""
    ),
    createInfo(
      "",
      "City:",
      patientById?.address?.[0]?.city || "",
      "State:",
      patientById?.address?.[0]?.state || ""
    ),
    createInfo(
      "",
      "Postal Code:",
      patientById?.address?.[0]?.postalCode || "",
      "Country:",
      patientById?.address?.[0]?.country || ""
    ),
    createInfo(
      "",
      "Contact Phone:",
      phoneData?.value || "",
      "Contact Email:",
      emailData?.value || ""
    ),
    createInfo(
      "Choices",
      "HIPAA Notice Received:",
      "YES",
      "Care Team Status:",
      patientById?.active === true ? "Active" : "Inactive"
    ),
    createInfo("", "Allow SMS:", "YES", "Allow Email:", "YES"),
    createInfo(
      "",
      "Allow Patient Portal:",
      "YES",
      "Allow Health Information Exchange:",
      "YES"
    ),
  ];

  //---------------Appointment----------------

  const headAppointment = [
    "Doctor",
    "Status",
    "Duration",
    "Time",
    "Date",
    "Consulting Topic",
  ];

  //------------Immunization------------------

  const headImmunization = [
    "Vaccine",
    "Status",
    "Lot Number",
    "Dose Quantity",
    "Expired Date",
    "Notes",
  ];

  //----------------Insurance-----------------

  const headInsurance = [
    "Name",
    "Type",
    "Status",
    "Period",
    "Administrator",
    "Coverage Area",
  ];

  //---------------Observation--------------

  const headObservation = [
    "Name",
    "Status",
    "Classification",
    "Type",
    "Period",
    "Issued",
    "Result",
    "Method",
  ];

  //--------------Related Patient---------------

  const headRelated = [
    "Name",
    "Gender",
    "Relationship",
    "Status",
    "Phone Number",
    "Address",
  ];

  //-------------Claim-----------------------

  const headClaim = [
    "External ID",
    "Status",
    "Category",
    "Use",
    "Patient",
    "Created",
    "Party Responsible",
    "Process Priority",
  ];

  //--------------Encounter------------------

  const headEncounter = [
    "Patient",
    "Date",
    "Time",
    "Duration",
    "Type",
    "Reason",
    "Status",
  ];

  //------------Condition-----------------------

  const headCondition = [
    "Patient Name",
    "Date",
    "Time",
    "Severity",
    "Disease",
    "Body Site",
    "Verification Status",
  ];

  return (
    <Box>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          marginBottom: "16px",
        }}
      >
        <Box sx={{ display: "flex", gap: 1, alignItems: "center" }}>
          <IconButton
            onClick={() => history.back()}
            style={{
              width: "40px",
              height: "40px",
              border: "1px solid #F4F4F4",
              background: "white",
            }}
          >
            <BackArrow />
          </IconButton>
          <Typography variant="h2" sx={{ fontWeight: 600, fontSize: "24px" }}>
            Patient Report
          </Typography>
        </Box>

        <Button
          variant="contained"
          sx={{ display: "flex", borderRadius: "8px" }}
        >
          <DownloadIcon
            sx={{ width: "18px", heigth: "18px", marginRight: "5px" }}
          />
          <Typography sx={{ fontSize: "14px" }}>Download PDF</Typography>
        </Button>
      </Box>

      {loading && <Loading />}
      {!loading && (
        <Box
          sx={{
            background: "white",
            padding: "16px",
            borderRadius: "8px",
          }}
        >
          {informationCheck && (
            <Box sx={{ marginBottom: "40px" }}>
              <Typography
                variant="h4"
                sx={{
                  fontSize: "20px",
                  fontWeigth: 700,
                  marginBottom: "20px",
                }}
              >
                Patient Data:
              </Typography>

              <Divider />
              <TableContainer>
                <Table sx={{ minWidth: 650 }}>
                  <TableBody>
                    {inforData?.map((item, index) => (
                      <TableRow key={index}>
                        <TableCell sx={{ color: "#28a745", fontWeight: 700 }}>
                          {item.field}
                        </TableCell>
                        <TableCell sx={{ fontWeight: 700 }}>
                          {item.firstLabel}
                        </TableCell>
                        <TableCell>{item.firstValue}</TableCell>
                        <TableCell sx={{ fontWeight: 700 }}>
                          {item.secondLabel}
                        </TableCell>
                        <TableCell>{item.secondValue}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </Box>
          )}

          {appointmentCheck && (
            <Box sx={{ marginBottom: "40px" }}>
              <Typography
                variant="h4"
                sx={{
                  fontSize: "20px",
                  fontWeigth: 700,
                  marginBottom: "20px",
                }}
              >
                Appointment:
              </Typography>

              <Divider />
              <TableContainer>
                <Table sx={{ minWidth: 650 }}>
                  <TableHead>
                    <TableRow>
                      {headAppointment.map((item, index) => (
                        <TableCell key={index} sx={{ fontWeight: 700 }}>
                          {item}
                        </TableCell>
                      ))}
                    </TableRow>
                  </TableHead>

                  <TableBody>
                    {appointment?.map((item) => (
                      <TableRow key={item.id}>
                        <TableCell>
                          {item?.participant?.map((x) => {
                            if (x?.actor?.type === "Practitioner")
                              return x?.actor?.display;
                          }) || "-"}
                        </TableCell>
                        <TableCell>
                          {item?.status?.charAt(0).toUpperCase() +
                            item?.status?.slice(1) || ""}
                        </TableCell>
                        <TableCell>{item?.minutesDuration} mins</TableCell>
                        <TableCell>{item?.start?.slice(11, 19)}</TableCell>
                        <TableCell>{item?.start?.slice(0, 10)}</TableCell>
                        <TableCell>{item?.description || "-"}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </Box>
          )}

          {immunizationCheck && (
            <Box sx={{ marginBottom: "40px" }}>
              <Typography
                variant="h4"
                sx={{
                  fontSize: "20px",
                  fontWeigth: 700,
                  marginBottom: "20px",
                }}
              >
                Immunization:
              </Typography>

              <Divider />
              <TableContainer>
                <Table>
                  <TableHead>
                    <TableRow>
                      {headImmunization.map((item, index) => (
                        <TableCell key={index} sx={{ fontWeight: 700 }}>
                          {item}
                        </TableCell>
                      ))}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {immunization?.map((item) => (
                      <TableRow key={item.id}>
                        <TableCell>{item?.vaccineCode?.text || "-"}</TableCell>
                        <TableCell>
                          {`${item?.status
                            ?.charAt(0)
                            .toUpperCase()}${item?.status?.slice(1)}` || ""}
                        </TableCell>
                        <TableCell>{item?.lotNumber || "-"}</TableCell>
                        <TableCell>
                          {item?.doseQuantity?.value || "-"}
                        </TableCell>
                        <TableCell>{item?.expirationDate || "-"}</TableCell>
                        <TableCell>{item?.note?.[0]?.text || "-"}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </Box>
          )}

          {insuranceCheck && (
            <Box sx={{ marginBottom: "40px" }}>
              <Typography
                variant="h4"
                sx={{
                  fontSize: "20px",
                  fontWeigth: 700,
                  marginBottom: "20px",
                }}
              >
                Insurance:
              </Typography>

              <Divider />
              <TableContainer>
                <Table>
                  <TableHead>
                    <TableRow>
                      {headInsurance.map((item, index) => (
                        <TableCell key={index} sx={{ fontWeight: 700 }}>
                          {item}
                        </TableCell>
                      ))}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {insurance?.map((item) => (
                      <TableRow key={item.id}>
                        <TableCell>{item?.name || "-"}</TableCell>
                        <TableCell>
                          {item?.type?.[0]?.coding?.[0]?.display || "-"}
                        </TableCell>
                        <TableCell>
                          {`${item?.status
                            ?.charAt(0)
                            .toUpperCase()}${item?.status?.slice(1)}` || ""}
                        </TableCell>
                        <TableCell>
                          {item?.period?.start?.slice(0, 4) || "N/A"} -{" "}
                          {item?.period?.end?.slice(0, 4) ||
                            new Date().getFullYear()}
                        </TableCell>
                        <TableCell>
                          {item?.administeredBy?.display || "-"}
                        </TableCell>
                        <TableCell>
                          {item?.coverageArea?.[0]?.display || "-"}
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </Box>
          )}

          {observationCheck && (
            <Box sx={{ marginBottom: "40px" }}>
              <Typography
                variant="h4"
                sx={{
                  fontSize: "20px",
                  fontWeigth: 700,
                  marginBottom: "20px",
                }}
              >
                Observation:
              </Typography>

              <Divider />
              <TableContainer>
                <Table>
                  <TableHead>
                    <TableRow>
                      {headObservation.map((item, index) => (
                        <TableCell key={index} sx={{ fontWeight: 700 }}>
                          {item}
                        </TableCell>
                      ))}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {observation?.map((item) => (
                      <TableRow key={item.id}>
                        <TableCell>{item?.subject?.display || "-"}</TableCell>
                        <TableCell>
                          {`${item?.status
                            ?.charAt(0)
                            .toUpperCase()}${item?.status?.slice(1)}` || ""}
                        </TableCell>
                        <TableCell>
                          {item?.category?.[0]?.coding?.[0]?.display}
                        </TableCell>
                        <TableCell>
                          {item?.code?.coding?.[0]?.display}
                        </TableCell>
                        <TableCell>
                          {item?.effectivePeriod?.start?.slice(0, 4) || "N/A"} -{" "}
                          {item?.effectivePeriod?.end?.slice(0, 4) ||
                            new Date().getFullYear()}
                        </TableCell>
                        <TableCell>
                          {item?.issued?.slice(0, 10) || "-"}
                        </TableCell>
                        <TableCell>
                          {item?.value
                            ? `${item?.value?.value || "-"} ${
                                item?.value?.unit
                              }`
                            : "-"}
                        </TableCell>
                        <TableCell>
                          {item?.method?.coding?.[0]?.display || "-"}
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </Box>
          )}

          {relatedCheck && (
            <Box sx={{ marginBottom: "40px" }}>
              <Typography
                variant="h4"
                sx={{
                  fontSize: "20px",
                  fontWeigth: 700,
                  marginBottom: "20px",
                }}
              >
                Related Patient:
              </Typography>

              <Divider />
              <TableContainer>
                <Table>
                  <TableHead>
                    <TableRow>
                      {headRelated.map((item, index) => (
                        <TableCell key={index} sx={{ fontWeight: 700 }}>
                          {item}
                        </TableCell>
                      ))}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {related?.map((item) => (
                      <TableRow key={item.id}>
                        <TableCell>
                          {item?.name?.[0]?.family && item?.name?.[0]?.given
                            ? `${item?.name?.[0]?.given?.join(" ")} ${
                                item?.name?.[0]?.family
                              }`
                            : `${item?.name?.[0]?.text}`}
                        </TableCell>
                        <TableCell>
                          {`${item?.gender
                            ?.charAt(0)
                            .toUpperCase()}${item?.gender?.slice(1)}` || "-"}
                        </TableCell>
                        <TableCell>
                          {item.relationship
                            ? item.relationship[0].coding
                              ? item.relationship[0].coding[1]
                                ? (item.relationship[0].coding[1]
                                    .code as string)
                                : "-"
                              : "-"
                            : "-"}
                        </TableCell>
                        <TableCell>
                          {item?.active === true ? "Active" : "Inactive"}
                        </TableCell>
                        <TableCell>
                          {item?.telecom?.map((x) => {
                            if (x?.system === "phone") return x?.value;
                          }) || "-"}
                        </TableCell>
                        <TableCell>
                          {item.address
                            ? item.address[0].line
                              ? (item.address[0].line[0] as string)
                              : "-"
                            : "-"}
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </Box>
          )}

          {claimCheck && (
            <Box>
              <Typography
                variant="h4"
                sx={{
                  fontSize: "20px",
                  fontWeigth: 700,
                  marginBottom: "20px",
                }}
              >
                Claim:
              </Typography>

              <Divider />
              <TableContainer>
                <Table>
                  <TableHead>
                    <TableRow>
                      {headClaim.map((item, index) => (
                        <TableCell key={index} sx={{ fontWeight: 700 }}>
                          {item}
                        </TableCell>
                      ))}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {claim?.map((item) => (
                      <TableRow key={item.id}>
                        <TableCell>
                          {item?.identifier?.[0]?.value || "-"}
                        </TableCell>
                        <TableCell>
                          {" "}
                          {`${item?.status
                            ?.charAt(0)
                            .toUpperCase()}${item?.status?.slice(1)}` || "-"}
                        </TableCell>
                        <TableCell>
                          {`${item?.type?.coding?.[0]?.code
                            ?.charAt(0)
                            .toUpperCase()}${item?.type?.coding?.[0]?.code?.slice(
                            1
                          )}` || "-"}
                        </TableCell>
                        <TableCell>
                          {item?.use ? "Claim" : "Preauthorization"}
                        </TableCell>
                        <TableCell>{item?.patient?.display || "-"}</TableCell>
                        <TableCell>
                          {item?.created || new Date().getFullYear()}
                        </TableCell>
                        <TableCell>{item?.provider?.display || "-"}</TableCell>
                        <TableCell>{item?.priority?.display || "-"}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </Box>
          )}

          {encounterCheck && (
            <Box sx={{ marginBottom: "40px" }}>
              <Typography
                variant="h4"
                sx={{
                  fontSize: "20px",
                  fontWeigth: 700,
                  marginBottom: "20px",
                }}
              >
                Encounter:
              </Typography>

              <Divider />
              <TableContainer>
                <Table>
                  <TableHead>
                    <TableRow>
                      {headEncounter.map((item, index) => (
                        <TableCell key={index} sx={{ fontWeight: 700 }}>
                          {item}
                        </TableCell>
                      ))}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {encounter?.map((item) => (
                      <TableRow key={item.id}>
                        <TableCell>{item?.subject?.display || "-"}</TableCell>
                        <TableCell>
                          {new Date(
                            item?.identifier?.[0]?.period?.start as string
                          ).toLocaleDateString() || ""}
                        </TableCell>
                        <TableCell>
                          {new Date(
                            item?.identifier?.[0]?.period?.start as string
                          ).toLocaleTimeString() || "-"}
                        </TableCell>
                        <TableCell>
                          {(item?.length?.value as number) +
                            " " +
                            (item.length?.code as string) || "-"}
                        </TableCell>
                        <TableCell>
                          {item?.type?.[0]?.coding?.[0]?.display || "-"}
                        </TableCell>
                        <TableCell>
                          {item?.reasonCode?.[0]?.coding?.[0]?.display || "-"}
                        </TableCell>
                        <TableCell>{item?.status || "-"}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </Box>
          )}

          {conditionCheck && (
            <Box sx={{ marginBottom: "40px" }}>
              <Typography
                variant="h4"
                sx={{
                  fontSize: "20px",
                  fontWeigth: 700,
                  marginBottom: "20px",
                }}
              >
                Condition:
              </Typography>

              <Divider />
              <TableContainer>
                <Table>
                  <TableHead>
                    <TableRow>
                      {headCondition.map((item, index) => (
                        <TableCell key={index} sx={{ fontWeight: 700 }}>
                          {item}
                        </TableCell>
                      ))}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {condition?.map((item) => (
                      <TableRow key={item.id}>
                        <TableCell>{item?.subject?.display || "-"}</TableCell>
                        <TableCell>
                          {new Date(
                            item?.identifier?.[0]?.period?.start as string
                          ).toLocaleDateString() || ""}
                        </TableCell>
                        <TableCell>
                          {new Date(
                            item?.identifier?.[0]?.period?.start as string
                          ).toLocaleTimeString() || "-"}
                        </TableCell>
                        <TableCell>
                          {item?.severity?.coding?.[0]?.display || "-"}
                        </TableCell>
                        <TableCell>
                          {item?.code?.coding?.[0]?.display || "-"}
                        </TableCell>
                        <TableCell>
                          {item?.bodySite?.[0]?.coding?.[0]?.display || "-"}
                        </TableCell>
                        <TableCell>
                          {item?.verificationStatus?.coding?.[0]?.code || "-"}
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </Box>
          )}
        </Box>
      )}
    </Box>
  );
};
