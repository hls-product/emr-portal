import {
  Box,
  Button,
  Checkbox,
  Divider,
  FormControl,
  FormControlLabel,
  FormGroup,
  IconButton,
  TextField,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import { getCheckList } from "../../../store/reducers/patientRecord/patientRecordSlice";
import { useAppDispatch } from "../../../store/Store";
import QuestionMarkIcon from "@mui/icons-material/QuestionMark";
import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import CheckIcon from "@mui/icons-material/Check";
import DownloadIcon from "@mui/icons-material/Download";
import AddTaskIcon from "@mui/icons-material/AddTask";
import DoneAllIcon from "@mui/icons-material/DoneAll";
import SettingsBackupRestoreIcon from "@mui/icons-material/SettingsBackupRestore";
import Loading from "../../../components/Loading";
import { DetailDialog } from "./DetailDialog";
import { patientService } from "../../../services/agent";

interface TitleProps {
  title?: string;
  subTitle?: string;
}

export interface CheckList {
  appointmentCheck?: boolean;
  claimCheck?: boolean;
  immunizationCheck?: boolean;
  informationCheck?: boolean;
  insuranceCheck?: boolean;
  invoiceCheck?: boolean;
  observationCheck?: boolean;
  relatedCheck?: boolean;
  encounterCheck?: boolean;
  conditionCheck?: boolean;
}

export const RenderTitle = (props: TitleProps) => {
  const { title, subTitle } = props;
  return (
    <Box sx={{ display: "flex", alignItems: "center" }}>
      <Typography
        sx={{ fontSize: "20px", fontWeigth: 600, marginRight: "10px" }}
      >
        {title}
      </Typography>
      <Typography sx={{ fontSize: "14px" }}>{subTitle}</Typography>
    </Box>
  );
};

export const GenerateReport = () => {
  return (
    <Button variant="contained" sx={{ display: "flex", borderRadius: "8px" }}>
      <CheckIcon sx={{ width: "18px", heigth: "18px", marginRight: "5px" }} />
      <Typography sx={{ fontSize: "14px" }}>Generate Report</Typography>
    </Button>
  );
};

export const Download = () => {
  return (
    <Button variant="contained" sx={{ display: "flex", borderRadius: "8px" }}>
      <DownloadIcon
        sx={{ width: "18px", heigth: "18px", marginRight: "5px" }}
      />
      <Typography sx={{ fontSize: "14px" }}>Download</Typography>
    </Button>
  );
};

const GenerateNewReport = () => {
  return (
    <Button variant="contained" sx={{ display: "flex", borderRadius: "8px" }}>
      <AddTaskIcon sx={{ width: "18px", heigth: "18px", marginRight: "5px" }} />
      <Typography sx={{ fontSize: "14px" }}>Generate New Report</Typography>
    </Button>
  );
};

export const PatientRecords: React.FC = () => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();

  const [loading, setLoading] = useState<boolean>(true);
  const [searchParams, setSearchParams] = useSearchParams();
  const id = searchParams.get("id");
  const [patientName, setPatientName] = useState("");

  useEffect(() => {
    if (!id) throw history.back();
    patientService
      .getById(id)
      .then((res) => {
        setPatientName(() => {
          const name = res.name?.find((item) => item.use === "official");
          return name?.text || name?.given?.join(" ") + " " + name?.family;
        });
        setLoading(false);
      })
      .catch(() => history.back());
  }, [id]);

  //--------------CCR--------------------------

  const [isCheckCCR, setIsCheckCCR] = useState(false);
  const [startPeriod, setStartPeriod] = useState<string>("2014-08-18T21:11:54");
  const [endPeriod, setEndPeriod] = useState<string>("2015-08-18T21:11:54");

  const handleChangeStart = (value: any) => {
    setStartPeriod(value.toISOString().replace(/.\d+Z$/g, "Z") as string);
  };

  const handleChangeEnd = (value: any) => {
    setEndPeriod(value.toISOString().replace(/.\d+Z$/g, "Z") as string);
  };

  const ChooseDateCCR = () => {
    return (
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          marginTop: "16px",
        }}
      >
        <LocalizationProvider dateAdapter={AdapterDayjs}>
          <DesktopDatePicker
            label="Star Date"
            inputFormat="MM/DD/YYYY"
            value={startPeriod}
            onChange={handleChangeStart}
            renderInput={(params) => (
              <TextField
                sx={{ width: "45%" }}
                {...params}
                error={startPeriod ? startPeriod >= endPeriod : true}
              />
            )}
            InputProps={{
              style: {
                height: "48px",
                borderRadius: "12px",
                color: "black",
              },
            }}
          />
        </LocalizationProvider>

        <LocalizationProvider dateAdapter={AdapterDayjs}>
          <DesktopDatePicker
            label="End Date"
            inputFormat="MM/DD/YYYY"
            value={endPeriod}
            onChange={handleChangeEnd}
            renderInput={(params) => (
              <TextField
                sx={{ width: "45%" }}
                {...params}
                error={endPeriod ? endPeriod <= startPeriod : true}
              />
            )}
            InputProps={{
              style: {
                height: "48px",
                borderRadius: "12px",
                color: "black",
              },
            }}
          />
        </LocalizationProvider>
      </Box>
    );
  };

  //----------------patient report-----------------

  const [checkList, setCheckList] = useState<CheckList>({
    informationCheck: true,
    appointmentCheck: false,
    immunizationCheck: false,
    insuranceCheck: false,
    observationCheck: false,
    relatedCheck: false,
    claimCheck: false,
    encounterCheck: false,
    conditionCheck: false,
  });

  const {
    informationCheck,
    appointmentCheck,
    immunizationCheck,
    insuranceCheck,
    observationCheck,
    relatedCheck,
    claimCheck,
    encounterCheck,
    conditionCheck,
  } = checkList;

  const CheckArray = [
    {
      name: "informationCheck",
      check: informationCheck,
      label: "Information",
    },
    {
      name: "appointmentCheck",
      check: appointmentCheck,
      label: "Appointment",
    },
    {
      name: "immunizationCheck",
      check: immunizationCheck,
      label: "Immunization",
    },
    {
      name: "insuranceCheck",
      check: insuranceCheck,
      label: "Insurance",
    },
    {
      name: "observationCheck",
      check: observationCheck,
      label: "Observation",
    },
    {
      name: "relatedCheck",
      check: relatedCheck,
      label: "Related Patient",
    },
    {
      name: "claimCheck",
      check: claimCheck,
      label: "Claim",
    },
    {
      name: "encounterCheck",
      check: encounterCheck,
      label: "Encounter",
    },
    {
      name: "conditionCheck",
      check: conditionCheck,
      label: "Condition",
    },
  ];

  const handleCheckList = (event: React.ChangeEvent<HTMLInputElement>) => {
    setCheckList({
      ...checkList,
      [event.target.name]: event.target.checked,
    });
  };

  const handleCheckAll = () => {
    setCheckList({
      informationCheck: true,
      appointmentCheck: true,
      immunizationCheck: true,
      insuranceCheck: true,
      observationCheck: true,
      relatedCheck: true,
      claimCheck: true,
      encounterCheck: true,
      conditionCheck: true,
    });
  };

  const handleClearAll = () => {
    setCheckList({
      informationCheck: false,
      appointmentCheck: false,
      immunizationCheck: false,
      insuranceCheck: false,
      observationCheck: false,
      relatedCheck: false,
      claimCheck: false,
      encounterCheck: false,
      conditionCheck: false,
    });
  };

  const handleViewPDF = () => {
    dispatch(getCheckList(checkList));
    navigate(`patient-record?id=${id}`);
  };

  //----------------Dialog------------------------

  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      {loading && <Loading />}
      {!loading && (
        <Box sx={{ background: "white", borderRadius: "8px", padding: "16px" }}>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              marginBottom: "16px",
            }}
          >
            <Typography sx={{ fontSize: "24px", fontWeight: 600 }}>
              Patient Reports - {patientName}
            </Typography>
            <IconButton
              sx={{ border: "1px solid #eaeaea" }}
              onClick={handleClickOpen}
            >
              <QuestionMarkIcon />
            </IconButton>
          </Box>

          <Box
            sx={{
              background: "#FAFAFA",
              padding: "16px",
              borderRadius: "8px",
              marginBottom: "16px",
            }}
          >
            <RenderTitle
              title="Continuity of Care Record (CCR)"
              subTitle="(Pop ups need to be enabled to see these reports)"
            />

            <FormControlLabel
              checked={isCheckCCR}
              control={<Checkbox size="small" />}
              label={
                <Typography sx={{ fontSize: "14px" }}>
                  Use Date Range
                </Typography>
              }
              onChange={() => setIsCheckCCR(!isCheckCCR)}
            />

            {isCheckCCR && <ChooseDateCCR />}

            <Box sx={{ marginTop: "20px", gap: 1, display: "flex" }}>
              <GenerateReport />
              <Download />
            </Box>
          </Box>

          <Divider />

          <Box
            sx={{
              background: "#FAFAFA",
              padding: "16px",
              borderRadius: "8px",
              margin: "16px 0",
            }}
          >
            <RenderTitle
              title="Continuity of Care Document (CCD)"
              subTitle="(Pop ups need to be enabled to see these reports)"
            />
            <Box sx={{ marginTop: "20px", gap: 1, display: "flex" }}>
              <GenerateReport />
              <GenerateNewReport />
              <Download />
            </Box>
          </Box>

          <Divider />

          <Box
            sx={{
              background: "#FAFAFA",
              padding: "16px",
              borderRadius: "8px",
              marginTop: "16px",
            }}
          >
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
                marginBottom: "16px",
              }}
            >
              <RenderTitle title="Patient Report" />
              <Box sx={{ display: "flex", gap: 1 }}>
                <Button
                  variant="text"
                  sx={{ display: "flex", borderRadius: "8px" }}
                  onClick={handleCheckAll}
                >
                  <DoneAllIcon
                    sx={{ width: "18px", heigth: "18px", marginRight: "5px" }}
                  />
                  <Typography sx={{ fontSize: "14px" }}>Check All</Typography>
                </Button>

                <Button
                  variant="text"
                  sx={{ display: "flex", borderRadius: "8px" }}
                  onClick={handleClearAll}
                >
                  <SettingsBackupRestoreIcon
                    sx={{ width: "18px", heigth: "18px", marginRight: "5px" }}
                  />
                  <Typography sx={{ fontSize: "14px" }}>Clear All</Typography>
                </Button>
              </Box>
            </Box>

            <Box sx={{ display: "flex" }}>
              <FormControl component="fieldset" variant="standard">
                <FormGroup row>
                  {CheckArray.map((item, index) => (
                    <FormControlLabel
                      key={index}
                      control={
                        <Checkbox
                          name={item.name}
                          checked={item.check}
                          onChange={handleCheckList}
                          size="small"
                        />
                      }
                      label={
                        <Typography sx={{ fontSize: "14px" }}>
                          {item.label}
                        </Typography>
                      }
                    />
                  ))}
                </FormGroup>
              </FormControl>
            </Box>

            <Box sx={{ marginTop: "20px", gap: 1, display: "flex" }}>
              <Button
                variant="contained"
                sx={{ display: "flex", borderRadius: "8px" }}
                onClick={handleViewPDF}
              >
                <CheckIcon
                  sx={{ width: "18px", heigth: "18px", marginRight: "5px" }}
                />
                <Typography sx={{ fontSize: "14px" }}>
                  Generate Report
                </Typography>
              </Button>

              <Button
                variant="contained"
                sx={{ display: "flex", borderRadius: "8px" }}
              >
                <DownloadIcon
                  sx={{ width: "18px", heigth: "18px", marginRight: "5px" }}
                />
                <Typography sx={{ fontSize: "14px" }}>Download PDF</Typography>
              </Button>
            </Box>
          </Box>

          <DetailDialog open={open} handleClose={handleClose} />
        </Box>
      )}
    </div>
  );
};
