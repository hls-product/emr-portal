import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import { IconButton } from "@mui/material";
import { styled } from "@mui/system";
import React from "react";
import { Link, useSearchParams } from "react-router-dom";
import Loading from "../../../components/Loading";
import { diagnosticReportsService } from "../../../services/agent";

interface TableProps {
  category: string;
  categoryCode: string;
  categoryShort: string;
  note: string;
  subject: string;
  issued: string;
  status: any;
  performer: string;
  id: string;
  conclusion: string;
}

const Table = styled("table")({
  background: "white",
  flex: 1,
  padding: "8px 16px",
  borderRadius: "16px",
  "& tr": {
    lineHeight: "22px",
  },
  "& th": {
    padding: "8px 0",
    textAlign: "left",
    lineHeight: "22px",
  },
  "& td": {
    padding: "8px 0",
    textAlign: "right",
  },
});

const DetailTable: React.FC<TableProps> = ({ ...props }) => (
  <div
    style={{
      display: "flex",
      justifyContent: "space-between",
      gap: "20px",
    }}
  >
    <Table>
      <tr>
        <th>Category</th>
        <td>{props.category}</td>
      </tr>
      <tr>
        <th>Category Code</th>
        <td>{props.categoryCode}</td>
      </tr>
      <tr>
        <th>Category Short Name</th>
        <td>{props.categoryShort}</td>
      </tr>
      <tr>
        <th>Note</th>
        <td>{props.note}</td>
      </tr>
      <tr>
        <th>Subject</th>
        <td>{props.subject}</td>
      </tr>
    </Table>
    <Table>
      <tr>
        <th>Issued</th>
        <td>{props.issued}</td>
      </tr>
      <tr>
        <th>Status</th>
        <td>
          <div
            style={{
              padding: "3px 12px",
              border: "1px solid rgba(255, 115, 0, 1)",
              color: "rgba(255, 115, 0, 1)",
              borderRadius: "21px",
              width: "fit-content",
              float: "right",
            }}
          >
            {props.status}
          </div>
        </td>
      </tr>
      <tr>
        <th>Performer</th>
        <td>{props.performer}</td>
      </tr>
      <tr>
        <th>Conclusion</th>
        <td>{props.conclusion}</td>
      </tr>
      <tr>
        <th>ID</th>
        <td>{props.id}</td>
      </tr>
    </Table>
  </div>
);

export const DiagnosticReportsDetail: React.FC = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const id = searchParams.get("id");
  const [loading, setLoading] = React.useState<boolean>(true);
  const [data, setData] = React.useState<TableProps>({
    category: "",
    categoryCode: "",
    categoryShort: "",
    note: "",
    subject: "",
    issued: "",
    status: "",
    performer: "",
    id: "",
    conclusion: "",
  });

  React.useEffect(() => {
    if (!id) throw history.back();
    diagnosticReportsService
      .getById(id)
      .then((res) => {
        setData(() => {
          const newData = {
            category: res.category
              ? res.category[0].coding
                ? (res.category[0].coding[0].display as string)
                : ""
              : "",
            categoryCode: res.category
              ? res.category[0].coding
                ? (res.category[0].coding[0].code as string)
                : ""
              : "",
            categoryShort: res.category
              ? res.category[0].coding
                ? (res.category[0].coding[1].code as string)
                : ""
              : "",
            status: res.status as string,
            note: res.code
              ? res.code.coding
                ? (res.code.coding[0].display as string)
                : ""
              : "",
            subject: res.subject ? (res.subject.display as string) : "",
            issued: res.issued ? (res.issued.slice(0, 10) as string) : "",
            performer: res.performer
              ? (res.performer[0].display as string)
              : "",
            conclusion: res.conclusion as string,
            id: res.id as string,
          } as TableProps;
          return newData;
        });
        setLoading(false);
      })
      .catch(() => history.back());
  }, [id]);

  return (
    <div>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <div
          style={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <IconButton
            style={{
              color: "rgba(108, 93, 211, 1)",
              background: "white",
              borderRadius: "50%",
              width: "40px",
              height: "40px",
              marginRight: "16px",
            }}
            onClick={() => history.back()}
          >
            <ArrowBackIcon />
          </IconButton>
          <h1 style={{ fontSize: "24px" }}>Diagnostic Report Details</h1>
        </div>
        <Link
          to={`update?id=${data.id}`}
          style={{
            padding: "10px 30px",
            borderRadius: "30px",
            color: "rgba(108, 93, 211, 1)",
            background: "white",
            textDecoration: "none",
          }}
        >
          <EditOutlinedIcon style={{ marginRight: "12px" }} />
          Edit
        </Link>
      </div>
      {loading && <Loading></Loading>}
      {!loading && <DetailTable {...data} />}
    </div>
  );
};
