import { Add, Create, Delete, Search, Visibility } from "@mui/icons-material";
import {
  Box,
  IconButton,
  InputAdornment,
  TextField,
  Typography,
} from "@mui/material";
import React from "react";
import { Link, useNavigate } from "react-router-dom";
import { DeleteDialog } from "../../../components/DeleteDialog";
import { RoundedButton } from "../../../components/RoundedButton";
import { AdvancedSearch } from "../../../components/Table/AdvancedSearch";
import { CustomTable } from "../../../components/Table/Table";
import { HeadCell } from "../../../components/Table/types";
import { diagnosticReportsModel } from "../../../models/diagnosticReports";
import { diagnosticReportsService } from "../../../services/agent";

const headCells: HeadCell[] = [
  {
    id: "number",
    numeric: false,
    label: "No.",
  },
  {
    id: "category",
    numeric: false,
    label: "Category",
  },
  {
    id: "subject",
    numeric: false,
    label: "Subject",
  },
  {
    id: "performer",
    numeric: false,
    label: "Performer",
  },
  {
    id: "note",
    numeric: false,
    label: "Note",
  },
  {
    id: "conclusion",
    numeric: false,
    label: "Conclusion",
  },
  {
    id: "status",
    numeric: false,
    label: "Status",
  },
  {
    id: "action",
    numeric: false,
    label: "Action",
  },
];
const advancedSearchProps = [
  {
    name: "status",
    label: "Status",
    select: true,
    menuItems: [
      {
        value: "registered",
        display: "Registered",
      },
      {
        value: "partial",
        display: "Partial",
      },
      {
        value: "preliminary",
        display: "Preliminary",
      },
      {
        value: "final",
        display: "Final",
      },
    ],
  },
  {
    name: "category",
    label: "Category",
  },
  {
    name: "performer",
    label: "Performer",
  },
];

export const DiagnosticReports: React.FC = () => {
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = React.useState(false);
  const [data, setData] = React.useState<diagnosticReportsModel[]>([]);
  const [page, setPage] = React.useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = React.useState<number>(10);
  const [rowsCount, setRowsCount] = React.useState<number>(0);
  const [deletePopUpOpen, setDeletePopUpOpen] = React.useState(false);
  const [openAdvancedSearch, setOpenAdvancedSearch] = React.useState(false);
  const [currentSelectedId, setCurrentSelectedId] = React.useState("");

  // Search states
  const [searchValue, setSearchValue] = React.useState<string>("");
  const [debounceSeachValue, setDebounceSearchValue] =
    React.useState<string>("");
  const [filter, setFilter] = React.useState<{ [key: string]: string }>({});

  const getDiagnosticReportList = async (filter?: {
    [keys: string]: string;
  }) => {
    const params = {
      ...filter,
      page: page + 1,
      size: rowsPerPage,
      name: debounceSeachValue,
    };
    setIsLoading(true);
    try {
      let { data, paging } = await diagnosticReportsService.getList({ params });
      setData(data);
      setRowsCount(paging.total);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      console.log("getDiagnosticReportList error: ", error);
    }
  };
  const deleteDiagnosticReport = async (id: string) => {
    setIsLoading(true);
    try {
      let response = await diagnosticReportsService.delete(id);
      setIsLoading(false);
      return response;
    } catch (error) {
      setIsLoading(false);
      console.log("deletePatient error: ", error);
    }
  };

  // Event Handler
  const handleChangePage = (event: any, newPage: number) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event: any) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleDelete = (id: string) => {
    setDeletePopUpOpen(true);
    setCurrentSelectedId(id);
  };
  const handleCancelDelete = () => {
    setDeletePopUpOpen(false);
  };
  const handleConfirmDelete = () => {
    setDeletePopUpOpen(false);
    deleteDiagnosticReport(currentSelectedId).then((res) => {
      setCurrentSelectedId("");
      getDiagnosticReportList();
    });
  };
  const handleClickAdvancedSearchOpen = () => {
    setOpenAdvancedSearch(true);
  };
  //Debounce for name search
  React.useEffect(() => {
    let timeout = setTimeout(() => {
      setDebounceSearchValue(searchValue);
    }, 500);
    return () => {
      clearTimeout(timeout);
    };
  }, [searchValue]);

  // Back to previous page if current page is empty after deleting an item
  React.useEffect(() => {
    let totalPages = Math.ceil(rowsCount / rowsPerPage);
    let currentPage = page + 1;
    if (currentPage > totalPages && totalPages > 0) {
      setPage(totalPages - 1);
    }
  }, [rowsCount]);
  React.useEffect(() => {
    getDiagnosticReportList();
  }, [debounceSeachValue, rowsPerPage, page]);

  const mapDataToRender = () => {
    const render = data.map((item, index) => ({
      id: item.id as string,
      number: index + 1,
      category: item.category?.[0].coding?.[0].display,
      subject: item.subject?.display,
      performer: item.performer?.[0].display,
      note: item.code?.coding?.[0]?.display,
      conclusion: item.conclusion,
      status: <Typography textTransform="capitalize" children={item.status} />,
      action: (
        <Box style={{ textAlign: "center", whiteSpace: "nowrap" }}>
          <Link
            to={`diagnosticReports/update?id=${item.id}`}
            children={<IconButton children={<Create />} />}
          />
          <Link
            to={`diagnosticReports?id=${item.id}`}
            children={<IconButton children={<Visibility />} />}
          />

          <Link
            to={`#`}
            onClick={() => {
              handleDelete(item.id as string);
            }}
            children={<IconButton children={<Delete />} />}
          />
        </Box>
      ),
    }));
    return render;
  };

  return (
    <>
      {/* Dialog for delete item popup */}
      <DeleteDialog
        open={deletePopUpOpen}
        onClose={handleCancelDelete}
        onConfirm={handleConfirmDelete}
        onCancel={handleCancelDelete}
      />
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          width: "100%",
          padding: "20px",
          backgroundColor: "white",
          borderRadius: "16px",
        }}
      >
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            gap: "8px",
          }}
        >
          <Typography variant="h2" children={"DIAGNOSTIC REPORTS"} />
          <Box sx={{ display: "flex", gap: "8px" }}>
            <TextField
              placeholder="Search report..."
              InputProps={{
                startAdornment: (
                  <InputAdornment
                    position="start"
                    children={<Search sx={{ height: 20 }} />}
                  />
                ),
              }}
              sx={{
                "& .MuiInputBase-root": {
                  height: 48,
                  width: 331,
                  borderRadius: 3,
                },
              }}
              value={searchValue}
              onChange={(e) => {
                setSearchValue(e.target.value);
              }}
            />
            <RoundedButton
              variant="contained"
              onClick={handleClickAdvancedSearchOpen}
              children="Advanced Search"
            />
            <RoundedButton
              variant="contained"
              endIcon={<Add />}
              onClick={() => {
                navigate("diagnosticReports/add");
              }}
              children="Add New Plan"
            />
          </Box>
        </Box>
        {openAdvancedSearch && (
          <AdvancedSearch
            props={advancedSearchProps}
            filter={filter}
            description="Find diagnostic report with..."
            onFilterChange={setFilter}
            onApply={getDiagnosticReportList}
            onClose={setOpenAdvancedSearch}
          />
        )}
        <CustomTable
          title={"DIAGNOSTIC REPORTS"}
          headcells={headCells}
          body={mapDataToRender()}
          paging={{
            page: page,
            rowsPerPage: rowsPerPage,
            rowsCount: rowsCount,
          }}
          loading={isLoading}
          toolbar={false}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Box>
    </>
  );
};
