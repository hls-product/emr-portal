import React from "react";
import * as _ from "lodash";
import { Box, TextField, styled, Typography, Button } from "@mui/material";
import { useSelector } from "react-redux";
import { IRootState } from "../../../store/reducers";
import { useAppDispatch } from "../../../store/Store";
import { useNavigate, useSearchParams } from "react-router-dom";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import AddIcon from "@mui/icons-material/Add";
import Loading from "../../../components/Loading";
import { diagnosticReportsModel } from "../../../models/diagnosticReports";
import {
  getDiagnosticReportsDataByID,
  postDiagnosticReportsData,
  updateDiagnosticReportsData,
} from "../../../store/reducers/diagnosticReports/diagnosticReports";
import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { StyledArrowBack } from "../style";
import { diagnosticReportsService } from "../../../services/agent";

export const DiagnosticReportsEdit: React.FC = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const navigate = useNavigate();
  const id = searchParams.get("id");
  const [loading, setLoading] = React.useState<boolean>(id? true : false);
  
  const [startPeriod, setStartPeriod] = React.useState<string>(
    new Date().toISOString().replace(/.\d+Z$/g, "Z")
  );
  const [endPeriod, setEndPeriod] = React.useState<string>(
    new Date().toISOString().replace(/.\d+Z$/g, "Z")
  );
  const [formValue, setFormValue] = React.useState<{ [key: string]: string }>({
    startPeriod: startPeriod,
    endPeriod: endPeriod,
  });

  //declare function
  const handleStartChange = (newValue: Date | null) => {
    setStartPeriod(newValue?.toISOString().replace(/.\d+Z$/g, "Z") as string);
    setFormValue({
      ...formValue,
      startPeriod: startPeriod as unknown as string,
    });
  };
  const handleEndChange = (newValue: Date | null) => {
    setEndPeriod(newValue?.toISOString().replace(/.\d+Z$/g, "Z") as string);
    setFormValue({
      ...formValue,
      endPeriod: endPeriod as unknown as string,
    });
  };
  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setFormValue({
      ...formValue,
      [event.currentTarget.name]: event.currentTarget.value,
    });
    console.log(formValue);
  };
  const handleSelectChange = (event: SelectChangeEvent) => {
    setFormValue({ ...formValue, [event.target.name]: event.target.value });
  };
  const handleSubmit = () => {
    //mapping value to submit
    const submitValue = {
      status: formValue?.status,
      category: [
        {
          coding: [
            {
              system: "http://snomed.info/sct",
              code: formValue?.categoryCode,
              display: formValue?.category,
            },
            {
              system: "http://terminology.hl7.org/CodeSystem/v2-0074",
              display: formValue?.categoryShort,
            },
          ],
        },
      ],
      code: {
        coding: [
          {
            system: "http://loinc.org",
            code: "58410-2",
            display: formValue?.code,
          },
        ],
      },
      subject: {
        reference: "Patient/f001",
        display: formValue?.subject,
      },
      issued: startPeriod,
      performer: [
        {
          reference: "Organization/f001",
          display: formValue?.performer,
        },
      ],
      conclusion: formValue?.conclusion,
    } as diagnosticReportsModel;

    if (formValue?.category && formValue?.subject && formValue?.performer) {
      setLoading(true);
      if (id)
      diagnosticReportsService
        .update({ body: submitValue }, id)
        .then(() => navigate(`/admin/patient/details?id=${id}`, { state: 7 }))
        .catch(() => setLoading(false));
    else
      diagnosticReportsService
        .post({ body: submitValue })
        .then(() => navigate(`/admin/patient/details?id=${id}`, { state: 7 }))
        .catch(() => setLoading(false));
    }
  };

  //styling
  const style = {
    "& .MuiOutlinedInput-root": {
      height: "100%",
      "& fieldset": {
        border: "1px solid #EAEAEA",
        borderRadius: "0.8rem",
        padding: 0,
        margin: 0,
      },
      "&:hover fieldset": {
        border: "1px solid #dbd7d7",
      },
      "&.Mui-focused fieldset": {
        border: "1px solid #EAEAEA",
      },
    },
  };
  const StyledButton = styled(Button)(({}) => ({
    margin: "0",
    color: "#FFFFFF",
    fontWeight: "700",
    fontSize: "1rem",
    lineHeight: "1.2rem",
    backgroundColor: "#6C5DD3",
    borderRadius: "6rem",
    padding: "10px 16px",
    whiteSpace: "nowrap",
    "&:hover": {
      backgroundColor: "#887ae6",
    },
  }));
  //Field header Mapping
  const diagnosticReportsDetailsFields = [
    {
      name: "category",
      value: formValue?.category,
      label: "Category Name",
      required: true,
    },
    {
      name: "categoryShort",
      value: formValue?.categoryShort,
      label: "Category Short Name",
      required: false,
    },
    {
      name: "categoryCode",
      value: formValue?.categoryCode,
      label: "Category Code",
      required: false,
    },
  ];
  const coverageFields = [
    {
      name: "code",
      value: formValue?.code,
      label: "Note",
      required: false,
    },
    {
      name: "subject",
      value: formValue?.subject,
      label: "Subject",
      required: true,
    },
    {
      name: "performer",
      value: formValue?.performer,
      label: "Performer",
      required: true,
    },
    {
      name: "conclusion",
      value: formValue?.conclusion,
      label: "Conclusion",
      required: false,
    },
  ];
  const Status = [
    { value: "Final", display: "Final" },
    { value: "Not Final", display: "Not Final" },
  ];

  React.useEffect(() => {
    if(id)
    diagnosticReportsService.getById(id).then(res => {
      setFormValue({
        status: "final",
      });
      setStartPeriod(
        res.issued
          ? res.issued
          : new Date().toISOString().replace(/.\d+Z$/g, "Z")
      );
    })
    setLoading(false);
  }, [id]);

  return (
    <Box>
      {/* Header */}
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <Typography sx={{ fontWeight: 700, fontSize: 24, display: "flex", alignItems: "center", gap:"16px" }}>
          <StyledArrowBack onClick={() =>  history.back()}/>
          ADD DIAGNOSTIC REPORT
        </Typography>
        <StyledButton onClick={handleSubmit}>
          {searchParams.get("id") ? (
            <>Save changes</>
          ) : (
            <>
              {" "}
              Add <AddIcon style={{ marginLeft: "10px" }} />
            </>
          )}
        </StyledButton>
      </Box>
      {/* Body */}
      {loading && <Loading />}
      {!loading && (
        <Box
          style={{
            display: "flex",
            justifyContent: "space-between",
            background: "white",
            borderRadius: "16px",
            margin: "16px 0",
            padding: "16px",
          }}
        >
          {/* Left Col */}
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              gap: "16px",
              width: "49%",
            }}
          >
            <Typography sx={{ fontWeight: 600, fontSize: 20 }}>
              General Information
            </Typography>
            {diagnosticReportsDetailsFields.map((item) => (
              <>
                <TextField
                  name={item.name}
                  value={item.value}
                  onChange={handleInputChange}
                  label={item.label}
                  required={item.required}
                  error={item.required ? !item.value : false}
                  sx={style}
                />
              </>
            ))}

            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                "& .MuiFormControl-root": {
                  flexBasis: "100%",
                },
                ...style,
              }}
            >
              <FormControl>
                <InputLabel id="status-select-label">Status</InputLabel>
                <Select
                  labelId="status-select-label"
                  id="status-select"
                  value={formValue?.status}
                  label="Status"
                  name="status"
                  onChange={handleSelectChange}
                  required
                  error={!formValue?.status}
                >
                  {Status.map(({ value, display }) => (
                    <MenuItem value={value}>{display}</MenuItem>
                  ))}
                </Select>
              </FormControl>
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DesktopDatePicker
                  label="Valid From"
                  inputFormat="MM/dd/yyyy"
                  value={new Date(startPeriod)}
                  onChange={handleStartChange}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      required
                      error={startPeriod ? startPeriod >= endPeriod : true}
                    />
                  )}
                />
              </LocalizationProvider>
            </Box>
          </Box>
          {/* Right Col*/}
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              gap: "16px",
              width: "49%",
            }}
          >
            <Typography sx={{ fontWeight: 600, fontSize: 20 }}>
              More Information
            </Typography>
            {coverageFields.map((item) => (
              <TextField
                name={item.name}
                value={item.value}
                onChange={handleInputChange}
                label={item.label}
                required={item.required}
                error={item.required ? !item.value : false}
                sx={style}
              />
            ))}
          </Box>
        </Box>
      )}
    </Box>
  );
};
