/* Libs */
import styled from "styled-components";
import { TablePagination } from "@mui/material";
import Chip from "@mui/material/Chip";
import CloseIcon from "@mui/icons-material/Close";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { YearPicker } from "@mui/x-date-pickers/YearPicker";
import { DesktopDatePicker } from "@mui/x-date-pickers/DesktopDatePicker";

export const StyledDatePicker = styled(DatePicker)`
  width: 100%;
  height: 100%;

  & label {
    color: black !important;
  }

  & fieldset {
    border: 1px solid black !important;
    height: 100%;
    /* margin-top: 0.7rem; */
  }
`;

export const StyledYearLeftPicker = styled(DatePicker)`
  width: 100%;
  height: 100%;

  & label {
    color: black !important;
  }

  & fieldset {
    border: 1px solid black !important;
    height: 100%;
    /* margin-top: 0.7rem; */
  }
`;

export const StyledYearRightPicker = styled(DatePicker)`
  width: 100%;
  height: 100%;
  margin-left: 0.3rem;

  & label {
    color: black !important;
  }

  & fieldset {
    border: 1px solid black !important;
    height: 100%;
    /* margin-top: 0.7rem; */
  }
`;

export const StyledDatePickerBox = styled("div")``;

export const StyledTablePagination = styled(TablePagination)`
  background-color: white;
  border: none;
  margin-bottom: 0;
  margin-top: 0.8rem;
  padding-bottom: 0;
`;

export const StyledChip = styled(Chip)(({}) => ({
  marginTop: "7px",
  marginBottom: "5px",
  backgroundColor: "#4E4E4E",
  color: "white",
  width: "6rem",
  height: "1.5rem",
}));

// Close chip icon
export const StyledCloseChipIcon = styled(CloseIcon)(({}) => ({
  height: "1rem",
  width: "1rem",
  padding: "2px",
  marginLeft: "5px",
  backgroundColor: "#D3D3D3",
  borderRadius: "50%",
}));
