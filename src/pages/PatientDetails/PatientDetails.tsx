import React from "react";
import { Link, useNavigate, useLocation } from "react-router-dom";
import { Box, Button, Toolbar, styled } from "@mui/material";
import {
  StyledArrowBack,
  StyledTopContainer,
  StyledTopLeftContainer,
  StyledComponentContainer,
} from "./style";
import { Details } from "./Information/Details";
import { Appointment } from "./Appointment/Appointment";
import { Immunization } from "./Immunization/MedicalRecord";
import { MedicalRecord } from "./MedicalRecord";
import { Insurance } from "./Insurance/Insurance";
import { Observation } from "./Observation/Observation";
import { RelatedPatient } from "./RelatedPatient/RelatedPatient";
import { DiagnosticReports } from "./DiagnosticReports/DiagnosticReports";
import { Claim } from "./Claim/Claim";
import { PatientRecords } from "./PatientRecords/PatientRecords";
import { StyledHeaderTypography } from "../../components/styledTypography";
import { Encounter } from "./Encounter/Encounter";
import { Condition } from "./Condition/Condition";

type toolbarStatusType = {
  status: number;
  changeStatus: (arg0: number) => void;
};

export const EnhancedActionToolbar = (props: toolbarStatusType) => {
  const tabs: string[] = [
    "Information",
    "Appointment",
    "Medical Record",
    "Immunization",
    "Insurance Plans",
    "Observation",
    "Related Patient",
    "Diagnostic Report",
    "Claim",
    "Encounter",
    "Condition",
    "Patient Records",
  ];

  return (
    <Toolbar
      sx={{
        borderRadius: "8px",
        marginBottom: "16px",
        padding: "12px",
        marginTop: "24px",
        background: "#FFFFFF",
      }}
    >
      <Box sx={{ flexGrow: 1, alignItems: "center" }}>
        {tabs.map((item, index) => (
          <Button
            variant={`${props.status === index ? "contained" : "text"}`}
            onClick={() => {
              props.changeStatus(index);
            }}
            key={index}
            sx={{
              padding: "9px 16px",
              borderRadius: "10px",
              mr: 0.5,
              pointerEvents: props.status === index ? "none" : "auto",
              background: props.status === index ? "auto" : "none",
              color: props.status === index ? "" : "#4E4E4E",
            }}
          >
            {item}
          </Button>
        ))}
      </Box>
    </Toolbar>
  );
};

export const PatientDetails: React.FC = () => {
  const navigate = useNavigate();
  const location = useLocation();

  const [selectedTab, setSelectedTab] = React.useState<number>(
    location.state ? (location.state as number) : 0
  );

  const StyledButton = styled(Button)(({}) => ({
    margin: "0",
    color: "#FFFFFF",
    fontWeight: "700",
    fontSize: "1rem",
    lineHeight: "1.2rem",
    backgroundColor: "#6C5DD3",
    borderRadius: "6rem",
    padding: "10px 16px",
    whiteSpace: "nowrap",
    "&:hover": {
      backgroundColor: "#887ae6",
    },
  }));
  return (
    <StyledComponentContainer>
      <StyledTopContainer>
        <StyledTopLeftContainer>
          <Link
            to="/admin/patient"
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <StyledArrowBack />
          </Link>
          <StyledHeaderTypography
            style={{
              marginBottom: "0",
              whiteSpace: "nowrap"
            }}
          >
            Patient Detail
          </StyledHeaderTypography>
        </StyledTopLeftContainer>
      </StyledTopContainer>
      <EnhancedActionToolbar
        status={selectedTab}
        changeStatus={setSelectedTab}
      />
      {selectedTab === 0 && <Details />}
      {selectedTab === 1 && <Appointment />}
      {selectedTab === 2 && <MedicalRecord />}
      {selectedTab === 3 && <Immunization />}
      {selectedTab === 4 && <Insurance />}
      {selectedTab === 5 && <Observation />}
      {selectedTab === 6 && <RelatedPatient />}
      {selectedTab === 7 && <DiagnosticReports />}
      {selectedTab === 8 && <Claim />}
      {selectedTab === 9 && <Encounter />}
      {selectedTab === 10 && <Condition />}
      {selectedTab === 11 && <PatientRecords />}
    </StyledComponentContainer>
  );
};
