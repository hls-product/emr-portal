import React, { useState, useEffect, useCallback } from "react";
import { Routes } from "react-router-dom";
import { Grid, Box, Typography } from "@mui/material";
import { Header } from "../layout/Header";
import { Content } from "../layout/Content";
import { getRoutes, routes } from "../PortalRoutes";
import { Sidebar } from "../layout/Sidebar";
import { DrawerWrapper } from "./SupportCenter/Drawer";
import { userActiveService } from "../services/agent";
import { useSelector } from "react-redux";
import { IRootState } from "../store/reducers";

export const AdminPortal: React.FC = () => {
  const { openStatus } = useSelector(
    (state: IRootState) => state.sidebarReducer
  );
  const [openSupport, setOpenSupport] = React.useState<boolean>(false);

  const handleInteractSupport = () => {
    setOpenSupport(!openSupport);
  };

  useEffect(() => {
    userActiveService.setUserInDb();
  }, []);

  return (
    <Box sx={{ display: "flex", flexDirection: "column", minHeight: "100vh" }}>
      <Header
        openStatus={openStatus}
        handleInteractSupport={handleInteractSupport}
      />
      <Sidebar openStatus={openStatus} />
      <Content openStatus={openStatus}>
        <Routes>{getRoutes(routes)}</Routes>
      </Content>
      <DrawerWrapper
        openSupportStatus={openSupport}
        handleInteractSupport={handleInteractSupport}
      />
      {/* <MobileNavBar /> */}
    </Box>
  );
};
