import {styled as muiStyled, TextField, InputBase} from '@mui/material'

export const StyledTextField = muiStyled(TextField)(({}) => ({
    width: '100%',
    marginTop: '2px',
    '& .MuiInputBase-root': {
      borderRadius: '12px',
    },
    '& .MuiInputLabel-asterisk': {
      color: '#E65050',
    },
    '& .MuiInputLabel-root': {
      fontWeight: '500',
      fontSize: '16px',
      color: '#131626',
    },
    '& .MuiOutlinedInput-notchedOutline': {
      border: '1px solid #EAEAEA',
    },
    '& .MuiOutlinedInput-input': {
      padding: '9.5px 14px',
    },
}));

export const StyledInput = muiStyled(InputBase)(({}) => ({
  fontSize: '16px',
  height: '26px',
  '&::placeholder': {
    color: '#9FA0A6',
  },
  '& .css-ngazp5-MuiInputBase-input': {
    padding: '9px 0px 4px',
  },
}));