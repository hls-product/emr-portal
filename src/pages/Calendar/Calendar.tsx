import React from "react";
import {
  Typography,
  Button,
  Dialog,
  Box,
  Toolbar,
  SelectChangeEvent,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from "@mui/material";
import {
  ViewState,
  EditingState,
  IntegratedEditing,
} from "@devexpress/dx-react-scheduler";
import {
  Scheduler,
  WeekView,
  MonthView,
  Appointments,
  DayView,
  AppointmentTooltip,
  AppointmentForm,
  Resources,
} from "@devexpress/dx-react-scheduler-material-ui";
import { StyledInput, StyledTextField } from "./style";

import link_icon from "../../assets/icon/link_icon.svg";
import secondarySearchIcon from "../../assets/adminHeader/secondarySearchIcon.svg";
import arrow_down from "../../assets/icon/arrow_down.svg";
import bin_icon from "../../assets/icon/bin_icon.svg";
import doctor from "../../assets/image/doctor.svg";
import { StyledHeaderTypography } from "../../components/styledTypography";
interface DialogProps {
  open: boolean;
  onClose: () => void;
  department: string;
  handleSetDepartment: (event: SelectChangeEvent) => void;
  time: string;
  handleSetTime: (event: SelectChangeEvent) => void;
  status: string;
  handleSetStatus: (event: SelectChangeEvent) => void;
  priority: string;
  handleSetPriority: (event: SelectChangeEvent) => void;
}

interface PriorityType {
  id: number;
  text: string;
  color: string;
}

interface toolbarProps {
  openDialog: () => void;
  setCalendarView: (value: number) => void;
  selectedView: number;
}

interface ActivityType {
  label: string;
  color: string;
}

interface AssignCardType {
  id: number;
  icon: string;
  name: string;
  degree: string;
}

interface CalendarViewProps {
  setCalendarView: (value: number) => void;
  selectedView: number;
}

interface AppointmentType {
  id: number;
  title: string;
  owner: string;
  startDate: Date | string | number;
  endDate: Date | string | number;
  doctor: string;
  priority: 0 | 1 | 2 | 3;
}

interface resourceType {
  fieldName: string;
  title: string;
  instances: PriorityType[];
}

const resources: resourceType[] = [
  {
    fieldName: "priority",
    title: "Priority",
    instances: [
      { id: 0, text: "Urgent", color: "#E03535" },
      { id: 1, text: "Urgent", color: "#FF7300" },
      { id: 2, text: "Medium", color: "#FFDD00" },
      { id: 3, text: "Normal", color: "#1685DA" },
    ],
  },
];

const CalendarData: AppointmentType[] = [
  {
    id: 0,
    title: "Meeting",
    owner: "Thor",
    doctor: "Dr. Strange",
    priority: 0,
    startDate: "2022-08-11T07:00",
    endDate: "2022-08-11T09:30",
  },
  {
    id: 1,
    title: "Meeting",
    owner: "Scarlet Witch",
    doctor: "Dr. Strange",
    priority: 1,
    startDate: "2022-08-11T10:00",
    endDate: "2022-08-11T16:30",
  },
  {
    id: 2,
    title: "Meeting",
    owner: "Thor",
    doctor: "Dr. Strange",
    priority: 2,
    startDate: "2022-08-08T06:00",
    endDate: "2022-08-08T08:00",
  },
  {
    id: 3,
    title: "Meeting",
    owner: "Scarlet Witch",
    doctor: "Dr. Strange",
    priority: 3,
    startDate: "2022-08-08T16:00",
    endDate: "2022-08-08T18:30",
  },
  {
    id: 4,
    title: "Meeting",
    owner: "Thor",
    doctor: "Dr. Strange",
    priority: 2,
    startDate: "2022-08-09T06:30",
    endDate: "2022-08-09T12:30",
  },
  {
    id: 5,
    title: "Meeting",
    owner: "Scarlet Witch",
    doctor: "Dr. Strange",
    priority: 3,
    startDate: "2022-08-10T12:00",
    endDate: "2022-08-10T15:30",
  },
  {
    id: 6,
    title: "Meeting",
    owner: "Scarlet Witch",
    doctor: "Dr. Strange",
    priority: 0,
    startDate: "2022-08-12T08:30",
    endDate: "2022-08-12T11:00",
  },
];

const PriorityList: PriorityType[] = [
  {
    id: 0,
    text: "Urgent",
    color: "#E03535",
  },
  {
    id: 1,
    text: "Urgent",
    color: "#FF7300",
  },
  {
    id: 2,
    text: "Medium",
    color: "#FFDD00",
  },
  {
    id: 3,
    text: "Normal",
    color: "#1685DA",
  },
];

const Departments: string[] = [
  "Service customer",
  "Service User",
  "Service Provider",
  "Service Consumer",
];
const Times: string[] = [
  "8:00 AM",
  "10:00 AM",
  "11:00 AM",
  "12:00 PM",
  "2:00 PM",
  "3:00 PM",
  "5:00 PM",
];
const Status: ActivityType[] = [
  {
    label: "New",
    color: "#1685DA",
  },
  {
    label: "Old",
    color: "#FAFAFA",
  },
];
const Priority: ActivityType[] = [
  { label: "Urgent (High)", color: "#E03535" },
  { label: "Urgent (Low)", color: "#FF7300" },
  { label: "Medium", color: "#FFDD00" },
  { label: "Normal", color: "#1685DA" },
];

const AssignList: AssignCardType[] = [
  {
    id: 0,
    icon: doctor,
    name: "Dr. Star Lord",
    degree: "Allergy",
  },
  {
    id: 1,
    icon: doctor,
    name: "Dr. Strange",
    degree: "Allergy",
  },
  {
    id: 1,
    icon: doctor,
    name: "Dr. Sleep",
    degree: "Allergy",
  },
];

const AddActivityDialog = (Props: DialogProps) => {
  const {
    onClose,
    open,
    department,
    handleSetDepartment,
    handleSetPriority,
    handleSetStatus,
    handleSetTime,
    priority,
    status,
    time,
  } = Props;
  return (
    <Dialog open={open} onClose={onClose} maxWidth="md">
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "column",
          padding: "24px 20px",
          width: "640px",
          height: "fit-content",
          gap: "26px",
        }}
      >
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            width: "100%",
          }}
        >
          <Typography variant="h6" component="div">
            Add Activity
          </Typography>
          <Button onClick={onClose}>Cancel</Button>
        </div>

        <StyledTextField
          placeholder="Enter Title here..."
          variant="outlined"
          InputLabelProps={{ shrink: true }}
          label="Title"
        />

        <div
          style={{
            display: "flex",
            width: "100%",
            border: "1px solid #EAEAEA",
            borderRadius: "12px",
          }}
        >
          <img
            src={secondarySearchIcon}
            alt="Search"
            width={20}
            height={20}
            style={{ margin: "14px 0 14px 14px" }}
          />
          <StyledInput
            sx={{ m: 1, width: "fit-content" }}
            placeholder="Search..."
          />
        </div>

        <div
          style={{
            display: "flex",
            gap: "8px",
            width: "100%",
            flexDirection: "column",
          }}
        >
          {AssignList.map((item) => (
            <div
              key={item.id}
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
                background: "#FAFAFA",
                borderRadius: "8px",
                padding: "8px 16px 8px 8px",
                width: "100%",
              }}
            >
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "flex-start",
                  gap: "16px",
                }}
              >
                <img src={doctor} width={42} height={42} />
                <div
                  style={{
                    display: "flex",
                    alignItems: "flex-start",
                    justifyContent: "flex-start",
                    flexDirection: "column",
                  }}
                >
                  <Typography variant="body2" component="div">
                    {item.name}
                  </Typography>
                  <Typography variant="body2" component="div">
                    {item.degree}
                  </Typography>
                </div>
              </div>
              <img src={bin_icon} width={11} height={13} />
            </div>
          ))}
        </div>

        <FormControl sx={{ width: "100%" }}>
          <InputLabel id="demo-simple-select-label">Department</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            label="Department"
            value={department}
            onChange={handleSetDepartment}
            inputProps={{ "aria-label": "Without label" }}
            IconComponent={() => (
              <img src={arrow_down} style={{ marginRight: "14px" }} />
            )}
            sx={{
              "& .css-1d3z3hw-MuiOutlinedInput-notchedOutline": {
                border: "1px solid #EAEAEA",
                borderRadius: "12px",
              },
              "& .MuiSelect-select": {
                padding: "11px 16px",
              },
            }}
          >
            {Departments.map((item, index) => (
              <MenuItem key={index} value={item}>
                {item}
              </MenuItem>
            ))}
          </Select>
        </FormControl>

        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            gap: "16px",
            width: "100%",
          }}
        >
          <FormControl sx={{ width: "100%" }}>
            <InputLabel id="demo-simple-select-label">Department</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              label="Department"
              value={department}
              onChange={handleSetDepartment}
              inputProps={{ "aria-label": "Without label" }}
              IconComponent={() => (
                <img src={arrow_down} style={{ marginRight: "14px" }} />
              )}
              sx={{
                "& .css-1d3z3hw-MuiOutlinedInput-notchedOutline": {
                  border: "1px solid #EAEAEA",
                  borderRadius: "12px",
                },
                "& .MuiSelect-select": {
                  padding: "11px 16px",
                },
              }}
            >
              {Departments.map((item, index) => (
                <MenuItem key={index} value={item}>
                  {item}
                </MenuItem>
              ))}
            </Select>
          </FormControl>

          <FormControl sx={{ width: "100%" }}>
            <InputLabel id="demo-simple-select-label">Time</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              label="Time"
              value={time}
              onChange={handleSetTime}
              inputProps={{ "aria-label": "Without label" }}
              IconComponent={() => (
                <img src={arrow_down} style={{ marginRight: "14px" }} />
              )}
              sx={{
                "& .css-1d3z3hw-MuiOutlinedInput-notchedOutline": {
                  border: "1px solid #EAEAEA",
                  borderRadius: "12px",
                },
                "& .MuiSelect-select": {
                  padding: "11px 16px",
                },
              }}
            >
              {Times.map((item, index) => (
                <MenuItem key={index} value={item}>
                  {item}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </div>

        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            gap: "16px",
            width: "100%",
          }}
        >
          <FormControl sx={{ width: "100%" }}>
            <InputLabel id="demo-simple-select-label">Status</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              label="Status"
              value={status}
              onChange={handleSetStatus}
              inputProps={{ "aria-label": "Without label" }}
              IconComponent={() => (
                <img src={arrow_down} style={{ marginRight: "14px" }} />
              )}
              sx={{
                "& .css-1d3z3hw-MuiOutlinedInput-notchedOutline": {
                  border: "1px solid #EAEAEA",
                  borderRadius: "12px",
                },
                "& .MuiSelect-select": {
                  padding: "11px 16px",
                },
              }}
            >
              {Status.map((item, index) => (
                <MenuItem key={index} value={item.label}>
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "flex-start",
                    }}
                  >
                    <div
                      style={{
                        width: "12px",
                        height: "12px",
                        borderRadius: "50%",
                        background: `${item.color}`,
                        marginRight: "10px",
                      }}
                    ></div>
                    {item.label}
                  </div>
                </MenuItem>
              ))}
            </Select>
          </FormControl>

          <FormControl sx={{ width: "100%" }}>
            <InputLabel id="demo-simple-select-label">Priority</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              label="Priority"
              value={priority}
              onChange={handleSetPriority}
              inputProps={{ "aria-label": "Without label" }}
              IconComponent={() => (
                <img src={arrow_down} style={{ marginRight: "14px" }} />
              )}
              sx={{
                "& .css-1d3z3hw-MuiOutlinedInput-notchedOutline": {
                  border: "1px solid #EAEAEA",
                  borderRadius: "12px",
                },
                "& .MuiSelect-select": {
                  padding: "11px 16px",
                },
              }}
            >
              {Priority.map((item, index) => (
                <MenuItem key={index} value={item.label}>
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "flex-start",
                    }}
                  >
                    <div
                      style={{
                        width: "12px",
                        height: "12px",
                        borderRadius: "50%",
                        background: `${item.color}`,
                        marginRight: "10px",
                      }}
                    ></div>
                    {item.label}
                  </div>
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </div>

        <StyledTextField
          placeholder="Enter..."
          variant="outlined"
          InputLabelProps={{ shrink: true }}
          label="Note"
          multiline
        />
        <Button
          variant="contained"
          sx={{
            width: "100%",
            borderRadius: "99px",
            boxShadow: "0px 5px 10px rgba(74, 92, 208, 0.2)",
            height: "50px",
          }}
        >
          Save
        </Button>
      </Box>
    </Dialog>
  );
};

const RenderTimeButton = (Props: CalendarViewProps) => {
  const { selectedView, setCalendarView } = Props;
  return (
    <div
      style={{
        display: "flex",
        gap: "10px",
      }}
    >
      <Button
        variant={`${selectedView === 0 ? "contained" : "text"}`}
        onClick={() => {
          setCalendarView(0);
        }}
        sx={{ borderRadius: "10px", width: "57px", height: "40px" }}
      >
        Day
      </Button>
      <Button
        variant={`${selectedView === 1 ? "contained" : "text"}`}
        onClick={() => {
          setCalendarView(1);
        }}
        sx={{ borderRadius: "10px", width: "57px", height: "40px" }}
      >
        Week
      </Button>
      <Button
        variant={`${selectedView === 2 ? "contained" : "text"}`}
        onClick={() => {
          setCalendarView(2);
        }}
        sx={{ borderRadius: "10px", width: "57px", height: "40px" }}
      >
        Month
      </Button>
    </div>
  );
};

const RenderCalendarToolbar = (Props: toolbarProps) => {
  const { openDialog, selectedView, setCalendarView } = Props;
  return (
    <Toolbar
      sx={{
        padding: "0 !important",
      }}
    >
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          background: "#FFFFFF",
          borderRadius: "16px",
          padding: "16px",
          width: "100%",
        }}
      >
        <div></div>

        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            gap: "20px",
          }}
        >
          <RenderTimeButton
            setCalendarView={setCalendarView}
            selectedView={selectedView}
          />
          <div
            style={{
              display: "flex",
              gap: "16px",
            }}
          >
            {PriorityList.map((item) => (
              <div
                key={item.id}
                style={{
                  display: "flex",
                  alignItems: "center",
                  gap: "4px",
                }}
              >
                <div
                  style={{
                    width: "12px",
                    height: "12px",
                    borderRadius: "50%",
                    background: `${item.color}`,
                  }}
                ></div>
                <Typography variant="body2" component="div">
                  {item.text}
                </Typography>
              </div>
            ))}
          </div>
          <div
            style={{
              display: "flex",
              paddingLeft: "23px",
              borderLeft: "1px solid #EAEAEA",
              alignItems: "center",
              justifyContent: "center",
              gap: "23px",
            }}
          >
            <img src={link_icon} width={20} height={20} />
            <Button
              variant="contained"
              sx={{
                width: "120px",
                height: "40px",
                borderRadius: "99px",
                boxShadow: "0px 5px 10px rgba(74, 92, 208, 0.2)",
              }}
              onClick={() => {
                openDialog();
              }}
            >
              New Activity
            </Button>
          </div>
        </div>
      </Box>
    </Toolbar>
  );
};

export const DashboardCalendar: React.FC = () => {
  const [openDialog, setOpenDialog] = React.useState<boolean>(false);
  const [department, setDepartment] = React.useState<string>(Departments[0]);
  const [time, setTime] = React.useState<string>(Times[0]);
  const [status, setStatus] = React.useState<string>(Status[0].label);
  const [priority, setPriority] = React.useState<string>(Priority[0].label);
  const [selected, setSelected] = React.useState<number>(0);

  const handleSelect = (value: number) => {
    setSelected(value);
  };

  const onClose = () => {
    setOpenDialog(false);
  };

  const onOpen = () => {
    setOpenDialog(true);
  };

  const onAddActivity = () => {};

  const handleSetDepartment = (event: SelectChangeEvent) => {
    setDepartment(event.target.value as string);
  };

  const handleSetTime = (event: SelectChangeEvent) => {
    setTime(event.target.value as string);
  };

  const handleSetStatus = (event: SelectChangeEvent) => {
    setStatus(event.target.value as string);
  };

  const handleSetPriority = (event: SelectChangeEvent) => {
    setPriority(event.target.value as string);
  };

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        gap: "16px",
      }}
    >
      <StyledHeaderTypography>Calendar</StyledHeaderTypography>
      <RenderCalendarToolbar
        openDialog={onOpen}
        setCalendarView={handleSelect}
        selectedView={selected}
      />
      <Scheduler firstDayOfWeek={0} data={CalendarData}>
        <ViewState />
        <EditingState onCommitChanges={() => {}} />
        <IntegratedEditing />
        {selected === 0 && <DayView startDayHour={3} endDayHour={22} />}
        {selected === 1 && <WeekView startDayHour={3} endDayHour={22} />}
        {selected === 2 && <MonthView />}
        <Appointments />
        <AppointmentTooltip showCloseButton showOpenButton />
        <AppointmentForm readOnly />
        <Resources data={resources} />
      </Scheduler>
      <AddActivityDialog
        onClose={onClose}
        open={openDialog}
        department={department}
        handleSetDepartment={handleSetDepartment}
        time={time}
        handleSetTime={handleSetTime}
        status={status}
        handleSetStatus={handleSetStatus}
        priority={priority}
        handleSetPriority={handleSetPriority}
      />
    </Box>
  );
};
