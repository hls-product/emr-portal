import { Typography } from "@mui/material";
import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { questionnaireService } from "../../services/agent";
import { IRootState } from "../../store/reducers";
import {
  InputBoolean,
  InputDate,
  InputMultipleChoices,
  InputNumeric,
  InputRadio,
  InputText,
} from "../../components/questionInputType";
import { Question } from "@covopen/covquestions-js";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { StyledHeaderTypography } from "../../components/styledTypography";
import { useAppDispatch } from "../../store/Store";
import { changeView } from "../../store/reducers/changeView/changeViewSlice";
import Loading from "../../components/Loading";
import { ResultCategory } from "@covopen/covquestions-js/src/models/Questionnaire.generated";

export const QuestionOverViewDialog: React.FC = () => {
  const { value } = useSelector((state: IRootState) => state.changeViewReducer);
  const dispatch = useAppDispatch();
  const [questionnaire, setQuestionnaire] = React.useState<Question[]>([]);
  const [resultCateogry, setResultCategory] = React.useState<ResultCategory[]>(
    []
  );
  const [loading, setLoading] = React.useState<boolean>(false);
  const [error, setError] = React.useState<unknown>(null);

  useEffect(() => {
    const fetchSingle = async () => {
      try {
        setLoading(true);
        const data = await questionnaireService.getQuestionnaireById(
          String(value)
        );
        if (data) {
          const filterQuestionnaire = data.questions.map((question) => ({
            id: question.id,
            text: question.text,
            type: question.type,
            details: question.details,
            enableWhenExpression: question.enableWhenExpression
              ? JSON.parse(question.enableWhenExpression)
              : undefined,
            optional: question.optional,
            numericOptions: question.numberOptions
              ? question.numberOptions
              : undefined,
            options: question.options
              ? question.options.map((score) => ({
                  text: score.text,
                  value: score.value,
                  scores: score.scores ? JSON.parse(score.scores) : undefined,
                }))
              : undefined,
          })) as unknown as Question[];

          const filterResult = data.resultCategories
            ? (data.resultCategories.map((resultCategory) => ({
                id: resultCategory.id,
                description: resultCategory.description,
                results: resultCategory.results.map((result) => ({
                  id: result.id,
                  text: result.text,
                  expression: result.expression
                    ? JSON.parse(result.expression)
                    : undefined,
                })),
              })) as unknown as ResultCategory[])
            : [];

          setQuestionnaire(filterQuestionnaire);
          setResultCategory(filterResult);
        }
      } catch (error) {
        setError(error);
      }
      setLoading(false);
    };
    fetchSingle();
  }, [value]);

  if (loading) return <Loading />;

  return (
    <div
      style={{
        display: "flex",
        alignItems: "flex-start",
        justifyContent: "center",
        flexDirection: "column",
        // padding: "20px",
      }}
    >
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "flex-start",
          marginBottom: "24px",
          width: "100%",
          gap: "16px",
        }}
      >
        <ArrowBackIcon
          onClick={() => {
            dispatch(
              changeView({
                data: null,
                view: "general",
              })
            );
          }}
          sx={{
            cursor: "pointer",
          }}
        />
        <StyledHeaderTypography
          sx={{
            marginBottom: "0",
          }}
        >
          Question Preview
        </StyledHeaderTypography>
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          width: "100%",
          padding: "20px",
          backgroundColor: "white",
          borderRadius: "16px",
        }}
      >
        <Typography
          variant="h5"
          component="div"
          sx={{
            marginBottom: "10px",
            fontSize: "20px",
            fontWeight: "bold",
            color: "#131626",
          }}
        >
          Question
        </Typography>
        {questionnaire.map((item, index) => (
          <div
            key={item.id}
            style={{
              marginBottom: `${
                index + 1 === questionnaire.length ? "0" : "7px"
              }`,
              padding: "0 15px",
            }}
          >
            <Typography
              variant="h6"
              component="div"
              sx={{
                fontSize: "16px",
                fontWeight: "600",
                color: "#131626",
              }}
            >
              Q{index + 1}. {item.text}
            </Typography>
            {item.type === "multiselect" && (
              <InputMultipleChoices options={item.options} />
            )}
            {item.type === "select" && <InputRadio options={item.options} />}
            {item.type === "boolean" && <InputBoolean />}
            {item.type === "date" && <InputDate />}

            {item.type === "number" &&
              item.numericOptions?.min &&
              item.numericOptions?.max &&
              item.numericOptions?.step && (
                <InputNumeric
                  min={item.numericOptions?.min}
                  max={item.numericOptions?.max}
                  step={item.numericOptions?.step}
                />
              )}

            {item.type === "text" && <InputText />}
          </div>
        ))}
        <Typography
          variant="h5"
          component="div"
          sx={{
            margin: "24px 0 15px 0",
            fontSize: "20px",
            fontWeight: "bold",
            color: "#131626",
          }}
        >
          Result Category
        </Typography>
        {resultCateogry.map((category, index) => (
          <div
            key={category.id}
            style={{
              padding: "0 10px 5px 10px",
            }}
          >
            <Typography
              variant="h6"
              component="div"
              sx={{
                fontSize: "16px",
                fontWeight: "600",
                color: "#131626",
                marginBottom: "10px",
              }}
            >
              Category {index < 9 ? "0" + (index + 1) : index + 1}.{" "}
              {category.description}
            </Typography>
            {category.results.map((result, index) => (
              <div
                key={result.id}
                style={{
                  padding: "0 10px",
                  marginBottom: "7px",
                }}
              >
                <Typography
                  variant="h6"
                  component="div"
                  sx={{
                    fontSize: "14px",
                    fontWeight: "600",
                    color: "#131626",
                  }}
                >
                  Result {index + 1}. {result.id}
                </Typography>
                <Typography
                  variant="body2"
                  component="div"
                  sx={{
                    fontSize: "14px",
                    fontWeight: "400",
                    color: "#131626",
                  }}
                >
                  {result.text}
                </Typography>
              </div>
            ))}
          </div>
        ))}
      </div>
    </div>
  );
};
