import { Questionnaire } from "@covopen/covquestions-js";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { Box, Grid } from "@mui/material";
import { createStyles, makeStyles } from "@mui/styles";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  getAllQuestionnaires,
  getQuestionnaireByIdVersionAndLanguage
} from "../../api/api-client";
import { QuestionnaireEditor } from "../../components/questionnaireEditor/QuestionnaireEditor";
import { QuestionnaireSelection } from "../../components/questionnaireSelection/QuestionnaireSelection";
import { StyledHeaderTypography } from "../../components/styledTypography";
import { QuestionnaireBaseData } from "../../models/question";
import { setQuestionnaireInEditor } from "../../store/reducers/question/questionSlice";
import { useAppDispatch } from "../../store/Store";
import { getQueryParams, setQueryParams } from "../../utils/queryParams";

const useStyles = makeStyles(() =>
  createStyles({
    content: {
      background: "#ffffff",
    },
    editor: {
      background: "#ffffff",
    },
    settings: {
      display: "inline-flex",
      position: "absolute",
      right: 0,
    },
    marginRight: {
      marginRight: "10px",
    },
  })
);

export const QuestionDashboard: React.FC = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  
  const [showMenu, setShowMenu] = useState(false);
  const [allQuestionnaires, setAllQuestionnaires] = useState<
    QuestionnaireBaseData[]
  >([]);
  const [currentQuestionnaireSelection, setCurrentQuestionnaireSelection] =
    useState<QuestionnaireSelection>({});
  const [originalCurrentQuestionnaire, setOriginalCurrentQuestionnaire] =
    useState<Questionnaire | undefined>(undefined);
  const [executedQuestionnaire, setExecutedQuestionnaire] = useState<
    Questionnaire | undefined
  >(undefined);

  const classes = useStyles();

  function overwriteCurrentQuestionnaire(newQuestionnaire: Questionnaire) {
    setExecutedQuestionnaire(JSON.parse(JSON.stringify(newQuestionnaire)));
  }

  useEffect(() => {
    getAllQuestionnaires().then((value) => setAllQuestionnaires(value));
  }, []);

  useEffect(() => {
    if (
      currentQuestionnaireSelection.id !== undefined &&
      currentQuestionnaireSelection.version !== undefined &&
      currentQuestionnaireSelection.language !== undefined
    ) {
      setQueryParams(currentQuestionnaireSelection);

      getQuestionnaireByIdVersionAndLanguage(
        currentQuestionnaireSelection.id,
        currentQuestionnaireSelection.version,
        currentQuestionnaireSelection.language
      ).then((value) => {
        if (value !== undefined) {
          setOriginalCurrentQuestionnaire(value);
          dispatch(setQuestionnaireInEditor(value));
          overwriteCurrentQuestionnaire(value);
        } else {
          console.error(
            `Cannot get questionnaire with values ${JSON.stringify(
              currentQuestionnaireSelection
            )}`
          );
        }
      });
    }
  }, [dispatch, currentQuestionnaireSelection]);

  // Select Questionnaire that is saved in the query params
  const querySelection: QuestionnaireSelection = getQueryParams();
  if (querySelection.id != null && currentQuestionnaireSelection.id == null) {
    setCurrentQuestionnaireSelection(querySelection);
  }

  return (
    <Box
      sx={{
        width: "100%",
      }}
    >
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "flex-start",
          marginBottom: "24px",
          width: "100%",
          gap: "16px",
        }}
      >
        <ArrowBackIcon
          onClick={() => {
            navigate("/admin/questions");
          }}
          sx={{
            cursor: "pointer",
          }}
        />
        <StyledHeaderTypography
          sx={{
            marginBottom: "0",
          }}
        >
          Questionnaire
        </StyledHeaderTypography>
      </div>
      <Grid
        container
        className={`${classes.content} flex-grow overflow-pass-through`}
        direction="column"
        justifyContent="center"
        alignItems="center"
      >
        <Grid
          item
          container
          direction="row"
          className={`flex-grow overflow-pass-through`}
        >
          <Grid
            item
            container
            xs={12}
            onClick={() => setShowMenu(false)}
            className={`${classes.editor} flex-grow overflow-pass-through`}
          >
            <QuestionnaireEditor />
          </Grid>
        </Grid>
      </Grid>
    </Box>
  );
};
