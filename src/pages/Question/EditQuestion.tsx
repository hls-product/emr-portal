import React, { useEffect, useState } from "react";
import {
  AppBar,
  Box,
  Button,
  FormControlLabel,
  Grid,
  IconButton,
  Switch,
  Tab,
  Tabs,
  ThemeProvider,
  Typography,
} from "@mui/material";
import { makeStyles, createStyles } from "@mui/styles";
import MenuIcon from "@mui/icons-material/Menu";
import { QuestionnaireExecution } from "../../components/questionExecution";
import { QuestionnaireEditor } from "../../components/questionnaireEditor/QuestionnaireEditor";
import { ISOLanguage, Questionnaire } from "@covopen/covquestions-js";
import { useAppDispatch } from "../../store/Store";
import { IRootState } from "../../store/reducers";
import { setQuestionnaireInEditor } from "../../store/reducers/question/questionSlice";
import {
  QuestionnaireSelection,
  QuestionnaireSelectionDrawer,
} from "../../components/questionnaireSelection/QuestionnaireSelection";
import { useSelector } from "react-redux";
import { QuestionnaireBaseData } from "../../models/question";
import { SettingSelection } from "../../components/questionnaireSelection/SettingSelection";
import { getQueryParams, setQueryParams } from "../../utils/queryParams";
import { UserInstructions } from "../../components/questionUserInstructions";
import { removeStringRepresentationFromQuestionnaire } from "../../components/questionnaireEditor/converters";
import {
  getAllQuestionnaires,
  getQuestionnaireByIdVersionAndLanguage,
} from "../../api/api-client";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { StyledHeaderTypography } from "../../components/styledTypography";
import { useNavigate } from "react-router-dom";
import Loading from "../../components/Loading";

type Tab =
  | "question"
  | "general"
  | "category"
  | "variable"
  | "test"
  | "preview";

const useStyles = makeStyles(() =>
  createStyles({
    content: {
      background: "#ffffff",
    },
    editor: {
      background: "#ffffff",
      // boxShadow: "3px 0px 34px rgba(0, 0, 0, 0.06)",
    },
    settings: {
      display: "inline-flex",
      position: "absolute",
      right: 0,
    },
    marginRight: {
      marginRight: "10px",
    },
  })
);

export const EditQuestion: React.FC = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const currentQuestionnaire = useSelector(
    (state: IRootState) => state.questionReducer
  );

  const { loading, data } = useSelector(
    (state: IRootState) => state.getQuestionnaireByIdReducer
  );

  const duplicatedIds = useSelector((state: IRootState) => {
    const questionnaire = state.questionReducer.questionnaire;
    const ids: string[] = [];
    if (questionnaire.questions !== undefined) {
      ids.push(...questionnaire.questions.map((it) => it.id));
    }
    if (questionnaire.resultCategories !== undefined) {
      for (const resultCategory of questionnaire.resultCategories) {
        ids.push(resultCategory.id);
        if (resultCategory.results !== undefined) {
          ids.push(...resultCategory.results.map((it) => it.id));
        }
      }
    }
    if (questionnaire.variables !== undefined) {
      ids.push(...questionnaire.variables.map((it) => it.id));
    }
    return ids.filter(
      (item, index) => item !== undefined && ids.indexOf(item) !== index
    );
  });

  const [allQuestionnaires, setAllQuestionnaires] = useState<
    QuestionnaireBaseData[]
  >([]);
  const [currentQuestionnaireSelection, setCurrentQuestionnaireSelection] =
    useState<QuestionnaireSelection>({});
  const [originalCurrentQuestionnaire, setOriginalCurrentQuestionnaire] =
    useState<Questionnaire | undefined>(undefined);
  const [executedQuestionnaire, setExecutedQuestionnaire] = useState<
    Questionnaire | undefined
  >(undefined);

  const [showMenu, setShowMenu] = useState(false);

  const [isJsonMode, setIsJsonMode] = useState(false);
  const [isHelpOpen, setIsHelpOpen] = useState(false);

  const classes = useStyles();

  function overwriteCurrentQuestionnaire(newQuestionnaire: Questionnaire) {
    setExecutedQuestionnaire(JSON.parse(JSON.stringify(newQuestionnaire)));
  }

  const handleVersionChanged = (newVersion: number) => {
    const questionnaire = allQuestionnaires.filter(
      (it) =>
        it.id === currentQuestionnaireSelection.id && it.version === newVersion
    )[0];
    const changedValues = {
      version: newVersion,
      availableLanguages: questionnaire.meta.availableLanguages,
      language: currentQuestionnaireSelection.language,
    };
    if (
      currentQuestionnaireSelection.language !== undefined &&
      changedValues.availableLanguages.indexOf(
        currentQuestionnaireSelection.language
      ) === -1
    ) {
      if (changedValues.availableLanguages.indexOf("en") > -1) {
        changedValues.language = "en";
      } else {
        changedValues.language = changedValues.availableLanguages[0];
      }
    }
    setCurrentQuestionnaireSelection({
      ...currentQuestionnaireSelection,
      ...changedValues,
    });
  };

  const resetQuestionnaire = () => {
    if (originalCurrentQuestionnaire) {
      dispatch(setQuestionnaireInEditor(originalCurrentQuestionnaire));
      overwriteCurrentQuestionnaire(originalCurrentQuestionnaire);
    }
  };

  useEffect(() => {
    getAllQuestionnaires().then((value) => setAllQuestionnaires(value));
  }, []);

  useEffect(() => {
    if (
      currentQuestionnaireSelection.id !== undefined &&
      currentQuestionnaireSelection.version !== undefined &&
      currentQuestionnaireSelection.language !== undefined
    ) {
      setQueryParams(currentQuestionnaireSelection);

      getQuestionnaireByIdVersionAndLanguage(
        currentQuestionnaireSelection.id,
        currentQuestionnaireSelection.version,
        currentQuestionnaireSelection.language
      ).then((value) => {
        if (value !== undefined) {
          setOriginalCurrentQuestionnaire(value);
          dispatch(setQuestionnaireInEditor(value));
          overwriteCurrentQuestionnaire(value);
        } else {
          console.error(
            `Cannot get questionnaire with values ${JSON.stringify(
              currentQuestionnaireSelection
            )}`
          );
        }
      });
    }
  }, [dispatch, currentQuestionnaireSelection]);

  // useEffect(() => {
  //   if (
  //     currentQuestionnaire === undefined ||
  //     currentQuestionnaire.questionnaire === undefined ||
  //     currentQuestionnaire.questionnaire.title === ""
  //   ) {
  //     setCurrentTitle(undefined);
  //   } else {
  //     setCurrentTitle(currentQuestionnaire.questionnaire.title);
  //   }

  //   if (!hasAnyError) {
  //     setExecutedQuestionnaire(currentQuestionnaire.questionnaire);
  //   }
  // }, [currentQuestionnaire, hasAnyError]);

  // Select Questionnaire that is saved in the query params
  const querySelection: QuestionnaireSelection = getQueryParams();
  if (querySelection.id != null && currentQuestionnaireSelection.id == null) {
    setCurrentQuestionnaireSelection(querySelection);
  }

  if (loading) return <Loading />;

  return (
    <Box
      sx={{
        width: "100%",
      }}
      // sx={{
      //   minHeight: "100vh",
      // }}
    >
      {/* <AppBar position="static">
        <Toolbar variant="dense">
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={() => setShowMenu(!showMenu)}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap>
            CovQuestions{" "}
            {currentTitle !== undefined ? <> - {currentTitle}</> : null}
          </Typography>
          <div className={classes.settings}>
            <Button
              onClick={resetQuestionnaire}
              className={classes.marginRight}
              variant="outlined"
              color="secondary"
            >
              Reset
            </Button>
            {currentQuestionnaireSelection.language !== undefined &&
            currentQuestionnaireSelection.availableLanguages !== undefined ? (
              <SettingSelection
                title="Language"
                values={currentQuestionnaireSelection.availableLanguages}
                selectedValue={currentQuestionnaireSelection.language}
                handleChange={(value) => {
                  setCurrentQuestionnaireSelection({
                    ...currentQuestionnaireSelection,
                    ...{ language: value as ISOLanguage },
                  });
                }}
              />
            ) : null}
            {currentQuestionnaireSelection.version !== undefined &&
            currentQuestionnaireSelection.availableVersions !== undefined ? (
              <SettingSelection
                title="Version"
                values={currentQuestionnaireSelection.availableVersions}
                selectedValue={currentQuestionnaireSelection.version}
                handleChange={(value) => {
                  handleVersionChanged(value as number);
                }}
              />
            ) : null} */}
      {/* <Button
              onClick={() => setIsHelpOpen(true)}
              className={classes.marginRight}
              variant="outlined"
              color="secondary"
            >
              ?
            </Button> */}
      {/* </div>
        </Toolbar>
      </AppBar> */}

      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "flex-start",
          marginBottom: "24px",
          width: "100%",
          gap: "16px",
        }}
      >
        <ArrowBackIcon
          onClick={() => {
            navigate("/admin/questions");
          }}
          sx={{
            cursor: "pointer",
          }}
        />
        <StyledHeaderTypography
          sx={{
            marginBottom: "0",
          }}
        >
          Questionnaire
        </StyledHeaderTypography>
      </div>
      <Grid
        container
        className={`${classes.content} flex-grow overflow-pass-through`}
        direction="column"
        justifyContent="center"
        alignItems="center"
      >
        <Grid
          item
          container
          direction="row"
          className={`flex-grow overflow-pass-through`}
        >
          {/* {showMenu ? (
            <Grid item xs={3}>
              <QuestionnaireSelectionDrawer
                handleChange={(value) => {
                  setCurrentQuestionnaireSelection(value);
                  setShowMenu(false);
                }}
                allQuestionnaires={allQuestionnaires}
                selectedValue={
                  currentQuestionnaireSelection ?? {
                    id: "",
                    version: 0,
                    language: "de",
                  }
                }
              />
            </Grid>
          ) : null} */}

          <Grid
            item
            container
            xs={12}
            onClick={() => setShowMenu(false)}
            className={`${classes.editor} flex-grow overflow-pass-through`}
          >
            <QuestionnaireEditor mode="edit" data={data} />
          </Grid>
        </Grid>
      </Grid>
    </Box>
  );
};
