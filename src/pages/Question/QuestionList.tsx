import { Add, Create, Visibility } from "@mui/icons-material";
import DeleteIcon from "@mui/icons-material/Delete";
import { Box, IconButton, Typography } from "@mui/material";
import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { DeleteDialog } from "../../components/DeleteDialog";
import { RoundedButton } from "../../components/RoundedButton";
import { StyledHeaderTypography } from "../../components/styledTypography";
import { CustomTable } from "../../components/Table/Table";
import { HeadCell } from "../../components/Table/types";
import { Toolbar } from "../../components/toolbar";
import { question } from "../../data/toolbar";
import { IRootState } from "../../store/reducers";
import { changeView } from "../../store/reducers/changeView/changeViewSlice";
import { deleteQuestionnaireById } from "../../store/reducers/questionnaire/deleteQuestionnaireByIdSlice";
import { getQuestionnaireById } from "../../store/reducers/questionnaire/getQuestionnaireByIdSlice";
import { getAllQuestionnaire } from "../../store/reducers/questionnaire/getQuestionnaireSlice";
import { useAppDispatch } from "../../store/Store";
import { QuestionOverViewDialog } from "./QuestionOverViewDialog";

const headCells: HeadCell[] = [
  { id: "number", numeric: true, label: "No." },
  { id: "questionaire", numeric: false, label: "Questionnaire" },
  { id: "author", numeric: false, label: "Author" },
  { id: "date", numeric: false, label: "Date" },
  { id: "availableLanguages", numeric: false, label: "Available Language" },
  { id: "version", numeric: false, label: "Version" },
  { id: "action", numeric: false, label: "Action" },
];
const HeadCells: string[] = [
  "#",
  "Questionnaire",
  "Author",
  "Date",
  "Available Language",
  "Version",
  "Action",
];

export const QuestionList: React.FC = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const [rowsPerPage, setRowsPerPage] = React.useState<number>(5);
  const [page, setPage] = React.useState<number>(0);
  const [openDialog, setOpenDialog] = React.useState<boolean>(false);
  const [id, setId] = React.useState<string>("");
  const { data, loading, paging } = useSelector(
    (state: IRootState) => state.getQuestionnaireReducer
  );
  const { view } = useSelector((state: IRootState) => state.changeViewReducer);

  const handleClickDelete = (id: string) => {
    setOpenDialog(true);
    setId(id);
  };

  const handleClickCancelDelete = () => {
    setOpenDialog(false);
  };

  const handleClickConfirmDelete = () => {
    setOpenDialog(false);
    dispatch(
      deleteQuestionnaireById({
        id: id,
      })
    );
  };
  const handleChangePage = (event: any, newPage: number) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event: any) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const mapDataToRender = () => {
    const render = data.map((item, index) => ({
      id: item.id as string,
      number: index + 1,
      questionaire: item.title,
      author: item.meta.author,
      date: item.meta.creationDate,
      availableLanguages: item.meta.availableLanguages.map(
        (language, index) => (
          <React.Fragment key={index}>
            {language}
            {index < item.meta.availableLanguages.length - 1 && ", "}
          </React.Fragment>
        )
      ),
      version: item.version,
      action: (
        <Box style={{ textAlign: "center" }}>
          <IconButton
            onClick={() => {
              navigate(`/admin/questions/${item.id}/edit`);
              dispatch(
                getQuestionnaireById({
                  id: item.id,
                })
              );
            }}
            children={<Create />}
          />
          <IconButton
            onClick={() => {
              dispatch(
                changeView({
                  data: item.id,
                  view: "detail",
                })
              );
            }}
            children={<Visibility />}
          />
          <IconButton
            onClick={() => {
              handleClickDelete(item.id);
            }}
            children={<DeleteIcon />}
          />
        </Box>
      ),
    }));
    return render;
  };

  useEffect(() => {
    dispatch(
      getAllQuestionnaire({
        page: page + 1,
        size: rowsPerPage,
      })
    );
  }, [page, rowsPerPage]);

  return (
    <>
      {view === "general" ? (
        <Box
          sx={{
            width: "100%",
            display: "flex",
            flexDirection: "column",
            gap: "24px",
          }}
        >
          <StyledHeaderTypography
            style={{
              marginBottom: "0",
            }}
            children={"Questionaire"}
          />
          <Toolbar
            actionList={question}
            defaultValue={question[0]}
            background="#FFFFFF"
          />
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              width: "100%",
              padding: "20px",
              backgroundColor: "white",
              borderRadius: "16px",
            }}
          >
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                gap: "8px",
              }}
            >
              <Typography variant="h2" children={"QUESTIONAIRE"} />
              <RoundedButton
                variant="contained"
                endIcon={<Add />}
                onClick={() => {
                  navigate("add");
                }}
                children="Add Questionaire"
              />
            </Box>
            <CustomTable
              title="Questionaire"
              headcells={headCells}
              body={mapDataToRender()}
              paging={{
                page: page,
                rowsPerPage: rowsPerPage,
                rowsCount: paging.total,
              }}
              loading={loading}
              toolbar={false}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
          </Box>
        </Box>
      ) : (
        <QuestionOverViewDialog />
      )}
      <DeleteDialog
        open={openDialog}
        onClose={handleClickCancelDelete}
        onConfirm={handleClickConfirmDelete}
        onCancel={handleClickCancelDelete}
      />
    </>
  );
};
