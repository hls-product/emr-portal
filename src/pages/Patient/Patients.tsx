// Import React/React Router/Redux/Axios
import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";

// Import MUI components
import { Add, Create, Search, Visibility } from "@mui/icons-material";
import DeleteIcon from "@mui/icons-material/Delete";
import {
  Box,
  IconButton,
  InputAdornment,
  TextField,
  Typography,
} from "@mui/material";
import { RoundedButton } from "../../components/RoundedButton";

// Import common components
import { DeleteDialog } from "../../components/DeleteDialog";
import { AdvancedSearch } from "../../components/Table/AdvancedSearch";
import { CustomTable } from "../../components/Table/Table";
import { HeadCell } from "../../components/Table/types";

// Import reducers/api services
import { patientService } from "../../services/agent";

// Import type
import { PatientGetRequest } from "../../models/apiRequest/patient";
import { patientModel } from "../../models/patients";

// Main component
const headCells: HeadCell[] = [
  {
    id: "number",
    numeric: true,
    label: "No.",
  },
  {
    id: "name",
    numeric: true,
    label: "Patient name",
  },
  {
    id: "gender",
    numeric: true,
    label: "Gender",
  },
  {
    id: "address",
    numeric: true,
    label: "Address",
  },
  {
    id: "phone",
    numeric: true,
    label: "Phone number",
  },
  {
    id: "dob",
    numeric: true,
    label: "Date of birth",
  },
  {
    id: "doctor",
    numeric: true,
    label: "Doctor name",
  },
  {
    id: "action",
    numeric: true,
    label: "Action",
  },
];
const advancedSearchProps = [
  {
    name: "gender",
    label: "Gender",
    select: true,
    menuItems: [
      {
        value: "male",
        display: "Male",
      },
      {
        value: "female",
        display: "Female",
      },
    ],
  },
  {
    name: "disease",
    label: "Disease",
    select: true,
    menuItems: [
      {
        value: "insomnia",
        display: "Insomnia",
      },
      {
        value: "depression",
        display: "Depression",
      },
    ],
  },
  {
    name: "status",
    label: "Status",
    select: true,
    menuItems: [
      {
        value: "status1",
        display: "Status1",
      },
      {
        value: "status2",
        display: "Status2",
      },
    ],
  },
  {
    name: "doctor",
    label: "Doctor Name",
    select: true,
    menuItems: [
      {
        value: "doctor1",
        display: "Doctor 1",
      },
      {
        value: "doctor2",
        display: "Doctor 2",
      },
    ],
  },
  {
    name: "phone",
    label: "Phone Number",
  },
  {
    name: "id",
    label: "Patient ID",
  },
  // {
  //   name: "period",
  //   dateTime: "period",
  //   inputFormat: "YYYY"
  // },
];
export const Patients: React.FC = () => {
  const navigate = useNavigate();

  // States
  const [isLoading, setIsLoading] = React.useState(false);
  const [data, setData] = React.useState<patientModel[]>([]);
  const [page, setPage] = React.useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = React.useState<number>(10);
  const [rowsCount, setRowsCount] = React.useState<number>(0);
  const [deletePopUpOpen, setDeletePopUpOpen] = React.useState(false);
  const [openAdvancedSearch, setOpenAdvancedSearch] = React.useState(false);
  const [currentSelectedId, setCurrentSelectedId] = React.useState("");

  // Search states
  const [searchValue, setSearchValue] = React.useState<string>("");
  const [debounceSeachValue, setDebounceSearchValue] = useState<string>("");
  const [filter, setFilter] = React.useState<{ [key: string]: string }>({});
  // Api calling functions

  const getPatientList = async (filter?: PatientGetRequest) => {
    const params: PatientGetRequest = {
      ...filter,
      page: page + 1,
      size: rowsPerPage,
      name: debounceSeachValue,
    };
    setIsLoading(true);
    try {
      let { data, paging } = await patientService.getList({ params });
      setData(data);
      setRowsCount(paging.total);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      console.log("getPatientList error: ", error);
    }
  };

  const deletePatient = async (id: string) => {
    setIsLoading(true);
    try {
      let response = await patientService.delete(id);
      setIsLoading(false);
      return response;
    } catch (error) {
      setIsLoading(false);
      console.log("deletePatient error: ", error);
    }
  };

  // Event Handler
  const handleChangePage = (event: any, newPage: number) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event: any) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleDelete = (id: string) => {
    setDeletePopUpOpen(true);
    setCurrentSelectedId(id);
  };
  const handleCancelDelete = () => {
    setDeletePopUpOpen(false);
  };
  const handleConfirmDelete = () => {
    setDeletePopUpOpen(false);
    //currentPatientIdSelected
    deletePatient(currentSelectedId).then((res) => {
      setCurrentSelectedId("");
      getPatientList();
    });
  };
  const handleClickAdvancedSearchOpen = () => {
    setOpenAdvancedSearch(true);
  };

  // useEffect

  //Debounce for name search
  React.useEffect(() => {
    let timeout = setTimeout(() => {
      setDebounceSearchValue(searchValue);
    }, 500);
    return () => {
      clearTimeout(timeout);
    };
  }, [searchValue]);

  // Back to previous page if current page is empty after deleting an item
  React.useEffect(() => {
    let totalPages = Math.ceil(rowsCount / rowsPerPage);
    let currentPage = page + 1;
    if (currentPage > totalPages && totalPages > 0) {
      setPage(totalPages - 1);
    }
  }, [rowsCount]);
  React.useEffect(() => {
    getPatientList();
  }, [debounceSeachValue, rowsPerPage, page]);

  const mapDataToRender = () => {
    const render = data.map((item, index) => ({
      id: item.id as string,
      number: index + 1,
      name:
        item.name?.find((x) => x.use === "official")?.text ||
        item.name?.find((x) => x.use === "official")?.family,
      gender: (
        <Typography
          sx={{
            textTransform: "capitalize",
          }}
          children={item.gender}
        />
      ),
      address: item.address?.find((x) => x.use === "home")?.text,
      phone: item.telecom?.find((x) => x.system === "phone")?.value,
      dob: item.birthDate,
      doctor: item.generalPractitioner
        ? item.generalPractitioner[0].display
        : "Dr Doom",
      action: (
        <Box style={{ textAlign: "center" }}>
          <Link
            to={`details/edit?id=${item.id}`}
            children={<IconButton children={<Create />} />}
          />
          <Link
            to={`details?id=${item.id}`}
            children={<IconButton children={<Visibility />} />}
          />

          <Link
            to={`#`}
            onClick={() => {
              handleDelete(item.id as string);
            }}
            children={<IconButton children={<DeleteIcon />} />}
          />
        </Box>
      ),
    }));
    return render;
  };
  
  return (
    <>
      {/* Dialog for delete item popup */}
      <DeleteDialog
        open={deletePopUpOpen}
        onClose={handleCancelDelete}
        onConfirm={handleConfirmDelete}
        onCancel={handleCancelDelete}
      />
      <Box>
        <Typography
          fontSize={"24px"}
          fontWeight={700}
          color={"#131626"}
          style={{ paddingBottom: "20px" }}
          children="Patients"
        />
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            width: "100%",
            padding: "20px",
            backgroundColor: "white",
            borderRadius: "16px",
          }}
        >
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: "8px",
            }}
          >
            <Typography variant="h2" children={"PATIENT LIST"} />
            <Box sx={{ display: "flex", gap: "8px" }}>
              <TextField
                placeholder="Search patient name..."
                InputProps={{
                  startAdornment: (
                    <InputAdornment
                      position="start"
                      children={<Search sx={{ height: 20 }} />}
                    />
                  ),
                }}
                sx={{
                  "& .MuiInputBase-root": {
                    height: 48,
                    width: 331,
                    borderRadius: 3,
                  },
                }}
                value={searchValue}
                onChange={(e) => {
                  setSearchValue(e.target.value);
                }}
              />
              <RoundedButton
                variant="contained"
                onClick={handleClickAdvancedSearchOpen}
                children="Advanced Search"
              />
              <RoundedButton
                variant="contained"
                endIcon={<Add />}
                onClick={() => {
                  navigate("add");
                }}
                children="Add Patient"
              />
            </Box>
          </Box>
          {openAdvancedSearch && (
            <AdvancedSearch
              props={advancedSearchProps}
              filter={filter}
              description="Find patients with..."
              onFilterChange={setFilter}
              onApply={getPatientList}
              onClose={setOpenAdvancedSearch}
            />
          )}
          <CustomTable
            title={"PATIENT LIST"}
            headcells={headCells}
            body={mapDataToRender()}
            paging={{
              page: page,
              rowsPerPage: rowsPerPage,
              rowsCount: rowsCount,
            }}
            loading={isLoading}
            toolbar={false}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Box>
      </Box>
    </>
  );
};
