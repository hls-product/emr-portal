import React from "react";
import { useState, useRef } from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import UnkownPersonPic from "./../../../assets/image/unknownPerson.png";

// Import styling
import {
  ArrowBack,
  Container,
  Section,
  SaveButton,
  Header,
  Body,
  StyledBodySectionTitle,
  UploadAvatar,
  UploadAvatarIcon,
  StyledInputTextField as TextField,
  StyledMenuItem,
  StyledDesktopDatePicker,
  Row,
  StyledInputSelect as Select,
  StyledChip,
  StyledHealInfoContainer,
  StyledCloseChipIcon,
  DeleteBadge,
  StyledAvatarUploaded,
  StyledAvatarDeleteIcon,
  StyledCancelEditButton,
} from "./styles.components";

// Import MUI components
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { SelectChangeEvent } from "@mui/material/Select";
import OutlinedInput from "@mui/material/OutlinedInput";
import Box from "@mui/material/Box";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";

// Common components
import Loading from "../../../components/Loading";
import {
  StyledTypography as Typography,
  StyledHeaderTypography as HeaderTypography,
} from "../../../components/styledTypography";
import doctor from "../../../assets/image/doctor.svg";
import { MenuItem } from "@mui/material";
import { patientService } from "../../../services/agent";

// Import type
import { patientModel } from "../../../models/patients";
export const EditPatient: React.FC = () => {
  const patientInitialState = {
    id: "",
    name: [
      {
        use: "official" as "official",
        text: "",
        given: [""],
        family: "",
      },
    ],
    gender: "unknown" as "unknown",
    telecom: [
      {
        system: "phone" as "phone",
        value: "",
        rank: 0,
      },
    ],
    address: [
      {
        text: "",
      },
    ],
    disease: "",
    status: 0,
  };
  // Hook instantilizing
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState<boolean>(true);
  // patient info states when loading
  const [imageUploaded, setImageUploaded] = useState<any>(doctor);
  const [id, setId] = useState("");
  const [name, setName] = useState("");
  const [gender, setGender] = useState("");
  const [phone, setPhone] = useState<string[]>([]);
  const [email, setEmail] = useState<string[]>([]);
  const [bloodGroup, setBloodgroup] = useState("AB");
  const [address, setAddress] = useState("");
  const [insuranceNumber, setInsuranceNumber] = useState("IS2341243");
  const [insuranceExpiryDate, setInsuranceExpiryDate] = useState<Date | null>(
    new Date("2014-08-18T21:11:54")
  );
  const [birthDate, setBirthDate] = useState<Date | null>(
    new Date("2014-08-18T21:11:54")
  );
  const [protectorName, setProtectorName] = useState("Snoop Cat");
  const [protectorRelationship, setProtectorRelationship] = useState("Spouse");
  const [protectorPhone, setProtectorPhone] = useState("02394012344");
  const [protectorEmail, setProtectorEmail] = useState(
    "snoopLoveWeed@gmail.com"
  );
  const [allergies, setAllergies] = useState(["Bird", "Cat", "Dog", "Rat"]);
  const [anamnesis, setAnamnesis] = useState(["Bird", "Cat", "Dog", "Rat"]);
  const [searchParams, setSearchParams] = useSearchParams();

  // Default res for options to be picked
  const uploadInputRef = useRef<any>(null);
  const genderItems = [
    { value: "male", label: "Male" },
    { value: "female", label: "Female" },
  ];
  const relationshipsItems = [
    { value: "Father", label: "Father" },
    { value: "Mother", label: "Mother" },
    { value: "Spouse", label: "Spouse" },
    { value: "Sibling", label: "Sibling" },
    { value: "Offspring", label: "Offspring" },
  ];
  const bloodGroupsItems = [
    { value: "A", label: "A" },
    { value: "B", label: "B" },
    { value: "AB", label: "AB" },
    { value: "0", label: "0" },
  ];
  const allergiesItems = ["Gluten", "Celery", "Tree nuts", "Animal"];
  const anamnesisItems = ["Gluten", "Celery", "Tree nuts", "Animal"];

  // Event handlers
  const handleExpiryDateChange = (newValue: Date | any) => {
    setInsuranceExpiryDate(newValue);
  };
  const handleBirthDateChange = (newValue: Date | any) => {
    setBirthDate(newValue);
  };
  const handleAllergiesSelectChange = (event: SelectChangeEvent<any>) => {
    const {
      target: { value },
    } = event;
    setAllergies(
      // On autofill we get a stringified value.
      typeof value === "string" ? value.split(",") : value
    );
  };
  const deleteAllergySelected = (e: React.MouseEvent, value: string) => {
    e.preventDefault();
    console.log("clicked delete allergy");
    const allergiesStateCloned = [...allergies];
    const indexOfDeletedItem = allergiesStateCloned.indexOf(value);
    if (indexOfDeletedItem !== -1) {
      allergiesStateCloned.splice(indexOfDeletedItem, 1);
    }
    setAllergies(allergiesStateCloned);
  };
  const deleteAnamnesisSelected = (e: React.MouseEvent, value: string) => {
    e.preventDefault();
    console.log("clicked delete anamnesis");
    const anamnesisStateCloned = [...anamnesis];
    const indexOfDeletedItem = anamnesisStateCloned.indexOf(value);
    if (indexOfDeletedItem !== -1) {
      anamnesisStateCloned.splice(indexOfDeletedItem, 1);
    }
    setAnamnesis(anamnesisStateCloned);
  };
  const handleAnamnesisSelectChange = (event: SelectChangeEvent<any>) => {
    const {
      target: { value },
    } = event;
    setAnamnesis(
      // On autofill we get a stringified value.
      typeof value === "string" ? value.split(",") : value
    );
  };
  const handleUploadImage = (event: any) => {
    const imageSrc = URL.createObjectURL(event.target.files[0]);
    console.log(imageSrc);
    setImageUploaded(imageSrc);
  };
  const deleteAvatarUploaded = () => {
    setImageUploaded(null);
  };

  const onSave = () => {
    patientService.patch({
      id: searchParams.get("id") as unknown as string,
      body: {
        name: [{ use: "official", text: name }],
        gender: gender as patientModel["gender"],
        birthDate: birthDate?.toISOString().split("T")[0],
        telecom: [
          ...phone.map((item, index) => ({
            system: "phone" as "phone" | "fax" | "email" | "pager" | "url" | "sms" | "other",
            value: item,
          })),
          ...email.map((item, index) => ({
            system: "email" as "phone" | "fax" | "email" | "pager" | "url" | "sms" | "other",
            value: item,
          }))
        ],
        address: [
          {
            use: "home",
            text: address,
          },
        ],
      },
  });
    navigate(`/admin/patient/details?id=${id}`);
  };

  // useEffect
  // Call api on mount
  React.useEffect(() => {
    try {
      patientService
        .getById(searchParams.get("id") as unknown as string)
        .then((res) => {
          setId(res.id as string);
          setName(() => {
            const name = res.name?.find((item) => item.use === "official");
            return (
              name?.text ||
              (((name?.given?.join(" ") as string) +
                " " +
                name?.family) as string)
            );
          });
          setGender(res.gender as string);
          setPhone(
            res.telecom
              ?.filter((item) => item.system === "phone")
              .map((item) => item.value) as string[]
          );
          setEmail(
            res.telecom
              ?.filter((item) => item.system === "email")
              .map((item) => item.value) as string[]
          );
          setAddress(() => {
            const address = res.address?.find((item) => item.use === "home");
            return address?.text
              ? (address?.text as string)
              : [
                  address?.line,
                  address?.district,
                  address?.city,
                  address?.state,
                  address?.country,
                ]
                  .filter((item) => item)
                  .join(" ");
          });
          setBirthDate(new Date(res.birthDate as string))
        })
        .then(() => setIsLoading(false));
    } catch (err) {
      console.error("Cannot get patient details");
      history.back();
    }
  }, []);
  return (
    <>
      {isLoading && <Loading />}
      {!isLoading && (
        <Container>
          {!imageUploaded ? (
            <input
              ref={uploadInputRef}
              type="file"
              accept="image/*"
              style={{ display: "none" }}
              onChange={handleUploadImage}
            />
          ) : null}
          {/* Top section */}
          <Header>
            <Box
              sx={{
                display: "flex",
                gap: "16px",
                alignItems: "center",
              }}
            >
              <ArrowBack
                onClick={() => navigate(`/admin/patient/details?id=${id}`)}
              />
              <HeaderTypography
                style={{
                  marginBottom: "0",
                }}
                children={"Edit Patient"}
              />
            </Box>
            <SaveButton onClick={onSave} children={"Save"} />
          </Header>
          {/* End of top section */}
          <Body>
            {/* Left Section: General Info */}
            <Section>
              <Typography
                color="#081735"
                fontSize={16}
                fontWeight="600"
                padding="8px 0"
                children="GENERAL INFO"
              />
              {!imageUploaded ? (
                <UploadAvatar
                  onClick={() =>
                    uploadInputRef.current && uploadInputRef.current.click()
                  }
                >
                  <UploadAvatarIcon />
                  Upload avatar
                </UploadAvatar>
              ) : (
                <DeleteBadge
                  overlap="circular"
                  anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
                  badgeContent={
                      <StyledAvatarDeleteIcon onClick={deleteAvatarUploaded} />
                  }
                  children={
                    <StyledAvatarUploaded
                      alt="Patient Avatar"
                      src={imageUploaded}
                    />
                  }
                />
              )}
              <TextField
                label="Patient name"
                id="patientNameInput"
                onChange={(event) => setName(event.target.value)}
                sx={{ width: "100%", height: "3rem" }}
                value={name}
              />
              <Row>
                <TextField
                  label="Gender"
                  id="patientGenderSelect"
                  select
                  sx={{ width: "48%", height: "3rem" }}
                  value={gender}
                  onChange={(event) => setGender(event.target.value)}
                  children={genderItems.map((option) => (
                    <MenuItem
                      key={option.value}
                      value={option.value}
                      children={option.label}
                    />
                  ))}
                />
                 <LocalizationProvider dateAdapter={AdapterDateFns}>
                  <StyledDesktopDatePicker
                    label="Birth Date"
                    inputFormat="dd/MM/yyyy"
                    value={birthDate}
                    onChange={handleBirthDateChange}
                    renderInput={(params) => (
                      <TextField
                        sx={{ width: "48%", height: "3rem" }}
                        {...params}
                      />
                    )}
                  />
                </LocalizationProvider>
              </Row>
              <TextField
                  label="Phone Number"
                  id="patientPhoneNumberInput"
                  sx={{ width: "100%", height: "3rem" }}
                  value={phone}
                  onChange={(event) => setPhone(event.target.value.split(","))}
                />
              <TextField
                label="Email"
                id="patientEmailInput"
                sx={{ width: "100%", height: "3rem" }}
                value={email}
                onChange={(event) => setEmail(event.target.value.split(","))}
              />
              <TextField
                label="Address"
                id="patientAddressInput"
                sx={{ width: "100%", height: "3rem" }}
                value={address}
                onChange={(event) => setAddress(event.target.value)}
              />
              <Row>
                <TextField
                  label="Health Insurance Number"
                  id="patientHealthInsuranceNumberInput"
                  sx={{ width: "48%", height: "3rem" }}
                  value={insuranceNumber}
                />
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                  <StyledDesktopDatePicker
                    label="Expired Date"
                    inputFormat="dd/MM/yyyy"
                    value={insuranceExpiryDate}
                    onChange={handleExpiryDateChange}
                    renderInput={(params) => (
                      <TextField
                        sx={{ width: "48%", height: "3rem" }}
                        {...params}
                      />
                    )}
                  />
                </LocalizationProvider>
              </Row>
              <Typography
                color="#081735"
                fontSize={16}
                fontWeight="600"
                padding="8px 0"
                children="PROTECTOR INFO"
              />
              <Row>
                <TextField
                  label="Protector"
                  id="protectorNameInput"
                  sx={{ width: "48%", height: "3rem" }}
                  value={protectorName}
                />
                <TextField
                  label="Relationship"
                  id="protectorRelationshipSelect"
                  select
                  sx={{ width: "48%", height: "3rem" }}
                  value={protectorRelationship}
                  children={relationshipsItems.map((option) => (
                    <MenuItem
                      key={option.value}
                      value={option.value}
                      children={option.label}
                    />
                  ))}
                />
              </Row>
              <Row>
                <TextField
                  label="Phone Number"
                  id="protectorPhoneNumberInput"
                  sx={{ width: "48%", height: "3rem" }}
                  value={protectorPhone}
                />
                <TextField
                  label="Email"
                  id="protectorEmailInput"
                  sx={{ width: "48%", height: "3rem" }}
                  value={protectorEmail}
                />
              </Row>
            </Section>
            {/* End of Left Section: General Info */}
            {/* Right Section: Health Infomation */}
            <Section>
              <Typography
                color="#081735"
                fontSize={16}
                fontWeight="600"
                padding="8px 0"
                children="HEALTH INFORMATION"
              />
              <TextField
                label="Blood group"
                id="bloodGroupSelect"
                select
                sx={{ width: "100%", height: "3rem" }}
                value={bloodGroup}
                children={bloodGroupsItems.map((option) => (
                  <MenuItem
                    key={option.value}
                    value={option.value}
                    children={option.label}
                  />
                ))}
              />
              <FormControl sx={{ width: "100%", height: "3rem" }}>
                <InputLabel id="allergiesSelectLableId" children="Allergies" />
                <Select
                  labelId="allergiesSelectLableId"
                  id="allergiesSelect"
                  multiple
                  value={allergies}
                  onChange={handleAllergiesSelectChange}
                  input={<OutlinedInput label="Allergies" />}
                  sx={{ width: "100%", height: "3rem" }}
                  renderValue={(selected: any) => (
                    <Box sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}>
                      {selected.map((value: any) => (
                        <StyledChip
                          key={value}
                          label={value}
                          onDelete={(e) => deleteAllergySelected(e, value)}
                          deleteIcon={
                            <StyledCloseChipIcon
                              onMouseDown={(event) => {
                                event.stopPropagation();
                              }}
                              style={{
                                color: "white",
                                fontSize: "1.2em",
                                padding: "0",
                              }}
                            />
                          }
                        />
                      ))}
                    </Box>
                  )}
                  children={allergiesItems.map((allergy) => (
                    <MenuItem
                      key={allergy}
                      value={allergy}
                      children={allergy}
                    />
                  ))}
                />
              </FormControl>
              <FormControl sx={{ width: "100%", height: "3rem" }}>
                <InputLabel id="anamnesisSelectLableId">Anamnesis</InputLabel>
                <Select
                  labelId="anamnesisSelectLableId"
                  id="anamnesisSelect"
                  multiple
                  value={anamnesis}
                  onChange={handleAnamnesisSelectChange}
                  input={<OutlinedInput label="Anamnesis" />}
                  sx={{ width: "100%", height: "3rem" }}
                  renderValue={(selected: any) => (
                    <Box sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}>
                      {selected.map((value: any) => (
                        <StyledChip
                          key={value}
                          label={value}
                          onDelete={(e) => deleteAnamnesisSelected(e, value)}
                          deleteIcon={
                            <StyledCloseChipIcon
                              onMouseDown={(event) => {
                                event.stopPropagation();
                              }}
                              style={{
                                color: "white",
                                fontSize: "1.2em",
                                padding: "0",
                              }}
                            />
                          }
                        />
                      ))}
                    </Box>
                  )}
                  children={anamnesisItems.map((anamnesis) => (
                    <MenuItem
                      key={anamnesis}
                      value={anamnesis}
                      children={anamnesis}
                    />
                  ))}
                />
              </FormControl>
            </Section>
            {/* End of right section: Health Infomation */}
          </Body>
        </Container>
      )}
    </>
  );
};
