import {
  Box,
  Button,
  Divider,
  FormControl,
  FormControlLabel,
  InputLabel,
  MenuItem,
  Select,
  Switch,
  Toolbar,
  Typography,
  IconButton,
} from "@mui/material";
import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { Logo } from "../../ui-components/Logo";
import binIcon from "./trash.svg";
import LogoOnly from "./LogoOnly.svg";
import phone from "./phone.svg";
import {
  StyledBodyMainContainer,
  StyledBodyMainContainer1,
  StyledBodyMainContainer2,
  StyledLeftBodyChildContainer,
  StyledLeftBodyChildContainer1,
  StyledRightBodyChildContainer,
  StyledRightBodyChildContainer2,
  StyledRightBodyChildContainer3,
  StyledRightBodyChildContainer4,
  StyledTextField,
} from "./style";
import { useAppDispatch } from "../../store/Store";
import { useSelector } from "react-redux";
import {
  DataSetup,
  getDataSetup,
  selectDataSetup,
  selectLoading,
  updateDataSetup,
} from "../../store/reducers/setup/settingSlice";
import { Footer } from "./footer";
import Loading from "../../components/Loading";

type toolbarStatusType = {
  status: number;
};

export const EnhancedActionToolbar = (props: toolbarStatusType) => {
  const [selectedTab, setSelectedTab] = React.useState<number>(props.status);
  const navigate = useNavigate();

  return (
    <Toolbar
      sx={{
        borderRadius: "8px",
        marginBottom: "16px",
        padding: "12px",
        marginTop: "24px",
        background: "#FFFFFF",
      }}
    >
      <Box sx={{ flexGrow: 1, alignItems: "center" }}>
        <Button
          variant={`${selectedTab === 1 ? "contained" : "text"}`}
          onClick={() => {
            if (selectedTab !== 1) {
              setSelectedTab(1);
              navigate("/admin/setup");
            }
          }}
          key={1}
          sx={{
            padding: "9px 16px",
            borderRadius: "10px",
            mr: 0.5,
            color: `${selectedTab === 1 ? "" : "#4E4E4E"}`,
          }}
        >
          General
        </Button>
        <Button
          variant={`${selectedTab === 2 ? "contained" : "text"}`}
          onClick={() => {
            if (selectedTab !== 2) {
              setSelectedTab(2);
              navigate(`/admin/setup/notification`);
            }
          }}
          key={1}
          sx={{
            padding: "9px 16px",
            borderRadius: "10px",
            mr: 0.5,
            color: `${selectedTab === 2 ? "" : "#4E4E4E"}`,
          }}
        >
          Notification
        </Button>

        <Button
          variant={`${selectedTab === 3 ? "contained" : "text"}`}
          onClick={() => {
            if (selectedTab !== 3) {
              setSelectedTab(3);
              navigate(`/admin/setup/payment`);
            }
          }}
          key={1}
          sx={{
            padding: "9px 16px",
            borderRadius: "10px",
            mr: 0.5,
            color: `${selectedTab === 3 ? "" : "#4E4E4E"}`,
          }}
        >
          Payment
        </Button>

        <Button
          variant={`${selectedTab === 4 ? "contained" : "text"}`}
          onClick={() => {
            if (selectedTab !== 4) {
              setSelectedTab(4);
              navigate("/admin/setup/frontcms");
            }
          }}
          key={1}
          sx={{
            padding: "9px 16px",
            borderRadius: "10px",
            mr: 0.5,
            color: `${selectedTab === 4 ? "" : "#4E4E4E"}`,
          }}
        >
          Front CMS
        </Button>
        <Button
          variant={`${selectedTab === 5 ? "contained" : "text"}`}
          onClick={() => {
            if (selectedTab !== 5) {
              setSelectedTab(5);
              navigate(`/admin/setup/backup`);
            }
          }}
          key={1}
          sx={{
            padding: "9px 16px",
            borderRadius: "10px",
            mr: 0.5,
            color: `${selectedTab === 5 ? "" : "#4E4E4E"}`,
          }}
        >
          Backup / Restore
        </Button>
      </Box>
    </Toolbar>
  );
};

export const Setup: React.FC = () => {
  const dispatch = useAppDispatch();
  const accessToken = localStorage.getItem("access_token");
  const loading = useSelector(selectLoading);
  const dataSetup: DataSetup[] = useSelector(selectDataSetup);

  useEffect(() => {
    dispatch(getDataSetup(accessToken as string));
  }, [accessToken]);

  useEffect(() => {
    setHospitalName(dataSetup.find((x) => x.key === "Hospital Name"));
    setHospitalCode(dataSetup.find((x) => x.key === "Hospital Code"));
    setAddress(dataSetup.find((x) => x.key === "Address"));
    setPhoneNumber(dataSetup.find((x) => x.key === "Phone"));
    setEmail(dataSetup.find((x) => x.key === "Email"));
    setApiUrl(dataSetup.find((x) => x.key === "Mobile App API URL"));
    setPrimaryColor(
      dataSetup.find((x) => x.key === "Mobile App Primary Color Code")
    );
    setSecondColor(
      dataSetup.find((x) => x.key === "Mobile App Secondary Color Code")
    );
    setLanguage(dataSetup.find((x) => x.key === "Language"));
    setLanguageRtl(dataSetup.find((x) => x.key === "Language RTL Text Mode"));
    setTimeZone(dataSetup.find((x) => x.key === "Time Zone"));
    setDateFormat(dataSetup.find((x) => x.key === "Date Format"));
    setCurrency(dataSetup.find((x) => x.key === "Currency"));
    setCurrencySymbol(dataSetup.find((x) => x.key === "Currency Symbol"));
    setCreditLimit(dataSetup.find((x) => x.key === "Credit Limit"));
    setTimeFormat(dataSetup.find((x) => x.key === "Time Format"));
    setDoctorMode(dataSetup.find((x) => x.key === "Doctor Restriction Mode"));
    setSuperadmin(dataSetup.find((x) => x.key === "Superadmin Visibility"));
    setPatientPanel(dataSetup.find((x) => x.key === "Patient Panel"));
  }, [dataSetup]);

  //-------------general setting----------------------
  const [hospitalName, setHospitalName] = useState<DataSetup>();
  const [hospitalCode, setHospitalCode] = useState<DataSetup>();
  const [address, setAddress] = React.useState<DataSetup>();
  const [phoneNumber, setPhoneNumber] = React.useState<DataSetup>();
  const [email, setEmail] = React.useState<DataSetup>();

  //-----------------mobile app----------------
  const [apiUrl, setApiUrl] = React.useState<DataSetup>();
  const [primaryColor, setPrimaryColor] = React.useState<DataSetup>();
  const [secondColor, setSecondColor] = React.useState<DataSetup>();

  //----------------language----------------
  const [language, setLanguage] = React.useState<DataSetup>();
  const [languageRtl, setLanguageRtl] = React.useState<DataSetup>();

  //-------------date time----------------
  const [timeZone, setTimeZone] = React.useState<DataSetup>();
  const [dateFormat, setDateFormat] = React.useState<DataSetup>();

  //----------------currency-------------
  const [currency, setCurrency] = React.useState<DataSetup>();
  const [currencySymbol, setCurrencySymbol] = React.useState<DataSetup>();
  const [creditLimit, setCreditLimit] = React.useState<DataSetup>();
  const [timeFormat, setTimeFormat] = React.useState<DataSetup>();

  //----------------miscellaneous-------------
  const [doctorMode, setDoctorMode] = React.useState<DataSetup>();
  const [superadmin, setSuperadmin] = React.useState<DataSetup>();
  const [patientPanel, setPatientPanel] = React.useState<DataSetup>();

  const updateArray = [
    hospitalName,
    hospitalCode,
    address,
    phoneNumber,
    email,
    apiUrl,
    primaryColor,
    secondColor,
    language,
    languageRtl,
    timeZone,
    dateFormat,
    currency,
    currencySymbol,
    creditLimit,
    timeFormat,
    doctorMode,
    superadmin,
    patientPanel,
  ];

  const handleUpdate = () => {
    dispatch(
      updateDataSetup(accessToken as string, updateArray as DataSetup[])
    );
  };

  return (
    <Box
      sx={{
        position: "relative",
        minHeight: "calc(100vh-100px)",
      }}
    >
      <div className="weight-600 size-24px">Setting</div>
      <EnhancedActionToolbar status={1} />
      {loading && <Loading />}

      {!loading && (
        <div>
          <StyledBodyMainContainer>
            <StyledBodyMainContainer1>
              <StyledLeftBodyChildContainer>
                <Typography
                  sx={{
                    float: "left",
                    lineHeight: "24px",
                    fontWeight: "600",
                    fontSize: "16px",
                    fill: "#081735",
                  }}
                >
                  GENERAL SETTING
                </Typography>
                <StyledTextField
                  InputLabelProps={{
                    shrink: true,
                  }}
                  required
                  id="hospitalName"
                  label="Hospital Name"
                  value={hospitalName?.value}
                  onChange={(e: any) => {
                    setHospitalName({
                      ...hospitalName,
                      value: e.target.value,
                    } as DataSetup);
                  }}
                />
                <StyledTextField
                  InputLabelProps={{
                    shrink: true,
                  }}
                  id="hospitalCode"
                  label="Hospital Code"
                  value={hospitalCode?.value}
                  onChange={(e: any) => {
                    setHospitalCode({
                      ...hospitalCode,
                      value: e.target.value,
                    } as DataSetup);
                  }}
                />
                <StyledTextField
                  InputLabelProps={{
                    shrink: true,
                  }}
                  required
                  id="address"
                  label="Address"
                  value={address?.value}
                  onChange={(e: any) => {
                    setAddress({
                      ...address,
                      value: e.target.value,
                    } as DataSetup);
                  }}
                />
                <StyledTextField
                  InputLabelProps={{
                    shrink: true,
                  }}
                  required
                  id="phone"
                  label="Phone"
                  value={phoneNumber?.value}
                  onChange={(e: any) => {
                    setPhoneNumber({
                      ...phoneNumber,
                      value: e.target.value,
                    } as DataSetup);
                  }}
                />
                <StyledTextField
                  InputLabelProps={{
                    shrink: true,
                  }}
                  required
                  id="email"
                  label="Email"
                  value={email?.value}
                  onChange={(e: any) => {
                    setEmail({
                      ...email,
                      value: e.target.value,
                    } as DataSetup);
                  }}
                />
                <Divider />
                <Box
                  sx={{
                    width: "100%",
                    display: "flex",
                    justifyContent: "space-between",
                  }}
                >
                  <Typography
                    sx={{
                      float: "left",
                      lineHeight: "24px",
                      fontWeight: "600",
                      fontSize: "16px",
                      fill: "#081735",
                      width: "50%",
                    }}
                  >
                    HOSPITAL LOGO
                  </Typography>
                  <Typography
                    sx={{
                      float: "left",
                      lineHeight: "24px",
                      fontWeight: "600",
                      fontSize: "16px",
                      fill: "#081735",
                      width: "50%",
                    }}
                  >
                    HOSPITAL SMALL LOGO
                  </Typography>
                </Box>
                <Box
                  sx={{
                    width: "100%",
                    display: "flex",
                    justifyContent: "space-between",
                  }}
                >
                  <Box
                    sx={{
                      marginLeft: "28px",
                      display: "flex",
                      width: "50%",
                    }}
                  >
                    <Logo variant="primary" size={1.48} />
                    <IconButton sx={{ marginLeft: "10%" }}>
                      <img src={binIcon} />
                    </IconButton>
                  </Box>
                  <Box
                    sx={{
                      display: "flex",
                      width: "50%",
                      marginBottom: "8px",
                      marginRight: "30px",
                    }}
                  >
                    <img src={LogoOnly} />
                    <IconButton sx={{ marginLeft: "5%" }}>
                      <img src={binIcon} />
                    </IconButton>
                  </Box>
                </Box>
              </StyledLeftBodyChildContainer>
              <StyledLeftBodyChildContainer1>
                <Box
                  sx={{
                    width: "100%",
                    display: "flex",
                    justifyContent: "space-between",
                  }}
                >
                  <Typography
                    sx={{
                      float: "left",
                      lineHeight: "24px",
                      fontWeight: "600",
                      fontSize: "16px",
                      fill: "#081735",
                      paddingBottom: "0px",
                      paddingTop: "5px",
                    }}
                  >
                    MOBILE APP
                  </Typography>
                  <Box
                    sx={{
                      marginLeft: "28px",
                      display: "flex",
                      justifyContent: "flex-end",
                      width: "50%",
                    }}
                  >
                    <Box sx={{ marginRight: "5%", paddingTop: "5px" }}>
                      <img src={phone} />
                    </Box>
                    <Typography
                      sx={{
                        lineHeight: "20px",
                        fontWeight: "600",
                        fontSize: "14px",
                        color: "#6C5DD3",
                        fill: "#6C5DD3",
                        paddingBottom: "0px",
                        paddingTop: "5px",
                      }}
                    >
                      Register Your Android App
                    </Typography>
                  </Box>
                </Box>
                <StyledTextField
                  required
                  id="mobileAppURL"
                  label="Mobile App API URL"
                  defaultValue={apiUrl?.value}
                  onChange={(e: any) => {
                    setApiUrl({
                      ...apiUrl,
                      value: e.target.value,
                    } as DataSetup);
                  }}
                />
                <Box
                  sx={{
                    width: "100%",
                    display: "flex",
                    justifyContent: "space-between",
                  }}
                >
                  <StyledTextField
                    InputLabelProps={{
                      shrink: true,
                    }}
                    required
                    id="mobileAppPrimaryColor"
                    label="Mobile App Primary Color Code"
                    value={primaryColor?.value}
                    onChange={(e: any) => {
                      setPrimaryColor({
                        ...primaryColor,
                        value: e.target.value,
                      } as DataSetup);
                    }}
                    sx={{ width: "49%" }}
                  />
                  <StyledTextField
                    InputLabelProps={{
                      shrink: true,
                    }}
                    required
                    id="mobileAppSecondaryColor"
                    label="Mobile App Secondary Color Code"
                    value={secondColor?.value}
                    onChange={(e: any) => {
                      setSecondColor({
                        ...secondColor,
                        value: e.target.value,
                      } as DataSetup);
                    }}
                    sx={{ width: "49%" }}
                  />
                </Box>
                <Divider />
                <Typography
                  sx={{
                    float: "left",
                    lineHeight: "24px",
                    fontWeight: "600",
                    fontSize: "16px",
                    fill: "#081735",
                  }}
                >
                  MOBILE APP LOGO
                </Typography>
                <Box
                  sx={{
                    marginLeft: "28px",
                    display: "flex",
                    width: "50%",
                  }}
                >
                  <Logo variant="primary" size={1.48} />
                  <IconButton sx={{ marginLeft: "10%" }}>
                    <img src={binIcon} />
                  </IconButton>
                </Box>
              </StyledLeftBodyChildContainer1>
            </StyledBodyMainContainer1>

            <StyledBodyMainContainer2>
              <StyledRightBodyChildContainer>
                <Typography
                  sx={{
                    float: "left",
                    lineHeight: "24px",
                    fontWeight: "600",
                    fontSize: "16px",
                    fill: "#081735",
                    paddingBottom: "15px",
                    paddingTop: "5px",
                  }}
                >
                  LANGUAGE
                </Typography>
                <StyledTextField
                  InputLabelProps={{
                    shrink: true,
                  }}
                  required
                  id="language"
                  label="Language"
                  value={language?.value}
                  onChange={(e: any) => {
                    setLanguage({
                      ...language,
                      value: e.target.value,
                    } as DataSetup);
                  }}
                  sx={{ marginTop: "4px" }}
                />
                <FormControlLabel
                  sx={{ marginTop: "8px" }}
                  control={
                    <Switch
                      checked={languageRtl?.value === "false" ? false : true}
                      onChange={(e: any) => {
                        setLanguageRtl({
                          ...languageRtl,
                          value: e.target.checked ? "true" : "false",
                        } as DataSetup);
                      }}
                    />
                  }
                  label="Language RTL Text Mode"
                />
              </StyledRightBodyChildContainer>
              <StyledRightBodyChildContainer2>
                <Typography
                  sx={{
                    float: "left",
                    lineHeight: "24px",
                    fontWeight: "600",
                    fontSize: "16px",
                    fill: "#081735",
                    paddingBottom: "15px",
                    paddingTop: "5px",
                  }}
                >
                  DATE TIME
                </Typography>
                <Box
                  sx={{
                    width: "100%",
                    display: "flex",
                    justifyContent: "space-between",
                  }}
                >
                  <FormControl sx={{ width: "49%" }}>
                    <InputLabel id="dateFormat-select-label" required>
                      Date Format
                    </InputLabel>
                    <Select
                      required
                      labelId="dateFormat-select-label"
                      id="dateFormat-select"
                      label="Date Format"
                      value={dateFormat?.value}
                      defaultValue="dd/mm/yyyy"
                      onChange={(e: any) => {
                        setDateFormat({
                          ...dateFormat,
                          value: e.target.value as string,
                        } as DataSetup);
                      }}
                      sx={{ borderRadius: "12px" }}
                    >
                      <MenuItem value={"dd/mm/yyyy"}>dd/mm/yyyy</MenuItem>
                      <MenuItem value={"dd-mm-yyyy"}>dd-mm-yyyy</MenuItem>
                      <MenuItem value={"mm/dd/yyyy"}>mm/dd/yyyy</MenuItem>
                      <MenuItem value={"yyyy/mm/dd"}>yyyy/mm/dd</MenuItem>
                    </Select>
                  </FormControl>
                  <FormControl sx={{ width: "49%" }}>
                    <InputLabel id="dateFormat-select-label" required>
                      Time Zone
                    </InputLabel>
                    <Select
                      required
                      label="Time Zone"
                      value={timeZone?.value}
                      defaultValue="(GMT) UTC"
                      onChange={(e: any) => {
                        setTimeZone({
                          ...timeZone,
                          value: e.target.value as string,
                        } as DataSetup);
                      }}
                      sx={{ borderRadius: "12px" }}
                    >
                      <MenuItem value={"(GMT) UTC"}>(GMT) UTC</MenuItem>
                      <MenuItem value={"(UTC) GMT"}>(UTC) GMT</MenuItem>
                    </Select>
                  </FormControl>
                </Box>
              </StyledRightBodyChildContainer2>
              <StyledRightBodyChildContainer3>
                <Typography
                  sx={{
                    float: "left",
                    lineHeight: "24px",
                    fontWeight: "600",
                    fontSize: "16px",
                    fill: "#081735",
                    paddingBottom: "15px",
                    paddingTop: "5px",
                  }}
                >
                  CURRENCY
                </Typography>
                <Box
                  sx={{
                    width: "100%",
                    display: "flex",
                    justifyContent: "space-between",
                    marginTop: "0.5rem",
                  }}
                >
                  <FormControl sx={{ width: "49%" }}>
                    <InputLabel required>Currency</InputLabel>
                    <Select
                      required
                      label="Currency"
                      value={currency?.value}
                      defaultValue="USD"
                      onChange={(e: any) => {
                        setCurrency({
                          ...currency,
                          value: e.target.value as string,
                        } as DataSetup);
                      }}
                      sx={{ borderRadius: "12px" }}
                    >
                      <MenuItem value={"USD"}>USD</MenuItem>
                      <MenuItem value={"VND"}>VND</MenuItem>
                      <MenuItem value={"EURO"}>EURO</MenuItem>
                      <MenuItem value={"GBP"}>GBP</MenuItem>
                    </Select>
                  </FormControl>
                  <FormControl sx={{ width: "49%" }}>
                    <InputLabel required>Currency Symbol</InputLabel>
                    <Select
                      required
                      label="Currency Symbol"
                      value={currencySymbol?.value}
                      defaultValue="$"
                      onChange={(e: any) => {
                        setCurrencySymbol({
                          ...currencySymbol,
                          value: e.target.value as string,
                        } as DataSetup);
                      }}
                      sx={{ borderRadius: "12px" }}
                    >
                      <MenuItem value={"$"}>$</MenuItem>
                      <MenuItem value={"€"}>€</MenuItem>
                    </Select>
                  </FormControl>
                </Box>
                <Box
                  sx={{
                    width: "100%",
                    display: "flex",
                    justifyContent: "space-between",
                    marginTop: "2.5rem",
                  }}
                >
                  <FormControl sx={{ width: "49%" }}>
                    <InputLabel required>Credit Limit</InputLabel>
                    <Select
                      required
                      label="Credit Limit"
                      value={creditLimit?.value}
                      defaultValue={20000}
                      onChange={(e: any) => {
                        setCreditLimit({
                          ...creditLimit,
                          value: e.target.value as number,
                        } as DataSetup);
                      }}
                      sx={{ borderRadius: "12px" }}
                    >
                      <MenuItem value={20000}>20000</MenuItem>
                      <MenuItem value={30000}>30000</MenuItem>
                      <MenuItem value={40000}>40000</MenuItem>
                      <MenuItem value={50000}>50000</MenuItem>
                    </Select>
                  </FormControl>
                  <FormControl sx={{ width: "49%" }}>
                    <InputLabel required>Time Format</InputLabel>
                    <Select
                      required
                      label="Time Format"
                      value={timeFormat?.value}
                      defaultValue="12 Hour"
                      onChange={(e: any) => {
                        setTimeFormat({
                          ...timeFormat,
                          value: e.target.value as string,
                        } as DataSetup);
                      }}
                      sx={{ borderRadius: "12px" }}
                    >
                      <MenuItem value={"12 Hour"}>12 Hour</MenuItem>
                      <MenuItem value={"24 Hour"}>24 Hour</MenuItem>
                    </Select>
                  </FormControl>
                </Box>
              </StyledRightBodyChildContainer3>
              <StyledRightBodyChildContainer4>
                <Typography
                  sx={{
                    float: "left",
                    lineHeight: "24px",
                    fontWeight: "600",
                    fontSize: "16px",
                    fill: "#081735",
                    paddingBottom: "15px",
                    paddingTop: "5px",
                  }}
                >
                  MISCELLANEOUS
                </Typography>
                <FormControlLabel
                  sx={{ marginTop: "8px" }}
                  control={
                    <Switch
                      checked={doctorMode?.value === "false" ? false : true}
                      onChange={(e: any) => {
                        setDoctorMode({
                          ...doctorMode,
                          value: e.target.checked ? "true" : "false",
                        } as DataSetup);
                      }}
                    />
                  }
                  label="Doctor Restriction Mode"
                />
                <FormControlLabel
                  sx={{ marginTop: "8px" }}
                  control={
                    <Switch
                      checked={superadmin?.value === "true" ? true : false}
                      onChange={(e: any) => {
                        setSuperadmin({
                          ...superadmin,
                          value: e.target.checked ? "true" : "false",
                        } as DataSetup);
                      }}
                    />
                  }
                  label="Superadmin Visibility"
                />
                <FormControlLabel
                  sx={{ marginTop: "8px" }}
                  control={
                    <Switch
                      checked={patientPanel?.value === "true" ? true : false}
                      onChange={(e: any) => {
                        setPatientPanel({
                          ...patientPanel,
                          value: e.target.checked ? "true" : "false",
                        } as DataSetup);
                      }}
                    />
                  }
                  label="Patient Panel"
                />
              </StyledRightBodyChildContainer4>
            </StyledBodyMainContainer2>
          </StyledBodyMainContainer>
          <div onClick={handleUpdate}>
            <Footer label="Save General Setting" />
          </div>
        </div>
      )}
    </Box>
  );
};
