import { styled as muiStyled, TextField } from '@mui/material';
export const StyledLeftBodyChildContainer = muiStyled('div')(({}) => ({
    marginTop: '5px',
    paddingTop: '0.5rem',
    paddingBottom: '1rem',
    paddingLeft: '1rem',
    paddingRight: '1rem',
    width: '100%',
    height: '40rem',
    backgroundColor: '#FFFFFF',
    borderRadius: '1rem',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between'
  }));
  export const StyledLeftBodyChildContainer1 = muiStyled('div')(({}) => ({
    marginTop: '1.5rem',
    paddingTop: '0.5rem',
    paddingBottom: '1rem',
    paddingLeft: '1rem',
    paddingRight: '1rem',
    width: '100%',
    height: '23rem',
    backgroundColor: '#FFFFFF',
    borderRadius: '1rem',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between'
  }));
export const StyledRightBodyChildContainer = muiStyled('div')(({}) => ({
  marginTop: '5px',
  paddingTop: '0.5rem',
  paddingBottom: '1rem',
  paddingLeft: '1rem',
  paddingRight: '1rem',
  width: '100%',
  height: '12rem',
  backgroundColor: '#FFFFFF',
  borderRadius: '1rem',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'flex-start'
}));
  
export const StyledRightBodyChildContainer2 = muiStyled('div')(({}) => ({
  marginTop: '1.5rem',
  paddingTop: '0.5rem',
  paddingBottom: '1rem',
  paddingLeft: '1rem',
  paddingRight: '1rem',
  width: '100%',
  height: '9rem',
  backgroundColor: '#FFFFFF',
  borderRadius: '1rem',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'flex-start'
}));

export const StyledRightBodyChildContainer3 = muiStyled('div')(({}) => ({
  marginTop: '1.5rem',
  paddingTop: '0.5rem',
  paddingBottom: '1rem',
  paddingLeft: '1rem',
  paddingRight: '1rem',
  width: '100%',
  height: '16rem',
  backgroundColor: '#FFFFFF',
  borderRadius: '1rem',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'flex-start'
}));

export const StyledRightBodyChildContainer4 = muiStyled('div')(({}) => ({
  marginTop: '1.5rem',
  paddingTop: '0.5rem',
  paddingBottom: '1rem',
  paddingLeft: '1rem',
  paddingRight: '1rem',
  width: '100%',
  height: '23rem',
  backgroundColor: '#FFFFFF',
  borderRadius: '1rem',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'flex-start'
}));  
  
export const StyledBodyMainContainer2 = muiStyled('div')(({}) => ({
  margin: '0',
  padding: '0',
  width: '49.5%',
  height: '100%',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-between'
}));

export const StyledBodyMainContainer1 = muiStyled('div')(({}) => ({
  margin: '0',
  padding: '0',
  width: '49.5%',
  height: '100%',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-between'
}));

export const StyledBodyMainContainer = muiStyled('div')(({}) => ({
    margin: '0',
    padding: '0',
    width: '100%',
    height: '100%',
    display: 'flex',
    justifyContent: 'space-between'
}));

export const StyledTextField = muiStyled(TextField)`
  & .MuiOutlinedInput-root {
    color: #131626;
    height: 100%;
    & fieldset {
      border: 1px solid #EAEAEA;
      border-radius: 0.8rem;
      padding: 0;
      margin: 0
    }

    & input {
        color: #131626;
        font-weight : 500;
        font-size: 16px;
    }
    &:hover fieldset {
      border: 1px solid #EAEAEA;       
    }

    &.Mui-focused {
        color: #131626;
    }

    &.Mui-focused fieldset {
      border: 1px solid #131626;
    }
  }
  & .MuiFormLabel-root {
    color: #131626;
    font-weight : 500;
    font-size: 16px;
  }
`;  