import { Create, Delete } from "@mui/icons-material";
import { Box, IconButton, Typography } from "@mui/material";
import React from "react";
import { Link } from "react-router-dom";
import { DeleteDialog } from "../../../components/DeleteDialog";
import { RoundedButton } from "../../../components/RoundedButton";

import { CustomTable } from "../../../components/Table/Table";
import { HeadCell } from "../../../components/Table/types";
import { notificationTemplateService } from "../../../services/agent";
import { EnhancedActionToolbar } from "../Setup";
import { NotificationEditModal } from "./NotificationEdit";
export interface NotificationTemplates {
  id?: string;
  category?: string;
  channel?: string;
  value?: string;
  description?: string;
}

const headCells: HeadCell[] = [
  {
    id: "number",
    numeric: true,
    label: "No.",
  },
  {
    id: "category",
    numeric: false,
    label: "Event",
  },
  {
    id: "channel",
    numeric: false,
    label: "Option",
  },
  {
    id: "value",
    numeric: false,
    label: "Value",
  },
  {
    id: "description",
    numeric: false,
    label: "Sample Message",
  },
  {
    id: "action",
    numeric: false,
    label: "Action",
  },
];

export const NotificationSetup: React.FC = () => {
  const [isLoading, setIsLoading] = React.useState(false);
  const [data, setData] = React.useState<NotificationTemplates[]>([]);
  const [page, setPage] = React.useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = React.useState<number>(10);
  const [rowsCount, setRowsCount] = React.useState<number>(0);
  const [deletePopUpOpen, setDeletePopUpOpen] = React.useState(false);
  const [currentSelectedId, setCurrentSelectedId] = React.useState("");
  const [openEdit, setOpenEdit] = React.useState<boolean>(false);
  const getNotificationTemplateList = async () => {
    const params = {
      page: page + 1,
      size: rowsPerPage,
    };
    setIsLoading(true);
    try {
      let { data, paging } = await notificationTemplateService.getList({
        params,
      });
      setData(data);
      setRowsCount(paging.total);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      console.log("getNotificationTemplate error: ", error);
    }
  };
  const deleteNotificationTemplate = async (id: string) => {
    setIsLoading(true);
    try {
      let response = await notificationTemplateService.delete(id);
      setIsLoading(false);
      return response;
    } catch (error) {
      setIsLoading(false);
      console.log("deleteNotificationTemplate error: ", error);
    }
  };
  const handleChangePage = (event: any, newPage: number) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event: any) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleDelete = (id: string) => {
    setDeletePopUpOpen(true);
    setCurrentSelectedId(id);
  };
  const handleCancelDelete = () => {
    setDeletePopUpOpen(false);
  };
  const handleConfirmDelete = () => {
    setDeletePopUpOpen(false);
    deleteNotificationTemplate(currentSelectedId).then((res) => {
      setCurrentSelectedId("");
      getNotificationTemplateList();
    });
  };

  React.useEffect(() => {
    let totalPages = Math.ceil(rowsCount / rowsPerPage);
    let currentPage = page + 1;
    if (currentPage > totalPages && totalPages > 0) {
      setPage(totalPages - 1);
    }
  }, [rowsCount]);

  React.useEffect(() => {
    getNotificationTemplateList();
    console.log(process.env);
  }, [rowsPerPage, page]);

  const mapDataToRender = () => {
    const render = data.map((item, index) => ({
      id: item.id as string,
      number: index + 1,
      category: (
        <Typography
          textTransform="capitalize"
          children={item.category || "-"}
        />
      ),
      channel: (
        <Typography textTransform="capitalize" children={item.channel || "-"} />
      ),
      value: (
        <Typography children={item.value || "-"} />
      ),
      description: item.description || "-",
      action: (
        <Box style={{ display: "flex", textAlign: "center" }}>
          <Link
            to="#"
            onClick={() => {
              setCurrentSelectedId(item.id as string);
              setOpenEdit(true);
            }}
            children={<IconButton children={<Create />} />}
          />
          <Link
            to={`#`}
            onClick={() => {
              handleDelete(item.id as string);
            }}
            children={<IconButton children={<Delete />} />}
          />
        </Box>
      ),
    }));
    return render;
  };

  return (
    <Box>
      <DeleteDialog
        open={deletePopUpOpen}
        onClose={handleCancelDelete}
        onConfirm={handleConfirmDelete}
        onCancel={handleCancelDelete}
      />
      <div className="weight-600 size-24px">Setting</div>
      <EnhancedActionToolbar status={2} />
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          width: "100%",
          padding: "20px",
          backgroundColor: "white",
          borderRadius: "16px",
        }}
      >
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            gap: "8px",
          }}
        >
          <Typography variant="h2" children={"NOTIFICATION TEMPLATES"} />
          <RoundedButton
            variant="contained"
            onClick={() => {
              setCurrentSelectedId("");
              setOpenEdit(true);
            }}
            children="Add Notification Template"
          />
        </Box>
        <NotificationEditModal
          open={openEdit}
          setOpen={setOpenEdit}
          notiSetup={data.find((x) => x.id === currentSelectedId)}
          setLoading={setIsLoading}
          getNotificationTemplateList={getNotificationTemplateList}
        />
        <CustomTable
          headcells={headCells}
          body={mapDataToRender()}
          paging={{
            page: page,
            rowsPerPage: rowsPerPage,
            rowsCount: rowsCount,
          }}
          loading={isLoading}
          toolbar={false}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Box>
    </Box>
  );
};
