import { Button } from "@mui/material";
import Backdrop from "@mui/material/Backdrop";
import Box from "@mui/material/Box";
import Fade from "@mui/material/Fade";
import FormControl from "@mui/material/FormControl";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormLabel from "@mui/material/FormLabel";
import Modal from "@mui/material/Modal";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import TextField from "@mui/material/TextField";
import { styled } from "@mui/system";
import * as React from "react";
import { notificationTemplateService } from "../../../services/agent";
import { NotificationTemplates } from "./NotificationSetup";

const style = {
  display: "flex",
  flexDirection: "column",
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 1200,
  background: "rgba(255, 255, 255, 1)",
  borderRadius: "16px",
  padding: "30px",
};
interface Props {
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  notiSetup?: NotificationTemplates;
  setLoading: React.Dispatch<React.SetStateAction<boolean>>;
  getNotificationTemplateList: () => void;
}

export const NotificationEditModal: React.FC<Props> = ({
  open,
  setOpen,
  notiSetup,
  setLoading,
  getNotificationTemplateList,
}) => {
  const [description, setDescription] = React.useState(notiSetup?.description);
  const [category, setCategory] = React.useState(notiSetup?.category);
  const [channel, setChannel] = React.useState(notiSetup?.channel);
  const [value, setValue] = React.useState(notiSetup?.value);
  const handleDescriptionChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setDescription(event.target.value);
  };
  const handleCategoryChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setCategory(event.target.value);
  };
  const handleChannelChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setChannel(event.target.value);
  };
  const handleValueChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue(event.target.value);
  };
  const handleSubmit = () => {
    const newNotification = {
      id: notiSetup?.id as string,
      category: category as string,
      channel: channel as string,
      value: notiSetup?.value as string,
      description: description as string,
    };
    setLoading(true);

    if (newNotification.id) {
      notificationTemplateService
        .patch(newNotification)
        .then(() => {
          setOpen(false);
          getNotificationTemplateList();
        })
        .catch((err) => console.log(err));
    } else {
      notificationTemplateService
        .add(newNotification)
        .then(() => {
          setOpen(false);
          getNotificationTemplateList();
        })
        .catch((err) => console.log(err));
    }
  };
  const CSSButton = styled(Button)({
    background: "rgba(108, 93, 211, 1)",
    color: "white",
    padding: "15px 0",
    borderRadius: "100px",
    marginTop: "24px",
    "&:hover": {
      background: "rgba(108, 93, 211, 1)",
      color: "white",
      cursor: "pointer",
    },
  });

  React.useEffect(() => {
    setDescription(notiSetup?.description);
    setCategory(notiSetup?.category);
    setChannel(notiSetup?.channel);
    setValue(notiSetup?.value);
  }, [notiSetup]);

  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={() => setOpen(false)}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 300,
        }}
      >
        <Fade in={open}>
          <Box sx={style}>
            <span
              style={{
                fontWeight: 600,
                color: "rgba(19, 22, 38, 1)",
                fontSize: "18px",
              }}
            >
              {notiSetup?.id
                ? "Edit Notification Template"
                : "Add Notification Template"}
            </span>
            <FormControl>
              <TextField
                id="outlined-multiline-flexible"
                label="Event..."
                value={category}
                onChange={handleCategoryChange}
                fullWidth
                sx={{ marginTop: "16px" }}
              />
              <TextField
                id="outlined-multiline-flexible"
                label="Value..."
                value={value}
                onChange={handleValueChange}
                fullWidth
                sx={{ marginTop: "16px" }}
              />
              <FormLabel
                id="edit-notification-label"
                sx={{
                  margin: "10px 0",
                  fontWeight: 600,
                  color: "rgba(19, 22, 38, 1)",
                  fontSize: "16px",
                }}
              >
                Option
              </FormLabel>
              <RadioGroup
                aria-labelledby="edit-notification-label"
                defaultValue=""
                name="radio-buttons-group"
                value={channel?.toLowerCase()}
                onChange={handleChannelChange}
              >
                <FormControlLabel
                  value="email"
                  control={<Radio />}
                  label="Email"
                />
                <FormControlLabel value="sms" control={<Radio />} label="SMS" />
                <FormControlLabel
                  value="mobile app"
                  control={<Radio />}
                  label="Mobile App"
                />
              </RadioGroup>
              <TextField
                id="outlined-multiline-flexible"
                label="Sample Message..."
                multiline
                maxRows={4}
                value={description}
                onChange={handleDescriptionChange}
                fullWidth
                sx={{
                  marginTop: "16px",
                  "& textarea": { height: "200px !important" },
                }}
              />
            </FormControl>
            <CSSButton onClick={handleSubmit}>Confirm</CSSButton>
          </Box>
        </Fade>
      </Modal>
    </div>
  );
};
