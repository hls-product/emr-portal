import React, { useState } from "react";
import {
  Grid,
  Typography,
  Button,
  Paper,
  Divider,
  TextField,
} from "@mui/material";
import { EnhancedActionToolbar } from "./Setup";
import { Footer } from "./footer";

import paypal from "../../assets/payment/paypal.svg";
import stripe from "../../assets/payment/stripe.svg";
import paytm from "../../assets/payment/paytm.svg";
import flutter from "../../assets/payment/flutter.svg";
import amazon from "../../assets/payment/amazon.svg";
import googlewallet from "../../assets/payment/googlewallet.svg";
import jcb from "../../assets/payment/jcb.svg";
import moneygram from "../../assets/payment/moneygram.svg";
import payza from "../../assets/payment/payza.svg";
import cirrus from "../../assets/payment/cirrus.svg";
import western from "../../assets/payment/western.svg";
import webmoney from "../../assets/payment/webmoney.svg";
import master from "../../assets/payment/master.svg";

interface PaymentType {
  id: string;
  label: string;
  icon: string;
}

const PaymentList: PaymentType[] = [
  {
    id: "paypal",
    label: "Paypal",
    icon: paypal,
  },
  {
    id: "stripe",
    label: "Stripe",
    icon: stripe,
  },
  {
    id: "paytm",
    label: "Paytm",
    icon: paytm,
  },
  {
    id: "flutterWavel",
    label: "Flutter Wavel",
    icon: flutter,
  },
  {
    id: "amazone",
    label: "Amazon",
    icon: amazon,
  },
  {
    id: "googleWallet",
    label: "Google Wallet",
    icon: googlewallet,
  },
  {
    id: "jcb",
    label: "JCB",
    icon: jcb,
  },
  {
    id: "moneygram",
    label: "MoneyGram",
    icon: moneygram,
  },
  {
    id: "payza",
    label: "Payza",
    icon: payza,
  },
  {
    id: "cirrus",
    label: "Cirrus",
    icon: cirrus,
  },
  {
    id: "western",
    label: "Western Union",
    icon: western,
  },
  {
    id: "webmoney",
    label: "WebMoney",
    icon: webmoney,
  },
  {
    id: "master",
    label: "MasterCard",
    icon: master,
  },
];

const RenderPaymentMethods = () => {
  const [isActive, setIsActive] = useState("paypal");

  const handleSelect = (id: string) => {
    setIsActive(id);
  };
  return (
    <>
      {PaymentList.map(({ id, label, icon }) => (
        <Button
          key={id}
          style={
            isActive === id ? { color: "white", background: " #6C5DD3" } : {}
          }
          sx={{
            border: "1px solid #EAEAEA",
            borderRadius: "21px",
            height: "38px",
            marginRight: "8px",
            marginBottom: "8px",
            background: "white",
            color: "black",
            padding: "9px 12px",
          }}
          onClick={() => handleSelect(id)}
        >
          <img
            src={icon}
            style={{
              marginRight: "6px",
              width: "20px",
              height: "20px",
              borderRadius: "50%",
              background: "white",
              color: "black",
              border: "1px solid #EAEAEA",
            }}
          />

          {label}
        </Button>
      ))}
    </>
  );
};

export const Payment: React.FC = () => {
  return (
    <div
      style={{
        position: "relative",
        height: "100vh",
      }}
    >
      <Typography sx={{ fontWeight: 600, fontSize: "24px" }}>
        Setting
      </Typography>
      <EnhancedActionToolbar status={3} />
      <Grid container spacing={2}>
        <Grid item xs={6}>
          <Paper sx={{ padding: "1rem", borderRadius: "16px" }}>
            <Typography
              sx={{
                textTransform: "uppercase",
                fontSize: "16px",
                fontWeight: 600,
                marginBottom: "16px",
                color: "#081735",
              }}
            >
              Payment methods
            </Typography>

            <RenderPaymentMethods />

            <Divider sx={{ color: "f4f4f4", marginTop: "16px" }} />

            <TextField
              required
              label="Paypal Username"
              defaultValue="Enter"
              fullWidth
              sx={{ marginTop: "26px" }}
              InputProps={{
                style: {
                  height: "48px",
                  borderRadius: "12px",
                  color: "#9fa0a6",
                },
              }}
              InputLabelProps={{
                style: {
                  color: "black",
                  fontWeight: 500,
                  fontSize: "18px",
                },
              }}
            />

            <TextField
              required
              label="Paypal Password"
              defaultValue="Enter"
              fullWidth
              sx={{ marginTop: "26px" }}
              InputProps={{
                style: {
                  height: "48px",
                  borderRadius: "12px",
                  color: "#9fa0a6",
                },
              }}
              InputLabelProps={{
                style: {
                  color: "black",
                  fontWeight: 500,
                  fontSize: "18px",
                },
              }}
            />

            <TextField
              required
              label="Paypal Signature"
              defaultValue="Enter"
              fullWidth
              sx={{ marginTop: "26px" }}
              InputProps={{
                style: {
                  height: "48px",
                  borderRadius: "12px",
                  color: "#9fa0a6",
                },
              }}
              InputLabelProps={{
                style: {
                  color: "black",
                  fontWeight: 500,
                  fontSize: "18px",
                },
              }}
            />
          </Paper>
        </Grid>

        <Grid item xs={6}>
          <Paper sx={{ padding: "1rem", borderRadius: "16px" }}>
            <Typography
              sx={{
                textTransform: "uppercase",
                fontSize: "16px",
                fontWeight: 600,
                marginBottom: "16px",
                color: "#081735",
              }}
            >
              Select Payment Gateway
            </Typography>
            <RenderPaymentMethods />
          </Paper>
        </Grid>
      </Grid>

      <Footer label="Save Payment Setting" />
    </div>
  );
};
