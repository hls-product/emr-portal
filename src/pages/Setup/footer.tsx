import React from 'react';
import {
    Box,
    Button,
    Checkbox,
    FormControlLabel,
    IconButton,
    InputAdornment,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TablePagination,
    TableRow,
    TextField,
    Typography,
  } from "@mui/material";
import { styled as muiStyled } from '@mui/material';
import { colorPalette } from '../../theme/ColorPalette';

interface ButtonProps {
    label?: string
  }

export const Footer: React.FC<ButtonProps> = ({ label}) => {
    return(
        <footer>
            <Box 
                sx={{
                    width:'100%', 
                    height:'84px',
                    display:'flex',
                    justifyContent:'flex-end',
                    alignItems:'center'
                    }}>
                <Button variant='contained' sx={{ borderRadius:'99px', marginRight:'20px', height:'40px' }}>{label}</Button>
            </Box>
        </footer>
    )
};

