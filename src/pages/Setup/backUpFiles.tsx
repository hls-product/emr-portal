import React, { useMemo } from "react";
import {
  Box,
  Button,
  Checkbox,
  Divider,
  FormControl,
  FormControlLabel,
  IconButton,
  Input,
  InputAdornment,
  InputLabel,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  TextField,
  Typography,
  useTheme,
} from "@mui/material";
import {
  KeyboardArrowLeft,
  KeyboardArrowRight,
  Visibility,
  VisibilityOff,
} from "@mui/icons-material";

import FirstPageIcon from "@mui/icons-material/FirstPage";
import LastPageIcon from "@mui/icons-material/LastPage";
import { EnhancedActionToolbar } from "./Setup";
import editIcon from "./edit.svg";
import { colorPalette } from "./../../theme/ColorPalette";
import { ReactComponent as DeleteIcon } from "./trash.svg";
import { ReactComponent as Download } from "./downloadIcon.svg";
import { ReactComponent as BackUp } from "./revertIcon.svg";
import { ReactComponent as DropFile } from "./dropFileIcon.svg";
import { Footer } from "./footer";

import Dropzone, { useDropzone } from "react-dropzone";

interface TablePaginationActionsProps {
  count: number;
  page: number;
  rowsPerPage: number;
  onPageChange: (
    event: React.MouseEvent<HTMLButtonElement>,
    newPage: number
  ) => void;
}

export function TablePaginationActions(props: TablePaginationActionsProps) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box
      sx={{
        display: "flex",
        alignItems: "baseline",
        justifyContent: "center",
        flexShrink: 2,
        ml: 2.5,
      }}
    >
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowRight />
        ) : (
          <KeyboardArrowLeft />
        )}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowLeft />
        ) : (
          <KeyboardArrowRight />
        )}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
}

const backupFile = new Array(15).fill({
  name: "db_ver_4.0_2022-04-16_15-07-02.sql",
  time: "20/11/2019 17:00:56",
});

export function BackUpTable() {
  const [dense, setDense] = React.useState<boolean>(false);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const handleChangePage = (
    event: React.MouseEvent<HTMLButtonElement> | null,
    newPage: number
  ) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  return (
    <Box sx={{ width: "100%" }}>
      <TableContainer>
        <Table size={dense ? "small" : "medium"}>
          <TableHead>
            <TableRow sx={{ backgroundColor: "#FAFAFA", stroke: "#EAEAEA" }}>
              <TableCell align="left">Name</TableCell>
              <TableCell align="left">Time</TableCell>
              <TableCell align="center">Action</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {backupFile
              .map((row) => (
                <TableRow key={row.no}>
                  <TableCell component="th" scope="row">
                    {row.name}
                  </TableCell>
                  <TableCell align="left">
                    <Typography
                      sx={{
                        display: "block",
                        fontSize: "14px",
                        fontWeight: "500",
                        lineHeight: "18px",
                        color: "#4E4E4E",
                      }}
                    >
                      {row.time}
                    </Typography>
                  </TableCell>

                  <TableCell align="center">
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                      }}
                    >
                      <BackUp style={{ cursor: "pointer" }} />
                      <Download style={{ cursor: "pointer" }} />
                      <DeleteIcon style={{ cursor: "pointer" }} />
                    </div>
                  </TableCell>
                </TableRow>
              ))
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)}
          </TableBody>

          <TablePagination
            rowsPerPageOptions={[5, 10, 25, { label: "All", value: -1 }]}
            count={backupFile.length}
            rowsPerPage={rowsPerPage}
            page={page}
            SelectProps={{
              inputProps: {
                "aria-label": "rows per page",
              },
              native: true,
            }}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
            ActionsComponent={TablePaginationActions}
            sx={{
              backgroundColor: "#FAFAFA",
            }}
          />
        </Table>
      </TableContainer>
    </Box>
  );
}

export function DropZone(props: any) {
  return (
    <Dropzone onDrop={(acceptedFiles) => console.log(acceptedFiles)}>
      {({ getRootProps, getInputProps }) => (
        <section>
          <div {...getRootProps({})}>
            <input {...getInputProps()} />
            <Box
              sx={{
                display: "flex",
                width: "100%",
                height: "201px",
                border: "1px dashed #EAEAEA",
                borderRadius: "12px",
                justifyContent: "center",
                alignItems: "center",
                cursor: "pointer",
              }}
            >
              <Button
                variant="outlined"
                component="label"
                startIcon={<DropFile />}
                sx={{
                  width: "50%",
                  height: "56px",
                  borderRadius: "99px",
                  fontSize: "14px",
                  fontWeight: "600",
                }}
              >
                Drop a file here or click
              </Button>
            </Box>
          </div>
        </section>
      )}
    </Dropzone>
  );
}
export const BackUpFiles: React.FC = () => {
  const [cronKey, setCronKey] = React.useState({
    password: "t7&hUsa*********************",
    showPassword: false,
  });

  const handleClickShowPassword = () => {
    setCronKey({ ...cronKey, showPassword: !cronKey.showPassword });
  };

  const handleMouseDownPassword = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    event.preventDefault();
  };

  return (
    <>
      <Box
        sx={{
          minHeight: "calc(100vh-100px)",
        }}
      >
        <div className="weight-600 size-24px">Setting</div>
        <EnhancedActionToolbar status={5} />

        <Box
          sx={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <Box
            sx={{
              width: "59%",
              height: "500px",
              background: "white",
              borderRadius: "16px",
            }}
          >
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                margin: "24px 16px",
                justifyContent: "center",
              }}
            >
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  marginBottom: "24px",
                }}
              >
                <Typography variant="h6" sx={{ textTransform: "uppercase" }}>
                  Backup File LIST
                </Typography>
                <Button
                  variant="outlined"
                  sx={{
                    display: "inline",
                    width: "167px",
                    height: "40px",
                    borderRadius: "99px",
                    border: "#F4F4F4",
                  }}
                >
                  Create Backup File
                </Button>
              </Box>

              <BackUpTable />
            </Box>
          </Box>

          <Box
            sx={{
              width: "39%",
              height: "500px",
              background: "white",
              borderRadius: "16px",
            }}
          >
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                margin: "16px",
                justifyContent: "center",
                gap: "16px",
              }}
            >
              <Typography variant="h6" sx={{ textTransform: "uppercase" }}>
                Upload From Local Directory
              </Typography>

              <DropZone />

              {/* <Button variant="outlined" component="label" startIcon={<DropFile/>}
                        sx={{
                            width:'50%',
                            height:'56px',
                            borderRadius:'99px',
                            fontSize:'14px',
                            fontWeight:'600'
                        }}
                    >
                        Drop a file here or click
                        <input hidden accept="" multiple type="file" />
                    </Button> */}

              <Divider />

              <Typography variant="h6" sx={{ textTransform: "uppercase" }}>
                Cron Secret Key
              </Typography>
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "space-between",
                }}
              >
                <FormControl sx={{ width: "100%" }} variant="standard">
                  <Input
                    disableUnderline={true}
                    readOnly={true}
                    type={cronKey.showPassword ? "text" : "password"}
                    value={cronKey.password}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                        >
                          {cronKey.showPassword ? (
                            <VisibilityOff />
                          ) : (
                            <Visibility />
                          )}
                        </IconButton>
                      </InputAdornment>
                    }
                    inputProps={{
                      style: {
                        color: "black",
                        border: "white",
                        cursor: "default",
                      },
                    }}
                  />
                </FormControl>
                {/* <Typography sx={{fontSize:'14px', fontWeight:'600'}}>{cronKey.password}</Typography>

                        <IconButton
                            onClick={handleClickShowPassword}
                            onMouseDown={handleMouseDownPassword}
                            >
                            {cronKey.showPassword ? <Visibility /> : <VisibilityOff />}
                        </IconButton> */}
              </Box>

              <Button
                variant="outlined"
                sx={{
                  width: "100%",
                  height: "56px",
                  border: "1px solid #F4F4F4",
                  borderRadius: "99px",
                }}
              >
                Generate
              </Button>
            </Box>
          </Box>
        </Box>
      </Box>
      <Footer label="Save Backup / Restore Setting" />
    </>
  );
};
