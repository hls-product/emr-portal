import React from "react";
import {
  Box,
  Checkbox,
  Divider,
  FormControlLabel,
  IconButton,
  InputAdornment,
  Switch,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  TextField,
  Typography,
  Link,
} from "@mui/material";
import { Circle, Create, Image, Visibility } from "@mui/icons-material";
import { EnhancedActionToolbar } from "./Setup";
import editIcon from "./edit.svg";
import { colorPalette } from "./../../theme/ColorPalette";
import { Logo } from "../../ui-components/Logo";
import binIcon from "./trash.svg";
import LogoOnly from "./LogoOnly.svg";
import { Footer } from "./footer";

export const FrontCMS: React.FC = () => {
  const [facebook, setFacebook] = React.useState(
    "https://www.facebook.com/login"
  );
  const handleChangeFacebook = (event: React.ChangeEvent<HTMLInputElement>) => {
    setFacebook(event.target.value);
  };

  const [twitter, setTwitter] = React.useState("https://www.twitter.com/login");
  const handleChangeTwitter = (event: React.ChangeEvent<HTMLInputElement>) => {
    setTwitter(event.target.value);
  };

  const [youtube, setYoutube] = React.useState("https://www.youtube.com/login");
  const handleChangeYoutube = (event: React.ChangeEvent<HTMLInputElement>) => {
    setYoutube(event.target.value);
  };

  const [linkedin, setLinkedin] = React.useState(
    "https://www.linkedin.com/login"
  );
  const handleChangeLinkedin = (event: React.ChangeEvent<HTMLInputElement>) => {
    setLinkedin(event.target.value);
  };

  const [instagram, setInstagram] = React.useState(
    "https://www.instagram.com/login"
  );
  const handleChangeInstagram = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setInstagram(event.target.value);
  };

  const [pinterest, setPinterest] = React.useState(
    "https://www.pinterest.com/login"
  );
  const handleChangePinterest = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setPinterest(event.target.value);
  };
  const [googlePlus, setGooglePlus] = React.useState(
    "https://www.googleplus.com/login"
  );
  const handleChangeGooglePlus = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setGooglePlus(event.target.value);
  };

  return (
    <>
      <Box
        sx={{
          minHeight: "calc(100vh-100px)",
        }}
      >
        <div className="weight-600 size-24px">Setting</div>
        <EnhancedActionToolbar status={4} />

        <Box
          sx={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <Box
            sx={{
              width: "49%",
              height: "699px",
              background: "white",
              borderRadius: "16px",
            }}
          >
            <Box sx={{ margin: "16px" }}>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "center",
                  gap: "18px",
                  marginBottom: "16px",
                }}
              >
                <Typography variant="h6" sx={{ textTransform: "uppercase" }}>
                  Front CMS Setting
                </Typography>

                <FormControlLabel
                  control={<Switch defaultChecked />}
                  label="Front CMS"
                />
                <FormControlLabel
                  control={<Switch defaultChecked />}
                  label="Online Appointment"
                />
                <FormControlLabel control={<Switch />} label="Sidebar" />
                <FormControlLabel
                  control={<Switch />}
                  label="Language RTL Text Mode"
                />
              </Box>
              <Divider />

              <Box
                sx={{
                  width: "100%",
                  display: "flex",
                  justifyContent: "space-between",
                  marginTop: "16px",
                }}
              >
                <Typography
                  sx={{
                    float: "left",
                    lineHeight: "24px",
                    fontWeight: "700",
                    fontSize: "16px",
                    textTransform: "uppercase",
                    fill: "#081735",
                    width: "50%",
                  }}
                >
                  Logo (369px X 76px)
                </Typography>
                <Typography
                  sx={{
                    float: "left",
                    lineHeight: "24px",
                    fontWeight: "700",
                    fontSize: "16px",
                    textTransform: "uppercase",
                    fill: "#081735",
                    width: "50%",
                  }}
                >
                  Favicon (32px X 32px)
                </Typography>
              </Box>

              <Box
                sx={{
                  width: "100%",
                  display: "flex",
                  justifyContent: "flex-start",
                  margin: "16px 0px",
                }}
              >
                <Box
                  sx={{
                    marginLeft: "28px",
                    display: "flex",
                    justifyContent: "flex-start",
                    gap: "35px",
                    width: "50%",
                  }}
                >
                  <Logo variant="primary" size={1.5} />
                  <IconButton sx={{}}>
                    <img src={binIcon} />
                  </IconButton>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "flex-start",
                    gap: "10px",
                    width: "50%",
                  }}
                >
                  <img src={LogoOnly} />
                  <IconButton sx={{ marginRight: "17rem" }}>
                    <img src={binIcon} />
                  </IconButton>
                </Box>
              </Box>
              <Divider />

              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  width: "100%",
                  margin: "26px 0px",
                  gap: "16px",
                }}
              >
                <TextField
                  label="Footer Text"
                  defaultValue="© Akametal 2020"
                  InputProps={{
                    style: {
                      height: "48px",
                      borderRadius: "12px",
                      color: "black",
                    },
                  }}
                  InputLabelProps={{
                    style: {
                      color: "black",
                    },
                  }}
                  sx={{ width: "100%" }}
                />

                <TextField
                  label="Google Analytics"
                  defaultValue='<script async
                                src="https://www.googletagmanager.com/gtag/js?
                                id=GA_TRACKING_ID"></script>
                                <script>'
                  multiline
                  InputProps={{
                    style: {
                      height: "104px",
                      borderRadius: "12px",
                      color: "black",
                    },
                  }}
                  InputLabelProps={{
                    style: {
                      color: "black",
                    },
                  }}
                  sx={{ width: "100%" }}
                />
              </Box>
            </Box>
          </Box>

          <Box
            sx={{
              width: "49%",
              height: "699px",
              background: "white",
              borderRadius: "16px",
            }}
          >
            <Box
              sx={{
                margin: "16px",
                display: "flex",
                flexDirection: "column",
                gap: "26px",
              }}
            >
              <Typography variant="h6" sx={{ textTransform: "uppercase" }}>
                social link
              </Typography>

              <TextField
                value={facebook}
                label="Facebook URL"
                onChange={handleChangeFacebook}
                InputProps={{
                  style: {
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  },
                }}
                InputLabelProps={{
                  style: {
                    color: "black",
                  },
                }}
                sx={{ width: "100%" }}
              />

              <TextField
                value={twitter}
                label="Twitter URL"
                onChange={handleChangeTwitter}
                InputProps={{
                  style: {
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  },
                }}
                InputLabelProps={{
                  style: {
                    color: "black",
                  },
                }}
                sx={{ width: "100%" }}
              />

              <TextField
                value={youtube}
                label="Youtube URL"
                onChange={handleChangeYoutube}
                InputProps={{
                  style: {
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  },
                }}
                InputLabelProps={{
                  style: {
                    color: "black",
                  },
                }}
                sx={{ width: "100%" }}
              />
              <TextField
                value={linkedin}
                label="Linkedin URL"
                onChange={handleChangeLinkedin}
                InputProps={{
                  style: {
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  },
                }}
                InputLabelProps={{
                  style: {
                    color: "black",
                  },
                }}
                sx={{ width: "100%" }}
              />
              <TextField
                value={instagram}
                label="Instagram URL"
                onChange={handleChangeInstagram}
                InputProps={{
                  style: {
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  },
                }}
                InputLabelProps={{
                  style: {
                    color: "black",
                  },
                }}
                sx={{ width: "100%" }}
              />
              <TextField
                value={pinterest}
                label="Pinterest URL"
                onChange={handleChangePinterest}
                InputProps={{
                  style: {
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  },
                }}
                InputLabelProps={{
                  style: {
                    color: "black",
                  },
                }}
                sx={{ width: "100%" }}
              />
              <TextField
                value={googlePlus}
                label="Google Plus URL"
                onChange={handleChangeGooglePlus}
                InputProps={{
                  style: {
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  },
                }}
                InputLabelProps={{
                  style: {
                    color: "black",
                  },
                }}
                sx={{ width: "100%" }}
              />
            </Box>
          </Box>
        </Box>
      </Box>
      <Footer label="Save Front CMS Setting" />
    </>
  );
};
