import React, { useState } from 'react';
import {
  Typography,
  Box,
  Button,
  InputLabel,
  FormControl,
  TextField, 
  SelectChangeEvent,
  Select,
  MenuItem,
  
  
} from '@mui/material';

import { useNavigate } from 'react-router-dom';

import {ReactComponent as BackArrow} from '../../assets/image/BackArrow.svg';
import {ReactComponent as Upload} from '../../assets/image/UploadIcon.svg';
import {ReactComponent as DropDown} from '../../assets/image/DropDownIcon.svg';
import { RoundedButton } from '../../components/RoundedButton';
import { EnhancedActionToolbar } from './AddStaff';


export const AddMoreDetails: React.FC = () =>{
    const navigate = useNavigate();

    const [epfNO, setEPF] = React.useState('')
    const handleChangeEPF= (event: React.ChangeEvent<HTMLInputElement>) => {
        setEPF(event.target.value);
    };

    const [basicSalary, setBasicSalary] = React.useState('')
    const handleChangeSalary= (event: React.ChangeEvent<HTMLInputElement>) => {
        setBasicSalary(event.target.value);
    };

    const [contract, setContract] = React.useState('')
    const ContractType=[
        {value:'Permanent'},
        {value:'Partnership'},
    ]
    const handleChangeContract=(event: SelectChangeEvent)=>{
        setContract(event.target.value);
    }

    
    const [workShift, setWorkShift] = React.useState('')
    const handleChangeWorkShift= (event: React.ChangeEvent<HTMLInputElement>) => {
        setWorkShift(event.target.value);
    };

    const [workLocation, setWorkLocation] = React.useState('')
    const handleChangeWorkLocation= (event: React.ChangeEvent<HTMLInputElement>) => {
        setWorkLocation(event.target.value);
    };

    const [accTitle, setAccTitle] = React.useState('')
    const handleChangeAccTitle= (event: React.ChangeEvent<HTMLInputElement>) => {
        setAccTitle(event.target.value);
    };

    const [bankAcc, setBankAcc] = React.useState('')
    const handleChangeBackAcc= (event: React.ChangeEvent<HTMLInputElement>) => {
        setBankAcc(event.target.value);
    };

    const [bankName, setBankName] = React.useState('')
    const handleChangeBankName= (event: React.ChangeEvent<HTMLInputElement>) => {
        setBankName(event.target.value);
    };

    const [IFSC, setIFSC] = React.useState('')
    const handleChangeIFSC= (event: React.ChangeEvent<HTMLInputElement>) => {
        setIFSC(event.target.value);
    };

    const [branchName, setBranchName] = React.useState('')
    const handleChangeBranch= (event: React.ChangeEvent<HTMLInputElement>) => {
        setBranchName(event.target.value);
    };

    const [facebook, setFacebook] = React.useState('')
    const handleChangeFacebook= (event: React.ChangeEvent<HTMLInputElement>) => {
        setFacebook(event.target.value);
    };

    const [twitter, setTwitter] = React.useState('')
    const handleChangeTwitter= (event: React.ChangeEvent<HTMLInputElement>) => {
        setTwitter(event.target.value);
    };
    
    const [linkedin, setLinkedin] = React.useState('')
    const handleChangeLinkedin= (event: React.ChangeEvent<HTMLInputElement>) => {
        setLinkedin(event.target.value);
    };

    const [instagram, setInstagram] = React.useState('')
    const handleChangeInstagram= (event: React.ChangeEvent<HTMLInputElement>) => {
        setInstagram(event.target.value);
    };


    return(
        <>
        <Box sx={{display:'flex', alignItems:'center', justifyContent:'space-between'}}>
            <Box sx={{ display: "flex", gap: 1, alignItems:'center'}}>
                <RoundedButton  onClick={() => navigate('/admin/human')}
                    style={{width:'40px', height:'40px', border:'1px solid #F4F4F4', background:'white'}}> 
                <BackArrow/> </RoundedButton>
                <Typography variant="h2">
                    Add Staff
                </Typography>
            </Box>
            <Box sx={{ display: "flex", gap: 1 }}>
                <RoundedButton style={{background: '#FFFFFF', border: '1px solid #F4F4F4', width:'122px'}}>Import Staff</RoundedButton>
                <RoundedButton variant="contained" style={{width:'100px'}}>
                    Save
                </RoundedButton>
            </Box>
        </Box>

        <EnhancedActionToolbar status={2}/>

        <Box
            sx={{
                display:'flex',
                flexDirection:'row',
                flexWrap:'wrap',
                justifyContent:'space-between',
                gap:'16px'
            }}
        >
            <Box sx={{width:'49.25%', height:'352px', borderRadius:'16px', background:'white'}}>
                <Box
                    sx={{
                        display:'flex',
                        flexDirection:'column',       
                        margin:'16px',
                        gap:'15.75px'
                        
                    }}
                >
                    <Typography variant='h3' sx={{marginBottom:'10.25px'}}>PAY ROLL</Typography>

                    <Box sx={{display:'flex',justifyContent:'space-between', width:"100%" }}>
                        <TextField
                            value={epfNO}
                            label="EPF No"
                            placeholder="Enter"
                            onChange={handleChangeEPF}
                            
                            InputProps={{
                                style:{    
                                    height: '48px',
                                    borderRadius: '12px',
                                    color: 'black'
                                }}}

                                InputLabelProps={{
                                style:{
                                    color:'black'
                                }
                            }}
                            sx={{
                                width:'49%',   
                            }}
                        />

                        <TextField
                            value={basicSalary}
                            label="Basic Salary"
                            placeholder="Enter"
                            onChange={handleChangeSalary}
                            
                            InputProps={{
                                style:{
                                    height: '48px',
                                    borderRadius: '12px',
                                    color: 'black'
                                }}}

                                InputLabelProps={{
                                style:{
                                    color:'black'
                                }
                            }}
                            sx={{
                                width:'49%',    
                            }}
                        />

                    </Box>

                    <Box sx={{display:'flex',justifyContent:'space-between', width:"100%" }}>
                        <FormControl sx={{ width: '49%',}}>
                        <InputLabel sx={{color:'black'}}>Contract Type</InputLabel>
                            <Select
                                value={contract}
                                onChange={handleChangeContract}
                                label="Contract Type"
                                placeholder='Select'
                                IconComponent={() => <DropDown style={{marginRight: '10px', cursor:'pointer'}} />}
                                sx={{
                                    height: '48px',
                                    borderRadius: '12px',
                                    color: 'black'
                                    
                                }}
                            >
                                {ContractType.map((option) => (
                                <MenuItem value={option.value}>
                                    {option.value}
                                </MenuItem>
                                ))}
                            </Select>
                        </FormControl>

                        <TextField
                            value={workShift}
                            label="Work Shift"
                            placeholder="Enter"
                            onChange={handleChangeWorkShift}
                            
                            InputProps={{
                                style:{
                                    height: '48px',
                                    borderRadius: '12px',
                                    color: 'black'
                                }}}

                                InputLabelProps={{
                                style:{
                                    color:'black'
                                }
                            }}
                            sx={{
                                width:'49%'
                            }}
                        />
                    </Box>

                    <TextField
                            value={workLocation}
                            label="Work Location"
                            placeholder="Enter"
                            onChange={handleChangeWorkLocation}
                            
                            InputProps={{
                                style:{
                                    height: '48px',
                                    borderRadius: '12px',
                                    color: 'black'
                                }}}

                                InputLabelProps={{
                                style:{
                                    color:'black'
                                }
                            }}
                            
                        />

                </Box>
            </Box>

            <Box sx={{width:'49.25%', height:'352px', borderRadius:'16px', background:'white'}}>
                <Box
                    sx={{
                        display:'flex',
                        flexDirection:'column',
                        alignItems:'flex-start',
                        margin:'16px',
                        gap:'15.75px'
                    }}
                >
                <Typography variant='h3' sx={{marginBottom:'10.25px', textTransform:'uppercase'}}>Bank Account Details</Typography>
                <TextField
                    value={accTitle}
                    label="Account Title"
                    placeholder="Enter"
                    onChange={handleChangeAccTitle}
                    
                    InputProps={{
                        style:{
                            height: '48px',
                            borderRadius: '12px',
                            color: 'black'
                        }}}

                        InputLabelProps={{
                        style:{
                            color:'black'
                        }
                    }}
                    sx={{width:'100%'}}
                /> 

                <TextField
                    value={bankAcc}
                    label="Bank Account No."
                    placeholder="Enter"
                    onChange={handleChangeBackAcc}
                    
                    InputProps={{
                        style:{
                            height: '48px',
                            borderRadius: '12px',
                            color: 'black'
                        }}}

                        InputLabelProps={{
                        style:{
                            color:'black'
                        }
                    }}
                    sx={{width:'100%'}}
                />

                <TextField
                    value={bankName}
                    label="Bank Name"
                    placeholder="Enter"
                    onChange={handleChangeBankName}
                    
                    InputProps={{
                        style:{
                            height: '48px',
                            borderRadius: '12px',
                            color: 'black'
                        }}}

                        InputLabelProps={{
                        style:{
                            color:'black'
                        }
                    }}
                     sx={{width:'100%'}}
                />

                
                    <Box sx={{display:'flex',justifyContent:'space-between', width:"100%" }}>
                        <TextField
                            value={IFSC}
                            label="IFSC Code"
                            placeholder="Enter"
                            onChange={handleChangeIFSC}
                            
                            InputProps={{
                                style:{
                                    height: '48px',
                                    borderRadius: '12px',
                                    color: 'black'
                                }}}

                                InputLabelProps={{
                                style:{
                                    color:'black'
                                }
                            }}
                            sx={{width:'49%'}}
                        />

                        <TextField
                            value={branchName}
                            label="Bank Branch Name"
                            placeholder="Enter"
                            onChange={handleChangeBranch}
                            
                            InputProps={{
                                style:{
                                    height: '48px',
                                    borderRadius: '12px',
                                    color: 'black'
                                }}}

                                InputLabelProps={{
                                style:{
                                    color:'black'
                                }
                            }}
                            sx={{width:'49%'}}
                        />
                    </Box>
                </Box>
            </Box>

            <Box sx={{width:'49.25%', height:'352px', borderRadius:'16px', background:'white'}}>
                <Box
                    sx={{
                        display:'flex',
                        flexDirection:'column',
                        alignItems:'flex-start',
                        margin:'16px',
                        gap:'15.75px'
                    }}
                    >
                    <Typography variant='h3' sx={{marginBottom:'10.25px', textTransform:'uppercase'}}>Social media link</Typography>

                    <TextField
                        value={facebook}
                        label="Facebook URL"
                        placeholder="Enter"
                        onChange={handleChangeFacebook}
                        
                        InputProps={{
                            style:{
                                height: '48px',
                                borderRadius: '12px',
                                color: 'black'
                            }}}

                            InputLabelProps={{
                            style:{
                                color:'black'
                            }
                        }}
                        sx={{width:'100%'}}
                    /> 
                    <TextField
                        value={twitter}
                        label="Twitter URL"
                        placeholder="Enter"
                        onChange={handleChangeTwitter}
                        
                        InputProps={{
                            style:{
                                height: '48px',
                                borderRadius: '12px',
                                color: 'black'
                            }}}

                            InputLabelProps={{
                            style:{
                                color:'black'
                            }
                        }}
                        sx={{width:'100%'}}
                    /> 
                    <TextField
                        value={linkedin}
                        label="Linkedin URL"
                        placeholder="Enter"
                        onChange={handleChangeLinkedin}
                        
                        InputProps={{
                            style:{
                                height: '48px',
                                borderRadius: '12px',
                                color: 'black'
                            }}}

                            InputLabelProps={{
                            style:{
                                color:'black'
                            }
                        }}
                        sx={{width:'100%'}}
                    /> 
                    <TextField
                        value={instagram}
                        label="Instagram URL"
                        placeholder="Enter"
                        onChange={handleChangeInstagram}
                        
                        InputProps={{
                            style:{
                                height: '48px',
                                borderRadius: '12px',
                                color: 'black'
                            }}}

                            InputLabelProps={{
                            style:{
                                color:'black'
                            }
                        }}
                        sx={{width:'100%'}}
                    />
                </Box>
            </Box>

            <Box sx={{width:'49.25%', height:'352px', borderRadius:'16px', background:'white'}}>
                <Box
                    sx={{
                        display:'flex',
                        flexDirection:'column',
                        alignItems:'flex-start',
                        margin:'16px',
                        gap:'15.75px'
                    }}
                    >
                    <Typography variant='h3' sx={{marginBottom:'10.25px', textTransform:'uppercase'}}>Upload Documents</Typography>
                    <Button variant="outlined" component="label" startIcon={<Upload/>}
                        sx={{
                            width:'100%',
                            height:'56px',
                            borderRadius:'99px',
                            fontSize:'14px',
                            fontWeight:'600'
                        }}
                    >
                        Upload Resume
                        <input hidden accept="" multiple type="file" />
                    </Button>

                    <Button variant="outlined" component="label" startIcon={<Upload/>}
                        sx={{
                            width:'100%',
                            height:'56px',
                            borderRadius:'99px',
                            fontSize:'14px',
                            fontWeight:'600'
                        }}
                    >
                        Upload Joining Letter
                        <input hidden accept="" multiple type="file" />
                    </Button>

                    <Button variant="outlined" component="label" startIcon={<Upload/>}
                        sx={{
                            width:'100%',
                            height:'56px',
                            borderRadius:'99px',
                            fontSize:'14px',
                            fontWeight:'600'
                        }}
                    >
                        Upload Other Document
                        <input hidden accept="" multiple type="file" />
                    </Button>
                  </Box>
            </Box>
        </Box>
                            
       
        </>
        
    )
}
