import React, { useState } from "react";

import {
  Box,
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
  TextField,
  Toolbar,
  Typography,
} from "@mui/material";

import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { DesktopDatePicker } from "@mui/x-date-pickers/DesktopDatePicker";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";

import { useNavigate } from "react-router-dom";

import { ReactComponent as BackArrow } from "../../assets/image/BackArrow.svg";
import { ReactComponent as DropDown } from "../../assets/image/DropDownIcon.svg";
import { ReactComponent as Upload } from "../../assets/image/UploadIcon.svg";
import Loading from "../../components/Loading";
import { RoundedButton } from "../../components/RoundedButton";
import { practitionerModel } from "../../models/practitioners";
import { practitionersService } from "../../services/agent";

type toolbarStatusType = {
  status: number;
};

export const EnhancedActionToolbar = (props: toolbarStatusType) => {
  const [selectedTab, setSelectedTab] = React.useState<number>(props.status);
  const navigate = useNavigate();
  return (
    <Toolbar
      sx={{
        borderRadius: "8px",
        marginBottom: "16px",
        padding: "16px",
        marginTop: "20px",
        background: "#FFFFFF",
      }}
    >
      <Box sx={{ flexGrow: 1, alignItems: "center" }}>
        <Button
          variant={`${selectedTab === 1 ? "contained" : "text"}`}
          onClick={() => {
            if (selectedTab !== 1) {
              setSelectedTab(1);
              navigate("/admin/human/add");
            }
          }}
          key={1}
          sx={{
            padding: "9px 16px",
            borderRadius: "10px",
            mr: 0.5,
            color: `${selectedTab === 1 ? "" : "#4E4E4E"}`,
          }}
        >
          Basic information
        </Button>
        <Button
          variant={`${selectedTab === 2 ? "contained" : "text"}`}
          onClick={() => {
            if (selectedTab !== 2) {
              setSelectedTab(2);
              navigate(`add-details`);
            }
          }}
          key={2}
          sx={{
            padding: "9px 16px",
            borderRadius: "10px",
            mr: 0.5,
            color: `${selectedTab === 2 ? "" : "#4E4E4E"}`,
          }}
        >
          Add More Details
        </Button>
      </Box>
    </Toolbar>
  );
};

export const AddStaff: React.FC = () => {
  const navigate = useNavigate();

  const [ID, setID] = React.useState("");

  const handleChangeID = (event: React.ChangeEvent<HTMLInputElement>) => {
    setID(event.target.value);
  };

  const [role, setRole] = React.useState("");
  const Role = [
    { value: "Doctor" },
    { value: "Nurse" },
    { value: "Admin" },
    { value: "Pharmacist" },
    { value: "Super Admin" },
    { value: "Accountant" },
    { value: "Pathologist" },
    { value: "Receptionist" },
    { value: "Radiologist" },
  ];
  const handleChangeRole = (event: SelectChangeEvent) => {
    setRole(event.target.value);
  };

  const [designation, setDesignation] = React.useState("");
  const Designation = [{ value: "Doctor" }, { value: "Nurse" }];
  const handleChangeDesignation = (event: SelectChangeEvent) => {
    setDesignation(event.target.value);
  };

  const [department, setDepartment] = React.useState("");
  const Department = [{ value: "Doctor" }, { value: "Nurse" }, { value: "OT" }];
  const handleChangeDepartment = (event: SelectChangeEvent) => {
    setDepartment(event.target.value);
  };

  const [specialist, setSpecialist] = React.useState("");
  const Specialist = [{ value: "Cardiologists" }, { value: "Nutritionist" }];
  const handleChangeSpecialist = (event: SelectChangeEvent) => {
    setSpecialist(event.target.value);
  };

  const [Fname, setFname] = React.useState("");
  const handleChangeFname = (event: React.ChangeEvent<HTMLInputElement>) => {
    setFname(event.target.value);
  };

  const [Lname, setLname] = React.useState("");
  const handleChangeLname = (event: React.ChangeEvent<HTMLInputElement>) => {
    setLname(event.target.value);
  };

  const [fatherName, setFatherName] = React.useState("");
  const handleChangeFatherName = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setFatherName(event.target.value);
  };

  const [motherName, setMotherName] = React.useState("");
  const handleChangeMotherName = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setMotherName(event.target.value);
  };

  const [gender, setGender] = React.useState("");
  const Gender = [
    { value: "Male" },
    { value: "Female" },
    { value: "Prefer not to say" },
  ];
  const handleChangeGender = (event: SelectChangeEvent) => {
    setGender(event.target.value);
  };

  const [maritalStatus, setmaritalStatus] = React.useState("");
  const MaritalStatus = [
    { value: "Single" },
    { value: "Married" },
    { value: "Widow" },
    { value: "Complicated" },
    { value: "Prefer not to say" },
  ];
  const handleChangeStatus = (event: SelectChangeEvent) => {
    setmaritalStatus(event.target.value);
  };

  const [bloodGroup, setbloodGroup] = React.useState("");
  const BloodGroup = [
    { value: "A+" },
    { value: "A-" },
    { value: "B+" },
    { value: "B-" },
    { value: "O+" },
    { value: "O-" },
    { value: "AB+" },
    { value: "AB-" },
  ];
  const handleChangeBlood = (event: SelectChangeEvent) => {
    setbloodGroup(event.target.value);
  };

  const [DOB, setDOB] = React.useState<Date | null>();
  const [day, setDay] = React.useState<string>("01");
  const [month, setMonth] = React.useState<string>("01");
  const [year, setYear] = React.useState<string>("1999");

  let dayString: string;
  let monthString: string;
  let yearString: string;
  const handleChangeDOB = (newValue: Date | null) => {
    setDOB(newValue);

    let dayNumber = (DOB?.getDate() as unknown as number) || 10;
    if (dayNumber < 10) {
      dayString = `0${dayNumber}`;
    } else {
      dayString = `${dayNumber}`;
    }
    setDay(dayString);

    let monthNumber = (DOB?.getMonth() as unknown as number) + 1 || 10;
    if (monthNumber < 10) {
      monthString = `0${monthNumber}`;
    } else {
      monthString = `${monthNumber}`;
    }
    setMonth(monthString);

    let yearNumber = (DOB?.getFullYear() as unknown as number) || 2000;
    yearString = `${yearNumber}`;
    setYear(yearString);
  };

  const [DOJoin, setDOJoin] = React.useState<Date | null>();
  const handleChangeDOJoin = (newValue: Date | null) => {
    setDOJoin(newValue);
  };

  const [phone, setPhone] = React.useState("");
  const handleChangePhone = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPhone(event.target.value);
  };

  const [emergency, setEmergency] = React.useState("");
  const handleChangeEmergency = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setEmergency(event.target.value);
  };

  const [email, setEmail] = React.useState("");
  const handleChangeEmail = (event: React.ChangeEvent<HTMLInputElement>) => {
    setEmail(event.target.value);
  };

  const [currentAdd, setcurrentAdd] = React.useState("");
  const handleChangecurrentAdd = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setcurrentAdd(event.target.value);
  };

  const [permanentAdd, setpermanentAdd] = React.useState("");
  const handleChangePermanentAdd = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setpermanentAdd(event.target.value);
  };

  const [qualification, setQualification] = React.useState("");
  const handleChangeQualification = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setQualification(event.target.value);
  };

  const [workExp, setWorkExp] = React.useState("");
  const handleChangeWorkExp = (event: React.ChangeEvent<HTMLInputElement>) => {
    setWorkExp(event.target.value);
  };

  const [specialization, setSpecialization] = React.useState("");
  const handleChangeSpecialization = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setSpecialization(event.target.value);
  };

  const [note, setNote] = React.useState("");
  const handleChangeNote = (event: React.ChangeEvent<HTMLInputElement>) => {
    setNote(event.target.value);
  };

  const [panNum, setPanNum] = React.useState("");
  const handleChangePanNum = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPanNum(event.target.value);
  };

  const [natID, setNatID] = React.useState("");
  const handleChangeNatID = (event: React.ChangeEvent<HTMLInputElement>) => {
    setNatID(event.target.value);
  };

  const [localID, setLocalID] = React.useState("");
  const handleChangeLocalID = (event: React.ChangeEvent<HTMLInputElement>) => {
    setLocalID(event.target.value);
  };

  const [refContact, setRefContact] = React.useState("");
  const handleChangeRefContact = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRefContact(event.target.value);
  };

  const [loading, setLoading] = useState<boolean>(false);

  const handleSave = () => {
    const dataStaff = {
      name: [
        {
          use: "official",
          family: Lname,
          given: [Fname],
        },
      ],
      gender: gender,
      birthDate: year + "-" + month + "-" + day,
      telecom: [
        {
          system: "phone",
          value: phone,
        },
        {
          system: "email",
          value: email,
        },
      ],
      address: [
        {
          use: "work",
          text: currentAdd,
        },
      ],
      qualification: [
        {
          code: {
            text: qualification,
          },
          issuer: {
            reference: refContact,
          },
        },
      ],
    };
    setLoading(true);
    practitionersService
      .post({
        body: dataStaff as practitionerModel,
      })
      .then(() => navigate("/admin/human"))
      .catch(() => setLoading(false));
  };

  return loading ? (
    <Loading />
  ) : (
    <>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <Box sx={{ display: "flex", gap: 1, alignItems: "center" }}>
          <RoundedButton
            onClick={() => navigate("/admin/human")}
            style={{
              width: "40px",
              height: "40px",
              border: "1px solid #F4F4F4",
              background: "white",
            }}
          >
            <BackArrow />
          </RoundedButton>
          <Typography variant="h2">Add Practitioner</Typography>
        </Box>
        <Box sx={{ display: "flex", gap: 1 }}>
          {/* <RoundedButton
            style={{
              background: "#FFFFFF",
              border: "1px solid #F4F4F4",
              width: "122px",
            }}
          >
            Import Practitioner
          </RoundedButton> */}
          <RoundedButton
            variant="contained"
            style={{ width: "100px" }}
            onClick={handleSave}
          >
            Save
          </RoundedButton>
        </Box>
      </Box>

      <EnhancedActionToolbar status={1} />

      <Box
        sx={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <Box
          sx={{
            width: "49.25%",
            height: "904px",
            background: "white",
            borderRadius: "16px",
          }}
        >
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              alignItems: "flex-start",
              justifyContent: "center",
              margin: "16px",
              gap: "26px",
            }}
          >
            <Typography variant="h3">BASIC INFORMATION</Typography>

            <Button
              variant="outlined"
              component="label"
              startIcon={<Upload />}
              sx={{
                width: "100%",
                height: "56px",
                borderRadius: "99px",
                fontSize: "14px",
                fontWeight: "600",
              }}
            >
              Upload avatar
              <input hidden accept="" multiple type="file" />
            </Button>

            <TextField
              required
              value={ID}
              label="Staff ID"
              placeholder="Staff ID"
              onChange={handleChangeID}
              InputProps={{
                style: {
                  height: "48px",
                  borderRadius: "12px",
                  color: "black",
                },
              }}
              InputLabelProps={{
                style: {
                  color: "black",
                },
              }}
              sx={{ width: "100%" }}
            />

            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                width: "100%",
              }}
            >
              <FormControl sx={{ width: "49%" }}>
                <InputLabel required sx={{ color: "black" }}>
                  Role
                </InputLabel>
                <Select
                  value={role}
                  onChange={handleChangeRole}
                  label="Role"
                  IconComponent={() => (
                    <DropDown
                      style={{ marginRight: "10px", cursor: "pointer" }}
                    />
                  )}
                  sx={{
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  }}
                >
                  {Role.map((option, index) => (
                    <MenuItem key={index} value={option.value}>
                      {option.value}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>

              <FormControl sx={{ width: "49%" }}>
                <InputLabel sx={{ color: "black" }}>Designation</InputLabel>
                <Select
                  value={designation}
                  onChange={handleChangeDesignation}
                  label="Designation"
                  IconComponent={() => (
                    <DropDown
                      style={{ marginRight: "10px", cursor: "pointer" }}
                    />
                  )}
                  sx={{
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  }}
                >
                  {Designation.map((option, index) => (
                    <MenuItem key={index} value={option.value}>
                      {option.value}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Box>

            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                width: "100%",
              }}
            >
              <FormControl sx={{ width: "49%" }}>
                <InputLabel sx={{ color: "black" }}>Department</InputLabel>
                <Select
                  value={department}
                  onChange={handleChangeDepartment}
                  label="Department"
                  IconComponent={() => (
                    <DropDown
                      style={{ marginRight: "10px", cursor: "pointer" }}
                    />
                  )}
                  sx={{
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  }}
                >
                  {Department.map((option, index) => (
                    <MenuItem key={index} value={option.value}>
                      {option.value}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>

              <FormControl sx={{ width: "49%" }}>
                <InputLabel sx={{ color: "black" }}>Specialist</InputLabel>
                <Select
                  value={specialist}
                  onChange={handleChangeSpecialist}
                  label="Specialist"
                  IconComponent={() => (
                    <DropDown
                      style={{ marginRight: "10px", cursor: "pointer" }}
                    />
                  )}
                  sx={{
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  }}
                >
                  {Specialist.map((option, index) => (
                    <MenuItem key={index} value={option.value}>
                      {option.value}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Box>

            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                width: "100%",
              }}
            >
              <TextField
                required
                value={Fname}
                label="First name"
                placeholder="Enter"
                onChange={handleChangeFname}
                InputProps={{
                  style: {
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  },
                }}
                InputLabelProps={{
                  style: {
                    color: "black",
                  },
                }}
                sx={{ width: "49%" }}
              />

              <TextField
                value={Lname}
                label="Last name"
                placeholder="Enter"
                onChange={handleChangeLname}
                InputProps={{
                  style: {
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  },
                }}
                InputLabelProps={{
                  style: {
                    color: "black",
                  },
                }}
                sx={{ width: "49%" }}
              />
            </Box>

            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                width: "100%",
              }}
            >
              <TextField
                value={fatherName}
                label="Father name"
                placeholder="Enter"
                onChange={handleChangeFatherName}
                InputProps={{
                  style: {
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  },
                }}
                InputLabelProps={{
                  style: {
                    color: "black",
                  },
                }}
                sx={{ width: "49%" }}
              />

              <TextField
                value={motherName}
                label="Mother name"
                placeholder="Enter"
                onChange={handleChangeMotherName}
                InputProps={{
                  style: {
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  },
                }}
                InputLabelProps={{
                  style: {
                    color: "black",
                  },
                }}
                sx={{ width: "49%" }}
              />
            </Box>

            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                width: "100%",
              }}
            >
              <FormControl sx={{ width: "49%" }}>
                <InputLabel sx={{ color: "black" }}>Gender</InputLabel>
                <Select
                  value={gender}
                  onChange={handleChangeGender}
                  label="Gender"
                  IconComponent={() => (
                    <DropDown
                      style={{ marginRight: "10px", cursor: "pointer" }}
                    />
                  )}
                  sx={{
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  }}
                >
                  {Gender.map((option, index) => (
                    <MenuItem key={index} value={option.value}>
                      {option.value}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>

              <FormControl sx={{ width: "49%" }}>
                <InputLabel sx={{ color: "black" }}>Marital Status</InputLabel>
                <Select
                  value={maritalStatus}
                  onChange={handleChangeStatus}
                  label="Marital Status"
                  IconComponent={() => (
                    <DropDown
                      style={{ marginRight: "10px", cursor: "pointer" }}
                    />
                  )}
                  sx={{
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  }}
                >
                  {MaritalStatus.map((option, index) => (
                    <MenuItem key={index} value={option.value}>
                      {option.value}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Box>

            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                width: "100%",
              }}
            >
              <FormControl sx={{ width: "49%" }}>
                <InputLabel sx={{ color: "black" }}>Blood Group</InputLabel>
                <Select
                  value={bloodGroup}
                  onChange={handleChangeBlood}
                  label="Blood Group"
                  IconComponent={() => (
                    <DropDown
                      style={{ marginRight: "10px", cursor: "pointer" }}
                    />
                  )}
                  sx={{
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  }}
                >
                  {BloodGroup.map((option, index) => (
                    <MenuItem key={index} value={option.value}>
                      {option.value}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>

              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DesktopDatePicker
                  label="Date of birth"
                  inputFormat="dd/MM/yyyy"
                  value={DOB}
                  onChange={handleChangeDOB}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      sx={{
                        width: "49%",
                        label: { color: "black" },
                      }}
                    />
                  )}
                  InputProps={{
                    style: {
                      height: "48px",
                      borderRadius: "12px",
                      color: "black",
                    },
                  }}
                />
              </LocalizationProvider>
            </Box>

            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                width: "100%",
              }}
            >
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DesktopDatePicker
                  label="Date of Joining"
                  inputFormat="dd/MM/yyyy"
                  value={DOJoin}
                  onChange={handleChangeDOJoin}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      sx={{
                        width: "49%",
                        label: { color: "black" },
                      }}
                    />
                  )}
                  InputProps={{
                    style: {
                      height: "48px",
                      borderRadius: "12px",
                      color: "black",
                    },
                  }}
                />
              </LocalizationProvider>

              <TextField
                value={phone}
                label="Phone"
                placeholder="Enter"
                onChange={handleChangePhone}
                InputProps={{
                  style: {
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  },
                }}
                InputLabelProps={{
                  style: {
                    color: "black",
                  },
                }}
                sx={{ width: "49%" }}
              />
            </Box>

            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                width: "100%",
              }}
            >
              <TextField
                value={emergency}
                label="Emergency Contact"
                placeholder="Enter"
                onChange={handleChangeEmergency}
                InputProps={{
                  style: {
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  },
                }}
                InputLabelProps={{
                  style: {
                    color: "black",
                  },
                }}
                sx={{ width: "49%" }}
              />

              <TextField
                required
                value={email}
                label="Email"
                placeholder="Enter"
                onChange={handleChangeEmail}
                InputProps={{
                  style: {
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  },
                }}
                InputLabelProps={{
                  style: {
                    color: "black",
                  },
                }}
                sx={{ width: "49%" }}
              />
            </Box>
          </Box>
        </Box>

        <Box
          sx={{
            width: "49.25%",
            height: "904px",
            background: "white",
            borderRadius: "16px",
          }}
        >
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              alignItems: "flex-start",
              justifyContent: "center",
              margin: "16px",
              gap: "26px",
            }}
          >
            <TextField
              value={currentAdd}
              label="Current Address"
              placeholder="Enter"
              onChange={handleChangecurrentAdd}
              InputProps={{
                style: {
                  height: "48px",
                  borderRadius: "12px",
                  color: "black",
                },
              }}
              InputLabelProps={{
                style: {
                  color: "black",
                },
              }}
              sx={{ width: "100%" }}
            />

            <TextField
              value={permanentAdd}
              label="Permanent Address"
              placeholder="Enter"
              onChange={handleChangePermanentAdd}
              InputProps={{
                style: {
                  height: "48px",
                  borderRadius: "12px",
                  color: "black",
                },
              }}
              InputLabelProps={{
                style: {
                  color: "black",
                },
              }}
              sx={{ width: "100%" }}
            />

            <TextField
              value={qualification}
              label="Qualification"
              placeholder="Enter"
              onChange={handleChangeQualification}
              multiline
              InputProps={{
                style: {
                  height: "122px",
                  borderRadius: "12px",
                  color: "black",
                },
              }}
              InputLabelProps={{
                style: {
                  color: "black",
                },
              }}
              sx={{ width: "100%" }}
            />

            <TextField
              value={workExp}
              label="Work Experience"
              placeholder="Enter"
              onChange={handleChangeWorkExp}
              multiline
              InputProps={{
                style: {
                  height: "122px",
                  borderRadius: "12px",
                  color: "black",
                },
              }}
              InputLabelProps={{
                style: {
                  color: "black",
                },
              }}
              sx={{ width: "100%" }}
            />
            <TextField
              value={specialization}
              label="Specialization"
              placeholder="Enter"
              onChange={handleChangeSpecialization}
              multiline
              InputProps={{
                style: {
                  height: "122px",
                  borderRadius: "12px",
                  color: "black",
                },
              }}
              InputLabelProps={{
                style: {
                  color: "black",
                },
              }}
              sx={{ width: "100%" }}
            />

            <TextField
              value={note}
              label="Note"
              placeholder="Enter"
              onChange={handleChangeNote}
              multiline
              InputProps={{
                style: {
                  height: "122px",
                  borderRadius: "12px",
                  color: "black",
                },
              }}
              InputLabelProps={{
                style: {
                  color: "black",
                },
              }}
              sx={{ width: "100%" }}
            />

            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                width: "100%",
              }}
            >
              <TextField
                required
                value={panNum}
                label="Pan Number"
                placeholder="Enter"
                onChange={handleChangePanNum}
                multiline
                InputProps={{
                  style: {
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  },
                }}
                InputLabelProps={{
                  style: {
                    color: "black",
                  },
                }}
                sx={{ width: "49%" }}
              />

              <TextField
                value={natID}
                label="National Identification Number"
                placeholder="Enter"
                onChange={handleChangeNatID}
                InputProps={{
                  style: {
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  },
                }}
                InputLabelProps={{
                  style: {
                    color: "black",
                  },
                }}
                sx={{ width: "49%" }}
              />
            </Box>

            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                width: "100%",
              }}
            >
              <TextField
                value={localID}
                label="Local Identification Number"
                placeholder="Enter"
                onChange={handleChangeLocalID}
                InputProps={{
                  style: {
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  },
                }}
                InputLabelProps={{
                  style: {
                    color: "black",
                  },
                }}
                sx={{ width: "49%" }}
              />

              <TextField
                value={refContact}
                label="Reference Contact"
                placeholder="Enter"
                onChange={handleChangeRefContact}
                InputProps={{
                  style: {
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  },
                }}
                InputLabelProps={{
                  style: {
                    color: "black",
                  },
                }}
                sx={{ width: "49%" }}
              />
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
};
