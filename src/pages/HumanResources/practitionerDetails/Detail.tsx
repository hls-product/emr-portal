import { SxProps, Typography } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import { useParams } from "react-router-dom";
import { practitionerModel } from "../../../models/practitioners";
import { practitionersService } from "../../../services/agent";
import doctor from "../../../assets/image/hrDoctor.svg";
import Loading from "../../../components/Loading";
interface RenderProps {
  title: string;
  value: number | string;
}
interface DetailContainerProps {
  title: string;
  data: RenderProps[];
  sx?: SxProps;
}
const RenderData = ({ title, value }: RenderProps) => (
  <Box
    sx={{
      width: "100%",
      display: "flex",
      justifyContent: "space-between",
      flex: 1,
      padding: "8px 0",
    }}
  >
    <Typography children={title} fontWeight={600} />
    <Typography children={value} />
  </Box>
);
const DetailContainer = ({ title, data, sx }: DetailContainerProps) => (
  <Box
    flexBasis="49.5%"
    bgcolor="#ffffff"
    padding="16px"
    borderRadius="16px"
    marginBottom="16px"
    sx={sx as SxProps}
  >
    <Typography
      children={title}
      fontSize={20}
      fontWeight={700}
      marginBottom="8px"
    />
    {data.map((item) => (
      <RenderData title={item.title} value={item.value} />
    ))}
  </Box>
);
export const Detail: React.FC = () => {
  const [loading, setLoading] = React.useState<boolean>(true);
  const [data, setData] = React.useState<practitionerModel>({});
  const { id } = useParams();

  React.useEffect(() => {
    try {
      practitionersService.getById(id as string).then((res) => {
        setData(res);
        setLoading(false);
      });
    } catch (error) {
      console.error("getPractitionerById err: " + error);
    }
  }, []);

  const generalInfo: DetailContainerProps["data"] = [
    {
      title: "Full Name",
      value:
        data.name?.find((item) => (item.use = "official"))?.text ||
        data.name?.find((item) => (item.use = "official"))?.given?.join(" ") +
          " " +
          data.name?.find((item) => (item.use = "official"))?.family ||
        "-",
    },
    {
      title: "Given Name",
      value:
        data.name?.find((item) => (item.use = "official"))?.given?.join(" ") ||
        "-",
    },
    {
      title: "Family Name",
      value: data.name?.find((item) => (item.use = "official"))?.family || "-",
    },
    {
      title: "Prefix",
      value:
        data.name
          ?.find((item) => (item.use = "official"))
          ?.prefix?.join(", ") || "-",
    },
    {
      title: "Suffix",
      value:
        data.name
          ?.find((item) => (item.use = "official"))
          ?.suffix?.join(", ") || "-",
    },
    {
      title: "Gender",
      value: data.gender ? (data.gender === "male" ? "Male" : "Female") : "-",
    },
    {
      title: "Date of Birth",
      value: new Date(data.birthDate as string).toLocaleDateString(),
    },
  ];
  const addressInfo: DetailContainerProps["data"] = [
    {
      title: "Address",
      value: data.address?.find((item) => item.use === "home")?.text || "-",
    },
    {
      title: "Line",
      value:
        data.address?.find((item) => item.use === "home")?.line?.join(", ") ||
        "-",
    },
    {
      title: "City",
      value: data.address?.find((item) => item.use === "home")?.city || "-",
    },
    {
      title: "District",
      value: data.address?.find((item) => item.use === "home")?.district || "-",
    },
    {
      title: "State",
      value: data.address?.find((item) => item.use === "home")?.state || "-",
    },
    {
      title: "Postal Code",
      value:
        data.address?.find((item) => item.use === "home")?.postalCode || "-",
    },
    {
      title: "Country",
      value: data.address?.find((item) => item.use === "home")?.country || "-",
    },
  ];
  return (
    <React.Fragment>
      {loading ? (
        <Loading />
      ) : (
        <Box
          display="flex"
          width="100%"
          justifyContent="space-between"
          flexWrap="wrap"
        >
          <Box
            flexBasis="49.5%"
            borderRadius="16px"
            display="flex"
            flexDirection="column"
            bgcolor="#ffffff"
            marginBottom="16px"
          >
            <img
              src={data.photo ? data.photo[0].url : doctor}
              alt="doctor photo"
              style={{ alignSelf: "center", width: "120px", marginTop: "16px" }}
            />
            <DetailContainer
              title="General Information"
              data={generalInfo}
              sx={{ marginBottom: 0 }}
            />
          </Box>

          {data.qualification && data.qualification?.length > 0 && (
            <Box
              flexBasis="49.5%"
              borderRadius="16px"
              display="flex"
              flexDirection="column"
              children={data.qualification?.map((item) => (
                <DetailContainer
                  title={item.code.text as string}
                  data={[
                    {
                      title: "Period",
                      value:
                        new Date(item.period?.start as string).getFullYear() +
                        " - " +
                        new Date(item.period?.end as string).getFullYear(),
                    },
                    {
                      title: "Issuer",
                      value:
                        item.issuer?.reference || item.issuer?.display || "-",
                    },
                  ]}
                />
              ))}
            />
          )}
          <DetailContainer title="Address Information" data={addressInfo} />
        </Box>
      )}
    </React.Fragment>
  );
};
