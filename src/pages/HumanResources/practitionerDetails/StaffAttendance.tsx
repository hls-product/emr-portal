import React from "react";
import {
  Toolbar,
  Box,
  Typography,
  FormControl,
  Select,
  MenuItem,
  SelectChangeEvent,
} from "@mui/material";
import * as _ from "lodash";

import { Header } from "../../../layout/Header";
import { Content } from "../../../layout/Content";
import { AdminFooter } from "../../../layout/Footer";
import { Sidebar } from "../../../layout/Sidebar";
import { EnhancedActionToolbar } from "../EnhancedActionToolbar";

import calendar_check_icon from "../../../assets/icon/calendar_check_icon.svg";
import calendar_load_icon from "../../../assets/icon/calendar_load_icon.svg";
import calendar_reject_icon from "../../../assets/icon/calendar_reject_icon.svg";
import calendar_icon from "../../../assets/icon/calendar_icon.svg";
import calendar_off_icon from "../../../assets/icon/calendar_off_icon.svg";
import option from "../../../assets/icon/option.svg";
import arrow_down from "../../../assets/icon/arrow_down.svg";

interface attendanceType {
  id: number;
  label: string;
  value: number;
  icon: string;
  iconBackground: string;
  iconBorder: string;
}

interface CalendarCard {
  list: attendanceType[];
}

interface attendanceStatus {
  id: number;
  label: string;
  color: string;
}

interface listStatus {
  list: attendanceStatus[];
}

interface tableHeader {
  year: string;
  onChangeYear: (event: SelectChangeEvent) => void;
}

interface dateMonth {
  key: string;
  date: number;
  month: number;
}

interface statusType {
  id: number;
  background: string;
  name: string;
  color: string;
  data: dateMonth[];
}

interface attendanceByYear {
  year: string;
  data: statusType[];
}

interface resultObject {
  name: string;
  background: string;
  color: string;
}

interface calendarType {
  year: string;
}

const attendanceCalendar: attendanceByYear[] = [
  {
    year: "2019",
    data: [
      {
        id: 0,
        name: "absent",
        background: "#FCEBEB",
        color: "#E03535",
        data: [
          {
            key: "11-1",
            date: 11,
            month: 1,
          },
          {
            key: "14-8",
            date: 14,
            month: 8,
          },
          {
            key: "27-6",
            date: 27,
            month: 6,
          },
          {
            key: "4-12",
            date: 4,
            month: 12,
          },
        ],
      },
      {
        id: 1,
        name: "late",
        background: "#FFF1E6",
        color: "#FF7300",
        data: [
          {
            key: "2-1",
            date: 2,
            month: 1,
          },
          {
            key: "3-2",
            date: 3,
            month: 2,
          },
          {
            key: "5-4",
            date: 5,
            month: 4,
          },
          {
            key: "6-9",
            date: 6,
            month: 9,
          },
          {
            key: "14-3",
            date: 14,
            month: 3,
          },
          {
            key: "15-6",
            date: 15,
            month: 6,
          },
          {
            key: "18-1",
            date: 18,
            month: 1,
          },
          {
            key: "14-8",
            date: 14,
            month: 8,
          },
          {
            key: "22-6",
            date: 22,
            month: 6,
          },
          {
            key: "20-10",
            date: 20,
            month: 10,
          },
        ],
      },
      {
        id: 2,
        name: "half",
        background: "#FEEAF4",
        color: "#F92F91",
        data: [
          {
            key: "11-4",
            date: 11,
            month: 4,
          },
          {
            key: "14-4",
            date: 14,
            month: 4,
          },
          {
            key: "15-10",
            date: 15,
            month: 10,
          },
          {
            key: "17-6",
            date: 17,
            month: 6,
          },
          {
            key: "20-8",
            date: 20,
            month: 8,
          },
        ],
      },
      {
        id: 3,
        name: "holiday",
        background: "#E8F3FB",
        color: "#1685DA",
        data: [
          {
            key: "7-6",
            date: 7,
            month: 6,
          },
          {
            key: "9-5",
            date: 9,
            month: 5,
          },
          {
            key: "11-8",
            date: 11,
            month: 8,
          },
          {
            key: "17-7",
            date: 17,
            month: 7,
          },
          {
            key: "24-3",
            date: 24,
            month: 3,
          },
          {
            key: "25-11",
            date: 25,
            month: 11,
          },
        ],
      },
    ],
  },
  {
    year: "2020",
    data: [
      {
        id: 0,
        name: "absent",
        background: "#FCEBEB",
        color: "#E03535",
        data: [
          {
            key: "12-1",
            date: 12,
            month: 1,
          },
          {
            key: "15-8",
            date: 15,
            month: 8,
          },
          {
            key: "28-6",
            date: 28,
            month: 6,
          },
          {
            key: "5-12",
            date: 5,
            month: 12,
          },
        ],
      },
      {
        id: 1,
        name: "late",
        background: "#FFF1E6",
        color: "#FF7300",
        data: [
          {
            key: "3-1",
            date: 3,
            month: 1,
          },
          {
            key: "4-2",
            date: 4,
            month: 2,
          },
          {
            key: "6-4",
            date: 6,
            month: 4,
          },
          {
            key: "7-9",
            date: 7,
            month: 9,
          },
          {
            key: "15-3",
            date: 15,
            month: 3,
          },
          {
            key: "16-6",
            date: 16,
            month: 6,
          },
          {
            key: "19-1",
            date: 19,
            month: 1,
          },
          {
            key: "20-9",
            date: 20,
            month: 9,
          },
          {
            key: "23-6",
            date: 23,
            month: 6,
          },
          {
            key: "21-10",
            date: 21,
            month: 10,
          },
        ],
      },
      {
        id: 2,
        name: "half",
        background: "#FEEAF4",
        color: "#F92F91",
        data: [
          {
            key: "12-4",
            date: 12,
            month: 4,
          },
          {
            key: "15-4",
            date: 15,
            month: 4,
          },
          {
            key: "16-10",
            date: 16,
            month: 10,
          },
          {
            key: "18-6",
            date: 18,
            month: 6,
          },
          {
            key: "21-8",
            date: 21,
            month: 8,
          },
        ],
      },
      {
        id: 3,
        name: "holiday",
        background: "#E8F3FB",
        color: "#1685DA",
        data: [
          {
            key: "8-6",
            date: 8,
            month: 6,
          },
          {
            key: "10-5",
            date: 10,
            month: 5,
          },
          {
            key: "12-8",
            date: 12,
            month: 8,
          },
          {
            key: "18-7",
            date: 18,
            month: 7,
          },
          {
            key: "25-3",
            date: 25,
            month: 3,
          },
          {
            key: "26-11",
            date: 26,
            month: 11,
          },
        ],
      },
    ],
  },
  {
    year: "2021",
    data: [
      {
        id: 0,
        name: "absent",
        background: "#FCEBEB",
        color: "#E03535",
        data: [
          {
            key: "11-2",
            date: 11,
            month: 2,
          },
          {
            key: "14-9",
            date: 14,
            month: 9,
          },
          {
            key: "27-7",
            date: 27,
            month: 7,
          },
          {
            key: "4-1",
            date: 4,
            month: 1,
          },
        ],
      },
      {
        id: 1,
        name: "late",
        background: "#FFF1E6",
        color: "#FF7300",
        data: [
          {
            key: "2-2",
            date: 2,
            month: 2,
          },
          {
            key: "3-3",
            date: 3,
            month: 3,
          },
          {
            key: "5-5",
            date: 5,
            month: 5,
          },
          {
            key: "6-10",
            date: 6,
            month: 10,
          },
          {
            key: "14-4",
            date: 14,
            month: 4,
          },
          {
            key: "15-7",
            date: 15,
            month: 7,
          },
          {
            key: "18-2",
            date: 18,
            month: 2,
          },
          {
            key: "19-10",
            date: 19,
            month: 10,
          },
          {
            key: "22-7",
            date: 22,
            month: 7,
          },
          {
            key: "20-11",
            date: 20,
            month: 11,
          },
        ],
      },
      {
        id: 2,
        name: "half",
        background: "#FEEAF4",
        color: "#F92F91",
        data: [
          {
            key: "11-5",
            date: 11,
            month: 5,
          },
          {
            key: "14-5",
            date: 14,
            month: 5,
          },
          {
            key: "15-11",
            date: 15,
            month: 11,
          },
          {
            key: "17-7",
            date: 17,
            month: 7,
          },
          {
            key: "20-9",
            date: 20,
            month: 9,
          },
        ],
      },
      {
        id: 3,
        name: "holiday",
        background: "#E8F3FB",
        color: "#1685DA",
        data: [
          {
            key: "7-7",
            date: 7,
            month: 7,
          },
          {
            key: "9-6",
            date: 9,
            month: 6,
          },
          {
            key: "11-9",
            date: 11,
            month: 9,
          },
          {
            key: "17-8",
            date: 17,
            month: 8,
          },
          {
            key: "24-4",
            date: 24,
            month: 4,
          },
          {
            key: "25-12",
            date: 25,
            month: 12,
          },
        ],
      },
    ],
  },
  {
    year: "2022",
    data: [
      {
        id: 0,
        name: "absent",
        background: "#FCEBEB",
        color: "#E03535",
        data: [
          {
            key: "11-1",
            date: 11,
            month: 1,
          },
          {
            key: "14-8",
            date: 14,
            month: 8,
          },
          {
            key: "27-6",
            date: 27,
            month: 6,
          },
          {
            key: "4-12",
            date: 4,
            month: 12,
          },
        ],
      },
      {
        id: 1,
        name: "late",
        background: "#FFF1E6",
        color: "#FF7300",
        data: [
          {
            key: "2-1",
            date: 2,
            month: 1,
          },
          {
            key: "3-2",
            date: 3,
            month: 2,
          },
          {
            key: "5-4",
            date: 5,
            month: 4,
          },
          {
            key: "6-9",
            date: 6,
            month: 9,
          },
          {
            key: "14-3",
            date: 14,
            month: 3,
          },
          {
            key: "15-6",
            date: 15,
            month: 6,
          },
          {
            key: "18-1",
            date: 18,
            month: 1,
          },
          {
            key: "19-9",
            date: 14,
            month: 8,
          },
          {
            key: "22-6",
            date: 27,
            month: 6,
          },
          {
            key: "20-10",
            date: 20,
            month: 10,
          },
        ],
      },
      {
        id: 2,
        name: "half",
        background: "#FEEAF4",
        color: "#F92F91",
        data: [
          {
            key: "11-4",
            date: 11,
            month: 4,
          },
          {
            key: "14-4",
            date: 14,
            month: 4,
          },
          {
            key: "15-10",
            date: 15,
            month: 10,
          },
          {
            key: "17-6",
            date: 17,
            month: 6,
          },
          {
            key: "20-8",
            date: 20,
            month: 8,
          },
        ],
      },
      {
        id: 3,
        name: "holiday",
        background: "#E8F3FB",
        color: "#1685DA",
        data: [
          {
            key: "7-6",
            date: 7,
            month: 6,
          },
          {
            key: "9-5",
            date: 9,
            month: 5,
          },
          {
            key: "11-8",
            date: 11,
            month: 8,
          },
          {
            key: "17-7",
            date: 17,
            month: 7,
          },
          {
            key: "24-3",
            date: 24,
            month: 3,
          },
          {
            key: "25-11",
            date: 25,
            month: 11,
          },
        ],
      },
    ],
  },
];

const attendanceCheckList: attendanceType[] = [
  {
    id: 0,
    label: "Total Present",
    value: 110,
    icon: calendar_check_icon,
    iconBackground: "#22AA4F",
    iconBorder: "#E9F7ED",
  },
  {
    id: 1,
    label: "Total Late",
    value: 2,
    icon: calendar_load_icon,
    iconBackground: "#FF7300",
    iconBorder: "#FFF1E6",
  },
  {
    id: 2,
    label: "Total Absent",
    value: 3,
    icon: calendar_reject_icon,
    iconBackground: "#E03535",
    iconBorder: "#FCEBEB",
  },
  {
    id: 3,
    label: "Total Half Day",
    value: 1,
    icon: calendar_icon,
    iconBackground: "#F92F91",
    iconBorder: "#FEEAF4",
  },
  {
    id: 4,
    label: "Total Holiday",
    value: 8,
    icon: calendar_off_icon,
    iconBackground: "#1685DA",
    iconBorder: "#E8F3FB",
  },
];

const listAttendanceStatus: attendanceStatus[] = [
  {
    id: 0,
    label: "Total Present",
    color: "#EAEAEA",
  },
  {
    id: 1,
    label: "Total Absent",
    color: "#E03535",
  },
  {
    id: 2,
    label: "Total Late",
    color: "#FF7300",
  },
  {
    id: 3,
    label: "Total Half Day",
    color: "#F92F91",
  },
  {
    id: 4,
    label: "Total Holiday",
    color: "#1685DA",
  },
];

const month = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

const RenderCalendarCard = (Props: CalendarCard) => {
  return (
    <Toolbar
      sx={{
        padding: "0 !important",
        background: "#FFFFFF",
      }}
    >
      <Box
        sx={{
          flexGrow: 1,
          alignItems: "center",
          display: "flex",
          marginBottom: "16px",
        }}
      >
        {Props.list.map((card) => (
          <div
            key={card.id}
            style={{
              border: "1px solid #EAEAEA",
              borderRadius: "8px",
              padding: "16px",
              display: "flex",
              justifyContent: "flex-start",
              alignItems: "flex-start",
              minWidth: "215px",
              minHeight: "88px",
              marginRight: "12px",
            }}
          >
            <div
              style={{
                width: "42px",
                height: "42px",
                background: `${card.iconBackground}`,
                border: `2px solid ${card.iconBorder}`,
                borderRadius: "50%",
                padding: "9px 9px",
              }}
            >
              <img src={card.icon} width={20} height={20} />
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "flex-start",
                justifyContent: "center",
                marginLeft: "16px",
              }}
            >
              <Typography
                variant="body2"
                component="div"
                sx={{
                  fontSize: "12px",
                  fontWeight: "500",
                  color: "#020C1B",
                  marginBottom: "10px",
                }}
              >
                {card.label}
              </Typography>
              <Typography
                variant="body2"
                component="div"
                sx={{
                  fontSize: "24px",
                  fontWeight: "600",
                  color: "#020C1B",
                }}
              >
                {card.value}
              </Typography>
            </div>
          </div>
        ))}
      </Box>
    </Toolbar>
  );
};

const RenderTableHeader = (Props: tableHeader) => {
  const { onChangeYear, year } = Props;
  return (
    <Toolbar
      sx={{
        background: "#FFFFFF",
        padding: "0 !important",
      }}
    >
      <Box
        sx={{
          flexGrow: 1,
          alignItems: "center",
          display: "flex",
          justifyContent: "space-between",
          marginBottom: "16px",
        }}
      >
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "flex-start",
          }}
        >
          <Typography
            variant="h6"
            component="div"
            sx={{
              fontSize: "16px",
              fontWeight: "600",
              color: "#131626",
              marginRight: "10px",
            }}
          >
            YEAR:
          </Typography>
          <FormControl sx={{ m: 1, minWidth: 120 }}>
            <Select
              value={year}
              onChange={onChangeYear}
              displayEmpty
              inputProps={{ "aria-label": "Without label" }}
              IconComponent={() => (
                <img src={arrow_down} style={{ marginRight: "14px" }} />
              )}
              sx={{
                "& .css-1d3z3hw-MuiOutlinedInput-notchedOutline": {
                  border: "1px solid #EAEAEA",
                  borderRadius: "12px",
                },
                "& .MuiSelect-select": {
                  padding: "11px 16px",
                },
              }}
            >
              <MenuItem value="2019">2019</MenuItem>
              <MenuItem value="2020">2020</MenuItem>
              <MenuItem value="2021">2021</MenuItem>
              <MenuItem value="2022">2022</MenuItem>
            </Select>
          </FormControl>
        </div>
        <RenderStatus list={listAttendanceStatus} />
      </Box>
    </Toolbar>
  );
};

const RenderStatus = (Props: listStatus) => {
  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      {Props.list.map((status) => (
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            marginRight: "16px",
          }}
          key={status.id}
        >
          <div
            style={{
              background: `${status.color}`,
              width: "12px",
              height: "12px",
              borderRadius: "50%",
            }}
          ></div>
          <Typography
            variant="body2"
            component="div"
            sx={{ marginLeft: "4px" }}
          >
            {status.label}
          </Typography>
        </div>
      ))}
      <img src={option} width={4} height={18} style={{ marginLeft: "26px" }} />
    </div>
  );
};

const RenderAttendanceCalendar = (Props: calendarType) => {
  const { year } = Props;
  return (
    <div
      style={{
        width: "100%",
      }}
    >
      {_.chunk(new Array(416).fill(0), 13).map((item, rowIndex) => (
        <div key={rowIndex} style={{ display: "flex", width: "100%" }}>
          {item.map((i, colIndex) => (
            <div
              style={{
                border: "1px solid #F4F4F4",
                minWidth: "121px",
                minHeight: "75px",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                background: `${
                  FilterByRowCol(rowIndex, colIndex, year) !== null
                    ? `${
                        (
                          FilterByRowCol(
                            rowIndex,
                            colIndex,
                            year
                          ) as unknown as resultObject
                        ).background
                      }`
                    : ""
                }`,
              }}
            >
              <Typography
                variant="body2"
                component="div"
                display={`${
                  rowIndex === 0 && colIndex !== 0 ? "block" : "none"
                }`}
                sx={{
                  fontSize: "14px",
                  fontWeight: "600",
                  color: "#4E4E4E",
                }}
              >
                {month[colIndex - 1]}
              </Typography>
              <Typography
                variant="body2"
                component="div"
                display={`${
                  rowIndex === 0 && colIndex === 0 ? "block" : "none"
                }`}
                sx={{
                  fontSize: "14px",
                  fontWeight: "500",
                  color: "#4E4E4E",
                }}
              >
                Date\Month
              </Typography>
              <Typography
                variant="body2"
                component="div"
                display={`${
                  rowIndex !== 0 && colIndex == 0 ? "block" : "none"
                }`}
                sx={{
                  fontSize: "14px",
                  fontWeight: "500",
                  color: "#4E4E4E",
                }}
              >
                {rowIndex}
              </Typography>
              <div
                style={{
                  width: "12px",
                  height: "12px",
                  borderRadius: "50%",
                  background: `${
                    FilterByRowCol(rowIndex, colIndex, year) !== null
                      ? `${
                          (
                            FilterByRowCol(
                              rowIndex,
                              colIndex,
                              year
                            ) as unknown as resultObject
                          ).color
                        }`
                      : ""
                  }`,
                }}
              ></div>
            </div>
          ))}
        </div>
      ))}
    </div>
  );
};

const FilterByRowCol = (row: number, col: number, inputYear: string) => {
  let yearData = attendanceCalendar.filter((year) => {
    return year.year === inputYear;
  });
  for (let i = 0; i < yearData[0].data.length; i++) {
    for (let j = 0; j < yearData[0].data[i].data.length; j++) {
      if (
        yearData[0].data[i].data[j].date === row &&
        yearData[0].data[i].data[j].month === col
      ) {
        let result: resultObject = {
          background: yearData[0].data[i].background,
          color: yearData[0].data[i].color,
          name: yearData[0].data[i].name,
        };
        return result;
      }
    }
  }
  return null;
};

export const StaffAttendance: React.FC = () => {
  const [open, setOpen] = React.useState<boolean>(true);
  const [year, setYear] = React.useState<string>(
    new Date().getFullYear().toString()
  );

  const handleChangeValue = (event: SelectChangeEvent) => {
    setYear(event.target.value as unknown as string);
  };
  return (
    <Box>
      <Box
        sx={{
          width: "100%",
          padding: "16px",
          background: "#FFFFFF",
          borderRadius: "16px",
        }}
      >
        <RenderCalendarCard list={attendanceCheckList} />
        <RenderTableHeader year={year} onChangeYear={handleChangeValue} />
        <RenderAttendanceCalendar year={year} />
      </Box>
    </Box>
  );
};
