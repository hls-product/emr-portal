import React from "react";
import {
  Box,
  Toolbar,
  Button,
  Typography,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TablePagination,
  IconButton,
  useTheme,
} from "@mui/material";
import FirstPageIcon from "@mui/icons-material/FirstPage";
import KeyboardArrowLeft from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRight from "@mui/icons-material/KeyboardArrowRight";
import LastPageIcon from "@mui/icons-material/LastPage";

import { StyledInput } from "../../../layout/Header/style";
import { EnhancedActionToolbar } from "../EnhancedActionToolbar";

import secondarySearchIcon from "../../../assets/adminHeader/secondarySearchIcon.svg";
import download from "../../../assets/icon/download_icon.svg";
import bin from "../../../assets/icon/bin_icon.svg";

interface tableData {
  id: number;
  fileName: string;
  type: "PDF";
  size: number;
}

interface TablePaginationActionsProps {
  count: number;
  page: number;
  rowsPerPage: number;
  onPageChange: (
    event: React.MouseEvent<HTMLButtonElement>,
    newPage: number
  ) => void;
}

const tableHead: string[] = ["#", "File name", "Type", "Size", "Action"];

const createTableData = (
  id: number,
  fileName: string,
  type: "PDF",
  size: number
): tableData => {
  return {
    id,
    fileName,
    type,
    size,
  };
};

const rows = [
  createTableData(0, "Joining Letter", "PDF", 1024),
  createTableData(1, "Joining Letter", "PDF", 1024),
  createTableData(2, "Joining Letter", "PDF", 1024),
  createTableData(3, "Joining Letter", "PDF", 1024),
  createTableData(4, "Joining Letter", "PDF", 1024),
  createTableData(5, "Joining Letter", "PDF", 1024),
  createTableData(6, "Joining Letter", "PDF", 1024),
  createTableData(7, "Joining Letter", "PDF", 1024),
];

function TablePaginationActions(props: TablePaginationActionsProps) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ flexShrink: 0, ml: 2.5 }}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowRight />
        ) : (
          <KeyboardArrowLeft />
        )}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowLeft />
        ) : (
          <KeyboardArrowRight />
        )}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
}

const RenderTableHead = () => {
  return (
    <Toolbar
      sx={{
        background: "#FFFFFF",
        padding: "0 !important",
      }}
    >
      <Box
        sx={{
          flexGrow: 1,
          alignItems: "center",
          display: "flex",
          justifyContent: "space-between",
          marginBottom: "16px",
        }}
      >
        <Typography
          variant="h6"
          component="div"
          sx={{
            fontSize: "16px",
            fontWeight: "600",
            color: "#131626",
            textTransform: "uppercase",
          }}
        >
          Documents list
        </Typography>
        <div
          style={{
            display: "flex",
            width: "fit-content",
            border: "1px solid #EAEAEA",
            borderRadius: "12px",
          }}
        >
          <img
            src={secondarySearchIcon}
            alt="Search"
            width={20}
            height={20}
            style={{ margin: "14px 0 14px 14px" }}
          />
          <StyledInput
            sx={{ m: 1, width: "fit-content" }}
            placeholder="Search documents..."
          />
        </div>
      </Box>
    </Toolbar>
  );
};

export const ProfileDocs: React.FC = () => {
  const [open, setOpen] = React.useState<boolean>(true);
  const [rowsPerPage, setRowsPerPage] = React.useState<number>(5);
  const [page, setPage] = React.useState<number>(0);


  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

  return (
    <Box>
      <Box
        sx={{
          width: "100%",
          padding: "16px",
          background: "#FFFFFF",
          borderRadius: "16px",
        }}
      >
        <RenderTableHead />
        <TableContainer>
          <Table>
            <TableHead
              sx={{
                background: "#FAFAFA",
              }}
            >
              <TableRow>
                {tableHead.map((cell) => (
                  <TableCell
                    align={`${
                      cell === "#"
                        ? "center"
                        : cell === "Action"
                        ? "center"
                        : "left"
                    }`}
                    sx={{
                      borderBottom: "1px solid #EAEAEA",
                      color: "#131626",
                      fontSize: "14px",
                      fontWeight: "600",
                    }}
                  >
                    {cell}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {(rowsPerPage > 0
                ? rows.slice(
                    page * rowsPerPage,
                    page * rowsPerPage + rowsPerPage
                  )
                : rows
              ).map((row) => (
                <TableRow>
                  <TableCell
                    align="center"
                    sx={{
                      borderBottom: "1px solid #EAEAEA",
                    }}
                  >
                    {row.id}
                  </TableCell>
                  <TableCell
                    sx={{
                      borderBottom: "1px solid #EAEAEA",
                    }}
                    align="left"
                  >
                    {row.fileName}
                  </TableCell>
                  <TableCell
                    align="left"
                    sx={{
                      borderBottom: "1px solid #EAEAEA",
                    }}
                  >
                    {row.type}
                  </TableCell>
                  <TableCell
                    align="left"
                    sx={{
                      borderBottom: "1px solid #EAEAEA",
                    }}
                  >
                    {row.size} KB
                  </TableCell>
                  <TableCell
                    align="left"
                    sx={{
                      borderBottom: "1px solid #EAEAEA",
                    }}
                  >
                    <div
                      style={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        gap: "5px",
                      }}
                    >
                      <img
                        src={download}
                        width={20}
                        height={20}
                        onClick={() => {}}
                        style={{ cursor: "pointer" }}
                      />
                      <img
                        src={bin}
                        width={20}
                        height={20}
                        onClick={() => {}}
                        style={{ cursor: "pointer" }}
                      />
                    </div>
                  </TableCell>
                </TableRow>
              ))}
              {emptyRows > 0 && (
                <TableRow style={{ height: 53 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
          ActionsComponent={TablePaginationActions}
        />
      </Box>
    </Box>
  );
};
