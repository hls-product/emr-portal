import React from "react";
import {
  Box,
  Toolbar,
  Button,
  Typography,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TablePagination,
  IconButton,
  useTheme,
} from "@mui/material";
import FirstPageIcon from "@mui/icons-material/FirstPage";
import KeyboardArrowLeft from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRight from "@mui/icons-material/KeyboardArrowRight";
import LastPageIcon from "@mui/icons-material/LastPage";

import { StyledInput } from "../../../layout/Header/style";
import { EnhancedActionToolbar } from "../EnhancedActionToolbar";

import cash_in from "../../../assets/icon/cash_in.svg";
import cash_total from "../../../assets/icon/cash_total.svg";
import cash_total_reverse from "../../../assets/icon/cash_total_reverse.svg";
import cash_reInvest from "../../../assets/icon/cash_reInvest.svg";
import secondarySearchIcon from "../../../assets/adminHeader/secondarySearchIcon.svg";

interface tableCard {
  id: number;
  label: string;
  revenue: number;
  icon: string;
  iconBackground: string;
  iconBorder: string;
}

interface TableCardList {
  list: tableCard[];
}

interface tableData {
  id: number;
  monthYear: string;
  date: string;
  mode: "Cash" | "Transfer to Bank Account";
  status: "Paid" | "UnPaid" | "Generated";
  netSalary: number;
}

interface TablePaginationActionsProps {
  count: number;
  page: number;
  rowsPerPage: number;
  onPageChange: (
    event: React.MouseEvent<HTMLButtonElement>,
    newPage: number
  ) => void;
}

const listTableCard: tableCard[] = [
  {
    id: 0,
    label: "Total Net Salary Paid",
    revenue: 7862,
    icon: cash_total,
    iconBackground: "#1685DA",
    iconBorder: "#E8F3FB",
  },
  {
    id: 1,
    label: "Total Cross Salary Paid",
    revenue: 3542,
    icon: cash_total_reverse,
    iconBackground: "#E03535",
    iconBorder: "#FCEBEB",
  },
  {
    id: 2,
    label: "Total Earning",
    revenue: 6566,
    icon: cash_in,
    iconBackground: "#22AA4F",
    iconBorder: "#E9F7ED",
  },
  {
    id: 3,
    label: "Total Reduction",
    revenue: 4314,
    icon: cash_reInvest,
    iconBackground: "#F92F91",
    iconBorder: "#FEEAF4",
  },
];

const tableHead: string[] = [
  "#",
  "Month-Year",
  "Date",
  "Mode",
  "Status",
  "Net Salary ($)",
  "Action",
];

const createTableData = (
  id: number,
  monthYear: string,
  date: string,
  mode: "Cash" | "Transfer to Bank Account",
  status: "Paid" | "UnPaid" | "Generated",
  netSalary: number
): tableData => {
  return {
    id,
    monthYear,
    date,
    mode,
    status,
    netSalary,
  };
};

const rows = [
  createTableData(
    1,
    "September 2021",
    "27/09/2021",
    "Cash",
    "Generated",
    27090
  ),
  createTableData(2, "September 2021", "27/09/2021", "Cash", "UnPaid", 27090),
  createTableData(
    3,
    "September 2021",
    "27/09/2021",
    "Transfer to Bank Account",
    "Paid",
    27090
  ),
  createTableData(
    4,
    "September 2021",
    "27/09/2021",
    "Transfer to Bank Account",
    "Paid",
    27090
  ),
  createTableData(5, "September 2021", "27/09/2021", "Cash", "Paid", 27090),
  createTableData(
    6,
    "September 2021",
    "27/09/2021",
    "Transfer to Bank Account",
    "UnPaid",
    27090
  ),
  createTableData(7, "September 2021", "27/09/2021", "Cash", "Paid", 27090),
  createTableData(
    8,
    "September 2021",
    "27/09/2021",
    "Transfer to Bank Account",
    "Paid",
    27090
  ),
  createTableData(9, "September 2021", "27/09/2021", "Cash", "Paid", 27090),
  createTableData(
    10,
    "September 2021",
    "27/09/2021",
    "Transfer to Bank Account",
    "UnPaid",
    27090
  ),
  createTableData(11, "September 2021", "27/09/2021", "Cash", "Paid", 27090),
  createTableData(12, "September 2021", "27/09/2021", "Cash", "Paid", 27090),
  createTableData(13, "September 2021", "27/09/2021", "Cash", "Paid", 27090),
  createTableData(
    14,
    "September 2021",
    "27/09/2021",
    "Transfer to Bank Account",
    "Generated",
    27090
  ),
  createTableData(
    15,
    "September 2021",
    "27/09/2021",
    "Transfer to Bank Account",
    "Paid",
    27090
  ),
  createTableData(
    16,
    "September 2021",
    "27/09/2021",
    "Transfer to Bank Account",
    "UnPaid",
    27090
  ),
  createTableData(17, "September 2021", "27/09/2021", "Cash", "Paid", 27090),
  createTableData(
    18,
    "September 2021",
    "27/09/2021",
    "Cash",
    "Generated",
    27090
  ),
  createTableData(
    19,
    "September 2021",
    "27/09/2021",
    "Transfer to Bank Account",
    "Paid",
    27090
  ),
  createTableData(20, "September 2021", "27/09/2021", "Cash", "UnPaid", 27090),
  createTableData(
    21,
    "September 2021",
    "27/09/2021",
    "Transfer to Bank Account",
    "Paid",
    27090
  ),
  createTableData(22, "September 2021", "27/09/2021", "Cash", "UnPaid", 27090),
  createTableData(
    23,
    "September 2021",
    "27/09/2021",
    "Transfer to Bank Account",
    "UnPaid",
    27090
  ),
  createTableData(
    24,
    "September 2021",
    "27/09/2021",
    "Transfer to Bank Account",
    "Generated",
    27090
  ),
];

function TablePaginationActions(props: TablePaginationActionsProps) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ flexShrink: 0, ml: 2.5 }}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowRight />
        ) : (
          <KeyboardArrowLeft />
        )}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowLeft />
        ) : (
          <KeyboardArrowRight />
        )}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
}

const RenderTableCard = (Props: TableCardList) => {
  return (
    <Toolbar
      sx={{
        padding: "0 !important",
        background: "#FFFFFF",
      }}
    >
      <Box sx={{ flexGrow: 1, alignItems: "center", display: "flex" }}>
        {Props.list.map((card) => (
          <div
            key={card.id}
            style={{
              border: "1px solid #EAEAEA",
              borderRadius: "8px",
              padding: "16px",
              display: "flex",
              justifyContent: "flex-start",
              alignItems: "flex-start",
              minWidth: "272px",
              minHeight: "88px",
              marginRight: "12px",
            }}
          >
            <div
              style={{
                width: "42px",
                height: "42px",
                background: `${card.iconBackground}`,
                border: `2px solid ${card.iconBorder}`,
                borderRadius: "50%",
                padding: "10px 9px",
              }}
            >
              <img src={card.icon} width={20} height={20} />
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "flex-start",
                justifyContent: "center",
                marginLeft: "16px",
              }}
            >
              <Typography
                variant="body2"
                component="div"
                sx={{
                  fontSize: "12px",
                  fontWeight: "500",
                  color: "#020C1B",
                  marginBottom: "10px",
                }}
              >
                {card.label}
              </Typography>
              <Typography
                variant="body2"
                component="div"
                sx={{
                  fontSize: "24px",
                  fontWeight: "600",
                  color: "#020C1B",
                }}
              >
                ${card.revenue}
              </Typography>
            </div>
          </div>
        ))}
      </Box>
    </Toolbar>
  );
};

const RenderTableHead = () => {
  return (
    <Toolbar
      sx={{
        background: "#FFFFFF",
        padding: "0 !important",
      }}
    >
      <Box
        sx={{
          flexGrow: 1,
          alignItems: "center",
          display: "flex",
          justifyContent: "space-between",
          margin: "16px 0",
        }}
      >
        <Typography
          variant="h6"
          component="div"
          sx={{
            fontSize: "16px",
            fontWeight: "600",
            color: "#131626",
            textTransform: "uppercase",
          }}
        >
          Payslip list
        </Typography>
        <div
          style={{
            display: "flex",
            width: "fit-content",
            border: "1px solid #EAEAEA",
            borderRadius: "12px",
          }}
        >
          <img
            src={secondarySearchIcon}
            alt="Search"
            width={20}
            height={20}
            style={{ margin: "14px 0 14px 14px" }}
          />
          <StyledInput
            sx={{ m: 1, width: "fit-content" }}
            placeholder="Search staff..."
          />
        </div>
      </Box>
    </Toolbar>
  );
};

export const ProfilePayroll: React.FC = () => {
  const [open, setOpen] = React.useState<boolean>(true);
  const [rowsPerPage, setRowsPerPage] = React.useState<number>(5);
  const [page, setPage] = React.useState<number>(0);

  const handleInteractSidebar = () => {
    setOpen(!open);
  };

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

  return (
    <Box>
      <Box
        sx={{
          width: "100%",
          padding: "16px",
          background: "#FFFFFF",
          borderRadius: "16px",
        }}
      >
        <RenderTableCard list={listTableCard} />
        <RenderTableHead />
        <TableContainer>
          <Table>
            <TableHead
              sx={{
                background: "#FAFAFA",
              }}
            >
              <TableRow>
                {tableHead.map((cell) => (
                  <TableCell
                    align={`${
                      cell === "#"
                        ? "center"
                        : cell === "Action"
                        ? "center"
                        : "left"
                    }`}
                    sx={{
                      borderBottom: "1px solid #EAEAEA",
                      color: "#131626",
                      fontSize: "14px",
                      fontWeight: "600",
                    }}
                  >
                    {cell}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {(rowsPerPage > 0
                ? rows.slice(
                    page * rowsPerPage,
                    page * rowsPerPage + rowsPerPage
                  )
                : rows
              ).map((row) => (
                <TableRow>
                  <TableCell
                    align="center"
                    sx={{
                      borderBottom: "1px solid #EAEAEA",
                    }}
                  >
                    {row.id}
                  </TableCell>
                  <TableCell
                    sx={{
                      borderBottom: "1px solid #EAEAEA",
                    }}
                    align="left"
                  >
                    {row.monthYear}
                  </TableCell>
                  <TableCell
                    align="left"
                    sx={{
                      borderBottom: "1px solid #EAEAEA",
                    }}
                  >
                    {row.date}
                  </TableCell>
                  <TableCell
                    align="left"
                    sx={{
                      borderBottom: "1px solid #EAEAEA",
                    }}
                  >
                    {row.mode}
                  </TableCell>
                  <TableCell
                    align="left"
                    sx={{
                      borderBottom: "1px solid #EAEAEA",
                    }}
                  >
                    <div
                      style={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "flex-start",
                      }}
                    >
                      <div
                        style={{
                          width: "12px",
                          height: "12px",
                          borderRadius: "50%",
                          marginRight: "10px",
                          background: `${
                            row.status === "Generated"
                              ? "#FF7300"
                              : row.status === "Paid"
                              ? "#22AA4F"
                              : "#E03535"
                          }`,
                        }}
                      ></div>
                      {row.status}
                    </div>
                  </TableCell>
                  <TableCell
                    align="left"
                    sx={{
                      borderBottom: "1px solid #EAEAEA",
                    }}
                  >
                    {row.netSalary}
                  </TableCell>
                  <TableCell
                    align="center"
                    sx={{
                      borderBottom: "1px solid #EAEAEA",
                    }}
                  >
                    <Button
                      sx={{
                        borderRadius: "99px",
                        border: "1px solid #F4F4F4",
                        fontSize: "14px",
                        fontWeight: "600",
                        width: "112px",
                        height: "40px",
                      }}
                    >
                      View Payslip
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
              {emptyRows > 0 && (
                <TableRow style={{ height: 53 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
          ActionsComponent={TablePaginationActions}
        />
      </Box>
    </Box>
  );
};
