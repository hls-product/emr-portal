import { ArrowBack } from "@mui/icons-material";
import { Box, Button, Grid, Typography } from "@mui/material";
import React from "react";
import { useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { Tab } from "../../components/tab";
import { selectProfileStaff } from "../../store/reducers/human/humanResourceSlice";
import { EnhancedActionToolbar } from "./EnhancedActionToolbar";
import { Detail } from "./practitionerDetails/Detail";
import { ProfileDocs } from "./practitionerDetails/ProfileDocs";
import { ProfilePayroll } from "./practitionerDetails/ProfilePayroll";
import { StaffAttendance } from "./practitionerDetails/StaffAttendance";

export const Profile: React.FC = () => {
  const navigate = useNavigate();
  const tabList = [
    {
      element: <Detail />,
      title: "General",
    },
    {
      element: <ProfilePayroll />,
      title: "Payroll",
    },
    {
      element: <StaffAttendance />,
      title: "Staff Attendance",
    },
    {
      element: <ProfileDocs />,
      title: "Documents",
    },
  ];
  return (
    <Box>
      <Box sx={{display: "flex", marginBottom: "16px", alignItems: "center", gap: "8px"}}>
      <Button
        onClick={() => navigate("/admin/human")}
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          "&:active": {
            color: "#6C5DD3"
          }
        }}
        children={<ArrowBack />}
      />
      <Typography
        component="div"
        sx={{
          fontSize: "24px",
          fontWeight: "600",
        }}
        children="Profile"
      />
      </Box>
      <Tab children={tabList} defaultValue={0} />
    </Box>
  );
};
