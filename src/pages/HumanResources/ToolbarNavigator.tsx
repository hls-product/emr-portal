import React from 'react'
import { Toolbar, Box, Button } from '@mui/material';
import {useNavigate} from 'react-router-dom'

export interface navigationLink {
    id: number;
    label: string;
    url: string;
}

export interface Props {
    defaultValue: number;
    listLink: navigationLink[];
}

export const listNavigationLink: navigationLink[] = [
    {
        id: 0,
        label: 'Profile',
        url: '',
    },
    {
        id: 1,
        label: 'Payroll',
        url: '/profile',
    },
    {
        id: 2,
        label: 'Staff Ateendance',
        url: '/profile/staff',
    },
    {
        id: 3,
        label: 'Documents',
        url: '/profile/docs',
    }
]

export const ToolbarNavigator: React.FC<Props> = ({defaultValue, listLink}) =>{
    const navigation = useNavigate();
    const [selected, setSelected] = React.useState<number>(defaultValue);
    return <Toolbar sx={{
        padding: '16px !important',
        borderRadius: '16px',
        background: '#FFFFFF',
        marginBottom: '24px'
    }}>
        <Box sx={{ flexGrow: 1, alignItems: 'center', }}>
            {listLink.map((link)=>(
                <Button key={link.id} 
                variant={`${selected === link.id ? 'contained' : 'text'}`}
                onClick={()=>{
                    setSelected(link.id);
                    navigation(link.url);
                }}
                sx={{
                    padding: '9px 16px',
                    color: `${selected === link.id ? '#FFFFFF' : '#4E4E4E'}`,
                    borderRadius: '10px',
                    marginRight: '7px'
                }}>
                    {link.label}
                </Button>
            ))}
        </Box>
    </Toolbar>
}

