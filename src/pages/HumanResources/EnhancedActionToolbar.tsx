import { Box, Button, Toolbar } from "@mui/material";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

type toolbarStatusType = {
  status: number;
};

export const EnhancedActionToolbar = (props: toolbarStatusType) => {
  const [selectedTab, setSelectedTab] = React.useState<number>(props.status);
  const navigate = useNavigate();

  return (
    <Toolbar
      sx={{
        borderRadius: "8px",
        marginBottom: "16px",
        padding: "12px",
        marginTop: "24px",
        background: "#FFFFFF",
        fontSize: "14px",
        fontWeight: 600,
      }}
    >
      <Box sx={{ flexGrow: 1, alignItems: "center" }}>
        <Button
          variant={`${selectedTab === 1 ? "contained" : "text"}`}
          onClick={() => {
            if (selectedTab !== 1) {
              setSelectedTab(1);
              navigate("/admin/human/profile");
            }
          }}
          key={1}
          sx={{
            padding: "9px 16px",
            borderRadius: "10px",
            mr: 0.5,
            color: `${selectedTab === 1 ? "" : "#4E4E4E"}`,
          }}
        >
          Profile
        </Button>
        <Button
          variant={`${selectedTab === 2 ? "contained" : "text"}`}
          onClick={() => {
            if (selectedTab !== 2) {
              setSelectedTab(2);
              navigate(`/admin/human/profile/payroll`);
            }
          }}
          key={2}
          sx={{
            padding: "9px 16px",
            borderRadius: "10px",
            mr: 0.5,
            color: `${selectedTab === 2 ? "" : "#4E4E4E"}`,
          }}
        >
          Payroll
        </Button>

        <Button
          variant={`${selectedTab === 3 ? "contained" : "text"}`}
          onClick={() => {
            if (selectedTab !== 3) {
              setSelectedTab(3);
              navigate(`/admin/human/profile/staff-attendance`);
            }
          }}
          key={3}
          sx={{
            padding: "9px 16px",
            borderRadius: "10px",
            mr: 0.5,
            color: `${selectedTab === 3 ? "" : "#4E4E4E"}`,
          }}
        >
          Staff Attendance
        </Button>

        <Button
          variant={`${selectedTab === 4 ? "contained" : "text"}`}
          onClick={() => {
            if (selectedTab !== 4) {
              setSelectedTab(4);
              navigate("/admin/human/profile/documents");
            }
          }}
          key={4}
          sx={{
            padding: "9px 16px",
            borderRadius: "10px",
            mr: 0.5,
            color: `${selectedTab === 4 ? "" : "#4E4E4E"}`,
          }}
        >
          Documents
        </Button>
      </Box>
    </Toolbar>
  );
};
