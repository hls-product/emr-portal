import { Circle, Search } from "@mui/icons-material";
import {
  Box,
  Button,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  FormControl,
  IconButton,
  InputAdornment,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  TextField,
  Toolbar,
  Typography,
  createTheme,
  ThemeProvider,
} from "@mui/material";
import React, { useState } from "react";
import MUIRichTextEditor from "mui-rte";
import { useNavigate } from "react-router-dom";
import { colorPalette } from "../../theme";
import editIcon from "../PatientDetails/icon/edit.svg";
import deleteIcon from "../PatientDetails/icon/delete.svg";
import downloadIcon from "../PatientDetails/icon/download.svg";
import AddIcon from "@mui/icons-material/Add";
import { ReactComponent as Done } from "./TickIcon.svg";
import { StyledTabContainer } from "../PatientDetails/style";
import { EnhancedActionToolbar } from "./PsychologicalTest";
import InvertColorsIcon from "@mui/icons-material/InvertColors";
import FormatBoldIcon from "@mui/icons-material/FormatBold";
import { borderRadius } from "@mui/system";

const patients = new Array(1).fill({
  name: "Exercise 1",
  answer: "1231",
  visit: "17",
  status: "Done",
  date: "27 Aug 2022",
});

patients.push({
  name: "Exercise 2",
  answer: "1231",
  visit: "17",
  status: "Done",
  date: "27 Aug 2022",
});

patients.push({
  name: "Exercise 3",
  answer: "1231",
  visit: "17",
  status: "Done",
  date: "27 Aug 2022",
});
patients.push({
  name: "Exercise 4",
  answer: "1231",
  visit: "17",
  status: "Done",
  date: "27 Aug 2022",
});
patients.push({
  name: "Exercise 5",
  answer: "1231",
  visit: "17",
  status: "Done",
  date: "27 Aug 2022",
});

interface THeadProps {
  numSelected: number;
  onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
  rowCount: number;
}

const THead: React.FC<THeadProps> = (props) => (
  <TableHead>
    <TableRow sx={{ backgroundColor: "#FAFAFA", stroke: "#EAEAEA" }}>
      <TableCell>
        <Checkbox
          indeterminate={
            props.numSelected > 0 && props.numSelected < props.rowCount
          }
          checked={props.numSelected === props.rowCount}
          onChange={props.onSelectAllClick}
        />
      </TableCell>
      <TableCell>
        <Typography variant="h6">Excercise Name</Typography>
      </TableCell>
      <TableCell>
        <Typography variant="h6">Date</Typography>
      </TableCell>
      <TableCell align="left">
        <Typography variant="h6">Visit</Typography>
      </TableCell>
      <TableCell align="center">
        <Typography variant="h6">Status</Typography>
      </TableCell>
      <TableCell align="center">
        <Typography variant="h6">Action</Typography>
      </TableCell>
    </TableRow>
  </TableHead>
);

export const ExcercisePost: React.FC = () => {
  const [selected, setSelected] = React.useState<readonly number[]>([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [searchTerm, setSearchTerm] = React.useState<string>("");
  const shrink = searchTerm.length > 0;

  const [isOpen, setIsOpen] = useState<boolean>(false);

  const handleClickOpen = () => {
    setIsOpen(true);
  };

  const handleClose = () => {
    setIsOpen(false);
  };

  function handleSelectAllClick(e: React.ChangeEvent<HTMLInputElement>) {
    setSelected(e.target.checked ? patients.map((_, index) => index) : []);
  }

  function handleClick(e: React.ChangeEvent<HTMLInputElement>, idx: number) {
    setSelected(
      selected.includes(idx)
        ? selected.filter((item) => item !== idx)
        : selected.concat(idx)
    );
  }

  function renderStatus(param: any) {
    switch (param) {
      case "Done":
        return <Done />;
    }
  }

  const [questionGroup, setQuestionGroup] = React.useState("Reasoning");
  const quesGroup = [
    { value: "Reasoning" },
    { value: "A" },
    { value: "B" },
    { value: "C" },
  ];

  const handleChangeQuestionGroup = (event: SelectChangeEvent) => {
    setQuestionGroup(event.target.value);
  };

  const [questionType, setQuestionType] = React.useState("Single Answer");
  const quesType = [
    { value: "Single Answer" },
    { value: "Short Text" },
    { value: "Ratio Answer" },
    { value: "Multiple Answer" },
    { value: "Long Text" },
    { value: "Number" },
    { value: "Date" },
    { value: "Time" },
    { value: "Dropdown" },
  ];

  const handleChangeQuestionType = (event: SelectChangeEvent) => {
    setQuestionType(event.target.value);
  };

  return (
    <Box>
      <div className="weight-600 size-24px">Excercise Post</div>

      <Box
        sx={{
          width: "100%",
          backgroundColor: colorPalette.white,
          borderRadius: "16px",
          paddingLeft: "20px",
          paddingTop: "10px",
          marginTop: "24px",
        }}
      >
        <StyledTabContainer
          sx={{ fontWeight: "600", fontSize: "16px", color: "#131626" }}
        >
          <Typography variant="h5" sx={{ textTransform: "uppercase" }}>
            Excercise Post list
          </Typography>
          <Box sx={{ display: "flex", gap: "16px" }}>
            <TextField
              label="Search"
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <Search sx={{ height: 20 }} />
                  </InputAdornment>
                ),
              }}
              onChange={(e) => setSearchTerm(e.target.value)}
              InputLabelProps={{
                shrink,
                sx: {
                  transform: shrink ? "" : "translate(45px, 12px) scale(1)",
                  color: "#9FA0A6",
                },
              }}
              variant="outlined"
              sx={{
                "& .MuiInputBase-root": {
                  height: 48,
                  width: 300,
                  borderRadius: 3,
                },
              }}
            />
            <Button
              variant="contained"
              sx={{ borderRadius: "99px", marginRight: "20px", height: "48px" }}
              onClick={handleClickOpen}
            >
              <AddIcon />
              <Typography
                sx={{ fontSize: "14px", fontWeight: "600", marginLeft: "10px" }}
              >
                New Excercise Post
              </Typography>
            </Button>
          </Box>
        </StyledTabContainer>
        <Table>
          <THead
            numSelected={selected.length}
            onSelectAllClick={handleSelectAllClick}
            rowCount={patients.length}
          />
          <TableBody>
            {patients
              .map(({ name, visit, status, date }, index) => (
                <TableRow key={index}>
                  <TableCell style={{ width: "8%" }}>
                    <Checkbox
                      checked={selected.includes(index)}
                      onChange={(e) => handleClick(e, index)}
                    />
                  </TableCell>
                  <TableCell
                    style={{
                      width: "45%",
                      fontSize: "14px",
                      fontWeight: "500",
                    }}
                  >
                    {name}
                  </TableCell>
                  <TableCell
                    style={{
                      width: "15%",
                      fontSize: "14px",
                      fontWeight: "500",
                    }}
                  >
                    {date}
                  </TableCell>
                  <TableCell style={{ fontSize: "14px", fontWeight: "500" }}>
                    {visit}
                  </TableCell>

                  <TableCell align="center" style={{ width: "10%" }}>
                    {" "}
                    {renderStatus(status)}{" "}
                  </TableCell>

                  <TableCell align="center" sx={{ width: "10%" }}>
                    <IconButton>
                      <img src={editIcon} />
                    </IconButton>
                    <IconButton>
                      <img src={deleteIcon} />
                    </IconButton>
                  </TableCell>
                </TableRow>
              ))
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)}
          </TableBody>
        </Table>
        <TablePagination
          count={patients.length}
          component="div"
          rowsPerPageOptions={[5, 10, 25, 50]}
          page={page}
          rowsPerPage={rowsPerPage}
          onPageChange={(ev, newValue) => setPage(newValue)}
          onRowsPerPageChange={(e) => {
            setRowsPerPage(parseInt(e.target.value, 10));
            setPage(0);
          }}
          showFirstButton
          showLastButton
        />
      </Box>

      <Dialog
        open={isOpen}
        onClose={handleClose}
        fullWidth
        PaperProps={{
          style: { borderRadius: "18px" },
        }}
      >
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          <DialogTitle sx={{ fontSize: "18px" }}>
            Add Excercise Post
          </DialogTitle>
          <DialogTitle
            sx={{
              fontSize: "16px",
              fontWeight: "600",
              color: "#4A5CD0",
              cursor: "pointer",
            }}
            onClick={handleClose}
          >
            Cancel
          </DialogTitle>
        </Box>

        <DialogContent
          sx={{ display: "flex", flexDirection: "column", gap: "26px" }}
        >
          <FormControl style={{ width: "100%" }}>
            <InputLabel sx={{ color: "black" }}>Question Group</InputLabel>
            <Select
              required
              fullWidth
              value={questionGroup}
              onChange={handleChangeQuestionGroup}
              label="Question Group"
              sx={{
                borderRadius: "12px",
                color: "black",
              }}
            >
              {quesGroup.map((option) => (
                <MenuItem value={option.value}>{option.value}</MenuItem>
              ))}
            </Select>
          </FormControl>

          <FormControl style={{ width: "100%" }}>
            <InputLabel sx={{ color: "black" }}>Question Type</InputLabel>
            <Select
              required
              fullWidth
              value={questionType}
              onChange={handleChangeQuestionType}
              label="Question Type"
              sx={{
                borderRadius: "12px",
                color: "black",
              }}
            >
              {quesType.map((option) => (
                <MenuItem value={option.value}>{option.value}</MenuItem>
              ))}
            </Select>
          </FormControl>

          <FormControl style={{ width: "100%" }}>
            <InputLabel sx={{ color: "black" }}>Difficult Level</InputLabel>
            <Select
              required
              fullWidth
              label="Difficult Level"
              defaultValue={2}
              sx={{
                borderRadius: "12px",
                color: "black",
              }}
            >
              <MenuItem value={1}>
                <Circle
                  sx={{
                    height: 12,
                    width: 12,
                    fill: "#E03535",
                    marginRight: "14px",
                  }}
                />
                Hard
              </MenuItem>

              <MenuItem value={2}>
                <Circle
                  sx={{
                    height: 12,
                    width: 12,
                    fill: "#FF7300",
                    marginRight: "14px",
                  }}
                />
                Medium
              </MenuItem>

              <MenuItem value={3}>
                <Circle
                  sx={{
                    height: 12,
                    width: 12,
                    fill: "#22AA4F",
                    marginRight: "14px",
                  }}
                />
                Easy
              </MenuItem>
            </Select>
          </FormControl>
          <Typography variant="h6">Exercise</Typography>

          <MUIRichTextEditor label="Enter..." inlineToolbar={true} />
        </DialogContent>
        <DialogActions>
          <Button
            variant="contained"
            onClick={handleClose}
            sx={{
              borderRadius: "99px",
              width: "131px",
              height: "50px",
              margin: "auto",
            }}
          >
            Save
          </Button>
        </DialogActions>
      </Dialog>
    </Box>
  );
};
