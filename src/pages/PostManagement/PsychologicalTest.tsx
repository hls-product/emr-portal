import { Circle, Search } from "@mui/icons-material";
import AddIcon from "@mui/icons-material/Add";
import VisibilityOutlinedIcon from "@mui/icons-material/VisibilityOutlined";
import {
  Box,
  Button,
  Checkbox,
  Dialog,
  DialogContent,
  DialogTitle,
  IconButton,
  InputAdornment,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  TextField,
  Toolbar,
  Typography
} from "@mui/material";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { colorPalette } from "../../theme";
import deleteIcon from "../PatientDetails/icon/delete.svg";
import editIcon from "../PatientDetails/icon/edit.svg";
import viewIcon from "../PatientDetails/icon/view.svg";
import { StyledTabContainer } from "../PatientDetails/style";
import { ReactComponent as AnsNum } from "./AnsNum.svg";

const patients = new Array(2).fill({
  name: "Test 1",
  gender: "Welcome Test",
  phone: "17",
  disease: "Long",
  status: "Draft",
  topic: "10/11/2021",
  date: "10/11/2021",
});

patients.push({
  name: "Test 2",
  gender: "Welcome Test",
  phone: "17",
  disease: "Long",
  status: "Published",
  topic: "10/11/2021",
  date: "10/11/2021",
});

patients.push({
  name: "Test 3",
  gender: "Welcome Test",
  phone: "17",
  disease: "Long",
  status: "Closed",
  topic: "10/11/2021",
  date: "10/11/2021",
});

patients.push({
  name: "Test 4",
  gender: "Welcome Test",
  phone: "17",
  disease: "Long",
  status: "Archived",
  topic: "10/11/2021",
  date: "10/11/2021",
});

interface THeadProps {
  numSelected: number;
  onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
  rowCount: number;
}

const THead: React.FC<THeadProps> = (props) => (
  <TableHead>
    <TableRow sx={{ backgroundColor: "#FAFAFA", stroke: "#EAEAEA" }}>
      <TableCell>
        <Checkbox
          indeterminate={
            props.numSelected > 0 && props.numSelected < props.rowCount
          }
          checked={props.numSelected === props.rowCount}
          onChange={props.onSelectAllClick}
        />
      </TableCell>
      <TableCell>
        <Typography sx={{ fontWeight: "600", fontSize: "14px" }}>
          Title
        </Typography>
      </TableCell>
      <TableCell>
        <Typography sx={{ fontWeight: "600", fontSize: "14px" }}>
          Topic
        </Typography>
      </TableCell>
      <TableCell>
        <Typography sx={{ fontWeight: "600", fontSize: "14px" }}>
          Answered
        </Typography>
      </TableCell>
      <TableCell>
        <Typography sx={{ fontWeight: "600", fontSize: "14px" }}>
          Viewed
        </Typography>
      </TableCell>
      <TableCell>
        <Typography sx={{ fontWeight: "600", fontSize: "14px" }}>
          Created Date
        </Typography>
      </TableCell>
      <TableCell>
        <Typography sx={{ fontWeight: "600", fontSize: "14px" }}>
          Edited Date
        </Typography>
      </TableCell>
      <TableCell>
        <Typography sx={{ fontWeight: "600", fontSize: "14px" }}>
          Status
        </Typography>
      </TableCell>
      <TableCell>
        <Typography
          sx={{ fontWeight: "600", fontSize: "14px", marginLeft: "37px" }}
        >
          Action
        </Typography>
      </TableCell>
    </TableRow>
  </TableHead>
);

type toolbarStatusType = {
  status: number;
};

export const EnhancedActionToolbar = (props: toolbarStatusType) => {
  const [selectedTab, setSelectedTab] = React.useState<number>(props.status);
  const navigate = useNavigate();
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const handleClickOpen = () => {
    setIsOpen(true);
  };

  const handleClose = () => {
    setIsOpen(false);
  };

  return (
    <Toolbar
      sx={{
        borderRadius: "8px",
        marginBottom: "16px",
        padding: "12px",
        marginTop: "24px",
        background: "#FFFFFF",
      }}
    >
      <Box sx={{ flexGrow: 1, alignItems: "center" }}>
        <Button
          variant={`${selectedTab === 1 ? "contained" : "text"}`}
          onClick={() => {
            if (selectedTab !== 1) {
              setSelectedTab(1);
              navigate("/admin/post");
            }
          }}
          key={1}
          sx={{
            padding: "9px 16px",
            borderRadius: "10px",
            mr: 0.5,
            color: `${selectedTab === 1 ? "" : "#4E4E4E"}`,
          }}
        >
          <Typography sx={{ fontSize: "14px", fontWeight: "600" }}>
            Psychological Test
          </Typography>
        </Button>
        <Button
          variant={`${selectedTab === 2 ? "contained" : "text"}`}
          onClick={() => {
            if (selectedTab !== 2) {
              setSelectedTab(2);
              navigate("/admin/post/test/questionBank");
            }
          }}
          key={1}
          sx={{
            padding: "9px 16px",
            borderRadius: "10px",
            mr: 0.5,
            color: `${selectedTab === 2 ? "" : "#4E4E4E"}`,
          }}
        >
          <Typography sx={{ fontSize: "14px", fontWeight: "600" }}>
            Question Bank
          </Typography>
        </Button>
        <Button
          variant={`${selectedTab === 3 ? "contained" : "text"}`}
          onClick={() => {
            if (selectedTab !== 3) {
              setSelectedTab(3);
              navigate("/admin/post/test/testResult");
            }
          }}
          key={1}
          sx={{
            padding: "9px 16px",
            borderRadius: "10px",
            mr: 0.5,
            color: `${selectedTab === 3 ? "" : "#4E4E4E"}`,
          }}
        >
          <Typography sx={{ fontSize: "14px", fontWeight: "600" }}>
            Test Result
          </Typography>
        </Button>
      </Box>
    </Toolbar>
  );
};

export const PsychologicalTest: React.FC = () => {
  const [selected, setSelected] = React.useState<readonly number[]>([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [searchTerm, setSearchTerm] = React.useState<string>("");
  const shrink = searchTerm.length > 0;
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const handleClickOpen = () => {
    setIsOpen(true);
  };

  const handleClose = () => {
    setIsOpen(false);
  };

  function handleSelectAllClick(e: React.ChangeEvent<HTMLInputElement>) {
    setSelected(e.target.checked ? patients.map((_, index) => index) : []);
  }

  function handleClick(e: React.ChangeEvent<HTMLInputElement>, idx: number) {
    setSelected(
      selected.includes(idx)
        ? selected.filter((item) => item !== idx)
        : selected.concat(idx)
    );
  }

  function renderCircle(param: any) {
    let fillColor = "";
    switch (param) {
      case "Draft":
        fillColor = "#9FA0A6";
        return fillColor;
      case "Published":
        fillColor = "#22AA4F";
        return fillColor;
      case "Closed":
        fillColor = "#E03535";
        return fillColor;
      case "Archived":
        fillColor = "#FF7300";
        return fillColor;
    }
  }

  return (
    <Box>
      <Box
        sx={{
          width: "100%",
          backgroundColor: colorPalette.white,
          borderRadius: "16px",
          paddingLeft: "20px",
          paddingTop: "10px",
        }}
      >
        <StyledTabContainer
          sx={{ fontWeight: "600", fontSize: "16px", color: "#131626" }}
        >
          <Typography variant="h5">PSYCHOLOGICAL TEST LIST</Typography>
          <Box sx={{ display: "flex", gap: "16px" }}>
            <TextField
              label="Search"
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <Search sx={{ height: 20 }} />
                  </InputAdornment>
                ),
              }}
              onChange={(e) => setSearchTerm(e.target.value)}
              InputLabelProps={{
                shrink,
                sx: {
                  transform: shrink ? "" : "translate(45px, 12px) scale(1)",
                  color: "#9FA0A6",
                },
              }}
              variant="outlined"
              sx={{
                "& .MuiInputBase-root": {
                  height: 48,
                  width: 300,
                  borderRadius: 3,
                },
              }}
            />
            <Button
              variant="contained"
              sx={{ borderRadius: "99px", marginRight: "20px", height: "48px" }}
            >
              <AddIcon />
              <Typography
                sx={{ fontSize: "14px", fontWeight: "600", marginLeft: "10px" }}
              >
                New Test
              </Typography>
            </Button>
          </Box>
        </StyledTabContainer>
        <Table>
          <THead
            numSelected={selected.length}
            onSelectAllClick={handleSelectAllClick}
            rowCount={patients.length}
          />
          <TableBody>
            {patients
              .map(
                (
                  { name, gender, phone, disease, status, topic, date },
                  index
                ) => (
                  <TableRow key={index}>
                    <TableCell style={{ width: "8%" }}>
                      <Checkbox
                        checked={selected.includes(index)}
                        onChange={(e) => handleClick(e, index)}
                      />
                    </TableCell>
                    <TableCell
                      style={{
                        width: "15%",
                        fontSize: "14px",
                        fontWeight: "500",
                      }}
                    >
                      {name}
                    </TableCell>
                    <TableCell style={{ fontSize: "14px", fontWeight: "500" }}>
                      {gender}
                    </TableCell>
                    <TableCell style={{ fontSize: "14px", fontWeight: "500" }}>
                      {phone}
                    </TableCell>
                    <TableCell style={{ fontSize: "14px", fontWeight: "500" }}>
                      {disease}
                    </TableCell>
                    <TableCell style={{ fontSize: "14px", fontWeight: "500" }}>
                      {topic}
                    </TableCell>
                    <TableCell style={{ fontSize: "14px", fontWeight: "500" }}>
                      {date}
                    </TableCell>
                    <TableCell>
                      {/* <Circle sx={{ height: 12, width: 12, fill: `${status === 1 ? '#E03535' : '#22AA4F'}` }}  />{" "} */}
                      <Circle
                        sx={{
                          height: 12,
                          width: 12,
                          fill: renderCircle(status),
                        }}
                      />{" "}
                      {status}
                    </TableCell>

                    <TableCell style={{ width: "12.9%" }}>
                      <IconButton onClick={handleClickOpen}>
                        <img src={viewIcon} />
                      </IconButton>
                      <IconButton sx={{ marginLeft: "5px" }}>
                        <img src={editIcon} />
                      </IconButton>
                      <IconButton sx={{ marginLeft: "5px" }}>
                        <img src={deleteIcon} />
                      </IconButton>
                    </TableCell>
                  </TableRow>
                )
              )
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)}
          </TableBody>
        </Table>
        <TablePagination
          count={patients.length}
          component="div"
          rowsPerPageOptions={[5, 10, 25, 50]}
          page={page}
          rowsPerPage={rowsPerPage}
          onPageChange={(ev, newValue) => setPage(newValue)}
          onRowsPerPageChange={(e) => {
            setRowsPerPage(parseInt(e.target.value, 10));
            setPage(0);
          }}
          showFirstButton
          showLastButton
        />
      </Box>

      <Dialog
        open={isOpen}
        onClose={handleClose}
        fullWidth={true}
        maxWidth={"md"}
        PaperProps={{
          style: { borderRadius: "18px" },
        }}
      >
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          <DialogTitle sx={{ fontSize: "18px" }}>Quickview</DialogTitle>
          <DialogTitle
            sx={{
              fontSize: "16px",
              fontWeight: "600",
              color: "#4A5CD0",
              cursor: "pointer",
            }}
            onClick={handleClose}
          >
            Cancel
          </DialogTitle>
        </Box>

        <DialogContent
          sx={{ display: "flex", flexDirection: "column", gap: "8px" }}
        >
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <Box
              sx={{
                display: "flex",
                width: "32%",
                height: "70px",
                background: "#E8F3FB",
                borderRadius: "8px",
                padding: "14px",
                gap: "13px",
              }}
            >
              <AnsNum />
              <div style={{ display: "flex", flexDirection: "column" }}>
                <Typography variant="h6">Số lượng đã trả lời</Typography>
                <Typography variant="h6" sx={{ color: "#1685DA" }}>
                  100
                </Typography>
              </div>
            </Box>

            <Box
              sx={{
                display: "flex",
                width: "32%",
                height: "70px",
                background: "#FFF1E6",
                borderRadius: "8px",
                padding: "14px",
                gap: "13px",
              }}
            >
              <VisibilityOutlinedIcon sx={{ color: "#FF7300" }} />
              <div style={{ display: "flex", flexDirection: "column" }}>
                <Typography variant="h6">Số lượng người xem</Typography>
                <Typography variant="h6" sx={{ color: "#FF7300" }}>
                  100
                </Typography>
              </div>
            </Box>

            <Box
              sx={{
                display: "flex",
                width: "32%",
                height: "70px",
                background: "#E9F7ED",
                borderRadius: "8px",
                padding: "14px",
                gap: "13px",
              }}
            >
              <VisibilityOutlinedIcon sx={{ color: "#1F9B48" }} />
              <div style={{ display: "flex", flexDirection: "column" }}>
                <Typography variant="h6">Bài làm gần nhất</Typography>
                <Typography variant="h6" sx={{ color: "#1F9B48" }}>
                  01/08/22 - 10:00 AM
                </Typography>
              </div>
            </Box>
          </Box>
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <Box
              sx={{
                display: "flex",
                width: "32%",
                height: "70px",
                border: "1px solid #F4F4F4",
                borderRadius: "8px",
                padding: "12px",
                gap: "13px",
              }}
            >
              <div>
                <Typography
                  sx={{
                    display: "inline",
                    fontSize: "24px",
                    fontWeight: "600",
                  }}
                >
                  1.5
                </Typography>
                <Typography variant="h6" sx={{ display: "inline" }}>
                  %
                </Typography>
              </div>
              <Typography variant="h6" sx={{ color: "#4E4E4E" }}>
                [Tỉ lệ kết quả theo từng phân loại nhóm bệnh]
              </Typography>
            </Box>

            <Box
              sx={{
                display: "flex",
                width: "32%",
                height: "70px",
                border: "1px solid #F4F4F4",
                borderRadius: "8px",
                padding: "12px",
                gap: "13px",
              }}
            >
              <div>
                <Typography
                  sx={{
                    display: "inline",
                    fontSize: "24px",
                    fontWeight: "600",
                  }}
                >
                  2
                </Typography>
                <Typography variant="h6" sx={{ display: "inline" }}>
                  %
                </Typography>
              </div>
              <Typography variant="h6" sx={{ color: "#4E4E4E" }}>
                Triệu chứng loạn dây thần kinh số 2
              </Typography>
            </Box>

            <Box
              sx={{
                display: "flex",
                width: "32%",
                height: "70px",
                border: "1px solid #F4F4F4",
                borderRadius: "8px",
                padding: "12px",
                gap: "13px",
              }}
            >
              <div>
                <Typography
                  sx={{
                    display: "inline",
                    fontSize: "24px",
                    fontWeight: "600",
                  }}
                >
                  2.8
                </Typography>
                <Typography variant="h6" sx={{ display: "inline" }}>
                  %
                </Typography>
              </div>
              <Typography variant="h6" sx={{ color: "#4E4E4E" }}>
                Triệu chứng loạn dây thần kinh số 3
              </Typography>
            </Box>
          </Box>

          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <Box
              sx={{
                display: "flex",
                width: "32%",
                height: "70px",
                border: "1px solid #F4F4F4",
                borderRadius: "8px",
                padding: "12px",
                gap: "13px",
              }}
            >
              <div>
                <Typography
                  sx={{
                    display: "inline",
                    fontSize: "24px",
                    fontWeight: "600",
                  }}
                >
                  3.5
                </Typography>
                <Typography variant="h6" sx={{ display: "inline" }}>
                  %
                </Typography>
              </div>
              <Typography variant="h6" sx={{ color: "#4E4E4E" }}>
                Bệnh liên quan đến lo âu
              </Typography>
            </Box>

            <Box
              sx={{
                display: "flex",
                width: "32%",
                height: "70px",
                border: "1px solid #F4F4F4",
                borderRadius: "8px",
                padding: "12px",
                gap: "13px",
              }}
            >
              <div>
                <Typography
                  sx={{
                    display: "inline",
                    fontSize: "24px",
                    fontWeight: "600",
                  }}
                >
                  2
                </Typography>
                <Typography variant="h6" sx={{ display: "inline" }}>
                  %
                </Typography>
              </div>
              <Typography variant="h6" sx={{ color: "#4E4E4E" }}>
                Triệu chứng loạn dây thần kinh số 2
              </Typography>
            </Box>

            <Box
              sx={{
                display: "flex",
                width: "32%",
                height: "70px",
                border: "1px solid #F4F4F4",
                borderRadius: "8px",
                padding: "12px",
                gap: "13px",
              }}
            >
              <div>
                <Typography
                  sx={{
                    display: "inline",
                    fontSize: "24px",
                    fontWeight: "600",
                  }}
                >
                  2.8
                </Typography>
                <Typography variant="h6" sx={{ display: "inline" }}>
                  %
                </Typography>
              </div>
              <Typography variant="h6" sx={{ color: "#4E4E4E" }}>
                Triệu chứng loạn dây thần kinh số 3
              </Typography>
            </Box>
          </Box>
        </DialogContent>
      </Dialog>
    </Box>
  );
};
