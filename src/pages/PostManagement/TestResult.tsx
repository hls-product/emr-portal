import { Circle, Search } from "@mui/icons-material";
import {
  Box,
  Button,
  Checkbox,
  FormControl,
  IconButton,
  InputAdornment,
  MenuItem,
  Select,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  TextField,
  Typography
} from "@mui/material";
import React, { useState } from "react";
import { colorPalette } from "../../theme";
import downloadIcon from "../PatientDetails/icon/download.svg";
import { StyledTabContainer } from "../PatientDetails/style";

const patients = new Array(2).fill({
  name: "Chemistry",
  classNumber: "Class 1",
  student: "Levina Akwei",
  status: "Pending",
  createDate: "20 May 2022",
  editDate: "20 May 2022",
});

patients.push({
  name: "Chemistry",
  classNumber: "Class 1",
  student: "Levina Akwei",
  status: "Submitted",
  createDate: "20 May 2022",
  editDate: "20 May 2022",
});

patients.push({
  name: "Chemistry",
  classNumber: "Class 1",
  student: "Levina Akwei",
  status: "Submitted",
  createDate: "20 May 2022",
  editDate: "20 May 2022",
});

patients.push({
  name: "Chemistry",
  classNumber: "Class 1",
  student: "Levina Akwei",
  status: "Pending",
  createDate: "20 May 2022",
  editDate: "20 May 2022",
});

interface THeadProps {
  numSelected: number;
  onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
  rowCount: number;
}

const THead: React.FC<THeadProps> = (props) => (
  <TableHead>
    <TableRow sx={{ backgroundColor: "#FAFAFA", stroke: "#EAEAEA" }}>
      <TableCell>
        <Checkbox
          indeterminate={
            props.numSelected > 0 && props.numSelected < props.rowCount
          }
          checked={props.numSelected === props.rowCount}
          onChange={props.onSelectAllClick}
        />
      </TableCell>
      <TableCell>
        <Typography sx={{ fontWeight: "600", fontSize: "14px" }}>
          Homework Title
        </Typography>
      </TableCell>
      <TableCell>
        <Typography sx={{ fontWeight: "600", fontSize: "14px" }}>
          Class
        </Typography>
      </TableCell>
      <TableCell>
        <Typography sx={{ fontWeight: "600", fontSize: "14px" }}>
          Student
        </Typography>
      </TableCell>
      <TableCell>
        <Typography sx={{ fontWeight: "600", fontSize: "14px" }}>
          Subject
        </Typography>
      </TableCell>
      <TableCell>
        <Typography sx={{ fontWeight: "600", fontSize: "14px" }}>
          Difficulty Level
        </Typography>
      </TableCell>
      <TableCell>
        <Typography sx={{ fontWeight: "600", fontSize: "14px" }}>
          Submitted Date
        </Typography>
      </TableCell>
      <TableCell>
        <Typography sx={{ fontWeight: "600", fontSize: "14px" }}>
          Submitted Date
        </Typography>
      </TableCell>
      <TableCell>
        <Typography
          sx={{ fontWeight: "600", fontSize: "14px", marginLeft: "0px" }}
        >
          Action
        </Typography>
      </TableCell>
    </TableRow>
  </TableHead>
);

type toolbarStatusType = {
  status: number;
};

export const TestResult: React.FC = () => {
  const [selected, setSelected] = React.useState<readonly number[]>([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [searchTerm, setSearchTerm] = React.useState<string>("");
  const shrink = searchTerm.length > 0;
  const [isOpen, setIsOpen] = useState<boolean>(false);

  function handleSelectAllClick(e: React.ChangeEvent<HTMLInputElement>) {
    setSelected(e.target.checked ? patients.map((_, index) => index) : []);
  }

  function handleClick(e: React.ChangeEvent<HTMLInputElement>, idx: number) {
    setSelected(
      selected.includes(idx)
        ? selected.filter((item) => item !== idx)
        : selected.concat(idx)
    );
  }

  function renderCircle(param: any) {
    let fillColor = "";
    switch (param) {
      case "Pending":
        fillColor = "#FF7300";
        return fillColor;
      case "Submitted":
        fillColor = "#22AA4F";
        return fillColor;
    }
  }

  return (
    <Box
      sx={{
        width: "100%",
        backgroundColor: colorPalette.white,
        borderRadius: "16px",
        paddingLeft: "20px",
        paddingTop: "10px",
      }}
    >
      <StyledTabContainer
        sx={{ fontWeight: "600", fontSize: "16px", color: "#131626" }}
      >
        <Typography variant="h5">TEST RESULT LIST</Typography>
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            gap: "16px",
          }}
        >
          <TextField
            label="Search"
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Search sx={{ height: 20 }} />
                </InputAdornment>
              ),
            }}
            onChange={(e) => setSearchTerm(e.target.value)}
            InputLabelProps={{
              shrink,
              sx: {
                transform: shrink ? "" : "translate(45px, 12px) scale(1)",
                color: "#9FA0A6",
              },
            }}
            variant="outlined"
            sx={{
              "& .MuiInputBase-root": {
                height: 48,
                width: 300,
                borderRadius: 3,
              },
            }}
          />
          <Typography sx={{ fontSize: "16px", fontWeight: "500" }}>
            Select Homework:
          </Typography>
          <FormControl sx={{ width: "198px" }}>
            <Select required defaultValue="1" sx={{ borderRadius: "12px" }}>
              <MenuItem value={1}>Homework A</MenuItem>
              <MenuItem value={2}>Homework B</MenuItem>
              <MenuItem value={3}>Homework C</MenuItem>
              <MenuItem value={4}>Homework D</MenuItem>
            </Select>
          </FormControl>
          <Button
            variant="contained"
            sx={{ borderRadius: "99px", marginRight: "20px", height: "48px" }}
          >
            <Typography sx={{ fontSize: "14px", fontWeight: "600" }}>
              Search
            </Typography>
          </Button>
        </Box>
      </StyledTabContainer>
      <Table>
        <THead
          numSelected={selected.length}
          onSelectAllClick={handleSelectAllClick}
          rowCount={patients.length}
        />
        <TableBody>
          {patients
            .map(
              (
                { name, classNumber, student, status, createDate, editDate },
                index
              ) => (
                <TableRow key={index}>
                  <TableCell style={{ width: "8%" }}>
                    <Checkbox
                      checked={selected.includes(index)}
                      onChange={(e) => handleClick(e, index)}
                    />
                  </TableCell>
                  <TableCell
                    style={{
                      width: "15%",
                      fontSize: "14px",
                      fontWeight: "500",
                    }}
                  >
                    {name}
                  </TableCell>

                  <TableCell style={{ fontSize: "14px", fontWeight: "500" }}>
                    {classNumber}
                  </TableCell>
                  <TableCell style={{ fontSize: "14px", fontWeight: "500" }}>
                    {student}
                  </TableCell>
                  <TableCell style={{ fontSize: "14px", fontWeight: "500" }}>
                    {name}
                  </TableCell>
                  <TableCell>
                    <Circle
                      sx={{
                        height: 12,
                        width: 12,
                        fill: renderCircle(status),
                      }}
                    />{" "}
                    {status}
                  </TableCell>
                  <TableCell style={{ fontSize: "14px", fontWeight: "500" }}>
                    {createDate}
                  </TableCell>
                  <TableCell style={{ fontSize: "14px", fontWeight: "500" }}>
                    {editDate}
                  </TableCell>
                  <TableCell style={{ width: "7%" }}>
                    <IconButton>
                      <img src={downloadIcon} />
                    </IconButton>
                  </TableCell>
                </TableRow>
              )
            )
            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)}
        </TableBody>
      </Table>
      <TablePagination
        count={patients.length}
        component="div"
        rowsPerPageOptions={[5, 10, 25, 50]}
        page={page}
        rowsPerPage={rowsPerPage}
        onPageChange={(ev, newValue) => setPage(newValue)}
        onRowsPerPageChange={(e) => {
          setRowsPerPage(parseInt(e.target.value, 10));
          setPage(0);
        }}
        showFirstButton
        showLastButton
      />
    </Box>
  );
};
