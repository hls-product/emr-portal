import { Box, Typography } from "@mui/material";
import React from "react";
import { Tab } from "../../components/tab";
import { PsychologicalTest } from "./PsychologicalTest";
import { QuestionBank } from "./QuestionBank";
import { TestResult } from "./TestResult";

const tabList = [
    {
        title: "Psychological Test",
        element: <PsychologicalTest/>
    },
    {
        title: "Question Bank",
        element: <QuestionBank />
    },
    {
        title: "Test Result",
        element: <TestResult />
    }
]

export const PostManagement: React.FC = () => {
  return (
    <Box>
      <Typography
        component="div"
        fontSize="24px"
        fontWeight="600"
        children="Post Management"
      />
      <Tab children={tabList} defaultValue={0} />
    </Box>
  );
};
