import { Circle, Search } from "@mui/icons-material";
import AddIcon from "@mui/icons-material/Add";
import CloseIcon from "@mui/icons-material/Close";
import {
  Box,
  Button,
  Checkbox,
  Dialog,
  DialogContent,
  DialogTitle,
  FormControl,
  FormControlLabel,
  FormGroup, IconButton,
  Input,
  InputAdornment,
  InputLabel,
  MenuItem,
  Radio,
  RadioGroup,
  Select,
  SelectChangeEvent,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  TextField, Typography
} from "@mui/material";
import React, { useState } from "react";
import { colorPalette } from "../../theme";
import deleteIcon from "../PatientDetails/icon/delete.svg";
import editIcon from "../PatientDetails/icon/edit.svg";
import viewIcon from "../PatientDetails/icon/view.svg";
import { StyledTabContainer } from "../PatientDetails/style";

const patients = new Array(1).fill({
  name: "Question 1",
  answer: "1231",
  view: "2664",
  status: "Draft",
  createDate: "20/11/2021",
  editDate: "20/11/2021",
});

patients.push({
  name: "Question 2",
  answer: "1231",
  view: "2664",
  status: "Published",
  createDate: "20/11/2021",
  editDate: "20/11/2021",
});

patients.push({
  name: "Question 3",
  answer: "1231",
  view: "2664",
  status: "Closed",
  createDate: "20/11/2021",
  editDate: "20/11/2021",
});

patients.push({
  name: "Question 4",
  answer: "1231",
  view: "2664",
  status: "Archived",
  createDate: "20/11/2021",
  editDate: "20/11/2021",
});

interface THeadProps {
  numSelected: number;
  onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
  rowCount: number;
}

const THead: React.FC<THeadProps> = (props) => (
  <TableHead>
    <TableRow sx={{ backgroundColor: "#FAFAFA", stroke: "#EAEAEA" }}>
      <TableCell>
        <Checkbox
          indeterminate={
            props.numSelected > 0 && props.numSelected < props.rowCount
          }
          checked={props.numSelected === props.rowCount}
          onChange={props.onSelectAllClick}
        />
      </TableCell>
      <TableCell>
        <Typography sx={{ fontWeight: "600", fontSize: "14px" }}>
          Title
        </Typography>
      </TableCell>
      <TableCell>
        <Typography sx={{ fontWeight: "600", fontSize: "14px" }}>
          Created Date
        </Typography>
      </TableCell>
      <TableCell>
        <Typography sx={{ fontWeight: "600", fontSize: "14px" }}>
          Answered
        </Typography>
      </TableCell>
      <TableCell>
        <Typography sx={{ fontWeight: "600", fontSize: "14px" }}>
          Viewed
        </Typography>
      </TableCell>
      <TableCell>
        <Typography sx={{ fontWeight: "600", fontSize: "14px" }}>
          Edited Date
        </Typography>
      </TableCell>
      <TableCell>
        <Typography sx={{ fontWeight: "600", fontSize: "14px" }}>
          Status
        </Typography>
      </TableCell>
      <TableCell>
        <Typography
          sx={{ fontWeight: "600", fontSize: "14px", marginLeft: "37px" }}
        >
          Action
        </Typography>
      </TableCell>
    </TableRow>
  </TableHead>
);

export const QuestionBank: React.FC = () => {
  const [selected, setSelected] = React.useState<readonly number[]>([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [searchTerm, setSearchTerm] = React.useState<string>("");
  const shrink = searchTerm.length > 0;
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const handleClickOpen = () => {
    setIsOpen(true);
  };

  const handleClose = () => {
    setIsOpen(false);
  };

  const [questionGroup, setQuestionGroup] = React.useState("Reasoning");
  const quesGroup = [
    { value: "Reasoning" },
    { value: "A" },
    { value: "B" },
    { value: "C" },
  ];

  const handleChangeQuestionGroup = (event: SelectChangeEvent) => {
    setQuestionGroup(event.target.value);
  };

  const [questionType, setQuestionType] = React.useState("2");
  const quesType = [
    { value: "1", type: "Single Answer" },
    { value: "2", type: "Short Text" },
    { value: "3", type: "Ratio Answer" },
    { value: "4", type: "Multiple Answer" },
    { value: "5", type: "Long Text" },
    { value: "6", type: "Number" },
    { value: "7", type: "Date" },
    { value: "8", type: "Time" },
    { value: "9", type: "Dropdown" },
  ];

  const [showhide, setShowhide] = useState("");

  const handleChangeQuestionType = (event: SelectChangeEvent) => {
    setQuestionType(event.target.value);
    const getuser = event.target.value;
    setShowhide(getuser);
    console.log(ratioArray);
  };

  const [question, setQuestion] = React.useState("Lorem ipsum");
  const handleChangeQuestion = (event: React.ChangeEvent<HTMLInputElement>) => {
    setQuestion(event.target.value);
  };

  function handleSelectAllClick(e: React.ChangeEvent<HTMLInputElement>) {
    setSelected(e.target.checked ? patients.map((_, index) => index) : []);
  }

  function handleClick(_e: React.ChangeEvent<HTMLInputElement>, idx: number) {
    setSelected(
      selected.includes(idx)
        ? selected.filter((item) => item !== idx)
        : selected.concat(idx)
    );
  }

  const [ratioArray, setRatioArray] = useState([
    { stt: 0, value: "Condimentum in in a ipsum.", isChecked: false },
    { stt: 1, value: "Semper a justo, lorem erat a.", isChecked: true },
  ]);
  let ratioCounter = ratioArray.length;

  const [checkboxArray, setCheckBoxArray] = useState([
    { stt: 0, value: "Condimentum in in a ipsum.", isChecked: true },
    { stt: 1, value: "Semper a justo, lorem erat a.", isChecked: true },
  ]);
  let checkboxCounter = checkboxArray.length;

  const handleAddRatio = () => {
    setRatioArray((prev) => [
      ...prev,
      { stt: ratioCounter++, value: "", isChecked: false },
    ]);
  };

  const handleAddCheckBox = () => {
    setCheckBoxArray((prev) => [
      ...prev,
      { stt: checkboxCounter++, value: "", isChecked: false },
    ]);

    console.log(checkboxArray);
  };

  const handleRemoveRatio = (id: number) => {
    ratioArray.splice(id, 1);

    while (id < ratioArray.length) {
      ratioArray[id].stt--;
      id++;
    }
    setRatioArray((prev) => [...prev]);

    console.log(ratioArray);
  };

  const handleRemoveCheckbox = (id: number) => {
    checkboxArray.splice(id, 1);

    while (id < checkboxArray.length) {
      checkboxArray[id].stt--;
      id++;
    }
    setCheckBoxArray((prev) => [...prev]);

    console.log(checkboxArray);
  };

  const handleChange = (item: any) => {
    if (item.isChecked == true) {
      item.isChecked = false;
    } else {
      item.isChecked = true;
    }

    setCheckBoxArray((prev) => [...prev]);

    console.log(checkboxArray);
  };

  const handleChangeShowAnswer = (item: any) => {
    item.isChecked = true;
    ratioArray.forEach((e) => {
      if (e.stt !== item.stt) {
        e.isChecked = false;
      }
    });

    setRatioArray((prev) => [...prev]);

    console.log(item);
    console.log(ratioArray);
  };

  const onTodoChange = (item: any) => {
    ratioArray.forEach((e) => {
      if (e.stt === item.stt) {
        e.value = item.value;
      }
    });
    checkboxArray.forEach((e) => {
      if (e.stt === item.stt) {
        e.value = item.value;
      }
    });
    setRatioArray((prev) => [...prev]);
    setCheckBoxArray((prev) => [...prev]);
  };

  function renderCircle(param: any) {
    let fillColor = "";
    switch (param) {
      case "Draft":
        fillColor = "#9FA0A6";
        return fillColor;
      case "Published":
        fillColor = "#22AA4F";
        return fillColor;
      case "Closed":
        fillColor = "#E03535";
        return fillColor;
      case "Archived":
        fillColor = "#FF7300";
        return fillColor;
    }
  }

  return (
    <Box>
      <Box
        sx={{
          width: "100%",
          backgroundColor: colorPalette.white,
          borderRadius: "16px",
          paddingLeft: "20px",
          paddingTop: "10px",
        }}
      >
        <StyledTabContainer
          sx={{ fontWeight: "600", fontSize: "16px", color: "#131626" }}
        >
          <Typography variant="h5">QUESTION BANK LIST</Typography>
          <Box sx={{ display: "flex", gap: "16px" }}>
            <TextField
              label="Search"
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <Search sx={{ height: 20 }} />
                  </InputAdornment>
                ),
              }}
              onChange={(e) => setSearchTerm(e.target.value)}
              InputLabelProps={{
                shrink,
                sx: {
                  transform: shrink ? "" : "translate(45px, 12px) scale(1)",
                  color: "#9FA0A6",
                },
              }}
              variant="outlined"
              sx={{
                "& .MuiInputBase-root": {
                  height: 48,
                  width: 300,
                  borderRadius: 3,
                },
              }}
            />
            <Button
              variant="contained"
              sx={{ borderRadius: "99px", marginRight: "20px", height: "48px" }}
              onClick={handleClickOpen}
            >
              <AddIcon />
              <Typography
                sx={{ fontSize: "14px", fontWeight: "600", marginLeft: "10px" }}
              >
                New Question
              </Typography>
            </Button>
          </Box>
        </StyledTabContainer>
        <Table>
          <THead
            numSelected={selected.length}
            onSelectAllClick={handleSelectAllClick}
            rowCount={patients.length}
          />
          <TableBody>
            {patients
              .map(
                (
                  { name, answer, view, status, createDate, editDate },
                  index
                ) => (
                  <TableRow key={index}>
                    <TableCell style={{ width: "8%" }}>
                      <Checkbox
                        checked={selected.includes(index)}
                        onChange={(e) => handleClick(e, index)}
                      />
                    </TableCell>
                    <TableCell
                      style={{
                        width: "15%",
                        fontSize: "14px",
                        fontWeight: "500",
                      }}
                    >
                      {name}
                    </TableCell>
                    <TableCell style={{ fontSize: "14px", fontWeight: "500" }}>
                      {createDate}
                    </TableCell>
                    <TableCell style={{ fontSize: "14px", fontWeight: "500" }}>
                      {answer}
                    </TableCell>
                    <TableCell style={{ fontSize: "14px", fontWeight: "500" }}>
                      {view}
                    </TableCell>
                    <TableCell style={{ fontSize: "14px", fontWeight: "500" }}>
                      {editDate}
                    </TableCell>
                    <TableCell>
                      {/* <Circle sx={{ height: 12, width: 12, fill: `${status === 1 ? '#E03535' : '#22AA4F'}` }}  />{" "} */}
                      <Circle
                        sx={{
                          height: 12,
                          width: 12,
                          fill: renderCircle(status),
                        }}
                      />{" "}
                      {status}
                    </TableCell>

                    <TableCell style={{ width: "12.9%" }}>
                      <IconButton>
                        <img src={viewIcon} />
                      </IconButton>
                      <IconButton sx={{ marginLeft: "5px" }}>
                        <img src={editIcon} />
                      </IconButton>
                      <IconButton sx={{ marginLeft: "5px" }}>
                        <img src={deleteIcon} />
                      </IconButton>
                    </TableCell>
                  </TableRow>
                )
              )
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)}
          </TableBody>
        </Table>
        <TablePagination
          count={patients.length}
          component="div"
          rowsPerPageOptions={[5, 10, 25, 50]}
          page={page}
          rowsPerPage={rowsPerPage}
          onPageChange={(_ev, newValue) => setPage(newValue)}
          onRowsPerPageChange={(e) => {
            setRowsPerPage(parseInt(e.target.value, 10));
            setPage(0);
          }}
          showFirstButton
          showLastButton
        />
      </Box>

      <Dialog
        open={isOpen}
        onClose={handleClose}
        fullWidth={true}
        maxWidth={"sm"}
        PaperProps={{
          style: { borderRadius: "18px" },
        }}
      >
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          <DialogTitle sx={{ fontSize: "18px" }}>Add question</DialogTitle>
          <DialogTitle
            sx={{
              fontSize: "16px",
              fontWeight: "600",
              color: "#4A5CD0",
              cursor: "pointer",
            }}
            onClick={handleClose}
          >
            Cancel
          </DialogTitle>
        </Box>

        <DialogContent
          sx={{ display: "flex", flexDirection: "column", gap: "26px" }}
        >
          <TextField
            value={question}
            label="Question"
            onChange={handleChangeQuestion}
            required
            multiline
            InputProps={{
              style: {
                height: "122px",
                borderRadius: "12px",
                color: "black",
              },
            }}
            InputLabelProps={{
              style: {
                color: "black",
              },
            }}
            sx={{ width: "100%" }}
          />

          <FormControl style={{ width: "100%" }}>
            <InputLabel sx={{ color: "black" }}>Question Group</InputLabel>
            <Select
              required
              fullWidth
              value={questionGroup}
              onChange={handleChangeQuestionGroup}
              label="Question Group"
              sx={{
                borderRadius: "12px",
                color: "black",
                height: "48px",
              }}
            >
              {quesGroup.map((option) => (
                <MenuItem value={option.value}>{option.value}</MenuItem>
              ))}
            </Select>
          </FormControl>

          <FormControl style={{ width: "100%" }}>
            <InputLabel sx={{ color: "black" }}>Question Type</InputLabel>
            <Select
              required
              fullWidth
              onChange={handleChangeQuestionType}
              label="Question Type"
              value={questionType}
              sx={{
                borderRadius: "12px",
                color: "black",
                height: "48px",
              }}
            >
              {quesType.map((option) => (
                <MenuItem value={option.value}>{option.type}</MenuItem>
              ))}
            </Select>
          </FormControl>

          {showhide === "3" && (
            <div>
              <Button
                variant="outlined"
                sx={{ width: "100%", height: "40px", borderRadius: "99px" }}
                onClick={handleAddRatio}
              >
                <AddIcon />
                <Typography variant="h6" sx={{ marginLeft: "10px" }}>
                  Add an answers
                </Typography>
              </Button>

              <RadioGroup>
                {ratioArray.map((item, id) => (
                  <div key={item.stt}>
                    <Box
                      sx={{
                        display: "flex",
                        justifyContent: "space-between",
                        alignItems: "center",
                      }}
                    >
                      <FormControlLabel
                        value={item}
                        checked={item.isChecked}
                        control={
                          <Radio
                            onChange={(e: any) => handleChangeShowAnswer(item)}
                          />
                        }
                        label={
                          <Input
                            disableUnderline={true}
                            autoFocus
                            placeholder="Typing..."
                            value={item.value}
                            onChange={(e: any) =>
                              onTodoChange({
                                stt: item.stt,
                                value: e.target.value,
                              })
                            }
                            inputProps={{
                              style: {
                                color: "black",
                                width: "356px",
                              },
                            }}
                          />
                        }
                      />

                      {item.isChecked === true && (
                        <Box
                          sx={{
                            width: "106px",
                            height: "24px",
                            border: "1px solid #BAE5C8",
                            color: "#BAE5C8",
                            borderRadius: "21px",
                            fontSize: "12px",
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                          }}
                        >
                          Correct Answer
                        </Box>
                      )}
                      <CloseIcon
                        fontSize="small"
                        sx={{ fill: "grey", cursor: "pointer" }}
                        onClick={() => handleRemoveRatio(id)}
                      />
                    </Box>
                  </div>
                ))}
              </RadioGroup>
            </div>
          )}

          {showhide === "4" && (
            <div>
              <Button
                variant="outlined"
                sx={{ width: "100%", height: "40px", borderRadius: "99px" }}
                onClick={handleAddCheckBox}
              >
                <AddIcon />
                <Typography variant="h6" sx={{ marginLeft: "10px" }}>
                  Add an answers
                </Typography>
              </Button>

              <FormGroup>
                {checkboxArray.map((item, id) => (
                  <div key={item.stt}>
                    <Box
                      sx={{
                        display: "flex",
                        justifyContent: "space-between",
                        alignItems: "center",
                      }}
                    >
                      <FormControlLabel
                        value={item}
                        checked={item.isChecked}
                        control={
                          <Checkbox onChange={(e) => handleChange(item)} />
                        }
                        label={
                          <Input
                            disableUnderline={true}
                            autoFocus
                            placeholder="Typing..."
                            value={item.value}
                            onChange={(e: any) =>
                              onTodoChange({
                                stt: item.stt,
                                value: e.target.value,
                              })
                            }
                            inputProps={{
                              style: {
                                color: "black",
                                width: "356px",
                              },
                            }}
                          />
                        }
                      />

                      {item.isChecked === true && (
                        <Box
                          sx={{
                            width: "106px",
                            height: "24px",
                            border: "1px solid #BAE5C8",
                            color: "#BAE5C8",
                            borderRadius: "21px",
                            fontSize: "12px",
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                          }}
                        >
                          Correct Answer
                        </Box>
                      )}
                      <CloseIcon
                        fontSize="small"
                        sx={{ fill: "grey", cursor: "pointer" }}
                        onClick={() => handleRemoveCheckbox(id)}
                      />
                    </Box>
                  </div>
                ))}
              </FormGroup>
            </div>
          )}

          <FormControl style={{ width: "100%" }}>
            <InputLabel sx={{ color: "black" }}>Difficult Level</InputLabel>
            <Select
              required
              fullWidth
              label="Difficult Level"
              defaultValue={2}
              sx={{
                borderRadius: "12px",
                color: "black",
                height: "48px",
              }}
            >
              <MenuItem value={1}>
                <Circle
                  sx={{
                    height: 12,
                    width: 12,
                    fill: "#E03535",
                    marginRight: "14px",
                  }}
                />
                Hard
              </MenuItem>

              <MenuItem value={2}>
                <Circle
                  sx={{
                    height: 12,
                    width: 12,
                    fill: "#FF7300",
                    marginRight: "14px",
                  }}
                />
                Medium
              </MenuItem>

              <MenuItem value={3}>
                <Circle
                  sx={{
                    height: 12,
                    width: 12,
                    fill: "#22AA4F",
                    marginRight: "14px",
                  }}
                />
                Easy
              </MenuItem>
            </Select>
          </FormControl>

          <FormControl style={{ width: "100%" }}>
            <InputLabel sx={{ color: "black" }}>Status</InputLabel>
            <Select
              required
              fullWidth
              label="Status"
              defaultValue={1}
              sx={{
                borderRadius: "12px",
                color: "black",
                height: "48px",
              }}
            >
              <MenuItem value={1}>
                <Circle
                  sx={{
                    height: 12,
                    width: 12,
                    fill: "#1685DA",
                    marginRight: "14px",
                  }}
                />
                Updated
              </MenuItem>

              <MenuItem value={2}>
                <Circle
                  sx={{
                    height: 12,
                    width: 12,
                    fill: "#FF7300",
                    marginRight: "14px",
                  }}
                />
                Closed
              </MenuItem>

              <MenuItem value={3}>
                <Circle
                  sx={{
                    height: 12,
                    width: 12,
                    fill: "#22AA4F",
                    marginRight: "14px",
                  }}
                />
                Posted
              </MenuItem>
            </Select>
          </FormControl>
        </DialogContent>
      </Dialog>
    </Box>
  );
};
