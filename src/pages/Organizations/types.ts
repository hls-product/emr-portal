export interface OrganizationData{
	id?: string;
	active: boolean;
	name: string;
}