import { styled as muiStyled } from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import Button from "@mui/material/Button";
import FileUploadIcon from "@mui/icons-material/FileUpload";
import TextField from "@mui/material/TextField";
import MenuItem from "@mui/material/MenuItem";
import { DesktopDatePicker } from "@mui/x-date-pickers/DesktopDatePicker";
import Select from "@mui/material/Select";
import Chip from "@mui/material/Chip";
import CloseIcon from "@mui/icons-material/Close";
import Badge from "@mui/material/Badge";
import Avatar from "@mui/material/Avatar";
import DeleteIcon from "@mui/icons-material/Delete";

// 1. Containers

// Component container
export const StyledComponentContainer = muiStyled("div")(({}) => ({
  width: "100%",
  height: "100%",
  margin: "0",
}));

// Container for top section (back arrow + page title + save button)
export const StyledTopContainer = muiStyled("div")(({}) => ({
  padding: "0",
  margin: "0",
  width: "100%",
  paddingBottom: "1rem",
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
}));

// Container for Back Arrow and Page Title
export const StyledTopLeftContainer = muiStyled("div")(({}) => ({
  padding: "0",
  margin: "0",
  display: "flex",
  justifyContent: "start",
  alignItems: "center",
  width: "20rem",
}));

// Body container
export const StyledBodyMainContainer = muiStyled("div")(({}) => ({
  margin: "0",
  padding: "0",
  width: "100%",
  height: "100%",
  display: "flex",
  justifyContent: "space-between",
}));

// Input row container
export const StyledInputRowContainer = muiStyled("div")(({}) => ({
  margin: "0",
  padding: "0",
  width: "100%",
  display: "flex",
  justifyContent: "space-between",
}));

// Body child container
export const StyledLeftBodyChildContainer = muiStyled("div")(({}) => ({
  margin: "0",
  paddingTop: "0.5rem",
  paddingBottom: "1rem",
  paddingLeft: "1rem",
  paddingRight: "1rem",
  width: "49%",
  height: "43rem",
  backgroundColor: "#FFFFFF",
  borderRadius: "1rem",
  display: "flex",
  flexDirection: "column",
  justifyContent: "space-between",
}));

export const StyledRightBodyChildContainer = muiStyled("div")(({}) => ({
  margin: "0",
  paddingTop: "0.5rem",
  paddingBottom: "1rem",
  paddingLeft: "1rem",
  paddingRight: "1rem",
  width: "49%",
  height: "43rem",
  backgroundColor: "#FFFFFF",
  borderRadius: "1rem",
  display: "flex",
  flexDirection: "column",
  justifyContent: "flex-start",
}));
export const StyledGeneralContainer = muiStyled("div")(({}) => ({
  margin: "0",
  paddingTop: "0.5rem",
  paddingBottom: "1rem",
  paddingLeft: "1rem",
  paddingRight: "1rem",
  width: "100%",
  backgroundColor: "#FFFFFF",
  borderRadius: "1rem",
  display: "flex",
  flexDirection: "column",
  justifyContent: "space-between",
}));
// Container for health information
export const StyledHealInfoContainer = muiStyled("div")(({}) => ({
  margin: "0",
  padding: "0",
  width: "100%",
  height: "41%",
  display: "flex",
  flexDirection: "column",
  justifyContent: "space-around",
}));

// 2. Child Elements of above containers

// Back Arrow
export const StyledArrowBack = muiStyled(ArrowBackIcon)(({}) => ({
  color: "#6C5DD3",
  backgroundColor: "white",
  width: "2.5rem",
  height: "2.5rem",
  borderRadius: "6rem",
  padding: "0.5rem",
  cursor: "pointer",
  "&:hover": {
    padding: "0.4rem",
  },
}));

// Page title

export const StyledPageTitle = muiStyled("div")(({}) => ({
  color: "#131626",
  fontWeight: "600",
  fontSize: "1.5rem",
  lineHeight: "2rem",
  padding: "0",
  margin: "0",
}));

// Save button
export const StyledSaveButton = muiStyled(Button)(({}) => ({
  margin: "0",
  color: "#FFFFFF",
  fontWeight: "600",
  fontSize: "1rem",
  lineHeight: "1.2rem",
  backgroundColor: "#6C5DD3",
  borderRadius: "6rem",
  width: "6.2rem",
  height: "2.5rem",
  "&:hover": {
    backgroundColor: "#887ae6",
  },
}));

// Body Section Title
export const StyledBodySectionTitle = muiStyled("div")(({}) => ({
  color: "#081735",
  fontWeight: "600",
  fontSize: "1rem",
  lineHeight: "1.5rem",
  padding: "0",
  margin: "0",
  height: "2.5rem",
  width: "100%",
  display: "flex",
  justifyContent: "flex-start",
  alignItems: "center",
}));

// Upload avatar button
export const StyledUploadAvatarButton = muiStyled("div")(({}) => ({
  width: "100%",
  height: "3.5rem",
  borderRadius: "6rem",
  backgroundColor: "#FFFFFF",
  border: "1px solid #F4F4F4",
  color: "#6C5DD3",
  fontSize: "0.9rem",
  fontWeight: "600",
  lineHeight: "1.2rem",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  cursor: "pointer",
  "&:hover": {
    backgroundColor: "#F4F4F4",
  },
}));

// Upload avatar icon
export const StyledUploadAvatarIcon = muiStyled(FileUploadIcon)(({}) => ({
  width: "1.2rem",
  height: "1.25rem",
  color: "#6C5DD3",
  padding: "0",
  margin: "0",
}));

// Input Style
export const StyledInputTextField = muiStyled(TextField)`
    & .MuiOutlinedInput-root {
      height: 100%;
      & fieldset {
        border: 1px solid #EAEAEA;
        border-radius: 0.8rem;
        padding: 0;
        margin: 0
      }

      &:hover fieldset {
        border: 1px solid #dbd7d7;       
      }

      &.Mui-focused fieldset {
        border: 1px solid #EAEAEA;
      }
    }
  `;

// Select item style
export const StyledMenuItem = muiStyled(MenuItem)`
    
  `;

// Date picker style
export const StyledDesktopDatePicker = muiStyled(DesktopDatePicker)`
  
  `;

// Select style
export const StyledInputSelect = muiStyled(Select)`
  &.MuiInputBase-root {
    
    
    & fieldset {
      border: 1px solid #EAEAEA;
        border-radius: 0.8rem;
        padding: 0;
        margin: 0;
        height: '100%';
        width: '100%';
    }

    &:hover fieldset {
      border: 1px solid #dbd7d7;  
    }
      
    &.Mui-focused fieldset {
      border: 1px solid #EAEAEA;
    }

  }
  & .MuiFormLabel-root {
    color: #131626;
    font-weight : 500;
    font-size: 16px;
  }
`;

// Chip style
export const StyledChip = muiStyled(Chip)(({}) => ({
  backgroundColor: "#4E4E4E",
  color: "white",
}));

// Close chip icon
export const StyledCloseChipIcon = muiStyled(CloseIcon)(({}) => ({
  height: "80%",
}));

// Avatar uploaded delete badge
export const StyledAvatarBadge = muiStyled(Badge)(({}) => ({
  width: "8rem",
  height: "8rem",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  alignSelf: "center",
}));

// Avatar uploaded Style
export const StyledAvatarUploaded = muiStyled(Avatar)(({}) => ({
  width: "100%",
  height: "100%",
}));

// Delete avatar uploaded icon
// Avatar uploaded delete badge
export const StyledAvatarDeleteIcon = muiStyled(DeleteIcon)(({}) => ({
  backgroundColor: "white",
  height: "2.5rem",
  width: "2.5rem",
  color: "#6C5DD3",
  padding: "0.5rem",
  borderRadius: "6.2rem",
  cursor: "pointer",
}));
