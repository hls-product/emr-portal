import {
  Box,
  Table,
  TableBody,
  TableRow,
  TableCell,
} from "@mui/material";
import { FormikProvider, FieldArray, useFormik, getIn } from "formik";

import * as yup from "yup";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { Organization } from "../../../models/organizations/organization";
import { organizationService } from "../../../services/agent";
import { colorPalette } from "../../../theme";
import Loading from "../../../components/Loading";
import TableHeader from "../../../components/Table/TableHeader";
import {
  StyledArrowBack,
  StyledBodyMainContainer,
  StyledBodySectionTitle,
  StyledGeneralContainer,
  StyledInputRowContainer,
  StyledInputTextField,
  StyledMenuItem,
  StyledTopContainer,
  StyledTopLeftContainer,
} from "../UpsertOrganization/style";
import { StyledHeaderTypography } from "../../../components/styledTypography";
const defaultValue: Organization = {
  identifier: [
    {
      use: "official",
      type: {
        text: "Zorginstelling naam",
      },
      system: "http://www.zorgkaartnederland.nl/",
      value: "Blijdorp MC",
    },
  ],
  active: true,
  type: [
    {
      coding: [
        {
          system: "http://snomed.info/sct",
          code: "405608006",
          display: "Academic Medical Center",
        },
        {
          system: "http://terminology.hl7.org/CodeSystem/organization-type",
          code: "prov",
        },
      ],
    },
  ],
  name: "",
  telecom: [],
  address: [],
  contact: [
    {
      purpose: {
        coding: [
          {
            system: "http://terminology.hl7.org/CodeSystem/contactentity-type",
            code: "ADMIN",
          },
        ],
      },
      name: {
        text: "mevr. D. de Haan",
      },
      telecom: [
        {
          system: "phone",
          value: "022-655 2321",
        },
        {
          system: "email",
          value: "cardio@burgersumc.nl",
        },
        {
          system: "fax",
          value: "022-655 2322",
        },
      ],
      address: {
        line: ["South Wing, floor 2"],
      },
    },
  ],
};
function DetailOrganization() {
  const { id } = useParams();
  const navigate = useNavigate();
  const [organization, setOrganization] = useState<Organization>(defaultValue);
  const [isLoading, setIsLoading] = useState(true);
  useEffect(() => {
    if (id) {
      organizationService.getById(id).then((res) => {
        setOrganization(res);
        setIsLoading(false);
        formik.initialValues = res;
      });
    } else {
      navigate("/admin/organizations");
    }
  }, []);
  const validationSchema = yup.object({
    name: yup.string().required("Name is required"),
    telecom: yup.array().of(
      yup.object().shape({
        value: yup.string().required("Value is required"),
      })
    ),
    address: yup.array().of(
      yup.object().shape({
        text: yup.string().required("Text is required"),
      })
    ),
  });
  const formik = useFormik({
    initialValues: organization,
    validationSchema: validationSchema,
    enableReinitialize: true,
    onSubmit: (values) => {
      onSubmitForm(values);
    },
  });
  const onSubmitForm = (item: Organization) => {
    if (item.id) {
      organizationService.patch(item).then(() => {
        navigate("/admin/organizations");
      });
    } else {
      organizationService.add(item).then(() => {
        navigate("/admin/organizations");
      });
    }
  };
  return isLoading ? (
    <Loading />
  ) : (
    <Box
      sx={{
        minHeight: "calc(100vh - 100px)",
      }}
    >
      <form onSubmit={formik.handleSubmit}>
        <StyledTopContainer>
          <StyledTopLeftContainer>
            {" "}
            <StyledArrowBack
              onClick={() => navigate(`/admin/organizations`)}
            />{" "}
            <StyledHeaderTypography
              style={{
                marginBottom: "0",
              }}
            >
              Detail Organization
            </StyledHeaderTypography>
          </StyledTopLeftContainer>
        </StyledTopContainer>
        <StyledBodyMainContainer>
          <StyledGeneralContainer>
            <StyledBodySectionTitle>GENERAL INFO</StyledBodySectionTitle>
            <StyledInputRowContainer>
              <StyledInputTextField
                              disabled={true}
                label="Name"
                id="name"
                name="name"
                sx={{ width: "70%", height: "3rem" }}
                defaultValue={formik.values.name}
                onChange={formik.handleChange}
                error={formik.touched.name && Boolean(formik.errors.name)}
                helperText={formik.touched.name && formik.errors.name}
              />
              <StyledInputTextField
                              disabled={true}
                label="Status"
                name="active"
                id="active"
                select
                sx={{ width: "28%", height: "3rem" }}
                defaultValue={formik.values.active}
                onChange={formik.handleChange}
              >
                <StyledMenuItem value={"" + true}>Active</StyledMenuItem>
                <StyledMenuItem value={"" + false}>Inactive</StyledMenuItem>
              </StyledInputTextField>
            </StyledInputRowContainer>
          </StyledGeneralContainer>
        </StyledBodyMainContainer>

        <Box
          sx={{
            backgroundColor: colorPalette.white,
            borderRadius: "16px",
            p: "16px",
            my: "16px",
          }}
        >
          <FormikProvider value={formik}>
            <FieldArray
              name="telecom"
              render={(arrayHelpers) => (
                <>
                  <Box
                    sx={{
                      display: "flex",
                      justifyContent: "space-between",
                      mb: "16px",
                    }}
                  >
                    <span style={{ fontSize: "16px", fontWeight: 600 }}>
                      TELECOM:
                    </span>
                  </Box>

                  <Table>
                    <TableHeader
                      columns={["System", "Value", "Use For"]}
                    />
                    <TableBody>
                      {formik.values.telecom.map((tele, index) => {
                        const value = `telecom[${index}].value`;
                        const touchedValue = getIn(formik.touched, value);
                        const errorValue = getIn(formik.errors, value);
                        return (
                          <TableRow key={index}>
                            <TableCell>
                              <StyledInputTextField
                              disabled={true}
                                name={`telecom[${index}].system`}
                                defaultValue={
                                  tele.system ? tele.system : "phone"
                                }
                                onChange={formik.handleChange}
                                select
                              >
                                <StyledMenuItem value={"phone"}>
                                  Phone
                                </StyledMenuItem>
                                <StyledMenuItem value={"fax"}>
                                  Fax
                                </StyledMenuItem>
                                <StyledMenuItem value={"email"}>
                                  Email
                                </StyledMenuItem>
                                <StyledMenuItem value={"pager"}>
                                  Pager
                                </StyledMenuItem>
                                <StyledMenuItem value={"url"}>
                                  Url
                                </StyledMenuItem>
                                <StyledMenuItem value={"sms"}>
                                  Sms
                                </StyledMenuItem>
                                <StyledMenuItem value={"other"}>
                                  Other
                                </StyledMenuItem>
                              </StyledInputTextField>
                            </TableCell>
                            <TableCell>
                              <StyledInputTextField
                              disabled={true}
                                name={`telecom[${index}].value`}
                                defaultValue={tele.value}
                                onChange={formik.handleChange}
                                error={Boolean(touchedValue && errorValue)}
                                helperText={
                                  touchedValue && errorValue ? errorValue : ""
                                }
                              />
                            </TableCell>

                            <TableCell>
                              <StyledInputTextField
                              disabled={true}
                                name={`telecom[${index}].use`}
                                defaultValue={tele.use ? tele.use : "home"}
                                onChange={formik.handleChange}
                                select
                              >
                                <StyledMenuItem value={"home"}>
                                  Home
                                </StyledMenuItem>
                                <StyledMenuItem value={"work"}>
                                  Work
                                </StyledMenuItem>
                                <StyledMenuItem value={"temp"}>
                                  Temp
                                </StyledMenuItem>
                                <StyledMenuItem value={"old"}>
                                  Old
                                </StyledMenuItem>
                                <StyledMenuItem value={"mobile"}>
                                  Mobile
                                </StyledMenuItem>
                              </StyledInputTextField>
                            </TableCell>
                          </TableRow>
                        );
                      })}
                    </TableBody>
                  </Table>
                </>
              )}
            />
          </FormikProvider>
        </Box>

        <Box
          sx={{
            backgroundColor: colorPalette.white,
            borderRadius: "16px",
            p: "16px",
            my: "16px",
          }}
        >
          <FormikProvider value={formik}>
            <FieldArray
              name="address"
              render={(arrayHelpers) => (
                <>
                  <Box
                    sx={{
                      display: "flex",
                      justifyContent: "space-between",
                      mb: "16px",
                    }}
                  >
                    <span style={{ fontSize: "16px", fontWeight: 600 }}>
                      ADDRESS:
                    </span>
                  </Box>

                  <Table>
                    <TableHeader
                      columns={[
                        "Text",
                        "City",
                        "Postal Code",
                        "Country",
                        "Use For",
                      ]}
                    />
                    <TableBody>
                      {formik.values.address.map((addr, index) => {
                        const text = `address[${index}].text`;
                        const touchedValue = getIn(formik.touched, text);
                        const errorValue = getIn(formik.errors, text);
                        return (
                          <TableRow key={index}>
                            <TableCell>
                              <StyledInputTextField
                              disabled={true}
                                name={`address[${index}].text`}
                                defaultValue={addr.text}
                                onChange={formik.handleChange}
                                error={Boolean(touchedValue && errorValue)}
                                helperText={
                                  touchedValue && errorValue ? errorValue : ""
                                }
                              />
                            </TableCell>
                            <TableCell>
                              <StyledInputTextField
                              disabled={true}
                                name={`address[${index}].city`}
                                defaultValue={addr.city}
                                onChange={formik.handleChange}
                              />
                            </TableCell>
                            <TableCell>
                              <StyledInputTextField
                              disabled={true}
                                name={`address[${index}].postalCode`}
                                defaultValue={addr.postalCode}
                                onChange={formik.handleChange}
                              />
                            </TableCell>
                            <TableCell>
                              <StyledInputTextField
                              disabled={true}
                                name={`address[${index}].country`}
                                defaultValue={addr.country}
                                onChange={formik.handleChange}
                              />
                            </TableCell>

                            <TableCell>
                              <StyledInputTextField
                              disabled={true}
                                name={`address[${index}].use`}
                                defaultValue={addr.use ? addr.use : "home"}
                                onChange={formik.handleChange}
                                select
                              >
                                <StyledMenuItem value={"home"}>
                                  Home
                                </StyledMenuItem>
                                <StyledMenuItem value={"work"}>
                                  Work
                                </StyledMenuItem>
                                <StyledMenuItem value={"old"}>
                                  Old
                                </StyledMenuItem>
                                <StyledMenuItem value={"billing"}>
                                  Billing
                                </StyledMenuItem>
                              </StyledInputTextField>
                            </TableCell>
                          </TableRow>
                        );
                      })}
                    </TableBody>
                  </Table>
                </>
              )}
            />
          </FormikProvider>
        </Box>
      </form>
    </Box>
  );
}

export default DetailOrganization;
