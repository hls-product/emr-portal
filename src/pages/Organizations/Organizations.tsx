import React from "react";
import { Link, useNavigate } from "react-router-dom";
import {
  IconButton,
  Box,
  InputAdornment,
  TextField,
  Typography
} from "@mui/material";
import { RoundedButton } from "../../components/RoundedButton";
import { Add, Create, Search, Visibility } from "@mui/icons-material";
import DeleteIcon from "@mui/icons-material/Delete";
import { HeadCell } from "../../components/Table/types";
import { organizationService } from "../../services/agent";
import { Organization } from "../../models/organizations/organization";
import { AdvancedSearch } from "../../components/Table/AdvancedSearch";
import { CustomTable } from "../../components/Table/Table";
import { DeleteDialog } from "../../components/DeleteDialog";

const headCells: HeadCell[] = [
  {
    id: "number",
    numeric: true,
    label: "No.",
  },
  {
    id: "id",
    numeric: false,
    label: "Organization ID",
  },
  {
    id: "name",
    numeric: false,
    label: "Organization Name",
  },
  {
    id: "status",
    numeric: false,
    label: "Status",
  },
  {
    id: "action",
    numeric: false,
    label: "Action"
  }
];
const advancedSearchProps = [
  {
    name: "active",
    label: "Status",
    select: true,
    menuItems: [
      {
        value: true,
        display: "Active",
      },
      {
        value: false,
        display: "Inactive",
      },
    ],
  },
];

export const Organizations = () => {
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = React.useState(false);
  const [data, setData] = React.useState<Organization[]>([]);
  const [page, setPage] = React.useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = React.useState<number>(10);
  const [rowsCount, setRowsCount] = React.useState<number>(0);
  const [deletePopUpOpen, setDeletePopUpOpen] = React.useState(false);
  const [openAdvancedSearch, setOpenAdvancedSearch] = React.useState(false);
  const [currentSelectedId, setCurrentSelectedId] = React.useState("");

  // Search states
  const [searchValue, setSearchValue] = React.useState<string>("");
  const [debounceSeachValue, setDebounceSearchValue] =
    React.useState<string>("");
  const [filter, setFilter] = React.useState<{ [key: string]: string }>({});

  // Api calling functions
  const getOrganizationList = async (filter: { [key: string]: string }) => {
    const params = {
      ...filter,
      page: page + 1,
      size: rowsPerPage,
      name: debounceSeachValue,
    };
    setIsLoading(true);
    try {
      let { data, paging } = await organizationService.getList({ params });
      setData(data);
      setRowsCount(paging.total);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      console.log("getOrganizationList error: ", error);
    }
  };

  const deleteOrganization = async (id: string) => {
    setIsLoading(true);
    try {
      let response = await organizationService.delete(id);
      setIsLoading(false);
      return response;
    } catch (error) {
      setIsLoading(false);
      console.log("deleteOrganization error: ", error);
    }
  };

  // Event Handler
  const handleChangePage = (event: any, newPage: number) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event: any) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleDelete = (id: string) => {
    setDeletePopUpOpen(true);
    setCurrentSelectedId(id);
  };
  const handleCancelDelete = () => {
    setDeletePopUpOpen(false);
  };
  const handleConfirmDelete = () => {
    setDeletePopUpOpen(false);
    //currentPatientIdSelected
    deleteOrganization(currentSelectedId).then((res) => {
      setCurrentSelectedId("");
      getOrganizationList(filter);
    });
  };
  const handleClickAdvancedSearchOpen = () => {
    setOpenAdvancedSearch(true);
  };

  // useEffect

  //Debounce for name search
  React.useEffect(() => {
    let timeout = setTimeout(() => {
      setDebounceSearchValue(searchValue);
    }, 500);

    return () => {
      clearTimeout(timeout);
    };
  }, [searchValue]);

  // Back to previous page if current page is empty after deleting an item
  React.useEffect(() => {
    let totalPages = Math.ceil(rowsCount / rowsPerPage);
    let currentPage = page + 1;
    if (currentPage > totalPages && totalPages > 0) {
      setPage(totalPages - 1);
    }
  }, [rowsCount]);
  React.useEffect(() => {
    getOrganizationList(filter);
  }, [debounceSeachValue, rowsPerPage, page]);

  const mapDataToRender = () => {
    const render = data.map((item, index) => ({
      id: item.id as string,
      number: index + 1,
      name: item.name as string,
      status: item.active === true? "Active" : "Inactive",
      action: (
        <Box style={{ textAlign: "center" }}>
          <Link
            to={`edit/${item.id}`}
            children={<IconButton children={<Create />} />}
          />
          <Link
            to={`detail/${item.id}`}
            children={<IconButton children={<Visibility />} />}
          />
          <IconButton
            children={<DeleteIcon />}
            onClick={() => {
              handleDelete(`${item.id}`);
            }}
          />
        </Box>
      ),
    }));
    return render
  };
  return (
    <>
      {/* Dialog for delete item popup */}
      <DeleteDialog
        open={deletePopUpOpen}
        onClose={handleCancelDelete}
        onConfirm={handleConfirmDelete}
        onCancel={handleCancelDelete}
      />
      <Box>
        <Typography
          fontSize={"24px"}
          fontWeight={700}
          color={"#131626"}
          style={{ paddingBottom: "20px" }}
          children="Organizations"
        />
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            width: "100%",
            padding: "20px",
            backgroundColor: "white",
            borderRadius: "16px",
          }}
        >
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: "8px",
            }}
          >
            <Typography variant="h2" children={"ORGANIZATION LIST"} />
            <Box sx={{ display: "flex", gap: "8px" }}>
              <TextField
                placeholder="Search organization name..."
                InputProps={{
                  startAdornment: (
                    <InputAdornment
                      position="start"
                      children={<Search sx={{ height: 20 }} />}
                    />
                  ),
                }}
                sx={{
                  "& .MuiInputBase-root": {
                    height: 48,
                    width: 331,
                    borderRadius: 3,
                  },
                }}
                value={searchValue}
                onChange={(e) => {
                  setSearchValue(e.target.value);
                }}
              />
              <RoundedButton
                variant="contained"
                onClick={handleClickAdvancedSearchOpen}
                children="Advanced Search"
              />
              <RoundedButton
                variant="contained"
                endIcon={<Add />}
                onClick={() => {
                  navigate("add");
                }}
                children="Add Organization"
              />
            </Box>
          </Box>
          {openAdvancedSearch && (
            <AdvancedSearch
              props={advancedSearchProps}
              filter={filter}
              description="Find locations with..."
              onFilterChange={setFilter}
              onApply={getOrganizationList}
              onClose={setOpenAdvancedSearch}
            />
          )}
          <CustomTable
            title={"ORGANIZATION LIST"}
            headcells={headCells}
            body={mapDataToRender()}
            paging={{
              page: page,
              rowsPerPage: rowsPerPage,
              rowsCount: rowsCount,
            }}
            loading={isLoading}
            toolbar={false}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Box>
      </Box>
    </>
  );
};
