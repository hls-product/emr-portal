import { Add, Search, Delete, Create, Visibility } from "@mui/icons-material";
import {
  Box,
  IconButton,
  InputAdornment,
  TextField,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { RoundedButton } from "../../components/RoundedButton";
import { CarePlan } from "../../models/carePlan";
import { DeleteDialog } from "../../components/DeleteDialog";
import { AdvancedSearch } from "../../components/Table/AdvancedSearch";
import { CustomTable } from "../../components/Table/Table";
import { carePlanService } from "../../services/agent";
import { HeadCell } from "../../components/Table/types";

const advancedSearchProps = [
  { name: "subject", label: "Patient Name" },
  {
    name: "category",
    label: "Category",
    select: true,
    menuItems: [
      { value: "Mental health care plan", display: "Mental health care plan" },
      {
        value: "Surgical inpatient care plan",
        display: "Surgical inpatient care plan",
      },
      {
        value: "Vulnerable adult care plan",
        display: "Vulnerable adult care plan",
      },
      { value: "Cancer care plan", display: "Cancer care plan" },
      {
        value: "Cardiac surgery inpatient care plan",
        display: "Cardiac surgery inpatient care plan",
      },
      { value: "Advance care plan", display: "Advance care plan" },
      { value: "Dementia care plan", display: "Dementia care plan" },
      { value: "Maternity care plan", display: "Maternity care plan" },
      { value: "Hysteroscopy care plan", display: "Hysteroscopy care plan" },
      {
        value: "Clinical management plan",
        display: "Clinical management plan",
      },
      { value: "Orthotic care plan", display: "Orthotic care plan" },
    ],
  },
  {
    name: "intent",
    label: "Intent",
    select: true,
    menuItems: [
      { value: "Proposal", display: "Proposal" },
      { value: "Plan", display: "Plan" },
      { value: "Order", display: "Order" },
      { value: "Option", display: "Option" },
    ],
  },
  {
    name: "status",
    label: "Status",
    select: true,
    menuItems: [
      { value: "Draft", display: "Draft" },
      { value: "Active", display: "Active" },
      { value: "On Hold", display: "On Hold" },
      { value: "Revoked", display: "Revoked" },
      { value: "Completed", display: "Completed" },
      { value: "Entered in Error", display: "Entered in Error" },
      { value: "Unknown", display: "Unknown" },
    ],
  },
  { name: "id", label: "Care Plan ID" },
];
const headCells: HeadCell[] = [
  {
    id: "number",
    numeric: true,
    label: "No.",
  },
  {
    id: "title",
    numeric: false,
    label: "Title",
  },
  {
    id: "subject",
    numeric: false,
    label: "Patient",
  },
  {
    id: "encounter",
    numeric: false,
    label: "Encounter",
  },
  {
    id: "careTeam",
    numeric: false,
    label: "Care Team",
  },
  {
    id: "intent",
    numeric: false,
    label: "Intent",
  },
  {
    id: "category",
    numeric: false,
    label: "Categories",
  },
  {
    id: "status",
    numeric: false,
    label: "Status",
  },
  {
    id: "period",
    numeric: false,
    label: "Period",
  },
  {
    id: "action",
    numeric: false,
    label: "Action",
  },
];

export const CarePlanList: React.FC = () => {
  const navigate = useNavigate();

  //------------selectors------------

  const [data, setData] = useState<CarePlan[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  //-----------useState-----------

  const [page, setPage] = useState<number>(0);

  const [rowsPerPage, setRowsPerPage] = useState<number>(10);

  const [rowsCount, setRowsCount] = useState<number>(0);

  const [deletePopUpOpen, setDeletePopUpOpen] = useState(false);

  const [openAdvancedSearch, setOpenAdvancedSearch] = React.useState(false);

  const [selectedId, setSelectedId] = useState("");

  //-----------search states----------------

  const [debounceSeachValue, setDebounceSearchValue] = useState<string>("");

  const [searchValue, setSearchValue] = useState<string>("");

  const [filter, setFilter] = useState<{ [key: string]: string }>({});

  //----------------event handle--------------------

  const handleChangePage = (event: any, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: any) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleDelete = (id: string) => {
    setDeletePopUpOpen(true);
    setSelectedId(id);
  };

  const handleCancelDelete = () => {
    setDeletePopUpOpen(false);
  };

  const handleConfirmDelete = () => {
    setDeletePopUpOpen(false);
    setLoading(true);
    carePlanService
      .delete(selectedId)
      .catch((err) => console.error(err))
      .finally(() => {
        setSelectedId("");
        carePlanService
          .getList({
            params: {
              page: page + 1,
              size: rowsPerPage,
            },
          })
          .then((res) => {
            setData(res.data);
            setRowsCount(res.paging.total);
            setLoading(false);
          })
          .catch((err) => console.error(err));
      });
  };

  const handleAdvancedSearchOpen = () => {
    setOpenAdvancedSearch(true);
  };

  const handleFilter = () => {
    setLoading(true);
    carePlanService
      .getList({
        params: {
          ...filter,
          title: debounceSeachValue,
          page: page + 1,
          size: rowsPerPage,
        },
      })
      .then((res) => {
        setData(res.data);
        setRowsCount(res.paging.total);
        setLoading(false);
      })
      .catch((err) => console.error(err));
  };
  //-----------init state-------------------

  useEffect(() => {
    carePlanService
      .getList({
        params: {
          ...filter,
          title: debounceSeachValue,
          page: page + 1,
          size: rowsPerPage,
        },
      })
      .then((res) => {
        setLoading(false);
        setData(res.data);
        setRowsCount(res.paging.total);
      })
      .catch((err) => console.log(err));
  }, [page, rowsPerPage, debounceSeachValue]);

  //----------debounce for name search------------------

  useEffect(() => {
    let timeout = setTimeout(() => {
      setLoading(true);
      setDebounceSearchValue(searchValue);
    }, 500);
    return () => {
      clearTimeout(timeout);
    };
  }, [searchValue]);

  //---------back to previous page if current page is empty after deleting an item---------

  useEffect(() => {
    let totalPages = Math.ceil(rowsCount / rowsPerPage);
    let currentPage = page + 1;
    if (currentPage > totalPages && totalPages > 0) {
      setPage(totalPages - 1);
    }
  }, [rowsCount]);
  //-----------map data------------------------

  const mapDataToRender = () => {
    const render = data?.map((item, index) => ({
      id: item.id as string,
      number: index + 1,
      title: item.title || "-",
      subject: item.subject?.reference || item.subject?.display || "-",
      encounter: item.encounter?.reference || item.encounter?.display || "-",
      careTeam:
        item.careTeam?.map((x) => x.reference || x.display).join(", ") || "-",
      intent: (
        <Typography textTransform="capitalize" children={item.intent || "-"} />
      ),
      category:
        item.category
          ?.filter((x) => x.coding?.[0])
          .map((x) => x.coding?.[0].display)
          .join(", ") || "-",
      status: (
        <Typography textTransform="capitalize" fontSize="14px" children={item.status || "-"} />
      ),
      period: `${item.period?.start?.slice(0, 4) || "2014"} - ${
        item.period?.end?.slice(0, 4) || new Date().getFullYear()
      }`,
      action: (
        <Box style={{ textAlign: "center" }}>
          <Link
            to={`edit?id=${item.id}`}
            children={<IconButton children={<Create />} />}
          />
          <Link
            to={`detail?id=${item.id}`}
            children={<IconButton children={<Visibility />} />}
          />
          <Link
            to={`#`}
            onClick={() => {
              handleDelete(item.id as string);
            }}
            children={<IconButton children={<Delete />} />}
          />
        </Box>
      ),
    }));
    return render;
  };

  return (
    <Box>
      <DeleteDialog
        open={deletePopUpOpen}
        onClose={handleCancelDelete}
        onConfirm={handleConfirmDelete}
        onCancel={handleCancelDelete}
      />

      <Box>
        <Typography
          fontSize={"24px"}
          fontWeight={700}
          color={"#131626"}
          style={{ paddingBottom: "20px" }}
          children="Care Plans"
        />
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            width: "100%",
            padding: "20px",
            backgroundColor: "white",
            borderRadius: "16px",
          }}
        >
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              gap: "8px",
            }}
          >
            <Typography variant="h2" children={"CARE PLAN LIST"} />
            <Box sx={{ display: "flex", gap: "8px" }}>
              <TextField
                placeholder="Search care plan title..."
                InputProps={{
                  startAdornment: (
                    <InputAdornment
                      position="start"
                      children={<Search sx={{ height: 20 }} />}
                    />
                  ),
                }}
                sx={{
                  "& .MuiInputBase-root": {
                    height: 48,
                    width: 331,
                    borderRadius: 3,
                  },
                }}
                value={searchValue}
                onChange={(e) => {
                  setSearchValue(e.target.value);
                }}
              />
              <RoundedButton
                variant="contained"
                onClick={handleAdvancedSearchOpen}
                children="Advanced Search"
              />
              <RoundedButton
                variant="contained"
                endIcon={<Add />}
                onClick={() => {
                  navigate("add");
                }}
                children="Add Care Plan"
              />
            </Box>
          </Box>

          {openAdvancedSearch && (
            <AdvancedSearch
              props={advancedSearchProps}
              filter={filter}
              description="Find patients with..."
              onFilterChange={setFilter}
              onApply={handleFilter}
              onClose={setOpenAdvancedSearch}
            />
          )}

          <CustomTable
            title={"CARE PLAN LIST"}
            headcells={headCells}
            body={mapDataToRender()}
            paging={{
              page: page,
              rowsPerPage: rowsPerPage,
              rowsCount: rowsCount,
            }}
            loading={loading}
            toolbar={false}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Box>
      </Box>
    </Box>
  );
};
