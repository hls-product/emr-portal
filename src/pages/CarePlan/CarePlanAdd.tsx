import {
  Box,
  CircularProgress,
  FormControl,
  IconButton,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
  TextField,
  Typography,
} from "@mui/material";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { RoundedButton } from "../../components/RoundedButton";
import { ReactComponent as BackArrow } from "../../assets/image/BackArrow.svg";
import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { carePlanService } from "../../services/agent";

const Status = [
  { value: "Draft" },
  { value: "Active" },
  { value: "On Hold" },
  { value: "Revoked" },
  { value: "Completed" },
  { value: "Entered in Error" },
  { value: "Unknown" },
];

const Intent = [
  { value: "Proposal" },
  { value: "Plan" },
  { value: "Order" },
  { value: "Option" },
];

const Category = [
  { value: "Mental health care plan" },
  { value: "Surgical inpatient care plan" },
  { value: "Vulnerable adult care plan" },
  { value: "Cancer care plan" },
  { value: "Cardiac surgery inpatient care plan" },
  { value: "Advance care plan" },
  { value: "Dementia care plan" },
  { value: "Maternity care plan" },
  { value: "Hysteroscopy care plan " },
  { value: "Clinical management plan" },
  { value: "Orthotic care plan" },
];

export const CarePlanAdd: React.FC = () => {
  const navigate = useNavigate();
  const [loading, setLoading] = useState<boolean>(false)

  //------------------------------------

  const [name, setName] = useState<string>("");
  const [status, setStatus] = useState<string>("Active");
  const [intent, setIntent] = useState<string>("Plan");
  const [category, setCategory] = useState<string>("Clinical management plan");
  const [startPeriod, setStartPeriod] = useState<string>("2014-08-18T21:11:54");
  const [endPeriod, setEndPeriod] = useState<string>("2015-08-18T21:11:54");
  const [created, setCreated] = useState<string>("2014-08-18T21:11:54");
  const [basedOn, setBasedOn] = useState<string>("");
  const [replaced, setReplaced] = useState<string>("");
  const [partOf, setPartOf] = useState<string>("");
  const [description, setDescription] = useState<string>("");
  const [careTeam, setCareTeam] = useState<string>("");
  const [addresses, setAddresses] = useState<string>("");
  const [goal, setGoal] = useState<string>("");

  //------------------------------------------------

  const handleChangeName = (event: React.ChangeEvent<HTMLInputElement>) => {
    setName(event.target.value as string);
  };

  const handleChangeStatus = (event: SelectChangeEvent) => {
    setStatus(event.target.value as string);
  };

  const handleChangeIntent = (event: SelectChangeEvent) => {
    setIntent(event.target.value as string);
  };

  const handleChangeCategory = (event: SelectChangeEvent) => {
    setCategory(event.target.value as string);
  };

  const handleChangeStart = (value: any) => {
    setStartPeriod(value.toISOString().replace(/.\d+Z$/g, "Z") as string);
  };

  const handleChangeEnd = (value: any) => {
    setEndPeriod(value.toISOString().replace(/.\d+Z$/g, "Z") as string);
  };

  const handleChangeCreated = (value: any) => {
    setCreated(value.toISOString().replace(/.\d+Z$/g, "Z") as string);
  };

  const handleChangeBasedOn = (event: React.ChangeEvent<HTMLInputElement>) => {
    setBasedOn(event.target.value as string);
  };

  const handleChangeReplaced = (event: React.ChangeEvent<HTMLInputElement>) => {
    setReplaced(event.target.value as string);
  };

  const handleChangePartOf = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPartOf(event.target.value as string);
  };

  const handleChangeDescription = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setDescription(event.target.value as string);
  };

  const handleChangeCareTeam = (event: React.ChangeEvent<HTMLInputElement>) => {
    setCareTeam(event.target.value as string);
  };

  const handleChangeAddresses = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setAddresses(event.target.value as string);
  };

  const handleChangeGoal = (event: React.ChangeEvent<HTMLInputElement>) => {
    setGoal(event.target.value as string);
  };

  const data = {
    basedOn: [{ display: basedOn }],
    replaces: [{ display: replaced }],
    partOf: [{ display: partOf }],
    status,
    intent,
    category: [{ coding: [{ display: category }] }],
    description,
    subject: { display: name },
    period: {
      start: startPeriod,
      end: endPeriod,
    },
    created,
    careTeam: [{ display: careTeam }],
    addresses: [{ display: addresses }],
    goal: [{ display: goal }],
  };

  const handleAdd = () => {
    try{
      setLoading(true);
      carePlanService.post({body: data})
      .then(() => navigate("/admin/care-plans"));
    }catch(err){
      console.error("Post Care Plan error: " + err);
      setLoading(false)
    }
  };

  return (
    <Box>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          marginBottom: "16px",
        }}
      >
        <Box sx={{ display: "flex", gap: 1, alignItems: "center" }}>
          <IconButton
            onClick={() => history.back()}
            style={{
              width: "40px",
              height: "40px",
              border: "1px solid #F4F4F4",
              background: "white",
            }}
          >
            <BackArrow />
          </IconButton>
          <Typography variant="h2" sx={{ fontWeight: 600, fontSize: "24px" }}>
            Add Care Plan
          </Typography>
        </Box>

        <RoundedButton
          variant="contained"
          style={{ padding: "10px 20px" }}
          disabled={loading}
          onClick={handleAdd}
        >
          {loading && (
            <CircularProgress
              size={16}
              sx={{ marginRight: "8px", color: "white" }}
            />
          )}
          Save
        </RoundedButton>
      </Box>

      <Box
        sx={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <Box
          sx={{
            width: "49.25%",
          }}
        >
          <Box
            sx={{
              background: "white",
              borderRadius: "16px",
              padding: "16px",
              marginBottom: "16px",
            }}
          >
            <Typography
              variant="h3"
              sx={{
                fontSize: "16px",
                fontWeight: 700,
                marginBottom: "20px",
                textTransform: "uppercase",
              }}
            >
              Basic Information
            </Typography>

            <TextField
              required
              label="Name"
              placeholder="Enter"
              InputProps={{
                style: {
                  height: "48px",
                  borderRadius: "12px",
                  color: "black",
                },
              }}
              InputLabelProps={{
                style: {
                  color: "black",
                },
              }}
              sx={{ width: "100%", marginBottom: "16px" }}
              onChange={handleChangeName}
            />

            <FormControl sx={{ width: "100%", marginBottom: "16px" }}>
              <InputLabel required sx={{ color: "black" }}>
                Status
              </InputLabel>
              <Select
                label="Status"
                sx={{
                  height: "48px",
                  borderRadius: "12px",
                  color: "black",
                }}
                onChange={handleChangeStatus}
              >
                {Status.map((item, index) => (
                  <MenuItem key={index} value={item.value}>
                    {item.value}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                width: "100%",
              }}
            >
              <FormControl sx={{ width: "49%" }}>
                <InputLabel required sx={{ color: "black" }}>
                  Intent
                </InputLabel>
                <Select
                  label="Intent"
                  sx={{
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  }}
                  onChange={handleChangeIntent}
                >
                  {Intent.map((item, index) => (
                    <MenuItem key={index} value={item.value}>
                      {item.value}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>

              <FormControl sx={{ width: "49%" }}>
                <InputLabel required sx={{ color: "black" }}>
                  Category
                </InputLabel>
                <Select
                  label="Category"
                  sx={{
                    height: "48px",
                    borderRadius: "12px",
                    color: "black",
                  }}
                  onChange={handleChangeCategory}
                >
                  {Category.map((item, index) => (
                    <MenuItem key={index} value={item.value}>
                      {item.value}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Box>
          </Box>

          <Box
            sx={{
              background: "white",
              borderRadius: "16px",
              padding: "16px",
              minHeight: "240px",
            }}
          >
            <Typography
              variant="h3"
              sx={{
                fontSize: "16px",
                fontWeight: 700,
                marginBottom: "20px",
                textTransform: "uppercase",
              }}
            >
              CarePlan Schedule
            </Typography>

            <Box
              sx={{
                gap: 1,
                display: "flex",
                justifyContent: "space-between",
                "& .MuiOutlinedInput-root": {
                  "& fieldset": {
                    borderRadius: "12px",
                    padding: 0,
                    margin: 0,
                  },
                },
              }}
            >
              <LocalizationProvider dateAdapter={AdapterDayjs}>
                <DesktopDatePicker
                  label="Starting time"
                  inputFormat="MM/DD/YYYY"
                  value={startPeriod}
                  onChange={handleChangeStart}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      error={startPeriod ? startPeriod >= endPeriod : true}
                    />
                  )}
                  InputProps={{
                    style: {
                      height: "48px",
                      borderRadius: "12px",
                      color: "black",
                    },
                  }}
                />
              </LocalizationProvider>

              <LocalizationProvider dateAdapter={AdapterDayjs}>
                <DesktopDatePicker
                  label="Ending time"
                  inputFormat="MM/DD/YYYY"
                  value={endPeriod}
                  onChange={handleChangeEnd}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      error={endPeriod ? endPeriod <= startPeriod : true}
                    />
                  )}
                  InputProps={{
                    style: {
                      height: "48px",
                      borderRadius: "12px",
                      color: "black",
                    },
                  }}
                />
              </LocalizationProvider>

              <LocalizationProvider dateAdapter={AdapterDayjs}>
                <DesktopDatePicker
                  label="Created"
                  inputFormat="MM/DD/YYYY"
                  value={created}
                  onChange={handleChangeCreated}
                  renderInput={(params) => <TextField {...params} />}
                  InputProps={{
                    style: {
                      height: "48px",
                      borderRadius: "12px",
                      color: "black",
                    },
                  }}
                />
              </LocalizationProvider>
            </Box>
          </Box>
        </Box>

        <Box
          sx={{
            width: "49.25%",
            background: "white",
            borderRadius: "16px",
            padding: "16px",
          }}
        >
          <Typography
            variant="h3"
            sx={{
              fontSize: "16px",
              fontWeight: 700,
              marginBottom: "20px",
              textTransform: "uppercase",
            }}
          >
            Advanced Information
          </Typography>

          <TextField
            label="Based On"
            placeholder="Enter"
            InputProps={{
              style: {
                height: "48px",
                borderRadius: "12px",
                color: "black",
              },
            }}
            InputLabelProps={{
              style: {
                color: "black",
              },
            }}
            sx={{ width: "100%", marginBottom: "16px" }}
            onChange={handleChangeBasedOn}
          />

          <TextField
            label="Replaced"
            placeholder="Enter"
            InputProps={{
              style: {
                height: "48px",
                borderRadius: "12px",
                color: "black",
              },
            }}
            InputLabelProps={{
              style: {
                color: "black",
              },
            }}
            sx={{ width: "100%", marginBottom: "16px" }}
            onChange={handleChangeReplaced}
          />

          <TextField
            label="Referenced"
            placeholder="Enter"
            InputProps={{
              style: {
                height: "48px",
                borderRadius: "12px",
                color: "black",
              },
            }}
            InputLabelProps={{
              style: {
                color: "black",
              },
            }}
            sx={{ width: "100%", marginBottom: "16px" }}
            onChange={handleChangePartOf}
          />

          <TextField
            label="Description"
            placeholder="Enter"
            InputProps={{
              style: {
                height: "48px",
                borderRadius: "12px",
                color: "black",
              },
            }}
            InputLabelProps={{
              style: {
                color: "black",
              },
            }}
            sx={{ width: "100%", marginBottom: "16px" }}
            onChange={handleChangeDescription}
          />

          <TextField
            label="Care Team"
            placeholder="Enter"
            InputProps={{
              style: {
                height: "48px",
                borderRadius: "12px",
                color: "black",
              },
            }}
            InputLabelProps={{
              style: {
                color: "black",
              },
            }}
            sx={{ width: "100%", marginBottom: "16px" }}
            onChange={handleChangeCareTeam}
          />

          <TextField
            label="Addresses"
            placeholder="Enter"
            InputProps={{
              style: {
                height: "48px",
                borderRadius: "12px",
                color: "black",
              },
            }}
            InputLabelProps={{
              style: {
                color: "black",
              },
            }}
            sx={{ width: "100%", marginBottom: "16px" }}
            onChange={handleChangeAddresses}
          />

          <TextField
            required
            label="Goal"
            placeholder="Enter"
            InputProps={{
              style: {
                height: "48px",
                borderRadius: "12px",
                color: "black",
              },
            }}
            InputLabelProps={{
              style: {
                color: "black",
              },
            }}
            sx={{ width: "100%" }}
            onChange={handleChangeGoal}
          />
        </Box>
      </Box>
    </Box>
  );
};
