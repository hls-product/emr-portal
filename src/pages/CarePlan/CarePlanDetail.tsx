import { Box, IconButton, Typography } from "@mui/material";
import { RoundedButton } from "../../components/RoundedButton";
import { useNavigate, useSearchParams } from "react-router-dom";
import { useSelector } from "react-redux";
import { IRootState } from "../../store/reducers";
import { ReactComponent as BackArrow } from "../../assets/image/BackArrow.svg";
import Loading from "../../components/Loading";
import { useEffect, useState } from "react";
import { CarePlan } from "../../models/carePlan";
import { carePlanService } from "../../services/agent";

interface DetailProps {
  label: string;
  value: string;
}

interface TitleProps {
  title: string;
}

export const CarePlanDetail = () => {
  const navigate = useNavigate();
  const [searchParams, setSearchParams] = useSearchParams();
  const [data, setData] = useState<CarePlan>({});
  const [loading, setLoading] = useState<boolean>(true)

  const id = searchParams.get("id") as string;

  const RenderDetail = (detailprops: DetailProps) => {
    const { label, value } = detailprops;
    return (
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          marginBottom: "16px",
        }}
      >
        <Typography
          variant="h3"
          sx={{
            fontSize: "16px",
            fontWeight: 600,
          }}
        >
          {label}
        </Typography>

        <Typography
          sx={{
            fontSize: "16px",
            fontWeight: 500,
          }}
        >
          {value}
        </Typography>
      </Box>
    );
  };

  const RenderTitle = (titleProps: TitleProps) => {
    const { title } = titleProps;
    return (
      <Typography
        variant="h3"
        sx={{
          fontSize: "20px",
          marginBottom: "20px",
        }}
      >
        {title}
      </Typography>
    );
  };

  const handleClickEdit = () => {
    navigate(`/admin/care-plans/edit?${data.id}`);
  };

  useEffect(() => {
    if(!id) throw history.back();
    carePlanService.getById(id)
    .then(res => {
      setData(res);
      setLoading(false);
    })
    .catch(err => {
      console.log(err);
      history.back();
    })
  }, [])
  return (
    <Box>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          marginBottom: "16px",
        }}
      >
        <Box sx={{ display: "flex", gap: 1, alignItems: "center" }}>
          <IconButton
            onClick={() => history.back()}
            style={{
              width: "40px",
              height: "40px",
              border: "1px solid #F4F4F4",
              background: "white",
            }}
          >
            <BackArrow />
          </IconButton>
          <Typography variant="h2" sx={{ fontWeight: 600, fontSize: "24px" }}>
            Care Plan Detail
          </Typography>
        </Box>

        <RoundedButton
          variant="contained"
          style={{ padding: "10px 20px" }}
          onClick={handleClickEdit}
        >
          Edit
        </RoundedButton>
      </Box>

      {loading ? (
        <Loading />
      ) : (
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
          }}
        >
          <Box
            sx={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            <Box
              sx={{ width: "49.25%", display: "flex", flexDirection: "column" }}
            >
              <Box
                sx={{
                  background: "white",
                  borderRadius: "16px",
                  padding: "16px 16px 0 16px",
                  marginBottom: "20px",
                }}
              >
                <RenderTitle title="Basic Information" />
                <RenderDetail
                  label="Patient Name"
                  value={data?.subject?.display || "-"}
                />
                <RenderDetail label="ID" value={data?.id || "-"} />
                <RenderDetail label="Status" value={data?.status || "-"} />
                <RenderDetail label="Intent" value={data?.intent || "-"} />
                <RenderDetail
                  label="Category"
                  value={data?.category?.[0]?.coding?.[0]?.display || "-"}
                />
              </Box>

              <Box
                sx={{
                  background: "white",
                  borderRadius: "16px",
                  padding: "16px 16px 0 16px",
                }}
              >
                <RenderTitle title="Care Plan Schedule" />
                <RenderDetail
                  label="Start-time Period"
                  value={data?.period?.start?.slice(0, 10) || "-"}
                />
                <RenderDetail
                  label="End-time Period"
                  value={data?.period?.end?.slice(0, 10) || "-"}
                />
                <RenderDetail
                  label="Frist Recorded"
                  value={data?.created?.slice(0, 10) || "-"}
                />
              </Box>
            </Box>

            <Box
              sx={{
                width: "49.25%",
                display: "flex",
                background: "white",
                borderRadius: "16px",
                padding: "16px 16px 0 16px",
                flexDirection: "column",
              }}
            >
              <RenderTitle title="Advanced Information" />
              <RenderDetail
                label="Fullfills CarePlan"
                value={data?.basedOn?.[0]?.display || "-"}
              />
              <RenderDetail
                label="Replaced Plan"
                value={data?.replaces?.[0]?.display || "-"}
              />
              <RenderDetail
                label="Referenced"
                value={data?.partOf?.[0]?.display || "-"}
              />
              <RenderDetail
                label="Description"
                value={data?.description || "-"}
              />
              <RenderDetail
                label="Care Team"
                value={data?.careTeam?.[0]?.display || "-"}
              />
              <RenderDetail
                label="Addresses"
                value={data?.addresses?.[0]?.display || "-"}
              />
              <RenderDetail
                label="Goal"
                value={data?.goal?.[0]?.display || "-"}
              />
            </Box>
          </Box>
        </Box>
      )}
    </Box>
  );
};
