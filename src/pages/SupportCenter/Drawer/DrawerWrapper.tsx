import * as React from "react"
import Drawer from "@mui/material/Drawer";
import { Paper} from "@mui/material";
import {SupportDrawer} from "./SupportDrawer";
import {NewMessageDrawer} from "./NewMessageDrawer";
interface Props {
  openSupportStatus: boolean;
  handleInteractSupport: () => void
}
export const DrawerWrapper: React.FC<Props> = ({openSupportStatus, handleInteractSupport})=> {
  const [postMessage, setPostMessage] = React.useState<boolean>(false)
  const handlePostMessage = () => {
    setPostMessage(!postMessage)
  }
  return(
    <React.Fragment >
      <Drawer
        style={{zIndex:1400}}
        anchor='right'
        open={openSupportStatus}
        onClose={handleInteractSupport}
      >
        <Paper style={{width:"830px", boxShadow: "none"}}>
          {(postMessage && <NewMessageDrawer handlePostMessage={handlePostMessage}
                                             handleInteractSupport={handleInteractSupport}/>) ||
            <SupportDrawer handleInteractSupport={handleInteractSupport}
                           handlePostMessage = {handlePostMessage}/>}
        </Paper>
      </Drawer>
    </React.Fragment>
  )
}


