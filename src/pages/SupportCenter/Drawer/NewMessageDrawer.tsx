import * as React from 'react';
import {Button, Typography} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Checkbox from '@mui/material/Checkbox';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { Link } from 'react-router-dom';

import {RichTextEditor} from "./RichTextEditor";
import "./drawer.css"

interface Props {
  handleInteractSupport: () => void,
  handlePostMessage: () => void
}


export const NewMessageDrawer: React.FC<Props> = ({handlePostMessage, handleInteractSupport}) => {

  const [value, setValue] = React.useState<Date | null>(null);

  const handleChange = (newValue: Date | null) => {
    setValue(newValue);
  };

  return(<>
    <div
      style={{
        borderBottom: '1px solid #EAEAEA',
        display: 'flex',
        justifyContent: 'space-between',
      }}
    >
      <div
        style={{
          display: "flex",
          alignItems:"center",
        }}
      >
        <ArrowBackIcon
          style={{
            cursor: "pointer",
            width: 40,
            paddingLeft: 20
          }}
          onClick={handlePostMessage}
        />
        <Typography
          sx={{fontSize: '24px', fontWeight: '700', color: '#131626', padding: "30px 20px"}}
        >
          Post New Message
        </Typography>
      </div>
      <ul
        style={{
          listStyle: "none",
          display: "flex",
          alignItems: "center"
        }}
      >
        <li>
          <Link
            to={`/admin/support`}
            state={true}
            onClick={() => {handleInteractSupport(); handlePostMessage()}}
            style={{
              background: "#6C5DD3",
              margin: "0 12px",
              padding: "10px 32px",
              borderRadius: "50px",
              color: "white",
              boxShadow: "0px 5px 10px 0px rgba(74, 92, 208, 0.2)",
              textDecoration:"none",
              fontWeight:"600"
            }}
          >
            Send
          </Link>
        </li>
        <li>
          <div
            style={{
              width: 1, height: 24, background: "#F4F4F4", margin: "0 12px",
            }}/>
        </li>
        <li>
          <Button
            sx={{
              minWidth: 40,
              minHeight: 40,
              margin: "0 12px",
              borderRadius: "50%",
              border: "1px solid #F4F4F4",
              color: "#6C5DD3"
            }}
            onClick={handleInteractSupport}
          >
            <CloseIcon/>
          </Button>
        </li>
      </ul>
    </div>
    <Box
      id='message-drawer-form'
      component="form"
      noValidate
      autoComplete="off"
      style={{
        display: "flex",
        flexDirection: "column",
        padding: "30px 20px 0 20px"
      }}
    >
      <TextField
        required
        id="title"
        label="Title"
        style={{paddingBottom: 10}}
      />
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <DesktopDatePicker
            label="Notice Date"
            inputFormat="MM/dd/yyyy"
            onChange={handleChange}
            value={value}
            renderInput={(params) => <TextField {...params} />}
          />
        </LocalizationProvider>
      <h1 style={{color: 'rgba(19, 22, 38, 1)', fontSize: 14, padding:"15px 0"}}>Message To</h1>
     <FormGroup
       style={{
         display:"flex",
         flexDirection: "row",
         justifyContent: "space-evenly",
         paddingTop: "15px"
       }}
     >
       <FormControlLabel control={<Checkbox size="medium"/>} label="Admin"/>
       <FormControlLabel control={<Checkbox size="medium"/>} label="Accountant"/>
       <FormControlLabel control={<Checkbox size="medium"/>} label="Doctor"/>
       <FormControlLabel control={<Checkbox size="medium"/>} label="Pharmacist"/>
       <FormControlLabel control={<Checkbox size="medium"/>} label="Super Admin"/>
       <FormControlLabel control={<Checkbox size="medium"/>} label="Nurse"/>
     </FormGroup>
      <hr
        style={{
          background:'rgba(244, 244, 244, 1)',
          height: "1px",
          border: 'none',
          margin: 16,
        }}
      />
      <h1 style={{color: 'rgba(19, 22, 38, 1)', fontSize: 14}}>Message</h1>
      <RichTextEditor/>
    </Box>

  </>)
}