import * as React from "react"
import Drawer from "@mui/material/Drawer";
import {Button, Modal, Paper, Typography} from "@mui/material";
import AddIcon from '@mui/icons-material/Add';
import CloseIcon from '@mui/icons-material/Close';
import {SupportTickets} from "../SupportTickets";


interface Props {
  handleInteractSupport: () => void,
  handlePostMessage: () => void
}

export const SupportDrawer: React.FC<Props> = ({handleInteractSupport, handlePostMessage}) => {
  return(
    <>
      <div
        style={{
          borderBottom: '1px solid #EAEAEA',
          display: 'flex',
          justifyContent: 'space-between',
        }}
      >
      <Typography
        sx={{fontSize: '24px', fontWeight: '700', color: '#131626', padding: "30px 20px"}}
      >
        Support Center
      </Typography>
      <ul
        style={{
          listStyle: "none",
          display: "flex",
          alignItems: "center"
        }}
      >
      <li>
        <Button
          style={{
            margin: "0 12px",
            padding: "10px 16px",
            border: "1px solid #F4F4F4",
            borderRadius: "50px",
            color: "#6C5DD3",
              whiteSpace: "nowrap"
          }}
        >
          Send Email
        </Button>
      </li>
      <li>
        <Button
          style={{
            margin: "0 12px",
            padding: "10px 16px",
            border: "1px solid #F4F4F4",
            borderRadius: "50px",
            color: "#6C5DD3",
              whiteSpace: "nowrap"
          }}
        >
          Send Message
        </Button>
      </li>
      <li>
        <Button
          style={{
            background: "#6C5DD3",
            margin: "0 12px",
            padding: "10px 16px",
            borderRadius: "50px",
            color: "white",
            boxShadow: "0px 5px 10px 0px rgba(74, 92, 208, 0.2)",
              whiteSpace: "nowrap"
          }}
          onClick={handlePostMessage}
        >
        <AddIcon
          style={{marginRight: "5px"}}
        />
          Post New Message
        </Button>
      </li>
      <li>
        <div
          style={{
            width: 1, height: 24, background: "#F4F4F4", margin: "0 12px",
          }}/>
      </li>
      <li>
        <Button
          sx={{
            minWidth: 40,
            minHeight: 40,
            margin: "0 12px",
            borderRadius: "50%",
            border: "1px solid #F4F4F4",
            color: "#6C5DD3"
          }}
          onClick={handleInteractSupport}
        >
        <CloseIcon/>
        </Button>
      </li>
    </ul>
  </div>
  <SupportTickets/>
  </>
)
}