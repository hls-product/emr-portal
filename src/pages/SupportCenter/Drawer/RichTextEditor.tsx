import * as React from 'react';
import {styled} from '@mui/system'

export const RichTextEditor: React.FC = () => {
  return(
    <div style={{ border:"1px solid rgba(234, 234, 234, 1)", borderRadius: "12px",}}>
        <textarea name="" id="" style={{
            width: "100%",
            height: "200px",
            border:0,
            background: "transparent",
            padding: "10px",
        }}/>
    </div>
  )
}