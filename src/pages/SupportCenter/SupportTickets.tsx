import * as React from 'react';
import EditOutlinedIcon from '@mui/icons-material/EditOutlined';
import DeleteOutlinedIcon from '@mui/icons-material/DeleteOutlined';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import "./Support.css"


export const SupportTickets: React.FC = () => {

  const tickets = [
    {
      publishedDate: "05.06.2022",
      noticeDate: "05.06.2022",
      to: ["Admin", "Accountant", "Doctor", "Pharmacist", "Pathologist", "Super Admin", "Nurse"],
      title: "Ticket #1",
      body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce eu sollicitudin magna, a venenatis sapien. Nullam efficitur quam mollis leo porttitor vehicula. Mauris sed tristique enim. Aenean condimentum nulla sit amet libero gravida, vitae commodo felis iaculis. Quisque consectetur odio sed tortor bibendum malesuada. Sed varius ipsum eget vestibulum semper. Etiam vehicula laoreet neque vitae tempus. Aenean hendrerit imperdiet enim, non elementum orci facilisis vel. Nulla lacinia arcu enim, et vulputate ante finibus nec. Donec at tincidunt massa."
    },
    {
      publishedDate: "05.06.2022",
      noticeDate: "05.06.2022",
      to: ["Admin", "Accountant", "Doctor", "Pharmacist", "Pathologist", "Super Admin", "Nurse"],
      title: "Ticket #2",
      body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce eu sollicitudin magna, a venenatis sapien. Nullam efficitur quam mollis leo porttitor vehicula. Mauris sed tristique enim. Aenean condimentum nulla sit amet libero gravida, vitae commodo felis iaculis. Quisque consectetur odio sed tortor bibendum malesuada. Sed varius ipsum eget vestibulum semper."
    }, {
      publishedDate: "05.06.2022",
      noticeDate: "05.06.2022",
      to: ["Admin", "Accountant", "Doctor", "Pharmacist", "Pathologist", "Super Admin", "Nurse"],
      title: "Ticket #3",
      body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce eu sollicitudin magna, a venenatis sapien. Nullam efficitur quam mollis leo porttitor vehicula. Mauris sed tristique enim. Aenean condimentum nulla sit amet libero gravida, vitae commodo felis iaculis."
    },
  ];

  const [readMore, setReadMore] = React.useState(() =>
    new Array(tickets.length).fill(true)
  )

  const toggleReadMore = (index:number) => {
    setReadMore(() => {
      const newState = [...readMore];
      newState[index] = !readMore[index];
      return newState;
    })
    console.log(readMore)
  }

  const buttonOutlineColor = ["#131626", "#22AA4F", "#1685DA", "#FF7300", "#E03535", "#7B61FF", "#F92F91"];

  return (
    <div style={{
      display: "flex",
      padding: 0,
    }}>
      <div>
        {tickets.map((ticket,index) => {
          return(
            <div
              className="ticket-container"
              style={{
                marginBottom: "6px",
                padding:"20px",
                margin: "6px 0",
                background:"#ffffff",
                borderRadius:"16px"
              }}>
              <div style={{
                display: "flex",
                marginBottom: 12,
                justifyContent: "space-between",
                alignItems:"center"
              }}>
                <div className="ticket-date">
                  <p>Published Date: <span>{ticket.publishedDate}</span></p>
                  <div
                    style={{
                      width:1,
                      height: 14,
                      margin:"0 16px",
                      background:"#F4F4F4"
                    }}/>
                  <p>Notice Date: <span>{ticket.noticeDate}</span></p>
                </div>
                <div className="ticket-edit">
                  <EditOutlinedIcon style={{color:"#4E4E4E"}}/>
                  <DeleteOutlinedIcon style={{color:"#4E4E4E"}}/>
                </div>
              </div>
              <div className="ticket-to">
                To:{
                ticket.to.map((item, index) => {
                  return(
                    <button style={{
                      background:"transparent",
                      color:buttonOutlineColor[index],
                      border:"1px solid"+ buttonOutlineColor[index],
                      borderRadius: 20,
                      padding: "3px 12px",
                      margin: "12px 4px",
                      fontSize: 12
                    }}>{item}</button>
                  )})
              }
              </div>
              <hr style={{
                background:"#F4F4F4",
                height:1,
                width:"100%",
                border:"none",
                margin:"0"
              }}/>
              <div className="ticket-content">
                <h1
                  style={{
                    fontSize: 20,
                    lineHeight:"32px",
                    paddingTop: 12,
                    margin:0}}
                >
                  {ticket.title}
                </h1>
                <p style={{
                  padding:"8px 0",
                  margin: 0
                }}>
                  {readMore[index] ? ticket.body.slice(0, 360): ticket.body}
                </p>
                <div
                  style={{
                    marginTop:8,
                    display:"flex",
                    alignItems:"center",
                    color:"#6C5DD3",
                    cursor:"pointer"
                  }}
                  onClick={(event)=>
                    toggleReadMore(index)}
                >
                  {(ticket.body.length>360) && (
                    (readMore[index] &&
                      <>
                        Read more
                        <KeyboardArrowDownIcon
                          style={{marginLeft: 8}}/>
                      </>
                    ) || (!readMore[index] && <>
                      Read less
                      <KeyboardArrowUpIcon
                        style={{marginLeft: 8}}/>
                    </>)
                  )}
                </div>
              </div>
            </div>
          )
        })}
      </div>
    </div>)
}