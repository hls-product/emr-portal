import React from 'react'

import {styled} from '@mui/system'
import {Button, Tabs, Tab} from "@mui/material";

//------------Custom Button--------------------
export const PillBtnPrimary = styled(Button)({
    background: "#6C5DD3",
    margin: "0 12px",
    padding: "10px 24px",
    borderRadius: "50px",
    color: "white",
    lineHeight: '20px',
    fontSize: "14px",
    '&:hover': {
        background: '#6C5DD3'
    }
});

export const PillBtnSecondary = styled(Button)({
    background: "white",
    margin: "0 12px",
    padding: "10px 25px",
    borderRadius: "50px",
    color: "#6C5DD3",
    lineHeight: '20px',
    fontSize: "14px",
});