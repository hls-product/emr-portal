import * as React from "react";

import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import { Box, Typography } from "@mui/material";
import { useLocation } from "react-router-dom";

import { Messages } from "./Messages";
import { PillBtnPrimary } from "./SupportComponent";
import { SupportTickets } from "./SupportTickets";
import { Tab } from "../../components/tab";

export const Support: React.FC = () => {
  const [value, setValue] = React.useState<any>(0);
  const [openDialog, setOpenDialog] = React.useState<boolean>(false);

  const tabList = [
    {
      title: "Support Center",
      element: (
        <Messages openDialog={openDialog} setOpenDialog={setOpenDialog} />
      ),
    },
    {
      title: "Announcement",
      element: <SupportTickets />,
    },
  ];

  return (
    <Box
      style={{
        height: "100%",
        display: "flex",
        flexDirection: "column",
        flex: 1,
      }}
    >
      <Box
        style={{
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <Typography
          fontSize={"24px"}
          fontWeight={700}
          color={"#131626"}
          style={{ paddingBottom: "20px" }}
          children="Support Center"
        />
        <ul
          style={{
            listStyle: "none",
            display: "flex",
            alignItems: "center",
            marginTop: "0px",
          }}
        >
          {value === 1 && (
            <li>
              <PillBtnPrimary>New Announcement</PillBtnPrimary>
            </li>
          )}
          {value === 0 && (
            <li>
              <PillBtnPrimary
                style={{ boxShadow: "0px 5px 10px 0px rgba(74, 92, 208, 0.2)" }}
                onClick={() => setOpenDialog(true)}
              >
                New Chat
                <EditOutlinedIcon style={{ marginLeft: "15px" }} />
              </PillBtnPrimary>
            </li>
          )}
        </ul>
      </Box>
      <Tab children={tabList} defaultValue={0}></Tab>
    </Box>
  );
};
