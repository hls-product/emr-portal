import {
  Box,
  Typography,
  Grid,
  FormControl,
  OutlinedInput,
  styled,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  TextField,
  DialogActions,
} from "@mui/material";
import React, { ChangeEvent } from "react";

//   import { SearchBar } from "../../../components/searchBar";
import { ChatBox } from "./ChatBox";
import {
  BoxHistory,
  ChatHistoryCard,
  ChatOverview,
  ContentOverview,
  MessengerBox,
  MessengerSearchBox,
  Notify,
  Overview,
  StyledTypography,
} from "./style";

import search from "../../../assets/search.svg";

import {
  chatService,
  patientService,
  practitionersService,
  userActiveService,
} from "../../../services/agent";
import { useCollection } from "react-firebase-hooks/firestore";
import {
  transformConversation,
  transformMessage,
} from "../../../services/firebaseService/utils";
import SendOutlinedIcon from "@mui/icons-material/SendOutlined";
import {
  getDoc,
  Timestamp,
  DocumentReference,
  query,
  collection,
  orderBy,
  limit,
  refEqual,
  getDocs,
} from "firebase/firestore";
import { Message } from "../../../models/message";
import doctor from "../../../assets/image/doctor.svg";
import { db } from "../../../services/firebaseService/config";
import InsertPhotoIcon from "@mui/icons-material/InsertPhoto";
import AttachmentIcon from "@mui/icons-material/Attachment";
import MicIcon from "@mui/icons-material/Mic";
import CloseIcon from "@mui/icons-material/Close";
import { produceWithPatches } from "immer";
import { FlexBox } from "../../../styles";
import TextsmsOutlinedIcon from "@mui/icons-material/TextsmsOutlined";
import { patientModel } from "../../../models/patients";
import _ from "lodash";
import { practitionerModel } from "../../../models/practitioners/practitioners";
import { PatientGetRequest } from "../../../models/apiRequest/patient";
import Loading from "../../../components/Loading";

interface ChatHistory {
  setSelectedChat: React.Dispatch<React.SetStateAction<string>>;
  setName: React.Dispatch<React.SetStateAction<string>>;
  conversations: Conversation[];
  setChatBoxPhoto: React.Dispatch<React.SetStateAction<string>>;
  conversationsSnapshot: ConversationSnapshot[];
}
interface ConversationSnapshot {
  id: string;
  users: User[];
}
interface User {
  ref?: DocumentReference;
  username: string;
  display: string;
  lastActive?: any;
  photoURL?: string;
}
interface Conversation {
  id: string;
  usersRef: DocumentReference[];
  lastActive: number;
  lastContent: string;
  lastContentType: "chat" | "attach" | "call" | "rating";
  display: string;
  photoURL: string;
  lastUser: DocumentReference;
  usersSeen: string[];
}
interface Props {
  openDialog: boolean;
  setOpenDialog: React.Dispatch<React.SetStateAction<boolean>>;
}
export interface NewConversationDialogProps {
  open: boolean;
  onClose: () => void;
  setConvoId: React.Dispatch<React.SetStateAction<string>>;
}
const NewConversationDialog: React.FC<NewConversationDialogProps> = (
  props: NewConversationDialogProps
) => {
  const { onClose, open, setConvoId } = props;
  const [selectedUsers, setSelectedUsers] = React.useState<string[]>([]);
  const [result, setResult] = React.useState<Array<patientModel | practitionerModel>>(
    []
  );
  const [multipleResults, setMultipleResults] = React.useState<string[]>([]);
  const handleSearch = _.debounce(
    async (event: ChangeEvent<HTMLInputElement>) => {
      const patientsRes = await patientService.getList({
        params: { page: 1, size: 50 } as PatientGetRequest,
      });
      const practitionersRes = await practitionersService.getList(
        {
          params:{
            page: 1,
            size: 50
          }
        }
      );
      const data = [
        ...patientsRes.data,
        ...(practitionersRes.data as unknown as practitionerModel[]),
      ];
      console.log(data);
      setResult(
        data.filter((item) => {
          const name = item.name?.find((x) => x.use === "official");
          return event.target.value
            ? name?.family?.includes(event.target.value) ||
                name?.text?.includes(event.target.value)
            : false;
        })
      );
    },
    500
  );
  const onClickSearchResult = (id: string, name: string) => {
    if (!multipleResults.some((item) => item === id))
      setMultipleResults([...multipleResults, id]);
    if (!selectedUsers.some((item) => item === name))
      setSelectedUsers([...selectedUsers, name]);
  };
  const handleSubmit = async () => {
    const res = await chatService.getConversationForSelectedUser([
      ...multipleResults,
    ]);
    if (res && res.length > 0) setConvoId(res[0].id);
    else chatService.createNewConversation(multipleResults, selectedUsers);
    setMultipleResults([]);
    setSelectedUsers([]);
    setResult([]);
    onClose();
  };

  return (
    <Dialog
      open={open}
      onClose={() => {
        setMultipleResults([]);
        setSelectedUsers([]);
        setResult([]);
        onClose();
      }}
      sx={{
        ".MuiPaper-root.MuiPaper-elevation.MuiPaper-rounded.MuiPaper-elevation24.MuiDialog-paper.MuiDialog-paperScrollPaper.MuiDialog-paperWidthSm":
          {
            width: "60%",
            height: "60%",
            padding: "24px 8px",
          },
      }}
    >
      <DialogTitle sx={{ fontSize: "20px" }}>NEW CONVERSATION</DialogTitle>
      <DialogContent>
        <DialogContentText>Create a new chat with:</DialogContentText>
        <TextField
          autoFocus
          id="name"
          type="text"
          fullWidth
          variant="standard"
          onChange={handleSearch}
        />
        <Box sx={{ marginTop: "16px" }}>
          {selectedUsers.map((item, index) => (
            <span
              style={{
                background: "#2c2c2c",
                color: "white",
                padding: "4px 0 4px 12px",
                marginRight: "4px",
                borderRadius: "16px",
              }}
            >
              {item}
              <CloseIcon
                style={{
                  transform: " translateY(20%)",
                  height: "16px",
                  cursor: "pointer",
                }}
                onClick={() => {
                  const newNameArray = [...selectedUsers];
                  const newIdArray = [...multipleResults];
                  newNameArray.splice(index, 1);
                  newIdArray.splice(index, 1);
                  setSelectedUsers(newNameArray);
                  setMultipleResults(newIdArray);
                }}
              />
            </span>
          ))}
        </Box>
        <Box
          sx={{
            overflowY: "scroll",
            msOverflowStyle: "none",
            scrollbarWidth: "none",
            margin: 0,
            padding: 0,
            height: "75%",
            "&::-webkit-scrollbar": {
              display: "none",
            },
          }}
        >
          {result?.length > 0 &&
            result.map((item) => {
              const officialName = item.name?.find((x) => x.use === "official");
              return (
                <Box
                  sx={{
                    display: "flex",
                    gap: "16px",
                    alignItems: "center",
                    borderBottom: "1px solid #EAEAEA",
                    padding: "12px 16px",
                    cursor: "pointer",
                    "&:hover": { opacity: 0.6 },
                  }}
                  onClick={() =>
                    onClickSearchResult(
                      item.id as string,
                      officialName?.text || (officialName?.family as string)
                    )
                  }
                  key={item.id}
                >
                  <img src={doctor} width={35} height={35} />
                  <Typography>
                    {officialName?.text || (officialName?.family as string)}
                  </Typography>
                </Box>
              );
            })}
        </Box>
      </DialogContent>
      <DialogActions>
        <Button
          onClick={() => {
            setMultipleResults([]);
            setSelectedUsers([]);
            setResult([]);
            onClose();
          }}
          sx={{ fontSize: 16 }}
        >
          Cancel
        </Button>
        <Button
          onClick={handleSubmit}
          sx={{
            fontSize: 16,
            background: "#6C5DD3",
            color: "white",
            padding: "8px 16px",
          }}
        >
          Go to conversation
        </Button>
      </DialogActions>
    </Dialog>
  );
};

const RenderChatHistory = (Props: ChatHistory) => {
  const {
    setSelectedChat,
    setName,
    setChatBoxPhoto,
    conversations,
    conversationsSnapshot,
  } = Props;

  return (
    <BoxHistory>
      {conversations
        .sort((a, b) => -a.lastActive + b.lastActive)
        .map((item, index) => (
          <ChatHistoryCard
            onClick={() => {
              setSelectedChat(item.id);
              setName(
                item.display ||
                  (conversationsSnapshot
                    ?.find((x) => x.id === item.id)
                    ?.users.filter(
                      (item: User) =>
                        item.username && item.username != chatService.username()
                    )
                    .map((item: User) => item.display)
                    .join(", ") as string)
              );
              setChatBoxPhoto(item.photoURL);
              chatService.setLastActive();
              chatService.setSeenUsers(item.id);
            }}
            key={index}
          >
            <ChatOverview>
              <img src={doctor} width={42} height={42} />
              <Overview>
                <StyledTypography
                  fontSize={16}
                  fontWeight="400"
                  color="#131626"
                >
                  {item.display ||
                    (conversationsSnapshot
                      ?.find((x) => x.id === item.id)
                      ?.users.filter(
                        (item: User) =>
                          item.username &&
                          item.username != chatService.username()
                      )
                      .map((item: User) => item.display)
                      .join(", ") as string)}
                </StyledTypography>
                {conversationsSnapshot.length > 0 && (
                  <ContentOverview>
                    {" "}
                    {refEqual(item.lastUser, chatService.userRef())
                      ? "You"
                      : conversationsSnapshot
                          ?.find((x) => x.id === item.id)
                          ?.users?.filter((u) =>
                            refEqual(u.ref as DocumentReference, item.lastUser)
                          )[0]?.display}{" "}
                    {item.lastContent
                      ? item.lastContentType === "chat"
                        ? ": " + item.lastContent
                        : ": Attach a file"
                      : null}
                  </ContentOverview>
                )}
              </Overview>
            </ChatOverview>

            <Overview>
              <FlexBox column={true} justify="space-between" align="center">
                <StyledTypography
                  fontSize={12}
                  fontWeight="500"
                  color="#131626"
                >
                  {new Date(item.lastActive)
                    .toLocaleString()
                    .split(" ")[1]
                    .slice(0, 5)}
                </StyledTypography>
                {/* <Notify isRead={false}></Notify> */}
                {item.usersSeen && (
                  <Notify
                    isRead={item.usersSeen.includes(chatService.userId())}
                  ></Notify>
                )}
              </FlexBox>
            </Overview>
          </ChatHistoryCard>
        ))}
    </BoxHistory>
  );
};

export const Messages: React.FC<Props> = ({ openDialog, setOpenDialog }) => {
  const [convoId, setConvoId] = React.useState<string>("dummy");
  const [name, setName] = React.useState<string>("");
  const [chatBoxPhoto, setChatBoxPhoto] = React.useState<string>("");
  const [conversationsSnapshot, setConversationsSnapshot] = React.useState<
    ConversationSnapshot[]
  >([]);
  const [value, setValue] = React.useState("");
  //real-time messages and chat-box selector
  const [messagesSnapshot, messagesLoading, _error] = useCollection(
    chatService.generateQueryGetMessages(convoId)
  );
  const [conversationsData, conversationsDataLoading, __error] = useCollection(
    chatService.generateQueryGetConversation()
  );
  const [attachments, setAttachments] = React.useState<File[]>([]);
  //get users snapshot
  React.useEffect(() => {
    if (!conversationsDataLoading && conversationsData) {
      const conversationsSnapshot = conversationsData.docs.map(async (item) => {
        const usersPromises = await item
          .data()
          ["usersRef"].map(async (ref: DocumentReference) => {
            const data = (await getDoc(ref)).data();
            return { ...data, ref: ref };
          });
        return await Promise.all(usersPromises).then((res) => ({
          id: item.id,
          users: res as User[],
        }));
      });
      Promise.all(conversationsSnapshot).then((res) => {
        setConversationsSnapshot(res as unknown as ConversationSnapshot[]);
        if (convoId === "dummy") {
          setConvoId(conversationsData?.docs[0].id as string);
          setName(
            conversationsData?.docs[0].data()["display"] ||
              res[0].users
                .filter(
                  (item: User) =>
                    item && item.username != chatService.username()
                )
                .map((item: User) => item.display)
                .join(", ")
          );
          setChatBoxPhoto(conversationsData?.docs[0].data()["photoURL"]);
        }
      });
    }
  }, [conversationsData]);
  //set text input
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue(event.target.value);
  };
  //send message
  const sendText = async (text: string) => {
    if (text) {
      chatService.sendNewMessages(text, convoId, "chat");
      setValue("");
    }
    if (attachments.length > 0) {
      attachments.forEach((item) =>
        chatService.sendNewAttachment(item, convoId)
      );
      setAttachments([]);
    }
  };
  const mediaInputRef = React.useRef<HTMLInputElement>(null);
  const handleImgInputClick = () => {
    mediaInputRef.current?.click();
  };
  const handleImgChange = async (event: ChangeEvent<HTMLInputElement>) => {
    if (event.currentTarget?.files) {
      setAttachments(Array.from(event.currentTarget?.files));
    }
  };
  const quickReply = [
    "Yes, I can help you with this!",
    "Come back next week!",
    "You need to take prescription medication",
    "OK!",
    "See you again!",
  ];
  const quickReplyExtend = [
    '"Yes, I can help you with this!"',
    '"Come back next week!"!',
    '"You need to take prescription medication."',
    "See you again !",
  ];
  const [openQuickReply, setOpenQuickReply] = React.useState<boolean>(false);
  const handleOpenQuickReply = () => {
    setOpenQuickReply(!openQuickReply);
  };
  //rendering..
  return (
    <MessengerBox>
      <Box
        sx={{ display: "flex", justifyContent: "space-between", gap: "16px" }}
      >
        <Box
          sx={{
            background: "#ffffff",
            flexBasis: "25%",
            padding: "16px",
            borderRadius: "16px",
          }}
        >
          <MessengerSearchBox>
            <img
              src={search}
              alt="Search"
              width={20}
              height={20}
              style={{ margin: "14px 0 14px" }}
            />
            {/* <SearchBar sx={{ m: 1 }} placeholder="Search ..." /> */}
          </MessengerSearchBox>
          {conversationsDataLoading && <Loading/>}
          {!conversationsDataLoading && (
            <RenderChatHistory
              conversations={
                conversationsData?.docs.map((item) =>
                  transformConversation(item)
                ) as Conversation[]
              }
              setSelectedChat={setConvoId}
              setName={setName}
              setChatBoxPhoto={setChatBoxPhoto}
              conversationsSnapshot={conversationsSnapshot}
            />
          )}
        </Box>
        <Box sx={{ background: "#ffffff", flex: 1, borderRadius: "16px" }}>
          {messagesLoading && <Loading/>}
          {!messagesLoading && !conversationsDataLoading && (
            <ChatBox
              chatBox={
                messagesSnapshot?.docs.map((item) =>
                  transformMessage(item)
                ) as Message[]
              }
              display={name}
              photoURL={chatBoxPhoto || doctor}
              convoId={convoId}
            />
          )}
          <Box>
            {attachments.map((item, index) =>
              item.type === "image/png" || "image/jpeg" ? (
                <img
                  src={URL.createObjectURL(item)}
                  style={{ height: "80px" }}
                />
              ) : item.type === "video/*" ? (
                <video src={URL.createObjectURL(item)} />
              ) : null
            )}
          </Box>
          {openQuickReply && (
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                padding: "0 16px",
                background: "white",
              }}
            >
              <span
                style={{
                  color: "rgba(159, 160, 166, 1)",
                  fontSize: "12px",
                  background: "white",
                  paddingLeft: "20px",
                }}
              >
                Click on an answer for a quick reply
              </span>
              {quickReplyExtend.map((item) => (
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    flexWrap: "wrap",
                    whiteSpace: "nowrap",
                    margin: "8px 0",
                  }}
                >
                  <span>{item}</span>
                  <div
                    style={{
                      background: "rgba(255, 255, 255, 1)",
                      border: "1px solid rgba(234, 234, 234, 1)",
                      padding: "0 12px",
                      borderRadius: "21px",
                      whiteSpace: "nowrap",
                      color: "rgba(123, 97, 255, 1)",
                      fontSize: "12px",
                      cursor: "pointer",
                    }}
                    onClick={() => sendText(item)}
                  >
                    Quick send
                  </div>
                </div>
              ))}
            </Box>
          )}
          <FormControl
            sx={{
              width: "100%",
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              marginTop: "12px",
              gap: "12px",
              padding: "16px",
            }}
            variant="outlined"
          >
            <Box sx={{ display: "flex", alignItems: "center" }}>
              <TextsmsOutlinedIcon
                onClick={handleOpenQuickReply}
                style={{
                  cursor: "pointer",
                  color: "rgba(108, 93, 211, 1)",
                  margin: "0 16px",
                }}
              />
            </Box>
            <Box sx={{ display: "flex", alignItems: "center" }}>
              <input
                type="file"
                ref={mediaInputRef}
                style={{ display: "none" }}
                accept="video/*, image/*"
                onChange={handleImgChange}
                multiple
              />
              <InsertPhotoIcon
                onClick={handleImgInputClick}
                sx={{ padding: 0, margin: 0, cursor: "pointer" }}
                color="primary"
              />
            </Box>
            <Box sx={{ display: "flex", alignItems: "center" }}>
              <AttachmentIcon
                sx={{ padding: 0, margin: 0, cursor: "pointer" }}
                color="primary"
              />
            </Box>
            <Box sx={{ display: "flex", alignItems: "center" }}>
              <MicIcon
                sx={{ padding: 0, margin: 0, cursor: "pointer" }}
                color="primary"
              />
            </Box>
            <OutlinedInput
              id="input-text"
              value={value}
              onChange={handleChange}
              endAdornment={
                <SendOutlinedIcon
                  sx={{
                    marginLeft: "5px",
                    cursor: "pointer",
                  }}
                  color="primary"
                  onClick={() => sendText(value)}
                />
              }
              inputProps={{
                "aria-label": "text",
              }}
              sx={{
                height: "40px",
                flex: 1,
                borderRadius: "12px",
                color: "rgba(19, 22, 38, 1)",
                fontSize: "16px",
              }}
              onKeyPress={(event) => {
                if (event.key === "Enter") sendText(value);
              }}
            />
          </FormControl>
        </Box>
      </Box>
      <NewConversationDialog
        open={openDialog}
        onClose={() => setOpenDialog(false)}
        setConvoId={setConvoId}
      />
    </MessengerBox>
  );
};
