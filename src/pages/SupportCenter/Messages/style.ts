import { styled as muiStyled, Box, Typography } from "@mui/material";
interface StyledTypographyProps {
    color?: string;
    fontSize?: number;
    fontWeight?:
      | "100"
      | "200"
      | "300"
      | "400"
      | "500"
      | "600"
      | "700"
      | "800"
      | "900";
  }

export const BoxHistory = muiStyled(Box)(() => ({
  display: "flex",
  alignItems: "flex-start",
  justifyContent: "flex-start",
  flexDirection: "column",
  gap: "10px",
  marginTop: "16px",
}));

export const MessengerBox = muiStyled(Box)(() => ({
  display: "flex",
  flexDirection: "column",
  gap: "24px"
}));

export const ChatHistoryCard = muiStyled("div")(() => ({
  border: "1px solid #F4F4F4",
  borderRadius: "8px",
  padding: "12px",
  display: "flex",
  alignItems: "center",
  justifyContent: "space-between",
  width: "100%",
  boxSizing: "border-box",
  cursor: "pointer",
}));

export const ChatOverview = muiStyled("div")(() => ({
  display: "flex",
  alignItems: "center",
  justifyContent: "flex-start",
  gap: "12px",
}));

export const Overview = muiStyled("div")(() => ({
  display: "flex",
  alignItems: "flex-start",
  justifyContent: "flex-start",
  flexDirection: "column",
}));

export const ContentOverview = muiStyled(Typography)(() => ({
  fontSize: "12px",
  fontWeight: "500",
  color: "#4E4E4E",
  whiteSpace: "nowrap",
  overflow: "hidden",
  textOverflow: "ellipsis",
  width: "169px",
}));

export const Notify = muiStyled(Typography)<{ isRead: boolean }>(
  ({ isRead }) => ({
    width: "8px",
    height: "8px",
     background: `${isRead ? "#FFFFFF" : "#6C5DD3"}`,
     borderRadius: "50%",
     marginTop: "8px"
  })
);

export const MessengerSearchBox = muiStyled("div")(() => ({
  display: "flex",
  alignItems: "center",
  justifyContent: "flex-start",
  width: "100%",
  border: "1px solid #EAEAEA",
  borderRadius: "12px",
  paddingRight: "8px",
  paddingLeft: "20px",
  boxSizing: "border-box",
}));

export const StyledTypography = muiStyled(Typography)<StyledTypographyProps>(
    ({ color, fontSize, fontWeight }) => ({
      fontSize: `${fontSize}px`,
      fontWeight: fontWeight,
      color: color,
    })
  );


  
  export const ChatBoxWrapper = muiStyled("div")(() => ({
    display: "flex",
    flexDirection: "column",
    borderRadius: "16px",
    height: 665,
    padding: "12px 20px"
  }));
  
  export const ChatBoxHeader = muiStyled("div")(() => ({
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    borderBottom:"1px solid #EAEAEA"
  }));
  
  export const UserWrapper = muiStyled("div")(() => ({
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    width: "100%",
  }));
  
  export const UserAvatar = muiStyled("div")(() => ({
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
  }));
  
  export const UserActiveStatus = muiStyled("div")(() => ({
    width: "9px",
    height: "9px",
    borderRadius: "50%",
    background: "#22AA4F",
    position: "absolute",
    top: 0,
    right: 0,
    border: "2px solid #FFFFFF",
  }));
  
  export const ChatStatus = muiStyled("div")(() => ({
    padding: "3px 12px",
    borderRadius: "21px",
    fontSize: "14px",
    fontWeight: "500"
  }));
  
interface ChatContentProps {
  from?: number | string;
  timeStamp?: number;
  currentTimeStamp?: number;
  index?: number;
  chatContentType?: "chat" | "attach" | "call" | "rating";
}

export const ChatContentWrapper = muiStyled("div")<ChatContentProps>(
  ({ from }) => ({
    display: "flex",
    alignItems: `${from === 9999 ? "flex-end" : "flex-start"}`,
    justifyContent: "center",
    flexDirection: "column",
    marginBottom: "8px",
  })
);

export const ChatTime = muiStyled(Typography)<ChatContentProps>(
  ({ timeStamp, currentTimeStamp, index }) => ({
    fontSize: "11px",
    fontWeight: "600",
    color: "#9FA0A6",
    width: "100%",
    margin: "24px 0 14px 0",
    display: `${
      (timeStamp as unknown as number) ===
      (currentTimeStamp as unknown as number)
        ? index === 0
          ? "block"
          : "none"
        : ((timeStamp as unknown as number) -
            (currentTimeStamp as unknown as number)) /
            60000 >
          15
        ? "block"
        : "none"
    }`,
  })
);

export const ChatContent = muiStyled("div")<ChatContentProps>(({ from }) => ({
  display: "flex",
  alignItems: "flex-end",
  justifyContent: `${from === 9999 ? "flex-end" : "flex-start"}`,
}));

export const ChatText = muiStyled(Typography)<ChatContentProps>(({ from }) => ({
  background: `${from === 9999 ? "#6C5DD3" : "#F4F4F4"}`,
  width: "100%",
  padding: "12px",
  borderRadius: `${
    from === 9999 ? "16px 16px 0px 16px" : "16px 16px 16px 0px"
  }`,
  fontSize: "13px",
  fontWeight: "400",
  color: `${from === 9999 ? "#FFFFFF" : "#131626"}`,
  marginLeft: "12px",
}));

export const ChatMedia = muiStyled("div")<ChatContentProps>(({ from }) => ({
  display: "flex",
  alignItems: "center",
  justifyContent: "flex-start",
  background: `${from === 9999 ? "#6C5DD3" : "#F4F4F4"}`,
  width: "fit-content",
  padding: "12px",
  borderRadius: `${
    from === 9999 ? "16px 16px 0px 16px" : "16px 16px 16px 0px"
  }`,
  fontSize: "13px",
  fontWeight: "500",
  color: `${from === 9999 ? "#FFFFFF" : "#131626"}`,
  marginLeft: "12px",
}));

export const ChatImage = muiStyled("div")<ChatContentProps>(
  ({ chatContentType }) => ({
    width: "40px",
    height: "40px",
    borderRadius: "50%",
    background: `${chatContentType === "attach" ? "#6C5DD3" : "#FFFFFF"}`,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  })
);

export const ChatMediaTextWrapper = muiStyled("div")(() => ({
  width: "fit-content",
  marginLeft: "10px",
  display: "flex",
  flexDirection: "column",
  alignItems: "flex-start",
  justifyContent: "center",
}));
