import React, { ChangeEvent } from "react";
import { Message } from "../../../models/message";
import {
  ChatBoxWrapper,
  ChatBoxHeader,
  UserAvatar,
  UserActiveStatus,
  ChatStatus,
  UserWrapper,
  StyledTypography 
} from "./style";
import VideocamIcon from '@mui/icons-material/Videocam';
import { ChatContentComponent } from "./ChatContent";

import doctor from "../../assets/image/doctor_1.svg";
import { StringMappingType } from "typescript";
import { Timestamp } from "firebase/firestore";
import { Box } from "@mui/material";

interface Props {
  chatBox: Message[];
  display: string;
  photoURL?: string;
  convoId?:string;
}

export const ChatBox: React.FC<Props> = ({ chatBox, display, photoURL, convoId }) => {
  const chatContainer = document.getElementById("chat-container")  
  React.useEffect(()=> {
    if(chatContainer){
      chatContainer.scrollTop = chatContainer.scrollHeight
    }
  },[chatBox])
  return (
    <ChatBoxWrapper>
      <ChatBoxHeader>
        <UserWrapper>
          <UserAvatar>
            <div style={{ position: "relative" }}>
              <img src={photoURL || '../../../assets/avatar.svg'} width={38} height={38} />
              <UserActiveStatus />
            </div>
            <StyledTypography
              marginLeft="12px"
              fontSize={16}
              fontWeight="500"
              color="#131626"
            >
              {display}
            </StyledTypography>
          </UserAvatar>
          {status && <ChatStatus>{status}</ChatStatus>}
        </UserWrapper>
        <Box>
          <VideocamIcon 
          sx={{padding:0, margin: 0, cursor:"pointer"}} 
          color="primary" 
          fontSize="large" 
          onClick={() => window.location.replace(`https://zoom-clone-test-app.herokuapp.com/${convoId}`)}/>
        </Box>
      </ChatBoxHeader>
      <Box
      id="chat-container"
      sx={{
        overflowY: "scroll",
        msOverflowStyle: "none",
        scrollbarWidth: "none",
        margin: 0,
        padding: 0,
        "&::-webkit-scrollbar":{
          display: "none"
        }
      }}
      >
      <ChatContentComponent chat={chatBox as Message[]} />
      </Box>
    </ChatBoxWrapper>
  );
};
