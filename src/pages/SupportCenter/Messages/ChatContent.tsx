import React, { ChangeEvent } from "react";
import { Message } from "../../../models/message";

import {
  ChatContentWrapper,
  ChatTime,
  ChatContent,
  ChatText,
  ChatMedia,
  ChatImage,
  ChatMediaTextWrapper,
  StyledTypography
} from "./style";

import doctor from "../../../assets/image/doctor.svg";
import call_end from "../../assets/icon/call_end.svg";
import attach_file from "../../assets/icon/attach_file.svg";
import happyEmote from "../../assets/icon/happyEmote.svg";
import eye from "../../assets/icon/eye_view.svg";
import { chatService } from "../../../services/agent";
import { refEqual } from "firebase/firestore";

interface Prop {
  chat: Message[]
}

export const ChatContentComponent: React.FC<Prop> = ({ chat }) => {
  let currentDate = `${new Date().getDate()}/${
    new Date().getMonth() + 1 <= 9
      ? `0${new Date().getMonth() + 1}`
      : `${new Date().getMonth() + 1}`
  }/${new Date().getFullYear()}`;

  // let timeStamp = chat[0].timestamp;
  let timeStamp = chat[0]?.timestamp;
  const handleSetTime = (value: number) => {
    if (value > timeStamp) {
      timeStamp = value;
    }
    return "";
  };
  React.useEffect(() => {
    chat.map((content, index) => {
      if(content.contentType === 'attach') {
        const img = document.getElementById(content.id) as HTMLImageElement | HTMLVideoElement;
        chatService.getAttachment(content.content)
        .then(res => img.src=res)
      }
    })
    
  },[chat])
  
  return (
    <>
      {chat.map((content, index) => (
        <ChatContentWrapper key={index} /*from={content.from}*/ from={refEqual(content.userSent,chatService.userRef()) ? 9999: 1}>
          <ChatTime
            index={index}
            timeStamp={content.timestamp}
            currentTimeStamp={timeStamp}
            align="center"
          >
            <div style={{ display: "none" }}>
              {handleSetTime(content.timestamp)}
            </div>
            {new Date(content.timestamp).toLocaleString().includes(currentDate)
              ? new Date(content.timestamp).toLocaleString().replace(currentDate, "Today").slice(0,12)
              : new Date(content.timestamp).toLocaleString().split(", ").map(item => item.slice(0,5)).join(", ")}
          </ChatTime>

          <ChatContent /*from={content.from}*/ from={refEqual(content.userSent,chatService.userRef()) ? 9999: 1}>
            {/* {content.from !== 9999 && ( */}
            {!refEqual(content.userSent,chatService.userRef()) && (
              <img src={doctor} width={40} height={40} />
            )}
            {content.contentType === "chat" ? (
              <ChatText from={refEqual(content.userSent,chatService.userRef()) ? 9999: 1}>{content.content}</ChatText>
            ) : (
              // <ChatMedia from={refEqual(content.userSent,chatService.userRef()) ? 9999: 1}>
              //   <ChatImage chatContentType={content.contentType}>
              //     <img
              //       src={
              //         content.contentType === "call"
              //           ? call_end
              //           : content.contentType === "rating"
              //           ? happyEmote
              //           : attach_file
              //       }
              //       width={
              //         content.contentType === "call"
              //           ? 18
              //           : content.contentType === "rating"
              //           ? 20
              //           : 17
              //       }
              //       height={
              //         content.contentType === "call"
              //           ? 13
              //           : content.contentType === "rating"
              //           ? 20
              //           : 17
              //       }
              //     />
              //   </ChatImage>
              //   <ChatMediaTextWrapper>
              //     <StyledTypography fontSize={14} fontWeight="600">
              //       {/* {content.label} */}
              //     </StyledTypography>
              //     {content.contentType === "attach" ? (
              //       <div
              //         style={{
              //           display: "flex",
              //           alignItems: "center",
              //           justifyContent: "center",
              //         }}
              //       >
              //         <StyledTypography
              //           fontSize={12}
              //           fontWeight="600"
              //           color="#6C5DD3"
              //           margin="2px 5px 0 0"
              //           style={{
              //             cursor: "pointer",
              //           }}
              //         >
              //           {/* {content.dialogContent} */}
              //         </StyledTypography>
              //         <img src={eye} width={16.5} height={12} />
              //       </div>
              //     ) : (
              //       <StyledTypography
              //         fontSize={12}
              //         fontWeight="500"
              //         marginTop="2px"
              //       >
              //         {/* {content.contentType === "call"
              //           ? content.callDate
              //           : content.rating} */}
              //       </StyledTypography>
              //     )}
              //   </ChatMediaTextWrapper> 
              // </ChatMedia>
              <img 
              id={content.id}
              style={{
                width: "40%",
                marginLeft:"12px"
              }}
              />
            )}
          </ChatContent>

          {/* <StyledTypography
            fontSize={10}
            fontWeight="500"
            color="#999999"
            display={chat.length - 1 === index ? "block" : "none"}
          >
            {content.status}
          </StyledTypography> */}
        </ChatContentWrapper>
      ))}
    </>
  );
};
